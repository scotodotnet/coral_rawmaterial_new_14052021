﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GeneralItemMasterSub.aspx.cs" Inherits="GeneralItemMasterSub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>General Items</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">

                                    <div class="row">

                                        <div class="col-md-3" runat="server" visible="false">
                                            <label class="control-label">ToolCode</label>
                                            <asp:Label ID="txtTransCode" class="form-control" runat="server">
                                            </asp:Label>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Scope</label>
                                            <asp:DropDownList ID="ddlScope" class="form-control select2" runat="server">
                                                <%--<asp:ListItem Value="Enercon">Enercon</asp:ListItem>--%>
                                                <asp:ListItem Value="Coral">Coral</asp:ListItem>
                                                <%--<asp:ListItem Value="Escrow">Escrow</asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">General Item Code</label>
                                            <asp:TextBox ID="txtRefItemCode" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>


                                        <div class="col-md-2">
                                            <label class="control-label">Item Name</label>
                                            <asp:TextBox ID="txtGenItemName" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">HSN Code (supplier number)</label>
                                            <asp:TextBox ID="txtSuppHSNCode" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Unit of Measurement (UOM)</label>
                                            <asp:DropDownList ID="ddlUOM" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Drawing Number</label>
                                            <asp:TextBox ID="txtDrawNo" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                    </div>
                                    <br />

                                    <div class="row">

                                        <div class="col-md-2" runat="server" visible="false">
                                            <label class="control-label">Amount of stations</label>
                                            <asp:TextBox ID="txtAmtStations" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="false">
                                            <label class="control-label">Amount Pro stations</label>
                                            <asp:TextBox ID="txtAmtProStations" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-4" runat="server" visible="false">
                                            <label class="control-label">Comments</label>
                                            <asp:TextBox ID="txtComment" class="form-control" TextMode="MultiLine" Style="resize: none"
                                                runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Special Instructions</label>
                                            <asp:TextBox ID="txtNote" class="form-control" TextMode="MultiLine" Style="resize: none"
                                                runat="server">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <br />

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary"  runat="server" visible="false">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Rate and Tax Details</h3>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label class="control-label">Rate</label>
                                                        <asp:TextBox ID="txtRate" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-2" >
                                                        <label class="control-label">CGST (%)</label>
                                                        <asp:TextBox ID="txtCGST" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">SGST (%)</label>
                                                        <asp:TextBox ID="txtSGST" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-2" style="padding-bottom: 1%">
                                                        <label class="control-label">IGST (%)</label>
                                                        <asp:TextBox ID="txtIGST" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-footer">
                                                <div class="form-group">
                                                    <asp:Button ID="btnSave" class="btn btn-primary" Text="Save" runat="server" OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnBack" class="btn btn-primary" Text="Back" runat="server" OnClick="btnBack_Click" />
                                                    <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnClear_Click" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/Master/BOMMasters.js"></script>
    <script type="text/javascript">
        $('[id*=txtItemName]').on('change', function () {
            var url = "https://translation.googleapis.com/language/translate/v2?key=API_Key";
            url += "&source=" + "DE";
            url += "&target=" + "EN";
            url += "&q=" + escape($('[id*=txtItemName]').val());
            $.get(url, function (data, status) {
                $('[id*=txtItemNameTransulate]').val(data.data.translations[0].translatedText);
            });
        });
    </script>
    <%--<script type="text/javascript">  
         $(document).ready(function () {
             
             //$('[id*=txtMatNo]').on("change", function () {
             window.setInterval(function () {
                 //alert($(this).val());
                 $.ajax({
                     type: "POST",
                     url: "MstAssetItemSub.aspx/GetCTNo",
                    // data: '{id: "' + $(this).val() + '" }',
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (response) {
                          // console.log(response);
                         if (response.d != "false") {
                             //     alert("Called");
                             $('[id*=txtItemCode]').val(response.d);
                            // ShowMessageErr   or('The Material Number Already Taken');
                            // $(this).focus();
                         } else {

                         }
                     },
                     error: function (XMLHttpRequest, textStatus, errorThrown) {
                         alert("Whoops something went wrong!" + errorThrown);
                     }
                 });
             },1000);
         });
        </script> --%>


    <%--<script src="/assets/js/GoogleTransulate.js"></script>--%>
</asp:Content>
