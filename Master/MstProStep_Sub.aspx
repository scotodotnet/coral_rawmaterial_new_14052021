﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstProStep_Sub.aspx.cs" Inherits="Master_MstProStep_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upWastSub" runat="server">
            <ContentTemplate>
                <%--header--%>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Production Step Master</h1>
                </section>

                <%--Body--%>
                <section class="content">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputName">Code</label>
                                            <asp:Label ID="txtCode" runat="server" Text="" Class="form-control" Enabled="false"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputName">Production Step</label>
                                            <asp:TextBox ID="txtProductionStep" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputName">Remark</label>
                                            <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Style="resize: none" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer no-padding">
                                <div class="col-md-6" runat="server" style="padding-top: 25px">
                                    <div class="form-group">
                                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>

