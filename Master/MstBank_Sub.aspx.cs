﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstBank_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionBankCode = "";
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Bank Master";

            if (Session["BankCode"] == null)
            {
                SessionBankCode = "";
            }
            else
            {
                SessionBankCode = Session["BankCode"].ToString();
                txtBankCode.Text = SessionBankCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
            
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        string Def_Chk = "";

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string HostName = getIPAndName.GetName();


        if (chkDefault.Checked == true)
        {
            Def_Chk = "Yes";
        }
        else
        {
            Def_Chk = "No";
        }

        //if (Def_Chk == "Yes")
        //{

        //    SSQL = "select * from MstBank where Default_Bank='Yes'";
        //    DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //    if (DT.Rows.Count > 0)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Already Default Bank exist.. Check it.!');", true);
        //    }
        //    else
        //    {
        //        ErrFlag = false;
        //    }
        //}

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstBank Where BankName='" + txtBankName.Text + "' And BankCode='" + txtBankCode.Text + "' And ";
            SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Bank Name Already Exsit');", true);
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Bank Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtBankCode.Text = Auto_Transaction_No;
                }
            }
        }


        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstBank set BankName='" + txtBankName.Text + "',IFSCCode='" + txtIFSCCode.Text + "',Branch='" + txtBranch.Text + "',";
                SSQL = SSQL + " Default_Bank='" + Def_Chk + "',AmtCharge='" + txtChargesAmt.Text + "',MinCharge='" + txtMinCharge.Text + "',";
                SSQL = SSQL + " MaxCharge='" + txtMaxCharge.Text + "' Where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "'";
                SSQL = SSQL + " And BankCode='" + txtBankCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Designation Details Updated Successfully');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Bank Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstBank(Ccode,Lcode,BankCode,BankName,IFSCCode,Branch,Default_Bank,AmtCharge,MinCharge,MaxCharge,Active,Status,";
                SSQL = SSQL + " UserID,UserName,CreateOn,Host_IP,Host_Name) Values('" + SessionCcode + "','" + SessionLcode + "', ";
                SSQL = SSQL + " '" + txtBankCode.Text + "','" + txtBankName.Text + "','" + txtIFSCCode.Text + "','" + txtBranch.Text + "','" + Def_Chk + "',";
                SSQL = SSQL + " '" + txtChargesAmt.Text + "','" + txtMinCharge.Text + "','" + txtMaxCharge.Text + "','1','Add','" + SessionUserID + "',";
                SSQL = SSQL + " '" + SessionUserName + "','" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "',";
                SSQL = SSQL + " '" + IP + "','" + HostName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Bank Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtBankCode.Enabled = true;

            Response.Redirect("MstBank_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('UOM Details Already Saved');", true);
        }
    }
    private void Clear_All_Field()
    {
        txtBankCode.ReadOnly = false;
        txtBankCode.Text = "";
        txtBankName.Text = "";
        txtIFSCCode.Text = "";
        txtBranch.Text = "";
        chkDefault.Checked = false;
        btnSave.Text = "Save";
        txtChargesAmt.Text = "";
        txtMinCharge.Text = "";
        txtMaxCharge.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete' And ";
        SSQL = SSQL + " BankCode='" + txtBankCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtBankCode.ReadOnly = true;

            txtBankName.Text = DT.Rows[0]["BankName"].ToString();
            txtBranch.Text = DT.Rows[0]["Branch"].ToString();
            txtIFSCCode.Text = DT.Rows[0]["IFSCCode"].ToString();

            txtChargesAmt.Text = DT.Rows[0]["AmtCharge"].ToString();
            txtMinCharge.Text = DT.Rows[0]["MinCharge"].ToString();
            txtMaxCharge.Text = DT.Rows[0]["MaxCharge"].ToString();

            if (DT.Rows[0]["Default_Bank"].ToString() == "Yes")
            {
                chkDefault.Checked = true;
            }
            else
            {
                chkDefault.Checked = false;
            }

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstBank_Main.aspx");
    }
}
