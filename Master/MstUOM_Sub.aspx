﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstUOM_Sub.aspx.cs" Inherits="Mst_UOM_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upUOMSub" runat="server">
            <ContentTemplate>
                  <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Unit Of Measurement Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName"> UOM Code <span class="text-danger">*</span> </label>
                                    <asp:Label  id="txtUOMCode" runat="server" class="form-control"/>
                                    <span id="DeptCode" class="text-danger"></span>
                                    <asp:RequiredFieldValidator ControlToValidate="txtUOMCode" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> UOM Short Name <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtShortName" runat="server" class="form-control" />
                                    <span id="ShortName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtShortName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> UOM Full Name <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtFullName" runat="server" class="form-control" />
                                    <span id="FullName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtFullName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-6" style="padding-top:25px">
                                <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <asp:Button ID="btnClear" class="btn btn-primary" runat="server"  Text="Clear" OnClick="btnClear_Click"/>
                                <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
      
    </div>
</asp:Content>

