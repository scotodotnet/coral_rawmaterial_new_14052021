﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ToolsMasterSub_26052021.aspx.cs" Inherits="ToolsMasterSub_Old" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Tools Item</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">

                                    <div class="row">

                                        <div class="col-md-3" runat="server" visible="false">
                                             <label class="control-label">ToolCode</label>
                                            <asp:Label ID="txtTransCode" class="form-control" runat="server">
                                            </asp:Label>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Assembly Stage</label>
                                            <asp:DropDownList ID="ddlAssemblyStage" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>


                                        <div class="col-md-2">
                                            <label class="control-label">Local Tools No</label>
                                            <asp:TextBox ID="txtToolsNo" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Scope</label>
                                            <asp:DropDownList ID="ddlScope" class="form-control select2" runat="server">
                                                <asp:ListItem Value="Enercon">Enercon</asp:ListItem>
                                                <asp:ListItem Value="Coral">Coral</asp:ListItem>
                                                <asp:ListItem Value="Escrow">Escrow</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Generator Model</label>
                                            <asp:DropDownList ID="ddlGenModel" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Module (Baugruppe)</label>
                                            <asp:DropDownList ID="ddlModule" class="form-control select2" runat="server">
                                                <asp:ListItem Value="1">Stator</asp:ListItem>
                                                <asp:ListItem Value="2">Rotor</asp:ListItem>
                                                <asp:ListItem Value="3">Rotor(Gema)</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Production Step</label>
                                            <asp:DropDownList ID="ddlProductionStep" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <br />
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label">Supplier</label>
                                            <asp:DropDownList ID="ddlSupp" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">SAP (supplier number)</label>
                                            <asp:TextBox ID="txtSuppSAPCode" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <%--Catagery of tools--%>
                                        <div class="col-md-2">
                                            <label class="control-label">Type</label>
                                            <asp:DropDownList ID="ddlType" class="form-control select2" runat="server">
                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                <asp:ListItem Value="1">Contraption</asp:ListItem>
                                                <asp:ListItem Value="2">Tools</asp:ListItem>
                                                <asp:ListItem Value="3">Resources</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Designation (Bezeichnung)</label>
                                            <asp:TextBox ID="txtDesignation" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Tool's Name</label>
                                            <asp:TextBox ID="txtToolsName" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Unit of Measurement (UOM)</label>
                                            <asp:DropDownList ID="ddlUOM" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                        
                                    </div>
                                    <br />

                                    <div class="row">

                                        <div class="col-md-2">
                                            <label class="control-label">HSN Code</label>
                                            <asp:TextBox ID="txtHSN" class="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">BDM Number</label>
                                            <asp:TextBox ID="txtBDMNo" class="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Released</label>
                                            <asp:RadioButtonList ID="rdbReleased" class="form-control" runat="server"
                                                RepeatColumns="4">
                                                <asp:ListItem Value="1" Text="Yes" style="padding-right: 15px" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Drawing Number</label>
                                            <asp:TextBox ID="txtDrawNo" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Revision</label>
                                            <asp:TextBox ID="txtRevision" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Amount of stations</label>
                                            <asp:TextBox ID="txtAmtStations" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                       
                                    </div>

                                    <br />

                                    <div class="row">

                                         <div class="col-md-2">
                                            <label class="control-label">Amount Pro stations</label>
                                            <asp:TextBox ID="txtAmtProStations" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Possible capacity</label>
                                            <asp:TextBox ID="txtPossibleCapa" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                          <div class="col-md-2">
                                            <label class="control-label">Availability drawings</label>
                                            <asp:TextBox ID="txtAvailabilityDraw" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Comments</label>
                                            <asp:TextBox ID="txtComment" class="form-control" TextMode="MultiLine" Style="resize: none"
                                                runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Special Instructions</label>
                                            <asp:TextBox ID="txtNote" class="form-control" TextMode="MultiLine" Style="resize: none"
                                                runat="server">
                                            </asp:TextBox>
                                        </div>
                                        
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Have Image ?</label>
                                                <br />
                                                <div class="form-control">
                                                    <asp:RadioButtonList ID="rbtCheckImg" RepeatColumns="2" runat="server">
                                                        <asp:ListItem Selected="True" Text="No" Value="No" style="padding: 20px"></asp:ListItem>
                                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Photo</label>
                                                <br />
                                                <img src="/assets/images/NoImage.png" style="width: 110px; height: 60px; cursor: pointer;" class="img-thumbnail" id="MaterialImg" />
                                                <input style="visibility: hidden" type="file" name="Img" class="form-control" onchange='sendFile(this);' id="f_UploadImage" />
                                                <asp:HiddenField ID="hidd_imgPath" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Rate and Tax Details</h3>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-2">
                                                        <label class="control-label">Required</label>
                                                        <asp:TextBox ID="txtRquQty" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">Rate</label>
                                                        <asp:TextBox ID="txtRate" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">Rate (Other)</label>
                                                        <asp:TextBox ID="txtRateOther" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">CGST (%)</label>
                                                        <asp:TextBox ID="txtCGST" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">SGST (%)</label>
                                                        <asp:TextBox ID="txtSGST" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-2" style="padding-bottom: 1%">
                                                        <label class="control-label">IGST (%)</label>
                                                        <asp:TextBox ID="txtIGST" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-footer">
                                                <div class="form-group">
                                                    <asp:Button ID="btnSave" class="btn btn-primary" Text="Save" runat="server" OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnBack" class="btn btn-primary" Text="Back" runat="server" OnClick="btnBack_Click" />
                                                    <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnClear_Click" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/Master/BOMMasters.js"></script>
    <script type="text/javascript">
        $('[id*=txtItemName]').on('change', function () {
            var url = "https://translation.googleapis.com/language/translate/v2?key=API_Key";
            url += "&source=" + "DE";
            url += "&target=" + "EN";
            url += "&q=" + escape($('[id*=txtItemName]').val());
            $.get(url, function (data, status) {
                $('[id*=txtItemNameTransulate]').val(data.data.translations[0].translatedText);
            });
        });
    </script>
    <%--<script type="text/javascript">  
         $(document).ready(function () {
             
             //$('[id*=txtMatNo]').on("change", function () {
             window.setInterval(function () {
                 //alert($(this).val());
                 $.ajax({
                     type: "POST",
                     url: "MstAssetItemSub.aspx/GetCTNo",
                    // data: '{id: "' + $(this).val() + '" }',
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (response) {
                          // console.log(response);
                         if (response.d != "false") {
                             //     alert("Called");
                             $('[id*=txtItemCode]').val(response.d);
                            // ShowMessageErr   or('The Material Number Already Taken');
                            // $(this).focus();
                         } else {

                         }
                     },
                     error: function (XMLHttpRequest, textStatus, errorThrown) {
                         alert("Whoops something went wrong!" + errorThrown);
                     }
                 });
             },1000);
         });
        </script> --%>


    <%--<script src="/assets/js/GoogleTransulate.js"></script>--%>
</asp:Content>
