﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstUOM.aspx.cs" Inherits="MstUOM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Unit of Measurement Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> UOM Code <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtUOMCode" runat="server" class="form-control"/>
                                    <span id="DeptCode" class="text-danger"></span>
                                    <asp:RequiredFieldValidator ControlToValidate="txtUOMCode" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> UOM Short Name <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtShortName" runat="server" class="form-control" />
                                    <span id="ShortName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtShortName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> UOM Full Name <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtFullName" runat="server" class="form-control" />
                                    <span id="FullName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtFullName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-2">
                                <div class="form-group" style="align-content:center">
                                    <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                </div>
                            </div>
                            <div class="col-md-5"></div>
                        </div>
                            
                        <div class="box-body no-padding">
                            <div class="table-responsive mailbox-messages">
                                <asp:Repeater ID="GrdDepartment" runat="server">
			                        <HeaderTemplate>
                                        <table  class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>S. No</th>
                                                    <th>UOM Code</th>
                                                    <th>UOM Short Name</th>
                                                    <th>UOM Full Name</th>
                                                    <th>Mode</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Container.ItemIndex + 1 %></td>
                                            <td><%# Eval("UOMCode")%></td>
                                            <td><%# Eval("UOMShortName")%></td>
                                            <td><%# Eval("UOMFullName")%></td>
                                            <td>
                                                <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                    Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("UOMCode")%>'>
                                                </asp:LinkButton>

                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                    Text="" CommandArgument="Delete" OnCommand="GridDeleteClick" CommandName='<%# Eval("UOMCode")%>' 
                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>                                
			                    </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

