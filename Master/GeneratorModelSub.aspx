﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GeneratorModelSub.aspx.cs" Inherits="GeneratorModelSub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <asp:UpdatePanel ID="upGenModelsub" runat="server">
        <ContentTemplate>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-sun-o text-primary"></i> Generator Model
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Model no. <span class="text-danger">*</span></label>
                                    <asp:TextBox ID="txtModelNo" class="form-control" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtModelNo" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Model Name</label>
                                    <asp:TextBox ID="txtModelName" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Description</label>
                                    <asp:TextBox ID="txtDescription" Rows="3" cols="30" Style="resize: none" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Active</label>
                                    <asp:CheckBox ID="chkActive" class="form-control" BorderStyle="None" Checked="true" runat="server"></asp:CheckBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" ValidationGroup="Validate_Field" OnClick="btnSave_Click" runat="server" Text="Save" />
                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" hidden>
                <div class="callout callout-info" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-info"></i>Info:</h4>
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    <!-- /.content -->
        </div>

     <!-- iCheck -->
    <script src="assets/adminlte/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('.checkbox').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
</asp:Content>

