﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Master_MstProStep_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionWasteMatCode = "";
    string SessionFinYearVal;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Production Step Master";

            if (Session["productStepID"] != null)
            {
                btnSearch_Click(sender, e);
            }   
        }
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstProductionStep where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        SSQL = SSQL + " and [Production_StepID]='" + Session["productStepID"] + "'";
        DataTable dt_Get = new DataTable();
        dt_Get = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_Get.Rows.Count > 0)
        {
            txtCode.Text = dt_Get.Rows[0]["Production_StepNo"].ToString();
            txtProductionStep.Text = dt_Get.Rows[0]["Production_StepName"].ToString();
            btnSave.Text = "Update";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtProductionStep.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Production Step')", true);
            return;
        }
        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Update MstProductionStep set Production_StepName='" + txtProductionStep.Text + "'";
                SSQL = SSQL + " where Production_StepNo='" + txtCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                //if()
                SSQL = "";
                SSQL = "insert into MstProductionStep (CCode,Lcode,Production_StepNo,Production_StepName,Remarks,Status,ActiveStatus,CreatedOn,Host_IP,Host_Name,UserName,UserRole)";
                SSQL = SSQL + " values('"+SessionCcode+"','"+SessionLcode+"','"+txtCode.Text+"','"+txtProductionStep.Text+"',)";
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {

    }
}