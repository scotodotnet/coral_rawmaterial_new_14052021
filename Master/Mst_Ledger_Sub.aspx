﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Mst_Ledger_Sub.aspx.cs" Inherits="Mst_AccountType_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upLedgSub" runat="server">
            <ContentTemplate>
                <%--header--%>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Ledger Master</h1>
                </section>

                <%--Body--%>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Ledger Code  </label>
                                                <asp:Label ID="lblLedgerCode" runat="server" class="form-control" />
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Ledger Category</label>
                                                <asp:DropDownList ID="DropDownList1" runat="server" class="form-control select2">
                                                    <asp:ListItem Value="1">Enercon</asp:ListItem>
                                                    <asp:ListItem Value="2">Coral</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supply For </label>
                                                <asp:DropDownList ID="ddlSupplyFor" runat="server" class="form-control select2">
                                                    <asp:ListItem Value="5">All</asp:ListItem>
                                                    <asp:ListItem Value="1">RawMaterial(BOM)</asp:ListItem>
                                                    <asp:ListItem Value="2">Tools</asp:ListItem>
                                                    <asp:ListItem Value="3">Asset</asp:ListItem>
                                                    <asp:ListItem Value="4">General Items</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputName">Ledger Name </label>
                                                <asp:TextBox ID="txtLedgerName" runat="server" class="form-control" />
                                                <span id="ShortName" class="text-danger"></span>

                                                <asp:RequiredFieldValidator ControlToValidate="txtLedgerName" Display="Dynamic"
                                                    ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1"
                                                    runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Ledger Group</label>
                                                <asp:DropDownList ID="ddlLedgerGrp" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlLedgerGrp_SelectedIndexChanged">
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">Supplier</asp:ListItem>
                                                    <asp:ListItem Value="2">Customer</asp:ListItem>
                                                    <asp:ListItem Value="3">Employee</asp:ListItem>
                                                    <asp:ListItem Value="4">General</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Account Type </label>
                                                <asp:DropDownList ID="ddlAccType" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Apartment No/Street </label>
                                                <div class="form-group row" runat="server" visible="false">
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtHouseNumber" runat="server" Class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    </div>
                                                </div>
                                                <asp:TextBox ID="txtAddr1" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Address2 </label>
                                                <asp:TextBox ID="txtAddr2" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">City </label>
                                                <asp:TextBox ID="txtCity" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Pincode </label>
                                                <asp:TextBox ID="txtPincode" runat="server" Class="form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                                    FilterType="Custom,Numbers" TargetControlID="txtPincode" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">State </label>
                                                <asp:TextBox ID="txtState" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Country </label>
                                                <asp:TextBox ID="txtCountry" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Region </label>
                                                <asp:DropDownList ID="ddlReion" runat="server" class="form-control select2">
                                                    <asp:ListItem Value="1">State</asp:ListItem>
                                                    <asp:ListItem Value="2">Country</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">PAN No </label>
                                                <asp:TextBox ID="txtPAN" runat="server" Class="form-control" MaxLength="10"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">TAN No </label>
                                                <asp:TextBox ID="txtTAN" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">GSTN No </label>
                                                <asp:TextBox ID="txtGstin" runat="server" Class="form-control" MaxLength="15"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">IEC </label>
                                                <asp:TextBox ID="txtIEC" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">LEI Code </label>
                                                <asp:TextBox ID="txtLEI" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Bank Name </label>
                                                <asp:DropDownList ID="ddlBankName" runat="server" Class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Branch Name </label>
                                                <asp:TextBox ID="txtBranchName" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">IFSC Code </label>
                                                <asp:TextBox ID="txtIFSC" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Account No </label>
                                                <asp:TextBox ID="txtAccountNo" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">MSME </label>
                                                <asp:RadioButtonList ID="rdbMSME" runat="server" class="form-control"
                                                    RepeatColumns="4">
                                                    <asp:ListItem Value="1" Text="Active" style="padding-right: 15px" Selected="True">Yes</asp:ListItem>
                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                </asp:RadioButtonList>
                                                <asp:CheckBox ID="chkMSME" BorderStyle="None" Visible="false" class="form-control" runat="server" Checke="true" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Notes </label>
                                                <asp:TextBox ID="txtNotes" runat="server" Class="form-control" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Active</label>
                                                <asp:RadioButtonList ID="rdbActive" runat="server" class="form-control"
                                                    RepeatColumns="4">
                                                    <asp:ListItem Value="1" Text="Active" style="padding-right: 15px" Selected="True">Yes</asp:ListItem>
                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary with-border">
                                                <div class="box-header">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Terms And Condition</h3>
                                                </div>

                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Packing Terms / Certificate</span> </label>
                                                                <asp:TextBox ID="txtPackTerms" runat="server" Class="form-control"
                                                                    TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Quality / Installation & Commissioning</label>
                                                                <asp:TextBox ID="txtQuality" runat="server" Class="form-control"
                                                                    TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Warranty</label>
                                                                <asp:TextBox ID="txtWarranty" runat="server" Class="form-control"
                                                                    TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Terms and Conditions</span> </label>
                                                                <asp:TextBox ID="txtTC" runat="server" Class="form-control" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Contact Details</h3>
                                                </div>

                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Name</label>
                                                                <asp:TextBox ID="txtContactName" runat="server" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Mabile No</label>
                                                                <asp:TextBox ID="txtMobile" runat="server" Class="form-control"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                                                    FilterType="Custom,Numbers" TargetControlID="txtMobile" ValidChars="0123456789./">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Mail Id</label>
                                                                <asp:TextBox ID="txtMail" runat="server" Class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2" style="padding-top: 1.75%">
                                                            <div class="form-group">
                                                                <asp:Button ID="btnAddCont" class="btn btn-primary" runat="server" Text="Add" ValidationGroup="Validate_Field" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" runat="server" style="padding-top: 1%">
                                                        <asp:Panel ID="pnlRMSingleRpter" class="panel panel-success" runat="server"
                                                            Height="300px" Style="overflow-x: scroll; overflow-y: scroll" BorderStyle="None">
                                                            <div class="col-md-12">
                                                                <asp:Repeater ID="rptContactDet" runat="server" EnableViewState="false">
                                                                    <HeaderTemplate>
                                                                        <table id="tblContactDet" class="table table-hover table-bordered table-striped">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>Contact_Name</th>
                                                                                    <th>Contact_Mobile No</th>
                                                                                    <th>Contact_Mail</th>
                                                                                    <th>Mode</th>
                                                                                </tr>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.ItemIndex + 1 %></td>

                                                                            <td>
                                                                                <asp:Label ID="lblLdgCode" Style="width: 100%; border: none"
                                                                                    runat="server" Text='<%# Eval("LedgerCode")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:Label ID="lblContName" Style="width: 100%; border: none"
                                                                                    runat="server" Text='<%# Eval("ContactName")%>'>
                                                                                </asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:Label ID="lblContMob" runat="server" Style="width: 100%; border: none"
                                                                                    Text='<%# Eval("ContactMobile")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:Label ID="lblContMail" runat="server" Style="width: 100%; border: none"
                                                                                    Text='<%# Eval("ContactMail")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                    Text="" CommandName='<%# Eval("LedgerCode") +","+ Eval("ContactMobile") %>' OnCommand="GridDelRMClick"
                                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate></table></FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="box-footer">
                                        <div class="form-group" style="align-content: center">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnApproval" class="btn btn-success" runat="server" Text="Approval" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" Visible="false" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <%--<script src="/assets/js/CoreExcel.js"></script>
    <script src="/assets/js/CoreExcel_xls.js"></script>--%>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            console.log("Start");
            $("#tblContactDet").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });
        });
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                    $("#tblContactDet").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "ordering": false,
                        "searching": true
                    });
                }
            });
        };
    </script>

    <style type="text/css">
        .table-loading-overlay {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 10;
            background: #000000;
            opacity: 0.5;
        }

        .table-loading-inner {
            width: 100%;
            height: 100%;
        }
    </style>

</asp:Content>

