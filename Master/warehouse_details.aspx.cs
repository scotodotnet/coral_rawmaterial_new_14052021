﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_warehouse_details : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
       
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL :: Warehouse";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Warehouseddl();
          
        }
        Load_Data();
    }

    private void Load_RackSerious(string Val)
    {
        string SSQL = "";
        SSQL = "Select RackSerious from MstWareHouseRackMain where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + " and WarehouseCode='" + Val + "'";
        ddlRackSerious.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlRackSerious.DataTextField = "RackSerious";
        ddlRackSerious.DataValueField = "RackSerious";
        ddlRackSerious.DataBind();
        ddlRackSerious.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Warehouseddl()
    {
        string SSQL = "";
        DataTable dt_Warehouse = new DataTable();
        SSQL = "Select (WarehouseCode +'~>'+ WarehouseName) as Text,WarehouseCode as value from MstWarehouse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        dt_Warehouse = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlWareHouse.DataSource = dt_Warehouse;
        ddlWareHouse.DataValueField = "value";
        ddlWareHouse.DataTextField = "Text";
        ddlWareHouse.DataBind();
        ddlWareHouse.Items.Insert(0, new ListItem("-Select-","-Select-",true));

        ddlAddWarehouseName.DataSource = dt_Warehouse;
        ddlAddWarehouseName.DataValueField = "value";
        ddlAddWarehouseName.DataTextField = "Text";
        ddlAddWarehouseName.DataBind();
        ddlAddWarehouseName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string _IP = getIPAndName.GetIP();
        string _HostName = getIPAndName.GetName();

        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit...');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
            }
        }
        if (txtWarehouseCode.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please enter the Warehouse Code');", true);
            return;
        }
        if (txtWarehouseName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please enter the Warehouse Name');", true);
            return;
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    try
                    {
                        SaveMode = "Update";
                        query = "Delete from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseCode.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Update Trans_GoodsReceipt_Sub set WarehouseName='" + txtWarehouseName.Text + "'  where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and WarehouseCode='" + txtWarehouseCode.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Update Trans_GoodsReturn_Sub set WarehouseName='" + txtWarehouseName.Text + "'  where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and WarehouseCode='" + txtWarehouseCode.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Update Trans_EmergencyStock_Ledger_All set WarehouseName='" + txtWarehouseName.Text + "'  where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and WarehouseCode='" + txtWarehouseCode.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Update MstWareHouseRackFloor set WarehouseName='" + txtWarehouseName.Text + "'  where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and WarehouseCode='" + txtWarehouseCode.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Update MstWareHouseRackMain set WarehouseName='" + txtWarehouseName.Text + "'  where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and WarehouseCode='" + txtWarehouseCode.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Update MstWareHouseRackSub set WarehouseName='" + txtWarehouseName.Text + "'  where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and WarehouseCode='" + txtWarehouseCode.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Update Trans_Stock_Ledger_All set WarehouseName='" + txtWarehouseName.Text + "'  where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and WarehouseCode='" + txtWarehouseCode.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);

                    }
                    catch(Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
                    }
                }
                else
                {
                    SaveMode = "Error";
                }
            }
            if (SaveMode != "Error")
            {
                try
                {
                    //Insert Compnay Details
                    query = "Insert Into MstWarehouse(Ccode,Lcode,WarehouseCode,WarehouseName,ZoneName,BinName,Description,CreatedOn,Host_IP,Host_Name,UserName,Status)";
                    query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtWarehouseCode.Text + "','" + txtWarehouseName.Text + "',";
                    query = query + "'','','" + txtDescription.Text + "',";
                    query = query + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + SessionUserName + "','Add')";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
                }

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Warehouse Details Saved Successfully...');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Details Saved Successfully');", true);
                }
                else if (SaveMode == "Update")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Warehouse Details Updated Successfully...');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Details Updated Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
                }
                
                Clear_All_Field();
                btnSave.Text = "Save";
                txtWarehouseCode.Enabled = true;
                Load_Data();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Warehouse Details Already Saved...');", true);
            }
        }
        Load_Warehouseddl();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtWarehouseCode.Text = "";
        txtWarehouseName.Text = "";
        //txtZoneName.Text = "";
        //txtBinName.Text = "";
        txtDescription.Text = "";
        txtRackFrom.Text = "";
        txtRackQty.Text = "";
        txtRackTo.Text = "";
        txtTotalRack.Text = "";
        txtTotalRackQty.Text = "";
        btnSave.Text = "Save";
        txtRackSerious.Text = "";
        txtWarehouseCode.Enabled = true;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseCode.Text + "' ";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                txtWarehouseName.Text = DT.Rows[0]["WarehouseName"].ToString();
                //txtZoneName.Text = DT.Rows[0]["ZoneName"].ToString();
                //txtBinName.Text = DT.Rows[0]["BinName"].ToString();
                txtDescription.Text = DT.Rows[0]["Description"].ToString();
                txtWarehouseCode.Enabled = false;
                btnSave.Text = "Update";
            }
            else
            {
                Clear_All_Field();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
        }
    }
    private void Load_Data()
    {
        try
        {
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status!='Delete'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            Repeater1.DataSource = DT;
            Repeater1.DataBind();
        }catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
        }
        Load_Rack();
        Load_AddRack();
    }

    private void Load_AddRack()
    {
        try
        {
            string SSQL = "";
            SSQL = "Select * from MstWareHouseRackFloor where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
            Repeater3.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            Repeater3.DataBind();

        }catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
        }
    }

    private void Load_Rack()
    {
        try
        {
            string query = "";
            query = "Select * from MstWareHouseRackMain where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
            Repeater2.DataSource = objdata.RptEmployeeMultipleDetails(query);
            Repeater2.DataBind();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
        }
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtWarehouseCode.Text = e.CommandName.ToString();
        //txtBinName.Text = e.CommandArgument.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Warehouse..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            //DataTable dtdPOreceipt = new DataTable();
            //DataTable dtdORreceipt = new DataTable();
            //DataTable dtdUPreceipt = new DataTable();
            DataTable dtdStock = new DataTable();
            DataTable dtdReservStock = new DataTable();

            //query = "select WarehouseCode,WarehouseName from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            //dtdPOreceipt = objdata.RptEmployeeMultipleDetails(query);

            //query = "select WarehouseCode,WarehouseName from Opening_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            //dtdORreceipt = objdata.RptEmployeeMultipleDetails(query);

            //query = "select WarehouseCode,WarehouseName from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            //dtdUPreceipt = objdata.RptEmployeeMultipleDetails(query);

            //query = "select WarehouseCode,WarehouseName from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            //dtdCSreceipt = objdata.RptEmployeeMultipleDetails(query);
            try
            {
                query = "Select isnull(WarehouseCode,'0') as code,sum(Add_Qty)-Sum(Minus_Qty) qty from Trans_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
                query = query + " group by WarehouseCode having (sum(Add_Qty)-Sum(Minus_Qty))>0";
                dtdStock = objdata.RptEmployeeMultipleDetails(query);
                if (dtdStock.Rows.Count > 0)
                {
                    if (Convert.ToDecimal(dtdStock.Rows[0]["qty"].ToString()) > 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry! The WareHouse is arrived some Quantity of Stock');", true);
                        return;
                    }
                }

                query = "Select isnull(WarehouseCode,'0') as code,sum(Add_Qty)-Sum(Minus_Qty) qty from Trans_EmergencyStock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
                query = query + " group by WarehouseCode having (sum(Add_Qty)-Sum(Minus_Qty))>0";
                dtdReservStock = objdata.RptEmployeeMultipleDetails(query);
                if (dtdStock.Rows.Count > 0)
                {
                    if (Convert.ToDecimal(dtdStock.Rows[0]["qty"].ToString()) > 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry! The WareHouse is arrived some quantity of stock');", true);
                        return;
                    }
                }
                
                if (!ErrFlag)
                {
                    DataTable DT = new DataTable();
                    query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + e.CommandName.ToString() + "' and BinName='" + e.CommandArgument.ToString() + "'";
                    DT = objdata.RptEmployeeMultipleDetails(query);
                    if (DT.Rows.Count != 0)
                    {
                        query = "Delete from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + e.CommandName.ToString() + "' and BinName='" + e.CommandArgument.ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Details Deleted Successfully');", true);
                        Load_Data();
                        Clear_All_Field();
                    }
                }
                else
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
                }
            }catch(Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
            }
        }
    }
    
    protected void btnRackSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string _IP = getIPAndName.GetIP();
        string _HostName = getIPAndName.GetName();

        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit...');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
            }
        }
        //User Rights Check End
        try
        {
            if (!ErrFlag)
            {
                query = "Select * from MstWareHouseRackMain where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + ddlWareHouse.SelectedValue + "' and RackSerious='" + txtRackSerious.Text + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    if (btnRackSave.Text == "Update")
                    {
                        SaveMode = "Update";
                        query = "Delete from MstWareHouseRackMain where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + ddlWareHouse.SelectedValue + "' and RackSerious='" + txtRackSerious.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Delete from MstWareHouseRackSub  where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + ddlWareHouse.SelectedValue + "' and RackSerious='" + txtRackSerious.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                    else
                    {
                        SaveMode = "Error";
                    }
                }
                if (SaveMode != "Error")
                {
                    //Insert Compnay Details
                    query = "Insert Into MstWareHouseRackMain(Ccode,Lcode,WarehouseCode,WarehouseName,Description,TotalRack,RackQty,FromRack,ToRack,TotalRackQty,RackSerious,CreatedOn,Host_IP,Host_Name,UserName,Status)";
                    query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWareHouse.SelectedItem.Value + "','" + ddlWareHouse.SelectedItem.Text.Split('>').LastOrDefault() + "',";
                    query = query + "'" + txtRackDescription.Text + "','" + txtTotalRack.Text + "','" + txtRackQty.Text + "','" + txtRackFrom.Text + "','" + txtRackTo.Text + "','" + txtTotalRackQty.Text + "','" + txtRackSerious.Text + "',";
                    query = query + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + SessionUserName + "','Add')";
                    objdata.RptEmployeeMultipleDetails(query);
                    
                    if (Convert.ToDecimal(txtTotalRack.Text) > 0)
                    {
                        for (int i = 0; i < Convert.ToDecimal(txtTotalRack.Text); i++)
                        {
                            query = "";
                            query = "Delete from MstWareHouseRackSub where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WarehouseCode='" + ddlWareHouse.SelectedValue + "' and RackSerious='" + txtRackSerious.TemplateSourceDirectory + "'";
                            query = query + " and RackName='" + txtRackSerious.Text + "_" + i + "'";
                            objdata.RptEmployeeMultipleDetails(query);

                            query = "";
                            query = "Insert Into MstWareHouseRackSub(Ccode,Lcode,WarehouseCode,WarehouseName,RackSerious,RackName,RackQty,RackQtyUse,RackQtyBalance,CreatedOn,Host_IP,Host_Name,UserName,Status)";
                            query = query + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWareHouse.SelectedValue + "','" + ddlWareHouse.SelectedItem.Text.Split('>').LastOrDefault() + "',";
                            query = query + "'" + txtRackSerious.Text + "','" + txtRackSerious.Text + "_" + i + "','" + txtRackQty.Text + "','0','" + txtRackQty.Text + "',";
                            query = query + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + SessionUserName + "','Add')";
                            objdata.RptEmployeeMultipleDetails(query);
                        }


                    }
                    if (SaveMode == "Insert")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Warehouse Rack Details Saved Successfully');", true);
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Rack Details Saved Successfully');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Warehouse Rack Details Updated Successfully');", true);
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Rack Details Updated Successfully');", true);
                    }


                    Clear_All_Field_Rack();
                    Load_Data();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Warehouse Rack Details Already Saved');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Rack Details Already Saved');", true);
                }
            }
        }catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
        }
    }

    private void Clear_All_Field_Rack()
    {
        btnRackSave.Text = "Save";
        ddlWareHouse.ClearSelection();
        txtRackFrom.Text = "0";
        txtRackTo.Text = "0";
        txtTotalRack.Text = "0";
        txtRackQty.Text = "0";
        txtTotalRackQty.Text = "0";
        txtRackSerious.Text = "";
        txtRackDescription.Text = "";
        ddlWareHouse.Enabled = true;
        txtRackSerious.ReadOnly = false;
    }

    protected void btnRackClean_Click(object sender, EventArgs e)
    {
        Clear_All_Field_Rack();
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        try
        {
            string SSQL = "";
            SSQL = "Select * from MstWareHouseRackMain where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            SSQL = SSQL + " and RackSerious='" + e.CommandArgument.ToString() + "'";
            DataTable dt_RackEdit = new DataTable();
            dt_RackEdit = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_RackEdit.Rows.Count > 0)
            {
                ddlWareHouse.Enabled = false;
                txtRackSerious.ReadOnly = true;
                ddlWareHouse.SelectedValue = dt_RackEdit.Rows[0]["WarehouseCode"].ToString();
                txtRackFrom.Text = dt_RackEdit.Rows[0]["FromRack"].ToString();
                txtRackTo.Text = dt_RackEdit.Rows[0]["ToRack"].ToString();
                txtTotalRack.Text = dt_RackEdit.Rows[0]["TotalRack"].ToString();
                txtRackQty.Text = dt_RackEdit.Rows[0]["RackQty"].ToString();
                txtTotalRackQty.Text = dt_RackEdit.Rows[0]["TotalRackQty"].ToString();
                txtRackSerious.Text = dt_RackEdit.Rows[0]["RackSerious"].ToString();
                txtRackDescription.Text = dt_RackEdit.Rows[0]["Description"].ToString();
                btnRackSave.Text = "Update";
                Load_Rack();
            }
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
        }
    }
    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
        }
        if (!ErrFlag)
        {
            try
            {
                SSQL = "Select sum(RackQtyUse) as Qty from MstWareHouseRackSub where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Warehousecode='" + e.CommandName.ToString() + "'";
                SSQL = SSQL + " and RackSerious='" + e.CommandArgument.ToString() + "' group by Rackserious having (sum(RackQtyUse))>0";
                DataTable dt_rackCheck = new DataTable();
                dt_rackCheck = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_rackCheck.Rows.Count > 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('The Rack contains Item you could not delete the Rack');", true);
                    return;
                }
                if (!ErrFlag)
                {
                    SSQL = "Update MstWareHouseRackMain set Status='Delete' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " and Warehousecode='" + e.CommandName + "' and RackSerious='" + e.CommandArgument + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Warehouse Rack Details Deleted Successfully');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something Went Wrong! Please Try again Later');", true);
            }
        }

    }
    protected void txtRackFrom_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtRackFrom.Text) > 0)
        {
            if (Convert.ToDecimal(txtRackTo.Text) > 0)
            {
                if (Convert.ToDecimal(txtRackFrom.Text) > Convert.ToDecimal(txtRackTo.Text))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('From Rack  is Greater then To Rack');", true);
                }
                else
                {
                    _totalRackCalc();
                }
            }
        }
    }

    private void _totalRackCalc()
    {
        txtTotalRack.Text = (Convert.ToDecimal(txtRackTo.Text) - Convert.ToDecimal(txtRackFrom.Text) + 1).ToString();
    }

    protected void txtRackTo_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtRackTo.Text) > -1)
        {
            if (Convert.ToDecimal(txtRackTo.Text) < Convert.ToDecimal(txtRackFrom.Text))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('To Rack Qty is Lesser then From Rack');", true);
            }
            else
            {
                _totalRackCalc();
            }
        }
    }

    protected void txtRackQty_TextChanged(object sender, EventArgs e)
    {
        if (txtRackQty.Text != "")
        {
            _totalQtyCalc();
        }
    }

    private void _totalQtyCalc()
    {
        txtTotalRackQty.Text = (Convert.ToDecimal(txtTotalRack.Text) * Convert.ToDecimal(txtRackQty.Text)).ToString();
    }

    protected void btnAddRackSave_Click(object sender, EventArgs e)
    {
        bool Rights_Check = false;
        string SSQL = "";
        bool ErrFlg = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string _IP = getIPAndName.GetIP();
        string _HostName = getIPAndName.GetName();

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");

        if (Rights_Check == false)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
        }
      
        if (ddlAddWarehouseName.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select the Warehouse Name');", true);
        }
        if (ddlRackSerious.DataSource!=null && ddlRackSerious.SelectedItem.Text=="-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select the Rack Serious Name');", true);
        }
        if (txtAddRackName.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Enter the Rack Name');", true);
        }
        if (txtAddRackQty.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Enter the Rack Quantity');", true);
        }
        try
        {
            if (!ErrFlg)
            {
                SSQL = "";
                SSQL = "Select * from MstWareHouseRackFloor where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WarehouseCode='" + ddlAddWarehouseName.SelectedValue + "'";
                SSQL = SSQL + " and RackSerious='" + ddlRackSerious.SelectedItem.Text + "' and RackName='" + txtAddRackName.Text + "' and ItemLocation='" + txtRackLocation.Text + "'";
                DataTable dt_check = new DataTable();
                dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_check.Rows.Count > 0)
                {
                    if (btnAddRackSave.Text == "Update")
                    {
                        SSQL = "";
                        SSQL = "Select RackQtyUse from MstWareHouseRackFloor where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WarehouseCode='" + ddlAddWarehouseName.SelectedValue + "'";
                        SSQL = SSQL + " and RackSerious='" + ddlRackSerious.SelectedItem.Text + "' and RackQtyUse > '0'";
                        DataTable dt_RackQty = new DataTable();
                        dt_RackQty = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_RackQty.Rows.Count > 0)
                        {
                            if (Convert.ToDecimal(dt_RackQty.Rows[0]["RackQtyUse"].ToString()) > 0 && Convert.ToDecimal(dt_RackQty.Rows[0]["RackQtyUse"].ToString()) > Convert.ToDecimal(txtAddRackQty.Text))
                            {
                                return;
                                ErrFlg = true;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Sorry! The item quantity on the Rack is Higher then " + txtAddRackQty.Text + "');", true);
                            }
                        }
                        if (!ErrFlg)
                        {
                            SSQL = "Update MstWareHouseRackFloor set ItemLocation='" + txtRackLocation.Text + "',RackName='" + txtAddRackName.Text + "',RackQty='" + txtAddRackQty.Text + "',RackQtyBalance='"+txtAddRackQty.Text+ "'-RackQtyUse";
                            SSQL = SSQL + ",Description='" + txtAddRackDescription.Text + "' where WarehouseCode='" + ddlAddWarehouseName.SelectedValue + "' and RackSerious='" + ddlRackSerious.SelectedItem.Text + "'";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Additional Rack Details Updated Successfully');", true);
                            Load_AddRack();
                            btnAddRackClr_Click(sender, e);
                        }
                    }
                    else if (btnAddRackSave.Text == "Save")
                    {
                        //SSQL = "";
                        //SSQL = "Insert into MstWareHouseRackFloor (Ccode,Lcode,WarehouseCode,WarehouseName,RackSerious,RackName,RackQty,RackQtyUse,RackQtyBalance,Description,CreatedOn,Host_IP,Host_Name,Username,ItemLocation,Status)";
                        //SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlAddWarehouseName.SelectedValue + "','" + ddlAddWarehouseName.SelectedItem.Text.ToString().Split('>').Last() + "','" + ddlRackSerious.SelectedItem.Text + "',";
                        //SSQL = SSQL + "'" + txtAddRackName.Text + "','" + txtAddRackQty.Text + "','0','" + txtAddRackQty.Text + "','" + txtAddRackDescription.Text + "','" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "',";
                        //SSQL = SSQL + "'" + _HostName + "','" + SessionUserName + "','" + txtRackLocation.Text + "','Add')";
                        //objdata.RptEmployeeMultipleDetails(SSQL);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Additional Rack Details Added Successfully');", true);
                        //Load_AddRack();
                        //btnAddRackClr_Click(sender, e);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Additional Rack Details Already Added');", true);
                        ErrFlg = true;
                        return;
                    }
                }
                else
                {
                    SSQL = "";
                    SSQL = "Insert into MstWareHouseRackFloor (Ccode,Lcode,WarehouseCode,WarehouseName,RackSerious,RackName,RackQty,RackQtyUse,RackQtyBalance,Description,CreatedOn,Host_IP,Host_Name,Username,ItemLocation,Status)";
                    SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlAddWarehouseName.SelectedValue + "','" + ddlAddWarehouseName.SelectedItem.Text.ToString().Split('>').Last() + "','" + ddlRackSerious.SelectedItem.Text + "',";
                    SSQL = SSQL + "'" + txtAddRackName.Text + "','" + txtAddRackQty.Text + "','0','" + txtAddRackQty.Text + "','" + txtAddRackDescription.Text + "','" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "',";
                    SSQL = SSQL + "'" + _HostName + "','" + SessionUserName + "','" + txtRackLocation.Text + "','Add')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Additional Rack Details Added Successfully');", true);
                    Load_AddRack();
                    btnAddRackClr_Click(sender, e);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
        }
    }

    protected void btnAddRackClr_Click(object sender, EventArgs e)
    {
        ddlAddWarehouseName.ClearSelection();
        ddlAddWarehouseName_SelectedIndexChanged(sender, e);
        txtRackLocation.Text = string.Empty;
        txtAddRackName.Text = string.Empty;
        txtAddRackQty.Text = "0";
        txtAddRackDescription.Text = string.Empty;
        btnAddRackSave.Text = "Save";
        ddlAddWarehouseName.Enabled = true;
        ddlRackSerious.Enabled = true;
    }

    protected void btnAddRackEditGrid_Command(object sender, CommandEventArgs e)
    {
        bool Rights_Check = false;
        string SSQL = "";
        bool ErrFlg = false;
      
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");
            if (Rights_Check == false)
            {
                ErrFlg = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit...');", true);
            }
        if (!ErrFlg)
        {
            string[] _warehouse = e.CommandName.ToString().Split(',');
            string[] _Rack = e.CommandArgument.ToString().Split(',');

            string warehousecode = _warehouse[0].ToString();
            string itemlocation = _warehouse[1].ToString();

            string rackserious = _Rack[0].ToString();
            string rackname = _Rack[1].ToString();
            try
            {
                SSQL = "";
                SSQL = "Select * from MstWareHouseRackFloor where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WarehouseCode='" + warehousecode + "' and ItemLocation='" + itemlocation + "'";
                SSQL = SSQL + " and RackSerious='" + rackserious + "' and RackName='" + rackname + "'";
                DataTable dt_get = new DataTable();
                dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_get.Rows.Count > 0)
                {
                    ddlAddWarehouseName.SelectedValue = dt_get.Rows[0]["WarehouseCode"].ToString();
                    ddlAddWarehouseName_SelectedIndexChanged(sender, e);
                    ddlAddWarehouseName.Enabled = false;
                    ddlRackSerious.SelectedValue = dt_get.Rows[0]["RackSerious"].ToString();
                    ddlRackSerious.Enabled = false;
                    txtRackLocation.Text = dt_get.Rows[0]["ItemLocation"].ToString();
                    txtAddRackName.Text = dt_get.Rows[0]["RackName"].ToString();
                    txtAddRackQty.Text = dt_get.Rows[0]["RackQty"].ToString();
                    txtAddRackDescription.Text = dt_get.Rows[0]["Description"].ToString();
                    btnAddRackSave.Text = "Update";
                    Load_AddRack();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
            }
        }
    }

    protected void btnAddRackDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        bool Rights_Check = false;
        string SSQL = "";
        bool ErrFlg = false;

        try
        {
            Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "WareHouse Master");
            if (Rights_Check == false)
            {
                ErrFlg = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete...');", true);
            }
            string[] _warehouse = e.CommandName.ToString().Split(',');
            string[] _Rack = e.CommandArgument.ToString().Split(',');

            string warehousecode = _warehouse[0].ToString();
            string itemlocation = _warehouse[1].ToString();

            string rackserious = _Rack[0].ToString();
            string rackname = _Rack[1].ToString();
            if (!ErrFlg)
            {
                SSQL = "";
                SSQL = "Update MstWareHouseRackFloor set Status='Delete' where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WarehouseCode='" + warehousecode + "'";
                SSQL = SSQL + " and ItemLocation='" + itemlocation + "' and RackSerious='" + rackserious + "' and rackname='" + rackname + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Additional Rack Details Deleted Successfully');", true);
                Load_AddRack();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Something went Wrong. Please Try Again');", true);
        }
    }

    protected void ddlAddWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_RackSerious(ddlAddWarehouseName.SelectedItem.Value);
    }
}
