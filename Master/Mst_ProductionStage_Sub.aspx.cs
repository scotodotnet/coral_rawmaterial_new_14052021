﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mst_ProductionStage_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionFinYearVal;
    string SessionUserName;

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();

            if (!IsPostBack)
            {
                Load_Generator();

                if (Session["ProductNo"] != null)
                {
                    lblStageCode.Text = Session["ProductNo"].ToString();
                    Search();
                }
            }
        }
    }

    private void Search()
    {
        try
        {
            lblErrorMsg.Text = "";

            SSQL = "Select * from ProductionStage where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
            SSQL = SSQL + " StageCode ='" + lblStageCode.Text + "' ";

            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt.Rows.Count > 0)
            {
                ddlGenModelName.SelectedValue = dt.Rows[0]["GenModelCode"].ToString();

                if (dt.Rows[0]["ProductPartName"].ToString() == "Stator")
                {
                    ddltProductName.SelectedValue = "1";
                }
                else
                {
                    ddltProductName.SelectedValue = "2";
                }

                if (dt.Rows[0]["ProductPartName"].ToString() == "Stator")
                {
                    ddlAssemblyStage.SelectedValue = "1";
                }
                else
                {
                    ddlAssemblyStage.SelectedValue = "2";
                }

                txtProductionStage.Text = dt.Rows[0]["StageName"].ToString();

                txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();


                btnSave.Text = "Update";
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Load_Generator()
    {
        try
        {
            lblErrorMsg.Text = "";
            SSQL = "";

            SSQL = "Select GenModelNo,GenModelName from [CORAL_ERP_Sales]..GeneratorModels where Status!='Delete' and Active='1' and";
            SSQL = SSQL + " Ccode ='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";

            ddlGenModelName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlGenModelName.DataTextField = "GenModelName";
            ddlGenModelName.DataValueField = "GenModelNo";
            ddlGenModelName.DataBind();
            ddlGenModelName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            
            lblErrorMsg.Text = "";

            GetIPAndName getIPAndName = new GetIPAndName();
            string SysIp = getIPAndName.GetIP();
            string SysName = getIPAndName.GetName();

            if (ddlGenModelName.SelectedItem.Text == "-Select-")
            {
                ErrFlg = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select Generator Model...');", true);
            }

            if (ddltProductName.SelectedItem.Text == "-Select-")
            {
                ErrFlg = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select Production Part...');", true);
            }

            if (txtProductionStage.Text == "")
            {
                ErrFlg = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Enter Production Stage Name...');", true);
            }

            if (!ErrFlg)
            {
                if (btnSave.Text != "Update")
                {
                    if (!ErrFlg)
                    {
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Production Stage", SessionFinYearVal, "1");

                        if (Auto_Transaction_No == "")
                        {
                            ErrFlg = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                        }
                        else
                        {
                            lblStageCode.Text = Auto_Transaction_No;
                        }
                    }
                }

                //Dublicate Name Checking
                if (btnSave.Text == "Save")
                {
                    SSQL = "";
                    SSQL = "Select * from ProductionStage where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " StageName='" + txtProductionStage.Text + "' and GenModelName='" + ddlGenModelName.SelectedItem.Text + "' ";
                    SSQL = SSQL + " And ProductPartName='" + ddltProductName.SelectedItem.Text + "'";

                    DataTable dt = new DataTable();
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Production Stage Already Exist')", true); ;
                        ErrFlg = true;
                    }
                }

                if (!ErrFlg)
                {
                    if (btnSave.Text == "Update")
                    {
                        SSQL = "";
                        SSQL = "Update ProductionStage set ";
                        SSQL = SSQL + " GenModelCode ='" + ddlGenModelName.SelectedValue + "',GenModelName ='" + ddlGenModelName.SelectedItem.Text + "',";
                        SSQL = SSQL + " ProductPartName='" + ddltProductName.SelectedItem.Text + "',AssemblyName='" + ddlAssemblyStage.SelectedItem.Text + "',";
                        SSQL = SSQL + " StageName='" + txtProductionStage.Text + "',Remarks='" + txtRemarks.Text + "',UpdateOn=Convert(Datetime,GetDate(),103)";
                        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And StageCode='" + lblStageCode.Text + "'";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    else
                    {
                       

                        SSQL = "Insert into ProductionStage(Ccode,Lcode,StageCode,GenModelCode,GenModelName,ProductPartName,AssemblyName,";
                        SSQL = SSQL + " StageName,Remarks,Status,Active,SysIP,SysName,UserName,UserId,CreatedOn)Values('" + SessionCcode + "',";
                        SSQL = SSQL + " '" + SessionLcode + "','" + lblStageCode.Text + "','" + ddlGenModelName.SelectedValue + "',";
                        SSQL = SSQL + " '" + ddlGenModelName.SelectedItem.Text + "','" + ddltProductName.SelectedItem.Text + "',";
                        SSQL = SSQL + " '" + ddlAssemblyStage.SelectedItem.Text + "','" + txtProductionStage.Text + "','" + txtRemarks.Text + "',";
                        SSQL = SSQL + " 'Add','1','" + SysIp + "','" + SysName + "','" + SessionUserName + "','" + SessionUserID + "',";
                        SSQL = SSQL + " Convert(Datetime,GetDate(),103))";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    if (btnSave.Text != "Update")
                    {

                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master_Get(SessionCcode, SessionLcode, "Production Stage", "1");
                    }

                    btnClear_Click(sender, e);
                    Response.Redirect("/Master/Mst_ProductionStage_Main.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlGenModelName.ClearSelection();
        txtRemarks.Text = "";

        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("ProductNo");
        Response.Redirect("/Master/Mst_ProductionStage_Main.aspx");
    }

    protected void ddlGenModelName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();

        //SSQL = "Select PartsName from [CORAL_ERP_Sales]..PartsType where GenModelName='" + ddlGenModelName.SelectedItem.Text + "' ";
        //SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";

        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //ddltProductName.DataSource = DT;
        //ddltProductName.DataTextField = "PartsName";
        //ddltProductName.DataValueField = "PartsName";
        //ddltProductName.DataBind();
        //ddltProductName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
}