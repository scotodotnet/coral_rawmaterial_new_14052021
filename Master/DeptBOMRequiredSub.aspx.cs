﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DeptBOMRequiredSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;

    string SessionCcode = "";
    string SessionLcode = "";
    string SessionUserID;
    string SessionUserName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();

            if (!IsPostBack)
            {
                Load_Department();
                Load_BOM();
                if ((string)Session["Mat_No"] != null)
                {
                    Search(Session["Mat_No"].ToString());
                }
            }
        }
    }

    private void Load_BOM()
    {
        SSQL = "";
        SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlMaterials.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMaterials.DataTextField = "Raw_Mat_Name";
        ddlMaterials.DataValueField = "Mat_No";
        ddlMaterials.DataBind();
        ddlMaterials.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Department()
    {
        SSQL = "";
        SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlDepartment.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDepartment.DataTextField = "deptname";
        ddlDepartment.DataValueField = "deptCode";
        ddlDepartment.DataBind();
        ddlDepartment.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Search(string id)
    {
        SSQL = "";
        SSQL = "Select * from Dept_BOM_Require where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Mat_No='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlDepartment.SelectedValue = dt.Rows[0]["deptCode"].ToString();
            ddlMaterials.SelectedValue = dt.Rows[0]["Mat_No"].ToString();
            txtRequiredQty.Text = dt.Rows[0]["Require_Qty"].ToString();
            
            btnSave.Text = "Update";
        }
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool Rights_Check = false;

        if (ddlMaterials.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Choose the Material')", true); ;
            ErrFlg = true;
        }


        if (ddlDepartment.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Choose the Department')", true); ;
            ErrFlg = true;
        }

        //User  Rights
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Department Required BOM");

        if (Rights_Check == false)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add New Department Required...');", true);
        }

        if (!ErrFlg)
        {

            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from Dept_BOM_Require where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Mat_No='" + ddlMaterials.SelectedValue + "' and DeptCode='" + ddlDepartment.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from Dept_BOM_Require where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Mat_No='" + ddlMaterials.SelectedValue + "' and DeptCode='" + ddlDepartment.SelectedValue + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Material already present this department')", true); ;
                
                    ErrFlg = true;
                    return;
                }
            }
            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "";
                SSQL = "Insert into Dept_BOM_Require(Ccode,Lcode,Mat_No,Raw_Mat_Name,Require_Qty,DeptCode,DeptName,CreatedOn,Host_IP,Host_Name,UserName,Status)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "' ,'" + ddlMaterials.SelectedValue + "','" + ddlMaterials.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + txtRequiredQty.Text + "','" + ddlDepartment.SelectedValue + "','" + ddlDepartment.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "',";
                SSQL = SSQL + " '" + Session["UserID"] + "','Add')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                Clear_All_Field();
                btnSave.Text = "Save";

                Response.Redirect("DeptBOMRequiredMain.aspx");

            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        ddlDepartment.ClearSelection();
        ddlMaterials.ClearSelection();
        txtRequiredQty.Text = "0.00";
        btnSave.Text = "Save";
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("id");
        Response.Redirect("/Master/DeptBOMRequiredMain.aspx");
    }
}