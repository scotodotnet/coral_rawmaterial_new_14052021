﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Mst_ProductionStep_Sub.aspx.cs" Inherits="Master_MstProductionStep_Sub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Assembly</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Trans.Code</label>
                                                <asp:Label runat="server" ID="txtTransCode" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Trans.Date</label>
                                                <asp:TextBox runat="server" ID="txtTransDate" class="form-control datepicker">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtTransDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Production Step Name</label>
                                                <asp:DropDownList runat="server" ID="txtAssemblyStageName" class="form-control select2">
                                                    <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAssemblyStageName" InitialValue="-Select-" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3" >
                                            <div class="form-group">
                                                <label>Production Step Name</label>
                                                <asp:TextBox runat="server" ID="txtProductionStep" class="form-control "></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtProductionStep" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <asp:TextBox runat="server" ID="txtRemarks" TextMode="MultiLine" Style="resize: none" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Status</label>
                                            <asp:RadioButtonList ID="RblStatus" class="form-control" runat="server"
                                                RepeatColumns="4">
                                                <asp:ListItem Value="1" Text="Active" style="padding-right: 15px" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="IN Active"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                   
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

