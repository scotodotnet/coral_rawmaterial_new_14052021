﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class GeneratorModelSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionFinYearVal;

    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionDesigCode = "";

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    bool Rights_Check = false;
    protected void Page_Load(object sender, EventArgs e)
    {

        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();

            if (!IsPostBack)
            {
                if ((string)Session["GenID"] != null)
                {
                    Search(Session["GenID"].ToString());
                }
            }
        }
    }
    private void Search(string id)
    {
        SSQL = "";
        SSQL = "Select * from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and id='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtModelNo.Enabled = false;
            txtModelNo.Text = dt.Rows[0]["GenModelNo"].ToString();
            txtModelName.Text = dt.Rows[0]["GenModelName"].ToString();
            txtDescription.Text = dt.Rows[0]["Description"].ToString();

            if (dt.Rows[0]["Active"].ToString() == "True")
            {
                chkActive.Checked = true;
            }
            else
            {
                chkActive.Checked = false;
            }

            btnSave.Text = "Update";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ChkStatus = "";

        if(chkActive.Checked==true)
        {
            ChkStatus = "1";
        }
        else
        {
            ChkStatus = "0";
        }

        //if (txtModelNo.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Generator Model Number')", true); ;
        //    ErrFlg = true;
        //}

        if (txtModelName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Generator Model Name')", true); ;
            ErrFlg = true;
        }


        //User  Rights
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Generator Model Master");

        if (Rights_Check == false)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add New...');", true);
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlg)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Generator Model Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtModelNo.Text = Auto_Transaction_No;
                }
            }
        }
        DataTable DT = new DataTable();

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from GeneratorModels Where GenModelName='" + txtModelName.Text + "' And ";
            SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlg = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Model Already Exsit');", true);
            }
        }

        if (!ErrFlg)
        {

            if (btnSave.Text == "Update")
            {
                SSQL = "Update GeneratorModels set GenModelName='" + txtModelName.Text + "',Description='" + txtDescription.Text + "',";
                SSQL = SSQL + " Active='" + ChkStatus + "' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                SSQL = SSQL + " GenModelNo ='" + txtModelNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "";
                SSQL = "Insert into GeneratorModels(Ccode,Lcode,GenModelNo,GenModelName,Description,CreatedOn,Host_IP,Host_Name,UserName,Status,Active)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "' ,'" + txtModelNo.Text + "','" + txtModelName.Text + "',";
                SSQL = SSQL + " '" + txtDescription.Text + "','" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "',";
                SSQL = SSQL + "'" + _IP + "','" + _HostName + "','" + Session["UserID"] + "','ADD','" + ChkStatus + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                
            }
            if (!ErrFlg)
            {
                btnClear_Click(sender, e);
                Response.Redirect("GeneratorModelMain.aspx");
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDescription.Text = "";
        txtModelName.Text = "";
        txtModelNo.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("id");
        Response.Redirect("/Master/GeneratorModelMain.aspx");
    }
}