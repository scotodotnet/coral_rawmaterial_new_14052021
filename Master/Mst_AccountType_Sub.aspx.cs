﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Mst_AccountType_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionFinYearVal;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionAccTypeNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Account Type";

            if (Session["AccountTypeNo"] == null)
            {
                SessionAccTypeNo = "";
            }
            else
            {
                SessionAccTypeNo = Session["AccountTypeNo"].ToString();
                txtAccTypeCode.Text = SessionAccTypeNo;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }
     
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from Acc_Mst_AccountType Where AccTypeName='" + txtAccTypeName.Text + "' And Ccode='" + SessionCcode + "' And ";
            SSQL = SSQL + " Lcode ='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "AccountType Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtAccTypeCode.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update Acc_Mst_AccountType set AccTypeName='" + txtAccTypeName.Text + "' Where Ccode='" + SessionCcode + "'";
                SSQL = SSQL + "  And Lcode ='" + SessionLcode + "' And AccTypeCode='" + txtAccTypeCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Type Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into Acc_Mst_AccountType(Ccode,Lcode,AccTypeCode,AccTypeName,Status,UserID,UserName,CreateOn)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtAccTypeCode.Text.ToUpper() + "',";
                SSQL = SSQL + "'" + txtAccTypeName.Text + "','Add','" + SessionUserID + "','" + SessionUserName + "',Getdate())";
                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Type Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtAccTypeCode.Enabled = true;

            Response.Redirect("Mst_AccountType_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Entry Already Exist');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtAccTypeCode.ReadOnly = false;
        txtAccTypeCode.Text = "";
        txtAccTypeName.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Acc_Mst_AccountType where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status = 'Add' And ";
        SSQL = SSQL + " AccTypeCode='" + txtAccTypeCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtAccTypeCode.ReadOnly = true;
            txtAccTypeName.Text = DT.Rows[0]["AccTypeName"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Mst_AccountType_Main.aspx");
    }

}
