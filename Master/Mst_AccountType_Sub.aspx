﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Mst_AccountType_Sub.aspx.cs" Inherits="Mst_AccountType_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upAccTpSub" runat="server">
            <ContentTemplate>
                 <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Account Type Master</h1>
        </section>

        <section class="content">  
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="Row">
                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName"> AccountType Code <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtAccTypeCode" runat="server" class="form-control"/>
                                    <span id="DeptCode" class="text-danger"></span>
                                    <asp:RequiredFieldValidator ControlToValidate="txtAccTypeCode" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> AccountType Name <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtAccTypeName" runat="server" class="form-control" />
                                    <span id="ShortName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtAccTypeName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        
                            <div class="box-footer" runat="server" style="padding-top:25px" >
                                <div class="form-group">
                                    <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                    <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                                    <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
       
    </div>
</asp:Content>

