﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Mst_Ledger_View.aspx.cs" Inherits="Mst_Ledger_View" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Ledger Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Ledger Code <span class="text-danger"></span> </label>
                                    <asp:Label  id="txtLedgerCode" runat="server" class="form-control" Enabled="false"/>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Ledger Name<span class="text-danger"></span> </label>
                                    <asp:Label  id="txtLedgerName" runat="server" class="form-control" />
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Ledger Group<span class="text-danger"></span> </label>
                                    <asp:Label  id="ddlLedgerGrp" runat="server" class="form-control">
                                    </asp:Label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Account Type<span class="text-danger"></span> </label>
                                    <asp:Label  id="ddlAccType" runat="server" class="form-control">
                                    </asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Apartment No/Street<span class="text-danger"></span> </label>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <asp:Label ID="txtHouseNumber" runat="server" Class="form-control"  ></asp:Label>
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="txtAddr1" runat="server" Class="form-control" ></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Address2<span class="text-danger"></span> </label>
                                    <asp:Label ID="txtAddr2" runat="server" Class="form-control"  ></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">City/Postal Code<span class="text-danger"></span> </label>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <asp:Label ID="txtCity" runat="server" Class="form-control"  ></asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="txtPincode" runat="server" Class="form-control"  ></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">State<span class="text-danger"></span> </label>
                                    <asp:Label ID="txtState" runat="server" Class="form-control"  ></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Country<span class="text-danger"></span> </label>
                                    <asp:Label ID="txtCountry" runat="server" Class="form-control"  ></asp:Label>
                                </div>
                            </div>

                             <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Mobile Code / Mobile No<span class="text-danger"></span></label>
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <asp:Label ID="txtMobiCode" runat="server" class="form-control"  ></asp:Label>
                                            </div>
                                            <div class="col-sm-8">
                                                <asp:Label ID="txtMobile" runat="server" Class="form-control"  ></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Std Code / Phone No<span class="text-danger"></span></label>
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <asp:Label ID="txtStdCode" runat="server" class="form-control"  ></asp:Label>
                                            </div>
                                            <div class="col-sm-8">
                                                <asp:Label ID="txtPhone" runat="server" Class="form-control"  ></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Mail Id<span class="text-danger"></span> </label>
                                    <asp:Label ID="txtMail" runat="server" Class="form-control"  ></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Account No<span class="text-danger"></span> </label>
                                    <asp:Label ID="txtAccountNo" runat="server" Class="form-control"  ></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Bank Name<span class="text-danger"></span> </label>
                                    <asp:Label ID="ddlBankName" runat="server" Class="form-control"></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Branch Name<span class="text-danger"></span> </label>
                                    <asp:Label ID="txtBranchName" runat="server" Class="form-control"  ></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">IFSC Code<span class="text-danger"></span> </label>
                                    <asp:Label ID="txtIFSC" runat="server" Class="form-control"  ></asp:Label>
                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="pnlOutTransaction" runat="server" Visible="false">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Transport / Zone<span class="text-danger"></span></label>
                                        <div class="form-group row">
                                            <div class="col-sm-7">
                                                <asp:Label ID="txtTransport" runat="server" class="form-control"  ></asp:Label>
                                            </div>
                                            <div class="col-sm-5">
                                                <asp:Label ID="txtZone" runat="server" class="form-control"  ></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">VAT / GST Number<span class="text-danger"></span> </label>
                                        <asp:Label ID="txtGstin" runat="server" Class="form-control"  ></asp:Label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Notes<span class="text-danger"></span> </label>
                                        <asp:Label ID="txtNotes" runat="server" Class="form-control"  TextMode="MultiLine" style="resize:none"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="pnlEmp" runat="server" Visible="false">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Department<span class="text-danger"></span> </label>
                                        <asp:Label ID="ddlDepartment" runat="server" Class="form-control"></asp:Label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Designation<span class="text-danger"></span> </label>
                                        <asp:Label ID="ddlDesignation" runat="server" Class="form-control"></asp:Label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Aadhar Number<span class="text-danger"></span> </label>
                                        <asp:Label ID="txtAadharNo" runat="server"  Class="form-control"></asp:Label>
                                    </div>
                                </div>

                            </div>
                        </asp:Panel>

                        

                        <div class="box-footer">
                            <div class="form-group" style="align-content:center">
                                <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Visible="false"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <asp:Button ID="btnClear" class="btn btn-primary"  runat="server"  Visible="false" Text="Clear" OnClick="btnClear_Click" />
                                <asp:Button ID="btnBack" class="btn btn-primary"  runat="server"  Text="Back To List" OnClick="btnBack_Click"/>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</asp:Content>

