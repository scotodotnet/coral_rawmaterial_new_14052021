﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Mst_ProductionStage_Sub.aspx.cs" Inherits="Mst_ProductionStage_Sub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upPrdSub" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class="fa fa-cog text-primary"></i>Production Stage</h1>
                    <asp:Label runat="server" ID="lblErrorMsg" style="color:red;font-size:x-large" ></asp:Label>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">

                                    <div class="row">
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label class="control-label">Generator Model<span class="text-danger">*</span></label>
                                                <asp:Label ID="lblStageCode" class="form-control" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Generator Model<span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddlGenModelName" class="form-control select2" AutoPostBack="true"
                                                    runat="server" OnSelectedIndexChanged="ddlGenModelName_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:HiddenField ID="hfGenModelCode" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Product Name<span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddltProductName" class="form-control select2" runat="server">
                                                    <asp:ListItem Value="1">Stator</asp:ListItem>
                                                    <asp:ListItem Value="2">Rotor</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Assembly Stage<span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddlAssemblyStage" class="form-control select2" runat="server">
                                                    <asp:ListItem Value="1">Pre Assembly</asp:ListItem>
                                                    <asp:ListItem Value="2">Final Assembly</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Production Stage<span class="text-danger">*</span></label>
                                                <asp:TextBox ID="txtProductionStage" class="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" Style="resize: none" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" ValidationGroup="Validate_Field" runat="server" Text="Save" />
                                        <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                    </div>
                                </div>
                                <!-- /.box-footer-->
                            </div>
                            <!-- /.box -->
                        </div>
                        <div class="col-md-3" hidden>
                            <div class="callout callout-info" style="margin-bottom: 0!important;">
                                <h4><i class="fa fa-info"></i>Info:</h4>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>

