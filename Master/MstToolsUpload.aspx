﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstToolsUpload.aspx.cs" Inherits="Master_ToolsUpload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-cubes text-primary"></i>Tools Upload
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="content-heading-anchor">
                                        <h4>Production Items</h4>
                                        <div class="fc-content-skeleton">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <br />
                                                        <asp:FileUpload ID="FileUpload" CssClass="btn btn-default fileinput-button" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer no-padding">
                                    <div class="box-footer" align="center">
                                        <asp:Button ID="btnDownload" runat="server" CssClass="btn btn-primary" OnClick="btnDownload_Click" Text="Tools Master Template" />
                                        <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-success" OnClick="btnUpload_Click" Text="Upload Data" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnUpload" />
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </div>

    <!-- jQuery 3 -->
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    
    <script type="text/javascript">
        $(function () {
            //Flat red color scheme for iCheck
            $('.flat-radio').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    </script>
</asp:Content>

