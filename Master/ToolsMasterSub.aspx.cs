﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ToolsMasterSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    public static string SessionCcode = "";
    public static string SessionLcode = "";
    string SessionUserCode;
    string SessionUserName;
    string SessionFinYear;
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYear = Session["FinYear"].ToString();
        }
        if (!IsPostBack)
        {

            Load_Generator();
            Load_AssemblyStage();
            Load_ProductionStep();

            Load_Data_Department();


            Load_Spplier();
            Load_UOM();

            if ((string)Session["TransNo"] != null)
            {
                btnSearch(sender, e);
            }
            else
            {

                TransactionNoGenerate TransNO = new TransactionNoGenerate();


                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get(SessionCcode, SessionLcode, "Tools Master", SessionFinYear, "5");

                txtTransCode.Text = Auto_Transaction_No.ToString();

                TransactionNoGenerate TransNO_CMW = new TransactionNoGenerate();

                
                string Auto_Transaction_No_CMW = TransNO_CMW.Auto_Generate_No_Numbering_Setup_Get(SessionCcode, SessionLcode, ddlAssemblyStage.SelectedItem.Text, SessionFinYear, "5");

                lblLocalId.Text = Auto_Transaction_No_CMW.ToString();

            }
        }
    }

    private void Load_UOM()
    {
        SSQL = "";
        SSQL = "Select * from MstUOM where Status!='Delete'";
        ddlUOM.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlUOM.DataTextField = "UOMShortName";
        ddlUOM.DataValueField = "UOMCode";
        ddlUOM.DataBind();
        ddlUOM.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Spplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select LedgerCode,LedgerName,City,MobileNo,GstNo from Acc_Mst_Ledger where Ccode = '" + SessionCcode + "' And ";
        SSQL = SSQL + " Lcode ='" + SessionLcode + "' And Status='Add' and LedgerGrpName='Supplier' ";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlSupp.DataSource = DT;
        ddlSupp.DataTextField = "LedgerName";
        ddlSupp.DataValueField = "LedgerCode";
        ddlSupp.DataBind();
        ddlSupp.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

   

    private void Load_ProductionStep()
    {
        SSQL = "";
        SSQL = "Select * from MstProductionStep where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlProductionStep.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlProductionStep.DataTextField = "ProductionStepName";
        ddlProductionStep.DataValueField = "TransNo";
        ddlProductionStep.DataBind();
        ddlProductionStep.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_AssemblyStage()
    {
        SSQL = "";
        SSQL = "Select * from MstAssetAssemblyStage where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
        ddlAssemblyStage.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAssemblyStage.DataTextField = "AssemblyStageName";
        ddlAssemblyStage.DataValueField = "TransCode";
        ddlAssemblyStage.DataBind();
        ddlAssemblyStage.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Data_Department()
    {
        SSQL = "";
        SSQL = "Select * from MstDepartment where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Status !='Delete'";


        ddlDeptName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
        ddlDeptName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Generator()
    {
        SSQL = "";
        SSQL = "Select * from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlGenModel.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlGenModel.DataTextField = "GenModelName";
        ddlGenModel.DataValueField = "GenModelNo";
        ddlGenModel.DataBind();
        ddlGenModel.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }



    

 

    private void btnSearch(object sender, EventArgs e)
    {
        SSQL = "Select * from MstTools where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete' ";
        SSQL = SSQL + " And ToolCode='" + Session["TransNo"].ToString() + "'";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            txtTransCode.Text = dt.Rows[0]["ToolCode"].ToString();
            txtToolsName.Text = dt.Rows[0]["ToolName"].ToString();
            txtToolsNo.Text = dt.Rows[0]["RefCode"].ToString();

            ddlAsblyCat.SelectedItem.Text = dt.Rows[0]["CategoryName"].ToString();
            ddlAssemblyStage.SelectedValue = dt.Rows[0]["AssemblyStageCode"].ToString();
            ddlAssemblyStage.SelectedItem.Text = dt.Rows[0]["AssemblyStageName"].ToString(); 
            ddlScope.SelectedItem.Text = dt.Rows[0]["Scope"].ToString();
            ddlGenModel.SelectedItem.Text = dt.Rows[0]["GenModelName"].ToString();
            ddlGenModel.SelectedValue = dt.Rows[0]["GenModelCode"].ToString();
            ddlModule.SelectedItem.Text = dt.Rows[0]["Module"].ToString();
            ddlProductionStep.SelectedValue = dt.Rows[0]["ProductionStepCode"].ToString();
            ddlProductionStep.SelectedItem.Text = dt.Rows[0]["ProductionStepName"].ToString();
            ddlSupp.SelectedValue = dt.Rows[0]["SupplierCode"].ToString();
            ddlSupp.SelectedItem.Text = dt.Rows[0]["SupplierName"].ToString();
            txtSuppSAPCode.Text = dt.Rows[0]["SapNo"].ToString();
            txtBDMNo.Text = dt.Rows[0]["BDMNumber"].ToString();

            lblLocalId.Text = dt.Rows[0]["CMWID"].ToString();
            txtPartNo.Text = dt.Rows[0]["PartNo"].ToString();
            txtSpec.Text = dt.Rows[0]["SplDecs"].ToString();

            txtNote.Text = dt.Rows[0]["Remarks"].ToString();

            ddlDeptName.SelectedItem.Text = dt.Rows[0]["DeptName"].ToString();
            ddlDeptName.SelectedValue = dt.Rows[0]["DeptCode"].ToString();

            if (dt.Rows[0]["Releshed"].ToString()=="1")
            {
                rdbReleased.SelectedValue = "1";
            }
            else
            {
                rdbReleased.SelectedValue = "0";
            }

            ddlType.SelectedItem.Text = dt.Rows[0]["Type"].ToString();
            txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
            txtDrawNo.Text = dt.Rows[0]["DrawingName"].ToString();
            txtRevision.Text = dt.Rows[0]["Revision"].ToString();
            txtAmtStations.Text = dt.Rows[0]["StationsAmt"].ToString();
            txtAmtProStations.Text = dt.Rows[0]["ProStationsAmt"].ToString();
            txtPossibleCapa.Text = dt.Rows[0]["PossibleCapacity"].ToString();
            txtAvailabilityDraw.Text = dt.Rows[0]["AvailabilityDraw"].ToString();
            txtComment.Text = dt.Rows[0]["Comment"].ToString();
            txtRate.Text = dt.Rows[0]["Rate"].ToString();
            txtRateOther.Text = dt.Rows[0]["RateOther"].ToString();
            txtCGST.Text = dt.Rows[0]["CGSTP"].ToString();
            txtSGST.Text = dt.Rows[0]["SGSTP"].ToString();
            txtIGST.Text = dt.Rows[0]["IGSTP"].ToString();
            txtRquQty.Text = dt.Rows[0]["ReqQty"].ToString();
            ddlUOM.SelectedValue = dt.Rows[0]["UOMCode"].ToString();
            ddlUOM.SelectedItem.Text = dt.Rows[0]["UOM"].ToString();
            txtHSN.Text = dt.Rows[0]["HSNCode"].ToString();
            txtMake.Text = dt.Rows[0]["Make"].ToString();

            Server.MapPath(dt.Rows[0]["ImagePath"].ToString());

            btnSave.Text = "Update";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            GetIPAndName getIPAndName = new GetIPAndName();
            //string _IP = getIPAndName.GetIP();
            string _IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(_IP))
            {
                _IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            //string _HostName = getIPAndName.GetName();
            string _HostName = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];

            if (!ErrFlg)
            {
                if (txtToolsName.Text.Contains("'"))
                {
                    txtToolsName.Text = txtToolsName.Text.Replace("'", "''");
                }

                string imgpath = "/assets/images/NoImage.png";

                if (hidd_imgPath.Value != imgpath)
                {
                    imgpath = hidd_imgPath.Value.ToString();
                }
                if (btnSave.Text == "Update")
                {
                    SSQL = "Delete from MstTools where ToolCode='" + Session["TransNo"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                if (!ErrFlg)
                {
                    //TransNo Get

                    if (btnSave.Text == "Save")
                    {
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get(SessionCcode, SessionLcode, "Tools Master", SessionFinYear, "5");
                        if (Auto_Transaction_No == "")
                        {
                            ErrFlg = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                        }
                        else
                        {
                            txtTransCode.Text = Auto_Transaction_No;
                        }
                    }
                    if (!ErrFlg)
                    {
                        if (btnSave.Text == "Save")
                        {
                            SSQL = "";
                            SSQL = "Select * from MstTools where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                            SSQL = SSQL + " AssemblyStageName='" + ddlAssemblyStage.SelectedItem.Text + "' and ToolName='" + txtToolsName.Text + "' ";
                            SSQL = SSQL + " And ToolCode='" + txtTransCode.Text + "' ";

                            DataTable dt_Check = new DataTable();

                            dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (dt_Check.Rows.Count > 0)
                            {
                                ErrFlg = true;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The ItemCode or Item name is Already Present')", true);
                                return;
                            }
                        }

                        if (!ErrFlg)
                        {
                            SSQL = "Insert Into MstTools (Ccode,Lcode,ToolCode,ToolName,CategoryName,AssemblyStageCode,";
                            SSQL = SSQL + " AssemblyStageName,DeptCode,DeptName,CMWID,RefCode,PartNo,SplDecs,Scope,Make,HSNCode,";
                            SSQL = SSQL + " UOMCode,UOM,SupplierCode,SupplierName,DrawingName,Revision,Type,GenModelName,";
                            SSQL = SSQL + " GenModelCode,Module,ProductionStepCode,ProductionStepName,SapNo,BDMNumber,Releshed,";
                            SSQL = SSQL + " Designation,StationsAmt,ProStationsAmt,PossibleCapacity,AvailabilityDraw,Comment,";
                            SSQL = SSQL + " ImageName,ImagePath,Rate,RateOther,CGSTP,SGSTP,IGSTP,ReqQty,UserCode,UserName,CreateOn,";
                            SSQL = SSQL + " Status,Remarks)Values ('" + SessionCcode + "','" + SessionLcode + "',";
                            SSQL = SSQL + " '" + txtTransCode.Text + "','" + txtToolsName.Text.Replace("'", "\"") + "',";
                            SSQL = SSQL + " '" + ddlAsblyCat.SelectedItem.Text + "',";
                            SSQL = SSQL + " '" + ddlAssemblyStage.SelectedValue + "','" + ddlAssemblyStage.SelectedItem.Text + "',";
                            SSQL = SSQL + " '" + ddlDeptName.SelectedValue + "','" + ddlDeptName.SelectedItem.Text + "',";
                            SSQL = SSQL + " '" + lblLocalId.Text + "', '" + txtToolsNo.Text + "','" + txtPartNo.Text + "',";
                            SSQL = SSQL + " '" + txtSpec.Text + "','" + ddlScope.SelectedItem.Text + "','" + txtMake.Text + "',";
                            SSQL = SSQL + " '" + txtHSN.Text + "','" + ddlUOM.SelectedValue + "','" + ddlUOM.SelectedItem.Text + "',";
                            SSQL = SSQL + " '" + ddlSupp.SelectedValue + "','" + ddlSupp.SelectedItem.Text + "', ";
                            SSQL = SSQL + " '" + txtDrawNo.Text + "','" + txtRevision.Text + "','" + ddlType.SelectedItem.Text + "',";
                            SSQL = SSQL + " '" + ddlGenModel.SelectedItem.Text + "','" + ddlGenModel.SelectedValue + "',";
                            SSQL = SSQL + " '" + ddlModule.SelectedItem.Text + "','" + ddlProductionStep.SelectedValue + "',";
                            SSQL = SSQL + " '" + ddlProductionStep.SelectedItem.Text + "','" + txtSuppSAPCode.Text + "',";
                            SSQL = SSQL + " '" + txtBDMNo.Text + "','0','" + txtDesignation.Text + "','" + txtAmtStations.Text + "',";
                            SSQL = SSQL + " '" + txtAmtProStations.Text + "','" + txtPossibleCapa.Text + "','" + txtAvailabilityDraw.Text + "',";
                            SSQL = SSQL + " '" + txtComment.Text + "','','" + imgpath + "','" + txtRate.Text + "',";
                            SSQL = SSQL + " '" + txtRateOther.Text + "','" + txtCGST.Text + "','" + txtSGST.Text + "','" + txtIGST.Text + "',";
                            SSQL = SSQL + " '" + txtRquQty.Text + "', '" + SessionUserCode + "','" + SessionUserName + "',";
                            SSQL = SSQL + " GetDate(),'Add','" + txtNote.Text + "')";

                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                    }
                }
                if (!ErrFlg)
                {
                    if (btnSave.Text == "Save")
                    {
                        //Tool Id update
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get_Update(SessionCcode, SessionLcode, "Tools Master", SessionFinYear, "5");

                        txtTransCode.Text = Auto_Transaction_No;


                        //CWM Id update

                        TransactionNoGenerate TransNO_CMWNo = new TransactionNoGenerate();

                        string Auto_Transaction_No_UP = TransNO_CMWNo.Auto_Generate_No_Numbering_Setup_Get_Update(SessionCcode, SessionLcode, ddlAssemblyStage.SelectedItem.Text, SessionFinYear, "5");

                        lblLocalId.Text = Auto_Transaction_No_UP.ToString();


                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset type Saved Successfully')", true);
                        btnClear_Click(sender, e);
                    }
                    else if (btnSave.Text == "Update")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset type Updated Successfully')", true);
                        btnClear_Click(sender, e);
                    }

                    btnBack_Click(sender, e);
                }

                
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

        txtTransCode.Text = "";
        txtToolsName.Text = "";
        txtToolsNo.Text = ""; ;
        ddlAssemblyStage.SelectedValue = "-Select-";
        ddlAssemblyStage.SelectedItem.Text = "-Select-";
        ddlScope.SelectedItem.Text = "-Select-";
        ddlGenModel.SelectedItem.Text = "-Select-";
        ddlGenModel.SelectedValue = "-Select-";
        ddlModule.SelectedItem.Text = "-Select-";
        ddlProductionStep.SelectedValue = "-Select-";
        ddlProductionStep.SelectedItem.Text = "-Select-";
        ddlSupp.SelectedValue = "-Select-";
        ddlSupp.SelectedItem.Text = "-Select-";
        txtSuppSAPCode.Text = "";
        ddlType.SelectedItem.Text = "";
        txtDesignation.Text = "";
        txtDrawNo.Text = "";
        txtRevision.Text = "";
        txtAmtStations.Text = "";
        txtAmtProStations.Text = "";
        txtPossibleCapa.Text = "";
        txtAvailabilityDraw.Text = "";
        txtComment.Text = "";
        txtNote.Text = "";
        txtRate.Text = "";
        txtRateOther.Text = "";
        txtCGST.Text = "";
        txtSGST.Text = "";
        txtIGST.Text = "";
        txtRquQty.Text = "";
        ddlUOM.SelectedItem.Text = "-Select-";
        ddlUOM.SelectedValue = "-Select-";
        txtHSN.Text = "";

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Master/ToolsMasterMain.aspx");
    }

   
    [WebMethod]
    public static string GetCTNo()
    {
        string msg = "";
        TransactionNoGenerate TransNO = new TransactionNoGenerate();
        string Auto_Transaction_No = "";// TransNO.Auto_Generate_No_Numbering_Setup_Master_Get(SessionCcode, SessionLcode, "Asset Item Master", "5");
        if (Auto_Transaction_No == "")
        {
            msg = "false";
        }
        else
        {
            msg = Auto_Transaction_No.Replace("AIM/", "CT/");
        }
        return msg;
    }

    protected void ddlAssemblyStage_SelectedIndexChanged(object sender, EventArgs e)
    {
        TransactionNoGenerate TransNO = new TransactionNoGenerate();

        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get(SessionCcode, SessionLcode, ddlAssemblyStage.SelectedItem.Text, SessionFinYear, "5");

        if (ddlScope.SelectedItem.Text == "Enercon")
        {
            lblLocalId.Text = Auto_Transaction_No.ToString();
            lblLocalId.Text = "E-" + lblLocalId.Text;
        }
        else if (ddlScope.SelectedItem.Text == "Coral")
        {
            lblLocalId.Text = Auto_Transaction_No.ToString();
            lblLocalId.Text = "C-" + lblLocalId.Text;
        }
    }

    protected void ddlScope_SelectedIndexChanged(object sender, EventArgs e)
    {
        TransactionNoGenerate TransNO = new TransactionNoGenerate();

        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get(SessionCcode, SessionLcode, ddlAssemblyStage.SelectedItem.Text, SessionFinYear, "5");

        if (ddlScope.SelectedItem.Text == "Enercon")
        {
            lblLocalId.Text = Auto_Transaction_No.ToString();
            lblLocalId.Text = "E-" + lblLocalId.Text;
        }
        else if (ddlScope.SelectedItem.Text == "Coral")
        {
            lblLocalId.Text = Auto_Transaction_No.ToString();
            lblLocalId.Text = "C-" + lblLocalId.Text;
        }
    }
}