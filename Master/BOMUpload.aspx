﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BOMUpload.aspx.cs" Inherits="Master_BOMUpload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-cubes text-primary"></i>BOM Materials Upload
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="content-heading-anchor">
                                        <h4>Production Items</h4>
                                        <div class="fc-content-skeleton">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Generator Model</label>
                                                        <asp:DropDownList ID="ddlGenerator" AutoPostBack="true" OnSelectedIndexChanged="ddlGenerator_SelectedIndexChanged"
                                                            CssClass="form-control select2" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Production Part Name</label>
                                                        <asp:DropDownList ID="ddlProduct" CssClass="form-control select2" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Supplier Name</label>
                                                        <asp:DropDownList ID="ddlSupplier" CssClass="form-control select2" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Department</label>
                                                        <asp:DropDownList ID="ddlDepartment" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                                            CssClass="form-control select2" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Item Stage</label>
                                                        <asp:DropDownList ID="ddlStage"
                                                            CssClass="form-control select2" runat="server">
                                                            <asp:ListItem Selected="True" Value="-Select-" Text="-Select-"></asp:ListItem>
                                                            <asp:ListItem Value="Stage1" Text="Stage1"></asp:ListItem>
                                                            <asp:ListItem Value="Stage2" Text="Stage2"></asp:ListItem>
                                                            <asp:ListItem Value="Stage3" Text="Stage3"></asp:ListItem>
                                                            <asp:ListItem Value="Stage4" Text="Stage4"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <br />
                                                        <asp:FileUpload ID="FileUpload" CssClass="btn btn-default fileinput-button" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Replace or Skip the Item (if Exists)<br />
                                                            <asp:RadioButtonList ID="rbtReplaceExistingItem" RepeatColumns="2" class="flat-radio form-control" runat="server">
                                                                <asp:ListItem value="Skip" Text="Skip" Selected="True" ></asp:ListItem>
                                                                <asp:ListItem Value="Replace" style="padding:40px" Text="Replace"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer no-padding">
                                    <div class="box-footer" align="center">
                                        <asp:Button ID="btnWareHouseList" class="btn btn-primary" runat="server" OnClick="btnWareHouseList_Click" Text="Empty Ware House List" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="btn btn-primary" OnClick="btnDownload_Click" Text="BOM Item Template" />
                                        <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-success" OnClick="btnUpload_Click" Text="Upload Data" />
                                        <asp:Button ID="btnClear" runat="server" CssClass="btn btn-danger" OnClick="btnClear_Click" Text="Clear" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnUpload" />
                    <asp:PostBackTrigger ControlID="btnWareHouseList" />
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </div>

    <!-- jQuery 3 -->
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    
    <script type="text/javascript">
        $(function () {
            //Flat red color scheme for iCheck
            $('.flat-radio').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    </script>
</asp:Content>

