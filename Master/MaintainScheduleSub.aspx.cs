﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MaintainScheduleSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            if (!IsPostBack)
            {
                if ((string)Session["Schedule_ID"] != null)
                {
                    Search(Session["Schedule_ID"].ToString());
                }
            }
        }
    }
    private void Search(string id)
    {
        SSQL = "";
        SSQL = "Select * from MaintenanceScheduleMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Schedule_ID='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtScheduleType.Enabled = false;
            txtScheduleType.Text = dt.Rows[0]["Schedule_Type"].ToString();
            txtSchedulename.Text = dt.Rows[0]["Schedule_Name"].ToString();
            txtScheduleDays.Text = dt.Rows[0]["Schedule_Days"].ToString();
          
            btnSave.Text = "Update";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (txtScheduleType.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Schedule Type')", true); ;
            ErrFlg = true;
        }
        if (txtSchedulename.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Schedule name')", true); ;
            ErrFlg = true;
        }
        if (txtScheduleDays.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Schedule Days')", true); ;
            ErrFlg = true;
        }

        if (!ErrFlg)
        {

            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from MaintenanceScheduleMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Schedule_Type='" + txtScheduleType.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MaintenanceScheduleMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Schedule_Type='" + txtScheduleType.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Schedule Type Already Taken')", true); ;

                    ErrFlg = true;
                    return;
                }
              
            }
            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "";
                SSQL = "Insert into MaintenanceScheduleMaster(Ccode,Lcode,Schedule_Days,Schedule_Name,Schedule_Type,CreatedOn,Host_IP,Host_Name,UserName,Status)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "' ,'" + txtScheduleDays.Text + "','" + txtSchedulename.Text + "','" + txtScheduleType.Text + "',";
                SSQL = SSQL + " '" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + Session["UserID"] + "','Add')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                btnClear_Click(sender, e);

                Response.Redirect("/Master/MaintainScheduleMain.aspx");
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtScheduleDays.Text = "0.0";
        txtSchedulename.Text = "";
        txtScheduleType.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("Schedule_ID");
        Response.Redirect("/Master/MaintainScheduleMain.aspx");
    }
}