﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Mst_ProductionStage_Main.aspx.cs" Inherits="Mst_ProductionStage_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <!-- Content Wrapper. Contains page content -->
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-1">
                            <asp:Button ID="btnAddNew" runat="server" CssClass="btn btn-primary" OnClick="btnAddNew_Click" Text="Add New" /><br />
                            <br />
                        </div>
                        <!-- /.col -->
                    </div>
                    <!--/.row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-cog text-primary"></i><span>Product Items</span></h3>

                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-hover table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>SL_No</th>
                                                                <th>Production Stage Name</th>
                                                                <th>Generator Name</th>
                                                                <th>Production Part Name</th>
                                                                <td>Mode</td>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tbody>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1 %> </td>
                                                            <td><%# Eval("StageName")%></td>
                                                            <td runat="server" visible="false"><%# Eval("StageCode")%></td>
                                                            <td><%# Eval("GenModelName")%></td>
                                                            <td><%# Eval("ProductPartName")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm  fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("StageCode")%>'>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteIssueEntry_Grid" class="btn btn-danger btn-sm  fa fa-trash" runat="server"
                                                                    Text="" OnCommand="btnDeleteIssueEntry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("StageCode")%>'
                                                                    CausesValidation="true" OnClientClick="retun Confirm('Are you sure want to Delete this Item ?')">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                            <!-- /.table -->
                                        </div>




                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </section>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                }
            });
        };
    </script>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
</asp:Content>

