﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstSupplier_Sub.aspx.cs" Inherits="MstSupplier_Sub_Old" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upSuppsub" runat="server">
            <ContentTemplate>

                <section class="content-header">
                    <h1><i class=" text-primary"></i>Supplier Master</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Code<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtLedgerCode" runat="server" class="form-control" />
                                                <span id="DeptCode" class="text-danger"></span>
                                                <asp:RequiredFieldValidator ControlToValidate="txtLedgerCode" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Name<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtLedgerName" runat="server" class="form-control" />
                                                <span id="ShortName" class="text-danger"></span>
                                                <asp:RequiredFieldValidator ControlToValidate="txtLedgerName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Ledger Group<span class="text-danger">*</span> </label>
                                                <asp:DropDownList ID="ddlLedgerGrp" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Account Type<span class="text-danger">*</span> </label>
                                                <asp:DropDownList ID="ddlAccType" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Apartment No/Street<span class="text-danger">*</span> </label>
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtHouseNumber" runat="server" Class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtAddr1" runat="server" Class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Address2<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtAddr2" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">City/Postal Code<span class="text-danger">*</span> </label>
                                                <div class="form-group row">
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtCity" runat="server" Class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtPincode" runat="server" Class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">State<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtState" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Country<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtCountry" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Mobile Code / Mobile No<span class="text-danger">*</span></label>
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtMobiCode" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtMobile" runat="server" Class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Std Code / Phone No<span class="text-danger">*</span></label>
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtStdCode" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtPhone" runat="server" Class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Mail Id<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtMail" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Web Site<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtWebSite" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Account No<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtAccountNo" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Bank Name<span class="text-danger">*</span> </label>
                                                <asp:DropDownList ID="ddlBankName" runat="server" Class="form-control select2"></asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Branch Name<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtBranchName" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">IFSC Code<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtIFSC" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Transport / Zone<span class="text-danger">*</span></label>
                                                <div class="form-group row">
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="txtTransport" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <asp:TextBox ID="txtZone" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">VAT / GST Number<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtGstin" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Notes<span class="text-danger">*</span> </label>
                                                <asp:TextBox ID="txtNotes" runat="server" Class="form-control" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Terms And Condition</h3>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="box-body no-padding">
                                                    <div class="row">

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Packing Terms / Certificate</span> </label>
                                                                <asp:TextBox ID="txtPackTerms" runat="server" Class="form-control" 
                                                                    TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Quality / Installation & Commissioning</label>
                                                                <asp:TextBox ID="txtQuality" runat="server" Class="form-control" 
                                                                    TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Warranty</label>
                                                                <asp:TextBox ID="txtWarranty" runat="server" Class="form-control" 
                                                                    TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Terms and Conditions</span> </label>
                                                                <asp:TextBox ID="txtTC" runat="server" Class="form-control" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" runat="server" visible="false">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>BOM Details</h3>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="box-body no-padding">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Purchase Order Type</label>
                                                                <asp:DropDownList ID="ddlPurType" runat="server" class="form-control select2" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlPurType_SelectedIndexChanged">
                                                                    <asp:ListItem Value="1">Single Item</asp:ListItem>
                                                                    <asp:ListItem Value="2">Model Based</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="pnlModel" runat="server" Visible="false">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Generator Model</label>
                                                                    <asp:DropDownList ID="txtGenerator_Model" runat="server" class="form-control select2" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="txtGenerator_Model_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Production PartName</label>
                                                                    <asp:DropDownList ID="txtProduction_Part_Name" runat="server" class="form-control select2" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="txtProduction_Part_Name_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtProduction_Part_Name" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlSingle" runat="server" Visible="false">
                                                        <div class="row">

                                                             <div class="form-group col-md-3">
                                                                <label for="exampleInputName">SAP No</label>
                                                                <asp:DropDownList ID="ddlSAPNo" runat="server" class="form-control select2" 
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSAPNo_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                              
                                                            </div>

                                                            <div class="form-group col-md-3">
                                                                <label for="exampleInputName">Material Name</label>
                                                                <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="txtItemNameSelect_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                                <asp:TextBox ID="txtItemCode" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                            </div>

                                                            <div class="form-group col-md-2" runat="server" style="padding-top: 5px">
                                                                <br />
                                                                <asp:Button ID="btnAddItem" class="btn btn-primary" runat="server" Text="Add"
                                                                    ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click" />
                                                            </div>

                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Repeater ID="rpBOMLIST" runat="server" EnableViewState="true">
                                                        <HeaderTemplate>
                                                            <table class="table table-hover table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <td>SL. No</td>
                                                                        <td>Model Name</td>
                                                                        <td>Production Part</td>
                                                                        <td>ItemName</td>
                                                                        <td>SAP No</td>
                                                                        <td>Mode</td>
                                                                    </tr>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Container.ItemIndex+1 %></td>
                                                                <td><%# Eval("ModelCode") %></td>
                                                                <td runat="server" visible="false"><%# Eval("ModelName") %></td>
                                                                <td runat="server" visible="false"><%# Eval("ProductionPartCode") %></td>
                                                                <td><%# Eval("ProductionPartName") %></td>
                                                                <td runat="server" visible="false"><%# Eval("ItemCode") %></td>
                                                                <td><%# Eval("ItemName") %> </td>
                                                                <td><%# Eval("SAPNo") %></td>
                                                                <td>
                                                                    <asp:LinkButton ID="btnDelItem" runat="server" class="btn btn-danger btn-sm fa fa-trash-o"
                                                                        Text="" OnCommand="btnDelItem_Command" CommandArgument="Delete" CommandName='<%# Eval("ItemCode") %>'
                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate></table></FooterTemplate>
                                                    </asp:Repeater>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group" style="align-content: center">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                }
            });
        };
    </script>

</asp:Content>

