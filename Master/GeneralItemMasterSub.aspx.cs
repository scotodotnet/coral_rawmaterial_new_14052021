﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class GeneralItemMasterSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    public static string SessionCcode = "";
    public static string SessionLcode = "";
    public static string SessionUserID = "";
    string SessionUserCode;
    string SessionUserName;
    string SessionFinYear;
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYear = Session["FinYear"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            
            
        }
        if (!IsPostBack)
        {
            Load_UOM();

            if ((string)Session["TransNo"] != null)
            {
                btnSearch(sender, e);
            }
            else
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get_Test(SessionCcode, SessionLcode, "General Item Master", SessionFinYear, "5");

                txtRefItemCode.Text = Auto_Transaction_No;
            }
        }
    }

    private void Load_UOM()
    {
        SSQL = "";
        SSQL = "Select * from MstUOM where Status!='Delete'";
        ddlUOM.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlUOM.DataTextField = "UOMShortName";
        ddlUOM.DataValueField = "UOMCode";
        ddlUOM.DataBind();
        ddlUOM.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }


    private void btnSearch(object sender, EventArgs e)
    {
        SSQL = "Select * from MstGeneralItem where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete' ";
        SSQL = SSQL + " And ItemCode='" + Session["TransNo"].ToString() + "'";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            txtTransCode.Text = dt.Rows[0]["ItemCode"].ToString();
            txtGenItemName.Text = dt.Rows[0]["ItemName"].ToString();
            txtRefItemCode.Text = dt.Rows[0]["RefCode"].ToString();
            ddlScope.SelectedItem.Text = dt.Rows[0]["Scope"].ToString();
            txtSuppHSNCode.Text = dt.Rows[0]["HSNNo"].ToString();
            txtNote.Text = dt.Rows[0]["SplDecs"].ToString();
            ddlUOM.SelectedValue = dt.Rows[0]["UOMCode"].ToString();
            ddlUOM.SelectedItem.Text = dt.Rows[0]["UOM"].ToString();
            txtDrawNo.Text = dt.Rows[0]["DrawingName"].ToString();

            btnSave.Text = "Update";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            GetIPAndName getIPAndName = new GetIPAndName();
            //string _IP = getIPAndName.GetIP();
            string _IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(_IP))
            {
                _IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            //string _HostName = getIPAndName.GetName();
            string _HostName = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];

            if (!ErrFlg)
            {
                if (txtGenItemName.Text.Contains("'"))
                {
                    txtGenItemName.Text = txtGenItemName.Text.Replace("'", "''");
                }


                if (btnSave.Text == "Update")
                {
                    SSQL = "Delete from MstGeneralItem where ItemCode='" + Session["TransNo"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                if (!ErrFlg)
                {
                    //TransNo Get

                    if (btnSave.Text == "Save")
                    {
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get_Update(SessionCcode, SessionLcode, "General Item Master", SessionFinYear, "5");
                        if (Auto_Transaction_No == "")
                        {
                            ErrFlg = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                        }
                        else
                        {
                            txtTransCode.Text = Auto_Transaction_No;
                        }
                    }
                    if (!ErrFlg)
                    {
                        if (btnSave.Text == "Save")
                        {
                            SSQL = "";
                            SSQL = "Select * from MstGeneralItem where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                            SSQL = SSQL + " ItemName='" + txtGenItemName.Text + "' And ItemCode='" + txtTransCode.Text + "' ";

                            DataTable dt_Check = new DataTable();

                            dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (dt_Check.Rows.Count > 0)
                            {
                                ErrFlg = true;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The ItemCode or Item name is Already Present')", true);
                                return;
                            }
                        }

                        if (!ErrFlg)
                        {
                            SSQL = "Insert Into MstGeneralItem (Ccode,Lcode,ItemCode,ItemName,RefCode,Scope,UOMCode,UOM,HSNNo,DrawingName,";
                            SSQL = SSQL + " StationsAmt,ProStationsAmt,Comment,SplDecs,Rate,CGSTP,SGSTP,IGSTP,UserCode,UserName,CreateOn,Status)";
                            SSQL = SSQL + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtTransCode.Text + "',";
                            SSQL = SSQL + " '" + txtGenItemName.Text + "','" + txtRefItemCode.Text + "','" + ddlScope.SelectedItem.Text + "',";
                            SSQL = SSQL + " '" + ddlUOM.SelectedValue + "','" + ddlUOM.SelectedItem.Text + "','" + txtSuppHSNCode.Text + "',";
                            SSQL = SSQL + " '" + txtDrawNo.Text + "','0.00','0.00','','" + txtNote.Text + "',";
                            SSQL = SSQL + " '0.00','0','0','0','" + SessionUserCode + "','" + SessionUserName + "',GetDate(),'Add')";


                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                    }
                }
                if (!ErrFlg)
                {
                    if (btnSave.Text == "Save")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset type Saved Successfully')", true);
                        btnClear_Click(sender, e);
                    }
                    else if (btnSave.Text == "Update")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Asset type Updated Successfully')", true);
                        btnClear_Click(sender, e);
                    }

                    btnBack_Click(sender, e);
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

        txtTransCode.Text = "";
        txtGenItemName.Text = "";
        txtRefItemCode.Text = ""; ;
        txtSuppHSNCode.Text = "";
        txtDrawNo.Text = "";
        txtAmtStations.Text = "";
        txtAmtProStations.Text = "";
        txtComment.Text = "";
        txtNote.Text = "";
        txtRate.Text = "";
        txtCGST.Text = "";
        txtSGST.Text = "";
        txtIGST.Text = "";
        ddlUOM.SelectedItem.Text = "-Select-";
        ddlUOM.SelectedValue = "-Select-";

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Master/GeneralItemMasterMain.aspx");
    }
}