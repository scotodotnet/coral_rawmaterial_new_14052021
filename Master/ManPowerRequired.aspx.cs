﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Master_ManPowerRequired : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();

            if (!IsPostBack)
            {
                
                Load_Genrator();
                Load_Product();
                Load_Data();
            }
        }
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "select * from RequiredManPower where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        RptrManPowerRequired.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        RptrManPowerRequired.DataBind();

    }

    private void Load_Product()
    {
        SSQL = "";
        SSQL = "Select  (ProductNo+' ~> '+ProductName) as Text,ProductNo as Value from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
        ddlProduct.DataTextField = "Text";
        ddlProduct.DataValueField = "Value";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    private void Load_Genrator()
    {
        SSQL = "";
        SSQL = "Select (GenModelNo+' ~> '+GenModelName) as Text,GenModelNo as Value from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";

        ddlGenerator.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlGenerator.DataTextField = "Text";
        ddlGenerator.DataValueField = "Value";
        ddlGenerator.DataBind();
        ddlGenerator.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void ddlGenerator_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlGenerator.SelectedValue != "-Select-")
        {
            SSQL = "";
            SSQL = "Select  (ProductNo+' ~> '+ProductName) as Text,ProductNo as Value from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
            SSQL = SSQL + " and GenModelNo='" + ddlGenerator.SelectedValue + "'";
            ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
            ddlProduct.DataTextField = "Text";
            ddlProduct.DataValueField = "Value";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
        else
        {
            Load_Product();
        }
    }

    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
      
        if (!ErrFlg)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from RequiredManPower where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                SSQL = SSQL + "GenModelNo='" + ddlGenerator.SelectedValue + "' and ProductionPartNo='" + ddlProduct.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from RequiredManPower where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                SSQL = SSQL + "GenModelNo='" + ddlGenerator.SelectedValue + "' and ProductionPartNo='" + ddlProduct.SelectedValue + "'";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Requirement Type Already Taken')", true);
                    return;
                }
            }
            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();
                string _generatorName = "";
                string _productName = "";
                SSQL = "";
                SSQL = "select * from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and GenModelNo='" + ddlGenerator.SelectedValue + "' and Status!='Delete'";
                DataTable Dt_Generator = new DataTable();
                Dt_Generator = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Dt_Generator.Rows.Count > 0)
                {
                    _generatorName = Dt_Generator.Rows[0]["GenModelName"].ToString();
                }
                SSQL = "";
                SSQL = "select * from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ProductNo='" + ddlProduct.SelectedValue + "' and Status!='Delete'";
                DataTable Dt_Product = new DataTable();
                Dt_Product = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Dt_Product.Rows.Count > 0)
                {
                    _productName = Dt_Product.Rows[0]["GenModelName"].ToString();
                }

                SSQL = "";
                SSQL = "inser into RequiredManPower(Ccode,Lcode,GenModelNo,GenModelName,ProductionPartNo,ProductionPartName,PersonsRequired,RequiredDays,";
                SSQL = SSQL + "FromDate_Str,FromDate,ToDate_Str,ToDate,CreatedOn,Host_IP,Host_Name,UserName,Status)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlGenerator.SelectedValue + "','" + _generatorName.ToString() + "','" + ddlProduct.SelectedValue + "',";
                SSQL = SSQL + "'" + _productName.ToString() + "','" + txtRequiredQty.Text + "','" + txtTotalDays.Text + "','" + txtFromDate.Text + "','" + Convert.ToDateTime(txtFromDate.Text) + "',";
                SSQL = SSQL + "'" + txtTODate.Text + "','" + Convert.ToDateTime(txtTODate.Text) + "',";
                SSQL = SSQL + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + Session["UserID"].ToString() + "','Add')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                btnClear_Click(sender, e);
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlGenerator.ClearSelection();
        ddlProduct.ClearSelection();
        txtRequiredQty.Text = "0.00";
        txtTotalDays.Text = "0.00";
        txtFromDate.Text = "";
        txtTODate.Text = "";
        btnSave.Text = "Save";
    }
    
    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from RequiredManPower where Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "' and MRQid='" + e.CommandName.ToString() + "'";
        DataTable dt_edit = new DataTable();
        dt_edit = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_edit.Rows.Count > 0)
        {
            ddlGenerator.SelectedValue = dt_edit.Rows[0]["GenModelNo"].ToString();
            ddlProduct.SelectedValue = dt_edit.Rows[0]["ProductionPartNo"].ToString();
            txtFromDate.Text = dt_edit.Rows[0]["FromDate_Str"].ToString();
            txtTODate.Text = dt_edit.Rows[0]["ToDate_Str"].ToString();
            txtTotalDays.Text = dt_edit.Rows[0]["RequiredDays"].ToString();
            txtRequiredQty.Text = dt_edit.Rows[0]["PersonsRequired"].ToString();
            btnSave.Text = "Update";
            Load_Data();
        }
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Update RequiredManPower set Status='Delete' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MRQid='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page,this.GetType(),"alert","alert('Item has been Deleted')",true);
        Load_Data();
    }
}