﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Bank.aspx.cs" Inherits="Master_Bank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-bank text-primary"></i> Bank 
            </h1>
        </section>
        <!-- Main content -->
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">

                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Bank Name </label>
                                                <asp:TextBox runat="server" ID="txtBankName" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtBankName" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>IFSC Code</label>
                                                <asp:TextBox runat="server" ID="txtIFSCCode" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtIFSCCode" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Branch</label>
                                                <asp:TextBox runat="server" ID="txtBranch" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtBranch" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                    </div>
                                    <!-- end row -->


                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Charges Amount</label>
                                                <asp:TextBox runat="server" ID="txtChargesAmt" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtChargesAmt" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>



                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Max. Charges</label>
                                                <asp:TextBox runat="server" ID="txtMaxCharge" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtMaxCharge" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Min. Charges</label>
                                                <asp:TextBox runat="server" ID="txtMinCharge" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtMinCharge" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-md-8">
                                                    <label class="checkbox-inline">
                                                        <asp:CheckBox ID="chkDefault" runat="server" />
                                                        Default
                                       
                                                    </label>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" ValidationGroup="Validate_Field" runat="server" Text="Save" />
                                                <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">

                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>BankName</th>
                                                                <th>IFSCCode</th>
                                                                <th>Branch</th>
                                                                <th>Default</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("BankName")%></td>
                                                        <td><%# Eval("IFSCCode")%></td>
                                                        <td><%# Eval("Branch")%></td>
                                                        <td><%# Eval("Default_Bank")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%# Eval("Branch")%>' CommandName='<%# Eval("BankName")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("Branch")%>' CommandName='<%# Eval("BankName")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Bank details?');">
                                                            </asp:LinkButton>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>

                                        </div>
                                        <!-- /.box-body -->
                                        
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <div class="col-md-3" hidden>
                                    <div class="callout callout-info" style="margin-bottom: 0!important;">
                                        <h4><i class="fa fa-info"></i>Info:</h4>
                                        <p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- /.content -->
    </div>
</asp:Content>

