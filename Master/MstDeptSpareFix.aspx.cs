﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstDeptSpareFix : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Waste Material";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Data();

            //string SSQL = "Select Max(Id) from MstDeptSpareFix";
            //DataTable DT = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (DT.Rows.Count>0)
            //{

            //}

            Load_Data_Empty_DeptCode();
        }

        Load_Data();
        Load_Data_Empty_DeptCode();
    }

    private void Load_Data_Empty_DeptCode()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();
         
        SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status <> 'Delete'";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlDeptName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check



        if (SaveMode != "Error")
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstDeptSpareFix set SpareFixName='" + txtSpareFixName.Text + "',DeptCode='" + ddlDeptName.SelectedValue + "',";
                SSQL = SSQL + " DeptName='" + ddlDeptName.SelectedItem.Text + "' where Ccode='" + SessionCcode + "' and ";
                SSQL = SSQL + " Lcode ='" + SessionLcode + "' and SpareFixCode='" + txtSpareFixCode.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Spare Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstDeptSpareFix(Ccode,Lcode,SpareFixCode,SpareFixName,DeptCode,DeptName,Status,UserID,UserName,CreateOn)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtSpareFixCode.Text.ToUpper() + "',";
                SSQL = SSQL + "'" + txtSpareFixName.Text + "','" +  ddlDeptName.SelectedValue + "','" + ddlDeptName.SelectedItem.Text + "',";
                SSQL = SSQL + "'','" + SessionUserID + "','" + SessionUserName + "',Getdate())";
                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Spare Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtSpareFixCode.Enabled = true;
            Load_Data();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Spare Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtSpareFixCode.ReadOnly = false;
        txtSpareFixCode.Text = "";
        txtSpareFixName.Text = "";
        ddlDeptName.SelectedItem.Text = "-Select-";
        ddlDeptName.SelectedItem.Value = "-Select-";
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select * from MstDeptSpareFix where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        GrdDepartment.DataSource = DT;
        GrdDepartment.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {

        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        bool Rights_Check = false;
        string SaveMode = "Update";

        //Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");

        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Department Details...');", true);
        //}

        if (SaveMode == "Update")
        {
            SSQL = "Select SpareFixCode,SpareFixName,DeptCode,DeptName from MstDeptSpareFix where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And SpareFixCode ='" + e.CommandName.ToString() + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            txtSpareFixCode.Text = DT.Rows[0]["SpareFixCode"].ToString();
            txtSpareFixCode.ReadOnly = true;

            txtSpareFixName.Text = DT.Rows[0]["SpareFixName"].ToString();

            ddlDeptName.SelectedItem.Text = DT.Rows[0]["DeptName"].ToString();
            ddlDeptName.SelectedItem.Value = DT.Rows[0]["DeptCode"].ToString();

            btnSave.Text = "Update";
        }
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "UOM");

        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete UOM...');", true);
        //}

        if (!ErrFlag)
        {
            SSQL = "Select * from MstDeptSpareFix where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SpareFixCode='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                SSQL = "Update MstDeptSpareFix set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " SpareFixCode ='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Waste Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();
            }
        }
    }
}
