﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstAssemblyStage_Sub.aspx.cs" Inherits="Master_MstAssemblyStage_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Asset Type</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        
                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Trans.Code</label>
                                                <asp:TextBox runat="server" ID="txtTransCode" class="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Trans.Date</label>
                                                <asp:TextBox runat="server" ID="txtTransDate" class="form-control datepicker"></asp:TextBox>
                                              
                                                <asp:RequiredFieldValidator ControlToValidate="txtTransDate" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Assembly Stage Name</label>
                                                <asp:TextBox runat="server" ID="txtAssemblyStageName" class="form-control" ></asp:TextBox>
                                                  <asp:AutoCompleteExtender ID="txtAssemblyStageName_AutoCompleteExtender" CompletionListItemCssClass="form-control" runat="server" DelimiterCharacters=""
                                                    Enabled="false" ServiceMethod="GetListofCountries" MinimumPrefixLength="1" EnableCaching="true"  CompletionListHighlightedItemCssClass="form-control"
                                                    TargetControlID="txtAssemblyStageName">
                                                </asp:AutoCompleteExtender>
                                                  <asp:Label ID="lblTransGerman" runat="server" Text=""></asp:Label>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAssemblyStageName" Display="Dynamic" ValidationGroup="ValidateDept_Field"
                                                    class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                                    ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <asp:TextBox runat="server" ID="txtRemarks" TextMode="MultiLine" style="resize:none" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Status</label>
                                            <asp:RadioButtonList ID="RblStatus" class="form-control" runat="server"
                                                RepeatColumns="4">
                                                <asp:ListItem Value="1" Text="Active" style="padding-right: 15px" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="IN Active"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        
                                    </div>
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

