﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ToolsMasterMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
        }
        Load_Data();
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "Select * from MstTools where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        string[] split_1 = e.CommandArgument.ToString().Split(',');
        Session["TransNo"] = e.CommandArgument.ToString();
        //Session["AssetCompCode"] = split_1[1].ToString();
        //Session["AssetCompLCode"] = split_1[2].ToString();
        //Session["DeptCode"] = split_1[3].ToString();
        //Session["AssetTypeCode"] = e.CommandName.Split(',').First().ToString();
        //Session["AssetCategoryTypeCode"] = e.CommandName.Split(',').Last().ToString();
        Response.Redirect("/Master/ToolsMasterSub.aspx");
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        string[] split_1 = e.CommandArgument.ToString().Split(',');
        
        SSQL = "";
        SSQL = "Update MstTools set Status='Delete' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and ToolCode='" + e.CommandArgument.ToString() + "'";
        //SSQL = SSQL + " and ItemCode='" + split_1[0].ToString() + "' and AssetTypeCode='" + e.CommandName.Split(',').First().ToString() + "'";
        //SSQL = SSQL + " and AssetCategoryTypeCode='" + e.CommandName.Split(',').Last().ToString() + "'";
        //SSQL = SSQL + " and DeptCode='"+split_1[3].ToString()+ "' and Asset_Comp_Code='"+split_1[1].ToString()+ "' and Asset_Comp_LCode='"+split_1[2].ToString()+"'";

        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session.Remove("TransNo");
        //Session.Remove("AssetTypeCode");
        //Session.Remove("AssetCategoryTypeCode");
        Response.Redirect("/Master/ToolsMasterSub.aspx");
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstToolsUpload.aspx");
    }
}