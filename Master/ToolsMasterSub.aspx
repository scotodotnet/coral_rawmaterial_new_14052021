﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ToolsMasterSub.aspx.cs" Inherits="ToolsMasterSub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Tools Item</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">

                                    <div class="row">

                                        <div class="col-md-2" runat="server">
                                            <label class="control-label">ToolCode</label>
                                            <asp:Label ID="txtTransCode" class="form-control" runat="server">
                                            </asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                         <div class="col-md-2">
                                            <label class="control-label">Scope</label>
                                            <asp:DropDownList ID="ddlScope" class="form-control select2" runat="server" 
                                                AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlScope_SelectedIndexChanged">
                                                <asp:ListItem Value="Enercon">Enercon</asp:ListItem>
                                                <asp:ListItem Value="Coral">Coral</asp:ListItem>
                                                <asp:ListItem Value="Escrow">Escrow</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Category <span class="text-danger">*</span> </label>
                                            <asp:DropDownList ID="ddlAsblyCat" class="form-control select2" runat="server">
                                                <asp:ListItem Value="1">Pre Assembly</asp:ListItem>
                                                <asp:ListItem Value="2">Final Assembly</asp:ListItem>
                                                <asp:ListItem Value="3">Bearing Assembly</asp:ListItem>
                                                <asp:ListItem Value="4">General</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Sub Category <span class="text-danger">*</span></label>
                                            <asp:DropDownList ID="ddlAssemblyStage" class="form-control select2" runat="server" 
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlAssemblyStage_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Department <span class="text-danger">*</span> </label>
                                            <asp:DropDownList ID="ddlDeptName" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">CMWID Number <span class="text-danger">*</span> </label>
                                            <asp:Label ID="lblLocalId" class="form-control" runat="server">
                                            </asp:Label>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Position Number <span class="text-danger">*</span> </label>
                                            <asp:TextBox ID="txtToolsNo" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                    </div>

                                    <div class="row" runat="server" visible="false">

                                        <div class="col-md-2">
                                            <label class="control-label">Generator Model</label>
                                            <asp:DropDownList ID="ddlGenModel" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Module (Baugruppe)</label>
                                            <asp:DropDownList ID="ddlModule" class="form-control select2" runat="server">
                                                <asp:ListItem Value="1">Stator</asp:ListItem>
                                                <asp:ListItem Value="2">Rotor</asp:ListItem>
                                                <asp:ListItem Value="3">Rotor(Gema)</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Production Step</label>
                                            <asp:DropDownList ID="ddlProductionStep" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                    </div>


                                    <br />
                                    <div class="row">

                                        <div class="col-md-3">
                                            <label class="control-label">German Description (Bezeichnung) <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtDesignation" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Tool Description <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtToolsName" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Part Number <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtPartNo" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Specification <span class="text-danger">*</span> </label>
                                            <asp:TextBox ID="txtSpec" class="form-control" runat="server" TextMode="MultiLine" 
                                                style="resize:none">
                                            </asp:TextBox>
                                        </div>

                                    </div>
                                    <br />
                                    <div class="row">

                                        <div class="col-md-3">
                                            <label class="control-label">Make <span class="text-danger">*</span> </label>
                                            <asp:TextBox ID="txtMake" class="form-control" Style="resize: none"
                                                runat="server">
                                            </asp:TextBox>
                                        </div>


                                        <div class="col-md-3">
                                            <label class="control-label">HSN Code <span class="text-danger">*</span> </label>
                                            <asp:TextBox ID="txtHSN" class="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Unit of Measurement (UOM) <span class="text-danger">*</span> </label>
                                            <asp:DropDownList ID="ddlUOM" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>


                                        <div class="col-md-3">
                                            <label class="control-label">Supplier <span class="text-danger">*</span></label>
                                            <asp:DropDownList ID="ddlSupp" class="form-control select2" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <br />

                                    <div class="row">

                                        <div class="col-md-3">
                                            <label class="control-label">Drawing Number <span class="text-danger">*</span> </label>
                                            <asp:TextBox ID="txtDrawNo" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Drawing Revision <span class="text-danger">*</span> </label>
                                            <asp:TextBox ID="txtRevision" class="form-control" runat="server">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Type <span class="text-danger">*</span> </label>
                                            <asp:DropDownList ID="ddlType" class="form-control select2" runat="server">
                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                <asp:ListItem Value="1">Contraption</asp:ListItem>
                                                <asp:ListItem Value="2">Tools</asp:ListItem>
                                                <asp:ListItem Value="3">Resources</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Remarks <span class="text-danger">*</span> </label>
                                            <asp:TextBox ID="txtNote" class="form-control" TextMode="MultiLine" Style="resize: none"
                                                runat="server">
                                            </asp:TextBox>
                                        </div>

                                    </div>
                                    <br />

                                    <div class="row" runat="server" visible="false">

                                        <div class="col-md-2">
                                            <label class="control-label">SAP (supplier number)</label>
                                            <asp:TextBox ID="txtSuppSAPCode" class="form-control" runat="server" Text="NA">
                                            </asp:TextBox>
                                        </div>

                                        <%--Catagery of tools--%>


                                        <div class="col-md-2">
                                            <label class="control-label">BDM Number</label>
                                            <asp:TextBox ID="txtBDMNo" class="form-control" runat="server" Text="NA"></asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Released</label>
                                            <asp:RadioButtonList ID="rdbReleased" class="form-control" runat="server"
                                                RepeatColumns="4">
                                                <asp:ListItem Value="1" Text="Yes" style="padding-right: 15px" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>



                                        <div class="col-md-2">
                                            <label class="control-label">Amount of stations<span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtAmtStations" class="form-control" runat="server" Text="0">
                                            </asp:TextBox>

                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtAmtStations"
                                                ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                            <asp:RequiredFieldValidator ControlToValidate="txtAmtStations" Display="Dynamic"
                                                ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator10"
                                                runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Amount Pro stations<span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtAmtProStations" class="form-control" runat="server" Text="0">
                                            </asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtAmtProStations"
                                                ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                            <asp:RequiredFieldValidator ControlToValidate="txtAmtProStations" Display="Dynamic"
                                                ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1"
                                                runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>

                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">Possible capacity</label>
                                            <asp:TextBox ID="txtPossibleCapa" class="form-control" runat="server" Text="NA">
                                            </asp:TextBox>
                                        </div>

                                    </div>

                                    <br />

                                    <div class="row">

                                        <div class="col-md-2" runat="server" visible="false">
                                            <label class="control-label">Availability drawings</label>
                                            <asp:TextBox ID="txtAvailabilityDraw" class="form-control" runat="server" Text="NA">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-3" runat="server" visible="false" >
                                            <label class="control-label">Comments</label>
                                            <asp:TextBox ID="txtComment" class="form-control" TextMode="MultiLine" Style="resize: none"
                                                runat="server" Text="NA">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Have Image ?</label>
                                                <br />
                                                <div class="form-control">
                                                    <asp:RadioButtonList ID="rbtCheckImg" RepeatColumns="2" runat="server">
                                                        <asp:ListItem Selected="True" Text="No" Value="No" style="padding: 20px"></asp:ListItem>
                                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Photo</label>
                                                <br />

                                                <asp:ImageMap runat="server" id="imgToolImage" src="/assets/images/NoImage.png" style="width: 110px; height: 60px; cursor: pointer;" class="img-thumbnail" ></asp:ImageMap>

                                                <%--<img src="/assets/images/NoImage.png" hidden="hidden" style="width: 110px; height: 60px; cursor: pointer;" class="img-thumbnail" id="MaterialImg" />--%>
                                                <input style="visibility: hidden" type="file" name="Img" class="form-control" onchange='sendFile(this);' id="f_UploadImage" />
                                                <asp:HiddenField ID="hidd_imgPath" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Rate and Tax Details</h3>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-2">
                                                        <label class="control-label">Required<span class="text-danger">*</span></label>
                                                        <asp:TextBox ID="txtRquQty" class="form-control" runat="server">
                                                        </asp:TextBox>

                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                                            FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtRquQty"
                                                            ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtRquQty" Display="Dynamic"
                                                            ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator3"
                                                            runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>

                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">Rate<span class="text-danger">*</span></label>
                                                        <asp:TextBox ID="txtRate" class="form-control" runat="server">
                                                        </asp:TextBox>

                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                                            FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtRate"
                                                            ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtRate" Display="Dynamic"
                                                            ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator4"
                                                            runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">Rate (Other)<span class="text-danger">*</span></label>
                                                        <asp:TextBox ID="txtRateOther" class="form-control" runat="server">
                                                        </asp:TextBox>

                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                                            FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtRateOther"
                                                            ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtRateOther" Display="Dynamic"
                                                            ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator5"
                                                            runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">CGST (%)<span class="text-danger">*</span></label>
                                                        <asp:TextBox ID="txtCGST" class="form-control" runat="server">
                                                        </asp:TextBox>

                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                                            FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtCGST"
                                                            ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtCGST" Display="Dynamic"
                                                            ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator6"
                                                            runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>

                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">SGST (%)<span class="text-danger">*</span></label>
                                                        <asp:TextBox ID="txtSGST" class="form-control" runat="server">
                                                        </asp:TextBox>

                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                                            FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtSGST"
                                                            ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtSGST" Display="Dynamic"
                                                            ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator7"
                                                            runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>

                                                    <div class="col-md-2" style="padding-bottom: 1%">
                                                        <label class="control-label">IGST (%)<span class="text-danger">*</span></label>
                                                        <asp:TextBox ID="txtIGST" class="form-control" runat="server">
                                                        </asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
                                                            FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtIGST"
                                                            ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtIGST" Display="Dynamic"
                                                            ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator8"
                                                            runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-footer">
                                                <div class="form-group">
                                                    <asp:Button ID="btnSave" class="btn btn-primary" Text="Save" runat="server" OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnBack" class="btn btn-primary" Text="Back" runat="server" OnClick="btnBack_Click" />
                                                    <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnClear_Click" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/Master/BOMMasters.js"></script>
    <script type="text/javascript">
        $('[id*=txtItemName]').on('change', function () {
            var url = "https://translation.googleapis.com/language/translate/v2?key=API_Key";
            url += "&source=" + "DE";
            url += "&target=" + "EN";
            url += "&q=" + escape($('[id*=txtItemName]').val());
            $.get(url, function (data, status) {
                $('[id*=txtItemNameTransulate]').val(data.data.translations[0].translatedText);
            });
        });
    </script>
    <%--<script type="text/javascript">  
         $(document).ready(function () {
             
             //$('[id*=txtMatNo]').on("change", function () {
             window.setInterval(function () {
                 //alert($(this).val());
                 $.ajax({
                     type: "POST",
                     url: "MstAssetItemSub.aspx/GetCTNo",
                    // data: '{id: "' + $(this).val() + '" }',
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (response) {
                          // console.log(response);
                         if (response.d != "false") {
                             //     alert("Called");
                             $('[id*=txtItemCode]').val(response.d);
                            // ShowMessageErr   or('The Material Number Already Taken');
                            // $(this).focus();
                         } else {

                         }
                     },
                     error: function (XMLHttpRequest, textStatus, errorThrown) {
                         alert("Whoops something went wrong!" + errorThrown);
                     }
                 });
             },1000);
         });
        </script> --%>


    <%--<script src="/assets/js/GoogleTransulate.js"></script>--%>
</asp:Content>
