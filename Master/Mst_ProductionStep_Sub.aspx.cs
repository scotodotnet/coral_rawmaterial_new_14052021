﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Google.API.Translate;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
public partial class Master_MstProductionStep_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionFinYear;
    string SessionUserID;
    string SessionUserName;
    string SessionIsAdmin;
    string Dept_Code_Delete = "";
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionIsAdmin = Session["IsAdmin"].ToString();
        SessionFinYear = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            if ((string)Session["TransNo"] != null)
            {
                btnSearch(sender, e);
            }
        }
    }
 
    private void btnSearch(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstProductionStep where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
        SSQL = SSQL + " and TransNo='" + Session["TransNo"].ToString() + "'";


        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            txtAssemblyStageName.SelectedValue = dt.Rows[0]["AssemblyStageCode"].ToString();
            txtProductionStep.Text = dt.Rows[0]["AssemblyName"].ToString();
            txtTransCode.Text = dt.Rows[0]["TransNo"].ToString();
            txtTransDate.Text = dt.Rows[0]["TransDate"].ToString();
            txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            RblStatus.SelectedValue = dt.Rows[0]["ActiveStatus"].ToString();
            btnSave.Text = "Update";

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            GetIPAndName getIPAndName = new GetIPAndName();
            string _IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(_IP))
            {
                _IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            string _HostName = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
            if (RblStatus.SelectedValue == null)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Active Status')", true);
                ErrFlag = true;
                return;
            }
                       
            if (txtProductionStep.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Assembly Name')", true);
                ErrFlag = true;
                return;
            }
            if (!ErrFlag)
            {

                if (btnSave.Text == "Update")
                {
                    SSQL = "";
                    SSQL = "Update MstProductionStep set TransDate='" + txtTransDate.Text + "',AssemblyStageCode='" + txtAssemblyStageName.SelectedValue + "' , AssemblyStageName='" + txtAssemblyStageName.SelectedItem.Text + "',";
                    SSQL = SSQL + "AssemblyName='" + txtProductionStep.Text + "',Remarks='" + txtRemarks.Text + "',Status='Update',ActiveStatus='" + RblStatus.SelectedValue + "',CreatedOn='" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "',";
                    SSQL = SSQL + "Host_IP='" + _IP + "',Host_Name='" + _HostName + "',UserName='" + SessionUserName + "',UserRole='" + SessionIsAdmin + "'";
                    SSQL = SSQL + " where TransNo='" + txtTransCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Details Updated Successfully')", true);
                   
                }

                if (btnSave.Text == "Save")
                {
                    DataTable dt_check = new DataTable();
                    SSQL = "";
                    SSQL = "Select * from MstProductionStep where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
                    SSQL = SSQL + " and ProductionStepName='" + txtProductionStep.Text + "'";

                    dt_check = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt_check.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Assembly Name is Already Present on Stage')", true);
                        ErrFlag = true;
                        return;
                    }
                    if (!ErrFlag)
                    {


                        //TransNo Get
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get_Update(SessionCcode, SessionLcode, "Production Step Master", SessionFinYear, "5");
                        if (Auto_Transaction_No == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                        }
                        else
                        {
                            txtTransCode.Text = Auto_Transaction_No;
                        }
                        if (!ErrFlag)
                        {

                            SSQL = "";
                            SSQL = "Insert into MstProductionStep (Ccode,Lcode,TransNo,ProductionStepName,Remarks,Status,ActiveStatus,";
                            SSQL = SSQL + " CreatedOn,Host_IP,Host_Name,UserName,UserRole) Values('" + SessionCcode + "','" + SessionLcode + "', ";
                            SSQL = SSQL + " '" + txtTransCode.Text + "','" + txtProductionStep.Text + "','" + txtRemarks.Text + "','Add',";
                            SSQL = SSQL + " '" + RblStatus.SelectedValue + "','" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "',";
                            SSQL = SSQL + " '" + _HostName + "','" + SessionUserName + "','" + SessionIsAdmin + "')";

                            objdata.RptEmployeeMultipleDetails(SSQL);

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Details Saved Successfully')", true);

                           // btnClear_Click(sender, e);
                        }
                    }
                }
                if (!ErrFlag)
                {
                    btnClear_Click(sender, e);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Error: Please Try Again')", true);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        RblStatus.ClearSelection();
        txtTransCode.Text = "";
        txtTransDate.Text = "";
        txtAssemblyStageName.ClearSelection();
        txtProductionStep.Text = "";
        txtRemarks.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Master/Mst_ProductionStep_Main.aspx");
    }
}