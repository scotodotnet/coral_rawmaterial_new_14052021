﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Mst_Ledger_Main.aspx.cs" Inherits="Mst_AccountType_Main" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom"  runat="server" Text="Add New" OnClick="btnAddNew_Click"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Ledger List</span></h3>
                                    <div class="box-tools pull-right">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12" runat="server" style="padding-top:15px">
					                        <div class="row">
					                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false" OnItemDataBound="Repeater2_ItemDataBound">
					                                <HeaderTemplate>
                                                        <table id="example" class="table table-responsive table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>S. No</th>
                                                                    <th>Ledger Name</th>
                                                                    <th>Leger Group</th>
                                                                    <th>Mobile</th>
                                                                    <th>Approval Status</th>
                                                                    <th>Mode</th>
                                                            </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td runat="server" visible="false"><%# Eval("LedgerCode")%></td>
                                                            <td><%# Eval("LedgerName")%></td>
                                                            <td><%# Eval("LedgerGrpName")%></td>
                                                            <td><%# Eval("MobileNo")%></td>
                                                            <td>
                                                                <asp:Label ID="lblStatus" runat="server" class="form-control" style="width:100%"
                                                                    text='<%# Eval("ApprovalStatus")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="btnEditGrid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                                    Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("LedgerCode")%>'>
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="btnApprovalGrid" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                                    Text="" OnCommand="GridApprovalEnquiryClick" CommandArgument="Approval" CommandName='<%# Eval("LedgerCode")%>'>
                                                                </asp:LinkButton>
                                                                
                                                                <asp:LinkButton ID="btnViewGrid" class="btn btn-primary btn-sm fa fa-list"  runat="server" 
                                                                    Text="" OnCommand="GridViewEnquiryClick" CommandArgument="View" CommandName='<%# Eval("LedgerCode")%>'>
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                    Text="" CommandArgument="Delete" OnCommand="GridDeleteEnquiryClick" CommandName='<%# Eval("LedgerCode")%>' 
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Ledger details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>                                
					                            </asp:Repeater>
					                        </div>
					                    </div><!-- /.table -->
                                    </div><!-- /.mail-box-messages -->
                                </div>
                                <div class="box-footer no-padding">
                                    <div class="box-footer center-block">
                                        <asp:Button ID="btnUpload" class="btn btn-primary"  runat="server" Text="Ledgers Upload" OnClick="btnUpload_Click"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

