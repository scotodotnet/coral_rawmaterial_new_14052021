﻿using Altius.BusinessAccessLayer.BALDataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class GeneratorModel : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionFinYearCode = Session["FinYearCode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            Load_Data();
        }
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status ='ADD'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Generator Model Master");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Edit...');", true);
        }

        if (!ErrFlag)
        {
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("GenID");
            Session["GenID"] = Enquiry_No_Str;
            Response.Redirect("/Master/GeneratorModelSub.aspx");
        }       
    }

    protected void btnDeleteIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        
        //Right Checking 

        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Generator Model Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Delete...');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Update GeneratorModels set Status='Delete' where id='" + e.CommandName.ToString() + "' and Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " and Lcode='" + SessionLcode + "'";

            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data();
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Generator Model Master");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Add...');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Generator Model..');", true);
        }
        else
        {
            Session.Remove("GenID");
            Response.Redirect("/Master/GeneratorModelSub.aspx");
        }
    }
}