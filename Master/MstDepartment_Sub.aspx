﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstDepartment_Sub.aspx.cs" Inherits="MstDepartment_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upDeptSub" runat="server">
            <ContentTemplate>
                <%--header--%>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Department Master</h1>
                </section>

                <%--Body--%>
                <section class="content">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3" runat="server" visible="false">
                                        <div class="form-group">
                                            <label for="exampleInputName">Department Code<span class="text-danger">*</span> </label>
                                            <asp:Label ID="txtDeptCode" runat="server" class="form-control" />
                                            <span id="DeptCode" class="text-danger"></span>
                                            <asp:RequiredFieldValidator ControlToValidate="txtDeptCode" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputName">Department Name  <span class="text-danger">*</span> </label>
                                            <asp:TextBox ID="txtDeptName" runat="server" class="form-control" />
                                            <span id="DeptName" class="text-danger"></span>
                                            <asp:RequiredFieldValidator ControlToValidate="txtDeptName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col-md-6" style="padding-top: 25px">
                                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>

