﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_MstDepartment : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Department";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Data();

            //string SSQL = "Select Max(Id) from MstDepartment";
            //DataTable DT = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (DT.Rows.Count>0)
            //{

            //}
        }

        Load_Data();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Department Code Check

        SSQL = "Select * from MstDepartment where DeptCode='" + txtDeptCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count>0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Department Code Is Already.....');", true);
        }

        //duplicate Department Name Check

        //SSQL = "Select * from MstDepartment where DeptName='" + txtDeptName.Text + "'";
        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //if (SaveMode != "Error")
        //{
        //    if (DT.Rows.Count > 0)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Department Name Is Already.....');", true);
        //    }
        //}

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstDepartment set DeptName='" + txtDeptName.Text + "' where Ccode='" + SessionCcode + "' and ";
                SSQL = SSQL + " Lcode ='" + SessionLcode + "' and DeptCode='" + txtDeptCode.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstDepartment(Ccode,Lcode,DeptCode,DeptName,Status,UserID,UserName,CreateOn)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtDeptCode.Text.ToUpper() + "',";
                SSQL = SSQL + "'" + txtDeptName.Text + "','','" + SessionUserID + "','" + SessionUserName + "',Getdate())";
                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtDeptCode.Enabled = true;
            Load_Data();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtDeptCode.ReadOnly = false;
        txtDeptCode.Text = "";
        txtDeptName.Text = "";
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        GrdDepartment.DataSource = DT;
        GrdDepartment.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {

        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        bool Rights_Check = false;
        string SaveMode = "Update";

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Department Details...');", true);
        }

        if (SaveMode == "Update")
        {
            SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " DeptCode ='" + e.CommandName.ToString() + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            txtDeptCode.Text = DT.Rows[0]["DeptCode"].ToString();
            txtDeptCode.ReadOnly = true;

            txtDeptName.Text = DT.Rows[0]["DeptName"].ToString();

            btnSave.Text = "Update";
        }
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Department...');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                SSQL = "Update MstDepartment set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " DeptCode ='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();
            }
        }
    }
}
