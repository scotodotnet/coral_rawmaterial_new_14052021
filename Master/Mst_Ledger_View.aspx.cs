﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Mst_Ledger_View : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionLedgerCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Ledger";

            Load_AccountType();
            Load_Department();
            Load_Designation();
            Load_Bank();

            if (Session["LedgerCode"] == null)
            {
                SessionLedgerCode = "";
            }
            else
            {
                SessionLedgerCode = Session["LedgerCode"].ToString();
                txtLedgerCode.Text = SessionLedgerCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }

    private void Load_Department()
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();

        //SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " Status = 'Add'";

        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //ddlDepartment.DataSource = DT;

        //DataRow dr = DT.NewRow();

        //dr["DeptCode"] = "-Select-";
        //dr["DeptName"] = "-Select-";

        //DT.Rows.InsertAt(dr, 0);
        //ddlDepartment.DataValueField = "DeptCode";
        //ddlDepartment.DataTextField = "DeptName";
        //ddlDepartment.DataBind();
    }

    private void Load_Designation()
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();

        //SSQL = "Select DesigCode,DesigName from MstDesignation where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " Status = 'Add'";

        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //ddlDesignation.DataSource = DT;

        //DataRow dr = DT.NewRow();

        //dr["DesigCode"] = "-Select-";
        //dr["DesigName"] = "-Select-";

        //DT.Rows.InsertAt(dr, 0);
        //ddlDesignation.DataValueField = "DesigCode";
        //ddlDesignation.DataTextField = "DesigName";
        //ddlDesignation.DataBind();
    }

    private void Load_Bank()
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();

        //SSQL = "Select BankCode,BankName from MstBank where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " Status = 'Add'";

        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //ddlBankName.DataSource = DT;

        //DataRow dr = DT.NewRow();

        //dr["BankCode"] = "-Select-";
        //dr["BankName"] = "-Select-";

        //DT.Rows.InsertAt(dr, 0);
        //ddlBankName.DataValueField = "BankCode";
        //ddlBankName.DataTextField = "BankName";
        //ddlBankName.DataBind();
    }


    private void Load_AccountType()
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();

        //SSQL = "Select AccTypeCode,AccTypeName from Acc_Mst_AccountType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " Status<>'Delete'";

        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //ddlAccType.DataSource = DT;

        //DataRow dr = DT.NewRow();

        //dr["AccTypeCode"] = "-Select-";
        //dr["AccTypeName"] = "-Select-";

        //DT.Rows.InsertAt(dr, 0);
        //ddlAccType.DataValueField = "AccTypeCode";
        //ddlAccType.DataTextField = "AccTypeName";
        //ddlAccType.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
       
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        
    }
  

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete' And ";
        SSQL = SSQL + " LedgerCode='" + txtLedgerCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtLedgerName.Text = DT.Rows[0]["LedgerName"].ToString();
            
            ddlLedgerGrp.Text = DT.Rows[0]["LedgerGrpName"].ToString();
            ddlAccType.Text = DT.Rows[0]["AccountType"].ToString();
            
            txtHouseNumber.Text = DT.Rows[0]["HouseNo"].ToString();
            txtAddr1.Text = DT.Rows[0]["Add1"].ToString();
            txtAddr2.Text = DT.Rows[0]["Add2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtPincode.Text = DT.Rows[0]["PinCode"].ToString();
            txtState.Text = DT.Rows[0]["State"].ToString();
            txtCountry.Text = DT.Rows[0]["Country"].ToString();
            txtStdCode.Text = DT.Rows[0]["Std_Code"].ToString();
            txtPhone.Text = DT.Rows[0]["TelNo"].ToString();
            txtMobiCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtMobile.Text = DT.Rows[0]["MobileNo"].ToString();
            txtMail.Text = DT.Rows[0]["MailID"].ToString();

            if (DT.Rows[0]["LedgerGrpName"].ToString() == "Employee")
            {
                pnlEmp.Visible = true;
                pnlOutTransaction.Visible = false;

                ddlDepartment.Text = DT.Rows[0]["DeptName"].ToString();
                ddlDesignation.Text = DT.Rows[0]["DesignationName"].ToString();
                txtAadharNo.Text = DT.Rows[0]["AadharNo"].ToString();
            }
            else
            {
                pnlOutTransaction.Visible = true;
                pnlEmp.Visible = false;

                txtTransport.Text = DT.Rows[0]["TransPort"].ToString();
                txtZone.Text = DT.Rows[0]["Zone"].ToString();
                txtGstin.Text = DT.Rows[0]["GSTNo"].ToString();
                txtNotes.Text = DT.Rows[0]["Description"].ToString();
            }

            ddlBankName.Text = DT.Rows[0]["BankName"].ToString();
            txtBranchName.Text = DT.Rows[0]["BranchName"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();
            txtAccountNo.Text= DT.Rows[0]["AccountNo"].ToString();

            btnSave.Text = "Update";
        }
       
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Mst_Ledger_Main.aspx");
    }

    protected void ddlLedgerGrp_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if(ddlLedgerGrp.SelectedItem.Text== "Employee")
        //{
        //    pnlOutTransaction.Visible = false;
        //    pnlEmp.Visible = true;
        //}
        //else
        //{
        //    pnlEmp.Visible = false;
        //    pnlOutTransaction.Visible = true;
        //}
    }

    protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();

        //SSQL = "Select * from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status = 'Add' And ";
        //SSQL = SSQL + " BankCode='" + ddlBankName.SelectedValue + "'";

        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //if (DT.Rows.Count != 0)
        //{
        //    txtBranchName.Text = DT.Rows[0]["Branch"].ToString();
        //    txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();

        //}
    }
}
