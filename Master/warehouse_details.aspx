﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="warehouse_details.aspx.cs" Inherits="master_forms_warehouse_details" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        
        <section class="content-header">
            <h1>
                <i class="fa fa-building text-primary"></i> Ware House
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- Default box -->
                    <%--<div class="box box-primary">
                        <div class="box-body">--%>
                           <%-- <div class="nav-tabs-custom">--%>
                              <%--  <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab">Ware House Details</a></li>
                                    <li><a href="#tab_2" data-toggle="tab">Ware House Rack Details</a></li>
                                    <li><a href="#tab_3" data-toggle="tab">Ware House Additional Rack Details</a></li>
                                </ul>--%>
                              <%--  <div class="tab-content">--%>
                                   <%-- <div class="tab-pane active" id="tab_1">--%>
                    <div class="box box-primary">
                        <div class="box-body">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class="content-heading-anchor">
                                                    <h4>Ware House Master</h4>
                                                    <div class="fc-content-skeleton">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Warehouse Code <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtWarehouseCode" MaxLength="30" class="form-control" AutoComplete="off" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtWarehouseCode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Warehouse Name <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtWarehouseName" class="form-control" Style="text-transform: uppercase" AutoComplete="off" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtWarehouseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                          
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Description </label>
                                                                    <asp:TextBox ID="txtDescription" TextMode="MultiLine" Style="resize: none" AutoComplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Button start -->
                                                        <div class="form-group" align="center">
                                                            <asp:Button ID="btnSave" class="btn btn-primary" CausesValidation="False" ValidationGroup="Validate_Field" runat="server" Text="Save" OnClick="btnSave_Click" />
                                                            <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                                        </div>
                                                        <!-- Button End -->
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="table-responsive mailbox-messages">
                                                                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                                        <HeaderTemplate>
                                                                            <table class="table table-responsive table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Warehouse Code</th>
                                                                                        <th>Warehouse Name</th>
                                                                                        <th>Description</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Eval("WarehouseCode")%></td>
                                                                                <td><%# Eval("WarehouseName")%></td>
                                                                                <td><%# Eval("Description")%></td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="btnEditGrid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                                        Text="" OnCommand="GridEditClick" CommandArgument='Edit' CommandName='<%# Eval("WarehouseCode")%>'>
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("WarehouseCode")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure want to delete this warehouse details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click"/>
                                                 <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="click"/>
                                            </Triggers>
                                        </asp:UpdatePanel>
                            </div>
                        </div>
                                   <%-- </div>--%>
                                   <%-- <div class="tab-pane" id="tab_2">--%>
                    <div class="box box-primary">
                        <div class="box-body">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" >
                                            <ContentTemplate>
                                                <div class="content-heading-anchor">
                                                    <h4>Ware House Rack Master</h4>
                                                    <div class="fc-content-skeleton">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Warehouse Name <span class="text-danger">*</span></label>
                                                                    <asp:DropDownList ID="ddlWareHouse" class="form-control select2" runat="server"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ControlToValidate="ddlWareHouse" InitialValue="-Select-" ValidationGroup="Validate_Field_Rack" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">From Rack <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtRackFrom" class="form-control" OnTextChanged="txtRackFrom_TextChanged" AutoComplete="off" AutoPostBack="true" Text="0" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtRackFrom" ValidationGroup="Validate_Field_Rack" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtRackFrom" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">To Rack <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtRackTo" class="form-control" runat="server" Text="0" AutoPostBack="true" AutoComplete="off" OnTextChanged="txtRackTo_TextChanged"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtRackTo" ValidationGroup="Validate_Field_Rack" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtRackTo" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Total Rack <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtTotalRack" class="form-control" runat="server" Text="0" AutoComplete="off" Enabled="false"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtTotalRack" ValidationGroup="Validate_Field_Rack" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtTotalRack" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Rack Qty <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtRackQty" class="form-control" runat="server" Text="0" AutoComplete="off" AutoPostBack="true" OnTextChanged="txtRackQty_TextChanged"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtRackQty" ValidationGroup="Validate_Field_Rack" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtRackQty" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Total Rack Qty <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtTotalRackQty" class="form-control" runat="server" Text="0" AutoComplete="off"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtTotalRackQty" ValidationGroup="Validate_Field_Rack" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtTotalRackQty" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Rack Serious <span class="text-danger">*</span> </label>
                                                                    <asp:TextBox ID="txtRackSerious" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtRackSerious" ValidationGroup="Validate_Field_Rack" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Description </label>
                                                                    <asp:TextBox ID="txtRackDescription" TextMode="MultiLine" AutoComplete="off" Style="resize: none" class="form-control" runat="server"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Button start -->
                                                        <div class="form-group" align="center">
                                                            <asp:Button ID="btnRackSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field_Rack" OnClick="btnRackSave_Click" />
                                                            <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                                            <asp:Button ID="btnRackClean" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnRackClean_Click" />
                                                        </div>
                                                        <!-- Button End -->
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="table-responsive mailbox-messages">
                                                                    <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                                        <HeaderTemplate>
                                                                            <table class="table table-responsive table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Warehouse Code</th>
                                                                                        <th>Warehouse Name</th>
                                                                                        <th>Rack Serious</th>
                                                                                        <th>From Rack</th>
                                                                                        <th>To Rack</th>
                                                                                        <th>Rack Qty</th>
                                                                                        <th>Total Rack</th>
                                                                                        <th>Total Rack Qty</th>
                                                                                        <th>Description</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Eval("WarehouseCode")%></td>
                                                                                <td><%# Eval("WarehouseName")%></td>
                                                                                <td><%# Eval("RackSerious")%></td>
                                                                                <td><%# Eval("FromRack")%></td>
                                                                                <td><%# Eval("ToRack")%></td>
                                                                                <td><%# Eval("RackQty")%></td>
                                                                                <td><%# Eval("TotalRack")%></td>
                                                                                <td><%# Eval("TotalRackQty")%></td>
                                                                                <td><%# Eval("Description")%></td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="btnEditGrid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                                        Text="" OnCommand="btnEditGrid_Command" CommandArgument='<%# Eval("RackSerious")%>' CommandName='<%# Eval("WarehouseCode")%>'>
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="btnDeleteGrid_Command" CommandArgument='<%# Eval("RackSerious")%>' CommandName='<%# Eval("WarehouseCode")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure want to delete this warehouse details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnRackSave" EventName="click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnRackClean" EventName="click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                            </div>
                        </div>
                                  <%--  </div>--%>
                                  <%--  <div class="tab-pane" id="tab_3">--%>
                    <div class="box box-primary">
                        <div class="box-body">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <div class="content-heading-anchor">
                                                    <h4>Ware House Additional Rack Master</h4>
                                                    <div class="fc-content-skeleton">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Warehouse Name <span class="text-danger">*</span></label>
                                                                    <asp:DropDownList ID="ddlAddWarehouseName" runat="server" class="form-control select2" OnSelectedIndexChanged="ddlAddWarehouseName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ControlToValidate="ddlAddWarehouseName" InitialValue="-Select-" ValidationGroup="Validate_Field_AddRack" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Rack Serious <span class="text-danger">*</span></label>
                                                                    <asp:DropDownList ID="ddlRackSerious" runat="server" class="form-control select2"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ControlToValidate="ddlRackSerious" InitialValue="-Select-" ValidationGroup="Validate_Field_AddRack" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Rack Location <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtRackLocation" runat="server" Class="form-control" AutoComplete="off"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtRackLocation" ValidationGroup="Validate_Field_AddRack" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Rack Name <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtAddRackName" runat="server" Class="form-control" AutoComplete="off"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtAddRackName" ValidationGroup="Validate_Field_AddRack" class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Rack Qty <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtAddRackQty" runat="server" Text="0" Class="form-control" AutoComplete="off"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtAddRackQty" ValidationGroup="Validate_Field_AddRack" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Description </label>
                                                                    <asp:TextBox ID="txtAddRackDescription" TextMode="MultiLine" Style="resize: none" class="form-control" runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Button start -->
                                                        <div class="form-group" align="center">
                                                            <asp:Button ID="btnAddRackSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field_AddRack" OnClick="btnAddRackSave_Click" />
                                                            <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                                            <asp:Button ID="btnAddRackClr" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnAddRackClr_Click" />
                                                        </div>
                                                        <!-- Button End -->
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="table-responsive mailbox-messages">
                                                                    <asp:Repeater ID="Repeater3" runat="server" EnableViewState="false">
                                                                        <HeaderTemplate>
                                                                            <table class="table table-responsive table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Warehouse Code</th>
                                                                                        <th>Warehouse Name</th>
                                                                                        <th>Rack Serious</th>
                                                                                        <th>Rack Name</th>
                                                                                        <th>Rack Location</th>
                                                                                        <th>Rack Qty</th>
                                                                                        <th>Description</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Eval("WarehouseCode")%></td>
                                                                                <td><%# Eval("WarehouseName")%></td>
                                                                                <td><%# Eval("RackSerious")%></td>
                                                                                <td><%# Eval("RackName")%></td>
                                                                                <td><%# Eval("ItemLocation")%></td>
                                                                                <td><%# Eval("RackQty")%></td>
                                                                                <td><%# Eval("Description")%></td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="btnAddRackEditGrid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                                        Text="" OnCommand="btnAddRackEditGrid_Command" CommandArgument='<%# Eval("RackSerious")+","+Eval("RackName")%>' CommandName='<%# Eval("WarehouseCode")+","+Eval("ItemLocation")%>'>
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnAddRackDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="btnAddRackDeleteGrid_Command" CommandArgument='<%#  Eval("RackSerious")+","+Eval("RackName")%>' CommandName='<%# Eval("WarehouseCode")+","+Eval("ItemLocation")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure want to delete this warehouse details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                             <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnAddRackSave" EventName="click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnAddRackClr" EventName="click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                            </div>
                        </div>
                                    <%--</div>--%>
                                <%--</div>--%>
                          <%--  </div>--%>
                       <%-- </div>
                        <!-- /.box-body -->
                    </div>--%>
                    <!-- /.box -->
                </div>
                <!-- /.col-md-6-->
            </div>
            <!--/.row-->
            <div class="col-md-3" hidden>
                <div class="callout callout-info" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-info"></i>Info:</h4>
                    <p>
                    </p>
                </div>
            </div>
        </section>
        <!-- /.content -->
                 <!-- /.content -->
           
    </div>
    <!-- jQuery 3 -->
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
     <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy'});
                    $('.select2').select2();
                }
            });
        };
    </script>   
</asp:Content>
