﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManPowerRequired.aspx.cs" Inherits="Master_ManPowerRequired" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-hand-o-up text-primary"></i>Man Power Required
            </h1>
        </section>
        <!-- Main content -->
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Generator Model <span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddlGenerator" AutoPostBack="true" OnSelectedIndexChanged="ddlGenerator_SelectedIndexChanged" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlGenerator" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Production Part Name <span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddlProduct" CssClass="form-control select2" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="txtRequiredQty" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Required Qty <span class="text-danger">*</span></label>
                                                <asp:TextBox ID="txtRequiredQty" class="form-control " runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtRequiredQty" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtRequiredQty" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Required Days To Complete <span class="text-danger">*</span></label>
                                                <asp:TextBox ID="txtTotalDays" class="form-control " runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtTotalDays" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtTotalDays" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Date From <span class="text-danger">*</span></label>
                                                <asp:TextBox ID="txtFromDate" class="form-control datepicker" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtFromDate" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Date To <span class="text-danger">*</span></label>
                                                <asp:TextBox ID="txtTODate" class="form-control datepicker" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtTODate" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtTODate" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
                                            <asp:Repeater ID="RptrManPowerRequired" runat="server">
                                                <HeaderTemplate>
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>S. No</th>
                                                                <th>Generator Model</th>
                                                                <th>Product</th>
                                                                <th>Required Persons</th>
                                                                <th>Required Days</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("GenModelNo")%></td>
                                                        <td><%# Eval("ProductionPartNo")%></td>
                                                        <td><%# Eval("PersonsRequired")%></td>
                                                        <td><%# Eval("RequiredDays")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="btnEditGrid_Command" CommandArgument="Edit" CommandName='<%# Eval("MRQid")%>'>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" CommandArgument="Delete" OnCommand="btnDeleteGrid_Command" CommandName='<%# Eval("MRQid")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" ValidationGroup="Validate_Field" runat="server" Text="Save" />
                                        <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                                    </div>
                                </div>
                                <!-- /.box-footer-->
                            </div>
                            <!-- /.box -->
                        </div>
                        <div class="col-md-3" hidden>
                            <div class="callout callout-info" style="margin-bottom: 0!important;">
                                <h4><i class="fa fa-info"></i>Info:</h4>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- /.content -->
    </div>
</asp:Content>

