﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Mst_ProductionStep_Main.aspx.cs" Inherits="Master_MstProductionStep_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span>Production Step List</span></h3>
                                    <div class="box-tools pull-right">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater2" runat="server" EnableViewState="true">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>S. No</th>
                                                                    <th>Trans.Code</th>
                                                                    <th>Production Step Name</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# Eval("TransNo")%></td>
                                                            <td><%# Eval("ProductionStepName")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnEditGrid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="btnEditGrid_Command" CommandArgument="Edit" CommandName='<%# Eval("TransNo")%>'>
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" CommandArgument="Delete" OnCommand="btnDeleteGrid_Command" CommandName='<%# Eval("TransNo")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Account Type details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

