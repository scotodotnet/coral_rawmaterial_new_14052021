﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Mst_AccountType_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionLedgerCode = "";
    string SessionFinYearVal = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Ledger";

            Load_AccountType();
            Load_Bank();
            Initial_Data_Contact_Details();

            if (Session["LedgerCode"] == null)
            {
                SessionLedgerCode = "";

                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = "";

                Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master_Get(SessionCcode, SessionLcode, "Supplier Master","1");

                lblLedgerCode.Text = Auto_Transaction_No;
            }
            else
            {
                SessionLedgerCode = Session["LedgerCode"].ToString();
                lblLedgerCode.Text = SessionLedgerCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }

    private void Load_Bank()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select BankCode,BankName from MstBank where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status = 'Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlBankName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["BankCode"] = "-Select-";
        dr["BankName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlBankName.DataValueField = "BankCode";
        ddlBankName.DataTextField = "BankName";
        ddlBankName.DataBind();
    }


    private void Load_AccountType()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select AccTypeCode,AccTypeName from Acc_Mst_AccountType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status<>'Delete'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlAccType.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["AccTypeCode"] = "-Select-";
        dr["AccTypeName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlAccType.DataValueField = "AccTypeCode";
        ddlAccType.DataTextField = "AccTypeName";
        ddlAccType.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        string MSME = "1";

        //if (chkAct.Checked == true)
        //{
        //    MSME = "1";
        //}
        //else { MSME = "0"; }

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string HostName = getIPAndName.GetName();

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from Acc_Mst_Ledger Where LedgerName='" + txtLedgerName.Text + "' And Ccode='" + SessionCcode + "' And ";
            SSQL = SSQL + " Lcode ='" + SessionLcode + "' And LedgerGrpName='" + ddlLedgerGrp.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
            }
        }

        //Auto NumberGen

        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = "";
                if (ddlLedgerGrp.SelectedItem.Text == "Supplier" || ddlLedgerGrp.SelectedItem.Text == "General")
                {
                    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Supplier Master", SessionFinYearVal, "1");
                }
                else if (ddlLedgerGrp.SelectedItem.Text == "Customer")
                {
                    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Customer Master", SessionFinYearVal, "1");
                }
                else if (ddlLedgerGrp.SelectedItem.Text == "Employee")
                {
                    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Employee Master", SessionFinYearVal, "1");
                }

                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    lblLedgerCode.Text = Auto_Transaction_No;
                }
            }
        }


        if (!ErrFlag)
        {


            SSQL = "Delete From Acc_Mst_Ledger where Ccode='" + SessionLcode + "' and LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " LedgerCode='" + lblLedgerCode.Text + "' ";

            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Type Details Updated Successfully');", true);


            SSQL = "Insert Into Acc_Mst_Ledger(Ccode,Lcode,LedgerCode,LedgerName,AccountTypeCode,AccountType,LedgerGrpId,LedgerGrpName,";
            SSQL = SSQL + " HouseNo,Add1,Add2,City,PinCode,State,Country,Std_Code,TelNo,Mobile_Code,MobileNo,MailID,TransPort,Zone,GSTNo,";
            SSQL = SSQL + " Description,DeptId,DeptName,DesignationId,DesignationName,AadharNo,AccountNo,BankId,BankName,BranchName,IFSCCode,Status,";
            SSQL = SSQL + " UserID,UserName,CreateOn,IP,SystemName,MSME_Creditor) Values('" + SessionCcode + "','" + SessionLcode + "','" + lblLedgerCode.Text.ToUpper() + "',";
            SSQL = SSQL + " '" + txtLedgerName.Text + "','" + ddlAccType.SelectedValue + "','" + ddlAccType.SelectedItem.Text + "',";
            SSQL = SSQL + " '" + ddlLedgerGrp.SelectedValue + "','" + ddlLedgerGrp.SelectedItem.Text + "','" + txtHouseNumber.Text + "',";
            SSQL = SSQL + " '" + txtAddr1.Text + "','" + txtAddr2.Text + "','" + txtCity.Text + "','" + txtPincode.Text + "','" + txtState.Text + "',";
            //SSQL = SSQL + " '" + txtCountry.Text + "','" + txtStdCode.Text + "','" + txtPhone.Text + "','" + txtMobiCode.Text + "','" + txtMobile.Text + "',";
            //SSQL = SSQL + " '" + txtMail.Text + "','" + txtTransport.Text + "','" + txtZone.Text + "','" + txtGstin.Text + "','" + txtNotes.Text + "',";
            //SSQL = SSQL + " '" + ddlDepartment.SelectedValue + "','" + ddlDepartment.SelectedItem.Text + "','" + ddlDesignation.SelectedValue + "',";
            //SSQL = SSQL + " '" + ddlDesignation.SelectedItem.Text + "','" + txtAadharNo.Text + "','" + txtAccountNo.Text + "','" + ddlBankName.SelectedValue + "',";
            SSQL = SSQL + " '" + ddlBankName.SelectedItem.Text + "','" + txtBranchName.Text + "','" + txtIFSC.Text + "','Add',";
            SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "',Getdate(),'" + IP + "','" + HostName + "','" + MSME + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Type Details Saved Successfully');", true);

            if (btnSave.Text != "Update")
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = "";

                Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Supplier Master", SessionFinYearVal, "1");
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            Response.Redirect("Mst_Ledger_Main.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Ledger Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        lblLedgerCode.Text = "";
        txtLedgerName.Text = "";
        ddlAccType.SelectedItem.Text = "";
        txtAddr1.Text = "";
        txtAddr2.Text = "";
        txtCity.Text = "";
        txtPincode.Text = "";
        txtState.Text = "";
        txtCountry.Text = "";
        txtGstin.Text = "";
        txtMail.Text = "";
        txtMobile.Text = "";
        txtNotes.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete' And ";
        SSQL = SSQL + " LedgerCode='" + lblLedgerCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtLedgerName.Text = DT.Rows[0]["LedgerName"].ToString();
            ddlLedgerGrp.SelectedValue = DT.Rows[0]["LedgerGrpId"].ToString();
            ddlLedgerGrp.SelectedItem.Text = DT.Rows[0]["LedgerGrpName"].ToString();
            ddlAccType.SelectedItem.Text = DT.Rows[0]["AccountType"].ToString();
            ddlAccType.SelectedValue = DT.Rows[0]["AccountTypeCode"].ToString();
            txtHouseNumber.Text = DT.Rows[0]["HouseNo"].ToString();
            txtAddr1.Text = DT.Rows[0]["Add1"].ToString();
            txtAddr2.Text = DT.Rows[0]["Add2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtPincode.Text = DT.Rows[0]["PinCode"].ToString();
            txtState.Text = DT.Rows[0]["State"].ToString();
            txtCountry.Text = DT.Rows[0]["Country"].ToString();
            //txtStdCode.Text = DT.Rows[0]["Std_Code"].ToString();
            //txtPhone.Text = DT.Rows[0]["TelNo"].ToString();
            //txtMobiCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtMobile.Text = DT.Rows[0]["MobileNo"].ToString();
            txtMail.Text = DT.Rows[0]["MailID"].ToString();
 
            //txtTransport.Text = DT.Rows[0]["TransPort"].ToString();
            //txtZone.Text = DT.Rows[0]["Zone"].ToString();
            txtGstin.Text = DT.Rows[0]["GSTNo"].ToString();
            txtNotes.Text = DT.Rows[0]["Description"].ToString();

            ddlBankName.SelectedValue = DT.Rows[0]["BankId"].ToString();
            ddlBankName.SelectedItem.Text = DT.Rows[0]["BankName"].ToString();
            txtBranchName.Text = DT.Rows[0]["BranchName"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();
            txtAccountNo.Text = DT.Rows[0]["AccountNo"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Mst_Ledger_Main.aspx");
    }

    protected void ddlLedgerGrp_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status = 'Add' And ";
        SSQL = SSQL + " BankCode='" + ddlBankName.SelectedValue + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            txtBranchName.Text = DT.Rows[0]["Branch"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();

        }
    }

    private void Initial_Data_Contact_Details()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("LedgerCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ContactName", typeof(string)));
        dt.Columns.Add(new DataColumn("ContactMobile", typeof(string)));
        dt.Columns.Add(new DataColumn("ContactMail", typeof(string)));


        rptContactDet.DataSource = dt;
        rptContactDet.DataBind();
        ViewState["ContactDet"] = rptContactDet.DataSource;
    }

    protected void GridDelRMClick(object sender, CommandEventArgs e)
    {

        try
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ContactDet"];
            string Filter = e.CommandName.ToString();

            string[] Val = Filter.Split(',');

            string ContName = Val[0].ToString();
            string ContMob = Val[1].ToString();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ContactName"].ToString() == ContName.ToString() && dt.Rows[i]["ContactMobile"].ToString() == ContMob.ToString())
                {
                    dt.Rows.RemoveAt(i);
                    dt.AcceptChanges();
                }
            }
            ViewState["ContactDet"] = dt;

            rptContactDet.DataSource = dt;
            rptContactDet.DataBind();
        }
        catch (Exception ex)
        {

        }
    }
}
