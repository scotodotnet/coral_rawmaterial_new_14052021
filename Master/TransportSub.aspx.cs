﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TransportSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();

            if (!IsPostBack)
            {
                if ((string)Session["Transport_ID"] != null)
                {
                    Search(Session["Transport_ID"].ToString());
                }
            }
        }
    }
    private void Search(string id)
    {
        SSQL = "";
        SSQL = "Select * from TransportMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Transport_ID='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtTransportNo.Enabled = false;
            txtTransportNo.Text = dt.Rows[0]["TransPort_No"].ToString();
            txtTransportName.Text = dt.Rows[0]["Transport_Name"].ToString();
            txtAddress.Text = dt.Rows[0]["Address_Full"].ToString();
            txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();

            btnSave.Text = "Update";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        //if (txtTransportNo.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Transport Number')", true); ;
        //    ErrFlg = true;
        //}
        if (txtTransportName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Transport Name')", true); ;
            ErrFlg = true;
        }
        if (txtAddress.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Address')", true); ;
            ErrFlg = true;
        }

        if (!ErrFlg)
        {

            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from TransportMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and TransPort_No='" + txtTransportNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from TransportMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and TransPort_No='" + txtTransportNo.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Transport Number Already Taken')", true); ;

                    ErrFlg = true;
                    return;
                }

            }

            //Auto generate Transaction Function Call
            if (btnSave.Text != "Update")
            {
                if (!ErrFlg)
                {
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Transport Master", SessionFinYearVal, "1");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlg = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        txtTransportNo.Text = Auto_Transaction_No;
                    }
                }
            }

            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "";
                SSQL = "Insert into TransportMaster(Ccode,Lcode,Transport_Name,TransPort_No,Address_Full,Remarks,CreatedOn,Host_IP,Host_Name,UserName,";
                SSQL = SSQL + " Status)values('" + SessionCcode + "','" + SessionLcode + "' ,'" + txtTransportName.Text + "','" + txtTransportNo.Text + "',";
                SSQL = SSQL + " '" + txtAddress.Text + "','" + txtRemarks.Text + "','" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "',";
                SSQL = SSQL + "'" + _IP + "','" + _HostName + "','" + Session["UserID"] + "','Add')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                btnClear_Click(sender, e);

                Response.Redirect("/Master/TransportMain.aspx");
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAddress.Text = "";
        txtRemarks.Text = "";
        txtTransportName.Text = "";
        txtTransportNo.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("Transport_ID");
        Response.Redirect("/Master/TransportMain.aspx");
    }
}