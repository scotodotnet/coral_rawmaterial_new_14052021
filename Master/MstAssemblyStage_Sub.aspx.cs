﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Google.API.Translate;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class Master_MstAssemblyStage_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionFinYear;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionIsAdmin;
    string Dept_Code_Delete = "";
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {                                          
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionIsAdmin = Session["IsAdmin"].ToString();
        SessionFinYear = Session["FinYear"].ToString();

        if (!IsPostBack)
        {               
            if ((string)Session["AssemblyTransNo"] != null)
            {
                btnSearch(sender, e);
            }
        }
    }
    private void btnSearch(object sender, EventArgs e)
    {

        SSQL = "";
        SSQL = "Select * from MstAssetAssemblyStage where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
        SSQL = SSQL + " and TransCode='" + Session["TransCode"].ToString() + "'";

      
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            txtAssemblyStageName.Text = dt.Rows[0]["AssemblyStageName"].ToString();
            txtTransCode.Text = dt.Rows[0]["TransCode"].ToString();
            txtTransDate.Text = dt.Rows[0]["TransDate"].ToString();
            txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            RblStatus.SelectedValue = dt.Rows[0]["ActiveStatus"].ToString();
            btnSave.Text = "Update";

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        { 
        bool ErrFlag = false;
        GetIPAndName getIPAndName = new GetIPAndName();
        //string _IP = getIPAndName.GetIP();
        string _IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (string.IsNullOrEmpty(_IP))
        {
            _IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        //string _HostName = getIPAndName.GetName();
        string _HostName = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
        if (RblStatus.SelectedValue == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Active Status')", true);
            ErrFlag = true;
            return;
        }
        if (txtAssemblyStageName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Type the Assembly Stage Name')", true);
            ErrFlag = true;
            return;
        }
        //if (txtTransDate.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Date Properly')", true);
        //    ErrFlag = true;
        //    return;
        //}
            if (!ErrFlag)
            {

                DataTable dt_check = new DataTable();
                SSQL = "";
                SSQL = "Select * from MstAssetAssemblyStage where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and AssemblyStageName='" + txtAssemblyStageName.Text + "'";
                dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_check.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Stage is Already Present')", true);
                    ErrFlag = true;
                    return;
                }
                if (!ErrFlag)
                {

                    //TransNo Get
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get_Update(SessionCcode, SessionLcode, "Assembly Stage Master",SessionFinYear, "5");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        txtTransCode.Text = Auto_Transaction_No;
                    }
                    if (!ErrFlag)
                    {

                        SSQL = "";
                        SSQL = "Insert into MstAssetAssemblyStage (Ccode,Lcode,TransCode,TransDate,AssemblyStageName,Remarks,Status,ActiveStatus,CreatedOn,Host_IP,Host_Name,UserName,UserRole)";
                        SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtTransCode.Text + "','" + txtTransDate.Text + "','" + txtAssemblyStageName.Text + "','" + txtRemarks.Text + "','Add',";
                        SSQL = SSQL + "'" + RblStatus.SelectedValue + "','" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + SessionUserName + "','" + SessionIsAdmin + "')";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert(Details Saved Successfully)", true);

                        btnClear_Click(sender, e);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Error: Please Try Again')", true);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

        RblStatus.ClearSelection();
        txtTransCode.Text = "";
        txtTransDate.Text = "";
        txtAssemblyStageName.Text = "";
        txtRemarks.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Master/MstAssemblyStage_Main.aspx");
    }
       
    protected void txtAssemblyStageName_TextChanged(object sender, EventArgs e)
    {
        try
        {
          //  string translated = Translator.Translate(txtAssemblyStageName.Text, Language.English, Language.Hindi);
            //Console.WriteLine(translated); // I like running.
            //lblTransGerman.Text = translated;
        }
        catch (Exception ex)
        {

        }
        
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetListofCountries(string prefixText)

    {
        BALDataAccess objdata = new BALDataAccess();
        string SSQL = "";
        SSQL = "select AssemblyStagename from MstAssetAssemblyStage where AssemblyStagename like '" + prefixText + "%' ";
        

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //    da.Fill(dt);

        List<string> CountryNames = new List<string>();

        for (int i = 0; i < dt.Rows.Count; i++)

        {

            CountryNames.Add(dt.Rows[i]["AssemblyStagename"].ToString());

        }

        return CountryNames;

        // }

    }
}
