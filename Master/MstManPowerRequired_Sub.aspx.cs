﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstManPowerRequired_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionMPRCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

       

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Required ManPower";

            Load_Genrator();
            Load_Product();


            if (Session["MRQid"] == null)
            {
                SessionMPRCode = "";
            }
            else
            {
                SessionMPRCode = Session["MRQid"].ToString();
                //txtDeptCode.Text = SessionMPRCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
            
        }
    }
    
    private void Load_Product()
    {
        string SSQL = "";
        SSQL = "Select  (ProductNo+' ~> '+ProductName) as Text,ProductNo as Value from ProductModel where Ccode='" + SessionCcode + "' ";
        SSQL = SSQL + " and Lcode='" + SessionLcode + "' and Status!='Delete'";

        ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
        ddlProduct.DataTextField = "Text";
        ddlProduct.DataValueField = "Value";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    private void Load_Genrator()
    {
        string SSQL = "";

        SSQL = "Select (GenModelNo+' ~> '+GenModelName) as Text,GenModelNo as Value from GeneratorModels where Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";

        ddlGenerator.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlGenerator.DataTextField = "Text";
        ddlGenerator.DataValueField = "Value";
        ddlGenerator.DataBind();
        ddlGenerator.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlg = false;
        bool Rights_Check = false;


        //User  Rights
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Man Power Requirement");

        if (Rights_Check == false)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add Man Power Requirement...');", true);
        }

        if (!ErrFlg)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from RequiredManPower where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + "GenModelNo='" + ddlGenerator.SelectedValue + "' and ProductionPartNo='" + ddlProduct.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from RequiredManPower where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + "GenModelNo='" + ddlGenerator.SelectedValue + "' and ProductionPartNo='" + ddlProduct.SelectedValue + "'";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Requirement Type Already Taken')", true);
                    return;
                }
            }

            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();
                string _generatorName = "";
                string _productName = "";
                SSQL = "";
                SSQL = "select * from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and GenModelNo='" + ddlGenerator.SelectedValue + "' and Status!='Delete'";
                DataTable Dt_Generator = new DataTable();
                Dt_Generator = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Dt_Generator.Rows.Count > 0)
                {
                    _generatorName = Dt_Generator.Rows[0]["GenModelName"].ToString();
                }
                SSQL = "";
                SSQL = "select * from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ProductNo='" + ddlProduct.SelectedValue + "' and Status!='Delete'";
                DataTable Dt_Product = new DataTable();
                Dt_Product = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Dt_Product.Rows.Count > 0)
                {
                    _productName = Dt_Product.Rows[0]["GenModelName"].ToString();
                }

                SSQL = "";
                SSQL = "insert into RequiredManPower(Ccode,Lcode,GenModelNo,GenModelName,ProductionPartNo,ProductionPartName,PersonsRequired,RequiredDays,";
                SSQL = SSQL + "FromDate_Str,FromDate,ToDate_Str,ToDate,CreatedOn,Host_IP,Host_Name,UserName,Status) values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + ddlGenerator.SelectedValue + "','" + _generatorName.ToString() + "','" + ddlProduct.SelectedValue + "',";
                SSQL = SSQL + " '" + _productName.ToString() + "','" + txtRequiredQty.Text + "','" + txtTotalDays.Text + "','" + txtFromDate.Text + "',";
                SSQL = SSQL + " '" + Convert.ToDateTime(txtFromDate.Text) + "','" + txtTODate.Text + "','" + Convert.ToDateTime(txtTODate.Text) + "',";
                SSQL = SSQL + " '" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "',";
                SSQL = SSQL + " '" + _IP + "','" + _HostName + "','" + SessionUserName + "','Add')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                Clear_All_Field();

                Response.Redirect("MstManPowerRequired_Main.aspx");
            }
        }
    }
    private void Clear_All_Field()
    {
        ddlGenerator.ClearSelection();
        ddlProduct.ClearSelection();
        txtRequiredQty.Text = "0.00";
        txtTotalDays.Text = "0.00";
        txtFromDate.Text = "";
        txtTODate.Text = "";
        btnSave.Text = "Save";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from RequiredManPower where Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "' and ";
        SSQL = SSQL + " MRQid ='" + SessionMPRCode.ToString() + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            ddlGenerator.SelectedValue = DT.Rows[0]["GenModelNo"].ToString();
            ddlProduct.SelectedValue = DT.Rows[0]["ProductionPartNo"].ToString();
            txtFromDate.Text = DT.Rows[0]["FromDate_Str"].ToString();
            txtTODate.Text = DT.Rows[0]["ToDate_Str"].ToString();
            txtTotalDays.Text = DT.Rows[0]["RequiredDays"].ToString();
            txtRequiredQty.Text = DT.Rows[0]["PersonsRequired"].ToString();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstManPowerRequired_Main.aspx");
    }

    protected void ddlGenerator_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlGenerator.SelectedValue != "-Select-")
        {
            string SSQL = "";

            SSQL = "Select (ProductNo+' ~> '+ProductName) as Text,ProductNo as Value from ProductModel where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " and Lcode='" + SessionLcode + "' and Status!='Delete' and GenModelNo='" + ddlGenerator.SelectedValue + "'";

            ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
            ddlProduct.DataTextField = "Text";
            ddlProduct.DataValueField = "Value";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
        else
        {
            Load_Product();
        }
    }

    protected void ddlProduct_SelectedIndexChanged(object sender,EventArgs e)
    {

    }
}
