﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstProStep_Main.aspx.cs" Inherits="Master_MstProStep_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Main content -->
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-1">
                            <%--<a href="/Master/BOMMasterSub.aspx" class="btn btn-primary btn-block margin-bottom"><i class="fa fa-plus"></i><span> Add</span></a>--%>
                            <asp:Button ID="btnAddNew" runat="server" CssClass="btn btn-primary" OnClick="btnAddNew_Click" Text="Add New" /><br />
                            <br />
                        </div>
                        <!-- /.col -->
                    </div>
                    <!--/.row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-cubes text-primary"></i><span>Production Step</span></h3>
                                    <%--  <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div>
                            <!-- /.box-tools -->--%>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
                                            <asp:Repeater ID="Repeater1" runat="server">
                                                <HeaderTemplate>
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Production Step No</th>
                                                             <%--   <th>Generator Model</th>
                                                                <th>Production Part</th>--%>
                                                                <th>Production Step</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <th><%# Container.ItemIndex+1 %></th>
                                                        <td><%# Eval("Production_StepNo")%></td>
                                                       <%-- <td><%# Eval("GenModelName")%></td>
                                                        <td><%# Eval("ProductionPartName")%></td>--%>
                                                        <td><%# Eval("Production_StepName")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm  fa fa-pencil" runat="server"
                                                                Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("Production_StepID")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteIssueEntry_Grid" class="btn btn-danger btn-sm fa fa-trash" runat="server"
                                                                Text="" OnCommand="btnDeleteIssueEntry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Production_StepID")%>' OnClientClick="retun Confirm('Are you sure want to Delete this Item ?')">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>

                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                            <!-- /.table -->
                                        </div>
                                        <!-- /.mail-box-messages -->

                                        <div class="modal modal-primary fade" id="modalDefault">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->
                                </div>
                               <%-- <div class="box-footer no-padding">
                                    <div class="box-footer">
                                        <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-success" OnClick="btnUpload_Click" Text="Upload BOM" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" OnClick="btnReport_Click" Text="Reports" />
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- Creates the bootstrap modal where the image will appear -->
    <%-- <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body" align="center">
                    <img src="" id="imagepreview" style="width: 450px; height: 280px;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

