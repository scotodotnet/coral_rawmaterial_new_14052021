﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstCustomer : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Customer Master";

            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Data();

            //string SSQL = "Select Max(Id) from MstDeptSpareFix";
            //DataTable DT = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (DT.Rows.Count>0)
            //{

            //}




            TransactionNoGenerate TransNO = new TransactionNoGenerate();
            string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get(SessionCcode, SessionLcode, "Customer Master", SessionFinYearVal, "1");
            if (Auto_Transaction_No == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
            }
            else
            {
                txtCustomerCode.Text = Auto_Transaction_No;
            }




        }

        Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check



        if (SaveMode != "Error")
        {
            if (btnSave.Text == "Save")
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get(SessionCcode, SessionLcode, "Customer Master", SessionFinYearVal, "5");
                if (Auto_Transaction_No == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtCustomerCode.Text = Auto_Transaction_No;
                }
            }

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstCustomer set CustName='" + txtCustomerName.Text + "',Address1='" + txtAddress1.Text + "',";
                SSQL = SSQL + " Address2='" + txtAddress2.Text + "',City='" + txtCity.Text + "',Pincode='" + txtPincode.Text + "',";
                SSQL = SSQL + " State='" + txtState.Text + "',Country='" + txtCountry.Text + "',Std_Code='" + txtStdCode.Text + "',";
                SSQL = SSQL + " TelNo='" + txtTel_no.Text + "',Mobile_Code='" + txtMobile_no.Text + "',MobileNo='" + txtMobile_no.Text + "',";
                SSQL = SSQL + " TinNo='" + txttin_no.Text + "',CstNo='" + txtcst_no.Text + "',MailID='" + txtMail_Id.Text + "',";
                SSQL = SSQL + " Description='" + txtdescription.Text + "' where Ccode='" + SessionCcode + "' and ";
                SSQL = SSQL + " Lcode ='" + SessionLcode + "' and CustCode='" + txtCustomerCode.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Spare Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstCustomer(Ccode,Lcode,CustCode,CustName,Address1,Address2,City,Pincode,State,Country,";
                SSQL = SSQL + " Std_Code,TelNo,Mobile_Code,MobileNo,TinNo,CstNo,MailID,Description,Status,UserID,UserName,CreateOn)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtCustomerCode.Text.ToUpper() + "',";
                SSQL = SSQL + "'" + txtCustomerName.Text + "','" + txtAddress1.Text + "','" + txtAddress2.Text + "',";
                SSQL = SSQL + "'" + txtCity.Text + "','" + txtPincode.Text + "','" + txtState.Text + "','" + txtCountry.Text + "',";
                SSQL = SSQL + "'" + txtStdCode.Text + "','" + txtTel_no.Text + "','" + txtMblCode.Text + "','" + txtMobile_no.Text + "',";
                SSQL = SSQL + "'" + txttin_no.Text + "','" + txtcst_no.Text + "','" + txtMail_Id.Text + "','" + txtdescription.Text + "',";
                SSQL = SSQL + "'','" + SessionUserID + "','" + SessionUserName + "',Getdate())";
                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Spare Details Saved Successfully');", true);
            }

            if (btnSave.Text == "Save")
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Get_Update(SessionCcode, SessionLcode, "Customer Master", SessionFinYearVal, "5");
                if (Auto_Transaction_No == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtCustomerCode.Text = Auto_Transaction_No;
                }
            }

            Clear_All_Field();

            TransactionNoGenerate TransNO_New = new TransactionNoGenerate();
            string Auto_Transaction_No_New = TransNO_New.Auto_Generate_No_Numbering_Setup_Get(SessionCcode, SessionLcode, "Customer Master", SessionFinYearVal, "5");
            if (Auto_Transaction_No_New == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
            }
            else
            {
                txtCustomerCode.Text = Auto_Transaction_No_New;
            }

            btnSave.Text = "Save";
            //txtCustomerCode.Enabled = true;
            Load_Data();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Spare Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        //txtCustomerCode.ReadOnly = false;
        txtCustomerCode.Text = "";
        txtCustomerName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtCity.Text = "";
        txtPincode.Text = "";
        txtState.Text = "";
        txtCountry.Text = "";
        txtStdCode.Text = "";
        txtTel_no.Text = "";
        txtMblCode.Text = "";
        txtMobile_no.Text = "";
        txttin_no.Text = "";
        txtcst_no.Text = "";
        txtMail_Id.Text = "";
        txtdescription.Text = "";
        txtRefno.Text = "";
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select * from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {

        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        bool Rights_Check = false;
        string SaveMode = "Update";

        //Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");

        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Department Details...');", true);
        //}

        if (SaveMode == "Update")
        {
            SSQL = "Select CustCode,CustName,Address1,Address2,City,Pincode,State,Country,Std_Code,TelNo,Mobile_Code,MobileNo,TinNo,CstNo,";
            SSQL = SSQL + " MailID,Description,UserID,UserName from MstCustomer where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And CustCode ='" + e.CommandName.ToString() + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            txtCustomerCode.Text = DT.Rows[0]["CustCode"].ToString();
            //txtCustomerCode.ReadOnly = true;

            txtCustomerName.Text = DT.Rows[0]["CustName"].ToString();

            txtAddress1.Text = DT.Rows[0]["Address1"].ToString();
            txtAddress2.Text = DT.Rows[0]["Address2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtPincode.Text = DT.Rows[0]["Pincode"].ToString();
            txtState.Text = DT.Rows[0]["State"].ToString();
            txtCountry.Text = DT.Rows[0]["Country"].ToString();
            txtStdCode.Text = DT.Rows[0]["Std_Code"].ToString();
            txtTel_no.Text = DT.Rows[0]["TelNo"].ToString();
            txtMblCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtMobile_no.Text = DT.Rows[0]["MobileNo"].ToString();
            txttin_no.Text = DT.Rows[0]["TinNo"].ToString();
            txtcst_no.Text = DT.Rows[0]["CstNo"].ToString();

            txtMail_Id.Text = DT.Rows[0]["MailID"].ToString();
            txtdescription.Text = DT.Rows[0]["Description"].ToString();
            btnSave.Text = "Update";
        }
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "UOM");

        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete UOM...');", true);
        //}

        if (!ErrFlag)
        {
            SSQL = "Select * from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " CustCode ='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                SSQL = "Update MstCustomer set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " CustCode ='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Customer Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();
            }
        }
    }
}
