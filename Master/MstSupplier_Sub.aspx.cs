﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstSupplier_Sub_Old : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearVal;
    string Dept_Code_Delete = "";
    string SessionLedgerCode = "";
    string GenModNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        SessionUserID = Session["UserId"].ToString();     

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Supplier";

            Initial_Data_Referesh();

            Load_AccountType();
            Load_AccountGroup();
            Load_Bank();
            Load_Data_Empty_Generator_Model();
            Load_Data_Empty_Production_Part_Name();
            Load_Data_Empty_ItemCode();
            ddlPurType_SelectedIndexChanged(sender, e);
            Load_Data_Empty_SAPNo();

            if (Session["LedgerCode"] == null)
            {
                SessionLedgerCode = "";
            }
            else
            {
                SessionLedgerCode = Session["LedgerCode"].ToString();
                txtLedgerCode.Text = SessionLedgerCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }

    private void Load_Bank()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select BankCode,BankName from MstBank where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status = 'Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlBankName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["BankCode"] = "-Select-";
        dr["BankName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlBankName.DataValueField = "BankCode";
        ddlBankName.DataTextField = "BankName";
        ddlBankName.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ModelCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelName", typeof(string)));
        dt.Columns.Add(new DataColumn("ProductionPartCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ProductionPartName", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        rpBOMLIST.DataSource = dt;
        rpBOMLIST.DataBind();
        ViewState["ItemTable"] = dt;

        //dt = Repeater1.DataSource;
    }

    private void Load_AccountType()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select AccTypeCode,AccTypeName from Acc_Mst_AccountType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status='Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlAccType.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["AccTypeCode"] = "-Select-";
        dr["AccTypeName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlAccType.DataValueField = "AccTypeCode";
        ddlAccType.DataTextField = "AccTypeName";
        ddlAccType.DataBind();
    }

    private void Load_AccountGroup()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select AccGroupCode,AccGroupName from Acc_Mst_AccountGroup where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " Lcode ='" + SessionLcode + "' And  Status='Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlLedgerGrp.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["AccGroupCode"] = "-Select-";
        dr["AccGroupName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlLedgerGrp.DataValueField = "AccGroupCode";
        ddlLedgerGrp.DataTextField = "AccGroupName";
        ddlLedgerGrp.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;


        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string HostName = getIPAndName.GetName();

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from Acc_Mst_Ledger Where LedgerName='" + txtLedgerName.Text + "' And Ccode='" + SessionCcode + "' And ";
            SSQL = SSQL + " Lcode ='" + SessionLcode + "' And LedgerGrpName='Supplier' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Already Exisit...');", true);
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Supplier Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtLedgerCode.Text = Auto_Transaction_No;
                }
            }
        }

        

        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update Acc_Mst_Ledger set LedgerName='" + txtLedgerName.Text + "',AccountTypeCode='"+ ddlAccType.SelectedValue +"',";
                SSQL = SSQL + " AccountType='" + ddlAccType.SelectedItem.Text + "',AccGrpCode='" + ddlLedgerGrp.SelectedValue + "',";
                SSQL = SSQL + " AccGrpName='" + ddlLedgerGrp.SelectedItem.Text + "',LedgerGrpId ='1',LedgerGrpName='Supplier', ";
                SSQL = SSQL + " HouseNo='" + txtHouseNumber.Text + "',Add1='" + txtAddr1.Text + "',Add2='" + txtAddr2.Text + "',city='" + txtCity.Text + "',";
                SSQL = SSQL + " PinCode ='" + txtPincode.Text + "',State='" + txtState.Text + "',Country='" + txtCountry.Text + "',Std_Code='" + txtStdCode.Text + "',";
                SSQL = SSQL + " TelNo='" + txtPhone.Text + "',Mobile_Code='" + txtMobiCode.Text + "',MobileNo='" + txtMobile.Text + "',";
                SSQL = SSQL + " MailID='" + txtMail.Text + "',TransPort='" + txtTransport.Text + "',Zone='" + txtZone.Text + "',GSTNo='" + txtGstin.Text + "',";
                SSQL = SSQL + " Description ='" + txtNotes.Text + "', AccountNo='" + txtAccountNo.Text + "',BankId='" + ddlBankName.SelectedValue + "',";
                SSQL = SSQL + " Website ='" + txtWebSite.Text + "',BankName='" + ddlBankName.SelectedItem.Text + "',";
                SSQL = SSQL + " BranchName='" + txtBranchName.Text + "',IFSCCode='" + txtIFSC.Text + "',TC='" + txtTC.Text + "',";
                SSQL = SSQL + " PackingTerms='" + txtPackTerms.Text + "',Quality='" + txtQuality.Text + "',Warranty='" + txtWarranty.Text + "' ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And  ";
                SSQL = SSQL + " Lcode ='" + SessionLcode + "' And LedgerCode='" + txtLedgerCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Type Details Updated Successfully');", true);
            }
            else
            {
                SSQL = "Insert Into Acc_Mst_Ledger(Ccode,Lcode,LedgerCode,LedgerName,AccountTypeCode,AccountType,AccGrpCode,AccGrpName,";
                SSQL = SSQL + " LedgerGrpId,LedgerGrpName,HouseNo, Add1,Add2,City,PinCode,State,Country,Std_Code,TelNo,Mobile_Code,";
                SSQL = SSQL + " MobileNo,MailID,TransPort,Zone,GSTNo,Description,AccountNo,BankId,BankName,BranchName,IFSCCode,";
                SSQL = SSQL + " Status,UserID,UserName,CreateOn,IP,SystemName,Website,DeptId,DeptName,DesignationId,DesignationName,";
                SSQL = SSQL + " AadharNo,TC,PackingTerms,Quality,Warranty)Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + txtLedgerCode.Text + "','" + txtLedgerName.Text + "','" + ddlAccType.SelectedValue + "',";
                SSQL = SSQL + " '" + ddlAccType.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + ddlLedgerGrp.SelectedValue + "','" + ddlLedgerGrp.SelectedItem.Text + "','1','Supplier',";
                SSQL = SSQL + " '" + txtHouseNumber.Text + "','" + txtAddr1.Text + "','" + txtAddr2.Text + "',";
                SSQL = SSQL + " '" + txtCity.Text + "','" + txtPincode.Text + "','" + txtState.Text + "','" + txtCountry.Text + "',";
                SSQL = SSQL + " '" + txtStdCode.Text + "','" + txtPhone.Text + "','" + txtMobiCode.Text + "','" + txtMobile.Text + "',";
                SSQL = SSQL + " '" + txtMail.Text + "','" + txtTransport.Text + "','" + txtZone.Text + "','" + txtGstin.Text + "',";
                SSQL = SSQL + " '" + txtNotes.Text + "','" + txtAccountNo.Text + "','" + ddlBankName.SelectedValue + "',";
                SSQL = SSQL + " '" + ddlBankName.SelectedItem.Text + "','" + txtBranchName.Text + "','" + txtIFSC.Text + "','Add',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "',Getdate(),'" + IP + "','" + HostName + "',";
                SSQL = SSQL + " '" + txtWebSite.Text + "','','','','','','" + txtTC.Text + "','" + txtPackTerms.Text + "',";
                SSQL = SSQL + " '" + txtQuality.Text + "','" + txtWarranty.Text + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Type Details Saved Successfully');", true);
            }

            SSQL = "Select * from Acc_Mst_Ledger_BOMList where LedgerName='" + txtLedgerName.Text + "' And Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " and Lcode='" + SessionLcode + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                SSQL = "Delete from Acc_Mst_Ledger_BOMList where LedgerName='" + txtLedgerName.Text + "' And Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " and Lcode='" + SessionLcode + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            DataTable dtBOMList = new DataTable();
            dtBOMList = (DataTable)ViewState["ItemTable"];

            if (dtBOMList.Rows.Count > 0)
            {
                for (int i = 0; i < dtBOMList.Rows.Count; i++)
                {
                    SSQL = "Insert Into Acc_Mst_Ledger_BOMList (Ccode,Lcode,LedgerCode,LedgerName,ItemCode,ItemName,SAPNo,";
                    SSQL = SSQL + " ModelCode,ModelName,ProductionPartCode,ProductionPartName,";
                    SSQL = SSQL + " UserID,UserName,CreateOn,IP,SystemName) Values ('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + txtLedgerCode.Text + "','" + txtLedgerName.Text + "',";
                    SSQL = SSQL + " '" + dtBOMList.Rows[i]["ItemCode"].ToString() + "','" + dtBOMList.Rows[i]["ItemName"].ToString() + "',";
                    SSQL = SSQL + " '" + dtBOMList.Rows[i]["SAPNo"].ToString() + "','" + dtBOMList.Rows[i]["ModelCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dtBOMList.Rows[i]["ModelName"].ToString() + "','" + dtBOMList.Rows[i]["ProductionPartCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dtBOMList.Rows[i]["ProductionPartName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " Getdate(),'" + IP + "','" + HostName + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtLedgerCode.Enabled = true;

            Response.Redirect("Mstsupplier_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Ledger Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtLedgerCode.ReadOnly = false;
        txtLedgerCode.Text = "";
        txtLedgerName.Text = "";
        ddlAccType.SelectedItem.Text = "-Select-";
        txtHouseNumber.Text = "";
        txtAddr1.Text = "";
        txtAddr2.Text = "";
        txtCity.Text = "";
        txtPincode.Text = "";
        txtState.Text = "";
        txtCountry.Text = "";
        txtMobiCode.Text = "";
        txtMobile.Text = "";
        txtStdCode.Text = "";
        txtPhone.Text = "";
        txtMail.Text = "";
        ddlBankName.SelectedItem.Text = "-Select-";
        txtAccountNo.Text = "";
        txtBranchName.Text = "";
        txtIFSC.Text = "";
        txtTransport.Text = "";
        txtZone.Text = "";
        txtGstin.Text = "";
        txtNotes.Text = "";
        txtWebSite.Text = "";
        Initial_Data_Referesh();
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status = 'Add' And ";
        SSQL = SSQL + " LedgerCode='" + txtLedgerCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtLedgerCode.ReadOnly = true;
            txtLedgerName.Text = DT.Rows[0]["LedgerName"].ToString();
            ddlAccType.SelectedItem.Text = DT.Rows[0]["AccountType"].ToString();
            ddlAccType.SelectedValue = DT.Rows[0]["AccountTypeCode"].ToString();

            ddlLedgerGrp.SelectedValue= DT.Rows[0]["AccGrpCode"].ToString();
            ddlLedgerGrp.SelectedItem.Text = DT.Rows[0]["AccGrpName"].ToString();

            txtHouseNumber.Text = DT.Rows[0]["HouseNo"].ToString();
            txtAddr1.Text = DT.Rows[0]["Add1"].ToString();
            txtAddr2.Text = DT.Rows[0]["Add2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtPincode.Text = DT.Rows[0]["PinCode"].ToString();
            txtState.Text = DT.Rows[0]["State"].ToString();
            txtCountry.Text = DT.Rows[0]["Country"].ToString();
            txtStdCode.Text = DT.Rows[0]["Std_Code"].ToString();
            txtPhone.Text = DT.Rows[0]["TelNo"].ToString();
            txtMobiCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtMobile.Text = DT.Rows[0]["MobileNo"].ToString();
            txtMail.Text = DT.Rows[0]["MailID"].ToString();
             
            txtTransport.Text = DT.Rows[0]["TransPort"].ToString();
            txtZone.Text = DT.Rows[0]["Zone"].ToString();
            txtGstin.Text = DT.Rows[0]["GSTNo"].ToString();
            txtNotes.Text = DT.Rows[0]["Description"].ToString();
             
            ddlBankName.SelectedValue = DT.Rows[0]["BankId"].ToString();
            ddlBankName.SelectedItem.Text = DT.Rows[0]["BankName"].ToString();
            txtBranchName.Text = DT.Rows[0]["BranchName"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();
            txtAccountNo.Text = DT.Rows[0]["AccountNo"].ToString();
            txtWebSite.Text = DT.Rows[0]["WebSite"].ToString();

            txtTC.Text = DT.Rows[0]["TC"].ToString();
            txtPackTerms.Text = DT.Rows[0]["PackingTerms"].ToString();
            txtWarranty.Text = DT.Rows[0]["Warranty"].ToString();
            txtQuality.Text = DT.Rows[0]["Quality"].ToString();


            DataTable DTSub = new DataTable();

            SSQL = "Select * From Acc_Mst_Ledger_BOMList where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
            SSQL = SSQL + " And LedgerCode='" + txtLedgerCode.Text + "' and LedgerName='" + txtLedgerName.Text + "' ";

            DTSub = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DTSub.Rows.Count != 0)
            {
                ViewState["ItemTable"] = DTSub;

                rpBOMLIST.DataSource = DTSub;
                rpBOMLIST.DataBind();
            }

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Mstsupplier_Main.aspx");
    }

    public void GridBOMDel_Click(object sender, CommandEventArgs e)
    {   
       
    }

    protected void ddlPurType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPurType.SelectedValue.ToString() == "1")
        {
            pnlModel.Visible = false;
            pnlSingle.Visible = true;
        }
        else if (ddlPurType.SelectedValue.ToString() == "2")
        {
            pnlSingle.Visible = false;
            pnlModel.Visible = true;
        }
        else
        {
            pnlSingle.Visible = false;
            pnlModel.Visible = false;
        }
    }

    protected void txtProduction_Part_Name_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ItemCode();
        if (txtProduction_Part_Name.SelectedValue != "Single Material")
        {
            Load_BoM_Mat_ItemTable();
        }
    }

    private void Load_OLD_data()
    {

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        rpBOMLIST.DataSource = dt;
        rpBOMLIST.DataBind();
    }

    protected void txtGenerator_Model_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_Production_Part_Name();
        Load_Data_Empty_ItemCode();
    }


    private void Load_Data_Empty_Generator_Model()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();


        SSQL = "Select GenModelNo,GenModelName from GeneratorModels where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status='Add'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtGenerator_Model.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["GenModelNo"] = "-Select-";
        dr["GenModelName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGenerator_Model.DataTextField = "GenModelName";
        txtGenerator_Model.DataValueField = "GenModelNo";
        txtGenerator_Model.DataBind();

    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable qry_dt = new DataTable();
            bool ErrFlag = false;
            DataRow dr = null;
            string SSQL = "";

            //if (txtReuiredQty.Text == "")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Reuired Qty...');", true);
            //}

            //check with Item Code And Item Name 
            SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Mat_No='" + txtItemCode.Text + "' And Raw_Mat_Name='" + txtItemName.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with BOM Material Details..');", true);
            }

            if (!ErrFlag)
            {
                Load_Data_Empty_Generator_Model();
                //UOM Code get
                string SAPNo = qry_dt.Rows[0]["Sap_No"].ToString();
                //string Item_Rate_Str = qry_dt.Rows[0]["Amount"].ToString();
                //txtItemRate.Text = Item_Rate_Str;

                txtGenerator_Model.SelectedItem.Text = qry_dt.Rows[0]["GenModelName"].ToString();

                GenModNo = qry_dt.Rows[0]["GenModelNo"].ToString();
                //txtGenerator_Model.SelectedValue = qry_dt.Rows[0]["GenModelNo"].ToString().Trim('');
                //txtProduction_Part_Name.SelectedValue = qry_dt.Rows[0]["ProductionPartNo"].ToString();

                string Partval = qry_dt.Rows[0]["ProductionPartNo"].ToString();

                txtProduction_Part_Name.SelectedItem.Text = qry_dt.Rows[0]["ProductionPartName"].ToString();

                // check view state is not null  
                if (ViewState["ItemTable"] != null)
                {
                    //get datatable from view state   
                    dt = (DataTable)ViewState["ItemTable"];

                    //check Item Already add or not
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCode.Text.ToString().ToUpper())
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                        }
                    }
                    if (!ErrFlag)
                    {
                        dr = dt.NewRow();
                        dr["ModelCode"] = GenModNo;
                        dr["ModelName"] = txtGenerator_Model.SelectedItem.Text;
                        dr["ProductionPartCode"] = Partval;
                        dr["ProductionPartName"] = txtProduction_Part_Name.SelectedItem.Text;
                        dr["ItemCode"] = txtItemCode.Text;
                        dr["ItemName"] = txtItemName.Text;
                        dr["SAPNo"] = SAPNo;

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        rpBOMLIST.DataSource = dt;
                        rpBOMLIST.DataBind();

                        txtItemNameSelect.SelectedItem.Text = "-Select-";
                        txtItemNameSelect.SelectedValue = "-Select-";
                        txtItemCode.Text = ""; txtItemName.Text = "";
                        //txtDate.Text = ""; txtDeptCode.SelectedValue = "-Select-"; txtOthers.Text = "";
                        //txtGenerator_Model.SelectedValue = "-Select-"; txtProduction_Part_Name.SelectedValue = "-Select-";
                        //txtItemNameSelect.SelectedValue = "-Select-"; txtItemRemarks.Text = "";

                    }
                }
                else
                {
                    dr = dt.NewRow();

                    dr["ModelCode"] = GenModNo;
                    dr["ModelName"] = txtGenerator_Model.SelectedItem.Text;
                    dr["ProductionPartCode"] = Partval;
                    dr["ProductionPartName"] = txtProduction_Part_Name.SelectedItem.Text;
                    dr["ItemCode"] = txtItemCode.Text;
                    dr["ItemName"] = txtItemName.Text;
                    dr["SAPNo"] = SAPNo;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    rpBOMLIST.DataSource = dt;
                    rpBOMLIST.DataBind();

                    txtItemCode.Text = ""; txtItemName.Text = "";
                    //txtDate.Text = "";txtDeptCode.SelectedItem.Text = "-Select-";txtOthers.Text = "";
                    //txtGenerator_Model.SelectedItem.Text = "-Select-";txtProduction_Part_Name.SelectedItem.Text = "-Select-";
                    //txtItemNameSelect.SelectedItem.Text = "Select";txtItemRemarks.Text = "";
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCode.Text = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;
        }
    }

    private void Load_Data_Empty_ItemCode()
    {

        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        SSQL = "Select Raw_Mat_Name as ItemName,Mat_No as ItemCode from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        if (txtGenerator_Model.SelectedValue != "-Select-")
        {
            SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.SelectedValue + "'";
        }
        if (txtProduction_Part_Name.SelectedValue != "-Select-" && txtProduction_Part_Name.SelectedValue != "All" && txtProduction_Part_Name.SelectedValue != "Single Material")
        {
            SSQL = SSQL + " And ProductionPartName='" + txtProduction_Part_Name.SelectedItem.Text + "'";
        }
        SSQL = SSQL + " Order by Raw_Mat_Name Asc";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();

    }

    private void Load_Data_Empty_Production_Part_Name()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = " Select 'All' ProductNo,'All' ProductName Union all ";
        SSQL = SSQL + " Select ProductNo,ProductName from ProductModel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.SelectedValue + "' and Status='Add'";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtProduction_Part_Name.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["ProductNo"] = "-Select-";
        dr["ProductName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);

        txtProduction_Part_Name.DataTextField = "ProductName";
        txtProduction_Part_Name.DataValueField = "ProductNo";
        txtProduction_Part_Name.DataBind();

    }

    private void Load_BoM_Mat_ItemTable()
    {
        string SSQL = "";
        //Initial_Data_Referesh();
        DataTable DT = new DataTable();

        SSQL = "Select Mat_No as ItemCode,Raw_Mat_Name as ItemName,Sap_No as SAPNo,GenModelNo[ModelCode],GenModelName[ModelName],";
        SSQL = SSQL + " ProductionPartNo[ProductionPartCode],ProductionPartName[ProductionPartName] ";
        SSQL = SSQL + " From BOMMaster Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " GenModelNo ='" + txtGenerator_Model.SelectedValue + "'";

        if (txtProduction_Part_Name.SelectedValue != "All")
        {
            SSQL = SSQL + " And ProductionPartNo='" + txtProduction_Part_Name.SelectedValue + "'";
        }
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable DTChkRows = new DataTable();
        DTChkRows = (DataTable)ViewState["ItemTable"];
        DataRow dr;

        if (DTChkRows.Rows.Count > 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                dr = DTChkRows.NewRow();
                dr["ModelCode"] = DT.Rows[i]["ModelCode"].ToString();
                dr["ModelName"] = DT.Rows[i]["ModelName"].ToString();
                dr["ProductionPartCode"] = DT.Rows[i]["ProductionPartCode"].ToString();
                dr["ProductionPartName"] = DT.Rows[i]["ProductionPartName"].ToString();
                dr["ItemCode"] = DT.Rows[i]["ItemCode"].ToString();
                dr["ItemName"] = DT.Rows[i]["ItemName"].ToString();
                dr["SAPNo"] = DT.Rows[i]["SAPNo"].ToString();

                DTChkRows.Rows.Add(dr);
                ViewState["ItemTable"] = DTChkRows;
                rpBOMLIST.DataSource = DTChkRows;
                rpBOMLIST.DataBind();
            }
        }
        else
        {
            ViewState["ItemTable"] = DT;
            rpBOMLIST.DataSource = DT;
            rpBOMLIST.DataBind();
            //TotalReqQty();
        }
    }

    protected void btnDelItem_Command(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    private void Load_Data_Empty_SAPNo()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        SSQL = "Select Sap_No as SAPNo,Raw_Mat_Name as ItemName,Mat_No as ItemCode from BOMMaster where Ccode='" + SessionCcode + "' ";
        SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Sap_No !=''";

        if (txtGenerator_Model.SelectedValue != "-Select-")
        {
            SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.SelectedValue + "'";
        }
        if (txtProduction_Part_Name.SelectedValue != "-Select-" && txtProduction_Part_Name.SelectedValue != "ALL" && txtProduction_Part_Name.SelectedValue != "Single Material")
        {
            SSQL = SSQL + " And ProductionPartName='" + txtProduction_Part_Name.SelectedItem.Text + "'";
        }
        SSQL = SSQL + " Order by Raw_Mat_Name Asc";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlSAPNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["SAPNo"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlSAPNo.DataTextField = "SAPNo";
        ddlSAPNo.DataValueField = "SAPNo";
        ddlSAPNo.DataBind();
    }

    protected void ddlSAPNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";

        if (ddlSAPNo.SelectedItem.Text != "-Select-")
        {
            SSQL = SSQL + " And Sap_No='" + ddlSAPNo.SelectedItem.Text + "'";
        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            DataTable dtRqu = new DataTable();

            txtItemName.Text = DT.Rows[0]["Raw_Mat_Name"].ToString();
            txtItemCode.Text = DT.Rows[0]["Mat_No"].ToString();
            txtItemNameSelect.SelectedItem.Text = DT.Rows[0]["Raw_Mat_Name"].ToString();
            
            //ddlSAPNo.SelectedItem.Text = DT.Rows[0]["SAP_No"].ToString();
            //txtItemNameSelect_SelectedIndexChanged(sender, e);
            //ddlBOMCode_SelectedIndexChanged(sender, e);
        }
    }
}
