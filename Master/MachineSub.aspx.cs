﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MachineSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string SessionFinYearVal = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            if (!IsPostBack)
            {
                if ((string)Session["Machine_ID"] != null)
                {
                    Search(Session["Machine_ID"].ToString());
                }
            }
        }
    }
    private void Search(string id)
    {
        SSQL = "";
        SSQL = "Select * from MachineMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Machine_ID='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtMachineNo.Enabled = false;
            txtMachineNo.Text = dt.Rows[0]["Machine_No"].ToString();
            txtMachineSNO.Text = dt.Rows[0]["Machine_Sno"].ToString();
            txtMake.Text = dt.Rows[0]["Make"].ToString();
            txtModel.Text = dt.Rows[0]["Model"].ToString();
            txtPurchaseYear.Text = dt.Rows[0]["Purchase_Year"].ToString();
            btnSave.Text = "Update";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        //if (txtMachineNo.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Machine Model Number')", true); ;
        //    ErrFlg = true;
        //}
        if (txtMachineSNO.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Machine Serial Number')", true); ;
            ErrFlg = true;
        }
       
        if (!ErrFlg)
        {

            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from MachineMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Machine_No='" + txtMachineNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MachineMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Machine_No='" + txtMachineNo.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Machine Number Already Taken')", true); ;
                    
                    ErrFlg = true;
                    return;
                }
                SSQL = "";
                SSQL = "Select * from MachineMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Machine_Sno='" + txtMachineSNO.Text + "'";
               // DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Machine Serial Number Already Taken')", true); ;

                    ErrFlg = true;
                    return;
                }
            }

            //Auto generate Transaction Function Call
            if (btnSave.Text != "Update")
            {
                if (!ErrFlg)
                {
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Machine Master", SessionFinYearVal, "1");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlg = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        txtMachineNo.Text = Auto_Transaction_No;
                    }
                }
            }

            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "";
                SSQL = "Insert into MachineMaster(Ccode,Lcode,Machine_No,Machine_Sno,Make,Model,Purchase_Year,CreatedOn,Host_IP,Host_Name,UserName,Status)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "' ,'" + txtMachineNo.Text + "','" + txtMachineSNO.Text + "','" + txtMake.Text + "','" + txtModel.Text + "','" + txtPurchaseYear.Text + "',";
                SSQL = SSQL + " '" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + Session["UserID"] + "','Add')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                btnClear_Click(sender, e);

                Session.Remove("Machine_ID");
                Response.Redirect("/Master/MachineMain.aspx");
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtMachineNo.Text = "";
        txtMachineSNO.Text = "";
        txtMake.Text = "";
        txtModel.Text = "";
        txtPurchaseYear.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("Machine_ID");
        Response.Redirect("/Master/MachineMain.aspx");
    }
}