﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ToolsMasterMain.aspx.cs" Inherits="ToolsMasterMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span>Tools List</span></h3>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">
                                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="true">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Ref_Code</th>
                                                                <th>Tools_Name</th>
                                                                <th>HSN_Code</th>
                                                                <th>UOM</th>
                                                                <th>Assembly_Stage</th>
                                                                <th>Scope</th>
								                                <%--<th>Production Step</th>
                                                                <th>Drawing_No</th>
                                                                <th>Generator_Model</th>--%>
                                                                <th>Type</th>
                                                                <th>Edit/Delete</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("RefCode")%></td>
                                                        <td><%# Eval("ToolName")%></td>
                                                        <td><%# Eval("HSNCode")%></td>	
                                                        <td><%# Eval("UOM")%></td>
                                                        <td><%# Eval("AssemblyStageName")%></td>
                                                        <td><%# Eval("Scope")%></td>
							                            <%--<td><%# Eval("ProductionStepName")%></td>
                                                        <td><%# Eval("DrawingName")%></td>
                                                        <td><%# Eval("GenModelName")%></td>--%>
                                                        <td><%# Eval("Type")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="btnEditGrid_Command" CommandArgument='<%#Eval("ToolCode")%>' CommandName="Edit">
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" CommandArgument='<%#Eval("ToolCode")%>' OnCommand="btnDeleteGrid_Command" CommandName="Delete"
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Account Type details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                    <!-- /.mail-box-messages -->
                                </div>
                                <div class="box-footer no-padding">
                                    <div class="box-footer">
                                        <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-success" OnClick="btnUpload_Click" Text="Upload" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/Master/BOMMasters.js"></script>
</asp:Content>
