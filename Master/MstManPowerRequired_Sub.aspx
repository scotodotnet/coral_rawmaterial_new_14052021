﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstManPowerRequired_Sub.aspx.cs" Inherits="MstManPowerRequired_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy'});
                    $('.select2').select2();
                }
            });
        };
    </script>

    <div class="content-wrapper">
        <asp:UpdatePanel ID="upManPwrSub" runat="server">
            <ContentTemplate>
                <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Required ManPower</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Generator Model <span class="text-danger">*</span></label>
                                    <asp:DropDownList ID="ddlGenerator" AutoPostBack="true" OnSelectedIndexChanged="ddlGenerator_SelectedIndexChanged" 
                                        Class="form-control select2" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="ddlGenerator" Display="Dynamic" InitialValue="-Select-" 
                                        ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" 
                                        EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Production Part Name <span class="text-danger">*</span></label>
                                    <asp:DropDownList ID="ddlProduct" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="txtRequiredQty" Display="Dynamic" InitialValue="-Select-" 
                                        ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" 
                                        EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Required Qty <span class="text-danger">*</span></label>
                                    <asp:TextBox ID="txtRequiredQty" class="form-control " runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtRequiredQty" Display="Dynamic" ValidationGroup="Validate_Field" 
                                        class="text-danger" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtRequiredQty" ValidChars="0123456789.">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Required Days To Complete <span class="text-danger">*</span></label>
                                    <asp:TextBox ID="txtTotalDays" class="form-control " runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtTotalDays" Display="Dynamic" ValidationGroup="Validate_Field" 
                                        class="text-danger" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtTotalDays" ValidChars="0123456789.">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date From <span class="text-danger">*</span></label>
                                    <asp:TextBox ID="txtFromDate" class="form-control datepicker" runat="server" AutoComplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Validate_Field" 
                                        class="text-danger" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtFromDate" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date To <span class="text-danger">*</span></label>
                                    <asp:TextBox ID="txtTODate" class="form-control datepicker" runat="server" AutoComplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtTODate" Display="Dynamic" ValidationGroup="Validate_Field" 
                                        class="text-danger" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtTODate" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>

                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <asp:Button ID="btnClear" class="btn btn-primary" runat="server"  Text="Clear"  OnClick="btnClear_Click"/>
                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                    </div>
                </div>
            </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
</asp:Content>

