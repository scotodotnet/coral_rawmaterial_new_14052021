﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Master_MstProStep_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionUserID;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string SessionFinYearVal = "";
    string SessionUserName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();

            Load_Data();
        }
    }
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstProductionStep where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        Session["productStepID"] = e.CommandName.ToString();
        Response.Redirect("/Master/MstProStep_Sub.aspx");
    }
    protected void btnDeleteIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Update MstProductionStep set Status='Delete' where Production_StepID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data(); 
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session.Remove("productStepID");
        Response.Redirect("/Master/MstProStep_Sub.aspx");
    }
}