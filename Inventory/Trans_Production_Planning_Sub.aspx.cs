﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_Production_Planning_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionDCNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal RetQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Production Planning";
            Initial_Data_Referesh();
            Load_Data_GenModel();

            if (Session["DCNo"] == null)
            {
                SessionDCNo = "";

                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Production Planning", SessionFinYearVal, "1");

                //lblTransNo.Text = Auto_Transaction_No;
                txtTransDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            }
            else
            {
                SessionDCNo = Session["DCNo"].ToString();
                lblTransNo.Text = SessionDCNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();

    }

    protected void Load_Data_GenModel()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select GenModelNo,GenModelName from CORAL_ERP_Sales..GeneratorModels Where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Status!='Delete'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlGenMdlNo.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["GenModelNo"] = "-Select-";
        dr["GenModelName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlGenMdlNo.DataTextField = "GenModelName";
        ddlGenMdlNo.DataValueField = "GenModelNo";
        ddlGenMdlNo.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string SysIP = getIPAndName.GetIP();
        string SysName = getIPAndName.GetName();

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["GenMdlDet"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }


        if (!ErrFlag)
        {
            TransactionNoGenerate TransNO = new TransactionNoGenerate();
            string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Production Planning", SessionFinYearVal, "1");
            if (Auto_Transaction_No == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
            }
            else
            {
                lblTransNo.Text = Auto_Transaction_No;
            }
        }

        SSQL = "Delete from Trans_ProductionPlaning_Main Where Ccode='" + SessionCcode + "' and TransNo='" + lblTransNo.Text + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Delete from Trans_ProductionPlaning_Sub Where Ccode='" + SessionCcode + "' and TransNo='" + lblTransNo.Text + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);


        if (!ErrFlag)
        {

            //Insert Main Table

            SSQL = "Insert Into Trans_ProductionPlaning_Main(Ccode,Lcode,FinYearCode,FinYearVal,TransNo,TransDate,Remarks,Approval_Status,Status, UserID,";
            SSQL = SSQL + " UserName,CreateOn,SysIP,SysName,GenModelCode,GenModelName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            SSQL = SSQL + " '" + SessionFinYearVal + "','" + lblTransNo.Text + "','" + txtTransDate.Text + "','" + txtRemarks.Text + "','0','Add',";
            SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "',";
            SSQL = SSQL + " '" + SysIP + "','" + SysName + "','" + ddlGenMdlNo.SelectedValue + "','" + ddlGenMdlNo.SelectedItem.Text + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["GenMdlDet"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                Label lblGrdMdlCode = rptGenPartType.Items[i].FindControl("lblGrdMdlode") as Label;
                Label lblGrdMdlName = rptGenPartType.Items[i].FindControl("lblGrdMdlName") as Label;
                Label lblGrdPrtName = rptGenPartType.Items[i].FindControl("lblGrdPratType") as Label;
                TextBox txtStartDate = rptGenPartType.Items[i].FindControl("txtGrdStartDate") as TextBox;
                TextBox txtEndDate = rptGenPartType.Items[i].FindControl("txtGrdEndDate") as TextBox;

                SSQL = "Insert Into Trans_ProductionPlaning_Sub(Ccode,Lcode,FinYearCode,FinYearVal,TransNo,TransDate,GenModelCode,GenModelName,";
                SSQL = SSQL + " PartsName,StartDate,EndDate,Status,UserID,UserName,CreateOn) values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + lblTransNo.Text + "','" + txtTransDate.Text + "',";
                SSQL = SSQL + " '" + lblGrdMdlCode.Text + "','" + lblGrdMdlName.Text + "',";
                SSQL = SSQL + " '" + lblGrdPrtName.Text + "','" + txtStartDate.Text + "',";
                SSQL = SSQL + " '" + txtEndDate.Text + "','Add','" + SessionUserID + "','" + SessionUserName + "',";
                SSQL = SSQL + " '" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (btnSave.Text == "Save")
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Production Planning", SessionFinYearVal, "1");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass Out Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass Out Details Updated Successfully');", true);
            }

            //Clear_All_Field();
            Session["GPOutNo"] = lblTransNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
            Response.Redirect("Trans_Production_Planning_Main.aspx");

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        lblTransNo.Text = ""; txtTransDate.Text = "";
        // txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        //txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("GPOutNo");
        //Load_Data_Enquiry_Grid();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["GenMdlDet"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["GenMdlDet"] = dt;
        Load_OLD_data();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["GenMdlDet"];
        rptGenPartType.DataSource = dt;
        rptGenPartType.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        DataTable dt = new DataTable();

        SSQL = "Select * from Trans_ProductionPlaning_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And TransNo ='" + lblTransNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtTransDate.Text = Main_DT.Rows[0]["TransDate"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            ddlGenMdlNo.SelectedValue = Main_DT.Rows[0]["GenModelCode"].ToString();

            SSQL = "Select GenModelCode,GenModelName,PartsName,StartDate, EndDate ";
            SSQL = SSQL + " From Trans_ProductionPlaning_Sub Where Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " LCode='" + SessionLcode + "' and Status!='Delete' And TransNo='" + lblTransNo.Text + "'";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["GenMdlDet"] = dt;
            rptGenPartType.DataSource = dt;
            rptGenPartType.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_Production_Planning_Main.aspx");
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("GenModelCode", typeof(string)));
        dt.Columns.Add(new DataColumn("GenModelName", typeof(string)));
        dt.Columns.Add(new DataColumn("PartsName", typeof(string)));
        dt.Columns.Add(new DataColumn("StartDate", typeof(string)));
        dt.Columns.Add(new DataColumn("EndDate", typeof(string)));

        rptGenPartType.DataSource = dt;
        rptGenPartType.DataBind();
        ViewState["GenMdlDet"] = rptGenPartType.DataSource;
    }

    protected void ddlGenMdlNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select '" + ddlGenMdlNo.SelectedValue + "'[GenModelCode],GenModelName,PartsName,'' StartDate,'' EndDate ";
        SSQL = SSQL + " From CORAL_ERP_Sales..PartsType Where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LCode='" + SessionLcode + "' and Status!='Delete' And GenModelName='" + ddlGenMdlNo.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        rptGenPartType.DataSource = DT;
        rptGenPartType.DataBind();
        ViewState["GenMdlDet"] = DT;
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtPrdStartDate.Text == "" && txtPrdEndDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the FromDate and Todate...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["GenMdlDet"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["GenMdlDet"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["PartsName"].ToString() == ddlPrdPartType.SelectedItem.Text.ToString())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();

                    dr["GenModelCode"] = ddlGenMdlNo.SelectedValue;
                    dr["GenModelName"] = ddlGenMdlNo.SelectedItem.Text;
                    dr["PartsName"] = ddlPrdPartType.SelectedItem.Text;
                    dr["StartDate"] = txtPrdStartDate.Text;
                    dr["EndDate"] = txtPrdEndDate.Text;

                    dt.Rows.Add(dr);
                    ViewState["GenMdlDet"] = dt;
                    rptGenPartType.DataSource = dt;
                    rptGenPartType.DataBind();

                    ddlGenMdlNo.SelectedIndex = 0; ddlPrdPartType.SelectedIndex = 0;
                    txtPrdStartDate.Text = ""; txtPrdEndDate.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();

                dr["GenModelCode"] = ddlGenMdlNo.SelectedValue;
                dr["GenModelName"] = ddlGenMdlNo.SelectedItem.Text;
                dr["PartsName"] = ddlPrdPartType.SelectedItem.Text;
                dr["StartDate"] = txtPrdStartDate.Text;
                dr["EndDate"] = txtPrdEndDate.Text;


                dt.Rows.Add(dr);
                ViewState["GenMdlDet"] = dt;
                rptGenPartType.DataSource = dt;
                rptGenPartType.DataBind();

                ddlGenMdlNo.SelectedIndex = 0; ddlPrdPartType.SelectedIndex = 0;
                txtPrdStartDate.Text = ""; txtPrdEndDate.Text = "";
            }
        }
    }
}