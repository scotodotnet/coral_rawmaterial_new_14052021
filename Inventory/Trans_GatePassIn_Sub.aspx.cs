﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_GatePassIn_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGPIN_No;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal INQty;

    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: GatePass IN";
            Initial_Data_Referesh();
            Load_GatePassOut_No();

            if (Session["GPIN_No"] == null)
            {
                SessionGPIN_No = "";
            }
            else
            {
                SessionGPIN_No = Session["GPIN_No"].ToString();
                txtGPINNo.Text = SessionGPIN_No;
                btnSearch_Click(sender, e);


            }
        }
        Load_OLD_data();
    }


    private void Load_GSTType()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "select GST_Type from GstMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And  Status='Add' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlGST.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["GST_Type"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlGST.DataTextField = "GST_Type";
        ddlGST.DataBind();
    }
    private void Load_GatePassOut_No()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select GPOutNo from Trans_GPOut_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And Approval_Status='0'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlGPOut.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["GPOutNo"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlGPOut.DataTextField = "GPOutNo";
        ddlGPOut.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
         
        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "GatePass IN", SessionFinYearVal,"1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGPINNo.Text = Auto_Transaction_No;
                }
            }
        }

        //string Roundoff = "0.0";

        //decimal value;
        //if (decimal.TryParse(txtTotAmt.Text, out value))
        //{
        //    value = Math.Round(value);
        //    Roundoff = value.ToString();
        //    // Do something with the new text value
        //}
        //else
        //{
        //    // Tell the user their input is invalid
        //}

        decimal Total = Convert.ToDecimal(txtTotAmt.Text);

        decimal Roundoff = Total - Convert.ToInt32(Total);

        if ((double)Roundoff <= .50)
        {
            txtTotAmt.Text = Convert.ToInt32(Total).ToString();
        }
        else
        {
            txtTotAmt.Text = (Convert.ToInt32(Total) + 1).ToString();
        }
        //lblTotal.Text = Total.ToString();

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //Update Main table

                SSQL = "Update Trans_GPIN_Main set GPIN_Date='" + txtGPINDate.Text + "',GPOut_No='" + ddlGPOut.SelectedItem.Text + "',";
                SSQL = SSQL + " GPOut_Date='" + txtGPOutDate.Text + "',SuppCode='" + hdSuppCode.Value + "',";
                SSQL = SSQL + " SuppName='" + txtSuppName.Text + "',SuppDet='" + txtSuppDet.Text + "',InvNo='" + txtSuppInvNo.Text + "',";
                SSQL = SSQL + " InvDate ='" + txtSuppInvDate.Text + "',Others='" + txtRemarks.Text + "',";
                SSQL = SSQL + " TotalQty='" + txtTotQty.Text + "',TotalAmt='" + txtTotAmt.Text + "'";
                SSQL = SSQL + " Where GPIN_No='" + txtGPINNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                //Update Sub table

                DataTable dt = new DataTable();

                //Load_OLD_data();
                //dt = (DataTable)ViewState["ItemTable"];
                //dt = (DataTable)Repeater1;

                for (int i = 0; i < Repeater1.Items.Count; i++)
                //foreach (RepeaterItem Item in Repeater1.Items)
                {
                    //string ReceivedQty = item.ToString();

                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                    Label UOMCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label UONName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;
                    Label Warehouse = Repeater1.Items[i].FindControl("lblWarehouse") as Label;
                    TextBox InQty = Repeater1.Items[i].FindControl("txtInQty") as TextBox;
                    TextBox Rate = Repeater1.Items[i].FindControl("txtRate") as TextBox;
                    Label ItemTotal = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                    Label CGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                    Label CGSTAmount = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                    Label SGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                    Label SGSTAmount = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                    Label IGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                    Label IGSTAmount = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                    Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                    SSQL = "Update Trans_GPIN_Sub Set InQty='" + InQty.Text.ToString() + "',Rate ='" + Rate.Text.ToString() + "',";
                    SSQL = SSQL + " Value='" + ItemTotal.Text + "',CGSTPer='" + CGSTPer.Text + "',CGSTAmt='" + CGSTAmount.Text + "',";
                    SSQL = SSQL + " SGSTPer='" + SGSTPer.Text + "',SGSTAmt='" + SGSTAmount.Text + "',IGSTPer='" + IGSTPer.Text + "',";
                    SSQL = SSQL + " IGSTAmt='" + IGSTAmount.Text + "',TotAmt='" + LineTotal.Text + "' Where ItemCode ='" + ItemCode.Text + "' ";
                    SSQL = SSQL + " And ItemName='" + ItemName.Text + "' And GPIN_No='" + txtGPINNo.Text + "' And Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " And LCode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                    SSQL = SSQL + " FinYearVal='" + SessionFinYearVal + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    //Stock Update
                    //DateTime transDate = Convert.ToDateTime(txtGPINDate.Text);

                    //SSQL = "Update Trans_Stock_Ledger_All set Add_Qty = '" + ReceiveQty.Text.ToString() + "',";
                    //SSQL = SSQL + " Add_Value='" + ItemTotal.Text.ToString() + "' Where ItemCode ='" + ItemCode.Text + "' ";
                    //SSQL = SSQL + " And ItemName='" + ItemName.Text + "' And Trans_No='" + txtGPINNo.Text + "' And Ccode='" + SessionCcode + "' ";
                    //SSQL = SSQL + " And LCode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                    //SSQL = SSQL + " FinYearVal='" + SessionFinYearVal + "'";

                    //objdata.RptEmployeeMultipleDetails(SSQL);

                }
            }
            else
            {
                //Insert Main Table

                SSQL = "Insert Into Trans_GPIN_Main(Ccode,Lcode,FinYearCode,FinYearVal,GPIN_No,GPIN_Date,GPOut_No,GPOut_Date,InvNo,InvDate,";
                SSQL = SSQL + " SuppCode,SuppName,SuppDet,Others,ServiceType,StockType,OtherAmt,PackingAmt,TotalQty,TotalAmt,Approval_Status,";
                SSQL = SSQL + " Status,UserID,UserName,CreateOn) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                SSQL = SSQL + " '" + SessionFinYearVal + "','" + txtGPINNo.Text + "','" + txtGPINDate.Text + "','" + ddlGPOut.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + txtGPOutDate.Text + "','" + txtSuppInvNo.Text + "','" + txtSuppInvDate.Text +"','" + hdSuppCode.Value + "',";
                SSQL = SSQL + " '" + txtSuppName.Text + "','" + txtSuppDet.Text + "','" + txtRemarks.Text + "','Service','Stock','0.00','0.00',";
                SSQL = SSQL + " '" + txtTotQty.Text + "','" + txtTotAmt.Text + "','0','ADD','" + SessionUserID + "','" + SessionUserName + "',";
                SSQL = SSQL + " Getdate())";

                objdata.RptEmployeeMultipleDetails(SSQL);

                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ItemTable"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                    Label UOMCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label UONName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                    Label DeptCode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;
                    Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label Warehouse = Repeater1.Items[i].FindControl("lblWarehouse") as Label;
                    TextBox InQty = Repeater1.Items[i].FindControl("txtInQty") as TextBox;
                    Label Rate = Repeater1.Items[i].FindControl("txtRate") as Label;
                    Label ItemTotal = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                    Label GSTType = Repeater1.Items[i].FindControl("lblGSTType") as Label;
                    Label CGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                    Label CGSTAmount = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                    Label SGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                    Label SGSTAmount = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                    Label IGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                    Label IGSTAmount = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                    Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                    SSQL = "Insert Into Trans_GPIN_Sub(Ccode,Lcode,FinYearCode,FinYearVal,GPIN_No,GPIN_Date,GPOut_No,GPOut_Date,ItemCode,ItemName,";
                    SSQL = SSQL + " UOMCode,UOMName,DeptCode,DeptName,WareHouseCode,WareHouseName,RackName,Remarks,OutQty,INQty,ScrabQty,Rate,Value,";
                    SSQL = SSQL + " DiscPer,DiscAmt,GSTType,CGSTPer,CGSTAmt,SGSTPer,SGSTAmt,IGSTPer,IGSTAmt,TotAmt,UserID,UserName,CreateOn) ";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtGPINNo.Text + "','" + txtGPINDate.Text + "','" + ddlGPOut.SelectedItem.Text + "','" + txtGPOutDate.Text + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["UOMName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["WarehouseCode"].ToString() + "','" + dt.Rows[i]["WarehouseName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["RackName"].ToString() + "','','" + dt.Rows[i]["OutQty"].ToString() + "',";
                    SSQL = SSQL + " '" + InQty.Text.ToString() + "','0.00','" + dt.Rows[i]["Rate"].ToString() + "',";
                    SSQL = SSQL + " '" + ItemTotal.Text.ToString() + "','0.00','0.00','"+ GSTType.Text.ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["CGSTPer"].ToString() + "','" + CGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["SGSTPer"].ToString() + "','" + SGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["IGSTPer"].ToString() + "','" + IGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + LineTotal.Text.ToString() + "','" + SessionUserID + "','" + SessionUserName + "',Getdate())";

                    objdata.RptEmployeeMultipleDetails(SSQL);


                    //Stock Insert

                    //DateTime transDate = Convert.ToDateTime(txtGPINDate.Text);

                    //SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,";
                    //SSQL = SSQL + " Supp_Code,Supp_Name,ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,Add_Qty,Add_Value,";
                    //SSQL = SSQL + " Minus_Qty,Minus_Value,UserID,UserName) Values(";
                    //SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    //SSQL = SSQL + " '" + txtGPINNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtGPINDate.Text + "','Goods Received',";
                    //SSQL = SSQL + " '" + hdSuppCode.Value + "','" + txtSuppName.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "',";
                    //SSQL = SSQL + " '" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["DeptCode"].ToString() + "',";
                    //SSQL = SSQL + " '" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                    //SSQL = SSQL + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + ReceiveQty.Text.ToString() + "',";
                    //SSQL = SSQL + " '" + ItemTotal.Text.ToString() + "','0.00','0.00','" + SessionUserID + "','" + SessionUserName + "')";

                    //objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass IN Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass IN Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["GPIN_No"] = txtGPINNo.Text;
            btnSave.Text = "Update";
            Response.Redirect("Trans_GatePassIn_Main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtGPINNo.Text = ""; txtGPINDate.Text = ""; ddlGPOut.SelectedItem.Text = "-Select-";
        ddlGPOut.SelectedValue = "-Select-"; txtSuppName.Text = "";txtSuppDet.Text = "";
        txtSuppInvNo.Text = ""; txtSuppInvDate.Text = ""; txtVehiNo.Text = "";txtNotes.Text = "";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("GPIN_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        TotalReqQty();

    }
    public void TotalReqQty()
    {

        //ReqQty = 0;
        //DataTable dt = new DataTable();
        //dt = (DataTable)ViewState["ItemTable"];
        //for (int i = 0; i < dt.Rows.Count; i++)
        //{
        //    ReqQty = Convert.ToDecimal(ReqQty) + Convert.ToDecimal(dt.Rows[i]["ReuiredQty"]);
        //}
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Trans_GPIN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GPIN_No ='" + txtGPINNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            //txtGPINNo.ReadOnly = true;
            txtGPINNo.Text = Main_DT.Rows[0]["GPIN_No"].ToString();
            txtGPINDate.Text = Main_DT.Rows[0]["GPIN_Date"].ToString();
            ddlGPOut.SelectedItem.Text = Main_DT.Rows[0]["GPOut_No"].ToString();
            txtGPOutDate.Text = Main_DT.Rows[0]["GPOut_Date"].ToString();
            hdSuppCode.Value = Main_DT.Rows[0]["SuppCode"].ToString();
            txtSuppName.Text = Main_DT.Rows[0]["SuppName"].ToString();
            txtSuppDet.Text = Main_DT.Rows[0]["SuppDet"].ToString();
            txtSuppInvNo.Text = Main_DT.Rows[0]["InvNo"].ToString();
            txtSuppInvDate.Text = Main_DT.Rows[0]["InvDate"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Others"].ToString();


            txtTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();
            //txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select ItemCode,ItemName,UOMCode,UOMName,DeptCode,DeptName,WarehouseCode,WarehouseName,RackName,OutQty,INQty,ScrabQty,Rate,";
            SSQL = SSQL + "Value [ItemTotal],GSTType,CGSTPer,CGSTAmt,SGSTPer,SGSTAmt,IGSTPer,IGSTAmt, ";
            SSQL = SSQL + "TotAmt [LineTotal] from Trans_GPIN_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + "FinYearCode = '" + SessionFinYearCode + "' And GPIN_No='" + txtGPINNo.Text + "'";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            TotalReqQty();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_GatePassIn_Main.aspx");
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * From Trans_GPOut_Sub Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And GP_Out_No='" + ddlGPOut.SelectedItem.Text + "' And ";
        SSQL = SSQL + " ItemName='" + ddlItemName.SelectedItem.Text +"' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString(); 
        hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
        hfWarehouseCode.Value = DT.Rows[0]["WareHouseCode"].ToString();
        hfWarehouseName.Value = DT.Rows[0]["WareHouseName"].ToString();
        hfRackNo.Value = DT.Rows[0]["RackName"].ToString();
        hfUOMCode.Value = DT.Rows[0]["UOMCode"].ToString();
        hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();

        txtOutQty.Text = DT.Rows[0]["OutQty"].ToString();
        txtRate.Text= DT.Rows[0]["Rate"].ToString();
    }

    protected void ddlGPOut_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Trans_GPOut_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And Approval_Status='0' and GPOutNo='" + ddlGPOut.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtGPOutDate.Text = DT.Rows[0]["GPOutDate"].ToString();
        txtSuppName.Text = DT.Rows[0]["SuppName"].ToString();
        hdSuppCode.Value = DT.Rows[0]["SuppCode"].ToString();
        //txtPartyInvNo.Text = "";
        //txtPartyInvDate.Text = "";
        txtSuppDet.Text = DT.Rows[0]["SuppDet"].ToString();

        SSQL = "Select ItemCode,ItemName From Trans_GPOut_Sub Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' and GP_Out_No='" + ddlGPOut.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlItemName.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataValueField = "ItemCode";
        ddlItemName.DataTextField = "ItemName";

        ddlItemName.DataBind();
    }

    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
    }
    protected void txtInQty_TextChanged(object sender, EventArgs e)
    {
        TextBox txtTest = ((TextBox)(sender));
        RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

        Label lblOrdQty = (Label)rpi.FindControl("lblOrdQty");
        TextBox txtInQty = (TextBox)rpi.FindControl("txtInQty");
        TextBox lblRate = (TextBox)rpi.FindControl("txtRate");
        Label lblItemTol = (Label)rpi.FindControl("lblItemTot");

        Label lblCGSTPre = (Label)rpi.FindControl("lblCGSTPer");
        Label lblSGSTPre = (Label)rpi.FindControl("lblSGSTPer");
        Label lblIGSTPre = (Label)rpi.FindControl("lblIGSTPer");

        Label lblCGSTAmt = (Label)rpi.FindControl("lblCGSTAmt");
        Label lblSGSTAmt = (Label)rpi.FindControl("lblSGSTAmt");
        Label lblIGSTAmt = (Label)rpi.FindControl("lblIGSTAmt");

        Label lblLineTol = (Label)rpi.FindControl("lblLineTot");

        //Label lblBalQty = (Label)rpi.FindControl("lblBalQty");
        //decimal BalQty = Convert.ToDecimal(lblOrdQty.Text) - Convert.ToDecimal(txtReceivQty.Text);
        //lblBalQty.Text = BalQty.ToString();

        decimal ItemTotal = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtInQty.Text);

        decimal CGSTPer = Convert.ToDecimal(lblCGSTPre.Text.ToString());
        decimal SGSTPer = Convert.ToDecimal(lblSGSTPre.Text.ToString());
        decimal IGSTPer = Convert.ToDecimal(lblIGSTPre.Text.ToString());

        decimal cgst = Convert.ToDecimal(ItemTotal) * (CGSTPer / 100);
        decimal sgst = Convert.ToDecimal(ItemTotal) * (SGSTPer / 100);
        decimal igst = Convert.ToDecimal(ItemTotal) * (IGSTPer / 100);

        LineTot = Convert.ToDecimal(ItemTotal) + Convert.ToDecimal(cgst) + Convert.ToDecimal(sgst) + Convert.ToDecimal(igst);

        //decimal TotalVal = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtReceivQty.Text);

        lblItemTol.Text = ItemTotal.ToString("#.##");
        lblLineTol.Text = LineTot.ToString("#.##");
        if (Convert.ToDecimal(cgst) > Convert.ToDecimal(0.00)) { lblCGSTAmt.Text = cgst.ToString("#.##"); } else { lblCGSTAmt.Text = "0.00"; }
        if (Convert.ToDecimal(sgst) > Convert.ToDecimal(0.00)) { lblSGSTAmt.Text = sgst.ToString("#.##"); } else { lblSGSTAmt.Text = "0.00"; }
        if (Convert.ToDecimal(igst) > Convert.ToDecimal(0.00)) { lblIGSTAmt.Text = igst.ToString("#.##"); } else { lblIGSTAmt.Text = "0.00"; }

        decimal SumQty = 0;
        decimal SumLineTot = 0;

        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            TextBox InQty = Repeater1.Items[i].FindControl("txtInQty") as TextBox;
            Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;

            SumQty = SumQty + Convert.ToDecimal(txtInQty.Text);
            SumLineTot = SumLineTot + Convert.ToDecimal(LineTotal.Text);
        }

        txtTotQty.Text = SumQty.ToString("#.##");
        txtTotAmt.Text = SumLineTot.ToString("#.##");
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        TextBox txtReceiveQyt = e.Item.FindControl("txtReceivQty") as TextBox;
    }

    protected void txtRoundOff_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtRoundOff.Text) < Convert.ToDecimal(0.5))
        {
            txtTotAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) - Convert.ToDecimal(txtRoundOff.Text)).ToString("#.##");
        }
        else
        {
            txtTotAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtRoundOff.Text)).ToString("#.##");
        }
    }

    protected void txtGPInQty_TextChanged(object sender, EventArgs e)
    {
        txtItemVal.Text = Math.Round((Convert.ToDecimal(txtRate.Text) * Convert.ToDecimal(txtGPInQty.Text)),2).ToString();

        txtItemNetAmt.Text = txtItemVal.Text;

        Load_GSTType();
    }

    protected void ddlGST_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select CGST_Per,SGST_Per,IGST_Per from GstMaster where GST_Type='" + ddlGST.SelectedItem.Text + "' And ";
        SSQL = SSQL + " CCode ='" + SessionCcode + "' And LCode ='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfCGSTPre.Value = DT.Rows[0]["CGST_Per"].ToString();
        hfSGSTPre.Value = DT.Rows[0]["SGST_Per"].ToString();
        hfIGSTPre.Value = DT.Rows[0]["IGST_Per"].ToString();


        //decimal cgst = Convert.ToDecimal(ItemTotal) * (CGSTPer / 100);

        txtCGSTAmt.Text = Math.Round((Convert.ToDecimal(txtItemVal.Text) * (Convert.ToDecimal(hfCGSTPre.Value) / 100)), 2).ToString();
        txtSGSTAmt.Text = Math.Round((Convert.ToDecimal(txtItemVal.Text) * (Convert.ToDecimal(hfSGSTPre.Value) / 100)), 2).ToString();
        txtIGSTAmt.Text = Math.Round((Convert.ToDecimal(txtItemVal.Text) * (Convert.ToDecimal(hfIGSTPre.Value) / 100)), 2).ToString();

        txtItemNetAmt.Text = (Convert.ToDecimal(txtItemVal.Text)+ Convert.ToDecimal(txtCGSTAmt.Text)+ Convert.ToDecimal(txtSGSTAmt.Text)+ Convert.ToDecimal(txtIGSTAmt.Text)).ToString();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtGPInQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Return Qty...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["UOMCode"] = hfUOMCode.Value;
                    dr["UOMName"] = hfUOMName.Value;
                    dr["DeptCode"] = hfDeptCode.Value;
                    dr["DeptName"] = hfDeptName.Value;
                    dr["WarehouseCode"] = hfWarehouseCode.Value;
                    dr["WarehouseName"] = hfWarehouseName.Value;
                    dr["RackName"] = hfRackNo.Value;

                    dr["OutQty"] = txtOutQty.Text;
                    dr["INQty"] = txtGPInQty.Text;
                    dr["ScrabQty"] = "0";
                    dr["Rate"] = txtRate.Text;
                    dr["ItemTotal"] = txtItemVal.Text;

                    dr["GSTType"] = ddlGST.SelectedItem.Text;
                    dr["CGSTPer"] = hfCGSTPre.Value;
                    dr["CGSTAmt"] = txtCGSTAmt.Text;
                    dr["SGSTPer"] = hfSGSTPre.Value;
                    dr["SGSTAmt"] = txtSGSTAmt.Text;
                    dr["IGSTPer"] = hfIGSTPre.Value;
                    dr["IGSTAmt"] = txtIGSTAmt.Text;
                    dr["LineTotal"] = txtItemNetAmt.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    TotalInQty();
                    TotalNetAmount();

                    ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                    txtOutQty.Text = "0"; txtGPInQty.Text = "0"; txtRate.Text = "0";
                    hfCGSTPre.Value = "0"; hfSGSTPre.Value = "0"; hfIGSTPre.Value = "0";
                    txtCGSTAmt.Text = "0"; txtSGSTAmt.Text = "0"; txtIGSTAmt.Text = "0";
                    ddlGST.SelectedItem.Text = "-Select-"; txtItemVal.Text = "0.00";txtItemNetAmt.Text = "0.00";
                    //LineTot = 0;ItemTot = 0;
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["UOMCode"] = hfUOMCode.Value;
                dr["UOMName"] = hfUOMName.Value;
                dr["DeptCode"] = hfDeptCode.Value;
                dr["DeptName"] = hfDeptName.Value;
                dr["WarehouseCode"] = hfWarehouseCode.Value;
                dr["WarehouseName"] = hfWarehouseName.Value;
                dr["RackName"] = hfRackNo.Value;

                dr["OutQty"] = txtOutQty.Text;
                dr["INQty"] = txtGPInQty.Text;
                dr["ScrabQty"] = "0";
                dr["Rate"] = txtRate.Text;
                dr["ItemTotal"] = ItemTot.ToString();

                dr["GSTType"] = ddlGST.SelectedItem.Text;
                dr["CGSTPer"] = hfCGSTPre.Value;
                dr["CGSTAmt"] = txtCGSTAmt.Text;
                dr["SGSTPer"] = hfSGSTPre.Value;
                dr["SGSTAmt"] = txtSGSTAmt.Text;
                dr["IGSTPer"] = hfIGSTPre.Value;
                dr["IGSTAmt"] = txtIGSTAmt.Text;
                dr["LineTotal"] = txtItemNetAmt.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                TotalInQty();
                TotalNetAmount();

                ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                ddlGST.SelectedItem.Text = "-Select-"; txtItemVal.Text = "0.00"; txtItemNetAmt.Text = "0.00";
                txtOutQty.Text = "0"; txtGPInQty.Text = "0"; txtRate.Text = "0";
                hfCGSTPre.Value = "0"; hfSGSTPre.Value = "0"; hfIGSTPre.Value = "0";
                txtCGSTAmt.Text = "0"; txtSGSTAmt.Text = "0"; txtIGSTAmt.Text = "0";
                //LineTot = 0; ItemTot = 0;
            }
        }
    }

    public void TotalInQty()
    {
        INQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            INQty = Convert.ToDecimal(INQty) + Convert.ToDecimal(dt.Rows[i]["INQty"]);
        }
        txtTotQty.Text = INQty.ToString();
    }

    public void TotalNetAmount()
    {
        decimal LineTotTst = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            LineTotTst = Convert.ToDecimal(LineTotTst) + Convert.ToDecimal(dt.Rows[i]["LineTotal"]);
        }
        txtTotAmt.Text = LineTotTst.ToString();
    }
    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMName", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("RackName", typeof(string)));
        dt.Columns.Add(new DataColumn("OutQty", typeof(string)));
        dt.Columns.Add(new DataColumn("INQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ScrabQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("GSTType", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }
}