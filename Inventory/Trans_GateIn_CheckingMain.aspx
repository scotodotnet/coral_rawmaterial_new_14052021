﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GateIn_CheckingMain.aspx.cs" Inherits="Inventory_Trans_GateIn_CheckingMain" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />

                        </div>
                        <!-- /.col -->
                    </div>
                    <!--/.row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-map-signs text-primary"></i><span>RFI QC</span></h3>
                                    <div class="box-tools pull-right">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">
                                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false" OnItemCommand="Repeater1_ItemCommand" OnItemDataBound="Repeater2_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Trans No</th>
                                                                <th>Trans Date</th>
                                                                <th>Goods Receipt No</th>
                                                                <th>Goods Receipt Date</th>
                                                                <th>Supplier Name</th>
                                                                <th>QC Checking</th>
                                                                <th>Status</th>
                                                                <th>Edit/Delete/Print</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("TransNo")%></td>
                                                        <td><%# Eval("TransDate")%></td>
                                                        <td><%# Eval("GRNo")%></td>
                                                        <td><%# Eval("GRDate")%></td>
                                                        <td><%# Eval("SuppName")%></td>
                                                        <td><%# Eval("QCPerson")%></td>
                                                        <td><asp:Label ID="lblStatus" style="width:100%" runat="server" class="form-control" Text='<%# Eval("App_Status") %>'></asp:Label></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("TransNo")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("TransNo")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this GateIn Checking details?');">
                                                            </asp:LinkButton>
                                                             <asp:LinkButton ID="btnPrint" class="btn btn-primary btn-sm fa fa-print" runat="server"
                                                        Text="" OnCommand="GridPrintClick" CommandArgument="Edit" CommandName='<%# Eval("TransNo ")%>'>
                                                    </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                    <!-- /.mail-box-messages -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


</asp:Content>

