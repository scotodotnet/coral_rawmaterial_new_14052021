﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_GRN_Convertion_Cost : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionDesigCode = "";
    static Decimal Amnt;
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    bool Rights_Check = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYearCode = Session["FinYearCode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();

            if (!IsPostBack)
            {
                Load_Data_Empty_GRN_No();

            }
            Load_OLD_data();
            //ddlModel.SelectedItem.Text = "-Select-";
            //txtPartsName.Text = "";


        }
    }

    private void Load_OLD_data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Trans_GRN_Conversion_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    private void Load_Data_Empty_GRN_No()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtGRN_No.Items.Clear();
        SSQL = "Select GRNo from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CurrencyType <> 'INR'";
        SSQL = SSQL + " And Approval_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtGRN_No.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["GRNo"] = "-Select-";
        dr["GRNo"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGRN_No.DataTextField = "GRNo";
        txtGRN_No.DataValueField = "GRNo";
        txtGRN_No.DataBind();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ChkStatus = "";
        string query = "";
        DataTable dt = new DataTable();




        if (txtGRN_No.SelectedIndex == 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Select the GRN No...')", true);
            ErrFlg = true;
        }
        if (txtConvertion_Value.Text == "" || txtConvertion_Value.Text == "0.0" || txtConvertion_Value.Text == "0.00" || txtConvertion_Value.Text == "0")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Enter the Conversion Cost...')", true);
            ErrFlg = true;
        }

        DataTable DT = new DataTable();

        if (!ErrFlg)
        {
            string Save_Mode = "Insert";
            SSQL = "Select * from Trans_GRN_Conversion_Det Where GRNo ='" + txtGRN_No.SelectedValue + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count != 0)
            {
                Save_Mode = "Update";
            }


            if (Save_Mode == "Update")
            {
                SSQL = "Update Trans_GRN_Conversion_Det set GRDate='" + txtGRN_Date.Text + "',CurrencyType='" + txtCurrency_Type.Text + "',Conversion_Value='" + txtConvertion_Value.Text + "'";
                SSQL = SSQL + " Where GRNo ='" + txtGRN_No.SelectedValue + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                SSQL = "";
                SSQL = "Insert into Trans_GRN_Conversion_Det(Ccode,Lcode,GRNo,GRDate,CurrencyType,Conversion_Value)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "' ,'" + txtGRN_No.SelectedValue + "','" + txtGRN_Date.Text + "',";
                SSQL = SSQL + " '" + txtCurrency_Type.Text + "','" + txtConvertion_Value.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

            }
            GRN_Rate_Update_To_Stock();

            btnClear_Click(sender, e);
            Load_OLD_data();
            if (Save_Mode == "Update")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Conversion Costing Details Updated Successfully...');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Conversion Costing Details Inserted Successfully...');", true);
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtGRN_No.SelectedIndex = 0;
        txtGRN_Date.Text = "";
        txtCurrency_Type.Text = "";
        txtConvertion_Value.Text = "";
        btnSave.Text = "Save";
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from Trans_GRN_Conversion_Det where GRNo='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            txtGRN_No.SelectedValue = DT.Rows[0]["GRNo"].ToString();
            txtGRN_Date.Text = DT.Rows[0]["GRDate"].ToString();
            txtCurrency_Type.Text = DT.Rows[0]["CurrencyType"].ToString();
            txtConvertion_Value.Text = DT.Rows[0]["Conversion_Value"].ToString();


            btnSave.Text = "Update";
        }
        else
        {
            txtGRN_No.SelectedIndex = 0;
            txtGRN_Date.Text = "";
            txtCurrency_Type.Text = "";
            txtConvertion_Value.Text = "";
            btnSave.Text = "Save";
        }
    }

    protected void txtGRN_No_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_T = new DataTable();

        query = "Select * from Trans_GoodsReceipt_Main where CCode='" + SessionCcode + "' And LCode='" + SessionLcode + "' ";
        query = query + " And GRNo='" + txtGRN_No.SelectedValue + "' ";
        DT_T = objdata.RptEmployeeMultipleDetails(query);
        if (DT_T.Rows.Count != 0)
        {
            txtGRN_Date.Text = DT_T.Rows[0]["GRDate"].ToString();
            txtCurrency_Type.Text = DT_T.Rows[0]["CurrencyType"].ToString();
        }
    }

    private void GRN_Rate_Update_To_Stock()
    {
        DataTable DT_GRN_Main = new DataTable();
        DataTable DT_GRN_Sub = new DataTable();
        DataTable DT_D = new DataTable();
        string query = "";
        query = "Select * from Trans_GoodsReceipt_Main where CCode = '" + SessionCcode + "' And LCode = '" + SessionLcode + "' And GRNo = '" + txtGRN_No.SelectedValue + "'";
        DT_GRN_Main = objdata.RptEmployeeMultipleDetails(query);
        if (DT_GRN_Main.Rows.Count != 0)
        {
            query = "Select * from Trans_GoodsReceipt_Sub where CCode = '" + SessionCcode + "' And LCode = '" + SessionLcode + "' And GRNo = '" + txtGRN_No.SelectedValue + "'";
            DT_GRN_Sub = objdata.RptEmployeeMultipleDetails(query);

            if (DT_GRN_Sub.Rows.Count != 0)
            {
                for (int i = 0; i < DT_GRN_Sub.Rows.Count; i++)
                {
                    //Get Rate And Calculate Value
                    string Euro_Item_Rate = DT_GRN_Sub.Rows[i]["Rate"].ToString();
                    string INR_Item_Rate = (Convert.ToDecimal(Euro_Item_Rate) * Convert.ToDecimal(txtConvertion_Value.Text)).ToString();
                    INR_Item_Rate = (Math.Round(Convert.ToDecimal(INR_Item_Rate), 2, MidpointRounding.AwayFromZero)).ToString();
                    string Item_Received_Qty = DT_GRN_Sub.Rows[i]["ReceiveQty"].ToString();
                    string Item_Total_Amt = (Convert.ToDecimal(INR_Item_Rate) * Convert.ToDecimal(Item_Received_Qty)).ToString();
                    Item_Total_Amt = (Math.Round(Convert.ToDecimal(Item_Total_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    string Discount_Percent = DT_GRN_Sub.Rows[i]["Disc"].ToString();
                    string Discount_Amt_Str = "0";
                    string Item_Final_Amt = "0";
                    Discount_Amt_Str = ((Convert.ToDecimal(Item_Total_Amt) * Convert.ToDecimal(Discount_Percent)) / Convert.ToDecimal(100)).ToString();
                    Discount_Amt_Str = (Math.Round(Convert.ToDecimal(Discount_Amt_Str), 2, MidpointRounding.AwayFromZero)).ToString();

                    Item_Final_Amt = (Convert.ToDecimal(Item_Total_Amt) - Convert.ToDecimal(Discount_Amt_Str)).ToString();
                    Item_Final_Amt = (Math.Round(Convert.ToDecimal(Item_Final_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    //GRN Update
                    query = "Select * from Trans_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                    query = query + " And Trans_No='" + DT_GRN_Sub.Rows[i]["GRNo"].ToString() + "' And ItemCode='" + DT_GRN_Sub.Rows[i]["ItemCode"].ToString() + "'";
                    DT_D = objdata.RptEmployeeMultipleDetails(query);
                    if (DT_D.Rows.Count != 0)
                    {
                        query = "Update Trans_Stock_Ledger_All set Add_Value='" + Item_Final_Amt + "' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                        query = query + " And Trans_No='" + DT_GRN_Sub.Rows[i]["GRNo"].ToString() + "' And ItemCode='" + DT_GRN_Sub.Rows[i]["ItemCode"].ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }

                }

                //RFI QC Update

                query = "Select * from Trans_GIChecking_Sub where CCode = '" + SessionCcode + "' And LCode = '" + SessionLcode + "' ";
                query = query + " And GRNo = '" + txtGRN_No.SelectedValue + "'";

                DT_GRN_Sub = objdata.RptEmployeeMultipleDetails(query);

                if (DT_GRN_Sub.Rows.Count != 0)
                {
                    string RFI_Trans_No = DT_GRN_Sub.Rows[0]["TransNo"].ToString();

                    for (int i = 0; i < DT_GRN_Sub.Rows.Count; i++)
                    {
                        string Euro_Item_Rate = DT_GRN_Sub.Rows[i]["Rate"].ToString();
                        string INR_Item_Rate = (Convert.ToDecimal(Euro_Item_Rate) * Convert.ToDecimal(txtConvertion_Value.Text)).ToString();
                        INR_Item_Rate = (Math.Round(Convert.ToDecimal(INR_Item_Rate), 2, MidpointRounding.AwayFromZero)).ToString();
                        string Item_Received_Qty = DT_GRN_Sub.Rows[i]["InspectQty"].ToString();
                        string Item_Total_Amt = (Convert.ToDecimal(INR_Item_Rate) * Convert.ToDecimal(Item_Received_Qty)).ToString();
                        Item_Total_Amt = (Math.Round(Convert.ToDecimal(Item_Total_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                        string Discount_Percent = DT_GRN_Sub.Rows[i]["DiscPer"].ToString();
                        string Discount_Amt_Str = "0";
                        string Item_Final_Amt = "0";

                        Discount_Amt_Str = ((Convert.ToDecimal(Item_Total_Amt) * Convert.ToDecimal(Discount_Percent)) / Convert.ToDecimal(100)).ToString();
                        Discount_Amt_Str = (Math.Round(Convert.ToDecimal(Discount_Amt_Str), 2, MidpointRounding.AwayFromZero)).ToString();

                        Item_Final_Amt = (Convert.ToDecimal(Item_Total_Amt) - Convert.ToDecimal(Discount_Amt_Str)).ToString();
                        Item_Final_Amt = (Math.Round(Convert.ToDecimal(Item_Final_Amt), 2, MidpointRounding.AwayFromZero)).ToString();


                        //RFI Update
                        query = "Select * from Trans_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                        query = query + " And Trans_No='" + RFI_Trans_No + "' And ItemCode='" + DT_GRN_Sub.Rows[i]["ItemCode"].ToString() + "'";
                        DT_D = objdata.RptEmployeeMultipleDetails(query);
                        if (DT_D.Rows.Count != 0)
                        {
                            query = "Update Trans_Stock_Ledger_All set Add_Value='" + Item_Final_Amt + "' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                            query = query + " And Trans_No='" + RFI_Trans_No + "' And ItemCode='" + DT_GRN_Sub.Rows[i]["ItemCode"].ToString() + "'";
                            objdata.RptEmployeeMultipleDetails(query);
                        }
                    }
                }
            }
        }
    }
}