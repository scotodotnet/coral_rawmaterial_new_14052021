﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Inventory_Trans_Reservation_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionTransNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal IssQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;
    bool ErrFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: BOM Reservation";
            Initial_Data_Referesh();
            Load_Data_RequisitionBy();
            Load_Data_Empty_Generator_Model();
            Load_Data_Empty_Production_Part_Name();
            Load_Data_SapNo();

            Load_Data_WorkOrder();
            Load_Data_WorkOrder_Single();

            if (Session["Reservation_No"] == null)
            {
                SessionTransNo = "";
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "BOM Reservation", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }

                txtTransDate.Text = DateTime.Now.ToShortDateString();
            }
            else
            {
                SessionTransNo = Session["Reservation_No"].ToString();
                txtTransNo.Text = SessionTransNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT_Check = new DataTable();
            string SaveMode = "Insert";
            bool ErrFlag = false;

            GetIPAndName getIPAndName = new GetIPAndName();
            string SysIP = getIPAndName.GetIP();
            string SysName = getIPAndName.GetName();

            
            DT_Check = (DataTable)ViewState["ItemTable"];

            if (DT_Check.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
            }

            //Auto generate Transaction Function Call

            if (btnSave.Text != "Update")
            {
                if (!ErrFlag)
                {
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "BOM Reservation", SessionFinYearVal, "1");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        txtTransNo.Text = Auto_Transaction_No;
                    }
                }
            }

            if (!ErrFlag)
            {
                //Insert Main Table

                if (btnSave.Text == "Save")
                {

                    SSQL = "Insert Into Trans_Reservation_Main(Ccode,Lcode,FinYearCode,FinYearVal,TransNo,TransDate,RequisitionBy,Remarks,";
                    SSQL = SSQL + " TotalRequisitionQty,Approval_Status,Status,UserID,UserName,SysIP,SysName,CreateOn) Values( ";
                    SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtTransNo.Text + "','" + txtTransDate.Text + "','" + ddlReserby.SelectedValue + "',";
                    SSQL = SSQL + " '" + txtRemarks.Text + "','" + lblTotQty.Text + "','0','Add','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " '" + SysIP + "','" + SysName + "',Convert(Datetime,GetDate(),103))";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    DataTable dt = new DataTable();
                    dt = (DataTable)ViewState["ItemTable"];

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SSQL = "Insert Into Trans_Reservation_Sub(Ccode,Lcode,FinYearCode,FinYearVal,TransNo,TransDate,Mat_Type,WorkOrderNo,";
                        SSQL = SSQL + " StockType,GenModelCode,GenModelName,GenModelPart,ProductionStage,SapNo,ItemCode,ItemName,UOMName,";
                        SSQL = SSQL + " RequisitionQty,Rate,Value,UserID,UserName,Status) Values( '" + SessionCcode + "', ";
                        SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                        SSQL = SSQL + " '" + txtTransNo.Text + "','" + txtTransDate.Text + "','1','" + dt.Rows[i]["WorkOrderNo"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["StockType"].ToString() + "','" + dt.Rows[i]["GenModelCode"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["GenModelName"].ToString() + "','" + dt.Rows[i]["GenModelPart"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["ProductionStage"].ToString() + "','" + dt.Rows[i]["SapNo"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["UOMName"].ToString() + "','" + dt.Rows[i]["ReservationQty"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "',";
                        SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','Add')";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "BOM Reservation", SessionFinYearVal, "1");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        txtTransNo.Text = Auto_Transaction_No;
                    }
                }
                else
                {
                    SSQL = "Delete from Trans_Reservation_Main where TransNo='" + txtTransNo.Text + "' and Ccode ='" + SessionCcode + "' and ";
                    SSQL = SSQL + " Lcode='" + SessionLcode + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Delete from Trans_Reservation_Sub where TransNo='" + txtTransNo.Text + "' and Ccode ='" + SessionCcode + "' and ";
                    SSQL = SSQL + " Lcode='" + SessionLcode + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Insert Into Trans_Reservation_Main(Ccode,Lcode,FinYearCode,FinYearVal,TransNo,TransDate,RequisitionBy,Remarks,";
                    SSQL = SSQL + " TotalRequisitionQty,Approval_Status,Status,UserID,UserName,SysIP,SysName,CreateOn) Values( ";
                    SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtTransNo.Text + "','" + txtTransDate.Text + "','" + ddlReserby.SelectedValue + "',";
                    SSQL = SSQL + " '" + txtRemarks.Text + "','" + lblTotQty.Text + "','0','Add','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " '" + SysIP + "','" + SysName + "',Convert(Datetime,GetDate(),103))";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    DataTable dt = new DataTable();
                    dt = (DataTable)ViewState["ItemTable"];

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SSQL = "Insert Into Trans_Reservation_Sub(Ccode,Lcode,FinYearCode,FinYearVal,TransNo,TransDate,Mat_Type,WorkOrderNo,";
                        SSQL = SSQL + " StockType,GenModelCode,GenModelName,GenModelPart,ProductionStage,SapNo,ItemCode,ItemName,UOMName,";
                        SSQL = SSQL + " RequisitionQty,Rate,Value,UserID,UserName,Status) Values( '" + SessionCcode + "', ";
                        SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                        SSQL = SSQL + " '" + txtTransNo.Text + "','" + txtTransDate.Text + "','1','" + dt.Rows[i]["WorkOrderNo"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["StockType"].ToString() + "','" + dt.Rows[i]["GenModelCode"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["GenModelName"].ToString() + "','" + dt.Rows[i]["GenModelPart"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["ProductionStage"].ToString() + "','" + dt.Rows[i]["SapNo"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["UOMName"].ToString() + "','" + dt.Rows[i]["ReservationQty"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "',";
                        SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','Add')";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "BOM Reservation", SessionFinYearVal, "1");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        txtTransNo.Text = Auto_Transaction_No;
                    }
                }
                 
                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Reservation Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Reservation Details Updated Successfully');", true);
                }

                Session["Reservation_No"] = txtTransNo.Text;                
                Response.Redirect("Trans_Reservation_Main.aspx");
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Load_Data_RequisitionBy()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select FirstName as EmployeeName from CORAL_ERP_HR..Employee_Mst where CompCode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LocCode='" + SessionLcode + "' And isactive!='No' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlReserby.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["EmployeeName"] = "-Select-";
        
        DT.Rows.InsertAt(dr, 0);

        ddlReserby.DataValueField = "EmployeeName";
        ddlReserby.DataTextField = "EmployeeName";
        ddlReserby.DataBind();
    }

    private void Clear_All_Field()
    {
        txtTransNo.Text = ""; txtTransDate.Text = "";
        btnSave.Text = "Save";
        Initial_Data_Referesh();
    }


    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        if (btnSave.Text == "Save")
        {

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
                {
                    dt.Rows.RemoveAt(i);
                    dt.AcceptChanges();
                }
            }
            ViewState["ItemTable"] = dt;
            Load_OLD_data();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Do Not Delete in Edit Mode');", true);
        }
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        rptBOMReser.DataSource = dt;
        rptBOMReser.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //Search Purchase Enquiry
            string SSQL = "";
            DataTable Main_DT = new DataTable();
            SSQL = "Select * from Trans_Reservation_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And TransNo ='" + txtTransNo.Text + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Main_DT.Rows.Count != 0)
            {

                txtTransNo.Text = Main_DT.Rows[0]["TransNo"].ToString();
                txtTransDate.Text = Main_DT.Rows[0]["TransDate"].ToString();
                ddlReserby.SelectedValue = Main_DT.Rows[0]["RequisitionBy"].ToString();
                txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

                //Pur_Enq_Main_Sub Table Load
                DataTable dt = new DataTable();
                SSQL = "Select WorkOrderNo,StockType,GenModelCode,GenModelName,GenModelPart,ProductionStage,SapNo,ItemCode,";
                SSQL = SSQL + " ItemName,UOMName,RequisitionQty[ReservationQty],Rate,Value From Trans_Reservation_Sub";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode = '" + SessionFinYearCode + "' And TransNo='" + txtTransNo.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if(dt.Rows[0]["StockType"].ToString()== "Requisition")
                {
                    ddlSelectType.SelectedIndex = 0;
                }
                else
                {
                    ddlSelectType.SelectedIndex = 1;
                }

                ViewState["ItemTable"] = dt;
                rptBOMReser.DataSource = dt;
                rptBOMReser.DataBind();

                btnSave.Text = "Update";
            }
            else
            {
                Clear_All_Field();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_Reservation_Main.aspx");
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Old_New_Type = "1";

      
        string SSQL = "";
        DataTable DT = new DataTable();
        string IssueType = "";

        SSQL = "Select UOMTypeCode[UOMCode],UOM_Full[UOMName],DeptCode,DeptName,WarehouseCode,WarehouseName,RackSerious ";
        SSQL = SSQL + " from BOMMaster Where Raw_Mat_Name ='" + ddlItemName.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();

        if (Old_New_Type == "1")
        {
            SSQL = "Select (sum(Add_Qty)-sum(Minus_Qty)) StockQty,(sum(Add_Value)-sum(Minus_Value)) stockVal,";
            SSQL = SSQL + " ((sum(Add_Value)-sum(Minus_Value))/(sum(Add_Qty)-sum(Minus_Qty))) Rate  from Trans_Stock_Ledger_All ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And  Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "' having (sum(Add_Qty)-sum(Minus_Qty))>0";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Old_New_Type == "2")
        {
            SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }

        if (DT.Rows.Count != 0)
        {
            txtStockQty.Text = DT.Rows[0]["StockQty"].ToString();
            //txtRate.Text = DT.Rows[0]["Rate"].ToString();
        }
        else
        {
            txtStockQty.Text = "0.00";

        }

    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtIssueQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Return Qty...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["WorkOrderNo"] = ddlWorkOrdNo.SelectedValue;
                    dr["StockType"] = "Requisition";
                    dr["GenModelCode"] = hfWOGenMdlCode.Value;
                    dr["GenModelName"] = lblWOGenMdlName.Text;
                    dr["GenModelPart"] = lblWOGenPart.Text;
                    dr["ProductionStage"] = ddlWOPrdStage.SelectedValue;
                    dr["SapNo"] = ddlSapNo.SelectedItem.Text;
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["UOMName"] = hfUOMName.Value;
                    dr["ReservationQty"] = txtIssueQty.Text;
                    dr["Rate"] = "0.00";
                    dr["Value"] = "0.00";

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    rptBOMReser.DataSource = dt;
                    rptBOMReser.DataBind();

                    lblWOGenMdlName.Text = "";
                    lblWOGenPart.Text = "";
                    ddlSapNo.SelectedIndex = 0;
                    ddlItemName.SelectedIndex = 0;
                    txtStockQty.Text = "0"; txtIssueQty.Text = "0";
                }
            }
            else
            {
                dr = dt.NewRow();

                dr["WorkOrderNo"] = ddlWorkOrdNo.SelectedValue;
                dr["StockType"] = "Requisition";
                dr["GenModelCode"] = hfWOGenMdlCode.Value;
                dr["GenModelName"] = lblWOGenMdlName.Text;
                dr["GenModelPart"] = lblWOGenPart.Text;
                dr["ProductionStage"] = ddlWOPrdStage.SelectedValue;
                dr["SapNo"] = ddlSapNo.SelectedItem.Text;
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["UOMName"] = hfUOMName.Value; 
                dr["ReservationQty"] = txtIssueQty.Text;
                dr["Rate"] = "0.00";
                dr["Value"] = "0.00";

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                rptBOMReser.DataSource = dt;
                rptBOMReser.DataBind();

                lblWOGenMdlName.Text = "";
                lblWOGenPart.Text = "";
                ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                txtStockQty.Text = "0"; txtIssueQty.Text = "0";
            }
        }
    }

   

    protected void txtIssueQty_TextChanged(object sender, EventArgs e)
    {
        //if (Convert.ToDecimal(txtIssueQty.Text) > Convert.ToDecimal(txtStockQty.Text))
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Issue Qty. is Higher than Stock Qty');", true);
        //    txtIssueQty.Text = "0";
        //}
        //if (!ErrFlag)
        //{
        //    if (Convert.ToDecimal(txtRate.Text) > 0)
        //    {
        //        txtValue.Text = Math.Round((Convert.ToDecimal(txtRate.Text) * Convert.ToDecimal(txtIssueQty.Text)), 2).ToString();
        //    }
        //}
        //else
        //{
        //    txtValue.Text = "0";
        //}
    }

    protected void txtReserveQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            ((TextBox)rptBOMReser.Items[index].FindControl("txtReserveQty")).Focus();

        }
        catch (Exception ex)
        {

        }
    }

    private void Load_Data_Empty_Generator_Model()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();


        SSQL = "Select GenModelNo,GenModelName from Coral_ERP_Sales..GeneratorModels where Ccode='" + SessionCcode + "' And ";
        SSQL = SSQL + " Lcode='" + SessionLcode + "' And  Status='Add'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //txtGenerator_Model.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();
        //dr["GenModelNo"] = "-Select-";
        //dr["GenModelName"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        //txtGenerator_Model.DataTextField = "GenModelName";
        //txtGenerator_Model.DataValueField = "GenModelNo";
        //txtGenerator_Model.DataBind();

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("WorkOrderNo", typeof(string)));
        dt.Columns.Add(new DataColumn("StockType", typeof(string)));
        dt.Columns.Add(new DataColumn("GenModelCode", typeof(string)));
        dt.Columns.Add(new DataColumn("GenModelName", typeof(string)));
        dt.Columns.Add(new DataColumn("GenModelPart", typeof(string)));
        dt.Columns.Add(new DataColumn("ProductionStage", typeof(string)));
        dt.Columns.Add(new DataColumn("SapNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMName", typeof(string)));
        dt.Columns.Add(new DataColumn("ReservationQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));

        rptBOMReser.DataSource = dt;
        rptBOMReser.DataBind();
        ViewState["ItemTable"] = rptBOMReser.DataSource;
    }

    protected void ddlWOPartName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL;
            DataTable DT = new DataTable();
            DataTable DT_Tmp = new DataTable();
            DataRow dr = null;

            SSQL = "Select * from Trans_GenerateWorkOrder_Sub where Work_Order_No='" + ddlWorkOrdNo.SelectedItem.Text + "' ";
            SSQL = SSQL + " and Production_Stage='" + ddlWOPrdStage.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    DT_Tmp = (DataTable)ViewState["ItemTable"];

                    dr = DT_Tmp.NewRow();
                    dr["WorkOrderNo"] = ddlWorkOrdNo.SelectedValue;
                    dr["StockType"] = "Requisition";
                    dr["GenModelCode"] = hfWOGenMdlCode.Value;
                    dr["GenModelName"] = lblWOGenMdlName.Text;
                    dr["GenModelPart"] = lblWOGenPart.Text;
                    dr["ProductionStage"] = ddlWOPrdStage.SelectedValue;
                    dr["SapNo"] = DT.Rows[i]["SAP_No"].ToString();
                    dr["ItemCode"] = DT.Rows[i]["Mat_No"].ToString();
                    dr["ItemName"] = DT.Rows[i]["Item_Desc"].ToString();
                    dr["UOMName"] = DT.Rows[i]["UOM"].ToString();
                    dr["ReservationQty"] = Math.Round(Convert.ToDecimal(DT.Rows[i]["Req_Qty"].ToString()), 2, MidpointRounding.AwayFromZero).ToString();
                    dr["Rate"] = Math.Round(Convert.ToDecimal(DT.Rows[i]["Item_Rate"].ToString()), 2, MidpointRounding.AwayFromZero).ToString();
                    dr["Value"] = "0.00";
                    DT_Tmp.Rows.Add(dr);
                }

                ViewState["ItemTable"] = DT_Tmp;
                rptBOMReser.DataSource = DT_Tmp;
                rptBOMReser.DataBind();

                Grid_Total_Calculate();

                ddlWorkOrdNo.SelectedIndex = 0;
                ddlWOPrdStage.SelectedIndex = 0;
                lblWOGenMdlName.Text = "";
                lblWOGenPart.Text = "";
            }
        }

        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }


    private void Load_BoM_Mat_ItemTable()
    {
        string SSQL = "";
        Initial_Data_Referesh();
        DataTable DT = new DataTable();

        SSQL = "Select BOM.Mat_No as ItemCode,BOM.Raw_Mat_Name as ItemName,BOM.DeptCode,BOM.DeptName,BOM.WarehouseCode,BOM.WarehouseName,";
        SSQL = SSQL + " BOM.RackSerious as RacKNo,BOM.UOMTypeCode as UOMCode,BOM.UOM_Full UOMName,(sum(STK.Add_Qty)-sum(STK.Minus_Qty)) StockQty,";
        SSQL = SSQL + " BOM.RequiredQty as IssueQty, BOM.Amount as Rate,(BOM.RequiredQty*BOM.Amount)Value from BOMMaster BOM ";
        SSQL = SSQL + " Inner Join Trans_Stock_Ledger_All STK on STK.ItemName=BOM.Raw_Mat_Name and STK.ItemCode=BOM.Mat_No ";
        SSQL = SSQL + " Where BOM.Ccode='" + SessionCcode + "' And BOM.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " BOM.GenModelNo ='" + lblWOGenMdlName.Text + "' ";

        if (lblWOGenPart.Text != "")
        {
            SSQL = SSQL + " And BOM.ProductionPartNo='" + lblWOGenPart.Text + "'";
        }

        SSQL = SSQL + " Group by BOM.Mat_No,BOM.Raw_Mat_Name,BOM.DeptCode,BOM.DeptName,BOM.WarehouseCode,BOM.WarehouseName,BOM.RackSerious,";
        SSQL = SSQL + " BOM.UOMTypeCode,BOM.UOM_Full,BOM.RequiredQty,BOM.Amount having (sum(STK.Add_Qty)-sum(STK.Minus_Qty))>0";

        //SSQL = "Select Mat_No as ItemCode,Raw_Mat_Name as ItemName,UOM_Full as UOMCode,RequiredQty as ReuiredQty,";
        //SSQL = SSQL + " Amount as Item_Rate from BOMMaster ";
        //SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " GenModelNo ='" + txtGenerator_Model.SelectedValue + "'";

        //if (ddlWOPartName.SelectedValue != "ALL")
        //{
        //    SSQL = SSQL + " And ProductionPartNo='" + ddlWOPartName.SelectedValue + "'";
        //}
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ViewState["ItemTable"] = DT;
        rptBOMReser.DataSource = DT;
        rptBOMReser.DataBind();
        TotalReqQty();
    }

    public void TotalReqQty()
    {
        IssQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IssQty = Convert.ToDecimal(IssQty) + Convert.ToDecimal(dt.Rows[i]["IssueQty"]);
        }
    }

    protected void txtGenerator_Model_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_Production_Part_Name();
        //Load_Data_Empty_ItemCode();
        //Load_ItemStock();
    }

    private void Load_Data_Empty_Production_Part_Name()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        //SSQL = "Select PartsName[PartName] from CORAL_ERP_Sales..PartsType where Ccode='" + SessionCcode + "' ";
        //SSQL = SSQL + " And Lcode='" + SessionLcode + "' And GenModelName='" + txtGenerator_Model.SelectedItem.Text + "' and Status='Add'";

        //Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //ddlWOPartName.DataSource = Main_DT;

        //DataRow dr = Main_DT.NewRow();
        //dr["PartName"] = "-Select-";

        //Main_DT.Rows.InsertAt(dr, 0);

        //ddlWOPartName.DataTextField = "PartName";
        //ddlWOPartName.DataValueField = "PartName";
        //ddlWOPartName.DataBind();
    }

    private void Load_Data_SapNo()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select Mat_No[ItemCode],Sap_No[SapNo] from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And Status!='Delete' order by Cast(Sap_No as int) ";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlSapNo.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["SapNo"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);

        ddlSapNo.DataTextField = "SapNo";
        ddlSapNo.DataValueField = "ItemCode";
        ddlSapNo.DataBind();
    }

    protected void ddlSapNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string Old_New_Type = "1";
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select Mat_No[ItemCode],UOMTypeCode[UOMCode],UOM_Full[UOMName],DeptCode,DeptName,WarehouseCode,WarehouseName,RackSerious ";
            SSQL = SSQL + " from BOMMaster Where Sap_No ='" + ddlSapNo.SelectedItem.Text + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlItemName.SelectedValue = DT.Rows[0]["ItemCode"].ToString();
            //hfUOMCode.Value = DT.Rows[0]["UOMCode"].ToString();
            hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();
            //hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
            //hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
            //hfWarehouseCode.Value = DT.Rows[0]["WarehouseCode"].ToString();
            //hfWarehouseName.Value = DT.Rows[0]["WarehouseName"].ToString();
            //hfRackNo.Value = DT.Rows[0]["RackSerious"].ToString();

            if (Old_New_Type == "1")
            {
                SSQL = "Select (sum(Add_Qty)-sum(Minus_Qty)) StockQty,(sum(Add_Value)-sum(Minus_Value)) stockVal,";
                SSQL = SSQL + " ((sum(Add_Value)-sum(Minus_Value))/(sum(Add_Qty)-sum(Minus_Qty))) Rate  from Trans_Stock_Ledger_All ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And  Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SapNo='" + ddlSapNo.SelectedItem.Text + "' having (sum(Add_Qty)-sum(Minus_Qty))>0";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (Old_New_Type == "2")
            {
                SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And SapNo='" + ddlSapNo.SelectedItem.Text + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (DT.Rows.Count != 0)
            {
                txtStockQty.Text = DT.Rows[0]["StockQty"].ToString();
                //txtRate.Text = DT.Rows[0]["Rate"].ToString();
            }
            else
            {
                txtStockQty.Text = "0.00";

            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void ddlSelectType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSelectType.SelectedItem.Text == "WorkOrder Based")
        {
            pnlWorkOrderBase.Visible = true;
            pnlReserSingle.Visible = false;
        }
        else
        {
            pnlWorkOrderBase.Visible = false;
            pnlReserSingle.Visible = true;
        }
    }

    private void Load_Data_WorkOrder()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select Work_Order_No from Trans_GenerateWorkOrder_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And Status!='Delete' And ApproveStatus='1' ";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlWorkOrdNo.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["Work_Order_No"] = "-Select-";

        Main_DT.Rows.InsertAt(dr, 0);
        ddlWorkOrdNo.DataTextField = "Work_Order_No";
        ddlWorkOrdNo.DataValueField = "Work_Order_No";
        ddlWorkOrdNo.DataBind();
    }

    private void Load_Data_WorkOrder_Single()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select Work_Order_No from Trans_GenerateWorkOrder_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And Status!='Delete' And ApproveStatus='1' ";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlSingWorkOrdNo.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["Work_Order_No"] = "-Select-";

        Main_DT.Rows.InsertAt(dr, 0);
        ddlSingWorkOrdNo.DataTextField = "Work_Order_No";
        ddlSingWorkOrdNo.DataValueField = "Work_Order_No";
        ddlSingWorkOrdNo.DataBind();
    }

    protected void ddlWorkOrdNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_GenerateWorkOrder_Main where Work_Order_No='" + ddlWorkOrdNo.SelectedItem.Text + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                lblWOGenMdlName.Text = DT.Rows[0]["GenModelName"].ToString();
                hfWOGenMdlCode.Value = DT.Rows[0]["GenModelCode"].ToString();
                lblWOGenPart.Text = DT.Rows[0]["PartType"].ToString();

                DataTable DT_Part = new DataTable();

                SSQL = "Select Production_Stage from Trans_GenerateWorkOrder_Sub where Work_Order_No='" + ddlWorkOrdNo.SelectedItem.Text + "'";
                SSQL = SSQL + " group by Production_Stage ";

                DT_Part = objdata.RptEmployeeMultipleDetails(SSQL);

                ddlWOPrdStage.DataSource = DT_Part;

                DataRow dr = DT_Part.NewRow();
                dr["Production_Stage"] = "-Select-";

                DT_Part.Rows.InsertAt(dr, 0);
                ddlWOPrdStage.DataTextField = "Production_Stage";
                ddlWOPrdStage.DataValueField = "Production_Stage";
                ddlWOPrdStage.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Grid_Total_Calculate()
    {
        try
        {
            decimal SumQtyTot = 0;
            for (int i = 0; i < rptBOMReser.Items.Count; i++)
            {
                TextBox txtGrdReserveQty = rptBOMReser.Items[i].FindControl("txtReserveQty") as TextBox;
                SumQtyTot = SumQtyTot + Convert.ToDecimal(txtGrdReserveQty.Text);
            }
            lblTotQty.Text = Math.Round(SumQtyTot, 2, MidpointRounding.AwayFromZero).ToString();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Text_Change_Value_Cal(object sender, int Row_No)
    {
        try
        {
            string Qty_Val = "0";
            int i = Row_No;

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];

            TextBox txtGrdOrdQty = rptBOMReser.Items[i].FindControl("txtReserveQty") as TextBox;

            if (txtGrdOrdQty.Text.ToString() != "")
            {
                TextBox txtTest = ((TextBox)(sender));
                RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

                Label lblGrdWorkOrdNo = rptBOMReser.Items[i].FindControl("lblWorkOrdNo") as Label;
                Label lblGrdStkType = rptBOMReser.Items[i].FindControl("lblStkType") as Label;
                Label lblGrdGenMdlCode = rptBOMReser.Items[i].FindControl("lblGenMdlCode") as Label;
                Label lblGrdGenMdlName = rptBOMReser.Items[i].FindControl("lblGenMdlName") as Label;
                Label lblGrdGenPartName = rptBOMReser.Items[i].FindControl("lblGenPartName") as Label;
                Label lblGrdPrdStage = rptBOMReser.Items[i].FindControl("lblPrdStage") as Label;
                Label lblGrdSAPNo = rptBOMReser.Items[i].FindControl("lblSapNo") as Label;
                Label lblGrdItemCode = rptBOMReser.Items[i].FindControl("lblItemCode") as Label;
                Label lblGrdItemName = rptBOMReser.Items[i].FindControl("lblItemName") as Label;
                Label lblGrdUOM = rptBOMReser.Items[i].FindControl("lblUOMName") as Label;
                TextBox txtGrdReserveQty = rptBOMReser.Items[i].FindControl("txtReserveQty") as TextBox;
                Label lblGrdRate = rptBOMReser.Items[i].FindControl("lblRate") as Label;
                Label lblGrdValue = rptBOMReser.Items[i].FindControl("lblValue") as Label;
                

                if (txtGrdReserveQty.Text != "") { Qty_Val = txtGrdReserveQty.Text.ToString(); }

                dt.Rows[i]["WorkOrderNo"] = lblGrdWorkOrdNo.Text;
                dt.Rows[i]["StockType"] = lblGrdStkType.Text;
                dt.Rows[i]["GenModelCode"] = lblGrdGenMdlCode.Text;
                dt.Rows[i]["GenModelName"] = lblGrdGenMdlName.Text;
                dt.Rows[i]["GenModelPart"] = lblGrdGenPartName.Text;
                dt.Rows[i]["ProductionStage"] = lblGrdPrdStage.Text;
                dt.Rows[i]["SapNo"] = lblGrdSAPNo.Text;
                dt.Rows[i]["ItemCode"] = lblGrdItemCode.Text;
                dt.Rows[i]["ItemName"] = lblGrdItemName.Text;
                dt.Rows[i]["UOMName"] = lblGrdUOM.Text;
                dt.Rows[i]["ReservationQty"] = txtGrdReserveQty.Text;
                dt.Rows[i]["Rate"] = lblGrdRate.Text;
                dt.Rows[i]["Value"] = lblGrdValue.Text;

                Grid_Total_Calculate();
                
            }

            ViewState["ItemTable"] = dt;
            rptBOMReser.DataSource = dt;
            rptBOMReser.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
}