﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_PO_GPIN_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: GatePass In";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_Enquiry_Grid();
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "1", "Purchase GatePass IN");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Add..');", true);
        }
        else
        {
            //Session.Remove("Pur_RequestNo_Approval");
            //Session.Remove("Pur_Request_No_Amend_Approval");
            Session.Remove("Pur_GIN_No");
            Response.Redirect("Trans_PO_GPIN_Sub.aspx");
        }

        //Response.Redirect("Trans_GatePassIn_Sub.aspx");
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool Rights_Check = false;
        bool ErrFlag = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "1", "Purchase GatePass IN");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Edit..');", true);
        }

        DataTable dtdpurchase = new DataTable();

        SSQL = "select ApprovalStatus from Trans_Pur_GIN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Pur_GIN_No='" + e.CommandName.ToString() + "'";

        dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);

        string status = dtdpurchase.Rows[0]["ApprovalStatus"].ToString();

        if (!ErrFlag)
        {
            if (status == "" || status == "0")
            {
                string Enquiry_No_Str = e.CommandName.ToString();
                Session.Remove("Pur_RequestNo_Approval");
                Session.Remove("Pur_Request_No_Amend_Approval");
                Session.Remove("Pur_GIN_No");
                Session["Pur_GIN_No"] = Enquiry_No_Str;
                Response.Redirect("Trans_PO_GPIN_Sub.aspx");
            }
            else if (status == "2")
            {
                ErrFlag = true;

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the UserID ');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass In Details Already Rejected..')", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass In Details Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass In Details put in pending..')", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass In Details put in pending..');", true);
            }
            else if (status == "1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass In Details Already Approved Cant Edit..')", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved GatePass In Cant Edit..');", true);
            }

        }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "1", "Purchase GatePass IN");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        SSQL = "Select * from Trans_Pur_GIN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + "FinYearCode = '" + SessionFinYearCode + "' And Pur_GIN_No='" + e.CommandName.ToString() + "' And ApprovalStatus='1'";

        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Do not Delete GatePass In Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable dtdpurchase = new DataTable();
            SSQL = "select ApprovalStatus from Trans_Pur_GIN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + "And FinYearCode ='" + SessionFinYearCode + "' And Pur_GIN_No='" + e.CommandName.ToString() + "'";

            dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
            string status = dtdpurchase.Rows[0]["ApprovalStatus"].ToString();

            if (status == "" || status == "0")
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Pur_GIN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Pur_GIN_No='" + e.CommandName.ToString() + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    //Delete Main Table
                    SSQL = "Update Trans_Pur_GIN_Main set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_GIN_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    //Delete Main Sub Table
                    SSQL = "Update Trans_Pur_GIN_Sub set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_GIN_No='" + e.CommandName.ToString() + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass In Details Deleted Successfully');", true);
                    Load_Data_Enquiry_Grid();
                }
            }
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass In Details Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass In Details put in pending..');", true);
            }

        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select Case when ApprovalStatus='0' then 'Approval Pending'  ";
        SSQL=SSQL+" when ApprovalStatus='1' then 'Approval Success' end App_Status,* from Trans_Pur_GIN_Main where Status!='Delete' And ";
        SSQL = SSQL + "Ccode = '" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblStatus = Item.FindControl("lblStatus") as Label;

            if (lblStatus.Text == "Approval Success")
            {
                lblStatus.BackColor = Color.Green;
                lblStatus.ForeColor = Color.White;

            }
            else
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.ForeColor = Color.White;
            }
        }
    }
}