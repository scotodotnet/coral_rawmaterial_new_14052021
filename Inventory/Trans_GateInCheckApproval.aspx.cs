﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Transaction_Trans_GateInCheckApproval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionUserType;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        SessionUserType = Session["RoleCode"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: GateIn Checking Approval";
            //Department_Code_Add();
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();
        }
        Load_Data_Empty_Grid();
    }

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }
    private void Load_Data_Empty_Grid()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            DataTable DT2 = new DataTable();

            DT.Columns.Add("TransNo");
            DT.Columns.Add("PONo");
            DT.Columns.Add("PODate");
            DT.Columns.Add("SuppName");
            DT.Columns.Add("MaterialType");

            if (txtRequestStatus.Text == "Pending List")
            {
                SSQL = "Select TransNo,GRNo,GRDate,SuppName,SuppInvNo,SuppInvDate From Trans_GIChecking_Main  ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";
                SSQL = SSQL + " And Status!='Delete' Order By TransNo desc";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Pending.DataSource = DT;
                Repeater_Pending.DataBind();
                Repeater_Pending.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Approved List")
            {
                SSQL = "Select TransNo,GRNo,GRDate,SuppName,SuppInvNo,SuppInvDate From Trans_GIChecking_Main  ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And Approval_Status = '1' ";
               
                SSQL = SSQL + " And Status!='Delete' Order By TransNo desc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Approve.DataSource = DT;
                Repeater_Approve.DataBind();
                Repeater_Approve.Visible = true;
                Repeater_Pending.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Rejected List")
            {

                SSQL = "Select TransNo,GRNo,GRDate,SuppName,SuppInvNo,SuppInvDate From Trans_GIChecking_Main  ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And Approval_Status = '-1' And Status!='Delete' Order By TransNo desc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Rejected.DataSource = DT;
                Repeater_Rejected.DataBind();
                Repeater_Rejected.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Pending.Visible = false;
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "1", "RFI QC Approval");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Request..');", true);
        }
        //User Rights Check End


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_GIChecking_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And TransNo='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Approval_Status = '-1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('CheckIn Details Already Cancelled.. Cannot Approved it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            SSQL = "Select * from Trans_GIChecking_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And TransNo='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Approval_Status = '1'";
           
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

           if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('CheckIn Details Already get Approved..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_GIChecking_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And TransNo='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Approval_Status = '1'";
           
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count == 0)
            {
                SSQL = "Update Trans_GIChecking_Main Set ";
                SSQL = SSQL + "  Approval_Status = '1',ApprovedBy='" + SessionUserName + "',ApprovedDate=Convert(Datetime,GetDate(),103) ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And FinYearCode ='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' And  TransNo ='" + e.CommandArgument.ToString() + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                //Stock Update
                DataTable DT_SUB = new DataTable();
                DataTable DT_Main = new DataTable();
                string TransNo = "";
                string TransDate = "";
                string SuppCode = "";
                string SuppName = "";
                string SuppType = "";
                string MatType = "";
                string CurType = "";


                TransNo = e.CommandArgument.ToString();

                SSQL = "Select * from Trans_GIChecking_Main where TransNo='" + TransNo + "' ";

                DT_Main = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Select * from Trans_GIChecking_Sub where TransNo='" + TransNo + "' ";

                DT_SUB = objdata.RptEmployeeMultipleDetails(SSQL);

                TransDate = DT_Main.Rows[0]["TransDate"].ToString();
                SuppCode = DT_Main.Rows[0]["SuppCode"].ToString();
                SuppName = DT_Main.Rows[0]["SuppName"].ToString();
                CurType = DT_Main.Rows[0]["CurrencyType"].ToString();

                if (DT_Main.Rows[0]["AccountType"].ToString() == "Escrow")
                {
                    SuppType = "2";
                }
                else if (DT_Main.Rows[0]["AccountType"].ToString() == "Coral")
                {
                    SuppType = "3";
                }

                if (DT_Main.Rows[0]["MaterialType"].ToString() == "RawMaterial")
                {
                    MatType = "1";
                }
                else if (DT_Main.Rows[0]["MaterialType"].ToString() == "Tools")
                {
                    MatType = "2";
                }
                else if (DT_Main.Rows[0]["MaterialType"].ToString() == "Asset")
                {
                    MatType = "3";
                }
                else if (DT_Main.Rows[0]["MaterialType"].ToString() == "General Item")
                {
                    MatType = "4";
                }

                for (int i = 0; i < DT_SUB.Rows.Count; i++)
                {

                    //Restricted Stock Move To Direct Stock (Add Qty)
                    SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                    SSQL = SSQL + " Trans_Type,Supp_Type,Mat_Type,Supp_Code,Supp_Name,SapNo,ItemCode,ItemName,DeptCode,DeptName,UOM,";
                    SSQL = SSQL + " WarehouseCode,WarehouseName,Add_Qty,Add_Value,Minus_Qty,Minus_Value,UserID,UserName,CurrencyType,ReceiveType)";
                    SSQL = SSQL + " Values( '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                    SSQL = SSQL + " '" + SessionFinYearVal + "', '" + TransNo + "','" + TransDate.ToString() + "','" + TransDate + "',";
                    SSQL = SSQL + " 'Goods Received','" + SuppType + "','" + MatType + "', '" + SuppCode + "','" + SuppName + "',";
                    SSQL = SSQL + " '" + DT_SUB.Rows[i]["SapNo"].ToString() + "','" + DT_SUB.Rows[i]["ItemCode"].ToString() + "',";
                    SSQL = SSQL + " '" + DT_SUB.Rows[i]["ItemName"].ToString() + "','" + DT_SUB.Rows[i]["DeptCode"].ToString() + "',";
                    SSQL = SSQL + " '" + DT_SUB.Rows[i]["DeptName"].ToString() + "','" + DT_SUB.Rows[i]["UOM"].ToString() + "',";
                    SSQL = SSQL + " 'WHM/00001','service godown',";
                    SSQL = SSQL + " '" + DT_SUB.Rows[i]["InspectQty"].ToString() + "','" + DT_SUB.Rows[i]["NetAmount"].ToString() + "',";
                    SSQL = SSQL + " '0.00','0.00','" + SessionUserID + "','" + SessionUserName + "','" + CurType + "','Direct')";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    //Restricted Stock Move To Direct Stock (Minus Qty)
                    SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                    SSQL = SSQL + " Trans_Type,Supp_Type,Mat_Type,Supp_Code,Supp_Name,SapNo,ItemCode,ItemName,DeptCode,DeptName,UOM,";
                    SSQL = SSQL + " WarehouseCode,WarehouseName,Add_Qty,Add_Value,Minus_Qty,Minus_Value,UserID,UserName,CurrencyType,ReceiveType)";
                    SSQL = SSQL + " Values( '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                    SSQL = SSQL + " '" + SessionFinYearVal + "', '" + TransNo + "','" + TransDate.ToString() + "','" + TransDate + "',";
                    SSQL = SSQL + " 'Goods Received','" + SuppType + "','" + MatType + "', '" + SuppCode + "','" + SuppName + "',";
                    SSQL = SSQL + " '" + DT_SUB.Rows[i]["SapNo"].ToString() + "','" + DT_SUB.Rows[i]["ItemCode"].ToString() + "',";
                    SSQL = SSQL + " '" + DT_SUB.Rows[i]["ItemName"].ToString() + "','" + DT_SUB.Rows[i]["DeptCode"].ToString() + "',";
                    SSQL = SSQL + " '" + DT_SUB.Rows[i]["DeptName"].ToString() + "','" + DT_SUB.Rows[i]["UOM"].ToString() + "',";
                    SSQL = SSQL + " 'WHM/00001','service godown','0.00','0.00',";
                    SSQL = SSQL + " '" + DT_SUB.Rows[i]["InspectQty"].ToString() + "','" + DT_SUB.Rows[i]["NetAmount"].ToString() + "',";
                    SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','" + CurType + "','Restricted Stock')";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('CheckIN Details Approved Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('CheckIN not is Avail give input correctly..');", true);
            }
        }
    }
    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "1", "RFI QC Approval");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Request..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_GIChecking_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And TransNo='" + e.CommandArgument.ToString() + "' And ";
            SSQL = SSQL + " Approval_Status = '1'";
            //if (SessionUserType == "Manager")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
            //}
            //else if (SessionUserType == "VP")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
            //}
            //else if (SessionUserType == "Finance")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
            //}
            //else if (SessionUserType == "JMD")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '4' ) ";
            //}
            //else
            // {
            //     SSQL = SSQL + " and  Approval_Status = '1' ";
            // }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('CheckIN Details Already Approved.. Cannot Cancelled it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_GIChecking_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And TransNo='" + e.CommandArgument.ToString() + "' ";
            //SSQL = SSQL + " And Approval_Status = '2'";
            SSQL = SSQL + " And Approval_Status = '-1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('CheckIn Details Already get Cancelled..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            SSQL = "Select * from Trans_GIChecking_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And TransNo='" + e.CommandArgument.ToString() + "' ";
            //SSQL = SSQL + " And Approval_Status = '2'";
            SSQL = SSQL + " And Approval_Status = '-1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table

                SSQL = "Update Trans_GIChecking_Main set Approval_Status='-1' where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " TransNo ='" + e.CommandArgument.ToString() + "'";

                SSQL = "Update Trans_GIChecking_Sub set Approval_Status='-1' where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " TransNo ='" + e.CommandArgument.ToString() + "'";


                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('CheckIn Details Cancelled Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('CheckIn not is Avail give input correctly..');", true);
            }
        }
    }
    protected void Repeater1_ItemCommand(object sender, CommandEventArgs e)
    {

        if (e.CommandName == "QCTest")
        {
            string Fname = e.CommandArgument.ToString();
            string path = MapPath("~/assets/uploads/CORAL/Document/QC_Test/" + Fname);
            byte[] vs = System.IO.File.ReadAllBytes(path);
            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Type", "Application/octet-stream");
            Response.AddHeader("Content-Length", vs.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Fname);
            Response.BinaryWrite(vs);
            Response.Flush();
        }
    }
    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //string[] Department_Spilit = txtDepartment.Text.Split('-');
            //string Department_Code = "";
            //string Department_Name = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            DataTable DT2 = new DataTable();

            DT.Columns.Add("TransNo");
            DT.Columns.Add("PONo");
            DT.Columns.Add("PODate");
            DT.Columns.Add("SuppName");
            DT.Columns.Add("MaterialType");


            if (txtRequestStatus.Text == "Pending List")
            {
                SSQL = "Select TransNo,PONo,PODate,SuppName,MaterialType From Trans_GIChecking_Main ";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";

                //if (SessionUserType == "Manager")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";
                //}
                //else if (SessionUserType == "VP")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' ) ";
                //}
                //else if (SessionUserType == "Finance")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
                //}
                //else if (SessionUserType == "JMD")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
                //}
                //else
                //{
                //    SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";
                //}
                SSQL = SSQL + " And Status!='Delete' Order By TransNo Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Pending.DataSource = DT;
                Repeater_Pending.DataBind();
                Repeater_Pending.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Approved List")
            {
                SSQL = "Select TransNo,PONo,PODate,SuppName,MaterialType From Trans_GIChecking_Main ";
                //SSQL = SSQL + " Case when ReceiveStatus='1' then 'Goods Received' else 'Goods Not Received' End Status ";
                //SSQL = SSQL + " From Trans_Coral_PurOrd_Main ";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And Approval_Status = '1' And Status!='Delete' Order By TransNo Asc";
                //if (SessionUserType == "Manager")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
                //}
                //else if (SessionUserType == "VP")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
                //}
                //else if (SessionUserType == "Finance")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
                //}
                //else if (SessionUserType == "JMD")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '4' ) ";
                //}
                //else
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
                //}
                SSQL = SSQL + " And Status!='Delete' Order By TransNo Asc";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Approve.DataSource = DT;
                Repeater_Approve.DataBind();
                Repeater_Approve.Visible = true;
                Repeater_Pending.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Rejected List")
            {
                SSQL = "Select TransNo,PONo,PODate,SuppName,MaterialType From Trans_GIChecking_Main";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Approval_Status = '-1' Order By TransNo Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Rejected.DataSource = DT;
                Repeater_Rejected.DataBind();
                Repeater_Rejected.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Pending.Visible = false;
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {
        string RptName = "";
        string Enquiry_No_Str = e.CommandName.ToString();
        RptName = "GateIn Check Report";
        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Transaction_No");
        Session.Remove("TransNo");
        Session["TransNo"] = Enquiry_No_Str;
        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?TransNo=" + Enquiry_No_Str + "&RptName=" + RptName, "_blank", "");

    }
    protected void Repeater_Approve_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void btnMRR_View_Command(object sender, CommandEventArgs e)
    {
        string RptName = "";
        string Enquiry_No_Str = e.CommandName.ToString();
        RptName = "RFI MRR Report";
        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Transaction_No");
        Session.Remove("TransNo");
        Session["TransNo"] = Enquiry_No_Str;
        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?TransNo=" + Enquiry_No_Str + "&RptName=" + RptName, "_blank", "");
    }

}