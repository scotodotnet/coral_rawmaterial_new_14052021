﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_GatePassOut_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionDCNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal RetQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Delivery Challan";
            Initial_Data_Referesh();
            Load_ItemStock();
            Load_Supplier();
            Load_Data_IdentificationNo();
            Load_Data_GST();
            Load_Data_NonStk_GST();

            Load_Data_UOM();

            if (Session["DCNo"] == null)
            {
                SessionDCNo = "";

                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Delivery Challan", SessionFinYearVal, "1");

                lblDCNo.Text = Auto_Transaction_No;
                txtDCDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            }
            else
            {
                SessionDCNo = Session["DCNo"].ToString();
                lblDCNo.Text = SessionDCNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();

    }

    private void Load_Data_UOM()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select UOMCode,UOMShortName[UOM] From MstUom Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete' ";

          
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlNonStkUOM.DataSource = DT;

            DataRow dr = DT.NewRow();

            dr["UOMCode"] = "-Select-";
            dr["UOM"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);

            ddlNonStkUOM.DataValueField = "UOMCode";
            ddlNonStkUOM.DataTextField = "UOM";
            ddlNonStkUOM.DataBind();

        }
        catch (Exception ex)
        {

        }
    }

    private void Load_Data_NonStk_GST()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select GST_Type[GSTName] From GstMaster Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete' ";

            if (ddlGSTType.SelectedItem.Text == "GST")
            {
                SSQL = SSQL + " And Description='GST' ";
            }
            else if (ddlGSTType.SelectedItem.Text == "IGST")
            {
                SSQL = SSQL + " And Description='IGST' ";
            }
            else if (ddlGSTType.SelectedItem.Text == "NONE")
            {
                SSQL = SSQL + " And Description='NONE' ";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlNonStkGST.DataSource = DT;

            DataRow dr = DT.NewRow();

            dr["GSTName"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);

            ddlNonStkGST.DataValueField = "GSTName";
            ddlNonStkGST.DataTextField = "GSTName";
            ddlNonStkGST.DataBind();

        }
        catch (Exception ex)
        {

        }
    }

    private void Load_Data_GST()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
             
            SSQL = "Select GST_Type[GSTName] From GstMaster Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete' ";

            if(ddlGSTType.SelectedItem.Text=="GST")
            {
                SSQL = SSQL + " And Description='GST' ";
            }
            else if (ddlGSTType.SelectedItem.Text == "IGST")
            {
                SSQL = SSQL + " And Description='IGST' ";
            }
            else if (ddlGSTType.SelectedItem.Text == "NONE")
            {
                SSQL = SSQL + " And Description='NONE' ";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlGSTName.DataSource = DT;

            DataRow dr = DT.NewRow();
            
            dr["GSTName"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);

            ddlGSTName.DataValueField = "GSTName";
            ddlGSTName.DataTextField = "GSTName";
            ddlGSTName.DataBind();

        }
        catch (Exception ex)
        {

        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }

        //Auto generate Transaction Function Call

        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Delivery Challan", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    lblDCNo.Text = Auto_Transaction_No;
                }
            }
        }

        string GPType = "";
        string StockType = "";

        GPType = ddlGPOutType.SelectedItem.Text;
        StockType = RdpStkType.SelectedItem.Text;

        if (!ErrFlag)
        {

            SSQL = "Delete From Trans_DC_Main where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " DCNo='" + lblDCNo.Text + "' ";

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete From Trans_DC_Sub where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " DCNo='" + lblDCNo.Text + "' ";

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Insert Into Trans_DC_Main(Ccode,Lcode,FinYearCode,FinYearVal,DCNo,DCDate,MatType,GPType,StockType,";
            SSQL = SSQL + " DeliveryDate,SuppInvNo,SuppInvDate,SuppCode,SuppName,TransportDetails,DriverName,DriverMobNo,";
            SSQL = SSQL + " Purpose,Remarks,TotQty,ItemTot,CurrencyType,GSTType,TotCGSTAmt,TotSGSTAmt,TotIGSTAmt,TotLineAmt,";
            SSQL = SSQL + " RoundOff,NetAmount,Status,Approval_Status,UserID,UserName,CreateOn) Values('" + SessionCcode + "',";
            SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + lblDCNo.Text + "',";
            SSQL = SSQL + " '" + txtDCDate.Text + "','" + ddlMatType.SelectedValue + "','" + GPType + "','" + StockType + "',";
            SSQL = SSQL + " '" + txtDeliDate.Text + "','" + txtSuppInvNo.Text + "','" + txtSuppInvDate.Text + "',";
            SSQL = SSQL + " '" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "','" + txtVehicleNo.Text + "',";
            SSQL = SSQL + " '" + txtDriverName.Text + "','" + txtDriMobiNo.Text + "','" + txtPurpose.Text + "','" + txtRemarks.Text + "',";
            SSQL = SSQL + " '" + lblTotQty.Text + "','" + lblTotItem.Text + "','" + ddlCurrencyType.SelectedValue + "',";
            SSQL = SSQL + " '" + ddlGSTType.SelectedValue + "','" + lblTotCGST.Text + "','" + lblTotSGST.Text + "',";
            SSQL = SSQL + " '" + lblTotIGST.Text + "','" + lblTotAmt.Text + "','" + txtRoundOff.Text + "','" + lblNetAmt.Text + "',";
            SSQL = SSQL + " 'Add','0','" + SessionUserID + "','" + SessionUserName + "',Convert(Datetime,getDate(),103))";

            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Insert Into Trans_DC_Sub(Ccode,Lcode,FinYearCode,FinYearVal,DCNo,DCDate,RefNo,ItemCode,ItemName,";
                SSQL = SSQL + " DeptCode,DeptName,WareHouseCode,WareHouseName,RackName,UOMCode,UOM,OutQty,Rate,ItemAmount,";
                SSQL = SSQL + " GSTType,CGSTPer,CGSTAmt,SGSTPer,SGSTAmt,IGSTPer,IGSTAmt,LineAmt,Remarks,Status,UserID,";
                SSQL = SSQL + " UserName,CreateOn) Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + lblDCNo.Text + "',";
                SSQL = SSQL + " '" + txtDCDate.Text + "','" + dt.Rows[i]["RefNo"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["WarehouseCode"].ToString() + "','" + dt.Rows[i]["WarehouseName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["RackName"].ToString() + "','" + dt.Rows[i]["UOMCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["UOMName"].ToString() + "','" + dt.Rows[i]["OutQty"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["ItemValue"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["GSTName"].ToString() + "','" + dt.Rows[i]["CGSTP"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["CGSTAmt"].ToString() + "','" + dt.Rows[i]["SGSTP"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["SGSTAmt"].ToString() + "','" + dt.Rows[i]["IGSTP"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["IGSTAmt"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["ItemRem"].ToString() + "','Add','" + SessionUserID + "','" + SessionUserName + "',";
                SSQL = SSQL + " Convert(Datetime, GetDate(), 103))";

                objdata.RptEmployeeMultipleDetails(SSQL);

            }

            if (btnSave.Text == "Save")
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Delivery Challan", SessionFinYearVal, "1");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass Out Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass Out Details Updated Successfully');", true);
            }

            //Clear_All_Field();
            Session["GPOutNo"] = lblDCNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
            Response.Redirect("Trans_DeliveryChallan_Main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Load_Supplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select LedgerCode,LedgerName From Acc_Mst_Ledger where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And Status='Add' And LedgerGrpName='Supplier' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlSupplier.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlSupplier.DataValueField = "LedgerCode";
        ddlSupplier.DataTextField = "LedgerName";
        ddlSupplier.DataBind();

    }


    private void Load_Data_IdentificationNo()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = "Select Mat_No[ItemCode],Sap_No[SapNo] from BomMaster Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode[ItemCode],RefCode[SapNo] from MstTools Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ItemCode[ItemCode],RefCode[SapNo] from CORAL_ERP_Asset..MstAssetItem ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = "Select ItemCode[ItemCode],RefCode[SapNo] from MstGeneralItem Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlIdentiNo.DataSource = DT;

            DataRow dr = DT.NewRow();

            dr["ItemCode"] = "-Select-";
            dr["SapNo"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);

            ddlIdentiNo.DataValueField = "ItemCode";
            ddlIdentiNo.DataTextField = "SapNo";
            ddlIdentiNo.DataBind();

        }
        catch (Exception ex)
        {

        }
    }

    private void Load_ItemStock()
    {
        string SSQL = "";
        DataTable DT = new DataTable();



        if (ddlMatType.SelectedItem.Text == "RawMaterial")
        {
            SSQL = "Select Mat_No[ItemCode],Raw_Mat_Name [ItemName] from BomMaster Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "Tools")
        {
            SSQL = "Select ToolCode[ItemCode],ToolName[ItemName] from MstTools Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "Asset")
        {
            SSQL = "Select ItemCode[ItemCode],ItemDesc [ItemName] from CORAL_ERP_Asset..MstAssetItem ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status!='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "General Items")
        {
            SSQL = "Select ItemCode[ItemCode],ItemName[ItemName] from MstGeneralItem Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlItemName.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["ItemName"] = "-Select-";
        dr["ItemCode"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataValueField = "ItemCode";
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataBind();
    }

    private void Clear_All_Field()
    {
        lblDCNo.Text = ""; txtDCDate.Text = "";
        // txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        //txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("GPOutNo");
        //Load_Data_Enquiry_Grid();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Trans_DC_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And DCNo ='" + lblDCNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtDCDate.Text = Main_DT.Rows[0]["DCDate"].ToString();
            ddlMatType.SelectedValue = Main_DT.Rows[0]["MatType"].ToString();
            txtDeliDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();

            txtSuppInvNo.Text = Main_DT.Rows[0]["SuppInvNo"].ToString();
            txtSuppInvDate.Text = Main_DT.Rows[0]["SuppInvDate"].ToString();
            ddlSupplier.SelectedValue = Main_DT.Rows[0]["SuppCode"].ToString();
            txtVehicleNo.Text = Main_DT.Rows[0]["TransportDetails"].ToString();
            txtDriverName.Text = Main_DT.Rows[0]["DriverName"].ToString();

            txtDriMobiNo.Text = Main_DT.Rows[0]["DriverMobNo"].ToString();
            txtPurpose.Text = Main_DT.Rows[0]["Purpose"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            ddlCurrencyType.SelectedValue = Main_DT.Rows[0]["CurrencyType"].ToString();
            ddlGSTType.SelectedValue = Main_DT.Rows[0]["GSTType"].ToString();

            lblTotQty.Text = Main_DT.Rows[0]["TotQty"].ToString();
            lblTotItem.Text = Main_DT.Rows[0]["ItemTot"].ToString();
            lblTotCGST.Text = Main_DT.Rows[0]["TotCGSTAmt"].ToString();
            lblTotSGST.Text = Main_DT.Rows[0]["TotSGSTAmt"].ToString();
            lblTotIGST.Text = Main_DT.Rows[0]["TotIGSTAmt"].ToString();
            lblTotAmt.Text = Main_DT.Rows[0]["TotLineAmt"].ToString();
            txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
            lblNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();

            if (Main_DT.Rows[0]["GPType"].ToString() == "Returnable")
            {
                ddlGPOutType.SelectedValue = "1";
            }
            else
            {
                ddlGPOutType.SelectedValue = "2";
            }


            if (Main_DT.Rows[0]["StockType"].ToString() == "Stock")
            {
                pnlStock.Visible = true;
                pnlNonStock.Visible = false;

                RdpStkType.SelectedValue = "1";
            }
            else
            {
                pnlStock.Visible = false;
                pnlNonStock.Visible = true;

                RdpStkType.SelectedValue = "2";
            }

            //txtTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();
            //txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
            //txtTotAmt.Text = Main_DT.Rows[0]["TotalAmount"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select RefNo,ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,RackName,";
            SSQL = SSQL + " UOMCode,UOM[UOMName],OutQty,Rate,ItemAmount[ItemValue],GSTType[GSTName],CGSTPer[CGSTP],";
            SSQL = SSQL + " CGSTAmt,SGSTPer[SGSTP],SGSTAmt,IGSTPer[IGSTP],IGSTAmt,LineAmt[LineTotal],Remarks[ItemRem] ";
            SSQL = SSQL + " From Trans_DC_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode = '" + SessionFinYearCode + "' And DCNo='" + lblDCNo.Text + "'";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_DeliveryChallan_Main.aspx");
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string Old_New_Type = "1";

            string SSQL = "";
            DataTable DT = new DataTable();
            string IssueType = "";

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = "Select Mat_No[ItemCode],UOMTypeCode[UOMCode],UOM_Full[UOMName],DeptCode,DeptName,WarehouseCode,WarehouseName,RackSerious ";
                SSQL = SSQL + " From BOMMaster Where Mat_No ='" + ddlItemName.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode[ItemCode],UOMCode [UOMCode],UOM[UOMName],DeptCode,DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From MstTools Where ToolCode ='" + ddlItemName.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ItemCode,UOMCode[UOMCode],UOM[UOMName],DeptCode,DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From Coral_ERP_Asset..MstAssetItem Where ItemCode ='" + ddlItemName.SelectedValue + "' and ";
                SSQL = SSQL + " Status!='Delete' and Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = "Select ItemCode,UOMCode[UOMCode],UOM[UOMName],'' DeptCode, '' DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From MstGeneralItem Where ItemCode ='" + ddlItemName.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlIdentiNo.SelectedValue = DT.Rows[0]["ItemCode"].ToString();

            hfUOMCode.Value = DT.Rows[0]["UOMCode"].ToString();
            hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();
            hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
            hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
            hfWarehouseCode.Value = DT.Rows[0]["WarehouseCode"].ToString();
            hfWarehouseName.Value = DT.Rows[0]["WarehouseName"].ToString();
            hfRackNo.Value = DT.Rows[0]["RackSerious"].ToString();

            if (Old_New_Type == "1")
            {

                SSQL = "Select (sum(Add_Qty)-sum(Minus_Qty)) StockQty,(sum(Add_Value)-sum(Minus_Value)) stockVal,";
                SSQL = SSQL + " ((sum(Add_Value)-sum(Minus_Value))/(sum(Add_Qty)-sum(Minus_Qty))) Rate  ";
                SSQL = SSQL + " From Trans_Stock_Ledger_All Where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + "   Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "' having (sum(Add_Qty)-sum(Minus_Qty))>0";


                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (Old_New_Type == "2")
            {
                SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (DT.Rows.Count != 0)
            {

                txtRate.Text = DT.Rows[0]["Rate"].ToString();
            }
            else
            {


            }
        }
        catch (Exception ex)
        {

        }

    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtGPOutQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Return Qty...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["RefNo"] = ddlIdentiNo.SelectedItem.Text;
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["DeptCode"] = hfDeptCode.Value;
                    dr["DeptName"] = hfDeptName.Value;
                    dr["WarehouseCode"] = hfWarehouseCode.Value;
                    dr["WarehouseName"] = hfWarehouseName.Value;
                    dr["RackName"] = hfRackNo.Value;
                    dr["UOMCode"] = hfUOMCode.Value;
                    dr["UOMName"] = hfUOMName.Value;
                    dr["OutQty"] = txtGPOutQty.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["ItemValue"] = lblItemValue.Text;
                    dr["GSTName"] = ddlGSTName.SelectedItem.Text;
                    dr["CGSTP"] = lblCGSTPre.Text;
                    dr["CGSTAmt"] = lblCGSTAmt.Text;
                    dr["SGSTP"] = lblSGSTPre.Text;
                    dr["SGSTAmt"] = lblCGSTAmt.Text;
                    dr["IGSTP"] = lblIGSTPre.Text;
                    dr["IGSTAmt"] = lblIGSTAmt.Text;
                    dr["LineTotal"] = lblLineTotal.Text;

                    dr["ItemRem"] = txtItemNotes.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    ddlItemName.SelectedIndex = 0; ddlIdentiNo.SelectedIndex = 0;
                    ddlGSTName.SelectedIndex = 0;
                    lblCGSTPre.Text = "0.00";lblCGSTAmt.Text = "0.00";
                    lblSGSTPre.Text = "0.00"; lblSGSTAmt.Text = "0.00";
                    lblIGSTPre.Text = "0.00"; lblIGSTAmt.Text = "0.00";
                    lblStockQty.Text = "0.00"; lblLineTotal.Text = "0.00";
                    txtGPOutQty.Text = "0"; txtRate.Text = "0"; lblItemValue.Text = "0.0"; txtItemNotes.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();

                dr = dt.NewRow();
                dr["RefNo"] = ddlIdentiNo.SelectedItem.Text;
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["DeptCode"] = hfDeptCode.Value;
                dr["DeptName"] = hfDeptName.Value;
                dr["WarehouseCode"] = hfWarehouseCode.Value;
                dr["WarehouseName"] = hfWarehouseName.Value;
                dr["RackName"] = hfRackNo.Value;
                dr["UOMCode"] = hfUOMCode.Value;
                dr["UOMName"] = hfUOMName.Value;

                dr["OutQty"] = txtGPOutQty.Text;
                dr["Rate"] = txtRate.Text;
                dr["ItemValue"] = lblItemValue.Text;

                dr["GSTName"] = ddlGSTName.SelectedItem.Text;
                dr["CGSTP"] = lblCGSTPre.Text;
                dr["CGSTAmt"] = lblCGSTAmt.Text;
                dr["SGSTP"] = lblSGSTPre.Text;
                dr["SGSTAmt"] = lblCGSTAmt.Text;
                dr["IGSTP"] = lblIGSTPre.Text;
                dr["IGSTAmt"] = lblIGSTAmt.Text;
                dr["LineTotal"] = lblLineTotal.Text;

                dr["ItemRem"] = txtItemNotes.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                ddlItemName.SelectedIndex = 0; ddlIdentiNo.SelectedIndex = 0;
                ddlGSTName.SelectedIndex = 0;
                lblCGSTPre.Text = "0.00"; lblCGSTAmt.Text = "0.00";
                lblSGSTPre.Text = "0.00"; lblSGSTAmt.Text = "0.00";
                lblIGSTPre.Text = "0.00"; lblIGSTAmt.Text = "0.00";
                lblStockQty.Text = "0.00"; txtRemarks.Text = "0.00";
                lblLineTotal.Text = "0.00";
                txtGPOutQty.Text = "0"; txtRate.Text = "0"; lblItemValue.Text = "0.0"; txtItemNotes.Text = "";
            }
            Grid_Total_Calculate();
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("RefNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("RackName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMName", typeof(string)));
        dt.Columns.Add(new DataColumn("OutQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemValue", typeof(string)));
        dt.Columns.Add(new DataColumn("GSTName", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRem", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        //dt = Repeater1.DataSource;
    }

    protected void txtGPOutQty_TextChanged(object sender, EventArgs e)
    {
        bool ErrFlg = false;

        if (Convert.ToDecimal(lblStockQty.Text) < Convert.ToDecimal(txtGPOutQty.Text))
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('More Then Stock Qty,Check It..');", true);

            txtGPOutQty.Text = "0.00";
        }

        if (!ErrFlg)
        {
            if (Convert.ToDecimal(txtRate.Text) > 0)
            {
                lblItemValue.Text = Math.Round((Convert.ToDecimal(txtRate.Text) * Convert.ToDecimal(txtGPOutQty.Text)), 2).ToString();


                if (ddlGSTName.SelectedItem.Text != "-Select-")
                {
                    ddlGSTName_SelectedIndexChanged(sender, e);
                }
            }
        }
    }

    protected void txtOutQty_TextChanged(object sender, EventArgs e)
    {
        TextBox txtTest = ((TextBox)(sender));
        RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

        TextBox txtGPOut = (TextBox)rpi.FindControl("txtOutQty");
        Label lblRate = (Label)rpi.FindControl("lblRate");

        Label lblValue = (Label)rpi.FindControl("lblValue");

        decimal Val = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtGPOut.Text);

        lblValue.Text = Math.Round(Val, 2).ToString();
    }

    protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "select HouseNo+','+Add1+','+Add2+','+ City+'-'+PinCode [Address] from Acc_Mst_Ledger where LedgerGrpName='Supplier' And ";
        SSQL = SSQL + " LedgerCode ='" + ddlSupplier.SelectedValue + "' And Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_ItemStock();
        Load_Data_IdentificationNo();
    }

    protected void ddlIdentiNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string Old_New_Type = "1";

            string SSQL = "";
            DataTable DT = new DataTable();
            string IssueType = "";

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = "Select Mat_No[ItemCode],UOMTypeCode[UOMCode],UOM_Full[UOMName],DeptCode,DeptName,WarehouseCode,WarehouseName,RackSerious ";
                SSQL = SSQL + " From BOMMaster Where Mat_No ='" + ddlIdentiNo.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode[ItemCode],UOMCode [UOMCode],UOM[UOMName],DeptCode,DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From MstTools Where ToolCode ='" + ddlIdentiNo.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ItemCode,UOMCode[UOMCode],UOM[UOMName],DeptCode,DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From Coral_ERP_Asset..MstAssetItem Where ItemCode ='" + ddlIdentiNo.SelectedValue + "' and ";
                SSQL = SSQL + " Status!='Delete' and Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = "Select ItemCode,UOMCode[UOMCode],UOM[UOMName],'' DeptCode, '' DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From MstGeneralItem Where ItemCode ='" + ddlIdentiNo.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlItemName.SelectedValue = DT.Rows[0]["ItemCode"].ToString();

            hfUOMCode.Value = DT.Rows[0]["UOMCode"].ToString();
            hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();
            hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
            hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
            hfWarehouseCode.Value = DT.Rows[0]["WarehouseCode"].ToString();
            hfWarehouseName.Value = DT.Rows[0]["WarehouseName"].ToString();
            hfRackNo.Value = DT.Rows[0]["RackSerious"].ToString();

            if (Old_New_Type == "1")
            {
                SSQL = "Select (sum(Add_Qty)-sum(Minus_Qty)) StockQty,(sum(Add_Value)-sum(Minus_Value)) stockVal,";
                SSQL = SSQL + " ((sum(Add_Value)-sum(Minus_Value))/(sum(Add_Qty)-sum(Minus_Qty))) Rate  ";
                SSQL = SSQL + " From Trans_Stock_Ledger_All Where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + "   Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And ItemCode='" + ddlIdentiNo.SelectedValue + "' having (sum(Add_Qty)-sum(Minus_Qty))>0";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (Old_New_Type == "2")
            {
                SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And ItemCode='" + ddlIdentiNo.SelectedValue + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (DT.Rows.Count != 0)
            {
                lblStockQty.Text = Math.Round(Convert.ToDecimal(DT.Rows[0]["StockQty"]), 2, MidpointRounding.AwayFromZero).ToString();
                txtRate.Text = Math.Round(Convert.ToDecimal(DT.Rows[0]["Rate"]), 2, MidpointRounding.AwayFromZero).ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item No Stock..');", true);

                ddlItemName.SelectedIndex = 0;
                txtRate.Text = "0.00";
                txtGPOutQty.Text = "0.00";
                lblItemValue.Text = "0.00";
            }
        }
        catch (Exception ex)
        {

        }
    }
   

    private void Grid_Total_Calculate()
    {
        try
        {
            decimal SumLineTot = 0;
            decimal SumQtyTot = 0;
            decimal SumItemTot = 0;
            decimal SumCGSTTot = 0;
            decimal SumSGSTTot = 0;
            decimal SumIGSTTot = 0;

            if (txtRoundOff.Text == "") { txtRoundOff.Text = "0.00"; }

            for (int i = 0; i < Repeater1.Items.Count; i++)
            {
                Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;
                TextBox txtOrdQty = Repeater1.Items[i].FindControl("txtOutQty") as TextBox;
                Label lblCGSTTot = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTTot = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTTot = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblITMTot = Repeater1.Items[i].FindControl("lblValue") as Label;

                SumItemTot = SumItemTot + Convert.ToDecimal(lblITMTot.Text);
                SumCGSTTot = SumCGSTTot + Convert.ToDecimal(lblCGSTTot.Text);
                SumSGSTTot = SumSGSTTot + Convert.ToDecimal(lblSGSTTot.Text);
                SumIGSTTot = SumIGSTTot + Convert.ToDecimal(lblIGSTTot.Text);
                SumLineTot = SumLineTot + Convert.ToDecimal(LineTotal.Text);
                SumQtyTot = SumQtyTot + Convert.ToDecimal(txtOrdQty.Text);
            }

            lblTotItem.Text = SumItemTot.ToString();
            
            lblTotCGST.Text = SumCGSTTot.ToString();
            lblTotSGST.Text = SumSGSTTot.ToString();
            lblTotIGST.Text = SumIGSTTot.ToString();

            lblTotAmt.Text = SumLineTot.ToString();
            lblTotQty.Text = SumQtyTot.ToString();
            lblNetAmt.Text = (SumLineTot + Convert.ToDecimal(txtRoundOff.Text)).ToString();
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtRoundOff_TextChanged(object sender, EventArgs e)
    {
        if (txtRoundOff.Text.ToString() != "") { Final_Total_Calculate(); }
    }

    private void Final_Total_Calculate()
    {
        try
        {
            string Final_NetAmt = "0";
            string AddorLess = "0";
            string Item_Total_Amt = lblTotAmt.Text;

            string[] ROff = txtRoundOff.Text.Split('.');


            if (Convert.ToDecimal(txtRoundOff.Text.ToString()) != 0)
            {
                AddorLess = txtRoundOff.Text.ToString();
            }

            if (Convert.ToDecimal(ROff[1].ToString()) < Convert.ToDecimal("49"))
            {
                Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) - Convert.ToDecimal(AddorLess)).ToString();
            }
            else if (Convert.ToDecimal(ROff[1].ToString()) > Convert.ToDecimal("49"))
            {
                Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) + Convert.ToDecimal(AddorLess)).ToString();
            }
            lblNetAmt.Text = Final_NetAmt;
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlGSTName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select * From GstMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
            SSQL = SSQL + " And Status !='Delete' And GST_Type='" + ddlGSTName.SelectedValue + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            lblCGSTPre.Text = DT.Rows[0]["CGST_Per"].ToString();
            lblSGSTPre.Text = DT.Rows[0]["SGST_Per"].ToString();
            lblIGSTPre.Text = DT.Rows[0]["IGST_Per"].ToString();


            if (lblItemValue.Text == "") { lblItemValue.Text = "0.00"; }
            if (lblCGSTPre.Text == "") { lblCGSTPre.Text = "0.00"; }
            if (lblSGSTPre.Text == "") { lblSGSTPre.Text = "0.00"; }
            if (lblIGSTPre.Text == "") { lblIGSTPre.Text = "0.00"; }

            if (Convert.ToDecimal(lblItemValue.Text) > 0)
            {
                if (Convert.ToDecimal(lblCGSTPre.Text) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = Convert.ToDecimal(lblItemValue.Text).ToString();
                    lblCGSTAmt.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(lblCGSTPre.Text)).ToString();
                    lblCGSTAmt.Text = (Convert.ToDecimal(lblCGSTAmt.Text) / Convert.ToDecimal(100)).ToString();
                    lblCGSTAmt.Text = (Math.Round(Convert.ToDecimal(lblCGSTAmt.Text), 2, MidpointRounding.AwayFromZero)).ToString();

                }
                else
                {
                    lblCGSTAmt.Text = "0.00";
                }

                if (Convert.ToDecimal(lblSGSTPre.Text) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = Convert.ToDecimal(lblItemValue.Text).ToString();
                    lblSGSTAmt.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(lblSGSTPre.Text)).ToString();
                    lblSGSTAmt.Text = (Convert.ToDecimal(lblSGSTAmt.Text) / Convert.ToDecimal(100)).ToString();
                    lblSGSTAmt.Text = (Math.Round(Convert.ToDecimal(lblSGSTAmt.Text), 2, MidpointRounding.AwayFromZero)).ToString();

                }
                else
                {
                    lblSGSTAmt.Text = "0.00";
                }

                if (Convert.ToDecimal(lblIGSTPre.Text) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = Convert.ToDecimal(lblItemValue.Text).ToString();
                    lblIGSTAmt.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(lblIGSTPre.Text)).ToString();
                    lblIGSTAmt.Text = (Convert.ToDecimal(lblIGSTAmt.Text) / Convert.ToDecimal(100)).ToString();
                    lblIGSTAmt.Text = (Math.Round(Convert.ToDecimal(lblIGSTAmt.Text), 2, MidpointRounding.AwayFromZero)).ToString();

                }
                else
                {
                    lblIGSTAmt.Text = "0.00";
                }

                lblLineTotal.Text = Math.Round(Convert.ToDecimal(lblItemValue.Text) + Convert.ToDecimal(lblCGSTAmt.Text) + Convert.ToDecimal(lblSGSTAmt.Text) + Convert.ToDecimal(lblIGSTAmt.Text), 2, MidpointRounding.AwayFromZero).ToString();
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlGSTType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select GST_Type[GSTName] From GstMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
            SSQL = SSQL + " And Status !='Delete' And Description='" + ddlGSTType.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (RdpStkType.SelectedIndex == 0)
            {

                ddlGSTName.DataSource = DT;

                DataRow dr = DT.NewRow();

                dr["GSTName"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);

                ddlGSTName.DataValueField = "GSTName";
                ddlGSTName.DataTextField = "GSTName";
                ddlGSTName.DataBind();
            }
            else
            {
                ddlNonStkGST.DataSource = DT;

                DataRow dr = DT.NewRow();

                dr["GSTName"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);

                ddlNonStkGST.DataValueField = "GSTName";
                ddlNonStkGST.DataTextField = "GSTName";
                ddlNonStkGST.DataBind();
            }


        }
        catch (Exception ex)
        {

        }
    }

    protected void RdpStkType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(RdpStkType.SelectedIndex==0)
        {
            pnlNonStock.Visible = false;
            pnlStock.Visible = true;
        }
        else
        {
            pnlNonStock.Visible = true;
            pnlStock.Visible = false;
        }
    }

    protected void btnNonStkAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemName"].ToString().ToUpper() == txtNonStkItemName.Text.ToString())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["RefNo"] = "NA";
                    dr["ItemCode"] = "NA";
                    dr["ItemName"] = txtNonStkItemName.Text;
                    dr["DeptCode"] = "NA";
                    dr["DeptName"] = "NA";
                    dr["WarehouseCode"] = "NA";
                    dr["WarehouseName"] = "NA";
                    dr["RackName"] = "NA";
                    dr["UOMCode"] = ddlNonStkUOM.SelectedValue;
                    dr["UOMName"] = ddlNonStkUOM.SelectedItem.Text;
                    dr["OutQty"] = txtNonOutQty.Text;
                    dr["Rate"] = txtNonStkRate.Text;
                    dr["ItemValue"] = lblNonStkItemVal.Text;
                    dr["GSTName"] = ddlNonStkGST.SelectedItem.Text;
                    dr["CGSTP"] = lblNonStkCGSTP.Text;
                    dr["CGSTAmt"] = lblNonStkCGSTA.Text;
                    dr["SGSTP"] = lblNonStkSGSTP.Text;
                    dr["SGSTAmt"] = lblNonStkSGSTA.Text;
                    dr["IGSTP"] = lblNonStkIGSTP.Text;
                    dr["IGSTAmt"] = lblNonStkIGSTA.Text;
                    dr["LineTotal"] = lblNonStkLineTot.Text;

                    dr["ItemRem"] = txtNonStkNotes.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    txtNonStkItemName.Text = ""; 
                    ddlGSTName.SelectedIndex = 0;
                    lblNonStkCGSTP.Text = "0.00"; lblNonStkCGSTA.Text = "0.00";
                    lblNonStkSGSTP.Text = "0.00"; lblNonStkSGSTA.Text = "0.00";
                    lblNonStkIGSTP.Text = "0.00"; lblNonStkIGSTA.Text = "0.00";
                    lblNonStkLineTot.Text = "0.00";
                    txtNonOutQty.Text = "0"; txtNonStkRate.Text = "0"; lblNonStkItemVal.Text = "0.0"; txtNonStkNotes.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["RefNo"] = "NA";
                dr["ItemCode"] = "NA";
                dr["ItemName"] = txtNonStkItemName.Text;
                dr["DeptCode"] = "NA";
                dr["DeptName"] = "NA";
                dr["WarehouseCode"] = "NA";
                dr["WarehouseName"] = "NA";
                dr["RackName"] = "NA";
                dr["UOMCode"] = ddlNonStkUOM.SelectedValue;
                dr["UOMName"] = ddlNonStkUOM.SelectedItem.Text;
                dr["OutQty"] = txtNonOutQty.Text;
                dr["Rate"] = txtNonStkRate.Text;
                dr["ItemValue"] = lblNonStkItemVal.Text;
                dr["GSTName"] = ddlNonStkGST.SelectedItem.Text;
                dr["CGSTP"] = lblNonStkCGSTP.Text;
                dr["CGSTAmt"] = lblNonStkCGSTA.Text;
                dr["SGSTP"] = lblNonStkSGSTP.Text;
                dr["SGSTAmt"] = lblNonStkSGSTA.Text;
                dr["IGSTP"] = lblNonStkIGSTP.Text;
                dr["IGSTAmt"] = lblNonStkIGSTA.Text;
                dr["LineTotal"] = lblNonStkLineTot.Text;

                dr["ItemRem"] = txtNonStkNotes.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                txtNonStkItemName.Text = "";
                ddlGSTName.SelectedIndex = 0;
                lblNonStkCGSTP.Text = "0.00"; lblNonStkCGSTA.Text = "0.00";
                lblNonStkSGSTP.Text = "0.00"; lblNonStkSGSTA.Text = "0.00";
                lblNonStkIGSTP.Text = "0.00"; lblNonStkIGSTA.Text = "0.00";
                lblNonStkLineTot.Text = "0.00";
                txtNonOutQty.Text = "0"; txtNonStkRate.Text = "0"; lblNonStkItemVal.Text = "0.0"; txtNonStkNotes.Text = "";
            }
            Grid_Total_Calculate();
        }
    }

    protected void txtNonStkRate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtNonOutQty.Text) > 0)
        {
            lblNonStkLineTot.Text = Math.Round((Convert.ToDecimal(txtNonStkRate.Text) * Convert.ToDecimal(txtNonOutQty.Text)), 2).ToString();


            if (ddlNonStkGST.SelectedItem.Text != "-Select-")
            {
                ddlNonStkGST_SelectedIndexChanged(sender, e);
            }
        }
        else
        {

            lblNonStkItemVal.Text = "0.00";
            lblNonStkCGSTP.Text = "0.00";
            lblNonStkCGSTA.Text = "0.00";
            lblNonStkSGSTP.Text = "0.00";
            lblNonStkSGSTA.Text = "0.00";
            lblNonStkIGSTP.Text = "0.00";
            lblNonStkIGSTA.Text = "0.00";
            lblNonStkLineTot.Text = "0.00";
            ddlNonStkGST.SelectedIndex = 0;
        }
    }

    protected void txtNonOutQty_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtNonStkRate.Text) > 0)
        {
            lblNonStkItemVal.Text = Math.Round((Convert.ToDecimal(txtNonStkRate.Text) * Convert.ToDecimal(txtNonOutQty.Text)), 2).ToString();

            if (ddlNonStkGST.SelectedItem.Text != "-Select-")
            {
                ddlNonStkGST_SelectedIndexChanged(sender, e);
            }
        }
        else
        {
            
            lblNonStkItemVal.Text = "0.00";
            lblNonStkCGSTP.Text = "0.00";
            lblNonStkCGSTA.Text = "0.00";
            lblNonStkSGSTP.Text = "0.00";
            lblNonStkSGSTA.Text = "0.00";
            lblNonStkIGSTP.Text = "0.00";
            lblNonStkIGSTA.Text = "0.00";
            lblNonStkLineTot.Text = "0.00";
            ddlNonStkGST.SelectedIndex = 0;
        }
    }

    protected void ddlNonStkGST_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select * From GstMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
            SSQL = SSQL + " And Status !='Delete' And GST_Type='" + ddlNonStkGST.SelectedValue + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            lblNonStkCGSTP.Text = DT.Rows[0]["CGST_Per"].ToString();
            lblNonStkSGSTP.Text = DT.Rows[0]["SGST_Per"].ToString();
            lblNonStkIGSTP.Text = DT.Rows[0]["IGST_Per"].ToString();


            if (lblNonStkLineTot.Text == "") { lblNonStkLineTot.Text = "0.00"; }
            if (lblNonStkCGSTP.Text == "") { lblNonStkCGSTP.Text = "0.00"; }
            if (lblNonStkSGSTP.Text == "") { lblNonStkSGSTP.Text = "0.00"; }
            if (lblNonStkIGSTP.Text == "") { lblNonStkIGSTP.Text = "0.00"; }

            if (Convert.ToDecimal(lblNonStkItemVal.Text) > 0)
            {
                if (Convert.ToDecimal(lblNonStkCGSTP.Text) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = Convert.ToDecimal(lblNonStkItemVal.Text).ToString();
                    lblNonStkCGSTA.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(lblNonStkCGSTP.Text)).ToString();
                    lblNonStkCGSTA.Text = (Convert.ToDecimal(lblNonStkCGSTA.Text) / Convert.ToDecimal(100)).ToString();
                    lblNonStkCGSTA.Text = (Math.Round(Convert.ToDecimal(lblNonStkCGSTA.Text), 2, MidpointRounding.AwayFromZero)).ToString();

                }
                else
                {
                    lblNonStkCGSTA.Text = "0.00";
                }

                if (Convert.ToDecimal(lblNonStkSGSTP.Text) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = Convert.ToDecimal(lblNonStkItemVal.Text).ToString();
                    lblNonStkSGSTA.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(lblNonStkSGSTP.Text)).ToString();
                    lblNonStkSGSTA.Text = (Convert.ToDecimal(lblNonStkSGSTA.Text) / Convert.ToDecimal(100)).ToString();
                    lblNonStkSGSTA.Text = (Math.Round(Convert.ToDecimal(lblNonStkSGSTA.Text), 2, MidpointRounding.AwayFromZero)).ToString();

                }
                else
                {
                    lblNonStkSGSTA.Text = "0.00";
                }

                if (Convert.ToDecimal(lblNonStkIGSTP.Text) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = Convert.ToDecimal(lblNonStkItemVal.Text).ToString();
                    lblNonStkIGSTA.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(lblNonStkIGSTP.Text)).ToString();
                    lblNonStkIGSTA.Text = (Convert.ToDecimal(lblNonStkIGSTA.Text) / Convert.ToDecimal(100)).ToString();
                    lblNonStkIGSTA.Text = (Math.Round(Convert.ToDecimal(lblNonStkIGSTA.Text), 2, MidpointRounding.AwayFromZero)).ToString();

                }
                else
                {
                    lblNonStkIGSTA.Text = "0.00";
                }

                lblNonStkLineTot.Text = Math.Round(Convert.ToDecimal(lblNonStkItemVal.Text) + Convert.ToDecimal(lblNonStkCGSTA.Text) + Convert.ToDecimal(lblNonStkSGSTA.Text) + Convert.ToDecimal(lblNonStkIGSTA.Text), 2, MidpointRounding.AwayFromZero).ToString();
            }
            else
            {
                lblNonStkItemVal.Text = "0.00";
                lblNonStkCGSTP.Text = "0.00";
                lblNonStkCGSTA.Text = "0.00";
                lblNonStkSGSTP.Text = "0.00";
                lblNonStkSGSTA.Text = "0.00";
                lblNonStkIGSTP.Text = "0.00";
                lblNonStkIGSTA.Text = "0.00";
                lblNonStkLineTot.Text = "0.00";
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlNonStkUOM_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}