﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_PO_GPIN_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGPIN_No;
    string SessionFinYearCode;
    string SessionFinYearVal;
 
    bool ErrFlag = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: GatePass IN";
            Initial_Data_Referesh();
            Load_TransPortName();
            Load_PurOrder_No();
            Load_Supplier();

            if (Session["Pur_GIN_No"] == null)
            {
                SessionGPIN_No = "";

                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "PurOrd Gatepass In", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGPINNo.Text = Auto_Transaction_No;
                }

                txtGPINDate.Text = DateTime.Now.ToShortDateString();

            }
            else
            {
                SessionGPIN_No = Session["Pur_GIN_No"].ToString();
                txtGPINNo.Text = SessionGPIN_No;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }


    private void Load_PurOrder_No()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            string MatType = "";

            //if (ddlAccType.SelectedItem.Text == "Enercon")
            //{
            //    if (ddlMatType.SelectedItem.Text == "RawMaterial")
            //    {
            //        MatType = "1";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "Tools")
            //    {
            //        MatType = "2";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "Assets")
            //    {
            //        MatType = "3";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "General Item")
            //    {
            //        MatType = "4";
            //    }
            //    if (ddlMatType.SelectedItem.Text == "RawMaterial")
            //    {
            //        SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //        SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='1' and SPM.PO_Status='1' And SPS.Std_PO_No not in ";
            //        SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' and MatType='1') ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='1' And SPM.PO_Status='1' ";
            //        SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No and MatType='1') ";
            //        SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //        SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No and MatType='1' ) ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //        SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //        SSQL = SSQL + " From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //        SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //        SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='1' And SPM.PO_Status='1' And ";
            //        SSQL = SSQL + " PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //        SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "Tools")
            //    {
            //        SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //        SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='2'  And SPS.Std_PO_No not in ";
            //        SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' and MatType='2' And SPM.PO_Status='1') ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='2' And SPM.PO_Status='1'";
            //        SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No and MatType='2') ";
            //        SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //        SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No and MatType='2' ) ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //        SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //        SSQL = SSQL + " From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //        SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //        SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='2' And SPM.PO_Status='1' And ";
            //        SSQL = SSQL + " PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //        SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "Assets")
            //    {
            //        SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //        SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And SPM.PO_Status='1' And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='3'  And SPS.Std_PO_No not in ";
            //        SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' and MatType='3') ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='3' And SPM.PO_Status='1' ";
            //        SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No and MatType='2') ";
            //        SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //        SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No and MatType='3' ) ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //        SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //        SSQL = SSQL + " From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //        SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //        SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='3' And SPM.PO_Status='1' And ";
            //        SSQL = SSQL + " PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //        SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "General Item")
            //    {
            //        SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //        SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='4' And SPM.PO_Status='1'  And SPS.Std_PO_No not in ";
            //        SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' and MatType='4') ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='4' And SPM.PO_Status='1' ";
            //        SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No and MatType='2') ";
            //        SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //        SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No and MatType='4' ) ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //        SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //        SSQL = SSQL + " From Trans_Enercon_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //        SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //        SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='4' And SPM.PO_Status='1' And ";
            //        SSQL = SSQL + " PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //        SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";
            //    }
            //}
            //else if (ddlAccType.SelectedItem.Text == "Escrow")
            //{
            //    if (ddlMatType.SelectedItem.Text == "RawMaterial")
            //    {
            //        SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //        SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='1' And SPM.PO_Status='1' And SPS.Std_PO_No not in ";
            //        SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' and MatType='1') ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='1' And SPM.PO_Status='1' ";
            //        SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No and MatType='1') ";
            //        SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //        SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No and MatType='1' ) ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //        SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //        SSQL = SSQL + " From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //        SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //        SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='1' And SPM.PO_Status='1' And ";
            //        SSQL = SSQL + " PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //        SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "Tools")
            //    {
            //        SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //        SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='2' And SPM.PO_Status='1' And SPS.Std_PO_No not in ";
            //        SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' and MatType='2') ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='2' And SPM.PO_Status='1' ";
            //        SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No and MatType='2') ";
            //        SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //        SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No and MatType='2' ) ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //        SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //        SSQL = SSQL + " From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //        SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //        SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='2' And SPM.PO_Status='1' And ";
            //        SSQL = SSQL + " PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //        SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "Assets")
            //    {
            //        SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //        SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='3' And SPM.PO_Status='1'  And SPS.Std_PO_No not in ";
            //        SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' and MatType='3') ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='3' And SPM.PO_Status='1' ";
            //        SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No and MatType='2') ";
            //        SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //        SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No and MatType='3' ) ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //        SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //        SSQL = SSQL + " From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //        SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //        SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='3' And SPM.PO_Status='1' And ";
            //        SSQL = SSQL + " PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //        SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "General Item")
            //    {
            //        SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //        SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='4' And SPM.PO_Status='1'  And SPS.Std_PO_No not in ";
            //        SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' and MatType='4') ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And  SPM.MatType='4' And SPM.PO_Status='1' ";
            //        SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //        SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No and MatType='2') ";
            //        SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //        SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No and MatType='4' ) ";

            //        SSQL = SSQL + " UNION ALL ";

            //        SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //        SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //        SSQL = SSQL + " From Trans_Escrow_PurOrd_Main SPM ";
            //        SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //        SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //        SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //        SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //        SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //        SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.MatType='4' And SPM.PO_Status='1' And ";
            //        SSQL = SSQL + " PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //        SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";
            //    }
            //}
            //else if (ddlAccType.SelectedItem.Text == "Coral")
            //{
            //    if (ddlMatType.SelectedItem.Text == "RawMaterial")
            //    {
            //        MatType = "1";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "Tools")
            //    {
            //        MatType = "2";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "Assets")
            //    {
            //        MatType = "3";
            //    }
            //    else if (ddlMatType.SelectedItem.Text == "General Item")
            //    {
            //        MatType = "4";
            //    }


            //    SSQL = " Select distinct Std_PO_No,Std_PO_Date from( ";
            //    SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //    SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Coral_PurOrd_Main SPM ";
            //    SSQL = SSQL + " Inner Join Trans_Coral_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode ";
            //    SSQL = SSQL + " And SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //    SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' And ";
            //    SSQL = SSQL + " SPM.MatType='"+ MatType +"' And SPM.PO_Status='1' And SPS.Std_PO_No not in ";
            //    SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            //    SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ) ";

            //    SSQL = SSQL + " UNION ALL ";

            //    SSQL = SSQL + " Select '1' as W, SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,";
            //    SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Coral_PurOrd_Main SPM ";
            //    SSQL = SSQL + " Inner Join Trans_Coral_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And  ";
            //    SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode And SPM.Std_PO_No = SPS.Std_PO_No Where SPM.Ccode = '" + SessionCcode + "' ";
            //    SSQL = SSQL + " And SPM.Lcode = '" + SessionLcode + "' And SPM.FinYearCode = '" + SessionFinYearCode + "' ";
            //    SSQL = SSQL + " And SPM.MatType='" + MatType + "' And SPM.PO_Status='1' And SPS.ItemCode not in (Select ItemCode from  ";
            //    SSQL = SSQL + " Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' ";
            //    SSQL = SSQL + " And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No ) ";
            //    SSQL = SSQL + " And SPS.Std_PO_No in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where ";
            //    SSQL = SSQL + " Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' ";
            //    SSQL = SSQL + " And PurOrdNo = SPS.Std_PO_No ) ";

            //    SSQL = SSQL + " UNION ALL ";

            //    SSQL = SSQL + " Select '1' as W,SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            //    SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            //    SSQL = SSQL + " From Trans_Coral_PurOrd_Main SPM ";
            //    SSQL = SSQL + " Inner Join Trans_Coral_PurOrd_Sub SPS on SPM.Ccode = SPS.Ccode And SPM.Lcode = SPS.Lcode And ";
            //    SSQL = SSQL + " SPM.FinYearCode = SPS.FinYearCode  And SPM.Std_PO_No = SPS.Std_PO_No ";
            //    SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.Ccode = SPS.Ccode And PRS.Lcode = PRS.Lcode And ";
            //    SSQL = SSQL + " SPS.FinYearCode = PRS.FinYearCode And PRS.PurOrdNo = SPS.Std_PO_No And PRS.ItemCode = SPS.ItemCode ";
            //    SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            //    SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            //    SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' ";
            //    SSQL = SSQL + " And SPM.MatType='" + MatType + "' And SPM.PO_Status='1' And PRS.Ccode = '" + SessionCcode + "' ";
            //    SSQL = SSQL + " And PRS.Lcode = '" + SessionLcode + "' And PRS.FinYearCode = '" + SessionFinYearCode + "' ";
            //    SSQL = SSQL + " Group By SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            //    SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ) as P Where W = '1' Order by Std_PO_No ";

            //}


            SSQL = " Select distinct Std_PO_No,Std_PO_Date from(  ";

            SSQL = SSQL + " Select '1' as W, SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No  ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And  ";
            SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' and (SPM.PO_Status='2' or SPM.PO_Status='3' or SPM.PO_Status='4') ";
            SSQL = SSQL + " And SPS.RefNo not in  (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub ";
            SSQL = SSQL + " Where Ccode = '" + SessionCcode + "'  And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "')  ";

            SSQL = SSQL + "  UNION ALL ";

            SSQL = SSQL + " Select '1' as W, SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,  ";
            SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Enercon_PurOrd_Main SPM ";
            SSQL = SSQL + " Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "'  And SPM.Lcode = '" + SessionLcode + "' ";
            SSQL = SSQL + " And SPM.FinYearCode = '" + SessionFinYearCode + "' And (SPM.PO_Status='2' or SPM.PO_Status='3' or SPM.PO_Status='4') ";
            SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.RefNo) ";
            SSQL = SSQL + " And SPS.RefNo in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where  Ccode = '" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "'  And PurOrdNo = SPS.RefNo) ";

            SSQL = SSQL + " UNION ALL ";

            SSQL = SSQL + " Select '1' as W,SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            SSQL = SSQL + " From Trans_Enercon_PurOrd_Main SPM  Inner Join Trans_Enercon_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No ";
            SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.PurOrdNo = SPS.RefNo And PRS.ItemCode = SPS.ItemCode ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And (SPM.PO_Status='2' or SPM.PO_Status='3' or SPM.PO_Status='4') ";
            SSQL = SSQL + " And  PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And ";
            SSQL = SSQL + " PRS.FinYearCode = '" + SessionFinYearCode + "'  ";
            SSQL = SSQL + " Group By SPS.RefNo,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ";

            // Escrow Purchase Order Fetch

            SSQL = SSQL + " UNION ALL ";

            SSQL = SSQL + " Select '1' as W, SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No  ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And  ";
            SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' and (SPM.PO_Status='2' or SPM.PO_Status='3' or SPM.PO_Status='4') ";
            SSQL = SSQL + " And SPS.RefNo not in  (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub ";
            SSQL = SSQL + " Where Ccode = '" + SessionCcode + "'  And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "')  ";

            SSQL = SSQL + "  UNION ALL ";

            SSQL = SSQL + " Select '1' as W, SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,  ";
            SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Escrow_PurOrd_Main SPM ";
            SSQL = SSQL + " Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "'  And SPM.Lcode = '" + SessionLcode + "' ";
            SSQL = SSQL + " And SPM.FinYearCode = '" + SessionFinYearCode + "' And (SPM.PO_Status='2' or SPM.PO_Status='3' or SPM.PO_Status='4') ";
            SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.RefNo) ";
            SSQL = SSQL + " And SPS.RefNo in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where  Ccode = '" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "'  And PurOrdNo = SPS.RefNo) ";

            SSQL = SSQL + " UNION ALL ";

            SSQL = SSQL + " Select '1' as W,SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            SSQL = SSQL + " From Trans_Escrow_PurOrd_Main SPM  Inner Join Trans_Escrow_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No ";
            SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.PurOrdNo = SPS.RefNo And PRS.ItemCode = SPS.ItemCode ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And ";
            SSQL = SSQL + " (SPM.PO_Status='2' or SPM.PO_Status='3' or SPM.PO_Status='4')  ";
            SSQL = SSQL + " And  PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And ";
            SSQL = SSQL + " PRS.FinYearCode = '" + SessionFinYearCode + "'  ";
            SSQL = SSQL + " Group By SPS.RefNo,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ";

            // Coral Purchase Order  Fetch

            SSQL = SSQL + " UNION ALL ";

            SSQL = SSQL + " Select '1' as W, SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Coral_PurOrd_Main SPM ";
            SSQL = SSQL + " Inner Join Trans_Coral_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No  ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And  ";
            SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' and (SPM.PO_Status='2' or SPM.PO_Status='3' or SPM.PO_Status='4') ";
            SSQL = SSQL + " And SPS.RefNo not in  (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub ";
            SSQL = SSQL + " Where Ccode = '" + SessionCcode + "'  And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "')  ";

            SSQL = SSQL + "  UNION ALL ";

            SSQL = SSQL + " Select '1' as W, SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,  ";
            SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty From Trans_Coral_PurOrd_Main SPM ";
            SSQL = SSQL + " Inner Join Trans_Coral_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "'  And SPM.Lcode = '" + SessionLcode + "' ";
            SSQL = SSQL + " And SPM.FinYearCode = '" + SessionFinYearCode + "' And (SPM.PO_Status='2' or SPM.PO_Status='3' or SPM.PO_Status='4') ";
            SSQL = SSQL + " And SPS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub Where Ccode = '" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "' And PurOrdNo = SPS.Std_PO_No) ";
            SSQL = SSQL + " And SPS.RefNo in (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub Where  Ccode = '" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode = '" + SessionLcode + "' And FinYearCode = '" + SessionFinYearCode + "'  And PurOrdNo = SPS.RefNo) ";

            SSQL = SSQL + " UNION ALL ";

            SSQL = SSQL + " Select '1' as W,SPS.RefNo [Std_PO_No],SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name, ";
            SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty ";
            SSQL = SSQL + " From Trans_Coral_PurOrd_Main SPM  Inner Join Trans_Coral_PurOrd_Sub SPS on SPM.Std_PO_No = SPS.Std_PO_No ";
            SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub PRS on PRS.PurOrdNo = SPS.RefNo And PRS.ItemCode = SPS.ItemCode ";
            SSQL = SSQL + " Where SPM.Ccode = '" + SessionCcode + "' And SPM.Lcode = '" + SessionLcode + "' And ";
            SSQL = SSQL + " SPM.FinYearCode = '" + SessionFinYearCode + "' And SPS.Ccode = '" + SessionCcode + "' And ";
            SSQL = SSQL + " SPS.Lcode = '" + SessionLcode + "' And SPS.FinYearCode = '" + SessionFinYearCode + "' And SPM.PO_Status='2' ";
            SSQL = SSQL + " And  PRS.Ccode = '" + SessionCcode + "' And PRS.Lcode = '" + SessionLcode + "' And ";
            SSQL = SSQL + " PRS.FinYearCode = '" + SessionFinYearCode + "'  ";
            SSQL = SSQL + " Group By SPS.RefNo,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty ";
            SSQL = SSQL + " having(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0 ";

            SSQL = SSQL + " ) as P Where W = '1' Order by Std_PO_No ";
             
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlOrdNo.DataSource = DT;

            DataRow dr = DT.NewRow();

            dr["Std_PO_No"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);
            ddlOrdNo.DataTextField = "Std_PO_No";
            ddlOrdNo.DataValueField = "Std_PO_No";
            ddlOrdNo.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    private void Load_Data_PurOrd_No()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        if (ddlAccType.SelectedItem.Text == "Enercon")
        {
            SSQL = "Select Std_PO_No[OrderNo],Std_PO_Date[OrderDate] from Trans_Enercon_PurOrd_Main where (PO_Status='2' or PO_Status='3' or PO_Status='4') ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
        }
        else if (ddlAccType.SelectedItem.Text == "Escrow")
        {
            SSQL = "Select Std_PO_No[OrderNo],Std_PO_Date[OrderDate] from Trans_Escrow_PurOrd_Main where (PO_Status='2' or PO_Status='3' or PO_Status='4') ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
        }
        else if (ddlAccType.SelectedItem.Text == "Coral")
        {
            SSQL = "Select Std_PO_No[OrderNo],Std_PO_Date[OrderDate] from Trans_Coral_PurOrd_Main where (PO_Status='2' or PO_Status='3' or PO_Status='4') ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlOrdNo.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["OrderNo"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlOrdNo.DataTextField = "OrderNo";
        ddlOrdNo.DataValueField = "OrderNo";
        ddlOrdNo.DataBind();
    }

    private void Load_Supplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " Lcode='" + SessionLcode + "' And Status!='Delete' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlSuppName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["LedgerName"] = "-Select-";
        dr["LedgerCode"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlSuppName.DataTextField = "LedgerName";
        ddlSuppName.DataValueField = "LedgerCode";
        ddlSuppName.DataBind();
    }

    private void Load_TransPortName()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Transport_Name[TransName],TransPort_No[TransNo] from TransportMaster where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " Lcode='" + SessionLcode + "' And Status!='Delete' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlTransName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["TransName"] = "-Select-";
        dr["TransNo"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlTransName.DataTextField = "TransName";
        ddlTransName.DataValueField = "TransNo";
        ddlTransName.DataBind();
    }
    
    private void Clear_All_Field()
    {
        txtGPINNo.Text = ""; txtGPINDate.Text = ""; ddlTransName.SelectedItem.Text = "-Select-";
        ddlTransName.SelectedValue = "-Select-"; ddlSuppName.SelectedValue = "-Select-";
        txtSuppInvNo.Text = ""; txtSuppInvDate.Text = ""; txtVehiNo.Text = "";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("GPIN_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        TotalReqQty();

    }
    public void TotalReqQty()
    {

        //ReqQty = 0;
        //DataTable dt = new DataTable();
        //dt = (DataTable)ViewState["ItemTable"];
        //for (int i = 0; i < dt.Rows.Count; i++)
        //{
        //    ReqQty = Convert.ToDecimal(ReqQty) + Convert.ToDecimal(dt.Rows[i]["ReuiredQty"]);
        //}
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        DataTable DT = new DataTable();
        SSQL = "Select * from Trans_Pur_GIN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Pur_GIN_No ='" + txtGPINNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            //txtGPINNo.ReadOnly = true;
            txtGPINNo.Text = Main_DT.Rows[0]["Pur_GIN_No"].ToString();
            txtGPINDate.Text = Main_DT.Rows[0]["Pur_GIN_Date"].ToString();
            ddlSuppName.SelectedValue = Main_DT.Rows[0]["SuppCode"].ToString();
            txtSuppInvNo.Text = Main_DT.Rows[0]["SuppInvNo"].ToString();
            txtSuppInvDate.Text = Main_DT.Rows[0]["SuppInvDate"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            ddlTransName.SelectedItem.Text = Main_DT.Rows[0]["TransportName"].ToString();
            hfTransportCode.Value = Main_DT.Rows[0]["TransportCode"].ToString();

            txtVehiNo.Text = Main_DT.Rows[0]["VehicleNo"].ToString();

            SSQL = "Select PurOrdNo[OrderNo],PurOrdDate[OrderDate],PurOrdRefNo[RefNo],PurRquNo[RequestNo],PurOrdDate[RequestDate],";
            SSQL = SSQL + " AccType,case when AccType='1' then 'Enercon' when AccType='2' then 'Escrow' ";
            SSQL = SSQL + " when AccType='3' Then 'Coral' End [AccountType], ";
            SSQL = SSQL + " case when MatType='1' then 'RawMaterial' when MatType='2' then 'Tools' when MatType='3' Then 'Asset' ";
            SSQL = SSQL + " When MatType='4' then 'General Item' End [MaterialType],DeptName,TotQty[TotalQuantity], ";
            SSQL = SSQL + " TotAmt[TotalAmt] From Trans_Pur_GIN_Sub  ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And Pur_GIN_No='" + txtGPINNo.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = DT;

            Repeater1.DataSource = DT;
            Repeater1.DataBind();

            ddlAccType.SelectedValue = DT.Rows[0]["AccType"].ToString();
            ddlOrdNo.SelectedItem.Text = Main_DT.Rows[0]["Pur_GIN_No"].ToString();

            txtTotAmt.Text = DT.Rows[0]["TotalAmt"].ToString();
            txtTotQty.Text = DT.Rows[0]["TotalQuantity"].ToString();
                            
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_PO_GPIN_Main.aspx");
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
       
        if (!ErrFlag)
        {
           
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("OrderNo", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderDate", typeof(string)));
        dt.Columns.Add(new DataColumn("RefNo", typeof(string)));
        dt.Columns.Add(new DataColumn("RequestNo", typeof(string)));
        dt.Columns.Add(new DataColumn("RequestDate", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("AccountType", typeof(string)));
        dt.Columns.Add(new DataColumn("MaterialType", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        
    }

    protected void ddlOrdNo_SelectedIndexChanged(object sender, EventArgs e)
    {

        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = " Select A.OrderNo,A.OrderDate,A.RefNo,A.RequestNo,A.RequestDate,A.SuppCode,A.SuppName,A.AccountType,A.MaterialType,";
        SSQL = SSQL + " A.DeptName,A.TotalQuantity,A.TotalAmt from (";
        SSQL = SSQL + " Select Std_PO_No[OrderNo],Std_PO_Date[OrderDate],RefNo[RefNo],Pur_Request_No[RequestNo],";
        SSQL = SSQL + " Pur_Request_Date[RequestDate],Supp_Code[SuppCode],Supp_Name[SuppName],";
        SSQL = SSQL + " 'Enercon' [AccountType],case when MatType='1' then 'RawMaterial' when MatType='2' then 'Tools' when MatType='3'";
        SSQL = SSQL + " then 'Asset' when MatType='4' then 'General Item' End [MaterialType],DeptName,TotalQuantity,TotalAmt ";
        SSQL = SSQL + " From Trans_Enercon_PurOrd_Main where (PO_Status='2' or PO_Status='3' or PO_Status='4') and ";
        SSQL = SSQL + " Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' and ";
        SSQL = SSQL + " RefNo='" + ddlOrdNo.SelectedValue + "' ";
        //SSQL = SSQL + " Std_PO_No='" + ddlOrdNo.SelectedValue + "' ";

        SSQL = SSQL + " UNION ALL ";

        SSQL = SSQL + " Select Std_PO_No[OrderNo],Std_PO_Date[OrderDate],RefNo[RefNo],Pur_Request_No[RequestNo],";
        SSQL = SSQL + "  Pur_Request_Date[RequestDate],Supp_Code[SuppCode],Supp_Name[SuppName],";
        SSQL = SSQL + " 'Escrow'[AccountType],case when MatType='1' then 'RawMaterial' when MatType='2' then 'Tools' when MatType='3'";
        SSQL = SSQL + " Then 'Asset' when MatType='4' then 'General Item' End [MaterialType],DeptName,TotalQuantity,TotalAmt ";
        SSQL = SSQL + " From Trans_Escrow_PurOrd_Main where (PO_Status='2' or PO_Status='3' or PO_Status='4') and ";
        SSQL = SSQL + " Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' and ";
        SSQL = SSQL + " RefNo='" + ddlOrdNo.SelectedValue + "' ";
        //SSQL = SSQL + " Std_PO_No='" + ddlOrdNo.SelectedValue + "' ";

        SSQL = SSQL + " UNION ALL ";

        SSQL = SSQL + " Select Std_PO_No[OrderNo],Std_PO_Date[OrderDate],RefNo[RefNo],Pur_Request_No[RequestNo],";
        SSQL = SSQL + " Pur_Request_Date[RequestDate],Supp_Code[SuppCode],Supp_Name[SuppName],";
        SSQL = SSQL + " 'Coral'[AccountType],case when MatType='1' then 'RawMaterial' when MatType='2' then 'Tools' when MatType='3'";
        SSQL = SSQL + " Then 'Asset' when MatType='4' then 'General Item' End [MaterialType],DeptName,TotalQuantity,TotalAmt ";
        SSQL = SSQL + " From Trans_Coral_PurOrd_Main Where (PO_Status='2' or PO_Status='3' or PO_Status='4') and ";
        SSQL = SSQL + " Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' and ";
        SSQL = SSQL + " RefNo='" + ddlOrdNo.SelectedValue + "')A";

        //SSQL = SSQL + " Std_PO_No='" + ddlOrdNo.SelectedValue + "')A";


        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ViewState["ItemTable"] = DT;

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

        txtTotQty.Text = DT.Rows[0]["TotalQuantity"].ToString();
        txtTotAmt.Text = DT.Rows[0]["TotalAmt"].ToString();

        ddlSuppName.SelectedValue = DT.Rows[0]["SuppCode"].ToString();
    }

    protected void ddlAccType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_PurOrder_No();
    }

  
    protected void ddlSuppName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " LedgerName ='" + ddlSuppName.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hdSuppCode.Value = DT.Rows[0]["LedgerCode"].ToString();
    }

    protected void ddlTransName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from TransportMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Transport_Name ='" + ddlTransName.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfTransportCode.Value = DT.Rows[0]["TransPort_No"].ToString();
    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_PurOrder_No();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT_Check = new DataTable();
            string SaveMode = "Insert";

            string AccTypeText = "";
            string MatTypeText = "";

            //check with Item Details Add with Grid
            DT_Check = (DataTable)ViewState["ItemTable"];

            if (DT_Check.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
            }

            //Auto generate Transaction Function Call
            if (btnSave.Text != "Update")
            {
                if (!ErrFlag)
                {
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "PurOrd Gatepass In", SessionFinYearVal, "1");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        txtGPINNo.Text = Auto_Transaction_No;
                    }
                }
            }

            if (!ErrFlag)
            {
                //if (btnSave.Text == "Update")
                //{
                //Update Main table

                SSQL = " Delete From Trans_Pur_Gin_Main Where Pur_GIN_No='" + txtGPINNo.Text + "' and";
                SSQL = SSQL + "  Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = " Delete From Trans_Pur_Gin_Sub Where Pur_GIN_No='" + txtGPINNo.Text + "' and";
                SSQL = SSQL + "  Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);
                //}


                //string imgpath = "/assets/images/NoImage.png";

                //if (hidd_imgPath.Value != imgpath)
                //{
                //    imgpath = hidd_imgPath.Value.ToString();
                //}

                //Insert Main Table

                SSQL = "Insert Into Trans_Pur_Gin_Main(CCode,LCode,FinYearCode,FinYearVal,Pur_GIN_No,Pur_GIN_Date,SuppCode,SuppName,SuppInvNo,";
                SSQL = SSQL + " SuppInvDate,TransportCode,TransportName,VehicleNo,Remarks,Status,UserId,UsreName,CreateOn,ApprovalStatus)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + txtGPINNo.Text + "','" + txtGPINDate.Text + "','" + ddlSuppName.SelectedValue + "',";
                SSQL = SSQL + " '" + ddlSuppName.SelectedItem.Text + "','" + txtSuppInvNo.Text + "','" + txtSuppInvDate.Text + "',";
                SSQL = SSQL + " '" + hfTransportCode.Value + "','" + ddlTransName.SelectedItem.Text + "','" + txtVehiNo.Text + "',";
                SSQL = SSQL + " '" + txtRemarks.Text + "','Add','" + SessionUserID + "','" + SessionUserName + "', Getdate(),'0')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                //Insert Sub Table

                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ItemTable"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Label PurOrdNo = Repeater1.Items[i].FindControl("lblOrdNo") as Label;
                    Label PurOrdDate = Repeater1.Items[i].FindControl("lblOrdDate") as Label;
                    Label PurOrdRefNo = Repeater1.Items[i].FindControl("lblPORefNo") as Label;
                    Label RquNo = Repeater1.Items[i].FindControl("lblRquNo") as Label;
                    Label RquDate = Repeater1.Items[i].FindControl("lblRquDate") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;
                    Label AccType = Repeater1.Items[i].FindControl("lblAccType") as Label;
                    Label MatType = Repeater1.Items[i].FindControl("lblMatType") as Label;

                    if (AccType.Text == "Enercon")
                    {
                        AccTypeText = "1";
                    }
                    else if (AccType.Text == "Escrow")
                    {
                        AccTypeText = "2";
                    }
                    else if (AccType.Text == "Coral")
                    {
                        AccTypeText = "3";
                    }

                    if (MatType.Text == "RawMaterial")
                    {
                        MatTypeText = "1";
                    }
                    else if (MatType.Text == "Tools")
                    {
                        MatTypeText = "2";
                    }
                    else if (MatType.Text == "Asset")
                    {
                        MatTypeText = "3";
                    }
                    else if (MatType.Text == "General Item")
                    {
                        MatTypeText = "4";
                    }

                    SSQL = "Insert Into Trans_Pur_Gin_Sub(CCode,LCode,FinYearCode,FinYearVal,Pur_GIN_No,Pur_GIN_Date,PurOrdNo,PurOrdDate,";
                    SSQL = SSQL + " PurOrdRefNo,PurRquNo,PurRquDate,DeptName,AccType,MatType,TotQty,TotAmt,Status,UserId,UsreName,CreateOn) ";
                    SSQL = SSQL + " values( '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                    SSQL = SSQL + " '" + SessionFinYearVal + "','" + txtGPINNo.Text + "','" + txtGPINDate.Text + "','" + PurOrdNo.Text + "',";
                    SSQL = SSQL + " '" + PurOrdDate.Text + "','" + PurOrdRefNo.Text + "','" + RquNo.Text + "', '" + RquDate.Text + "',";
                    SSQL = SSQL + " '" + DeptName.Text + "', '" + AccTypeText + "','" + MatTypeText + "','" + txtTotQty.Text + "',";
                    SSQL = SSQL + " '" + txtTotAmt.Text + "','Add','" + SessionUserID + "','" + SessionUserName + "', 'Getdate()')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                //Auto generate Transaction Function Call
                if (btnSave.Text != "Update")
                {
                    if (!ErrFlag)
                    {
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "PurOrd Gatepass In", SessionFinYearVal, "1");
                        if (Auto_Transaction_No == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                        }
                        else
                        {
                            txtGPINNo.Text = Auto_Transaction_No;
                        }
                    }
                }

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass IN Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass IN Details Updated Successfully');", true);
                }
                //Clear_All_Field();
                Session["GPIN_No"] = txtGPINNo.Text;
                btnSave.Text = "Update";
                Response.Redirect("Trans_PO_GPIN_Main.aspx");
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
}