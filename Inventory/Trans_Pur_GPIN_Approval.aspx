﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Pur_GPIN_Approval.aspx.cs" Inherits="Transaction_Coral_purchase_request_approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel runat="server" ID="upMain">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Purchase GatePass In List</span></h3>
                                    <asp:Label runat="server" ID="lblErrorMsg" Style="color: red; font-size: x-large"></asp:Label>
                                </div>

                                <div class="row" runat="server" style="padding-top: 25px; padding-bottom: 25px">
                                    <div class="col-md-12">
                                        <div class="row" style="padding-left: 15px">
                                            <div class="col-md-3">
                                                <label class="control-label" for="Req_No">Request Status</label>
                                                <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                                    OnSelectedIndexChanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>

                                            <div class="col-md-3" id="divCancelReason" runat="server" visible="false" style="padding-left: 15px">
                                                <label class="control-label" for="Req_No">Cancel Reason</label>
                                                <asp:TextBox ID="txtCancelReason" runat="server" class="form-control" Style="resize: none" TextMode="MultiLine">
                                                </asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">
                                            <div class="box-body no-padding">
                                                <div class="table-responsive mailbox-messages">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Trans No</th>
                                                                                <th>Date</th>
                                                                                <th>Supplier_InvNo</th>
                                                                                <th>Supplier_InvDate</th>
                                                                                <th>Supplier_Name</th>
                                                                                <th>Approve</th>
                                                                                <th>Cancel</th>
                                                                                <%--<th>View</th>--%>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Eval("Pur_GIN_No")%></td>
                                                                        <td><%# Eval("Pur_GIN_Date")%></td>
                                                                        <td><%# Eval("SuppInvNo")%></td>
                                                                        <td><%# Eval("SuppInvDate")%></td>
                                                                        <td><%# Eval("SuppName")%></td>

                                                                        <td>
                                                                            <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square" runat="server"
                                                                                Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Pur_GIN_No")%>' CommandName='<%# Eval("Pur_GIN_No")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Purchase Request details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="LinkButton1" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Pur_GIN_No")%>' CommandName='<%# Eval("Pur_GIN_No")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Purchase Request details?');">
                                                                            </asp:LinkButton>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-external-link-square" runat="server"
                                                                                Text="" OnCommand="GridEditPurRequestClick" CommandArgument='<%# Eval("Pur_GIN_No")%>'
                                                                                CommandName='<%# Eval("Pur_GIN_No")%>'>
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>

                                                        <div class="row">
                                                            <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false">
                                                                <HeaderTemplate>
                                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Trans No</th>
                                                                                <th>Date</th>
                                                                                <th>Supplier_InvNo</th>
                                                                                <th>Supplier_InvDate</th>
                                                                                <th>Supplier_Name</th>
                                                                                <%--<th>View</th>--%>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Eval("Pur_GIN_No")%></td>
                                                                        <td><%# Eval("Pur_GIN_Date")%></td>
                                                                        <td><%# Eval("SuppInvNo")%></td>
                                                                        <td><%# Eval("SuppInvDate")%></td>
                                                                        <td><%# Eval("SuppName")%></td>

                                                                        <td runat="server" Visible="false">
                                                                            <asp:LinkButton ID="btnApprovalReq_View"  class="btn btn-primary btn-sm fa fa-external-link-square" runat="server"
                                                                                Text="" OnCommand="GridEditPurRequestClick" CommandArgument='<%# Eval("Pur_GIN_No")%>'
                                                                                CommandName='<%# Eval("Pur_GIN_No")%>'>
                                                                            </asp:LinkButton>
                                                                        </td>

                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                        <div class="row">
                                                            <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
                                                                <HeaderTemplate>
                                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <<th>Trans No</th>
                                                                                <th>Date</th>
                                                                                <th>Supplier_InvNo</th>
                                                                                <th>Supplier_InvDate</th>
                                                                                <th>Supplier_Name</th>
                                                                                <th>Cancellation</th>
                                                                                <%--<th>View</th>--%>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Eval("Pur_GIN_No")%></td>
                                                                        <td><%# Eval("Pur_GIN_Date")%></td>
                                                                        <td><%# Eval("SuppInvNo")%></td>
                                                                        <td><%# Eval("SuppInvDate")%></td>
                                                                        <td><%# Eval("SuppName")%></td>
                                                                        <td><%# Eval("CanceledReason")%></td>

                                                                        <td runat="server" Visible="false">
                                                                            <asp:LinkButton ID="btnApprovalReq_View" class="btn btn-primary btn-sm fa fa-external-link-square" runat="server"
                                                                                Text="" OnCommand="GridEditPurRequestClick" CommandArgument='<%# Eval("Pur_GIN_No")%>'
                                                                                CommandName='<%# Eval("Pur_GIN_No")%>'>
                                                                            </asp:LinkButton>
                                                                        </td>

                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                }
            });
        };
    </script>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
</asp:Content>

