﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GatePassIn_Sub.aspx.cs" Inherits="Trans_GatePassIn_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" src="../assets/js/Trans_Receipt_Calc.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>

     <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.js-states').select2();
                }
            });
        };
    </script>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class=" text-primary"></i>GatePass In</h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">GatePassIn No</label>
                                    <asp:Label ID="txtGPINNo" runat="server" class="form-control" ></asp:Label>
                                </div>
                            </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">GatePassIn Date</label>
                                    <asp:TextBox ID="txtGPINDate" runat="server" class="form-control pull-right datepicker" AutoComplete="off"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtGPINDate" ValidationGroup="Validate_Field" class="form_error" 
                                         ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtGPINDate" ValidChars="0123456789./">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">GatePassOut No.</label>
                                    <asp:DropDownList runat="server" ID="ddlGPOut" class="form-control select2" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlGPOut_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">GatePassOut Date</label>
                                    <asp:TextBox ID="txtGPOutDate" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Name</label>
                                    <asp:TextBox ID="txtSuppName" runat="server" class="form-control" ></asp:TextBox>
                                    <asp:HiddenField ID="hdSuppCode" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Details</label>
                                    <asp:TextBox ID="txtSuppDet" runat="server" TextMode="MultiLine" style="resize:none" class="form-control">
                                    </asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Invoice No.</label>
                                    <asp:TextBox ID="txtSuppInvNo" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Invoice Date</label>
                                    <asp:TextBox ID="txtSuppInvDate" runat="server" class="form-control datepicker" AutoComplete="off" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtSuppInvDate" ValidationGroup="Validate_Field" class="form_error" 
                                         ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtSuppInvDate" ValidChars="0123456789./">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>

                            </div>
                        <div class="row">
                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Payment Mode</label>
                                    <asp:TextBox ID="txtPayMode" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Vehicle No</label>
                                    <asp:TextBox ID="txtVehiNo" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
					   
                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Notes</label>
                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Remarks</label>
                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

					    </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Item Details</span></h3>
                                        <div class="box-tools pull-right">
                                            <div class="has-feedback">
                                                <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <!-- /.box-tools -->
                                    </div>
                                    <!-- /.box-header -->

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputName">ItemName</label>
                                                <asp:DropDownList runat="server" ID="ddlItemName" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged" >
                                                </asp:DropDownList>

                                                <asp:HiddenField id="hfDeptCode" runat="server"/>
                                                <asp:HiddenField id="hfDeptName" runat="server"/>
                                                <asp:HiddenField id="hfWarehouseCode" runat="server"/>
                                                <asp:HiddenField id="hfWarehouseName" runat="server"/>
                                                <asp:HiddenField id="hfRackNo" runat="server"/>
                                                <asp:HiddenField id="hfUOMCode" runat="server"/>
                                                <asp:HiddenField id="hfUOMName" runat="server"/>

                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Out Qty</label>
                                                <asp:TextBox runat="server" ID="txtOutQty" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">In Qty</label>
                                                <asp:TextBox runat="server" ID="txtGPInQty" class="form-control" AutoPostBack="true"
                                                    OnTextChanged="txtGPInQty_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Rate</label>
                                                <asp:TextBox runat="server" ID="txtRate" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Item Value</label>
                                                <asp:TextBox runat="server" ID="txtItemVal" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">GST Type</label>
                                                <asp:DropDownList runat="server" ID="ddlGST" class="form-control select2" AutoPostBack="true" 
                                                    OnSelectedIndexChanged="ddlGST_SelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:HiddenField id="hfCGSTPre" runat="server"/>
                                                <asp:HiddenField id="hfSGSTPre" runat="server"/>
                                                <asp:HiddenField id="hfIGSTPre" runat="server"/>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">CGST</label>
                                                <asp:TextBox runat="server" ID="txtCGSTAmt" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">SGST</label>
                                                <asp:TextBox runat="server" ID="txtSGSTAmt" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">IGST</label>
                                                <asp:TextBox runat="server" ID="txtIGSTAmt" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Net Value</label>
                                                <asp:TextBox runat="server" ID="txtItemNetAmt" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" style="padding-top:25px">
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-primary" OnClick="btnAdd_Click" />
                                        </div>
                                    </div>

                                    <div class="box-body no-padding" >
                                        <div class="table-responsive mailbox-messages" runat="server" style="padding-top:15px">
					                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false" OnItemCommand="Repeater1_ItemCommand">
			                                    <HeaderTemplate>
                                                    <table id="tbltest"  class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>S. No</th>
                                                                <th runat="server" visible="false">Item Code</th>
                                                                <th>Item Name</th>
                                                                <th runat="server" visible="false">DeptCode</th>
                                                                <th runat="server" visible="false">DeptName</th>
                                                                <th runat="server" visible="false">WarehouseCode</th>
                                                                <th runat="server" visible="false">Warehouse</th>
                                                                <th>GatePass Out Qty</th>
                                                                <th>GatePass In Qty</th>
                                                                <th runat="server" visible="false">Scrab Qty</th>
                                                                <th>Rate</th>
                                                                <th runat="server" visible="false" >ItemTotal</th>
                                                                <th runat="server" visible="false">CGSTPer</th>
                                                                <th>CGST</th>
                                                                <th runat="server" visible="false">SGSTPer</th>
                                                                <th>SGST</th>
                                                                <th runat="server" visible="false">IGSTPer</th>
                                                                <th>IGST</th>
                                                                <th>Line Total</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                           
                                                            <%# Container.ItemIndex + 1 %>
                                                        </td>
                                                        <td runat="server" visible="false">
                                                             <asp:Label id="lblItemCode" runat="server" Text='<%# Eval("ItemCode")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label id="lblItemName" runat="server" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                             <asp:Label id="lblUOMCode" runat="server" Text='<%# Eval("UOMCode")%>'></asp:Label>
                                                        </td>
                                                        <td runat="server" visible="false">
                                                             <asp:Label id="lblUOMName" runat="server" Text='<%# Eval("UOMName")%>'></asp:Label>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                             <asp:Label id="lblDeptCode" runat="server" Text='<%# Eval("DeptCode")%>'></asp:Label>
                                                        </td>
                                                        <td runat="server" visible="false">
                                                             <asp:Label id="lblDeptName" runat="server" Text='<%# Eval("DeptName")%>'></asp:Label>
                                                        </td>
                                                        <td runat="server" visible="false">
                                                             <asp:Label id="lblWarehouseCode" runat="server" Text='<%# Eval("WarehouseCode")%>'></asp:Label>
                                                        </td>
                                                        <td runat="server" visible="false">
                                                             <asp:Label id="lblWarehouse" runat="server" Text='<%# Eval("WarehouseName")%>'></asp:Label>
                                                        </td>
                                                        <td runat="server" visible="false">
                                                             <asp:Label id="lblRackName" runat="server" Text='<%# Eval("RackName")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label id="lblOutQty" runat="server" Text='<%# Eval("OutQty")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:TextBox runat="server"  ID="txtInQty" Text='<%# Eval("INQty")%>' 
                                                               Width="100px" BorderStyle="None" AutoPostBack="true" OnTextChanged="txtInQty_TextChanged"> 
                                                            </asp:TextBox>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label id="lblScrabQty" runat="server" Text='<%# Eval("ScrabQty")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" id="txtRate" Text='<%# Eval("Rate")%>'
                                                                Width="100px" BorderStyle="None"></asp:TextBox> 
                                                        </td>
                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" id="lblItemTot" Text='<%# Eval("ItemTotal")%>'
                                                                BorderStyle="None" AutoPostBack="true" OnTextChanged="txtRate_TextChanged"></asp:Label> 
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" id="lblGSTType" Text='<%# Eval("GSTType")%>'></asp:Label>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" id="lblCGSTPer" Text='<%# Eval("CGSTPer")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" id="lblCGSTAmt" Text='<%# Eval("CGSTAmt")%>'></asp:Label>
                                                        </td>
                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" id="lblSGSTPer" Text='<%# Eval("SGSTPer")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" id="lblSGSTAmt" Text='<%# Eval("SGSTAmt")%>'></asp:Label>
                                                        </td>
                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" id="lblIGSTPer" Text='<%# Eval("IGSTPer")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" id="lblIGSTAmt" Text='<%# Eval("IGSTAmt")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" id="lblLineTot" Text='<%# Eval("LineTotal")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>                                
			                                </asp:Repeater>
                                            <!-- /.table -->            
                                        </div>
                                        <!-- /.mail-box-messages -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-2">
                                <label for="exampleInputName">Total Qty</label>
                                <asp:TextBox ID="txtTotQty" runat="server" class="form-control" ></asp:TextBox>
                            </div>

                            <div class="col-md-2" runat="server" visible="false">
                                <label for="exampleInputName">Round Off</label>
                                <asp:TextBox ID="txtRoundOff" runat="server" class="form-control" AutoPostBack="true"
                                    OnTextChanged="txtRoundOff_TextChanged" ></asp:TextBox>
                            </div>

                            <div class="col-md-2">
                                <label for="exampleInputName">Total Amount</label>
                                <asp:TextBox ID="txtTotAmt" runat="server" class="form-control" ></asp:TextBox>
                            </div>
                       </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary"  runat="server" Text="Save" ValidationGroup="Validate_Field" 
                                OnClick="btnSave_Click"/>
                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBack" class="btn btn-default"  runat="server" Text="Back To List" OnClick="btnBack_Click"/>
                        </div>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>
        </div> 
        </section>
        <!-- /.content -->
    </div>
</asp:Content>

