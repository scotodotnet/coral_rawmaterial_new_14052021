﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Reservation_Sub.aspx.cs" Inherits="Inventory_Trans_Reservation_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class=" text-primary"></i>Material Requisition</h1>
            <asp:Label runat="server" ID="lblErrorMsg" Style="color: red; font-size: x-large"></asp:Label>
        </section>
        <!-- Main content -->
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Trans No</label>
                                                <asp:Label ID="txtTransNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Trans Date</label>
                                                <asp:TextBox ID="txtTransDate" runat="server" class="form-control datepicker" AutoComplete="off"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Requisition By</label>
                                                <asp:DropDownList ID="ddlReserby" runat="server" class="form-control select2" AutoComplete="true">
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Notes</label>
                                                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" runat="server" Style="resize: none" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Item Details</span></h3>
                                                </div>

                                                <asp:Panel ID="pnlItemBase" runat="server">

                                                    <div class="row" runat="server" style="padding-top: 1%">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Type Of Material Requisition</label>
                                                                <asp:DropDownList ID="ddlSelectType" runat="server" class="form-control select2"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSelectType_SelectedIndexChanged">
                                                                    <asp:ListItem Value="1">WorkOrder Based</asp:ListItem>
                                                                    <asp:ListItem Value="2">Direct Stock</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="pnlWorkOrderBase" runat="server">
                                                        <div class="row" runat="server">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Work Order No</label>
                                                                    <asp:DropDownList ID="ddlWorkOrdNo" runat="server" class="form-control select2"
                                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlWorkOrdNo_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Generator Model</label>
                                                                    <asp:Label ID="lblWOGenMdlName" class="form-control" runat="server"></asp:Label>
                                                                    <asp:HiddenField ID="hfWOGenMdlCode" runat="server"></asp:HiddenField>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Generator PartName</label>
                                                                    <asp:Label ID="lblWOGenPart" class="form-control" runat="server"></asp:Label>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Production Stage</label>
                                                                    <asp:DropDownList ID="ddlWOPrdStage" runat="server" class="form-control select2"
                                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlWOPartName_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlReserSingle" runat="server" Visible="false">
                                                        <div class="row" runat="server" style="padding-top: 15px">

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Work Order No</label>
                                                                    <asp:DropDownList ID="ddlSingWorkOrdNo" runat="server" class="form-control select2"
                                                                        AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">SAP No</label>
                                                                    <asp:DropDownList ID="ddlSapNo" runat="server" class="form-control select2"
                                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlSapNo_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Item Name</label>
                                                                    <asp:DropDownList ID="ddlItemName" runat="server" class="form-control select2"
                                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                                                                    </asp:DropDownList>

                                                                    <asp:HiddenField ID="hfUOMName" runat="server" />
                                                                </div>

                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Stock Qty</label>
                                                                    <asp:Label ID="txtStockQty" runat="server" class="form-control"></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Reservation Qty</label>
                                                                    <asp:TextBox ID="txtIssueQty" runat="server" class="form-control"
                                                                        AutoPostBack="true" AutoComplete="off" OnTextChanged="txtIssueQty_TextChanged">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2" runat="server" style="padding-top: 25px">
                                                                <div class="form-group">
                                                                    <asp:Button runat="server" ID="btnAddItem" class="btn btn-primary" Text="Add Items" OnClick="btnAddItem_Click" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlGridItemDet" class="panel panel-success" runat="server"
                                                        Height="400px" Style="overflow-x: scroll; overflow-y: scroll" BorderStyle="None">
                                                        <div class="box-body no-padding" runat="server" style="padding-top: 15px">
                                                            <div class="table-responsive mailbox-messages">
                                                                <asp:Repeater ID="rptBOMReser" runat="server" EnableViewState="false">
                                                                    <HeaderTemplate>
                                                                        <table id="tblReserBOMDet" class="table table-hover table-bordered table-striped">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>WorkOrder_No</th>
                                                                                    <th>Generator_Part</th>
                                                                                    <th>Production_Stage</th>
                                                                                    <th>SapNo</th>
                                                                                    <th runat="server" visible="false">Item Code</th>
                                                                                    <th>Item_Name</th>
                                                                                    <th runat="server" visible="false">DeptCode</th>
                                                                                    <th runat="server" visible="false">DeptName</th>
                                                                                    <th runat="server" visible="false">WareHouseCode</th>
                                                                                    <th runat="server" visible="false">WareHouseName</th>
                                                                                    <th runat="server" visible="false">RackNo</th>
                                                                                    <th runat="server" visible="false">UOMCode</th>
                                                                                    <th>UOM</th>
                                                                                    <th>Material_Requisition_Qty</th>
                                                                                    <th>Mode</th>
                                                                                </tr>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.ItemIndex + 1 %></td>

                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblWorkOrdNo" Text='<%# Eval("WorkOrderNo")%>'></asp:Label>
                                                                            </td>

                                                                            <td runat="server" visible="false">
                                                                                <asp:Label runat="server" ID="lblStkType" Text='<%# Eval("StockType")%>'></asp:Label>
                                                                            </td>

                                                                            <td runat="server" visible="false">
                                                                                <asp:Label runat="server" ID="lblGenMdlCode" Text='<%# Eval("GenModelCode")%>'></asp:Label>
                                                                            </td>

                                                                            <td  runat="server" visible="false">
                                                                                <asp:Label runat="server" ID="lblGenMdlName" Text='<%# Eval("GenModelName")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblGenPartName" Text='<%# Eval("GenModelPart")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrdStage" Text='<%# Eval("ProductionStage")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblSapNo" Text='<%# Eval("SapNo")%>'></asp:Label>
                                                                            </td>

                                                                            <td runat="server" visible="false">
                                                                                <asp:Label runat="server" ID="lblItemCode" Text='<%# Eval("ItemCode")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblItemName" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblUOMName" Text='<%# Eval("UOMName")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtReserveQty" class="form-control" Text='<%# Eval("ReservationQty")%>'
                                                                                   ReadOnly="true" Width="100px" BorderStyle="None" AutoPostBack="true" OnTextChanged="txtReserveQty_TextChanged"> 
                                                                                </asp:TextBox>
                                                                            </td>

                                                                            <td runat="server" visible="false">
                                                                                <asp:Label runat="server" ID="lblRate" Text='<%# Eval("Rate")%>'></asp:Label>
                                                                            </td>

                                                                            <td runat="server" visible="false">
                                                                                <asp:Label runat="server" ID="lblValue" Text='<%# Eval("Value")%>'></asp:Label>
                                                                            </td>

                                                                            <td>
                                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                    Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>'
                                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate></table></FooterTemplate>
                                                                </asp:Repeater>
                                                                <!-- /.table -->
                                                            </div>
                                                            <!-- /.mail-box-messages -->
                                                        </div>
                                                    </asp:Panel>


                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-10"></div>
                                        <div class="col-md-2">
                                            <label for="exampleInputName">Total Requisition Qty</label>
                                            <asp:Label ID="lblTotQty" runat="server" class="form-control"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field"
                                            OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />
                                    </div>
                                </div>
                                <!-- /.box-footer-->
                            </div>
                            <!-- /.box -->
                        </div>

                    </div>

                </section>
                <!-- /.content -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript" src="../assets/js/Trans_Receipt_Calc.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#tblReserBOMDet").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": true
            });
        });
    </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.select2').select2();
                    $("#tblReserBOMDet").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "ordering": false,
                        "searching": true
                    });
                }
            });
        };
    </script>

</asp:Content>

