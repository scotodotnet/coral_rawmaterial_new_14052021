﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GRN_Convertion_Cost.aspx.cs" Inherits="Transaction_GRN_Convertion_Cost" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
    </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <asp:UpdatePanel ID="upGenModelsub" runat="server">
        <ContentTemplate>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-sun-o text-primary"></i>GRN Conversion Costing Process
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3" runat="server">
                                <div class="form-group has-feedback">
                                    <label class="control-label">GRN No</label>
                                    <asp:DropDownList ID="txtGRN_No" runat="server" class="form-control select2"  OnSelectedIndexChanged="txtGRN_No_SelectedIndexChanged"
                                           AutoPostBack="true">
                                        </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group has-feedback">
                                    <label class="control-label">GRN Date</label>
                                    <asp:Label ID="txtGRN_Date" class="form-control" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Currency Type</label>
                                    <asp:Label ID="txtCurrency_Type" class="form-control" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Conversion Value (INR)</label>
                                    <asp:TextBox ID="txtConvertion_Value" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                  <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" ValidationGroup="Validate_Field" OnClick="btnSave_Click" runat="server" Text="Save" />
                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" OnClick="btnClear_Click" Text="Clear" />
                          
                        </div>
                  
                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                                        <HeaderTemplate>
                                                        <table  class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>S. No</th>
                                                                   <th>GRN No</th>
                                                                    <th>GRN Date</th>
                                                                    <th>Currency Type</th>
                                                                    <th>Conversion Value</th>
                                                                    <th>Model</th>
                                                                    </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                           
                                                            <td><%# Eval("GRNo")%></td>
                                                            <td><%# Eval("GRDate")%></td>
                                                            <td><%# Eval("CurrencyType")%></td>
                                                          <td><%# Eval("Conversion_Value")%></td>
                                                            <td>
                                                                  <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("GRNo")%>'>
                                                                    </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>                                
			                                    </asp:Repeater>
                </div>
            </div>
              <div class="box-footer">
                       
                    </div>
            <div class="col-md-3" hidden>
                <div class="callout callout-info" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-info"></i>Info:</h4>
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    <!-- /.content -->
        </div>

     <!-- iCheck -->
    <script src="assets/adminlte/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('.checkbox').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>

</asp:Content>

