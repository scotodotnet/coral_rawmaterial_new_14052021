﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Production_Planning_Sub.aspx.cs" Inherits="Trans_Production_Planning_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Production Planning</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Production Plan No</label>
                                                <asp:Label ID="lblTransNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Production Plan Date</label>
                                                <asp:TextBox ID="txtTransDate" runat="server" class="form-control datepicker" AutoComplete="off"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" runat="server" class="form-control" Style="resize: none"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Item Details</span></h3>
                                                    <div class="box-tools pull-right">
                                                        <div class="has-feedback">
                                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="clearfix"></div>

                                                <asp:Panel ID="pnlStock" runat="server">
                                                    <div class="row" runat="server" style="padding-top: 15px">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Generator Model</label>
                                                                <asp:DropDownList ID="ddlGenMdlNo" runat="server" class="form-control select2"
                                                                    AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlGenMdlNo_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2" runat="server" visible="false">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Part Type</label>
                                                                <asp:DropDownList ID="ddlPrdPartType" runat="server" class="form-control select2">
                                                                    
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2" runat="server" visible="false">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Productiong Starting Date</label>
                                                                <asp:TextBox ID="txtPrdStartDate" runat="server" class="form-control datepicker">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-2" runat="server" visible="false">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Productiong Ending Date</label>
                                                                <asp:TextBox ID="txtPrdEndDate" runat="server" class="form-control datepicker">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2" runat="server" style="padding-top: 25px" visible="false">
                                                            <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-primary" OnClick="btnAdd_Click" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlRpter" runat="server" Height="300px" Style="overflow-x: scroll; overflow-y: scroll">
                                                    <div class="box-body no-padding">
                                                        <div class="table-responsive mailbox-messages">
                                                            <asp:Repeater ID="rptGenPartType" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="tbltest" class="table table-hover table-striped table-bordered">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S_No</th>
                                                                                <th>Genreator_ModelName</th>
                                                                                <th>Production_PartName</th>
                                                                                <th>Production_Start_Date</th>
                                                                                <th>Production_End_Date</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Container.ItemIndex + 1 %></td>

                                                                         <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblGrdMdlode" Text='<%# Eval("GenModelCode")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblGrdMdlName" Text='<%# Eval("GenModelName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:Label runat="server" ID="lblGrdPratType" Text='<%# Eval("PartsName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:TextBox runat="server" ID="txtGrdStartDate" class="form-control datepicker" Text='<%# Eval("StartDate")%>'></asp:TextBox>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:TextBox runat="server" ID="txtGrdEndDate" class="form-control datepicker" Text='<%# Eval("EndDate")%>'></asp:TextBox>
                                                                        </td>

                                                                        <td>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("PartsName")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                            <!-- /.table -->
                                                        </div>
                                                        <!-- /.mail-box-messages -->
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <div class="form-group">
                                    <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field"
                                        OnClick="btnSave_Click" />
                                    <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                    $("#tbltest").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": true,
                        "ordering": false,
                        "searching": true
                        //"drawCallback":true
                    });
                }
            });
        };
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            console.log("Start");
            $("#tbltest").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": true,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });
        });
    </script>

</asp:Content>

