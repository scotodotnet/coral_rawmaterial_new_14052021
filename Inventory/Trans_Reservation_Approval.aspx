﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Reservation_Approval.aspx.cs" Inherits="Inventory_Trans_Reservation_Approval" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="ffa-check-square-o text-primary"></i><span>Material Requisition Approval</span></h3>
                                </div>

                                <div class="row" runat="server" style="padding-top: 25px; padding-bottom: 25px">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="control-label" for="Req_No">Purchase Order Status</label>
                                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                                OnSelectedIndexChanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">
                                            <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Trans No</th>
                                                                <th>Trans Date</th>
                                                                <th>Machine ID</th>
                                                                <th>Remarks</th>
                                                                <th>Approve</th>
                                                                <th>Cancel</th>
                                                                <th>View</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex+1%></td>
                                                        <td><%# Eval("TransNo")%></td>
                                                        <td><%# Eval("TransDate")%></td>
                                                        <td><%# Eval("RequisitionBy")%></td>
                                                        <td><%# Eval("Remarks")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square" runat="server"
                                                                Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Reservation details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="LinkButton1" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Reservation details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-book"
                                                                runat="server" Text="" OnCommand="GridEditPurRequestClick"
                                                                CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'>
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false" OnItemDataBound="Repeater_Approve_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>Trans No</th>
                                                                    <th>Trans Date</th>
                                                                    <th>Machine ID</th>
                                                                    <th>Remarks</th>
                                                                    <th>View</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1%></td>
                                                            <td><%# Eval("TransNo")%></td>
                                                            <td><%# Eval("TransDate")%></td>
                                                            <td><%# Eval("RequisitionBy")%></td>
                                                            <td><%# Eval("Remarks")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-book"
                                                                    runat="server" Text="" OnCommand="GridEditPurRequestClick"
                                                                    CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'>
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>Trans No</th>
                                                                    <th>Trans Date</th>
                                                                    <th>Machine ID</th>
                                                                    <th>Remarks</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1%></td>
                                                            <td><%# Eval("TransNo")%></td>
                                                            <td><%# Eval("TransDate")%></td>
                                                            <td><%# Eval("RequisitionBy")%></td>
                                                            <td><%# Eval("Remarks")%></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

