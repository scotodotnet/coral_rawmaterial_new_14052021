﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GateIn_CheckingSub.aspx.cs" Inherits="Inventory_Trans_GateIn_CheckingSub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <section class="content-header">
                    <h1><i class=" text-primary"></i>QC (Incoming)</h1>
                    <asp:Label runat="server" ID="lblErrorMsg" Style="color: red; font-size: x-large"></asp:Label>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Trans No</label>
                                                <asp:Label ID="lblTransNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Trans Date</label>
                                                <asp:TextBox ID="txtTransDate" runat="server" class="form-control pull-right datepicker" AutoComplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txttransDate" ValidationGroup="Validate_Field" class="form_error"
                                                    ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars"
                                                    FilterType="Custom,Numbers" TargetControlID="txttransDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">QC Person</label>
                                                <asp:DropDownList runat="server" ID="ddlQCPerson" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">RFI No</label>
                                                <asp:DropDownList runat="server" ID="ddlRFINo" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlRFINo_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">RFI Date</label>
                                                <asp:Label runat="server" ID="lblRFIDate" class="form-control">
                                                </asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Goods Receipt No</label>
                                                <asp:Label runat="server" ID="lblGRNo" class="form-control">
                                                </asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Goods Receipt Date</label>
                                                <asp:Label ID="lblGRDate" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Purchase Order No</label>
                                                <asp:Label runat="server" ID="lblPurRefNo" class="form-control">
                                                </asp:Label>
                                                <asp:HiddenField ID="hfStdPoNo" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Purchase Order Date</label>
                                                <asp:Label runat="server" ID="lblPurOrdDate" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">GateIn No</label>
                                                <asp:Label runat="server" ID="lblGateInNo" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">GateIn Date</label>
                                                <asp:Label runat="server" ID="lblGateInDate" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Name</label>
                                                <asp:Label ID="lblSuppName" runat="server" class="form-control"></asp:Label>
                                                <asp:HiddenField ID="hfSuppCode" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Currency Type</label>
                                                <asp:Label ID="lblCurType" runat="server" class="form-control pull-right"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Invoice No</label>
                                                <asp:Label ID="lblSuppInvNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Invoice Date</label>
                                                <asp:Label ID="lblSuppInvDate" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Account Type</label>
                                                <asp:Label ID="lblAccType" runat="server" class="form-control pull-right"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Material Type</label>
                                                <asp:Label ID="lblMatType" runat="server" class="form-control pull-right"></asp:Label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" Style="resize: none" runat="server" TextMode="MultiLine"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form">QC Documentation</label>
                                                <br />
                                                <asp:FileUpload ID="fupQCDoc" class="form-control" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server" id="divFileDownload" visible="false">
                                            <div class="form-group">
                                                <label class="form">Documentation Download</label>
                                                <br />
                                                <asp:LinkButton ID="lnkQCFileName" runat="server" OnClick="lnkQCFileName_Click"></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">

                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Item Details</span></h3>
                                                </div>

                                                <div class="row" runat="server" style="padding-top: 1%">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">SapNo</label>
                                                            <asp:DropDownList runat="server" ID="ddlSapNo" class="form-control select2"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSapNo_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">ItemName</label>
                                                            <asp:Label ID="lblItemName" runat="server" class="form-control"></asp:Label>
                                                            <asp:HiddenField ID="hfItemCode" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">UOM</label>
                                                            <asp:Label ID="lblUOM" runat="server" class="form-control"></asp:Label>
                                                            <asp:HiddenField ID="hfDeptCode" runat="server" />
                                                            <asp:HiddenField ID="hfDeptName" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Received Qty</label>
                                                            <asp:Label ID="lblReceivedQty" runat="server" class="form-control"></asp:Label>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Inspection Qty</label>
                                                            <asp:TextBox ID="txtInspectQty" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtInspectQty" ValidationGroup="Validate_Field" class="form_error"
                                                                ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                                                FilterType="Custom,Numbers" TargetControlID="txtInspectQty" ValidChars="0123456789./">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Rejection Qty</label>
                                                            <asp:Label ID="Label3" runat="server" class="form-control"></asp:Label>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="control-label">Have NRC?</label>
                                                            <br />
                                                            <div class="form-control">
                                                                <asp:RadioButtonList ID="rdbSerialNo" RepeatColumns="2" runat="server">
                                                                    <asp:ListItem Selected="True" Text="No" Value="No" style="padding: 20px"></asp:ListItem>
                                                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form">Document Upload</label>
                                                            <br />
                                                            <asp:FileUpload ID="fupDocu" class="form-control" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Reason</label>
                                                            <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Style="resize: none"
                                                                class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-3"  runat="server" style="padding-top: 1.75%">
                                                        <div class="form-group">
                                                            <asp:Button ID="btnAdd" class="btn btn-primary" runat="server" Text="Add" />
                                                        </div>
                                                    </div>

                                                </div>

                                                <asp:Panel ID="pnlItemDet" runat="server" Height="400px" Style="overflow-x: scroll; overflow-y: scroll">
                                                    <div class="box-body no-padding">
                                                        <div class="table-responsive mailbox-messages" runat="server">
                                                            <asp:Repeater ID="rptItemQCChecking" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="tblItemDet" class="table table-hover table-bordered table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S._No</th>
                                                                                <th>SapNo</th>
                                                                                <th runat="server" visible="false">ItemCode</th>
                                                                                <th>Item_Description</th>
                                                                                <th runat="server" visible="false">DeptCode</th>
                                                                                <th>UOM</th>
                                                                                <th>Received_Qty</th>
                                                                                <th>Inspection_Qty</th>
                                                                                <th>Rejected_Qty</th>
                                                                                <th>Reason</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <%# Container.ItemIndex + 1 %>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:Label ID="lblGrdSapNo" runat="server" Text='<%# Eval("SapNo")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblGrdItemCode" runat="server" Text='<%# Eval("ItemCode")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblGrdItemName" runat="server" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblGrdDeptCode" runat="server" Text='<%# Eval("DeptCode")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblGrdDeptName" runat="server" Text='<%# Eval("DeptName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:Label ID="lblGrdUOMName" runat="server" Text='<%# Eval("UOM")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:Label ID="lblGrdReceiveQty" runat="server" Text='<%# Eval("ReceiveQty")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:TextBox runat="server" ID="txtGrdInspectQty" CssClass="form-control" Style="width: 100%"
                                                                                Text='<%# Eval("InspectQty")%>' AutoPostBack="true" OnTextChanged="txtGrdInspectQty_TextChanged">
                                                                            </asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                                                                FilterType="Custom,Numbers" TargetControlID="txtGrdInspectQty" ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:Label ID="lblGrdRejectQty" runat="server" Text='<%# Eval("RejectQty")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblGrdRate" runat="server" Text='<%# Eval("Rate")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblGrdItemTotal" runat="server" Text='<%# Eval("ItemTotal")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblGrdDiscPre" runat="server" Text='<%# Eval("DiscPer")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblGrdDiscAmt" runat="server" Text='<%# Eval("DiscAmt")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblGrdNetAmt" runat="server" Text='<%# Eval("NetAmount")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:TextBox runat="server" ID="txtGrdRemarks" Text='<%# Eval("Remarks")%>' Class="form-control"
                                                                                AutoPostBack="true" OnTextChanged="txtGrdRemarks_TextChanged"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                            <!-- /.table -->
                                                        </div>
                                                        <!-- /.mail-box-messages -->
                                                    </div>
                                                </asp:Panel>

                                                <div class="row" runat="server" style="padding-top: 2%">

                                                    <div class="col-md-10"></div>

                                                    <div class="col-md-2">
                                                        <label for="exampleInputName">Total Qty</label>
                                                        <asp:Label ID="lblTotQty" runat="server" class="form-control"></asp:Label>
                                                    </div>

                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <label for="exampleInputName">Total Reject Qty</label>
                                                        <asp:Label ID="lblTotRejectQty" runat="server" class="form-control"></asp:Label>
                                                    </div>

                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <label for="exampleInputName">Total Item Amount</label>
                                                        <asp:Label ID="lblTotItemAmt" runat="server" class="form-control"></asp:Label>
                                                    </div>

                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <label for="exampleInputName">Total Discount Amount</label>
                                                        <asp:Label ID="lblTotDiscAmt" runat="server" class="form-control"></asp:Label>
                                                    </div>

                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <label for="exampleInputName">Total Item NetAmount</label>
                                                        <asp:Label ID="lblTotItemNetAmt" runat="server" class="form-control"></asp:Label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="box-footer no-border">
                                <div class="form-group">
                                    <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field"
                                        OnClick="btnSave_Click" />
                                    <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
                <asp:PostBackTrigger ControlID="lnkQCFileName" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!-- jQuery 3 -->
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="/assets/adminlte/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">

        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
                    $('.select2').select2();
                    $("#tblItemDet").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": true,
                        "ordering": false,
                        "searching": true,
                        "bDestroy": true
                        //"drawCallback":true
                    });
                }
            });
        };
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            console.log("Start");
            $("#tblItemDet").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": true,
                "ordering": false,
                "searching": true,
                "bDestroy": true
                //"drawCallback":true
            });
        });
    </script>
</asp:Content>
