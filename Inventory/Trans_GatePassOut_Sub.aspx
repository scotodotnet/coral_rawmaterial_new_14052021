﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GatePassOut_Sub.aspx.cs" Inherits="Trans_GatePassOut_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <section class="content-header">
                    <h1><i class=" text-primary"></i>New GatePass Out</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">GatePass Out No</label>
                                                <asp:Label ID="lblGPOutNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">GatePass Out Date</label>
                                                <asp:TextBox ID="txtGPOutDate" runat="server" class="form-control datepicker" AutoComplete="off"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Material Type</label>
                                                <asp:DropDownList ID="ddlMatType" runat="server" class="form-control select2"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">RawMaterial</asp:ListItem>
                                                    <asp:ListItem Value="2">Tools</asp:ListItem>
                                                    <asp:ListItem Value="3">Asset</asp:ListItem>
                                                    <asp:ListItem Value="4">General Items</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Stock Type</label>
                                                <asp:RadioButtonList ID="RdpStkType" class="form-control" runat="server" AutoPostBack="true"
                                                    RepeatColumns="2" OnSelectedIndexChanged="RdpStkType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1" Text="Stock" style="padding-right: 20px" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="No Stock"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Type</label>
                                                <asp:DropDownList runat="server" ID="ddlGPOutType" class="form-control select2">
                                                    <asp:ListItem Value="1">Returnable</asp:ListItem>
                                                    <asp:ListItem Value="2">Non Returnable</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Delivery Date</label>
                                                <asp:TextBox ID="txtDeliDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Invoice No</label>
                                                <asp:TextBox ID="txtSuppInvNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Invoice Date</label>
                                                <asp:TextBox ID="txtSuppInvDate" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Name</label>
                                                <asp:DropDownList ID="ddlSupplier" runat="server" class="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Details</label>
                                                <asp:TextBox ID="txtSuppDet" runat="server" TextMode="MultiLine" Style="resize: none"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Style="resize: none"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Item Details</span></h3>
                                                    <div class="box-tools pull-right">
                                                        <div class="has-feedback">
                                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="clearfix"></div>

                                                <asp:Panel ID="pnlStock" runat="server" Visible="true">
                                                    <div class="row" runat="server" style="padding-top: 15px">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">SapNo/RefNo</label>
                                                                <asp:DropDownList ID="ddlIdentiNo" runat="server" class="form-control select2"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlIdentiNo_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Item Name</label>
                                                                <asp:DropDownList ID="ddlItemName" runat="server" class="form-control select2" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>

                                                            <asp:HiddenField ID="hfDeptCode" runat="server" />
                                                            <asp:HiddenField ID="hfDeptName" runat="server" />
                                                            <asp:HiddenField ID="hfWarehouseCode" runat="server" />
                                                            <asp:HiddenField ID="hfWarehouseName" runat="server" />
                                                            <asp:HiddenField ID="hfRackNo" runat="server" />
                                                            <asp:HiddenField ID="hfUOMCode" runat="server" />
                                                            <asp:HiddenField ID="hfUOMName" runat="server" />

                                                        </div>

                                                        <div class="col-md-2" runat="server">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Stock Qty</label>
                                                                <asp:Label ID="lblStockQty" runat="server" class="form-control"></asp:Label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2" runat="server" visible="false">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Rate</label>
                                                                <asp:TextBox ID="txtRate" runat="server" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Out Qty</label>
                                                                <asp:TextBox ID="txtGPOutQty" runat="server" class="form-control" AutoPostBack="true" AutoComplete="off"
                                                                    OnTextChanged="txtGPOutQty_TextChanged"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2" runat="server" visible="false">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Value</label>
                                                                <asp:TextBox ID="txtValue" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3" runat="server">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Notes</label>
                                                                <asp:TextBox ID="txtItemNotes" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <asp:Button runat="server" ID="btnAddItem" class="btn btn-primary" Text="Add Items" OnClick="btnAddItem_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlNonStock" runat="server" Visible="false">
                                                    <div class="row" runat="server" style="padding-top: 15px">

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Item Name</label>
                                                                <asp:TextBox ID="txtNonStkItemName" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">UOM</label>
                                                                <asp:DropDownList ID="ddlNonStkUOM" runat="server" class="form-control select2"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Out Qty</label>
                                                                <asp:TextBox ID="txtNonOutQty" runat="server" class="form-control" AutoPostBack="true">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3" runat="server">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Notes</label>
                                                                <asp:TextBox ID="txtNonStkNotes" runat="server" TextMode="MultiLine" Style="resize: none" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2" runat="server" style="padding-top: 2%">
                                                            <div class="form-group">
                                                                <asp:Button runat="server" ID="btnNonStkAdd" class="btn btn-primary" Text="Add Items" OnClick="btnNonStkAdd_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlRpter" runat="server" Height="200px" Style="overflow-x: scroll; overflow-y: scroll">
                                                    <div class="box-body no-padding">
                                                        <div class="table-responsive mailbox-messages">
                                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="tbltest" class="table table-hover table-striped table-bordered">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S. No</th>
                                                                                <th>SAP_No</th>
                                                                                <th runat="server" visible="false">Item Code</th>
                                                                                <th>Item Name</th>
                                                                                <th runat="server" visible="false">DeptCode</th>
                                                                                <th runat="server" visible="false">DeptName</th>
                                                                                <th runat="server" visible="false">WareHouseCode</th>
                                                                                <th runat="server" visible="false">WareHouseName</th>
                                                                                <th runat="server" visible="false">RackNo</th>
                                                                                <th runat="server" visible="false">UOMCode</th>
                                                                                <th>UOM</th>
                                                                                <th>Out Qty</th>
                                                                                <th runat="server" visible="false">Value</th>
                                                                                <th>Remarks</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Container.ItemIndex + 1 %></td>

                                                                        <td>
                                                                            <asp:Label runat="server" ID="Label1" Text='<%# Eval("SapNo")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblItemCode" Text='<%# Eval("ItemCode")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblItemName" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblDeptCode" Text='<%# Eval("DeptCode")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblDeptName" Text='<%# Eval("DeptName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblWarehouseCode" Text='<%# Eval("WarehouseCode")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblWarehouseName" Text='<%# Eval("WarehouseName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblRackNo" Text='<%# Eval("RackName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblUOMCode" Text='<%# Eval("UOMCode")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblUOMName" Text='<%# Eval("UOMName")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtOutQty" Text='<%# Eval("OutQty")%>'
                                                                                Width="100px" BorderStyle="None" AutoPostBack="true" OnTextChanged="txtOutQty_TextChanged"> 
                                                                            </asp:TextBox>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblItemRem" Text='<%# Eval("ItemRem")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                            <!-- /.table -->
                                                        </div>
                                                        <!-- /.mail-box-messages -->
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>

                                    <div class="row" runat="server" visible="false">
                                        <div class="col-md-8"></div>
                                        <div class="col-md-2">
                                            <label for="exampleInputName">Total Qty</label>
                                            <asp:TextBox ID="txtTotQty" runat="server" class="form-control"></asp:TextBox>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="false">
                                            <label for="exampleInputName">Round Off</label>
                                            <asp:TextBox ID="txtRoundOff" runat="server" class="form-control"></asp:TextBox>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="false">
                                            <label for="exampleInputName">Total Amount</label>
                                            <asp:TextBox ID="txtTotAmt" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field"
                                            OnClick="btnSave_Click" />

                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />


                                        <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    </div>
                                </div>
                                <!-- /.box-footer-->
                            </div>
                            <!-- /.box -->
                        </div>

                    </div>

                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                    $("#tbltest").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": true,
                        "ordering": false,
                        "searching": true
                        //"drawCallback":true
                    });
                }
            });
        };
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            console.log("Start");
            $("#tbltest").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": true,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });
        });
    </script>

</asp:Content>

