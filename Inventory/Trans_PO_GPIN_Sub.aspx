﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_PO_GPIN_Sub.aspx.cs" Inherits="Trans_PO_GPIN_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Purchase Order GatePass In</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">GatePassIn No</label>
                                                <asp:Label ID="txtGPINNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">GatePassIn Date</label>
                                                <asp:TextBox ID="txtGPINDate" runat="server" class="form-control pull-right datepicker" AutoComplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtGPINDate" ValidationGroup="Validate_Field" class="form_error"
                                                    ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars"
                                                    FilterType="Custom,Numbers" TargetControlID="txtGPINDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Transport Name</label>
                                                <asp:DropDownList runat="server" ID="ddlTransName" class="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlTransName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfTransportCode" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Vehicle No</label>
                                                <asp:TextBox ID="txtVehiNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server" visible="true">
                                            <div class="form-group">
                                                <label for="exampleInputName">Driver Name</label>
                                                <asp:TextBox ID="txtDriverName" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-2" runat="server" visible="true">
                                            <div class="form-group">
                                                <label for="exampleInputName">LR No.</label>
                                                <asp:TextBox ID="txtLRno" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="true">
                                            <div class="form-group">
                                                <label for="exampleInputName">DC No.</label>
                                                <asp:TextBox ID="txtDCNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="true">
                                            <div class="form-group">
                                                <label for="exampleInputName">DC Date</label>
                                                <asp:TextBox ID="txtDCDate" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="true">
                                            <div class="form-group">
                                                <label for="exampleInputName">E-Way Bill No.</label>
                                                <asp:TextBox ID="txtEwayBillNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Name</label>
                                                <asp:DropDownList ID="ddlSuppName" runat="server" class="form-control select2"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSuppName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdSuppCode" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Invoice No.</label>
                                                <asp:TextBox ID="txtSuppInvNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Invoice Date</label>
                                                <asp:TextBox ID="txtSuppInvDate" runat="server" class="form-control datepicker" AutoComplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtSuppInvDate" ValidationGroup="Validate_Field" class="form_error"
                                                    ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                                    FilterType="Custom,Numbers" TargetControlID="txtSuppInvDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" runat="server" Style="resize: none" TextMode="MultiLine" class="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="true">
                                            <div class="form-group">
                                                <label class="control-label">Photo</label>
                                                <br />
                                                <img src="/assets/images/NoImage.png" style="width: 110px; height: 60px; cursor: pointer;" class="img-thumbnail" id="MaterialImg" />
                                                <input style="visibility: hidden" type="file" name="Img" class="form-control" onchange='sendFile(this);' id="f_UploadImage" />
                                                <asp:HiddenField ID="hidd_imgPath" runat="server" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Item Details</span></h3>
                                                </div>

                                                <div class="row" runat="server" style="padding-top: 1%">
                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Account Type</label>
                                                            <asp:DropDownList runat="server" ID="ddlAccType" class="form-control select2"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlAccType_SelectedIndexChanged">
                                                                <asp:ListItem Value="1">Enercon</asp:ListItem>
                                                                <asp:ListItem Value="2">Escrow</asp:ListItem>
                                                                <asp:ListItem Value="3">Coral</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Material Type</label>
                                                            <asp:DropDownList runat="server" ID="ddlMatType" class="form-control select2"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                                <asp:ListItem Value="1">RawMaterial</asp:ListItem>
                                                                <asp:ListItem Value="2">Tools</asp:ListItem>
                                                                <asp:ListItem Value="3">Assets</asp:ListItem>
                                                                <asp:ListItem Value="4">General Item</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Purchase Order No</label>
                                                            <asp:DropDownList runat="server" ID="ddlOrdNo" class="form-control select2"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlOrdNo_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2" runat="server" style="padding-top: 25px">
                                                        <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-primary" OnClick="btnAdd_Click" />
                                                    </div>
                                                </div>

                                                <div class="box-body no-padding">
                                                    <div class="table-responsive mailbox-messages" runat="server" style="padding-top: 15px">
                                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="tbltest" class="table table-hover table-bordered table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S. No</th>
                                                                            <th>Order_No</th>
                                                                            <th>Order_Date</th>
                                                                            <th>Reference_No</th>
                                                                            <th>Request_No</th>
                                                                            <th>Request_Date</th>
                                                                            <th>Department</th>
                                                                            <th>Account_Type</th>
                                                                            <th>Material_Type</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <%# Container.ItemIndex + 1 %>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblOrdNo" runat="server" Text='<%# Eval("OrderNo")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblOrdDate" runat="server" Text='<%# Eval("OrderDate")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblPORefNo" runat="server" Text='<%# Eval("RefNo")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblRquNo" runat="server" Text='<%# Eval("RequestNo")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblRquDate" runat="server" Text='<%# Eval("RequestDate")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblDeptName" runat="server" Text='<%# Eval("DeptName")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblAccType" runat="server" Text='<%# Eval("AccountType")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblMatType" runat="server" Text='<%# Eval("MaterialType")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("OrderNo")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                        <!-- /.table -->
                                                    </div>
                                                    <!-- /.mail-box-messages -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" runat="server">
                                        <div class="col-md-8"></div>
                                        <div class="col-md-2">
                                            <label for="exampleInputName">Total Qty</label>
                                            <asp:TextBox ID="txtTotQty" runat="server" class="form-control"></asp:TextBox>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="false">
                                            <label for="exampleInputName">Round Off</label>
                                            <asp:TextBox ID="txtRoundOff" runat="server" class="form-control" AutoPostBack="true">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label for="exampleInputName">Total Amount</label>
                                            <asp:TextBox ID="txtTotAmt" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save"
                                            ValidationGroup="Validate_Field" OnClick="btnSave_Click" />

                                        <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />
                                    </div>
                                </div>
                                <!-- /.box-footer-->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript" src="../assets/js/Trans_Receipt_Calc.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', Autoclose: true });
                    $('.select2').select2();

                    function sendFile(file) {

                        var formData = new FormData();
                        formData.append('file', $('#f_UploadImage')[0].files[0]);
                        // alert(formData);
                        formData.append('filename', $('[id*=txtMatNo]').val() + '_' + $('[id*=txtMatName]').val());
                        formData.append("type", "PhotoBOM");
                        $.ajax({
                            type: 'post',
                            url: '/Handler.ashx',
                            data: formData,
                            xhr: function () {  // Custom XMLHttpRequest
                                var myXhr = $.ajaxSettings.xhr();
                                if (myXhr.upload) { // Check if upload property exists
                                    // update progressbar percent complete
                                    // $('#fileProgress').html('0%');
                                    $("progress").show();
                                    // For handling the progress of the upload
                                    // myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                                    myXhr.upload.addEventListener("progress", function (e) {
                                        if (e.lengthComputable) {
                                            $("#fileProgress").attr({
                                                value: e.loaded,
                                                max: e.total
                                            });
                                        }
                                    }, false);
                                }
                                return myXhr;
                            },
                            success: function (status) {
                                if (status != 'error') {
                                    $("#fileProgress").hide();
                                    var my_path = "/assets/uploads/CORAL/Photo/BOM/" + status;
                                    $('[id*=hidd_imgPath]').val(my_path);
                                    $("#myUploadedImg").attr("src", my_path);
                                }
                            },
                            processData: false,
                            contentType: false,
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert("Whoops something went wrong!" + errorThrown);
                            }
                        });
                    }
                    $(function () {

                        var fileupload = $("#f_UploadImage");
                        var filePath = $("#ImgPath_str");
                        var image = $("#MaterialImg");
                        var fReader = new FileReader();
                        image.click(function () {
                            fileupload.click();
                        });
                        fileupload.change(function () {
                            var fileExtension = ['jpeg', 'jpg'];
                            var fileName = $(this).val();
                            // filePath.html(fileName);
                            var input = document.getElementById("f_UploadImage");
                            var extension = fileName.replace(/^.*\./, '');
                            if ($.inArray(extension, fileExtension) == -1) {
                                swal("Please select only .jpg or .jpeg files.");
                                return false;
                            } else {
                                fReader.readAsDataURL(input.files[0]);
                                fReader.onloadend = function (event) {
                                    var img = document.getElementById("MaterialImg");
                                    img.src = event.target.result;
                                    if (img.src == null) {
                                        $("#SelectPhoto").val(0);
                                    } else {
                                        $("#SelectPhoto").val(1);
                                    }
                                }
                            }
                        });
                    });

                }
            });
        };
    </script>

    <script type="text/javascript">
        function sendFile(file) {

            var formData = new FormData();
            formData.append('file', $('#f_UploadImage')[0].files[0]);
            // alert(formData);
            formData.append('filename', $('[id*=txtGPINNo]').val());
            formData.append("type", "PhotoPOGIN");
            $.ajax({
                type: 'post',
                url: '/Handler.ashx',
                data: formData,
                xhr: function () {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) { // Check if upload property exists
                        // update progressbar percent complete
                        // $('#fileProgress').html('0%');
                        $("progress").show();
                        // For handling the progress of the upload
                        // myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                        myXhr.upload.addEventListener("progress", function (e) {
                            if (e.lengthComputable) {
                                $("#fileProgress").attr({
                                    value: e.loaded,
                                    max: e.total
                                });
                            }
                        }, false);
                    }
                    return myXhr;
                },
                success: function (status) {
                    if (status != 'error') {
                        $("#fileProgress").hide();
                        var my_path = "/assets/uploads/CORAL/Photo/POGIN/" + status;
                        $('[id*=hidd_imgPath]').val(my_path);
                        $("#myUploadedImg").attr("src", my_path);
                    }
                },
                processData: false,
                contentType: false,
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Whoops something went wrong!" + errorThrown);
                }
            });
        }
        $(function () {

            var fileupload = $("#f_UploadImage");
            var filePath = $("#ImgPath_str");
            var image = $("#MaterialImg");
            var fReader = new FileReader();
            image.click(function () {
                fileupload.click();
            });
            fileupload.change(function () {
                var fileExtension = ['jpeg', 'jpg'];
                var fileName = $(this).val();
                // filePath.html(fileName);
                var input = document.getElementById("f_UploadImage");
                var extension = fileName.replace(/^.*\./, '');
                if ($.inArray(extension, fileExtension) == -1) {
                    swal("Please select only .jpg or .jpeg files.");
                    return false;
                } else {
                    fReader.readAsDataURL(input.files[0]);
                    fReader.onloadend = function (event) {
                        var img = document.getElementById("MaterialImg");
                        img.src = event.target.result;
                        if (img.src == null) {
                            $("#SelectPhoto").val(0);
                        } else {
                            $("#SelectPhoto").val(1);
                        }
                    }
                }
            });
        });
    </script>

</asp:Content>

