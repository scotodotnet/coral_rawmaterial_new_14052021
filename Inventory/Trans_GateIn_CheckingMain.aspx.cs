﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;
using System.Drawing;

public partial class Inventory_Trans_GateIn_CheckingMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGPIN_No;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal INQty;

    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: RFI QC";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_OLD_data();
    }
    private void Load_OLD_data()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select TransNo,TransDate,GRNo,GRDate,SuppName,QCPerson,";
        SSQL = SSQL + " Case when Approval_Status='0' then 'Approval Pending' when Approval_Status!='0' then 'Approval Success' end App_Status";
        SSQL = SSQL + " From Trans_GIChecking_Main where Ccode = '" + SessionCcode + "' and Lcode = '" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' and Status!='Delete'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "1", "RFI QC");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('You do not have Rights to Add..');", true);
        }
        else
        {

            Session.Remove("QC_No");
            Response.Redirect("Trans_GateIn_CheckingSub.aspx");
        }
    }
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "1", "RFI QC");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit..');", true);
        }

        if (!ErrFlag)
        {

            string RFIQCNo = e.CommandName.ToString();

            Session["QC_No"] = RFIQCNo;
            Response.Redirect("Trans_GateIn_CheckingSub.aspx");

        }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "1", "RFI QC");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete..');", true);
        }

        RepeaterItem itm = (sender as LinkButton).NamingContainer as RepeaterItem;
        Label lblStatus = itm.FindControl("lblStatus") as Label;
        string Status = lblStatus.Text;
        if (Status == "Approval Success")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Transaction is already get Approved');", true);
        }

        if (!ErrFlag)
        {

            DataTable DT = new DataTable();
            SSQL = "Select * from Trans_GIChecking_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And TransNo='" + e.CommandName.ToString() + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                SSQL = "Update Trans_GIChecking_Main Set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And TransNo='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                //Delete Main Sub Table
                SSQL = "Update Trans_GIChecking_Sub Set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And TransNo='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                Load_OLD_data();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Trans Stock Out Details Deleted Successfully');", true);

            }


        }
    }
    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblStatus = Item.FindControl("lblStatus") as Label;

            if (lblStatus.Text == "Approval Success")
            {
                lblStatus.BackColor = Color.Green;
                lblStatus.ForeColor = Color.White;

            }
            else
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.ForeColor = Color.White;
            }
        }
    }
    protected void Repeater1_ItemCommand(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Inv")
        {
            string Fname = e.CommandArgument.ToString();
            string path = MapPath("~/assets/uploads/CORAL/Document/Pre_Inv/" + Fname);
            byte[] vs = System.IO.File.ReadAllBytes(path);
            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Type", "Application/octet-stream");
            Response.AddHeader("Content-Length", vs.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Fname);
            Response.BinaryWrite(vs);
            Response.Flush();
        }
        else if (e.CommandName == "Test")
        {
            string Fname = e.CommandArgument.ToString();
            string path = MapPath("~/assets/uploads/CORAL/Document/Pre_Test_Report/" + Fname);
            byte[] vs = System.IO.File.ReadAllBytes(path);
            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Type", "Application/octet-stream");
            Response.AddHeader("Content-Length", vs.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Fname);
            Response.BinaryWrite(vs);
            Response.Flush();
        }
        else if (e.CommandName == "QCTest")
        {
            string Fname = e.CommandArgument.ToString();
            string path = MapPath("~/assets/uploads/CORAL/Document/QC_Test/" + Fname);
            byte[] vs = System.IO.File.ReadAllBytes(path);
            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Type", "Application/octet-stream");
            Response.AddHeader("Content-Length", vs.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Fname);
            Response.BinaryWrite(vs);
            Response.Flush();
        }
    }
    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        string RptName = "";
        string Enquiry_No_Str = e.CommandName.ToString();
        RptName = "GateIn Check Report";
        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Transaction_No");
        Session.Remove("TransNo");
        Session["TransNo"] = Enquiry_No_Str;
        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?TransNo=" + Enquiry_No_Str + "&RptName=" + RptName, "_blank", "");
    }
}