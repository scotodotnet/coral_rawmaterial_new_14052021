﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GateInCheckApproval.aspx.cs" Inherits="Transaction_Trans_GateInCheckApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="ffa-check-square-o text-primary"></i><span>RFI QC Approval</span></h3>
                                </div>

                                <div class="row" runat="server" style="padding-top: 25px; padding-bottom: 25px">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="control-label" for="Req_No">Purchase Order Status</label>
                                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                                OnSelectedIndexChanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">
                                            <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false" OnItemCommand="Repeater1_ItemCommand">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>SL. No</th>
                                                                <th>TransNo</th>
                                                                <th>Good_Receipt_No</th>
                                                                <th>Good_Receipt_Date</th>
                                                                <th>Supplier</th>
                                                                <th>Supplier_Inv_No</th>
                                                                <th>Supplier_Inv_Date</th>
                                                                <th>Approve</th>
                                                                <th>Cancel</th>
                                                                <th>View</th>

                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex+1 %></td>
                                                        <td><%# Eval("TransNo")%></td>
                                                        <td><%# Eval("GRNo")%></td>
                                                        <td><%# Eval("GRDate")%></td>
                                                        <td><%# Eval("SuppName")%></td>
                                                        <td><%# Eval("SuppInvNo")%></td>
                                                        <td><%# Eval("SuppInvDate")%></td>
                                                       <%-- <td>
                                                            <asp:LinkButton runat="server" CommandArgument='<%# Eval("QCTest")%>' CommandName="QCTest"><%# Eval("QCTest")%></asp:LinkButton></td>--%>
                                                        <td>
                                                            <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square" runat="server"
                                                                Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this GateINCheck details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="LinkButton1" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this GateINCheck details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-book"
                                                                runat="server" Text="" OnCommand="GridEditPurRequestClick"
                                                                CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'>
                                                            </asp:LinkButton>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false" OnItemDataBound="Repeater_Approve_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>SL.No</th>
                                                                    <th>TransNo</th>
                                                                    <th>Good_Receipt_No</th>
                                                                    <th>Good_Receipt_Date</th>
                                                                    <th>Supplier</th>
                                                                    <th>Supplier_Inv_No</th>
                                                                    <th>Supplier_Inv_Date</th>
                                                                    
                                                                    <th>View</th>

                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1 %></td>
                                                            <td><%# Eval("TransNo")%></td>
                                                            <td><%# Eval("PONo")%></td>
                                                            <td><%# Eval("PODate")%></td>
                                                            <td><%# Eval("SuppName")%></td>
                                                            <%--<td><%# Eval("MaterialType")%></td>--%>
                                                            <td>
                                                                <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-file-text"
                                                                    runat="server" Text="" OnCommand="GridEditPurRequestClick"
                                                                    CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'>
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="btnMRR_View" class="btn btn-primary btn-sm fa fa-file-pdf-o"
                                                                    runat="server" Text="" OnCommand="btnMRR_View_Command"
                                                                    CommandArgument='<%# Eval("TransNo")%>' CommandName='<%# Eval("TransNo")%>'>
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sl. No</th>
                                                                    <th>TransNo</th>
                                                                    <th>Good_Receipt_No</th>
                                                                    <th>Good_Receipt_Date</th>
                                                                    <th>Supplier</th>
                                                                    <th>Supplier_Inv_No</th>
                                                                    <th>Supplier_Inv_Date</th>
                                                                    

                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1 %></td>
                                                            <td><%# Eval("TransNo")%></td>
                                                            <td><%# Eval("PONo")%></td>
                                                            <td><%# Eval("PODate")%></td>
                                                            <td><%# Eval("SuppName")%></td>
                                                            <%--<td><%# Eval("MaterialType")%></td>--%>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>


