﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Net;
using System.Drawing;
public partial class Inventory_Trans_GateIn_CheckingSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGPIN_No;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static Decimal INQty;

    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;
    bool ErrFlag = false;
    string Str_RFI_QC_Final;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //lblErrorMsg.Text = "";

            if (Session["UserId"] == null)
            {
                Response.Redirect("../Default.aspx");
                Response.Write("Your session expired");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionFinYearCode = Session["FinYearCode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();

            if (!IsPostBack)
            {
                Page.Title = "CORAL ERP :: RFI QC";

                Load_Data_GoodsReceipt_No();

                Load_QCPerson();

                Initial_Data_Referesh();

                if (Session["QC_No"] == null)
                {
                    SessionGPIN_No = "";

                    txtTransDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy");

                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "RFI QC", SessionFinYearVal, "1");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        lblTransNo.Text = Auto_Transaction_No;
                    }
                }
                else
                {
                    SessionGPIN_No = Session["QC_No"].ToString();
                    lblTransNo.Text = SessionGPIN_No;
                    btnSearch_Click(sender, e);
                }
            }
            Load_OLD_data();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    private void Initial_Data_Referesh()
    {
        try
        {
            lblErrorMsg.Text = "";

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("SapNo", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
            dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
            dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
            dt.Columns.Add(new DataColumn("UOM", typeof(string)));
            dt.Columns.Add(new DataColumn("ReceiveQty", typeof(string)));
            dt.Columns.Add(new DataColumn("InspectQty", typeof(string)));
            dt.Columns.Add(new DataColumn("RejectQty", typeof(string)));
            dt.Columns.Add(new DataColumn("Rate", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
            dt.Columns.Add(new DataColumn("DiscPer", typeof(string)));
            dt.Columns.Add(new DataColumn("DiscAmt", typeof(string)));
            dt.Columns.Add(new DataColumn("NetAmount", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

            rptItemQCChecking.DataSource = dt;
            rptItemQCChecking.DataBind();
            ViewState["ItemTable"] = rptItemQCChecking.DataSource;
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        rptItemQCChecking.DataSource = dt;
        rptItemQCChecking.DataBind();
    }

    private void Load_QCPerson()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select FirstName as EmployeeName from CORAL_ERP_HR..Employee_Mst where CompCode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LocCode='" + SessionLcode + "' And isactive!='No' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlQCPerson.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["EmployeeName"] = "-Select-";
        dr["EmployeeName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlQCPerson.DataTextField = "EmployeeName";
        ddlQCPerson.DataValueField = "EmployeeName";
        ddlQCPerson.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT_Check = new DataTable();
            string SaveMode = "Insert";
            bool ErrFlag = false;
            string fpath = "";
            string fname = "";
            string FilePath = "";

            GetIPAndName getIPAndName = new GetIPAndName();
            string SysIP = getIPAndName.GetIP();
            string SysName = getIPAndName.GetName();

            if (fupQCDoc.HasFile)
            {
                fname = fupQCDoc.FileName.ToString();
                fpath = "/assets/uploads/CORAL/Document/QC_Test/";
                fupQCDoc.SaveAs(Server.MapPath(fpath + fname));
                FilePath = fpath + fname;
            }

            if (!ErrFlag)
            {
                if (btnSave.Text == "Save")
                {
                    SSQL = " Insert Into Trans_GIChecking_Main (Ccode,Lcode,FinYearCode,FinYearVal,TransNo,TransDate,GRNo,GRDate,GateINNo,";
                    SSQL = SSQL + " GateINDate,PurOrdNo,PurOrdDate,PurRefNo,SuppCode,SuppName,SuppInvNo,SuppInvDate,AccountType,MaterialType,";
                    SSQL = SSQL + " QCPersonCode,QCPerson,Remarks,Approval_Status,QCDocPath,Status,UserID,UserName,CreateOn,SysIP,SysName,";
                    SSQL = SSQL + " TotalQty,TotalRejectQty,TotalItemAmt,TotalDiscAmt,TotalNetAmt,CurrencyType) ";
                    SSQL = SSQL + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + lblTransNo.Text + "','" + txtTransDate.Text + "','" + lblGRNo.Text + "',";
                    SSQL = SSQL + " '" + lblGRDate.Text + "','" + lblGateInNo.Text + "','" + lblGateInDate.Text + "',";
                    SSQL = SSQL + " '" + hfStdPoNo.Value + "','" + lblPurOrdDate.Text + "','" + lblPurRefNo.Text + "', ";
                    SSQL = SSQL + " '" + hfSuppCode.Value + "','" + lblSuppName.Text + "','" + lblSuppInvNo.Text + "','" + lblSuppInvDate.Text + "',";
                    SSQL = SSQL + " '" + lblAccType.Text + "',";
                    SSQL = SSQL + " '" + lblMatType.Text + "','" + ddlQCPerson.SelectedValue + "','" + ddlQCPerson.SelectedItem.Text + "',";
                    SSQL = SSQL + " '" + txtRemarks.Text + "','0','" + FilePath + "','Add','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " Convert(Datetime,GetDate(),103),'" + SysIP + "','" + SysName + "','" + lblTotQty.Text + "',";
                    SSQL = SSQL + " '" + lblTotRejectQty.Text + "','" + lblTotItemAmt.Text + "','" + lblTotDiscAmt.Text + "',";
                    SSQL = SSQL + " '" + lblTotItemNetAmt.Text + "','" + lblCurType.Text + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    DataTable dt = new DataTable();
                    dt = (DataTable)ViewState["ItemTable"];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SSQL = "Insert Into Trans_GIChecking_Sub(Ccode,Lcode,FinYearCode,FinYearVal,TransNo,TransDate,PurOrdNo,PurOrdDate,PurRefNo,";
                        SSQL = SSQL + " SapNo,ItemCode,ItemName,DeptCode,DeptName,UOM,ReceiveQty,InspectQty,RejectQty,Rate,ItemTotal,";
                        SSQL = SSQL + " DiscPer,DiscAmt,NetAmount,Remarks,Status,UserID,UserName,";
                        SSQL = SSQL + " CreateOn,GRNo,GRDate) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                        SSQL = SSQL + " '" + SessionFinYearVal + "','" + lblTransNo.Text + "','" + txtTransDate.Text + "','" + hfStdPoNo.Value + "',";
                        SSQL = SSQL + " '" + lblPurOrdDate.Text + "','" + lblPurRefNo.Text+ "','" + dt.Rows[i]["SapNo"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["UOM"].ToString() + "','" + dt.Rows[i]["ReceiveQty"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["InspectQty"].ToString() + "','" + dt.Rows[i]["RejectQty"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["ItemTotal"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["DiscPer"].ToString() + "','" + dt.Rows[i]["DiscAmt"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["NetAmount"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["Remarks"].ToString() + "','Add','" + SessionUserID + "','" + SessionUserName + "',";
                        SSQL = SSQL + " Convert(Datetime,GetDate(),103),'" + ddlRFINo.SelectedValue + "','" + lblGRDate.Text + "')";

                        objdata.RptEmployeeMultipleDetails(SSQL);

                    }
                }
                else
                {
                    SSQL = " Update Trans_GIChecking_Main set TransDate='" + txtTransDate.Text + "',GRNo='" + ddlRFINo.SelectedValue + "',";
                    SSQL = SSQL + " GRDate='" + lblGRDate.Text + "',GateINNo='" + lblGateInNo.Text + "',GateINDate='" + lblGateInDate.Text + "',";
                    SSQL = SSQL + " PurOrdNo='" + hfStdPoNo.Value + "',PurOrdDate='" + lblPurOrdDate.Text + "',";
                    SSQL = SSQL + " PurRefNo ='" + lblPurRefNo.Text + "',SuppCode='" + hfSuppCode.Value + "',";
                    SSQL = SSQL + " SuppName ='" + lblSuppName.Text + "',";
                    SSQL = SSQL + " SuppInvNo='" + lblSuppInvNo.Text + "',SuppInvDate='" + lblSuppInvDate.Text + "',";
                    SSQL = SSQL + " AccountType ='" + lblAccType.Text + "',MaterialType='" + lblMatType.Text + "',";
                    SSQL = SSQL + " QCPersonCode='" + ddlQCPerson.SelectedValue + "',QCPerson='" + ddlQCPerson.SelectedItem + "',";
                    SSQL = SSQL + " Remarks='" + txtRemarks.Text + "',QCDocPath='" + FilePath + "',ModifyBy='" + SessionUserName + "',";
                    SSQL = SSQL + " ModifyOn=Convert(Datetime,GetDate(),103),TotalQty='" + lblTotQty.Text + "',";
                    SSQL = SSQL + " TotalRejectQty ='" + lblTotRejectQty.Text + "',TotalItemAmt='" + lblTotItemAmt.Text + "',";
                    SSQL = SSQL + " TotalDiscAmt ='" + lblTotDiscAmt.Text + "',TotalNetAmt='" + lblTotItemNetAmt.Text + "' ";
                    SSQL = SSQL + " Where TransNo='" + lblTransNo.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    DataTable dt = new DataTable();
                    dt = (DataTable)ViewState["ItemTable"];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SSQL = "Update Trans_GIChecking_Sub set TransDate='" + txtTransDate.Text + "',PurOrdNo='" + hfStdPoNo.Value + "',";
                        SSQL = SSQL + " PurOrdDate ='" + lblPurOrdDate.Text + "',PurRefNo='" + lblPurRefNo.Text + "',";
                        SSQL = SSQL + " GRNo='" + ddlRFINo.SelectedValue + "',GRDate='" + lblGRDate.Text + "',";
                        SSQL = SSQL + " SapNo='" + dt.Rows[i]["SapNo"] + "',ItemCode='" + dt.Rows[i]["ItemCode"] + "',";
                        SSQL = SSQL + " ItemName ='" + dt.Rows[i]["ItemName"] + "',DeptCode='" + dt.Rows[i]["DeptCode"] + "',";
                        SSQL = SSQL + " DeptName='" + dt.Rows[i]["DeptName"] + "',UOM='" + dt.Rows[i]["UOM"] + "',";
                        SSQL = SSQL + " ReceiveQty='" + dt.Rows[i]["ReceiveQty"] + "',InspectQty='" + dt.Rows[i]["InspectQty"] + "',";
                        SSQL = SSQL + " RejectQty='" + dt.Rows[i]["RejectQty"] + "',Rate='" + dt.Rows[i]["Rate"] + "',";
                        SSQL = SSQL + " ItemTotal='" + dt.Rows[i]["ItemTotal"] + "',DiscPer='" + dt.Rows[i]["DiscPer"] + "',";
                        SSQL = SSQL + " DiscAmt='" + dt.Rows[i]["DiscAmt"] + "',NetAmount='" + dt.Rows[i]["NetAmount"] + "',";
                        SSQL = SSQL + " Remarks='" + dt.Rows[i]["Remarks"] + "' ";
                        SSQL = SSQL + " Where TransNo='" + lblTransNo.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And ItemCode='" + dt.Rows[i]["ItemCode"].ToString() + "' ";


                        objdata.RptEmployeeMultipleDetails(SSQL);

                    }
                }
            }
            if (btnSave.Text == "Save")
            {
                if (!ErrFlag)
                {
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "RFI QC", SessionFinYearVal, "1");
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        lblTransNo.Text = Auto_Transaction_No;
                    }
                }
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('RFI Inspection Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('RFI Inspection Updated Successfully');", true);
            }
            Session["T_No"] = lblTransNo.Text;
            Clear_All_Field();
            btnSave.Text = "Save";
            btnBack_Click(sender, e);
            //Response.Redirect("Trans_GateIn_CheckingMain.aspx");


        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        lblTransNo.Text = ""; txtTransDate.Text = ""; lblGateInNo.Text = "";
        ddlQCPerson.SelectedValue = "-Select-";
        ddlRFINo.SelectedIndex = 0;
        lblGRDate.Text = "";
        lblSuppName.Text = "";
        txtRemarks.Text = "";

        btnSave.Text = "Save";
        Session.Remove("T_No");
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }

        Load_OLD_data();
        Grid_Total_Value();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        DataTable Sub_DT = new DataTable();

        SSQL = "Select * from Trans_GIChecking_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And TransNo ='" + lblTransNo.Text + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            lblTransNo.Text = Main_DT.Rows[0]["TransNo"].ToString();
            txtTransDate.Text = Main_DT.Rows[0]["TransDate"].ToString();

            ddlRFINo.SelectedValue = Main_DT.Rows[0]["GRNo"].ToString();
            lblGRDate.Text = Main_DT.Rows[0]["GRDate"].ToString();
            lblSuppName.Text = Main_DT.Rows[0]["SuppName"].ToString();
            hfSuppCode.Value = Main_DT.Rows[0]["SuppCode"].ToString();
            lblSuppInvNo.Text = Main_DT.Rows[0]["SuppInvNo"].ToString();
            lblSuppInvDate.Text = Main_DT.Rows[0]["SuppInvDate"].ToString();

            lblAccType.Text = Main_DT.Rows[0]["AccountType"].ToString();
            lblMatType.Text = Main_DT.Rows[0]["MaterialType"].ToString();
            lblCurType.Text = Main_DT.Rows[0]["CurrencyType"].ToString();
            lblGateInNo.Text = Main_DT.Rows[0]["GateINNo"].ToString();
            lblGateInDate.Text = Main_DT.Rows[0]["GateINDate"].ToString();
            ddlQCPerson.SelectedValue = Main_DT.Rows[0]["QCPerson"].ToString();

            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            lblTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();
            lblTotRejectQty.Text = Main_DT.Rows[0]["TotalRejectQty"].ToString();
            lblTotItemAmt.Text = Main_DT.Rows[0]["TotalItemAmt"].ToString();
            lblTotDiscAmt.Text = Main_DT.Rows[0]["TotalDiscAmt"].ToString();
            lblTotItemNetAmt.Text = Main_DT.Rows[0]["TotalNetAmt"].ToString();

            if (Main_DT.Rows[0]["QCDocPath"].ToString() != "")
            {
                divFileDownload.Visible = true;
                lnkQCFileName.Text = Main_DT.Rows[0]["QCDocPath"].ToString();
            }

            ddlRFINo_SelectedIndexChanged(sender, e);

            lblPurRefNo.Text = Main_DT.Rows[0]["PurRefNo"].ToString();
            hfStdPoNo.Value = Main_DT.Rows[0]["PurOrdNo"].ToString();
            lblPurOrdDate.Text = Main_DT.Rows[0]["PurOrdDate"].ToString();

            //WorkOrder Table Load
            SSQL = "Select SapNo,ItemCode,ItemName,DeptCode,DeptName,UOM,ReceiveQty,InspectQty,RejectQty,Rate,ItemTotal,DiscPer,DiscAmt,";
            SSQL = SSQL + " NetAmount,Remarks From Trans_GIChecking_Sub where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And TransNo='" + lblTransNo.Text + "' ";

            Sub_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Sub_DT.Rows.Count != 0)
            {
                ViewState["ItemTable"] = Sub_DT;

                rptItemQCChecking.DataSource = Sub_DT;
                rptItemQCChecking.DataBind();
            }
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_GateIn_CheckingMain.aspx");
    }

    private void Load_Data_GoodsReceipt_No()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select GRNo from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Approval_Status = '1'  and Status!='Delete' and MatType='1' ";
         
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlRFINo.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["GRNo"] = "-Select-";
        dr["GRNo"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlRFINo.DataTextField = "GRNo";
        ddlRFINo.DataValueField = "GRNo";
        ddlRFINo.DataBind();
    }

    protected void ddlRFINo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";

            string SSQL = "";
            DataTable Main_DT = new DataTable();
            DataTable Sub_DT = new DataTable();

            SSQL = "Select * from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And GRNo ='" + ddlRFINo.SelectedItem.Text + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);


            if (Main_DT.Rows.Count != 0)
            {
                lblGRNo.Text= Main_DT.Rows[0]["GRNo"].ToString();
                lblGRDate.Text = Main_DT.Rows[0]["GRDate"].ToString();
                lblSuppName.Text = Main_DT.Rows[0]["SuppName"].ToString();
                hfSuppCode.Value = Main_DT.Rows[0]["SuppCode"].ToString();
                lblSuppInvNo.Text = Main_DT.Rows[0]["PartyInvNo"].ToString();
                lblSuppInvDate.Text = Main_DT.Rows[0]["PartyInvDate"].ToString();
                lblCurType.Text = Main_DT.Rows[0]["CurrencyType"].ToString();
                lblGateInNo.Text = Main_DT.Rows[0]["GPINo"].ToString();
                lblGateInDate.Text = Main_DT.Rows[0]["GPIDate"].ToString();

                if (Main_DT.Rows[0]["SuppType"].ToString() == "1") { lblAccType.Text = "Enercon"; }
                else if (Main_DT.Rows[0]["SuppType"].ToString() == "2") { lblAccType.Text = "Escrow"; }
                else if (Main_DT.Rows[0]["SuppType"].ToString() == "3") { lblAccType.Text = "Coral"; }

                if (Main_DT.Rows[0]["MatType"].ToString() == "1") { lblMatType.Text = "RawMaterial"; }
                else if (Main_DT.Rows[0]["MatType"].ToString() == "2") { lblMatType.Text = "Tools"; }
                else if (Main_DT.Rows[0]["MatType"].ToString() == "3") { lblMatType.Text = "Assets"; }
                else if (Main_DT.Rows[0]["MatType"].ToString() == "4") { lblMatType.Text = "General Item"; }


                SSQL = "Select SapNo,ItemCode from Trans_GoodsReceipt_Sub  where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And GRNo ='" + ddlRFINo.SelectedItem.Text + "'";

                Sub_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                ddlSapNo.DataSource = Sub_DT;
                ddlSapNo.DataBind();

                DataRow dr = Sub_DT.NewRow();

                dr["SapNo"] = "-Select-";

                Sub_DT.Rows.InsertAt(dr, 0);
                ddlSapNo.DataTextField = "SapNo";
                ddlSapNo.DataValueField = "SapNo";
                ddlSapNo.DataBind();

            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void ddlSapNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";

            string SSQL = "";
            DataTable Sub_DT = new DataTable();

            SSQL = "Select PurOrdNo,PurOrdDate,SapNo,ItemCode,ItemName,DeptName,DeptCode,UOM,ReceiveQty,ReceiveQty [InspectQty],";
            SSQL = SSQL + " '0.00'[RejectQty],Rate,ItemTotal,Disc[DiscPer],DiscAmt,(ItemTotal-DiscAmt)NetAmount,'' [Remarks] ";
            SSQL = SSQL + " From Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And GRNo ='" + ddlRFINo.SelectedItem.Text + "'  ";
            SSQL = SSQL + " And SapNo='" + ddlSapNo.SelectedItem.Text + "' And chkQC='1' ";

            Sub_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Sub_DT.Rows.Count != 0)
            {
                hfStdPoNo.Value = Sub_DT.Rows[0]["PurOrdNo"].ToString();
                lblPurOrdDate.Text = Sub_DT.Rows[0]["PurOrdDate"].ToString();

                ViewState["ItemTable"] = Sub_DT;

                rptItemQCChecking.DataSource = Sub_DT;
                rptItemQCChecking.DataBind();
                Grid_Total_Value();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Items QC Checking in This GRN...');", true);
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    private void Text_Change_Value_Cal(object sender, int Row_No)
    {
        try
        {
            lblErrorMsg.Text = "";
            string InspectQty = "0";
            string ItemTotal = "0";
            string DiscPre = "0";
            string DiscAmt = "0";
            string NetAmt = "0";
            string Rate = "";


            int i = Row_No;


            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];

            TextBox txtGrdInspQty = rptItemQCChecking.Items[i].FindControl("txtGrdInspectQty") as TextBox;

            if (txtGrdInspQty.Text.ToString() != "")
            {
                TextBox txtTest = ((TextBox)(sender));
                RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

                Label lblSAPNo = rptItemQCChecking.Items[i].FindControl("lblGrdSapNo") as Label;
                Label lblItemCode = rptItemQCChecking.Items[i].FindControl("lblGrdItemCode") as Label;
                Label lblItemName = rptItemQCChecking.Items[i].FindControl("lblGrdItemName") as Label;
                Label lblDeptCode = rptItemQCChecking.Items[i].FindControl("lblGrdDeptCode") as Label;
                Label lblDeptName = rptItemQCChecking.Items[i].FindControl("lblGrdDeptName") as Label;
                Label lblUOM = rptItemQCChecking.Items[i].FindControl("lblGrdUOMName") as Label;
                Label lblReceiveQty = rptItemQCChecking.Items[i].FindControl("lblGrdReceiveQty") as Label;
                TextBox txtInspectQty = rptItemQCChecking.Items[i].FindControl("txtGrdInspectQty") as TextBox;
                Label lblRejectQty = rptItemQCChecking.Items[i].FindControl("lblGrdRejectQty") as Label;
                Label lblGrdRate = rptItemQCChecking.Items[i].FindControl("lblGrdRate") as Label;
                Label lblItemTot = rptItemQCChecking.Items[i].FindControl("lblGrdItemTotal") as Label;
                Label lblDiscPre = rptItemQCChecking.Items[i].FindControl("lblGrdDiscPre") as Label;
                Label lblDiscAmt = rptItemQCChecking.Items[i].FindControl("lblGrdDiscAmt") as Label;
                Label lblNetAmt = rptItemQCChecking.Items[i].FindControl("lblGrdNetAmt") as Label;
                TextBox txtGrdRemarks = rptItemQCChecking.Items[i].FindControl("txtGrdRemarks") as TextBox;


                if (txtInspectQty.Text != "") { InspectQty = txtInspectQty.Text.ToString(); }
                if (lblDiscPre.Text != "") { DiscPre = lblDiscPre.Text.ToString(); }

                if (lblGrdRate.Text != "") { Rate = lblGrdRate.Text.ToString(); }
                

                ItemTotal = (Convert.ToDecimal(InspectQty) * Convert.ToDecimal(Rate)).ToString();
                ItemTotal = (Math.Round(Convert.ToDecimal(ItemTotal), 4, MidpointRounding.AwayFromZero)).ToString();

                if (Convert.ToDecimal(ItemTotal) != 0)
                {
                    //if (Convert.ToDecimal(lblDiscPer.Text.ToString()) != 0) { Discount_Percent = lblDiscPer.Text.ToString(); }
                    //if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                    //if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                    //if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                    //if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                    if (Convert.ToDecimal(DiscPre) != 0)
                    {
                        DiscAmt = (Convert.ToDecimal(ItemTotal) * Convert.ToDecimal(DiscPre)).ToString();
                        DiscAmt = (Convert.ToDecimal(DiscAmt) / Convert.ToDecimal(100)).ToString();
                        DiscAmt = (Math.Round(Convert.ToDecimal(DiscAmt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        DiscAmt = "0.00";
                    }

                    NetAmt = (Convert.ToDecimal(ItemTotal) - Convert.ToDecimal(DiscAmt)).ToString();



                    //if (Convert.ToDecimal(CGST_Per) != 0)
                    //{
                    //    string Item_Discount_Amt = "0.00";
                    //    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //    CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                    //    CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                    //    CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    //}
                    //else
                    //{
                    //    CGST_Amt = "0.00";
                    //}

                    //if (Convert.ToDecimal(VAT_Per) != 0)
                    //{
                    //    string Item_Discount_Amt = "0.00";
                    //    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //    VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                    //    VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                    //    VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    //}
                    //else
                    //{
                    //    VAT_Amt = "0.00";
                    //}

                    //if (Convert.ToDecimal(SGST_Per) != 0)
                    //{
                    //    string Item_Discount_Amt = "0.00";
                    //    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //    SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                    //    SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                    //    SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    //}
                    //else
                    //{
                    //    SGST_Amt = "0.00";
                    //}

                    //if (Convert.ToDecimal(IGST_Per) != 0)
                    //{
                    //    string Item_Discount_Amt = "0.00";
                    //    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //    IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                    //    IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                    //    IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    //}
                    //else
                    //{
                    //    IGST_Amt = "0.00";
                    //}

                    ////Other Charges
                    //if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                    ////Final Amt
                    //Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    ////Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                    //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                    //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                    //Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 4, MidpointRounding.AwayFromZero)).ToString();

                    //lblItemTot.Text = Item_Total;

                    //lblDisc.Text = Discount_Amt;
                    //lblTaxAmt.Text = Tax_Amt;
                    //lblCGSTAmt.Text = CGST_Amt;
                    //lblSGSTAmt.Text = SGST_Amt;
                    //lblIGSTAmt.Text = IGST_Amt;
                    ////txtVAT_AMT.Text = VAT_Amt;
                    //lblLineTot.Text = Final_Amount;
                    //txtTotAmt.Text = Final_Amount;
                    //txtNetAmt.Text = Final_Amount;

                    lblRejectQty.Text = (Convert.ToDecimal(lblReceiveQty.Text) - Convert.ToDecimal(InspectQty)).ToString();

                    lblItemTot.Text = ItemTotal;
                    lblDiscPre.Text = DiscPre;
                    lblDiscAmt.Text = DiscAmt;
                    lblNetAmt.Text = NetAmt;
                    

                }
                dt.Rows[i]["SAPNo"] = lblSAPNo.Text;
                dt.Rows[i]["ItemCode"] = lblItemCode.Text;
                dt.Rows[i]["ItemName"] = lblItemName.Text;
                dt.Rows[i]["DeptCode"] = lblDeptCode.Text;
                dt.Rows[i]["DeptName"] = lblDeptName.Text;
                dt.Rows[i]["UOM"] = lblUOM.Text;
                dt.Rows[i]["ReceiveQty"] = Math.Round(Convert.ToDecimal(lblReceiveQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["InspectQty"] = Math.Round(Convert.ToDecimal(txtInspectQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["RejectQty"] = Math.Round(Convert.ToDecimal(lblRejectQty.Text), 2, MidpointRounding.AwayFromZero).ToString();
                dt.Rows[i]["Rate"] = lblGrdRate.Text;
                dt.Rows[i]["ItemTotal"] = Math.Round(Convert.ToDecimal(lblItemTot.Text), 2, MidpointRounding.AwayFromZero).ToString();
                dt.Rows[i]["DiscPer"] = Math.Round(Convert.ToDecimal(lblDiscPre.Text), 2, MidpointRounding.AwayFromZero).ToString();
                dt.Rows[i]["DiscAmt"] = Math.Round(Convert.ToDecimal(lblDiscAmt.Text), 2, MidpointRounding.AwayFromZero).ToString();
                dt.Rows[i]["NetAmount"] = Math.Round(Convert.ToDecimal(lblNetAmt.Text), 2, MidpointRounding.AwayFromZero).ToString();
                dt.Rows[i]["Remarks"] = txtGrdRemarks.Text;

                Grid_Total_Value(); 
                //Final_Total_Calculate();
            }

            ViewState["ItemTable"] = dt;
            rptItemQCChecking.DataSource = dt;
            rptItemQCChecking.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void txtGrdInspectQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";

            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);
            ((TextBox)rptItemQCChecking.Items[index].FindControl("txtGrdRemarks")).Focus();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void txtGrdRemarks_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";

            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);
            //if (Convert.ToDecimal(rptItemQCChecking.Items.Count) == Convert.ToDecimal(index + 1))
            //{
            ((TextBox)rptItemQCChecking.Items[index].FindControl("txtGrdInspectQty")).Focus();
            //}
            //else
            //{
            //    ((TextBox)rptItemQCChecking.Items[index + 1].FindControl("txtGrdInspectQty")).Focus();
            //}
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void lnkQCFileName_Click(object sender, EventArgs e)
    {

        string[] SplitStr = lnkQCFileName.Text.Split('.');

        string SufStr = SplitStr[1].ToString();

        string path = MapPath("~" + lnkQCFileName.Text);
        WebClient user = new WebClient();

        byte[] vs = user.DownloadData(path);

        if (vs != null)
        {
            Response.ContentType = "Application/pdf";

            if (SufStr.ToString() == "pdf")
            {
                Response.AppendHeader("Content-Disposition", "attachment;filename=QC_Doc_" + lblTransNo.Text + ".pdf");
            }
            else if (SufStr.ToString() == "ppt" || SufStr.ToString() == "pptx")
            {
                Response.AppendHeader("Content-Disposition", "attachment;filename=QC_Doc_" + lblTransNo.Text + ".ppt");
            }
            else if (SufStr.ToString() == "jpg")
            {
                Response.AppendHeader("Content-Disposition", "attachment;filename=QC_Doc_" + lblTransNo.Text + ".jpg");
            }
            else if (SufStr.ToString() == "png")
            {
                Response.AppendHeader("Content-Disposition", "attachment;filename=QC_Doc_" + lblTransNo.Text + ".png");
            }
            else if (SufStr.ToString() == "zip")
            {
                Response.AppendHeader("Content-Disposition", "attachment;filename=QC_Doc_" + lblTransNo.Text + ".zip");
            }
            else if (SufStr.ToString() == "rar")
            {
                Response.AppendHeader("Content-Disposition", "attachment;filename=QC_Doc_" + lblTransNo.Text + ".rar");
            }


            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.TransmitFile(path);
            Response.End();
        }
    }

    public void Grid_Total_Value()
    {

        decimal SumInspQtyTot = 0;
        decimal SumRejctTot = 0;
        decimal SumItemTot = 0;
        decimal SumNetTot = 0;
        decimal SumDiscTot = 0;
        

        DataTable DT = new DataTable();

        DT = (DataTable)ViewState["ItemTable"];

        rptItemQCChecking.DataSource = DT;
        rptItemQCChecking.DataBind();

        for (int i = 0; i < rptItemQCChecking.Items.Count; i++)
        {
            TextBox txtGrdInspQty = rptItemQCChecking.Items[i].FindControl("txtGrdInspectQty") as TextBox;
            Label lblRejectQty = rptItemQCChecking.Items[i].FindControl("lblGrdRejectQty") as Label;
            Label lblItemTot = rptItemQCChecking.Items[i].FindControl("lblGrdItemTotal") as Label;
            Label lblDiscTot = rptItemQCChecking.Items[i].FindControl("lblGrdDiscAmt") as Label;
            Label lblNteTot = rptItemQCChecking.Items[i].FindControl("lblGrdNetAmt") as Label;

            SumInspQtyTot = SumInspQtyTot + Convert.ToDecimal(txtGrdInspQty.Text);
            SumRejctTot = SumRejctTot + Convert.ToDecimal(lblRejectQty.Text);
            SumItemTot = SumItemTot + Convert.ToDecimal(lblItemTot.Text);
            SumDiscTot = SumDiscTot + Convert.ToDecimal(lblDiscTot.Text);
            SumNetTot = SumNetTot + Convert.ToDecimal(lblNteTot.Text);

        }
        lblTotQty.Text = SumInspQtyTot.ToString();
        lblTotRejectQty.Text = SumRejctTot.ToString();
        lblTotItemAmt.Text = SumItemTot.ToString();
        lblTotDiscAmt.Text = SumDiscTot.ToString();
        lblTotItemNetAmt.Text = SumNetTot.ToString();
    }
}