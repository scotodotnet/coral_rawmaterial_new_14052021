﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_PO_GPIN_Main.aspx.cs" Inherits="Trans_PO_GPIN_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <section class="content">
                    <div class="row">
                        <div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />
                        </div>
                        <!-- /.col -->
                    </div>
                    <!--/.row-->
                    <div class="row">
                        <div class="col-md-12">

                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Purchase Order GatePass IN</span></h3>
                                    <div class="box-tools pull-right" runat="server" visible="false">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body">
                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false" OnItemDataBound="Repeater2_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="example" class="table table-responsive table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>GatePassIN No</th>
                                                                        <th>GatePassIN Date</th>
                                                                        <%--<th>Supplier Name</th>--%>
                                                                        <th>Supplier InvNo</th>
                                                                        <th>Supplier InvDate</th>
                                                                        <th>Transport Name</th>
                                                                        <th>Vehicle No</th>
                                                                        <th>Status</th>
                                                                        <th>Edit/Delete</th>
                                                                    </tr>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Eval("Pur_GIN_No")%></td>
                                                                <td><%# Eval("Pur_GIN_Date")%></td>
                                                                <%--<td><%# Eval("SuppName")%></td>--%>
                                                                <td><%# Eval("SuppInvNo")%></td>
                                                                <td><%# Eval("SuppInvDate")%></td>
                                                                <td><%# Eval("TransportName")%></td>
                                                                <td><%# Eval("VehicleNo")%></td>
                                                                <td>
                                                                    <asp:Label ID="lblStatus" style="width:100%" runat="server" class="form-control" Text='<%# Eval("App_Status") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("Pur_GIN_No")%>'>
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("Pur_GIN_No")%>'
                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Goods Received details?');">
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate></table></FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>




                                            <!-- /.table -->
                                        </div>
                                        <!-- /.mail-box-messages -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

