﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_GatePassOut_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGPOutNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal RetQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Gatepass Out";
            Initial_Data_Referesh();
            Load_ItemStock();
            Load_Supplier();
            Load_Data_IdentificationNo();
            Load_Data_UOM();

            if (Session["GPOut_No"] == null)
            {
                SessionGPOutNo = "";

                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "GatePass Out", SessionFinYearVal, "1");

                //lblGPOutNo.Text = Auto_Transaction_No;
                txtGPOutDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            }
            else
            {
                SessionGPOutNo = Session["GPOut_No"].ToString();
                lblGPOutNo.Text = SessionGPOutNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }

        //Auto generate Transaction Function Call

        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "GatePass Out", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    lblGPOutNo.Text = Auto_Transaction_No;
                }
            }
        }

        string GPType = "";
        string StockType = "";

        GPType = ddlGPOutType.SelectedItem.Text;
        StockType = RdpStkType.SelectedItem.Text;

        if (!ErrFlag)
        {

            SSQL = "Delete From Trans_GPOut_Main where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " GPOutNo='" + lblGPOutNo.Text + "' ";

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete From Trans_GPOut_Sub where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " GP_Out_No='" + lblGPOutNo.Text + "' ";

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Insert Into Trans_GPOut_Main(Ccode,Lcode,FinYearCode,FinYearVal,GPOutNo,GPOutDate,GPType,SuppCode,";
            SSQL = SSQL + " SuppName,SuppDet,DeliveryDate,Remarks,StockType,Status,UserID,UserName,CreateOn,Approval_Status,";
            SSQL = SSQL + " Return_Type,SuppInvNo,SuppInvDate,MatType)Values('" + SessionCcode + "','" + SessionLcode + "',";
            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + lblGPOutNo.Text + "',";
            SSQL = SSQL + " '" + txtGPOutDate.Text + "','" + GPType + "','" + ddlSupplier.SelectedValue + "',";
            SSQL = SSQL + " '" + ddlSupplier.SelectedItem.Text + "','" + txtSuppDet.Text + "','" + txtDeliDate.Text + "',";
            SSQL = SSQL + " '" + txtRemarks.Text + "','" + StockType + "','Add','" + SessionUserID + "',";
            SSQL = SSQL + " '" + SessionUserName + "',Convert(datetime,Getdate(),103),'0','" + GPType + "',";
            SSQL = SSQL + " '" + txtSuppInvNo.Text + "','" + txtSuppInvDate.Text + "','" + ddlMatType.SelectedValue + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                
                SSQL = "Insert Into Trans_GPOut_Sub(Ccode,Lcode,FinYearCode,FinYearVal,GP_Out_No,GP_Out_Date,";
                SSQL = SSQL + " RefNo,ItemCode,ItemName,UOMCode,UOMName,DeptCode,DeptName,WarehouseCode,WarehouseName,RackName,";
                SSQL = SSQL + " OutQty,Rate,Value,Remarks,Status,UserId,UserName,CreateOn)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + lblGPOutNo.Text + "','" + txtGPOutDate.Text + "','" + dt.Rows[i]["SapNo"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["UOMCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["UOMName"].ToString() + "','" + dt.Rows[i]["Deptcode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["RackName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["OutQty"].ToString() + "','0.00','0.00',";
                SSQL = SSQL + " '" + dt.Rows[i]["ItemRem"].ToString() + "','Add','" + SessionUserID + "','" + SessionUserName + "',";
                SSQL = SSQL + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);

            }

            if (SaveMode == "Insert")
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "GatePass Out", SessionFinYearVal, "1");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass Out Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass Out Details Updated Successfully');", true);
            }

            //Clear_All_Field();
            Session["GPOutNo"] = lblGPOutNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
            Response.Redirect("Trans_GatePassOut_Main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Load_Supplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select LedgerCode,LedgerName From Acc_Mst_Ledger where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And Status='Add' And LedgerGrpName='Supplier' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlSupplier.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlSupplier.DataValueField = "LedgerCode";
        ddlSupplier.DataTextField = "LedgerName";
        ddlSupplier.DataBind();

    }


    private void Load_Data_IdentificationNo()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = "Select Mat_No[ItemCode],Sap_No[SapNo] from BomMaster Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode[ItemCode],RefCode[SapNo] from MstTools Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ItemCode[ItemCode],RefCode[SapNo] from CORAL_ERP_Asset..MstAssetItem ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = "Select ItemCode[ItemCode],RefCode[SapNo] from MstGeneralItem Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlIdentiNo.DataSource = DT;

            DataRow dr = DT.NewRow();

            dr["ItemCode"] = "-Select-";
            dr["SapNo"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);

            ddlIdentiNo.DataValueField = "ItemCode";
            ddlIdentiNo.DataTextField = "SapNo";
            ddlIdentiNo.DataBind();

        }
        catch (Exception ex)
        {

        }
    }

    private void Load_ItemStock()
    {
        string SSQL = "";
        DataTable DT = new DataTable();



        if (ddlMatType.SelectedItem.Text == "RawMaterial")
        {
            SSQL = "Select Mat_No[ItemCode],Raw_Mat_Name [ItemName] from BomMaster Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "Tools")
        {
            SSQL = "Select ToolCode[ItemCode],ToolName[ItemName] from MstTools Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "Asset")
        {
            SSQL = "Select ItemCode[ItemCode],ItemDesc [ItemName] from CORAL_ERP_Asset..MstAssetItem ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status!='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "General Items")
        {
            SSQL = "Select ItemCode[ItemCode],ItemName[ItemName] from MstGeneralItem Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlItemName.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["ItemName"] = "-Select-";
        dr["ItemCode"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataValueField = "ItemCode";
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataBind();
    }

    private void Clear_All_Field()
    {
        lblGPOutNo.Text = ""; txtGPOutDate.Text = "";
        // txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        //txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("GPOutNo");
        //Load_Data_Enquiry_Grid();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Trans_GPOut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GPOutNo ='" + lblGPOutNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            lblGPOutNo.Text = Main_DT.Rows[0]["GPOutNo"].ToString();
            txtGPOutDate.Text = Main_DT.Rows[0]["GPOutDate"].ToString();

            ddlSupplier.SelectedItem.Text = Main_DT.Rows[0]["SuppName"].ToString();
            ddlSupplier.SelectedValue = Main_DT.Rows[0]["SuppCode"].ToString();
            txtSuppDet.Text = Main_DT.Rows[0]["SuppDet"].ToString();
            txtDeliDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            txtSuppInvNo.Text = Main_DT.Rows[0]["SuppInvNo"].ToString();
            txtSuppInvDate.Text = Main_DT.Rows[0]["SuppInvDate"].ToString();

            ddlMatType.SelectedValue= Main_DT.Rows[0]["MatType"].ToString();



            if (Main_DT.Rows[0]["GPType"].ToString() == "Returnable")
            {
                ddlGPOutType.SelectedValue = "1";
            }
            else
            {
                ddlGPOutType.SelectedValue = "2";
            }


            if (Main_DT.Rows[0]["StockType"].ToString() == "Stock")
            {
                RdpStkType.SelectedValue = "1";
            }
            else
            {
                RdpStkType.SelectedValue = "2";
            }

            //txtTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();
            //txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
            //txtTotAmt.Text = Main_DT.Rows[0]["TotalAmount"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select RefNo[SapNo],ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,RackName,UOMCode,UOMName,OutQty,Rate, ";
            SSQL = SSQL + " Value,Remarks[ItemRem] From Trans_GPOut_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode = '" + SessionFinYearCode + "' And GP_Out_No='" + lblGPOutNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_GatePassOut_Main.aspx");
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string Old_New_Type = "1";

            string SSQL = "";
            DataTable DT = new DataTable();
            string IssueType = "";

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = "Select Mat_No[ItemCode],UOMTypeCode[UOMCode],UOM_Full[UOMName],DeptCode,DeptName,WarehouseCode,WarehouseName,RackSerious ";
                SSQL = SSQL + " From BOMMaster Where Mat_No ='" + ddlItemName.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode[ItemCode],UOMCode [UOMCode],UOM[UOMName],DeptCode,DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From MstTools Where ToolCode ='" + ddlItemName.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ItemCode,UOMCode[UOMCode],UOM[UOMName],DeptCode,DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From Coral_ERP_Asset..MstAssetItem Where ItemCode ='" + ddlItemName.SelectedValue + "' and ";
                SSQL = SSQL + " Status!='Delete' and Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = "Select ItemCode,UOMCode[UOMCode],UOM[UOMName],'' DeptCode, '' DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From MstGeneralItem Where ItemCode ='" + ddlItemName.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlIdentiNo.SelectedValue = DT.Rows[0]["ItemCode"].ToString();

            hfUOMCode.Value = DT.Rows[0]["UOMCode"].ToString();
            hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();
            hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
            hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
            hfWarehouseCode.Value = DT.Rows[0]["WarehouseCode"].ToString();
            hfWarehouseName.Value = DT.Rows[0]["WarehouseName"].ToString();
            hfRackNo.Value = DT.Rows[0]["RackSerious"].ToString();

            if (Old_New_Type == "1")
            {

                SSQL = "Select (sum(Add_Qty)-sum(Minus_Qty)) StockQty,(sum(Add_Value)-sum(Minus_Value)) stockVal,";
                SSQL = SSQL + " ((sum(Add_Value)-sum(Minus_Value))/(sum(Add_Qty)-sum(Minus_Qty))) Rate  ";
                SSQL = SSQL + " From Trans_Stock_Ledger_All Where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + "   Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "' having (sum(Add_Qty)-sum(Minus_Qty))>0";


                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (Old_New_Type == "2")
            {
                SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (DT.Rows.Count != 0)
            {

                txtRate.Text = DT.Rows[0]["Rate"].ToString();
            }
            else
            {


            }
        }
        catch (Exception ex)
        {

        }

    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtGPOutQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Return Qty...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["SapNo"] = ddlIdentiNo.SelectedItem.Text;
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["DeptCode"] = hfDeptCode.Value;
                    dr["DeptName"] = hfDeptName.Value;
                    dr["WarehouseCode"] = hfWarehouseCode.Value;
                    dr["WarehouseName"] = hfWarehouseName.Value;
                    dr["RackName"] = hfRackNo.Value;
                    dr["UOMCode"] = hfUOMCode.Value;
                    dr["UOMName"] = hfUOMName.Value;

                    dr["OutQty"] = txtGPOutQty.Text;
                    dr["ItemRem"] = txtItemNotes.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    ddlItemName.SelectedIndex = 0; ddlIdentiNo.SelectedIndex = 0;
                    lblStockQty.Text = "0.00"; 
                    txtGPOutQty.Text = "0"; txtRate.Text = "0"; txtValue.Text = "0.0"; txtItemNotes.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();

                dr["SapNo"] = "Test";
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["DeptCode"] = hfDeptCode.Value;
                dr["DeptName"] = hfDeptName.Value;
                dr["WarehouseCode"] = hfWarehouseCode.Value;
                dr["WarehouseName"] = hfWarehouseName.Value;
                dr["RackName"] = hfRackNo.Value;
                dr["UOMCode"] = hfUOMCode.Value;
                dr["UOMName"] = hfUOMName.Value;

                dr["OutQty"] = txtGPOutQty.Text;
                dr["ItemRem"] = txtItemNotes.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                txtGPOutQty.Text = "0"; txtRate.Text = "0.00"; txtValue.Text = "0.0"; txtItemNotes.Text = "";
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("SapNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("RackName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMName", typeof(string)));
        dt.Columns.Add(new DataColumn("OutQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRem", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        //dt = Repeater1.DataSource;
    }

    protected void txtGPOutQty_TextChanged(object sender, EventArgs e)
    {
        bool ErrFlg = false;

        if (Convert.ToDecimal(lblStockQty.Text) < Convert.ToDecimal(txtGPOutQty.Text))
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('More Then Stock Qty,Check It..');", true);

            txtGPOutQty.Text = "0.00";
        }

        if (!ErrFlg)
        {
            if (Convert.ToDecimal(txtRate.Text) > 0)
            {
                txtValue.Text = Math.Round((Convert.ToDecimal(txtRate.Text) * Convert.ToDecimal(txtGPOutQty.Text)), 2).ToString();
            }
        }
    }

    protected void txtOutQty_TextChanged(object sender, EventArgs e)
    {
        TextBox txtTest = ((TextBox)(sender));
        RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

        TextBox txtGPOut = (TextBox)rpi.FindControl("txtOutQty");
        Label lblRate = (Label)rpi.FindControl("lblRate");

        Label lblValue = (Label)rpi.FindControl("lblValue");

        decimal Val = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtGPOut.Text);

        lblValue.Text = Math.Round(Val, 2).ToString();
    }

    protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "select HouseNo+','+Add1+','+Add2+','+ City+'-'+PinCode [Address] from Acc_Mst_Ledger where LedgerGrpName='Supplier' And ";
        SSQL = SSQL + " LedgerCode ='" + ddlSupplier.SelectedValue + "' And Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if(DT.Rows.Count>0)
        {
            txtSuppDet.Text = DT.Rows[0]["Address"].ToString();
        }
    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_ItemStock();
        Load_Data_IdentificationNo();
    }

    protected void ddlIdentiNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string Old_New_Type = "1";

            string SSQL = "";
            DataTable DT = new DataTable();
            string IssueType = "";

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = "Select Mat_No[ItemCode],UOMTypeCode[UOMCode],UOM_Full[UOMName],DeptCode,DeptName,WarehouseCode,WarehouseName,RackSerious ";
                SSQL = SSQL + " From BOMMaster Where Mat_No ='" + ddlIdentiNo.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode[ItemCode],UOMCode [UOMCode],UOM[UOMName],DeptCode,DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From MstTools Where ToolCode ='" + ddlIdentiNo.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ItemCode,UOMCode[UOMCode],UOM[UOMName],DeptCode,DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From Coral_ERP_Asset..MstAssetItem Where ItemCode ='" + ddlIdentiNo.SelectedValue + "' and ";
                SSQL = SSQL + " Status!='Delete' and Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = "Select ItemCode,UOMCode[UOMCode],UOM[UOMName],'' DeptCode, '' DeptName,'' WarehouseCode,'' WarehouseName,'' RackSerious ";
                SSQL = SSQL + " From MstGeneralItem Where ItemCode ='" + ddlIdentiNo.SelectedValue + "' and Status!='Delete' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' ";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlItemName.SelectedValue = DT.Rows[0]["ItemCode"].ToString();

            hfUOMCode.Value = DT.Rows[0]["UOMCode"].ToString();
            hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();
            hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
            hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
            hfWarehouseCode.Value = DT.Rows[0]["WarehouseCode"].ToString();
            hfWarehouseName.Value = DT.Rows[0]["WarehouseName"].ToString();
            hfRackNo.Value = DT.Rows[0]["RackSerious"].ToString();

            if (Old_New_Type == "1")
            {
                SSQL = "Select (sum(Add_Qty)-sum(Minus_Qty)) StockQty,(sum(Add_Value)-sum(Minus_Value)) stockVal,";
                SSQL = SSQL + " ((sum(Add_Value)-sum(Minus_Value))/(sum(Add_Qty)-sum(Minus_Qty))) Rate  ";
                SSQL = SSQL + " From Trans_Stock_Ledger_All Where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + "   Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And ItemCode='" + ddlIdentiNo.SelectedValue + "' having (sum(Add_Qty)-sum(Minus_Qty))>0";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (Old_New_Type == "2")
            {
                SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And ItemCode='" + ddlIdentiNo.SelectedValue + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (DT.Rows.Count != 0)
            {
                lblStockQty.Text = Math.Round(Convert.ToDecimal(DT.Rows[0]["StockQty"]), 2, MidpointRounding.AwayFromZero).ToString();
                txtRate.Text = Math.Round(Convert.ToDecimal(DT.Rows[0]["Rate"]), 2, MidpointRounding.AwayFromZero).ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item No Stock..');", true);

                ddlItemName.SelectedIndex = 0;
                txtRate.Text = "0.00";
                txtGPOutQty.Text = "0.00";
                txtValue.Text = "0.00";
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void Load_Data_UOM()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select UOMCode,UOMShortName[UOM] From MstUom Where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete' ";


            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlNonStkUOM.DataSource = DT;

            DataRow dr = DT.NewRow();

            dr["UOMCode"] = "-Select-";
            dr["UOM"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);

            ddlNonStkUOM.DataValueField = "UOMCode";
            ddlNonStkUOM.DataTextField = "UOM";
            ddlNonStkUOM.DataBind();

        }
        catch (Exception ex)
        {

        }
    }

    protected void RdpStkType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdpStkType.SelectedIndex == 0)
        {
            pnlNonStock.Visible = false;
            pnlStock.Visible = true;
        }
        else
        {
            pnlNonStock.Visible = true;
            pnlStock.Visible = false;
        }
    }

    protected void btnNonStkAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtNonOutQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Return Qty...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["SapNo"] = "NA";
                    dr["ItemCode"] = "NA";
                    dr["ItemName"] = txtNonStkItemName.Text;
                    dr["DeptCode"] = "-Select-";
                    dr["DeptName"] = "-Select-";
                    dr["WarehouseCode"] = "-Select-";
                    dr["WarehouseName"] = "-Select-";
                    dr["RackName"] = "-Select-";
                    dr["UOMCode"] = ddlNonStkUOM.SelectedValue;
                    dr["UOMName"] = ddlNonStkUOM.SelectedItem.Text;

                    dr["OutQty"] = txtNonOutQty.Text;
                    dr["ItemRem"] = txtNonStkNotes.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    ddlItemName.SelectedIndex = 0; ddlIdentiNo.SelectedIndex = 0;
                    lblStockQty.Text = "0.00"; txtRemarks.Text = "0.00";
                    txtGPOutQty.Text = "0"; txtRate.Text = "0"; txtValue.Text = "0.0"; txtItemNotes.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();

                dr["SapNo"] = "NA";
                dr["ItemCode"] = "NA";
                dr["ItemName"] = txtNonStkItemName.Text;
                dr["DeptCode"] = "-Select-";
                dr["DeptName"] = "-Select-";
                dr["WarehouseCode"] = "-Select-";
                dr["WarehouseName"] = "-Select-";
                dr["RackName"] = "-Select-";
                dr["UOMCode"] = ddlNonStkUOM.SelectedValue;
                dr["UOMName"] = ddlNonStkUOM.SelectedItem.Text;

                dr["OutQty"] = txtNonOutQty.Text;
                dr["ItemRem"] = txtNonStkNotes.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                txtGPOutQty.Text = "0"; txtRate.Text = "0.00"; txtValue.Text = "0.0"; txtItemNotes.Text = "";
            }
        }
    }
}