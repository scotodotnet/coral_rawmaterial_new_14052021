﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Web.Script.Serialization;
using Altius.BusinessAccessLayer.BALDataAccess;

public class Handler : IHttpHandler
{
    string SSQL = "";
    string fileName = "";
    string fileExtension = "";
    string _setfileName = "";
    BALDataAccess objdata = new BALDataAccess();
    public void ProcessRequest(HttpContext context)
    {

        ////Check if Request is to Upload the File.
        //if (context.Request.Files.Count > 0)
        //{
        //    //Fetch the Uploaded File.
        //    HttpPostedFile postedFile = context.Request.Files[0];

        //    //Set the Folder Path.
        //    string folderPath = context.Server.MapPath("~/Uploads/");

        //    //Set the File Name.
        //    string fileName = Path.GetFileName(postedFile.FileName);

        //    //Save the File in Folder.
        //    postedFile.SaveAs(folderPath + fileName);

        //    //Send File details in a JSON Response.
        //    string json = new JavaScriptSerializer().Serialize(
        //        new
        //        {
        //            name = fileName
        //        });
        //    context.Response.StatusCode = (int)HttpStatusCode.OK;
        //    context.Response.ContentType = "text/json";
        //    context.Response.Write(json);
        //    context.Response.End();
        //}
        context.Response.ContentType = "text/plain";
        try
        {
            string _pathGet = "";

            if (context.Request.Form.Get("type") == "PhotoProfile")
            {
                SSQL = "";
                SSQL = "select path from FilePaths where Type='" + context.Request.Form.Get("type") + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    _pathGet = dt.Rows[0]["Path"].ToString();
                }
            }

            if (context.Request.Form.Get("type") == "PhotoBOM")
            {
                SSQL = "";
                SSQL = "select path from FilePaths where Type='" + context.Request.Form.Get("type") + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    _pathGet = dt.Rows[0]["Path"].ToString();
                }
            }

            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                fileName = file.FileName;
                if (!Directory.Exists(_pathGet))
                {
                    Directory.CreateDirectory(_pathGet);
                }
                if (!string.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    _setfileName = context.Request.Form.Get("filename");
                    _setfileName = _setfileName + fileExtension;
                    string pathToSave_100 = HttpContext.Current.Server.MapPath(_pathGet) + _setfileName;
                    file.SaveAs(pathToSave_100);
                }
            }
            context.Response.Write(_setfileName);
        }
        catch (Exception ac)
        {

        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}