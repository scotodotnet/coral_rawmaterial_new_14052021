﻿$(document).ready(function () {


    var rowCountRmItemTbl = 0;
    $("#TblRmSingleItem").DataTable({
        stateSave: true,
        "processing": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iPageIndex) {
            $("td:first", nRow).html(iPageIndex + 1);
            CalcFooter();
            return nRow;
        },
        "drawCallback": function () {
            $('.txtReqQtyEdit').keyup(function () {
                var preVal = $(this).closest("tr").find('td:eq(6)').text();
                var multipleVal = $(this).closest("tr").find('.txtModelQtyEdit').val();
                var newVal = ($(this).val() * multipleVal);
                $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
                CalcFooter();
            });
            $('.txtModelQtyEdit').keyup(function () {
                var preVal = $(this).closest("tr").find('td:eq(6)').text();
                var multipleVal = $(this).closest("tr").find('.txtReqQtyEdit').val();
                var newVal = ($(this).val() * multipleVal);
                $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
                CalcFooter();
            });
            CalcFooter();
        }
    });

    $("#TblRmModelBased").DataTable({
        stateSave: true,
        "processing": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            CalcFooter();
            return nRow;
        },
        "drawCallback": function () {
            $('.txtReqQtyEdit').keyup(function () {
                var preVal = $(this).closest("tr").find('td:eq(6)').text();
                var multipleVal = $(this).closest("tr").find('.txtModelQtyEdit').val();
                var newVal = ($(this).val() * multipleVal);
                $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
                CalcFooter();
            });
            $('.txtModelQtyEdit').keyup(function () {
                var preVal = $(this).closest("tr").find('td:eq(6)').text();
                var multipleVal = $(this).closest("tr").find('.txtReqQtyEdit').val();
                var newVal = ($(this).val() * multipleVal);
                $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
                CalcFooter();
            });
            CalcFooter();
        }
    });

    //Tools Tables
    $("#TblToolSingleItem").DataTable({
        stateSave: true,
        "processing": true,
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false,
        "ordering": false,

        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            $(this).closest("tr").find('.txtModelQtyEdit').attr('style', 'width:50%');
            $(this).closest("tr").find('.txtReqQtyEdit').attr('style', 'width:50%');
            CalcFooter();
            return nRow;
        },
        "drawCallback": function () {
            $(this).closest("tr").find('.txtModelQtyEdit').attr('style', 'width:50%');
            $(this).closest("tr").find('.txtReqQtyEdit').attr('style', 'width:50%');
            $('.txtReqQtyEdit').keyup(function () {
                console.log($(this).val());
                var preVal = $(this).closest("tr").find('td:eq(6)').text();
                var multipleVal = $(this).closest("tr").find('.txtModelQtyEdit').val();
                var newVal = ($(this).val() * multipleVal);
                $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
                CalcFooter();
            });
            $('.txtModelQtyEdit').keyup(function () {
                console.log($(this).val());
                var preVal = $(this).closest("tr").find('td:eq(6)').text();
                var multipleVal = $(this).closest("tr").find('.txtReqQtyEdit').val();
                var newVal = ($(this).val() * multipleVal);
                $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
                CalcFooter();
            });
            CalcFooter();
        }
    });


    $("#TblToolModelBased").DataTable({
        stateSave: true,
        "processing": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            CalcFooter();
            return nRow;
        },
        "drawCallback": function () {
            $('.txtReqQtyEdit').keyup(function () {
                var preVal = $(this).closest("tr").find('td:eq(6)').text();
                var multipleVal = $(this).closest("tr").find('.txtModelQtyEdit').val();
                var newVal = ($(this).val() * multipleVal);
                $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
                CalcFooter();
            });
            $('.txtModelQtyEdit').keyup(function () {
                var preVal = $(this).closest("tr").find('td:eq(6)').text();
                var multipleVal = $(this).closest("tr").find('.txtReqQtyEdit').val();
                var newVal = ($(this).val() * multipleVal);
                $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
                CalcFooter();
            });
            CalcFooter();
        }
    });
    //Get Deptartment value
    $.ajax({
        url: "Trans_Enercon_PurReq_Sub.aspx/GetLoadDept",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#deptddl').empty();
            $('#deptddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
            $.each(response.d, function (i, value) {
                $('#deptddl').append($('<option>').text(value.DeptName).attr('value', value.DeptCode));
            });
        }
    });

    $(function () {
        if (window.opener != null && !window.opener.closed) {
            var parent = $(window.opener.document).contents();
            var ReqNoEdit = parent.find("[id*=EditEnqAsp]").val()
            $('#hiddenSaveType').val('2');
            $('#PurReqNotxt').html(ReqNoEdit);
            $.ajax({
                type: "POST",
                url: "Trans_Enercon_PurReq_Sub.aspx/Search",
                data: '{ReqNo:"' + ReqNoEdit + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    // console.log(response);
                    $.each(response.d, function (i, value) {

                        $('#MatTypddl').val(value.mattype);
                        $('#MatTypddl').select2().trigger('change.select2');
                        $('#MatTypddl').trigger('change');
                        $('#TransDatetxt').val(value.ReqDate);
                        $('#deptddl').val(value.DeptVal);
                        $('#deptddl').select2().trigger('change.select2');
                        $('#txtDescription').val(value.Description);
                        if (value.mattype == "1") {
                            $('#purOdrTypeddl').val(value.PurType);
                            $('#purOdrTypeddl').select2().trigger('change.select2');
                            $('#purOdrTypeddl').trigger('change');

                        } else if (value.mattype == "2") {
                            $('#toolpurOdrTypeddl').val(value.PurType);
                            $('#toolpurOdrTypeddl').select2().trigger('change.select2');
                            $('#purOdrTypeddl').trigger('change');
                        }
                        $.ajax({
                            type: "POST",
                            url: "Trans_Enercon_PurReq_Sub.aspx/SearchSubItem",
                            data: '{ReqNo:"' + ReqNoEdit + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                console.log(response);
                                if (response.d.length > 0) {
                                    var tableget = "";
                                    if ((value.mattype) == "1") {
                                        if ((value.PurType) == "1") {
                                            tableget = $('#TblRmSingleItem').DataTable();
                                            tableget = tableget.clear();
                                        }
                                        if ((value.PurType) == "2") {
                                            tableget = $('#TblRmModelBased').DataTable();
                                            tableget = tableget.clear();
                                        }
                                    }
                                    if ((value.mattype) == "2") {
                                        if ((value.PurType) == "1") {
                                            tableget = $('#TblToolSingleItem').DataTable();
                                            tableget = tableget.clear();
                                        }
                                        if ((value.PurType) == "2") {
                                            tableget = $('#TblToolModelBased').DataTable();
                                            tableget = tableget.clear();
                                        }
                                    }
                                    var length = Object.keys(response.d).length;
                                    var j = 0;

                                    function rmsearchloop() {
                                        var row = response.d[j];
                                        tableget.row.add([
                                            //"",
                                            //row.ToolsNo,
                                            //row.SApNo,
                                            //row.Matname,
                                            //row.UomTex,
                                            ////"<input type='text' value=" + row.ReqQty + " class='form-control txtReqQtyEdit' style='width:100%' />",
                                            ////"<input type='text' value=" + row.modelQty + " class='form-control txtModelQtyEdit' style='width:100%' />",
                                            //"<input type='text'  value=" + row.SumQty + " class='form-control sumQty' style='width:100%' />",
                                            //row.ReqDate,
                                            //"<input type='text'  value=" + row.CalenderWeek + " class='form-control txtCalenderWeek' style='width:100%' />",
                                            //row.Remarks,
                                            //"<i class='btn-danger btn-sm fa fa-trash-o delete'></i>"

                                             "",
                                            "<span  class='form-control' style='border: none;width:100%'>" + row.ToolsNo + "</span>",
                                            "<span  class='form-control' style='border: none;width:100%'>" + row.SApNo + "</span>",
                                            "<span  class='form-control' style='border: none;width:100%'>" + row.Matname + "</span>",
                                            "<span  class='form-control' style='border: none;width:100%'>" + row.UomTex + "</span>",
                                            "<input type='text'  value=" + row.SumQty + " class='form-control sumQty' style='width:75%'/>",
                                            "<span  class='form-control' style='border: none;width:100%'>" + row.ReqDate + "</span>",
                                            "<input type='text' class='form-control txtCalenderWeek' value='" + row.CalenderWeek + "' style='width:75%'/>",
                                            "<span  class='form-control' style='border: none;width:100%'>" + row.Remarks + "</span>",
                                            "<i class='btn-danger btn-sm fa fa-trash-o delete'></i>"

                                        ]).draw(true);
                                        j++;
                                        if (j < length) {
                                            setTimeout(function () { rmsearchloop(); }, (length / 50).toFixed(0));

                                        } else {

                                            return;
                                        }
                                    }
                                    rmsearchloop();
                                }
                            }
                        });
                    })
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);
                    $('.PurReqNotxt').html(errorThrown);
                    $('.PurReqNotxt').css("color", "Red");
                }
            });

        } else {
            window.setInterval(function () {
                var mattype = $('#MatTypddl').children("option:selected").val();
                $.ajax({
                    type: "POST",
                    url: "Trans_Enercon_PurReq_Sub.aspx/GetLastNo",
                    data: '{MatType:"' + mattype + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != "false") {
                            $('#PurReqNotxt').html(response.d);
                        } else {

                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('#PurReqNotxt').html(errorThrown);
                        $('#PurReqNotxt').css("color", "Red");
                        $('.btn').prop('disabled', true);
                        $('.form-control').prop('disabled', true);
                        parent.location = 'Default.aspx';
                        //alert('Please Login and Continue!!!')

                    }
                });
                if ($('#TransDatetxt').val() == "") {
                    Date.prototype.yyyymmdd = function () {
                        var yyyy = this.getFullYear().toString();
                        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                        var dd = this.getDate().toString();
                        return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding
                    };
                    var date = new Date();
                    $('#TransDatetxt').val(date.yyyymmdd());
                }

            }, 1000);
        }
    });


    $('#TblToolSingleItem').on('draw.dt', function () {
        $(this).closest("tr").find('.txtModelQtyEdit').attr('style', 'width:50%');
        $(this).closest("tr").find('.txtReqQtyEdit').attr('style', 'width:50%');
    });

    function CalcFooter() {
        var amtPage = 0;
        var amtTotal = 0;

        var Sum = 0;


        //Raw Material Item
        if ($("#MatTypddl").children("option:selected").val() == "1") {
            if ($("#purOdrTypeddl").children("option:selected").val() == "1") {
                $('#TblRmSingleItem').DataTable().$('.sumQty').each(function (index, Obj) {
                    var value = parseFloat($(this).text().replace(',', ''));
                    if (!isNaN(value)) amtTotal += value;
                });
            } else if ($("#purOdrTypeddl").children("option:selected").val() == "2") {
                $('#TblRmModelBased').DataTable().$('.sumQty').each(function (index, Obj) {
                    var value = parseFloat($(this).text().replace(',', ''));
                    if (!isNaN(value)) amtTotal += value;
                });
            }
        }
            //Tool Item
        else if ($("#MatTypddl").children("option:selected").val() == "2") {
            if ($("#toolpurOdrTypeddl").children("option:selected").val() == "1") {
                $('#TblToolSingleItem').DataTable().$('.sumQty').each(function (index, Obj) {
                    var value = parseFloat($(this).text().replace(',', ''));
                    if (!isNaN(value)) amtTotal += value;
                });
            } else if ($("#toolpurOdrTypeddl").children("option:selected").val() == "2") {
                $('#TblToolModelBased').DataTable().$('.sumQty').each(function (index, Obj) {
                    var value = parseFloat($(this).text().replace(',', ''));
                    if (!isNaN(value)) amtTotal += value;
                });
            }
        }

        $('#TotalitemsQty').text(
           amtTotal.toFixed(2)
        );
    }

    function fnpanelSelect() {

        if ($("#MatTypddl").children("option:selected").val() == "1") {
            $("#RawMatpnl").show();
            $("#Toolpnl").hide();
        } else if ($("#MatTypddl").children("option:selected").val() == "2") {
            $("#RawMatpnl").hide();
            $("#Toolpnl").show();
            fnPurchaseTypeSelect();
        }
    }
    fnpanelSelect();
    $("#MatTypddl").change(function () {
        fnpanelSelect();
    });

    function fnPurchaseTypeSelect() {
        if ($("#MatTypddl").children("option:selected").val() == "1") {
            if ($("#purOdrTypeddl").children("option:selected").val() == "1") {
                $(".divSingleItemRm").show();
                $(".divModelBased").hide();
            }
            else if ($("#purOdrTypeddl").children("option:selected").val() == "2") {
                $(".divSingleItemRm").hide();
                $(".divModelBased").show();
                getModelForRM();
            }
        } else if ($("#MatTypddl").children("option:selected").val() == "2") {
            if ($("#toolpurOdrTypeddl").children("option:selected").val() == "1") {
                $(".divSingleItemTool").show();
                $(".divModelTool").hide();
            }
            else if ($("#toolpurOdrTypeddl").children("option:selected").val() == "2") {
                $(".divSingleItemTool").hide();
                $(".divModelTool").show();
                getModelForTool();
            }
        }
    }
    fnPurchaseTypeSelect();
    $("#purOdrTypeddl").change(function () {
        fnPurchaseTypeSelect();
    });
    $("#toolpurOdrTypeddl").change(function () {
        fnPurchaseTypeSelect();
    });

    //Raw Material Madel
    function getModelForRM() {
        $.ajax({
            type: "POST",
            url: "Trans_Enercon_PurReq_Sub.aspx/GetModel",
            //data: '{MatType:"' + mattype + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                $('#GenModelddl').empty();
                $('#GenModelddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#GenModelddl').append($('<option>').text(value.GenName).attr('value', value.GenCode));
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#GenModelddl .form-error').html(errorThrown);
                $('#GenModelddl .form-error').css("color", "Red");
            }
        });
    }

    //Tool Model
    function getModelForTool() {
        $.ajax({
            type: "POST",
            url: "Trans_Enercon_PurReq_Sub.aspx/GetModel",
            //data: '{MatType:"' + mattype + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                $('#ToolGenModelddl').empty();
                $('#ToolGenModelddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#ToolGenModelddl').append($('<option>').text(value.GenName).attr('value', value.GenCode));
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#ToolGenModelddl .form-error').html(errorThrown);
                $('#ToolGenModelddl .form-error').css("color", "Red");
            }
        });
    }

    $('#GenModelddl').change(function (e) {
        $.ajax({
            type: "POST",
            url: "Trans_Enercon_PurReq_Sub.aspx/GetModelBasedPart",
            data: '{GenModelNo:"' + $(this).val() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#GenProdpart').empty();
                $('#GenProdpart').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $('#GenProdpart').append($('<option>').text("ALL").attr('value', 'ALL'));
                $.each(response.d, function (i, value) {
                    $('#GenProdpart').append($('<option>').text(value.partName).attr('value', value.partNo));
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#GenProdpart .form-error').html(errorThrown);
                $('#GenProdpart .form-error').css("color", "Red");
            }
        });
    });

    $('#ToolGenModelddl').change(function (e) {
        $.ajax({
            type: "POST",
            url: "Trans_Enercon_PurReq_Sub.aspx/GetModelBasedPart",
            data: '{GenModelNo:"' + $(this).val() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#ToolGenProdpart').empty();
                $('#ToolGenProdpart').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $('#ToolGenProdpart').append($('<option>').text("ALL").attr('value', 'ALL'));
                $.each(response.d, function (i, value) {
                    $('#ToolGenProdpart').append($('<option>').text(value.partName).attr('value', value.partNo));
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#ToolGenProdpart .form-error').html(errorThrown);
                $('#ToolGenProdpart .form-error').css("color", "Red");
            }
        });
    });

    $('#GenProdpart').change(function (e) {
        var Table = $('#TblRmModelBased').DataTable();
        var err = false;
        var ModelQtyVal = $('#GenModelQtytxt').val();
        var GenReqDate = $('#GenReqDatetxt').val();
        var GenModelNo = $('#GenModelddl').val();
        $('.divModelBased input[type="text"]').each(
    function (index) {
        var input = $(this);
        var span = $(input).next('span');
        input.attr('style', '');
        span.text('');
        if (input.val() == "" || input.val() == "0") {
            span.text('*This Field is Required');
            span.css('color', 'red');
            input.attr('style', 'border-color:red');
            err = true;
        }
    })
        if (!err) {
            $.ajax({
                type: "POST",
                url: "Trans_Enercon_PurReq_Sub.aspx/GetGenModelProductItems",
                data: "{'GenModelNo':'" + GenModelNo + "','ProductModelNo':'" + $(this).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $('#GenProdpart .form-error').html(errorThrown);
                    $('#GenProdpart .form-error').css("color", "Red");
                }
            });
        }
    });
    function OnSuccess(response) {

        $('#TblRmModelBased').DataTable().clear();
        var length = Object.keys(response.d).length;
        var j = 0;
        if (length > 0) {
            function blockLoop() {
                var row = response.d[j];
                $('#TblRmModelBased').DataTable().row.add([
                        "",
                           row.SApNo,
                           row.Matname,
                           row.UomTex,
                           "<input type='text'  value=" + row.ReqQty + " class='form-control txtReqQtyEdit' style='width:50%' />",
                           "<input type='text'  value=" + $('#GenModelQtytxt').val() + " class='form-control txtModelQtyEdit' style='width:50%' />",
                          "<span  class='sumQty'>" + ((row.ReqQty) * ($('#GenModelQtytxt').val())) + "</span>",
                           $('#GenReqDatetxt').val(),
                          "<input type='text'  value=" + $('#GenCalenderWeektxt').val() + " class='form-control txtCalenderWeek' style='width:50%' />",
                           $('#GenRemarktxt').val(),
                           "<i class='btn-danger btn-sm fa fa-trash-o delete'></i>"

                ]).draw(true);
                j++;
                if (j < length) {
                    setTimeout(function () { blockLoop(); }, (length / 50).toFixed(0));

                } else {
                    ClearSub();
                    return;
                }
            }
            blockLoop();
        }
    }

    $('#ToolGenModelProPartddl').change(function (e) {
        var Table = $('#TblToolModelBased').DataTable();
        var err = false;
        var ModelQtyVal = $('#ToolGenReqQtytxt').val();
        var GenReqDate = $('#ToolGenReqDatetxt').val();
        var GenModelNo = $('#ToolGenModelddl').val();
        $('.divModelTool input[type="text"]').each(
    function (index) {
        var input = $(this);
        var span = $(input).next('span');
        input.attr('style', '');
        span.text('');
        if (input.val() == "" || input.val() == "0") {
            span.text('*This Field is Required');
            span.css('color', 'red');
            input.attr('style', 'border-color:red');
            err = true;
        }
    })
        if (!err) {
            $.ajax({
                type: "POST",
                url: "Trans_Enercon_PurReq_Sub.aspx/GetGenModelToolItems",
                data: "{'GenModelNo':'" + GenModelNo + "','ProductModelNo':'" + $(this).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccessToolModel,
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $('#ToolGenProdpart .form-error').html(errorThrown);
                    $('#ToolGenProdpart .form-error').css("color", "Red");
                }
            });
        }
    });

    function OnSuccessToolModel(response) {

        $('#TblToolModelBased').DataTable().clear();
        var length = Object.keys(response.d).length;
        var j = 0;
        if (length > 0) {
            function blockLoopTool() {
                var row = response.d[j];
                $('#TblToolModelBased').DataTable().row.add([
                        "",
                           row.SApNo,
                           row.Matname,
                           row.UomTex,
                           "<input type='text'  value=" + row.ReqQty + " class='form-control txtReqQtyEdit' style='width:50%' />",
                           "<input type='text'  value=" + $('#ToolGenModelQtytxt').val() + " class='form-control txtModelQtyEdit' style='width:50%' />",
                          "<span  class='sumQty'>" + ((row.ReqQty) * ($('#ToolGenModelQtytxt').val())) + "</span>",
                           $('#ToolGenReqDatetxt').val(),
                         "<input  type='text' value=" + $('#ToolGenCalenderWeektxt').val() + " class='form-control txtCalenderWeek' style='width:50%' />",
                           $('#ToolGenRemarktxt').val(),
                           "<i class='btn-danger btn-sm fa fa-trash-o delete'></i>"

                ]).draw(true);
                j++;
                if (j < length) {
                    setTimeout(function () { blockLoopTool(); }, (length / 50).toFixed(0));

                } else {
                    ClearSub();
                    return;
                }
            }
            blockLoopTool();
        }
    }



    //var tabledeleteRM = $('#TblRmSingleItem').DataTable();
    $('#TblRmSingleItem tbody').on('click', '.delete', function () {
        //  alert('');
        var rs = confirm('Are you sure want to delete this details!!!');
        if (rs == true) {
            $('#TblRmSingleItem').DataTable()
                .row($(this).parents('tr'))
                .remove()
                .draw();
            rowCountRmItemTbl--;
        }
    });

    //var table = $('#TblToolSingleItem').DataTable();
    $('#TblToolSingleItem tbody').on('click', '.delete', function () {
        var rs = confirm('Are you sure want to delete this details!!!');
        if (rs == true) {
            $('#TblToolSingleItem').DataTable()
                .row($(this).parents('tr'))
                .remove()
                .draw();
            rowCountRmItemTbl--;
        }
    });
    //var table = $('#TblToolSingleItem').DataTable();
    $('#TblRmModelBased tbody').on('click', '.delete', function () {
        var rs = confirm('Are you sure want to delete this details!!!');
        if (rs == true) {
            var table = $('#TblRmModelBased').DataTable()
                .row($(this).parents('tr'))
                .remove()
                .draw();
            //console.log(table.rows().count());
            rowCountRmItemTbl--;
        }
    });
    $('#TblToolModelBased tbody').on('click', '.delete', function () {
        var rs = confirm('Are you sure want to delete this details!!!');
        if (rs == true) {
            $('#TblToolModelBased').DataTable()
                .row($(this).parents('tr'))
                .remove()
                .draw();
            rowCountRmItemTbl--;
        }
    });



    $.ajax({
        type: "POST",
        url: "Trans_Enercon_PurReq_Sub.aspx/GetLoadMatSAP",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#SapNoddl').empty();
            $('#MatNameddl').empty();
            $('#BOMCodeddl').empty();
            $('#SapNoddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
            $('#MatNameddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
            $('#BOMCodeddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
            $.each(response.d, function (i, value) {
                $('#SapNoddl').append($('<option>').text(value.SAPNo).attr('value', value.ItemCode));
                $('#MatNameddl').append($('<option>').text(value.ItemName).attr('value', value.ItemCode));
                $('#BOMCodeddl').append($('<option>').text(value.ItemCode).attr('value', value.ItemCode));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#SapNoddl .form-error').html(errorThrown);
            $('#SapNoddl .form-error').css("color", "Red");
        }
    });

    $.ajax({
        type: "POST",
        url: "Trans_Enercon_PurReq_Sub.aspx/GetLoadToolSAP",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#ToolSapNoddl').empty();
            $('#ToolItemNameddl').empty();
            $('#ToolItemCTNoddl').empty();
            $('#ToolSapNoddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
            $('#ToolItemNameddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
            $('#ToolItemCTNoddl').append($('<option>').text("-Select-").attr('value', '-Select-'));
            $.each(response.d, function (i, value) {
                $('#ToolSapNoddl').append($('<option>').text(value.SAPNo).attr('value', value.ItemCode));
                $('#ToolItemNameddl').append($('<option>').text(value.ItemName).attr('value', value.ItemCode));
                $('#ToolItemCTNoddl').append($('<option>').text(value.ToolCode).attr('value', value.ItemCode));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#ToolSapNoddl .form-error').html(errorThrown);
            $('#ToolSapNoddl .form-error').css("color", "Red");
        }
    });


    $('#SapNoddl').change(function (e) {
        var value = $(this).val();
        $('#MatNameddl').val(value);
        $('#MatNameddl').select2().trigger('change.select2');
        $('#BOMCodeddl').val(value);
        $('#BOMCodeddl').select2().trigger('change.select2');
        GetItemReqQty($(this).val());
    });

    $('#MatNameddl').change(function (e) {
        var value = $(this).val();
        $('#SapNoddl').val(value);
        $('#SapNoddl').select2().trigger('change.select2');
        $('#BOMCodeddl').val(value);
        $('#BOMCodeddl').select2().trigger('change.select2');
        GetItemReqQty($(this).val());
    });

    $('#BOMCodeddl').change(function (e) {
        var value = $(this).val();
        $('#MatNameddl').val(value);
        $('#MatNameddl').select2().trigger('change.select2');
        $('#SapNoddl').val(value);
        $('#SapNoddl').select2().trigger('change.select2');
        GetItemReqQty($(this).val());
    });

    $('#ToolSapNoddl').change(function (e) {
        var value = $(this).val();
        $('#ToolItemNameddl').val(value);
        $('#ToolItemNameddl').select2().trigger('change.select2');
        $('#ToolItemCTNoddl').val(value);
        $('#ToolItemCTNoddl').select2().trigger('change.select2');
        GetItemReqQty($(this).val());
    });
    $('#ToolItemNameddl').change(function (e) {
        var value = $(this).val();
        $('#ToolSapNoddl').val(value);
        $('#ToolSapNoddl').select2().trigger('change.select2');
        $('#ToolItemCTNoddl').val(value);
        $('#ToolItemCTNoddl').select2().trigger('change.select2');
        GetItemReqQty($(this).val());
    });
    $('#ToolItemCTNoddl').change(function (e) {
        var value = $(this).val();
        $('#ToolSapNoddl').val(value);
        $('#ToolSapNoddl').select2().trigger('change.select2');
        $('#ToolItemNameddl').val(value);
        $('#ToolItemNameddl').select2().trigger('change.select2');
        GetItemReqQty($(this).val());
    });

    function GetItemReqQty(val) {
        $.ajax({
            type: "POST",
            url: "Trans_Enercon_PurReq_Sub.aspx/GetLoadItemQty",
            data: '{itemCode:"' + val + '",MatType:"' + $("#MatTypddl").val() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $.each(response.d, function (i, value) {
                    if ($("#MatTypddl").val() == "1") {
                        $('#ReqQtytxt').val(value.Qty);
                        $('#SingleUOMCode').val(value.UOMCode);
                        $('#SingleUOMName').val(value.UOMName);
                        $('#ReqQtytxt').trigger('keyup');
                    } else if ($("#MatTypddl").val() == "2") {
                        $('#ToolReqqtytxt').val(value.Qty);
                        $('#SingleUOMCodeTool').val(value.UOMCode);
                        $('#SingleUOMNameTool').val(value.UOMName);
                        //$('#ToolReqqtytxt').trigger('keyup');
                    }

                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if ($("#MatTypddl").val() == "1") {
                    $('#ReqQtytxt .form-error').html(errorThrown);
                    $('#ReqQtytxt .form-error').css("color", "Red");
                } else if ($("#MatTypddl").val() == "2") {
                    $('#ToolReqqtytxt .form-error').html(errorThrown);
                    $('#ToolReqqtytxt .form-error').css("color", "Red");
                }
            }
        });
    }

    $('#modelQty').keyup(function () {
        $('#sumQtxt').val(CalcFunction.Mul($(this).val(), $('#ReqQtytxt').val()).toFixed(2));
        $('#sumQtxt').text(CalcFunction.Mul($(this).val(), $('#ReqQtytxt').val()).toFixed(2));
        $(this).attr('style', '');
        $('#sumQtxt').attr('style', '');

    });
    $('#ReqQtytxt').keyup(function () {
        $('#sumQtxt').val(CalcFunction.Mul($(this).val(), $('#modelQty').val()).toFixed(2));
        $('#sumQtxt').text(CalcFunction.Mul($(this).val(), $('#modelQty').val()).toFixed(2));
        $(this).attr('style', '');
        $('#sumQtxt').attr('style', '');
    });

    //$('#ToolReqqtytxt').keyup(function () {
    //    $('#ToolsumQtxt').val(CalcFunction.Mul($(this).val(), $('#ToolmodelQty').val()).toFixed(2));
    //    $('#ToolsumQtxt').text(CalcFunction.Mul($(this).val(), $('#ToolmodelQty').val()).toFixed(2));
    //    $(this).attr('style', '');
    //    $('#ToolsumQtxt').attr('style', '');

    //});
    //$('#ToolmodelQty').keyup(function () {
    //    $('#ToolsumQtxt').val(CalcFunction.Mul($(this).val(), $('#ToolReqqtytxt').val()).toFixed(2));
    //    $('#ToolsumQtxt').text(CalcFunction.Mul($(this).val(), $('#ToolReqqtytxt').val()).toFixed(2));
    //    $(this).attr('style', '');
    //    $('#ToolsumQtxt').attr('style', '');
    //});


    $('#additembtnRm').click(function (e) {
        var err = false;

        $('.divSingleItemRm .req').each(function (index) {
            var input = $(this);
            var span = $(input).next('.form-error');
            // input.attr('style', '');
            // span.text('');
            if (input.val() == "" || input.val() == "0" || input.val() == "-Select-") {
                span.html('*This Field is Required');
                span.css('color', 'red');
                input.attr('style', 'border-color:red');
                err = true;
            }
        })


        $('.divSingleItemRm .selectreq').each(
        function (index) {
            if ($(this).children("option:selected").val() == "-Select-") {
                // console.log("hai");
                err = true;
                var sn = $(this).next('p');
                sn.text("*This Field is Required");
                sn.css('color', 'red');
            }
        })
        if (!err) {
            addRMSingleNewItemtoTable(e);


        }

    });

    function addRMSingleNewItemtoTable() {
        var err = false;
        var table = $('#TblRmSingleItem').DataTable();
        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if ($('#SapNoddl').children('option:selected').text() == data[1]) {
                err = true;
                alert('This Item Already in a table');
            }
        });
        if (!err) {
            table.row.add([
                "",
                   $('#SapNoddl').children('option:selected').text(),
                   $('#MatNameddl').children('option:selected').text(),
                   $('#SingleUOMName').val(),
                   "<input type='text'  value=" + $('#ReqQtytxt').val() + " class='form-control txtReqQtyEdit' style='width:50%' />",
                   "<input type='text'  value=" + $('#modelQty').val() + " class='form-control txtModelQtyEdit' style='width:50%' />",
                  "<span  class='sumQty'>" + CalcFunction.Mul($('#ReqQtytxt').val(), $('#modelQty').val()) + "</span>",
                   $('#ReqDate').val(),
                   "<input  type='text' value=" + $('#CalenderWeektxt').val() + " class='form-control txtCalenderWeek' style='width:50%' />",
                   $('#Remark').val(),
                   "<i class='btn-danger btn-sm fa fa-trash-o delete'></i>"
            ]).draw(true);
        }
    }


    $('#Tooladditembtn').click(function (e) {
        var err = false;
        $('.divSingleItemTool .req').each(function (index) {
            var input = $(this);
            var span = $(input).next('span');
            input.attr('style', '');
            span.text('');
            if (input.val() == "" || input.val() == "0") {
                span.text('*This Field is Required');
                span.css('color', 'red');
                input.attr('style', 'border-color:red');
                err = true;
            }
        })


        $('.divSingleItemTool select').each(
        function (index) {
            if ($(this).children("option:selected").val() == "-Select-") {

            }
        })
        if (!err) {
            addToolSingleNewItemtoTable(e);
        }

    });

    function addToolSingleNewItemtoTable() {
        var err = false;
        var table = $('#TblToolSingleItem').DataTable();
        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if ($('#ToolSapNoddl').children('option:selected').text() == data[1]) {
                err = true;
                alert('This Item Aready in a table');
            }
        });
        if (!err) {
            table.row.add([
                "",
                "<span  class='form-control' style='border: none;width:100%'>" + $('#ToolItemCTNoddl').children('option:selected').text() + "</span>",
                "<span  class='form-control' style='border: none;width:100%'>" + $('#ToolSapNoddl').children('option:selected').text() + "</span>",
                "<span  class='form-control' style='border: none;width:100%'>" + $('#ToolItemNameddl').children('option:selected').text() + "</span>",
                "<span  class='form-control' style='border: none;width:100%'>" + $('#SingleUOMNameTool').val() + "</span>",
                "<input type='text'  value=" + $('#ToolsumQtxt').val() + " class='form-control sumQty' style='width:75%'/>",
                "<span  class='form-control' style='border: none;width:100%'>" + $('#ToolReqDate').val() + "</span>",
                "<input type='text' class='form-control txtCalenderWeek' value='" + $('#ToolCalenderWeektxt').val() + "' style='width:75%'/>",
                "<span  class='form-control' style='border: none;width:100%'>" + $('#ToolRemark').val() + "</span>",
                "<i class='btn-danger btn-sm fa fa-trash-o delete'></i>"
            ]).draw(true);
            //Clear the sub Field
            ClearSub();
        }
    }

    CalcFunction = {
        Sum: function (x, y) {
            return x + y;
        },
        Sub: function (x, y) {
            return x - y;
        },
        Div: function (x, y) {
            return x / y;
        },
        Mul: function (x, y) {
            return x * y;
        }
    }

});


function SaveMsgAlert(msg) {
    alert(msg);
}

//On UpdatePanel Refresh
var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    //alert("DatePicker");
    prm.add_endRequest(function (sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.datepicker').datepicker({ format: 'dd/MM/yyyy', autoclose: true });
            $('.select2').select2();

            $("#example").DataTable({
                stateSave: true,
                "drawCallback": function (settings) {
                    $('.delete').click(function () {
                        var confm = confirm("Are you sure Want to delete this Details");
                        if (confm == true) {
                            var id = $(this).closest("tr").find('td:eq(2)').text();
                            alert(id);
                        }
                    });
                    $('.txtReqQtyEdit').keyup(function () {
                        var preVal = $(this).closest("tr").find('td:eq(10)').text();
                        var multipleVal = $(this).closest("tr").find('td:eq(9)').text();
                        var newVal = ($(this).val() * multipleVal);
                        $(this).closest("tr").find('td:eq(10)').text(newVal.toFixed(2));
                    });
                }
            });

        }
    });
};

//Single Raw Material Save Function
function RmSingleItemGetTableValues() {
    //Create an Array to hold the Table values.
    var err = false;
    var lists = new Array();
    var table = $('#TblRmSingleItem').DataTable();
    var lenght = table.rows().count();

    if (lenght <= 0) {
        alert('Please Add Atleast one Sub item');
        err = true;
        return;
    }
    if (!err) {

        $("#TblRmSingleItem tbody tr").each(function () {
            var currentRow = $(this);
            var list = {};
            list.SAPNo = currentRow.find("td:eq(1)").text();
            list.ItemName = currentRow.find("td:eq(2)").text();
            list.UOMCode = currentRow.find("td:eq(3)").text();
            list.RequiredQty = currentRow.find('.txtReqQtyEdit').val();
            list.ModelQty = currentRow.find('.txtModelQtyEdit').val();
            list.SumQty = currentRow.find('.sumQty').text();
            list.RequiredDate = currentRow.find("td:eq(7)").text();
            list.CalenderWeek = currentRow.find('.txtCalenderWeek').val();
            list.Remarks = currentRow.find("td:eq(9)").text();
            lists.push(list);

        });
        console.log(lists);

        //Convert the JSON object to string and assign to Hidden Field.
        document.getElementsByName("MatsingleItemsJSON")[0].value = JSON.stringify(lists);
        SaveApi(JSON.stringify(lists));

        //clearMain();
        //table.clear();
        //table.draw(true);

    }
}


//Model Based Raw Material Save Function
function RmModelBasedGetTableValues() {
    //  alert();
    //Create an Array to hold the Table values.
    var err = false;
    var lists = new Array();
    //Reference the Table.
    var table = $("#TblRmModelBased").DataTable();
    var lenght = table.rows().count();

    if (lenght <= 0) {
        alert('Please Add Atleast one Sub item');
        err = true;
        return;
    }
    if (!err) {

        $("#TblRmModelBased tbody tr").each(function () {
            var currentRow = $(this);
            var list = {};
            list.SAPNo =currentRow.find("td:eq(1)").text();
            list.ItemName =currentRow.find("td:eq(2)").text();
            list.UOMCode = currentRow.find("td:eq(3)").text();
            list.RequiredQty = currentRow.find('.txtReqQtyEdit').val();
            list.ModelQty = currentRow.find('.txtModelQtyEdit').val();
            list.SumQty = currentRow.find('.sumQty').text();
            list.RequiredDate = currentRow.find("td:eq(7)").text();
            list.CalenderWeek = currentRow.find('.txtCalenderWeek').val();
            list.Remarks = currentRow.find("td:eq(9)").text();
            lists.push(list);

        });
        //Convert the JSON object to string and assign to Hidden Field.
         document.getElementsByName("MatModelBasedJSON")[0].value = JSON.stringify(lists);
         SaveApi(JSON.stringify(lists));
    }
}

//Tool Single Item Save Function
function ToolSingleItemGetTableValues() {
    //  alert();
    //Create an Array to hold the Table values.
    var lists = new Array();
    var err = false;
    //Reference the Table.
    var table = $("#TblToolSingleItem").DataTable();
    var lenght = table.rows().count();

    if (lenght <= 0) {
        alert('Please Add Atleast one Sub item');
        err = true;
        return;
    }
    if (!err) {

        $("#TblToolSingleItem tbody tr").each(function () {
            var currentRow = $(this);
            var list = {};
            list.RefNo = currentRow.find("td:eq(1)").text();
            list.SAPNo = currentRow.find("td:eq(2)").text();
            list.ItemName = currentRow.find("td:eq(3)").text();
            list.UOMCode = currentRow.find("td:eq(4)").text();
            list.SumQty = currentRow.find('.sumQty').val();
            list.RequiredDate = currentRow.find("td:eq(6)").text();
            list.CalenderWeek = currentRow.find('.txtCalenderWeek').val();
            list.Remarks = currentRow.find("td:eq(8)").text();
            lists.push(list);

        });

        alert(JSON.stringify(lists))
        //Convert the JSON object to string and assign to Hidden Field.
        document.getElementsByName("ToolsingleItemsJSON")[0].value = JSON.stringify(lists);
        SaveApi(JSON.stringify(lists));
        
    }
}


//Tool Model Save Function
function ToolModelBasedGetTableValues() {
    //  alert();
    //Create an Array to hold the Table values.
    var lists = new Array();
    var err = false;
    //Reference the Table.
    var table = $("#TblToolModelBased").DataTable();

    var lenght = table.rows().count();

    if (lenght <= 0) {
        alert('Please Add Atleast one Sub item');
        err = true;
        return;
    }
    if (!err) {

        $("#TblToolModelBased tbody tr").each(function () {
            var currentRow = $(this);
            var list = {};
            list.SAPNo = currentRow.find("td:eq(1)").text();
            list.ItemName = currentRow.find("td:eq(2)").text();
            list.UOMCode = currentRow.find("td:eq(3)").text();
            list.RequiredQty = currentRow.find('.txtReqQtyEdit').val();
            list.ModelQty = currentRow.find('.txtModelQtyEdit').val();
            list.SumQty = currentRow.find('.sumQty').text();
            list.RequiredDate = currentRow.find("td:eq(7)").text();
            list.CalenderWeek = currentRow.find('.txtCalenderWeek').val();
            list.Remarks = currentRow.find("td:eq(9)").text();
            lists.push(list);   

        });
        //Convert the JSON object to string and assign to Hidden Field.
        document.getElementsByName("ToolModelBasedJSON")[0].value = JSON.stringify(lists);
        SaveApi(JSON.stringify(lists));

    }
}

function ClearControl() {
    alert('Clear')
}

//Item Save API
function SaveApi(data) {
    console.log(data);
    var matType = $('#MatTypddl').children('option:selected').val();
    var reqDate = $('#TransDatetxt').val();
    var reqNo = $('#PurReqNotxt').text();
    var deptVal = $('#deptddl').children('option:selected').val();
    var description = $('#txtDescription').val();
    var SumQty = $('#TotalitemsQty').text();
    var MatItem = "";
    if (matType == "1") {
        MatItem = $('#purOdrTypeddl').children('option:selected').val();
    }
    else if (matType == "2") {
        MatItem = $('#toolpurOdrTypeddl').children('option:selected').val();
    }
    var SaveType = $('#hiddenSaveType').val();
    $.ajax({
        type: "POST",
        url: "Trans_Enercon_PurReq_Sub.aspx/SaveItems",
        data: "{'reqNo':'" + reqNo + "','SaveType':'" + SaveType + "','sumQty':'" + SumQty + "','description':'" + description + "','deptVal':'" + deptVal + "','reqDate':'" + reqDate + "','mattype':'" + matType + "','purType':'" + MatItem + "','data':'" + data + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d == "true") {
                alert('Details Saved Successfully');
                ClearAllData();
                if (SaveType == "2") {
                    window.close();
                }
                else {
                    //window.location.href = "Trans_Enercon_PurReq_Main.aspx";
                    window.location.replace("Trans_Enercon_PurReq_Main.aspx");
                }
            }
        },
        failure: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}
//Clear the Main Fields
function clearMain() {
    $('#Mainpnl .form-control').each(function (index, element) {
        if ($(element).is('select')) {
            $(element).prop('selectedIndex', 0);
            $(element).select2().trigger('change.select2');
        } else {
            $(element).val("");
            $(element).text("");
            $(element).attr('style', '');
            $(element).next('span').text();
        }
    });
}
function ClearSub() {

    $('.divSingleItemTool .form-control ').each(function (index, element) {
        if ($(element).parents().is(".divTable")) {
            return false;
        }
        if ($(element).is('select')) {
            $(element).prop('selectedIndex', 0);
            $(element).select2().trigger('change.select2');
        } else {
            $(element).val("");
            $(element).text("");
            $(element).attr('style', '');
            $(element).next('span').text();
        }
    });
    $('.divSingleItemRm .form-control ').each(function (index, element) {
        if ($(element).parents().is(".divTable")) {
            return false;
        }
        if ($(element).is('select')) {
            $(element).prop('selectedIndex', 0);
            $(element).select2().trigger('change.select2');
        } else {
            $(element).val("");
            $(element).text("");
            $(element).attr('style', '');
            $(element).next('span').text();
        }
    });

    $('.divModelBased .form-control ').each(function (index, element) {
        if ($(element).parents().is(".divTable")) {
            return false;
        }
        if ($(element).is('select')) {
            $(element).prop('selectedIndex', 0);
            $(element).select2().trigger('change.select2');
        } else {
            $(element).val("");
            $(element).text("");
            $(element).attr('style', '');
            $(element).next('span').text();
        }
    });

    $('.divModelTool .form-control ').each(function (index, element) {
        if ($(element).parents().is(".divTable")) {
            return false;
        }
        if ($(element).is('select')) {
            $(element).prop('selectedIndex', 0);
            $(element).select2().trigger('change.select2');
        } else {
            $(element).val("");
            $(element).text("");
            $(element).attr('style', '');
            $(element).next('span').text();
        }
    });
}
function ClearAllData() {
    clearMain();
    ClearSub();

    var table = $('.table').DataTable();
    table.clear();
    table.draw(true);
}
