﻿$(document).ready(function () {

    var rowCountRmItemTbl = 0;

    var saveType;
    $("#TblItemDet").DataTable({
        stateSave: false,
        "processing": true,
        "bPaginate": false,
        "bLengthChange": false, "bFilter": true, "bInfo": false, "bAutoWidth": false,
        "columnDefs": [
            {
                "targets": [2],
                "visible": false
            },

            {
                "targets": [7],
                "visible": false
            },
            {
                "targets": [11],
                "visible": false
            },
            {
                "targets": [13],
                "visible": false
            },
            {
                "targets": [15],
                "visible": false
            }
        ],


        //"ordering":false,


        "fnRowCallback": function (nRow, aData, iDisplayIndex, iPageIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            CalcFooter();
            return nRow;
        },
        "drawCallback": function () {
            $('.txtOrdQtyEdit').keyup(function () {
                if ($("#ddlCurrencyType option:selected").text() == "INR") {
                    var OrdQty = $(this).closest('tr').find('.txtOrdQtyEdit').val();
                    var ItmAmt = ($(this).closest("tr").find('.txtINREdit').val() * OrdQty);
                    $(this).closest("tr").find('td:eq(8)').html(ItmAmt.toFixed(2));

                    var row = $(this).closest('tr');
                    var tbl = $("#TblItemDet").DataTable();

                    if ($("#ddlTaxType").children('option:selected').val() == "GST") {
                        var CGSTP = tbl.row(row).data()[11];
                        var SGSTP = tbl.row(row).data()[13];
                        var resultCGST = ((ItmAmt * (CGSTP)) / 100);
                        var resultSGST = ((ItmAmt * (SGSTP)) / 100);
                        $(this).closest("tr").find('td:eq(9)').text(resultCGST.toFixed(2));
                        $(this).closest("tr").find('td:eq(10)').text(resultSGST.toFixed(2));
                        $(this).closest("tr").find('td:eq(12)').text(((resultSGST) + (resultCGST) + (ItmAmt)).toFixed(2));
                    }
                    else if ($("#ddlTaxType").children('option:selected').val() == "IGST") {
                        var IGSTP = tbl.row(row).data()[15];

                        var resultIGST = ((ItmAmt * (IGSTP)) / 100);

                        $(this).closest("tr").find('td:eq(11)').text(resultIGST.toFixed(2));
                        $(this).closest("tr").find('td:eq(12)').text(((resultIGST) + (ItmAmt)).toFixed(2));
                    }
                }
                else {
                    var OrdQty = $(this).closest('tr').find('.txtOrdQtyEdit').val();
                    var ItmAmt = ($(this).closest("tr").find('.txtERUEdit').val() * OrdQty);
                    $(this).closest("tr").find('td:eq(8)').html(ItmAmt.toFixed(2));
                    $(this).closest("tr").find('td:eq(12)').html(ItmAmt.toFixed(2));
                    
                }
                CalcFooter();
            });

            $('.txtModelQtyEdit').keyup(function () {
                var RquQty = $(this).closest("tr").find('td:eq(4)').text();
                var ModelQty = $(this).closest("tr").find('.txtModelQtyEdit').val();
                var OdrQty = RquQty * ModelQty;
                $(this).closest("tr").find('.txtOrdQtyEdit').val(OdrQty.toFixed(2));

                if ($('#ddlCurrencyType').children('option:selected').val() == "INR") {

                    var OrdQty = $(this).closest('tr').find('.txtOrdQtyEdit').val();
                    var ItmAmt = ($(this).closest("tr").find('.txtINREdit').val() * OrdQty);
                    $(this).closest("tr").find('td:eq(8)').html(ItmAmt.toFixed(2));

                    var row = $(this).closest('tr');
                    var tbl = $("#TblItemDet").DataTable();

                    if ($("#ddlTaxType").children('option:selected').val() == "GST") {

                        var CGSTP = tbl.row(row).data()[11];
                        var SGSTP = tbl.row(row).data()[13];
                        var resultCGST = ((ItmAmt * (CGSTP)) / 100);
                        var resultSGST = ((ItmAmt * (SGSTP)) / 100);
                        $(this).closest("tr").find('td:eq(9)').text(resultCGST.toFixed(2));
                        $(this).closest("tr").find('td:eq(10)').text(resultSGST.toFixed(2));
                        $(this).closest("tr").find('td:eq(12)').text(((resultSGST) + (resultCGST) + (ItmAmt)).toFixed(2));
                    }
                    else if ($("#ddlTaxType").children('option:selected').val() == "IGST") {

                        alert("IGST")
                        var IGSTP = tbl.row(row).data()[15];

                        var resultIGST = ((ItmAmt * (IGSTP)) / 100);

                        $(this).closest("tr").find('td:eq(11)').text(resultIGST.toFixed(2));
                        $(this).closest("tr").find('td:eq(12)').text(((resultIGST) + (ItmAmt)).toFixed(2));
                    }
                }
                else {
                    var OrdQty = $(this).closest('tr').find('.txtOrdQtyEdit').val();
                    var ItmAmt = ($(this).closest("tr").find('.txtERUEdit').val() * OrdQty);
                    $(this).closest("tr").find('td:eq(8)').html(ItmAmt.toFixed(2));
                    $(this).closest("tr").find('td:eq(12)').html(ItmAmt.toFixed(2));
                }
                CalcFooter();
            });

            $('.txtINREdit').keyup(function () {
                var OrdQty = $(this).closest('tr').find('.txtOrdQtyEdit').val();
                var ItmAmt = ($(this).closest("tr").find('.txtINREdit').val() * OrdQty);
                $(this).closest("tr").find('td:eq(8)').html(ItmAmt.toFixed(2));

                var row = $(this).closest('tr');
                var tbl = $("#TblItemDet").DataTable();

                if ($("#ddlTaxType").children('option:selected').val() == "GST") {
                    var CGSTP = tbl.row(row).data()[11];
                    var SGSTP = tbl.row(row).data()[13];
                    var resultCGST = ((ItmAmt * (CGSTP)) / 100);
                    var resultSGST = ((ItmAmt * (SGSTP)) / 100);
                    $(this).closest("tr").find('td:eq(9)').text(resultCGST.toFixed(2));
                    $(this).closest("tr").find('td:eq(10)').text(resultSGST.toFixed(2));
                    $(this).closest("tr").find('td:eq(12)').text(((resultSGST) + (resultCGST) + (ItmAmt)).toFixed(2));
                }
                else if ($("#ddlTaxType").children('option:selected').val() == "IGST") {
                    var IGSTP = tbl.row(row).data()[15];

                    var resultIGST = ((ItmAmt * (IGSTP)) / 100);

                    $(this).closest("tr").find('td:eq(11)').text(resultIGST.toFixed(2));
                    $(this).closest("tr").find('td:eq(12)').text(((resultIGST) + (ItmAmt)).toFixed(2));
                }

                CalcFooter();
            });

            $('.txtERUEdit').keyup(function () {
                var OrdQty = $(this).closest('tr').find('.txtOrdQtyEdit').val();
                var ItmAmt = ($(this).closest("tr").find('.txtERUEdit').val() * OrdQty);
                $(this).closest("tr").find('td:eq(8)').html(ItmAmt.toFixed(2));
                $(this).closest("tr").find('td:eq(12)').html(ItmAmt.toFixed(2));
                CalcFooter();
            });

            //$('.txtReqQtyEdit').keyup(function () {
            //    var preVal = $(this).closest("tr").find('td:eq(6)').text();
            //    var multipleVal = $(this).closest("tr").find('.txtModelQtyEdit').val();
            //    var newVal = ($(this).val() * multipleVal);
            //    $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
            //    CalcFooter();
            //});
            //$('.txtModelQtyEdit').keyup(function () {
            //    var preVal = $(this).closest("tr").find('td:eq(6)').text();
            //    var multipleVal = $(this).closest("tr").find('.txtReqQtyEdit').val();
            //    var newVal = ($(this).val() * multipleVal);
            //    $(this).closest("tr").find('.sumQty').text(newVal.toFixed(2));
            //    CalcFooter();
            //});
            CalcFooter();
        }
    });
    $('#TblItemDet').on('draw.dt', function () {
        // alert();
        //$(this).closest("tr").find('.txtModelQtyEdit').attr('style', 'width:50%');
        //$(this).closest("tr").find('.txtReqQtyEdit').attr('style', 'width:50%');
    });
    function getSupplier() {

        $.ajax({
            type: "POST",
            url: "Trans_Escrow_PurOrd_Sub.aspx/GetSupplier",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#ddlSuppName').empty();
                $('#ddlSuppName').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#ddlSuppName').append($('<option>').text(value.SuppName).attr('value', value.SuppCode));
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#ddlSuppName .form-error').html(errorThrown);
                $('#ddlSuppName .form-error').css("color", "Red");
            }
        });
    }

    function getDepartment() {
        $.ajax({
            type: "POST",
            url: "Trans_Escrow_PurOrd_Sub.aspx/GetDepartment",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#ddlDeptName').empty();
                $('#ddlDeptName').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#ddlDeptName').append($('<option>').text(value.DeptName).attr('value', value.DeptCode));
                });
            },
            else: function (XMLHttpRequest, textstatus, errorThrown) {
                $('#ddlDeptName .form-error').html(errorThrown);
                $('#ddlDeptName .form-error').css("color", "Red");
            }
        });
    }

    function getDeliveryMode() {
        $.ajax({
            type: "POST",
            url: "Trans_Escrow_PurOrd_Sub.aspx/GetDeliveryMode",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#ddlDeliveryMode').empty();
                $('#ddlDeliveryMode').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#ddlDeliveryMode').append($('<option>').text(value.DeliveryMode).attr('value', value.DeliveryCode));
                });
            },
            else: function (XMLHttpRequest, textstatus, errorThrown) {
                $('#ddlDeliveryMode .form-error').html(errorThrown);
                $('#ddlDeliveryMode .form-error').css("color", "Red");
            }
        });
    }

    function EditPurOudNo() {

        var PurOrdNo = $('#lblPurOrdNo').text();

        $.ajax({
            type: "POST",
            url: "Trans_Escrow_PurOrd_Sub.aspx/GetEditPurOrd",
            data: '{PurOrdNo:"' + PurOrdNo + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#ddlRequestNo').empty();
                $('#ddlRequestNo').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#ddlRequestNo').append($('<option>').text(value.PurRquNo).attr('value', value.PurRquNo));
                });
            },
            error: function (XMLHttprequest, textstatus, errorthrown) {
                $('#ddlRequestNo .form-error').html(errorthrown);
                $('#ddlRequestNo .form-error').css("color", "Red");
            }

        });
    }

    function getPurRquNo() {
        //alert($('#hiddenSaveType').val());

        $.ajax({
            type: "POST",
            url: "Trans_Escrow_PurOrd_Sub.aspx/GetPurRquNos",
            data: '{MatType:"' + $('#ddlMatTyp').children('option:selected').text() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#ddlRequestNo').empty();
                $('#ddlRequestNo').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#ddlRequestNo').append($('<option>').text(value.PurRquNo).attr('value', value.PurRquNo));
                });
            },
            error: function (XMLHttprequest, textstatus, errorthrown) {
                $('#ddlRequestNo .form-error').html(errorthrown);
                $('#ddlRequestNo .form-error').css("color", "Red");
            }
        });
    }

    getSupplier();
    getDepartment();
    getDeliveryMode();
    getPurRquNo();

    $('#ddlSuppName').change(function (e) {

        $.ajax({
            type: "POST",
            url: "Trans_Escrow_PurOrd_Sub.aspx/Suppliers",
            data: '{SuppName:"' + $('#ddlSuppName').children('option:selected').text() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $.each(response.d, function (i, value) {
                    //$("#lblSuppCode").val(value.SuppCode);
                    $("#txtPayTerms").val(value.TC);
                });
            }
        });
    });



    $('#ddlRequestNo').change(function (e) {

        var SaveType = $('#hiddenSaveType').val();

        if (SaveType == "2") {

            alert(SaveType)
            $.ajax({
                type: "POST",
                url: "Trans_Escrow_PurOrd_Sub.aspx/EditSelPurRquNo",
                data: '{PurOrdNo:"' + $('#lblPurOrdNo').text() + '",PurRquNo:"' + $('#ddlRequestNo').children('option:selected').text() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != "") {
                        $("#txtReqDate").html(response.d);
                    }
                    getItemDetails();
                }
            });
        }
        else {
            $.ajax({
                type: "POST",
                url: "Trans_Escrow_PurOrd_Sub.aspx/SelPurRquNo",
                data: '{PurRquNo:"' + $('#ddlRequestNo').children('option:selected').text() + '"}',
                //data: '{SuppName:"' + $('#ddlSuppName').children('option:selected').text() + '",PurRquNo:"' + $('#ddlRequestNo').children('option:selected').text() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != "") {
                        $("#txtReqDate").html(response.d);
                    }
                    getItemDetails();
                }
            });
        }
    });

    function TblRowChecked() {
        var Rlist = new Array();
        var err = false;
        var Tab = $('#TblItemDet').DataTable();
        var RowCount = Tab.row().count();
        console.log(RowCount);
        if (RowCount <= 0) {
            alert('Please Add Atleast one Sub item');
            err = true;
            return;
        }

        if (!err) {
            Tab.row().every(function (rowIdx, tableLoop, rowLoop) {
                var row = this.data();
                var list = {};
                list.SAPNo = $('.lblSAPNo').html(row[1]).text();
                list.ItemName = row[2];
                list.ModelQty = $('.txtModelQtyEdit').html(row[3]).val();
                list.RequiredQty = row[4];
                list.OrderQty = $('.txtOrdQtyEdit').html(row[5]).val();
                list.ReuiredDate = row[6];
                list.Rate_INR = $('.txtINREdit').html(row[7]).val();
                list.Rate_Other = $('.txtERUEdit').html(row[8]).val();
                list.ItemAmt = row[9];
                list.CGSTAmt = row[10];
                list.SGSTAmt = row[11];
                list.IGSTAmt = row[12];
                list.NetAmt = row[13];
                list.Calweek = row[14];
                Rlist.push(list);
            });
            return JSON.stringify(Rlist);
        }
    }

    $('#txtRoundOff').change(function (e) {
        var NetAmt = $('#lblTotAmt').text();
        var AOL = $('#txtRoundOff').val();
        var arr = NetAmt.split(".");

        if (parseFloat(arr[1]) < parseFloat("49")) {

            $('#lblFinNetAmt').html((parseFloat(NetAmt) - parseFloat(AOL)).toFixed(2));
        }
        else if (parseFloat(arr[1]) > parseFloat("49")) {
            $('#lblFinNetAmt').html((parseFloat(NetAmt) + parseFloat(AOL)).toFixed(2));
        }
    });

    $('#ddlCurrencyType').change(function (e) {

        if (window.opener != null && !window.opener.closed) {
            var CurrType = $("#ddlCurrencyType option:selected").text();
            $.ajax({
                type: "POST",
                url: "Trans_Escrow_PurOrd_Sub.aspx/SearchPurOrdSubItem",
                data: '{PurOrdNo:"' + $('#lblPurOrdNo').text() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.d.length > 0) {

                        tableget = $('#TblItemDet').DataTable();
                        tableget = tableget.clear();

                        var length = Object.keys(response.d).length;
                        var j = 0;

                        function rmsearchloop() {
                            var row = response.d[j];
                            tableget.row.add([
                                "",
                                row.SAPNo,
                                row.ItemCode,
                                row.ItemName,
                                "<input type='text' value=" + row.ModelQty + " class='form-control txtModelQtyEdit' style='width:100%' />",
                                row.RequiredQty,
                                "<input type='text' value=" + row.OrderQty + " class='form-control txtOrdQtyEdit' style='width:100%' />",
                                row.ReuiredDate,
                                "<input type='text' value=" + row.Rate_INR + " class='form-control txtINREdit' style='width:100%' />",
                                "<input type='text' value=" + row.Rate_Other + " class='form-control txtERUEdit' style='width:100%' />",
                                "<span class='form-control itemAmt' style='border:none'>" + row.ItemAmt + "</span>",
                                "<span class='form-control CGSTP' style='border:none'>" + row.CGSTP + "</span",
                                "<span class='form-control CGSTA' style='border:none'>" + row.CGSTAmt + "</span>",
                                "<span class='form-control SGSTP' style='border:none'>" + row.SGSTP + "</span>",
                                "<span class='form-control SGSTA' style='border:none'>" + row.SGSTAmt + "</span>",
                                "<span class='form-control IGSTP' style='border:none'>" + row.IGSTP + "</span>",
                                "<span class='form-control IGSTA' style='border:none'>" + row.IGSTAmt + "</span>",
                                "<span class='form-control LineTot' style='border:none'>" + row.IncTaxAmt + "</span>",
                                "<span class='form-control Calweek' style='border:none'>" + row.CalWeek + "</span>"

                            ]).draw(true);


                            if ($("#ddlCurrencyType option:selected").text() == "INR") {
                                $('.txtERUEdit').prop("disabled", true);
                                $('.txtERUEdit').val("0.00");

                                $('.itemAmt').text("0.00");
                            }
                            else {
                                $('.txtINREdit').prop("disabled", true);
                                $('.txtINREdit').val("0.00");
                                $('.itemAmt').text("0.00");
                            }

                            if ($("#ddlTaxType option:selected").text() == "NONE") {

                                alert("text")
                                $('.CGSTP').text("0.00");
                                $('.CGSTA').text("0.00");
                                $('.CGSTP').text("0.00");
                                $('.SGSTA').text("0.00");
                                $('.CGSTP').text("0.00");
                                $('.IGSTA').text("0.00");
                                $('.LineTot').text("0.00");
                            }

                            j++;
                            if (j < length) {
                                setTimeout(function () { rmsearchloop(); }, (length / 50).toFixed(0));

                            } else {

                                return;
                            }
                        }
                        rmsearchloop();
                    }
                }
            });
        }
        else {
            $.ajax({
                type: "POST",
                url: "Trans_Escrow_PurOrd_Sub.aspx/CalcRate",
                data: '{CurrencyType:"' + $('#ddlCurrencyType').children('option:selected').text() + '",Data:"' + TblRowChecked() + '"}',
                contentType: "jsaon",
                success: function (responsed) {
                    console.log(responsed.d)
                }
            });
            getItemDetails();
        }
    });

    //Validation
    $('#btnSavePurOrd').click(function (e) {
        var err = false;
        $('.PruOrd .req').each(function (index) {
            var input = $(this);
            var span = $(input).next('.form-error');
            // input.attr('style', '');
            // span.text('');
            if (input.val() == "" || input.val() == "0" || input.val() == "-Select-") {
                span.html('*This Field is Required');
                span.css('color', 'red');
                input.attr('style', 'border-color:red');
                err = true;
            }
        })

        $('.PruOrd .selectreq').each(
            function (index) {
                if ($(this).children("option:selected").val() == "-Select-") {
                    err = true;
                    var sn = $(this).next('p');
                    sn.text("*This Field is Required");
                    sn.css('color', 'red');
                }
            })

        if (!err) {
            SavePurOrd();
        }
    });

    $(function () {
        if (window.opener != null && !window.opener.closed) {

            var parent = $(window.opener.document).contents();
            var OrdNoEdit = parent.find("[id*=EditPurOrdNo]").val();
            saveType = parent.find("[id*=SaveType]").val();

            //var PurOrdNo = parent.find("[id*=EditPurOrdNo]").val()

            //$('#ddlRequestNo').hide();
            $('#ddlRequestNo').attr('style', 'dispaly:none');

            $('#hiddenSaveType').val(saveType);

            $('#lblPurOrdNo').html(OrdNoEdit);
            $.ajax({
                type: "POST",
                url: "Trans_Escrow_PurOrd_Sub.aspx/Search",
                data: '{PurOrdNo:"' + OrdNoEdit + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    $.each(response.d, function (i, value) {

                        $('#ddlMatTyp').val(value.MatType);
                        $('#ddlMatTyp').select2().trigger('change.select2');
                        $('#ddlMatTyp').trigger('change');
                        $('#lblPurOrdNo').text(OrdNoEdit);
                        $('#txtCoralPurOrdNo').val(value.CoralPurOrdNo);
                        $('#txtOrdDate').val(value.PurDate);
                        
                        $('#txtQutNo').val(value.QuatNo);
                        $('#txtQutDate').val(value.QuatDate);

                        $('#ddlSuppName').val(value.SuppCode);
                        $('#ddlSuppName').select2().trigger('change.select2');
                        //$('#ddlSuppName').trigger('change');
                        getDepartment();

                        
                        $('#ddlDeptName').val(value.DeptCode);
                        $('#ddlDeptName').text(value.DeptName);
                        $('#ddlDeptName').select2().trigger('change.select2');
                        //$('#ddlDeptName').trigger('change');
                        getDeliveryMode();
                        $('#ddlDeliveryMode').val(value.DeliModeCode);
                        $('#ddlDeliveryMode').select2().trigger('change.select2');
                        //$('#ddlDeliveryMode').trigger('change');
                        $('#txtPaymentMode').val(value.PayMode);
                        $('#txtPaymentMode').select2().trigger('change.select2');
                        //$('#txtPaymentMode').trigger('change');
                        $('#txtDeliDate').val(value.PurRquDate);
                        $('#txtDeliAt').val(value.DeliAt);
                        $('#txtPayTerms').val(value.Payterms);
                        $('#txtDesc').val(value.SubIns);
                        $('#txtSplDesc').val(value.SplNotes);
                        $('#txtNote').val(value.FreightChr);
                        $('#txtOthers').val(value.ETA);
                        $('#ddlCurrencyType').val(value.CurType);
                        $('#ddlCurrencyType').select2().trigger('change.select2');
                        //$('#ddlCurrencyType').trigger('change');

                        $('#ddlTaxType').val(value.TaxType);
                        $('#ddlTaxType').select2().trigger('change.select2');
                        //$('#ddlTaxType').trigger('change');

                        //$('#ddlRequestNo').trigger('change');

                        //EditPurOudNo();
                        //$('#ddlRequestNo').Style.visibility  = 'hidden';
                        $('#ddlRequestNo').val(value.PurRquNo);
                        $('#ddlRequestNo').text(value.PurRquNo);
                        $('#ddlRequestNo').select2().trigger('change.select2');

                        $('#lblPurRquNo').text(value.PurRquNo);

                        $('#txtReqDate').text(value.PurRquDt);

                        $('#lblTotQty').text(value.TotQty);
                        $('#lblItemAmtTol').text(value.TotItmAmt);
                        $('#lblTotCGSTAmt').text(value.TotCGSTAmt);
                        $('#lblTotSGSTAmt').text(value.TotSGSTAmt);
                        $('#lblTotIGSTAmt').text(value.TotIGSTAmt);
                        $('#lblTotAmt').text(value.TotNetAmt);
                        $('#txtRoundOff').val(value.AOL);
                        $('#lblFinNetAmt').text(value.FinalAmt)

                        $.ajax({
                            type: "POST",
                            url: "Trans_Escrow_PurOrd_Sub.aspx/SearchPurOrdSubItem",
                            data: '{PurOrdNo:"' + OrdNoEdit + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                console.log(value);
                                if (response.d.length > 0) {

                                    tableget = $('#TblItemDet').DataTable();
                                    tableget = tableget.clear();

                                    var length = Object.keys(response.d).length;
                                    var j = 0;

                                    function rmsearchloop() {
                                        var row = response.d[j];
                                        tableget.row.add([
                                            "",
                                            row.SAPNo,
                                            row.ItemCode,
                                            row.ItemName,
                                            "<input type='text' value=" + row.ModelQty + " class='form-control txtModelQtyEdit' style='width:100%' />",
                                            row.RequiredQty,
                                            "<input type='text' value=" + row.OrderQty + " class='form-control txtOrdQtyEdit' style='width:100%' />",
                                            row.ReuiredDate,
                                            "<input type='text' value=" + row.Rate_INR + " class='form-control txtINREdit' style='width:100%' />",
                                            "<input type='text' value=" + row.Rate_Other + " class='form-control txtERUEdit' style='width:100%' />",
                                            "<span class='form-control itemAmt' style='border:none'>" + row.ItemAmt + "</span>",
                                            row.CGSTP,
                                            row.CGSTAmt,
                                            row.SGSTP,
                                            row.SGSTAmt,
                                            row.IGSTP,
                                            row.IGSTAmt,
                                            row.IncTaxAmt,
                                            row.CalWeek,
                                            "<i class='btn-danger btn-sm fa fa-trash-o delete'></i>"

                                        ]).draw(true);

                                        if ($("#ddlCurrencyType option:selected").text() == "INR") {
                                            $('.txtERUEdit').prop("disabled", true);
                                        }
                                        else {
                                            $('.txtINREdit').prop("disabled", true);
                                        }

                                        j++;
                                        if (j < length) {
                                            setTimeout(function () { rmsearchloop(); }, (length / 50).toFixed(0));

                                        } else {

                                            return;
                                        }
                                    }
                                    rmsearchloop();
                                }



                            }
                        });


                    })
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);
                    $('.PurReqNotxt').html(errorThrown);
                    $('.PurReqNotxt').css("color", "Red");
                }
            });
        }
        else {
            window.setInterval(function () {
                var mattype = $('#ddlMatTyp').children("option:selected").val();
                $.ajax({
                    type: "POST",
                    url: "Trans_Escrow_PurOrd_Sub.aspx/GetLastNo",
                    data: '{MatType:"' + mattype + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != "false") {
                            $('#lblPurOrdNo').html(response.d);
                        } else {

                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('#lblPurOrdNo').html(errorThrown);
                        $('#lblPurOrdNo').css("color", "Red");
                        $('.btn').prop('disabled', true);
                        $('.form-control').prop('disabled', true);
                        parent.location = '/Default.aspx';
                        //alert('Please Login and Continue!!!')
                    }
                });
                if ($('#txtOrdDate').val() == "") {
                    Date.prototype.yyyymmdd = function () {
                        var yyyy = this.getFullYear().toString();
                        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                        var dd = this.getDate().toString();
                        return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding
                    };
                    var date = new Date();
                    $('#txtOrdDate').val(date.yyyymmdd());
                }
            }, 1000);
        }
    });

    function getItemDetails() {
        $.ajax({
            type: "POST",
            url: "Trans_Escrow_PurOrd_Sub.aspx/getItemDetails",
            data: '{MatType:"' + $('#ddlMatTyp').children('option:selected').text() + '",PurRquNo:"' + $('#ddlRequestNo').children('option:selected').text() + '",CurrencyTyp:"' + $('#ddlCurrencyType').children('option:selected').text() + '",GstType:"' + $('#ddlTaxType').children('option:selected').text() + '"}',
            //data: '{MatType:"' + $('#ddlMatTyp').children('option:selected').text() + '",PurRquNo:"' + $('#ddlRequestNo').children('option:selected').text() + '",SuppName:"' + $('#ddlSuppName').children('option:selected').text() + '",CurrencyTyp:"' + $('#ddlCurrencyType').children('option:selected').text() + '",GstType:"' + $('#ddlTaxType').children('option:selected').text() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var tblItemDet = $('#TblItemDet').DataTable();
                tblItemDet.clear();
                $.each(response.d, function (i, value) {
                    tblItemDet.row.add([
                        "",
                        value.SAPNo,
                        value.ItemCode,
                        value.ItemName,
                        "<input type='text' value=" + value.ModelQty + " class='form-control txtModelQtyEdit' style='width:100%' />",
                        value.RequiredQty,
                        "<input type='text' value=" + value.OrderQty + " class='form-control txtOrdQtyEdit' style='width:100%' />",
                        value.ReuiredDate,
                        "<input type='text' value=" + value.Rate_INR + " class='form-control txtINREdit' style='width:100%' />",
                        "<input type='text' value=" + value.Rate_Other + " class='form-control txtERUEdit' style='width:100%' />",
                        "<span class='form-control itemAmt' style='border:none'>" + value.ItemAmt + "</span>",
                        value.CGSTP,
                        value.CGSTAmt,
                        value.SGSTP,
                        value.SGSTAmt,
                        value.IGSTP,
                        value.IGSTAmt,
                        value.IncTaxAmt,
                        value.CalWeek
                    ]).draw(true);
                });



                if ($("#ddlCurrencyType option:selected").text() == "INR") {
                    $('.txtERUEdit').prop("disabled", true);
                }
                else {
                    $('.txtINREdit').prop("disabled", true);
                }
            }
        });
    }

    function CalcFooter() {
        var ItmAmt = 0;
        var OrdQty = 0;
        var CGSTTot = 0;
        var SGSTTot = 0;
        var IGSTTot = 0;
        var TotNet = 0;
        var TotFinAmt = 0;

        var AOL = $('#txtRoundOff').val();



        $('#TblItemDet').DataTable().$('.txtOrdQtyEdit').each(function (index, Obj) {
            var value = $(this).closest('tr').find('.txtOrdQtyEdit').val();
            var ItemAmt = $(this).closest('tr').find('td:eq(8)').text();
            var CGSTAmt = $(this).closest('tr').find('td:eq(9)').text();
            var SGSTAmt = $(this).closest('tr').find('td:eq(10)').text();
            var IGSTAmt = $(this).closest('tr').find('td:eq(11)').text();
            var TotAmt = $(this).closest('tr').find('td:eq(12)').text();
            var finalAmt = $(this).closest('tr').find('td:eq(12)').text();

            if (!isNaN(value)) OrdQty += parseFloat(value);
            if (!isNaN(ItemAmt)) ItmAmt += parseFloat(ItemAmt);
            if (!isNaN(CGSTAmt)) CGSTTot += parseFloat(CGSTAmt);
            if (!isNaN(SGSTAmt)) SGSTTot += parseFloat(SGSTAmt);
            if (!isNaN(IGSTAmt)) IGSTTot += parseFloat(IGSTAmt);
            if (!isNaN(TotAmt)) TotNet += parseFloat(TotAmt);
            if (!isNaN(finalAmt)) TotFinAmt += parseFloat(finalAmt);
        });

        $('#lblTotQty').text(OrdQty.toFixed(2));
        $('#lblItemAmtTol').text(ItmAmt.toFixed(2));
        $('#lblTotCGSTAmt').text(CGSTTot.toFixed(2));
        $('#lblTotSGSTAmt').text(SGSTTot.toFixed(2));
        $('#lblTotIGSTAmt').text(IGSTTot.toFixed(2));
        $('#lblTotAmt').text(TotNet.toFixed(2));

        //if (parseFloat(arr[1]) < parseFloat("49")) {

        $('#lblFinNetAmt').text(TotFinAmt.toFixed(2));


        $('#txtRoundOff').change();
    }
});


function SavePurOrd() {
        
    var err = false;
    var tablesave = $('#TblItemDet').DataTable();
    var lenght = tablesave.rows().count();
    var lists = new Array();

    if (lenght <= 0) {
        alert('Please Add Atleast one Sub item');
        err = true;
        return;
    }

    if (!err) {

        $('#TblItemDet tbody tr').each(function () {
            var CurRow = $(this);
            var list = {};

            list.SAPNo = CurRow.find("td:eq(1)").text();
            //list.ItemCode = CurRow.find("td:eq(2)").text();
            list.ItemName = CurRow.find("td:eq(2)").text().replace(/'/g, '');
            list.ModelQty = CurRow.find(".txtModelQtyEdit").val().replace(/'/g, '');
            list.RequiredQty = CurRow.find("td:eq(4)").text();
            list.OrderQty = CurRow.find(".txtOrdQtyEdit").val().replace(/'/g, '');
            //list.ReuiredDate = CurRow.find("td:eq(7)").text();
            list.Rate_INR = CurRow.find(".txtINREdit").val().replace(/'/g, '');
            list.Rate_Other = CurRow.find(".txtERUEdit").val().replace(/'/g, '');
            list.ItemAmt = CurRow.find("td:eq(8)").text();
            //list.CGSTP = CurRow.find("td:eq(11)").text();
            list.CGSTAmt = CurRow.find("td:eq(9)").text();
            //list.SGSTP = CurRow.find("td:eq(13)").text();
            list.SGSTAmt = CurRow.find("td:eq(10)").text();
            //list.IGSTP = CurRow.find("td:eq(15)").text();
            list.IGSTAmt = CurRow.find("td:eq(11)").text();
            list.IncTaxAmt = CurRow.find("td:eq(12)").text();
            list.Calweek = CurRow.find("td:eq(13)").text().replace(/'/g, '');

            lists.push(list);
        });
        SaveApi(JSON.stringify(lists));
    }
}

function SaveApi(data) {

    var MatType = $('#ddlMatTyp').children('option:selected').val();
    var PurOrdDate = $('#txtOrdDate').val();
    var PurOrdNo = $('#lblPurOrdNo').text();
    var SuppName = $('#ddlSuppName').children('option:selected').text();
    var SuppCode = $('#ddlSuppName').children('option:selected').val();
    var PurRquDate = $('#txtReqDate').text();

    var QuatNo = $('#txtQutNo').val();
    var QuatDate = $('#txtQutDate').val();

    var PurRquNo = "";

    if ($('#hiddenSaveType').val() == "1") {
        PurRquNo = $('#ddlRequestNo').children('option:selected').text();
    }
    else {
        PurRquNo = $('#lblPurRquNo').text();
    }
    
    var DeliMode = $('#ddlDeliveryMode').children('option:selected').text();
    var DeliModeCode = $('#ddlDeliveryMode').children('option:selected').val();
    var DeliDate = $('#txtDeliDate').val();
    var DeliAt = $('#txtDeliAt').val().replace(/'/g, '');
    var PayMode = $('#txtPaymentMode').children('option:selected').text();
    var DeptName = $('#ddlDeptName').children('option:selected').text();
    var DeptCode = $('#ddlDeptName').children('option:selected').val();
    var Payterms = $('#txtPayTerms').val().replace(/'/g, '');
    var Desc = $('#txtDesc').val().replace(/'/g, '');
    var SplDesc = $('#txtSplDesc').val().replace(/'/g, '');
    var Note = $('#txtNote').val().replace(/'/g, '');
    var Others = $('#txtOthers').val().replace(/'/g, '');
    var TotQty = $('#lblTotQty').text();
    var TotItemAmt = $('#lblItemAmtTol').text();
    var TotCGST = $('#lblTotCGSTAmt').text();
    var TotSGST = $('#lblTotSGSTAmt').text();
    var TotIGST = $('#lblTotIGSTAmt').text();
    var TotDisc = "0";
    var TaxP = "0";
    var TaxAmt = "0";
    var OtherAmt = "0";
    var NetAmt = $('#lblFinNetAmt').text();
    var AOL = $('#txtRoundOff').val();
    var CoralPurOrdNo = $('#txtCoralPurOrdNo').val().replace(/'/g, '');
    var GSTP = "0";
    var GSTName = $('#ddlTaxType').children('option:selected').text();
    var CurType = $('#ddlCurrencyType').children('option:selected').text();
    var SaveType = $('#hiddenSaveType').val();

    

    $.ajax({
        type: "POST",
        url: "Trans_Escrow_PurOrd_Sub.aspx/SaveItems",
        //data: "{'data':'" + data + "'}",
        data: JSON.stringify({ 'SaveType': SaveType, 'MatType': MatType, 'PurOrdNo': PurOrdNo, 'PurOrdDate': PurOrdDate, 'SuppName': SuppName, 'SuppCode': SuppCode, 'PurRquDate': PurRquDate, 'PurRquNo': PurRquNo, 'DeliModeCode': DeliModeCode, 'DeliMode': DeliMode, 'DeliDate': DeliDate, 'DeliAt': DeliAt, 'PayMode': PayMode, 'DeptName': DeptName, 'DeptCode': DeptCode, 'Payterms': Payterms, 'Desc': Desc, 'SplNotes': SplDesc, 'Note': Note, 'Others': Others, 'TotQty': TotQty, 'TotItemAmt': TotItemAmt, 'TotCGST': TotCGST, 'TotSGST': TotSGST, 'TotIGST': TotIGST, 'TotDisc': TotDisc, 'TaxP': TaxP, 'TaxAmt': TaxAmt, 'OtherAmt': OtherAmt, 'NetAmt': NetAmt, 'AOL': AOL, 'CoralPurOrdNo': CoralPurOrdNo, 'GSTP': GSTP, 'GSTName': GSTName, 'CurType': CurType, 'SaveType': SaveType, 'QuatNo': QuatNo, 'QuatDate': QuatDate, 'data': data }),

        //data: "{'SaveType':'" + SaveType + "','MatType':'" + MatType + "','PurOrdNo':'" + PurOrdNo + "','PurOrdDate':'" + PurOrdDate + "','SuppName':'" + SuppName + "','SuppCode':'" + SuppCode + "','PurRquDate':'" + PurRquDate + "','PurRquNo':'" + PurRquNo + "','DeliModeCode':'" + DeliModeCode + "','DeliMode':'" + DeliMode + "','DeliDate':'" + DeliDate + "','DeliAt':'" + DeliAt + "','PayMode':'" + PayMode + "','DeptName':'" + DeptName + "','DeptCode':'" + DeptCode + "','Payterms':'" + Payterms + "','Desc':'" + Desc + "','SplNotes':'" + SplDesc + "','Note':'" + Note + "','Others':'" + Others + "','TotQty':'" + TotQty + "','TotItemAmt':'" + TotItemAmt + "','TotCGST':'" + TotCGST + "','TotSGST':'" + TotSGST + "','TotIGST':'" + TotIGST + "','TotDisc':'" + TotDisc + "','TaxP':'" + TaxP + "','TaxAmt':'" + TaxAmt + "','OtherAmt':'" + OtherAmt + "','NetAmt':'" + NetAmt + "','AOL':'" + AOL + "','CoralPurOrdNo':'" + CoralPurOrdNo + "','GSTP':'" + GSTP + "','GSTName':'" + GSTName + "','CurType':'" + CurType + "','SaveType':'" + SaveType + "','data':'" + data + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d == "true") {
                alert('Details Saved Successfully');
                ClearAllData();
                if (SaveType == "2") {
                    window.close();
                }
                //window.open('Trans_Escrow_PurOrd_Main.aspx','');
                //window.location.href = "Trans_Escrow_PurOrd_Main.aspx";
                window.location.replace("Trans_Escrow_PurReq_Main.aspx");
            }
        },
        failure: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function SaveMsgAlert(msg) {
    alert(msg);
}

var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    prm.add_endRequest(function (sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.RowTest').DataTable({ "paging": false, "ordering": false, "searching": false });
            $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
            $('.select2').select2();
        }
    });
};

//Clear the Main Fields
function clearMain() {
    $('#PruOrd .form-control').each(function (index, element) {
        if ($(element).is('select')) {
            $(element).prop('selectedIndex', 0);
            $(element).select2().trigger('change.select2');
        } else {
            $(element).val("");
            $(element).text("");
            $(element).attr('style', '');
            $(element).next('span').text();
        }
    });
}
function ClearSub() {

    $('.PruOrd .form-control ').each(function (index, element) {
        if ($(element).parents().is(".divTable")) {
            return false;
        }
        if ($(element).is('select')) {
            $(element).prop('selectedIndex', 0);
            $(element).select2().trigger('change.select2');
        } else {
            $(element).val("");
            $(element).text("");
            $(element).attr('style', '');
            $(element).next('span').text();
        }
    });
}
function ClearAllData() {
    clearMain();
    ClearSub();

    var table = $('.table').DataTable();
    table.clear();
    table.draw(true);
}
