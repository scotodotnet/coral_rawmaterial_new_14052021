﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Admin_Module_MenuHead_Rights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Form List";

            Module_Name_And_MenuName_Add();
            txtModuleName_SelectedIndexChanged(sender, e);
            Load_Module_Form_Details();
        }
        
    }

    private void Module_Name_And_MenuName_Add()
    {
        //Module Name Add
        DataTable dtcate = new DataTable();
        string SSQL = "";
        SSQL = "Select * from Admin_Company_Module_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " Order by ModuleName Asc";

        dtcate = objdata.RptEmployeeMultipleDetails(SSQL);
        txtModuleName.DataSource = dtcate;
        txtModuleName.DataTextField = "ModuleName";
        txtModuleName.DataValueField = "ModuleID";
        txtModuleName.DataBind();
    }


    private void Load_Module_Form_Details()
    {
        string SSQL = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        string Module_ID = "";
        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();

        Module_ID = txtModuleName.SelectedValue.ToString();

        if (Module_ID == "") { Module_ID = "0"; }

        string Menu_ID = "";
        //Menu_ID = Decrypt(txtMenuName.SelectedValue.ToString()).ToString();
        Menu_ID = ddlMenuHead.SelectedValue.ToString();

        if (Menu_ID == "") { Menu_ID = "0"; }

        DataTable DT = new DataTable();
        SSQL = "Select FormID,FormName from Admin_Module_MenuForm_List where ModuleID='" + Module_ID + "' And MenuID='" + Menu_ID + "'";
        SSQL = SSQL + " Order by FormID Asc";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        Load_Module_Form_Rights();

    }

    private void Load_Module_Form_Rights()
    {

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            string ModuleName = txtModuleName.SelectedItem.ToString();
            //string ModuleID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();
            string ModuleID = txtModuleName.SelectedValue.ToString();
            string ModuleID_Encrypt = txtModuleName.SelectedValue.ToString();

            string MenuName = ddlMenuHead.SelectedItem.ToString();
            //string MenuID = Decrypt(txtMenuName.SelectedValue.ToString()).ToString();
            string MenuID = ddlMenuHead.SelectedValue.ToString();
            string MenuID_Encrypt = ddlMenuHead.SelectedValue.ToString();

            string FormName = "";
            string FormID = "0";
            string FormID_Encrypt = "0";

            Label FormID_lbl = (Label)gvsal.FindControl("FormID");
            FormID = FormID_lbl.Text.ToString();
            FormName = gvsal.Cells[1].Text.ToString();
            //FormID_Encrypt = Encrypt(FormID).ToString();
            FormID_Encrypt = FormID.ToString();
            //Get Company Module Rights
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from Admin_Company_Module_MenuForm_Rights where ModuleID='" + ModuleID_Encrypt + "' And MenuID='" + MenuID_Encrypt + "' And FormID='" + FormID_Encrypt + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //User Rights Update in Grid
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "";
        bool ErrFlag = false;

        string Module_ID_Encrypt = "0";
        string Module_ID = "0";
        string Module_Name = "0";

        string Menu_ID_Encrypt = "0";
        string Menu_ID = "0";
        string Menu_Name = "0";

        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();
        Module_ID = txtModuleName.SelectedValue.ToString();
        Module_Name = txtModuleName.SelectedItem.ToString();
        Module_ID_Encrypt = txtModuleName.SelectedValue.ToString();

        //Menu_ID = Decrypt(txtMenuName.SelectedValue.ToString()).ToString();

        Menu_ID = ddlMenuHead.SelectedValue.ToString();

        Menu_Name = ddlMenuHead.SelectedItem.ToString();
        Menu_ID_Encrypt = ddlMenuHead.SelectedValue.ToString();

        SSQL = "Delete from Admin_Company_Module_MenuForm_Rights where ModuleID='" + Module_ID_Encrypt + "' And MenuID='" + Menu_ID_Encrypt + "'";
        SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            SaveMode = "Insert";
            
            string Form_ID_Encrypt = "0";
            string Form_ID = "0";
            string Form_Name = "0";

            CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");

            Label FormID_lbl = (Label)gvsal.FindControl("FormID");
            Form_ID = FormID_lbl.Text.ToString();
            Form_Name = gvsal.Cells[1].Text.ToString();
            //Form_ID_Encrypt = Encrypt(Form_ID).ToString();
            Form_ID_Encrypt = Form_ID.ToString();

            if (ChkSelect_chk.Checked == true)
            {
                //Get Form LI ID
                string Form_LI_ID = "";
                DataTable dtID = new DataTable();

                SSQL = "Select * from Admin_Module_MenuForm_List where ModuleID='" + Module_ID + "' And MenuID='" + Menu_ID + "' And ";
                SSQL = SSQL + " FormID ='" + Form_ID + "'";

                dtID = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtID.Rows.Count != 0)
                {
                    Form_LI_ID = dtID.Rows[0]["Form_LI_ID"].ToString();
                }

                //Insert User Rights
                SSQL = "Insert Into Admin_Company_Module_MenuForm_Rights(CompCode,LocCode,ModuleID,ModuleName,MenuID,MenuName,FormID,FormName,";
                SSQL = SSQL + " Form_LI_ID) Values('" + SessionCcode + "','" + SessionLcode + "','" + Module_ID_Encrypt + "',";
                SSQL = SSQL + " '" + Module_Name + "','" + Menu_ID_Encrypt + "','" + Menu_Name + "','" + Form_ID_Encrypt + "','" + Form_Name + "',";
                SSQL = SSQL + " '" + Form_LI_ID + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Module Menu Form Access Rights Details Saved Successfully');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Module Menu Form Access Rights Details Not Saved Properly...');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Load_Module_Form_Details();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        Load_Module_Form_Details();
    }

    protected void txtModuleName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable dtMenu = new DataTable();
        if (txtModuleName.SelectedValue != "")
        {
            query = "Select * from Admin_Company_Module_MenuHead_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ModuleID='" + txtModuleName.SelectedValue + "' order by MenuName Asc";
            dtMenu = objdata.RptEmployeeMultipleDetails(query);
            ddlMenuHead.DataSource = dtMenu;
            ddlMenuHead.DataTextField = "MenuName";
            ddlMenuHead.DataValueField = "MenuID";
            ddlMenuHead.DataBind();
            btnView_Click(sender, e);
        }
    }
    protected void ddlMenuHead_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMenuHead.SelectedValue != "")
        {
            btnView_Click(sender, e);
        }
    }

    
}