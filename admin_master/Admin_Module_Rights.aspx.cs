﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Admin_Module_Rights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Module List";

            Load_Module_Details();
        }
        
    }

    private void Load_Module_Details()
    {
        string SSQL = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        DataTable DT = new DataTable();
        SSQL = "Select ModuleID,ModuleName from Admin_Module_List order by ModuleID Asc";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        grdModule.DataSource = DT;
        grdModule.DataBind();

        if (grdModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        Load_Module_Rights();
    }

    private void Load_Module_Rights()
    {

        foreach (GridViewRow gvsal in grdModule.Rows)
        {
            string ModuleName = "";
            string ModuleID = "0";
            //string ModuleID_Encrypt = "0";

            Label ModuleID_lbl = (Label)gvsal.FindControl("ModuleID");
            ModuleID = ModuleID_lbl.Text.ToString();
            ModuleName = gvsal.Cells[1].Text.ToString();
            //ModuleID_Encrypt = Encrypt(ModuleID).ToString();
            //Get Company Module Rights
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from Admin_Company_Module_Rights where ModuleID='" + ModuleID + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //User Rights Update in Grid
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "";
        bool ErrFlag = false;

        SSQL = "Delete from Admin_Company_Module_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

        objdata.RptEmployeeMultipleDetails(SSQL);

        foreach (GridViewRow gvsal in grdModule.Rows)
        {
            SaveMode = "Insert";
            string Module_ID_Encrypt = "0";
            string Module_ID = "0";
            string Module_Name = "0";

            CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");

            Label ModuleID_lbl = (Label)gvsal.FindControl("ModuleID");
            Module_ID = ModuleID_lbl.Text.ToString();
            Module_Name = gvsal.Cells[1].Text.ToString();

            //Module_ID_Encrypt = Encrypt(Module_ID).ToString();

            if (ChkSelect_chk.Checked == true)
            {
                //Get Module Link
                string Module_Link_Str = "";
                DataTable DT = new DataTable();
                SSQL = "Select * from Admin_Module_List where ModuleID='" + Module_ID + "'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    Module_Link_Str = DT.Rows[0]["ModuleLink"].ToString();
                }

                //Insert User Rights
                SSQL = "Insert Into Admin_Company_Module_Rights(CompCode,LocCode,ModuleID,ModuleName,ModuleLink) Values('" + SessionCcode + "',";
                SSQL = SSQL + "'" + SessionLcode + "','" + Module_ID + "','" + Module_Name + "','" + Module_Link_Str + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
}