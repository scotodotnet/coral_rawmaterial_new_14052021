﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FinancialYear_Sub.aspx.cs" Inherits="FinancialYear_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Financial Year Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Financial Year Code <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtFinCode" runat="server" class="form-control"/>
                                    <span id="DeptCode" class="text-danger"></span>
                                    <asp:RequiredFieldValidator ControlToValidate="txtFinCode" Display="Dynamic" ValidationGroup="Validate_Field" 
                                        class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Financial Year Name <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtFinName" runat="server" class="form-control" />
                                    <span id="ShortName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtFinName" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Starting Date<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtStartDate" runat="server" class="form-control datepicker" ></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtStartDate" ValidationGroup="Validate_Field" class="form_error" 
                                         ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtStartDate" ValidChars="0123456789./">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Ending Date<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtEndDate" runat="server" class="form-control datepicker" ></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtEndDate" ValidationGroup="Validate_Field" class="form_error" 
                                         ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtEndDate" ValidChars="0123456789./">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Active Mode<span class="text-danger">*</span> </label>
                                   <asp:RadioButtonList ID="rbtIsActive" runat="server" CssClass="form-control" RepeatColumns="2">
                                        <asp:ListItem Selected="True" Value="Yes" style="padding:20px" Text="Yes"></asp:ListItem>
                                        <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                         
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server"  Text="Clear" OnClick="btnCancel_Click"/>
                            <asp:Button ID ="btnBack" class="btn btn-default" runat="server"  Text="Back To List" OnClick="btnBack_Click"/>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

