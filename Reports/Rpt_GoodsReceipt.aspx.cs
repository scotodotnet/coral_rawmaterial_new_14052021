﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Rpt_GoodsReceipts : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Report Goods Return";

            Load_Data_Supplier();
            Load_Data_Item();
            Load_Data_SapNo();
            Load_Data_GrnNo();
        }
    }

    private void Load_Data_GrnNo()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataRow dr;

            SSQL = "Select Distinct Grno[GRNo] from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and Status!='Delete'  ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlGRNNo.DataSource = DT;

            dr = DT.NewRow();
            dr["GRNo"] = "-Select-";
            DT.Rows.InsertAt(dr, 0);

            ddlGRNNo.DataValueField = "GRNo";
            ddlGRNNo.DataTextField = "GRNo";
            ddlGRNNo.DataBind();
        }
        catch (Exception ex)
        {

        }
    }


    private void Load_Data_SapNo()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataRow dr;

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = "Select Mat_No[ItemCode],Sap_No[SapNo] from BomMaster Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode[ItemCode],RefCode[SapNo] from MstTools Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ItemCode[ItemCode],RefCode[SapNo] from CORAL_ERP_Asset..MstAssetItem ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "General Item")
            {
                SSQL = "Select ItemCode[ItemCode],RefCode[SapNo] from MstGeneralItem Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlSapNo.DataSource = DT;

            dr = DT.NewRow();

            dr["SapNo"] = "-Select-";
            dr["ItemCode"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);
            ddlSapNo.DataTextField = "SapNo";
            ddlSapNo.DataValueField = "ItemCode";
            ddlSapNo.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    private void Load_Data_Supplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status='Add' And LedgerGrpName='Supplier' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlSupplier.DataSource = DT;

        dr = DT.NewRow();
        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlSupplier.DataValueField = "LedgerCode";
        ddlSupplier.DataTextField = "LedgerName";
        ddlSupplier.DataBind();
    }

    private void Load_Data_Item()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        if (ddlMatType.SelectedItem.Text == "RawMaterial")
        {
            SSQL = "Select Mat_No as ItemCode,Raw_Mat_Name as ItemName from BOMMaster Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status !='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "Tools")
        {
            SSQL = "Select ToolCode as ItemCode,ToolName as ItemName from MstTools Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status !='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "Asset")
        {
            SSQL = "Select ItemCode,ItemDesc[ItemName] from CORAL_ERP_Asset..MstAssetItem Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status !='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "General Item")
        {
            SSQL = "Select ItemCode,ItemName from MstGeneralItem Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status !='Delete'";
        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlItem.DataSource = DT;

        dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItem.DataValueField = "ItemCode";
        ddlItem.DataTextField = "ItemName";
        ddlItem.DataBind();
    }

    protected void btnRecList_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataGrid GvExcel = new DataGrid();
            string daycolspan = "";

            SSQL = "Select GRM.GRNo[Goods Received No],GRM.GRDate[Goods Received Date],GRM.PurOrdNo[Purchase Order No],GRM.PurOrdDate[Purchase Order Date],";
            SSQL = SSQL + " GRM.PartyInvNo[Supplier Invoice No],GRM.PartyInvDate[Supplier Invoice Date],";
            SSQL = SSQL + " GRM.SuppName[Supplier Name], GRM.PayTerms[Payment Terms],GRM.PayMode[Payment Mode],GRM.VehicleNo[Vehicle No],";
            SSQL = SSQL + " GRM.Remarks[Remarks],GRS.ItemName[Item Name],GRS.SapNo [Sap No],GRS.PurOrdQty [Purchase Order Quantity],";
            SSQL = SSQL + " GRS.ReceiveQty [Received Quantity],GRS.BalQty [Balance Quantity],GRM.CurrencyType [Currency Type],GRS.Rate,";
            SSQL = SSQL + " GRS.ItemTotal[Item toal],GRS.Disc [Discount(%)],GRS.DiscAmt[Discount Amount],GRS.CGSTPer[CGST(%)],";
            SSQL = SSQL + " GRS.CGSTAmt[CGST Amount],GRS.SGSTPer[SGST(%)],GRS.SGSTAmt[SGST Amount],GRS.IGSTPer[IGST(%)],";
            SSQL = SSQL + " GRS.IGSTAmt[IGST Amount],GRS.LineTotal [Net Amount] From Trans_GoodsReceipt_Main GRM ";
            SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRM.GRNo=GRS.GRNo ";

            SSQL = SSQL + " Where GRM.CCode='" + SessionCcode + "' and GRM.LCode ='" + SessionLcode + "' and ";
            SSQL = SSQL + "  GRM.FinYearCode='" + SessionFinYearCode + "'";


            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                SSQL = SSQL + " And Convert (DateTime,GRM.GRDate, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                SSQL = SSQL + " And Convert (DateTime,GRM.GRDate, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
            }
            //else
            //{
            //    SSQL = SSQL + " And Convert (DateTime,GRNo, 103)>=Convert (DateTime,Getdate(),103) ";
            //    SSQL = SSQL + " And Convert (DateTime,GRNo, 103)<=Convert (DateTime,Getdate(),103) ";
            //}

            if (ddlSupplier.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And GRM.SuppName='" + ddlSupplier.SelectedItem.Text + "'";
            }

            if (ddlItem.SelectedItem.Text != "-Select-")
            {
                //SSQL = SSQL + " And SuppName='" + ddlCust.SelectedItem.Text + "'";
            }

            if (ddlPurType.SelectedItem.Text == "Escrow")
            {
                SSQL = SSQL + " And GRM.SuppType= '" + ddlPurType.SelectedValue + "'";
            }
            else if (ddlPurType.SelectedItem.Text == "Enercon")
            {
                SSQL = SSQL + " And GRM.SuppType= '" + ddlPurType.SelectedValue + "'";
            }
            else if (ddlPurType.SelectedItem.Text == "Coral")
            {
                SSQL = SSQL + " And GRM.SuppType= '" + ddlPurType.SelectedValue + "'";
            }

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = SSQL + " And GRM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = SSQL + " And GRM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = SSQL + " And GRM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "General Item")
            {
                SSQL = SSQL + " And GRM.MatType= '" + ddlMatType.SelectedValue + "'";
            }

            SSQL = SSQL + " And GRM.Status ='0' Order By CONVERT(datetime,GRM.GRDate,105) ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                string attachment = "attachment;filename=GoodsReceived_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 27) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 27) + "'>");
                Response.Write("<a style=\"font-weight:bold\">Goods Received Report</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");

                DateTime dateTime = DateTime.UtcNow.Date;

                if (txtFromDate.Text == "" || txtToDate.Text == "")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 27) + "'>");
                    Response.Write("<a style=\"font-weight:bold\"> As On Date  -" + dateTime.ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 27) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                if (ddlPurType.SelectedItem.Text == "Enercon")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 27) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=ENERCONE</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Escrow")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 27) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=ESCROW</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Coral")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 27) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=CORAL</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }

                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnSamplereport_Click(object sender, EventArgs e)
    {
        string RptName = "";

        RptName = "Goods Received Report";

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?SuppName=" + ddlSupplier.SelectedItem.Text + "&ItemName=" + ddlItem.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blanck", "");
    }

    protected void btnGRNInv_Click(object sender, EventArgs e)
    {
        string RptName = "";
        Boolean ErrFlag = false;

        RptName = "Goods Received Invoice Format";

        if (ddlMatType.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Material Type...');", true);
        }

        if (ddlGRNNo.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Goods Receipt No');", true);
        }

        if (!ErrFlag)
        {
            ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?GRN_No=" + ddlGRNNo.SelectedItem.Text + "&MatType=" + ddlMatType.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
        }
    }

    protected void btnItemWise_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataGrid GvExcel = new DataGrid();
        string daycolspan = "";

        SSQL = "Select GRM.GRNo,GRM.GRDate,GRM.PurOrdNo,GRM.PurOrdDate,GRM.SuppName,GRS.SapNo,GRS.ItemName,GRS.Rate,GRS.PurOrdQty,";
        SSQL = SSQL + " GRS.ReceiveQty,GRS.ExcessQty From Trans_GoodsReceipt_Main GRM ";
        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRS.GRNo=GRM.GRNo ";
        SSQL = SSQL + " Where GRM.Ccode='" + SessionCcode + "' and GRM.LCode='" + SessionLcode + "' and GRM.Status!='Delete' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            string attachment = "attachment;filename=ExcessQtyReport_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
            Response.Clear();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            GvExcel.DataSource = DT;
            GvExcel.DataBind();

            GvExcel.HeaderStyle.Font.Bold = true;

            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GvExcel.RenderControl(htextw);

            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
            Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");
            Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
            //Response.Write("--");
            //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");
            Response.Write("<a style=\"font-weight:bold\">Goods Received Report (Excess)</a>");
            Response.Write("  ");

            Response.Write("</td>");
            Response.Write("</tr>");

            DateTime dateTime = DateTime.UtcNow.Date;

            if (txtFromDate.Text == "" || txtToDate.Text == "")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> As On Date  -" + dateTime.ToString("dd/MM/yyyy") + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");

                Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                Response.Write("&nbsp;&nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                Response.Write("  ");
                Response.Write("</td>");
                Response.Write("</tr>");
            }

            if (ddlPurType.SelectedItem.Text == "Enercon")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");

                Response.Write("<a style=\"font-weight:bold\">Supplier Type=ENERCONE</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else if (ddlPurType.SelectedItem.Text == "Escrow")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");

                Response.Write("<a style=\"font-weight:bold\">Supplier Type=ESCROW</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else if (ddlPurType.SelectedItem.Text == "Coral")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");

                Response.Write("<a style=\"font-weight:bold\">Supplier Type=CORAL</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
            }

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
        }
    }

    protected void btnStdReceiptList_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataGrid GvExcel = new DataGrid();
        string daycolspan = "";

        //Change Purchase Order No in to Reference No


        //SSQL = "Select GRM.GRNo[Goods Received No],GRM.GRDate[Goods Received Date],GRM.PurOrdNo[Purchase Order No],";
        //SSQL = SSQL + " GRM.PurOrdDate[Purchase Order Date],";
        //SSQL = SSQL + " GRM.PartyInvNo[Supplier Invoice No],GRM.PartyInvDate[Supplier Invoice Date],GRM.SuppName[Supplier Name],";
        //SSQL = SSQL + " GRM.Remarks[Remarks],GRS.ItemName[Item Name],GRS.SapNo [Sap No],GRS.UOM,GRS.PurOrdQty [Purchase Order Quantity],";
        //SSQL = SSQL + " GRS.ReceiveQty [Received Quantity] From Trans_GoodsReceipt_Main GRM ";
        //SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRM.GRNo=GRS.GRNo ";

        SSQL = "Select GRM.GRNo[Goods Received No],GRM.GRDate[Goods Received Date],POM.RefNo[Purchase Order No],";
        SSQL = SSQL + " GRM.PurOrdDate[Purchase Order Date],";
        SSQL = SSQL + " GRM.PartyInvNo[Supplier Invoice No],GRM.PartyInvDate[Supplier Invoice Date],GRM.SuppName[Supplier Name],";
        SSQL = SSQL + " GRM.Remarks[Remarks],GRS.ItemName[Item Name],GRS.SapNo [Sap No],GRS.UOM,GRS.PurOrdQty [Purchase Order Quantity],";
        SSQL = SSQL + " GRS.ReceiveQty [Received Quantity] From Trans_GoodsReceipt_Main GRM ";
        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRM.GRNo=GRS.GRNo ";

        if (ddlPurType.SelectedItem.Text == "Escrow")
        {
            SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Main POM on (GRM.PurOrdNo=POM.Std_PO_No or GRM.PurOrdNo=POM.RefNo) ";
        }
        else if (ddlPurType.SelectedItem.Text == "Enercon")
        {
            SSQL = SSQL + " Inner join Trans_Enercon_PurOrd_Main POM on (GRM.PurOrdNo=POM.Std_PO_No or GRM.PurOrdNo=POM.RefNo) ";
        }
        else if (ddlPurType.SelectedItem.Text == "Coral")
        {
            SSQL = SSQL + " Inner join Trans_Coral_PurOrd_Main POM on (GRM.PurOrdNo=POM.Std_PO_No or GRM.PurOrdNo=POM.RefNo) ";
        }

        SSQL = SSQL + " Where GRM.CCode='" + SessionCcode + "' and GRM.LCode ='" + SessionLcode + "' and ";
        SSQL = SSQL + " GRM.FinYearCode='" + SessionFinYearCode + "'";


        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            SSQL = SSQL + " And Convert (DateTime,GRM.GRDate, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
            SSQL = SSQL + " And Convert (DateTime,GRM.GRDate, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
        }
        //else
        //{
        //    SSQL = SSQL + " And Convert (DateTime,GRNo, 103)>=Convert (DateTime,Getdate(),103) ";
        //    SSQL = SSQL + " And Convert (DateTime,GRNo, 103)<=Convert (DateTime,Getdate(),103) ";
        //}

        if (ddlSupplier.SelectedItem.Text != "-Select-")
        {
            SSQL = SSQL + " And GRM.SuppName='" + ddlSupplier.SelectedItem.Text + "'";
        }

        if (ddlItem.SelectedItem.Text != "-Select-")
        {
            //SSQL = SSQL + " And SuppName='" + ddlCust.SelectedItem.Text + "'";
        }

        if (ddlPurType.SelectedItem.Text == "Escrow")
        {
            SSQL = SSQL + " And GRM.SuppType= '" + ddlPurType.SelectedValue + "'";
        }
        else if (ddlPurType.SelectedItem.Text == "Enercon")
        {
            SSQL = SSQL + " And GRM.SuppType= '" + ddlPurType.SelectedValue + "'";
        }
        else if (ddlPurType.SelectedItem.Text == "Coral")
        {
            SSQL = SSQL + " And GRM.SuppType= '" + ddlPurType.SelectedValue + "'";
        }

        if (ddlMatType.SelectedItem.Text == "RawMaterial")
        {
            SSQL = SSQL + " And GRM.MatType= '" + ddlMatType.SelectedValue + "'";
        }
        else if (ddlMatType.SelectedItem.Text == "Tools")
        {
            SSQL = SSQL + " And GRM.MatType= '" + ddlMatType.SelectedValue + "'";
        }
        else if (ddlMatType.SelectedItem.Text == "Asset")
        {
            SSQL = SSQL + " And GRM.MatType= '" + ddlMatType.SelectedValue + "'";
        }
        else if (ddlMatType.SelectedItem.Text == "General Item")
        {
            SSQL = SSQL + " And GRM.MatType= '" + ddlMatType.SelectedValue + "'";
        }

        SSQL = SSQL + " And GRM.Status ='0' Order By CONVERT(datetime,GRM.GRDate,105)";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {

            string attachment = "attachment;filename=GoodsReceived_Standard_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
            Response.Clear();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            GvExcel.DataSource = DT;
            GvExcel.DataBind();

            GvExcel.HeaderStyle.Font.Bold = true;

            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GvExcel.RenderControl(htextw);

            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
            Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 13) + "'>");
            Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
            //Response.Write("--");
            //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 13) + "'>");
            Response.Write("<a style=\"font-weight:bold\">Goods Received Report</a>");
            Response.Write("  ");

            Response.Write("</td>");
            Response.Write("</tr>");

            DateTime dateTime = DateTime.UtcNow.Date;

            if (txtFromDate.Text == "" || txtToDate.Text == "")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 13) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> As On Date  -" + dateTime.ToString("dd/MM/yyyy") + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 13) + "'>");

                Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                Response.Write("&nbsp;&nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                Response.Write("  ");
                Response.Write("</td>");
                Response.Write("</tr>");
            }

            if (ddlPurType.SelectedItem.Text == "Enercon")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 13) + "'>");

                Response.Write("<a style=\"font-weight:bold\">Supplier Type=ENERCONE</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else if (ddlPurType.SelectedItem.Text == "Escrow")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 13) + "'>");

                Response.Write("<a style=\"font-weight:bold\">Supplier Type=ESCROW</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else if (ddlPurType.SelectedItem.Text == "Coral")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 13) + "'>");

                Response.Write("<a style=\"font-weight:bold\">Supplier Type=CORAL</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
            }

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
        }
    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Item();
        Load_Data_SapNo();
    }

    protected void btnPurGPinList_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataGrid GvExcel = new DataGrid();
            string daycolspan = "";

            SSQL = "Select PGM.Pur_GIN_No[Purchase GatePass In No],PGM.Pur_GIN_Date[Purchase GatePass In Date],PGM.SuppInvNo[Invoice No],";
            SSQL = SSQL + " PGM.SuppInvDate[Invoice Date],PGM.SuppName[SupplierName],PGM.TransportName[Transport],PGM.VehicleNo[VehicleNo],";
            SSQL = SSQL + " PGS.PurOrdNo[Purchase Order No],PGS.PurOrdDate[Purchase Order Date],";
            SSQL = SSQL + " Case When PGS.AccType='1' then 'Enercon' When PGS.AccType='2' then 'Escrow' When PGS.AccType='3' then 'Coral' End Supplier_Type,";
            SSQL = SSQL + " Case When PGS.MatType='1' then 'RawMaterial' when PGS.MatType='2' then 'Tools' when PGS.MatType='3' then 'Asset' ";
            SSQL = SSQL + " When PGS.MatType='4' then 'General Items' end MatrialType,PGS.TotQty[Total_PurchaseOrder_Qty],PGS.TotAmt[Total_PurchaseOrder_Amount], ";
            SSQL = SSQL + " Case When PGM.ApprovalStatus='1' then 'Approval Success' when PGM.ApprovalStatus='0' then 'Approval Pending' End Approval_Status ";
            SSQL = SSQL + " From Trans_Pur_GIN_Main PGM Inner Join Trans_Pur_GIN_Sub PGS on PGS.Pur_GIN_No=PGM.Pur_GIN_No ";
            SSQL = SSQL + " Where PGM.CCode='" + SessionCcode + "' and  PGM.LCode='" + SessionLcode + "' and PGM.Status!='Delete' ";


            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                SSQL = SSQL + " And Convert (DateTime,PGM.Pur_GIN_Date, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                SSQL = SSQL + " And Convert (DateTime,PGM.Pur_GIN_Date, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
            }

            if (ddlSupplier.SelectedItem.Text!="-Select-")
            {
                SSQL = SSQL + " And PGM.SuppName='" + ddlSupplier.SelectedItem.Text + "'";
            }

            if (ddlPurType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PGS.AccType='" + ddlPurType.SelectedValue + "'";
            }

            if (ddlMatType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PGS.MatType='" + ddlMatType.SelectedValue + "'";
            }


            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                string attachment = "attachment;filename=Purchase_GatePass_In_List_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 14) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 14) + "'>");
                Response.Write("<a style=\"font-weight:bold\">Purchase GatePass In List</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");

                DateTime dateTime = DateTime.UtcNow.Date;

                if (txtFromDate.Text == "" || txtToDate.Text == "")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 14) + "'>");
                    Response.Write("<a style=\"font-weight:bold\"> As On Date  -" + dateTime.ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 14) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }

                if (ddlPurType.SelectedItem.Text == "Enercon")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 14) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=ENERCONE</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Escrow")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 14) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=ESCROW</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Coral")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 14) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=CORAL</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }

                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
            }
        }
        catch (Exception Ex)
        {

        }
    }

    protected void btnGPOutList_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataGrid GvExcel = new DataGrid();
            string daycolspan = "";


            SSQL = " Select GPM.GPOutNo [GatePass Out No],GPM.GPOutDate [GatePass Out Date],GPM.SuppInvNo[Invoice No],";
            SSQL = SSQL + " GPM.SuppInvDate[Invoice Date],GPM.SuppName[SupplierName],GPM.DeliveryDate, ";
            SSQL = SSQL + " Case When GPM.MatType='1' then 'RawMaterial' when GPM.MatType='2' then 'Tools' when GPM.MatType='3' then 'Asset' ";
            SSQL = SSQL + " when GPM.MatType='4' then 'General Items' end MatrialType,GPS.ItemName,GPS.UOMName,GPS.OutQty ";
            SSQL = SSQL + " From Trans_GPOut_Main GPM Inner Join Trans_GPOut_Sub GPS on GPS.GP_Out_No=GPM.GPOutNo ";
            SSQL = SSQL + " Where GPM.CCode='"+ SessionCcode +"' and  GPM.LCode='"+ SessionLcode +"' and GPM.Status!='Delete' ";


            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                SSQL = SSQL + " And Convert (DateTime,GPM.GPOutDate, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                SSQL = SSQL + " And Convert (DateTime,GPM.GPOutDate, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
            }
           
            if (ddlSupplier.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And GPM.SuppName='" + ddlSupplier.SelectedItem.Text + "'";
            }

           

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = SSQL + " And GPM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = SSQL + " And GPM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = SSQL + " And GPM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "General Item")
            {
                SSQL = SSQL + " And GPM.MatType= '" + ddlMatType.SelectedValue + "'";
            }

            SSQL = SSQL + " Order By CONVERT(datetime,GPM.GPOutDate,105)";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                string attachment = "attachment;filename=GatePass_Out_List" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");
                Response.Write("<a style=\"font-weight:bold\">GatePass Out List</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");

                DateTime dateTime = DateTime.UtcNow.Date;

                if (txtFromDate.Text == "" || txtToDate.Text == "")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");
                    Response.Write("<a style=\"font-weight:bold\"> As On Date  -" + dateTime.ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 10) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }

                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
            }
        }
        catch(Exception Ex)
        {

        }
    }

    protected void btnDeliveryChallan_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataGrid GvExcel = new DataGrid();
            string daycolspan = "";

            SSQL = " Select DCM.DCNo[DeliveryChallan_No],DCM.DCDate[DeliveryChallan_Date],DCM.SuppName,DCM.SuppInvNo[Supplier_Invoice_No], ";
            SSQL = SSQL + " DCM.SuppInvDate [Supplier_Invoice_Date],DCM.GPType [GatePass_Type],DCM.StockType[Stock_Type],DCM.TransportDetails, ";
            SSQL = SSQL + " DCM.DriverName,DCM.DriverMobNo,DCM.Purpose,DCM.Remarks,DCS.RefNo,DCS.ItemName,DCS.OutQty,DCS.Rate,";
            SSQL = SSQL + " DCS.ItemAmount,DCS.GSTType,DCS.CGSTAmt,DCS.SGSTAmt,DCS.IGSTAmt,DCS.LineAmt ";
            SSQL = SSQL + " From Trans_DC_Main DCM  Inner Join Trans_DC_Sub DCS on DCS.DCNo=DCM.DCNo ";
            SSQL = SSQL + " Where DCM.Ccode='" + SessionCcode + "' and DCM.Lcode='" + SessionLcode + "' and DCM.Status!='Delete' ";


            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                SSQL = SSQL + " And Convert (DateTime,DCM.DCDate, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                SSQL = SSQL + " And Convert (DateTime,DCM.DCDate, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
            }

            if (ddlSupplier.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And DCM.SuppName='" + ddlSupplier.SelectedItem.Text + "'";
            }

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = SSQL + " And DCM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = SSQL + " And DCM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = SSQL + " And DCM.MatType= '" + ddlMatType.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "General Item")
            {
                SSQL = SSQL + " And DCM.MatType= '" + ddlMatType.SelectedValue + "'";
            }

            SSQL = SSQL + " Order By CONVERT(datetime,DCM.DCDate,105)";


            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                string attachment = "attachment;filename=Delivery_Challan_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");
                Response.Write("<a style=\"font-weight:bold\">Delivery Challan List</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");

                DateTime dateTime = DateTime.UtcNow.Date;

                if (txtFromDate.Text == "" || txtToDate.Text == "")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");
                    Response.Write("<a style=\"font-weight:bold\"> As On Date  -" + dateTime.ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }

                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
            }
        }
        catch (Exception Ex)
        {

        }
    }
}