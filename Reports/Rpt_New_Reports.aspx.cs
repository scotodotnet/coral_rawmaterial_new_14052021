﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Rpt_New_Reports : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Accounting General Purchase";

            Load_Data_Supplier();
            Load_Data_Account_Type();
            Load_Data_Bank();
            Load_Data_Month();
            Load_Data_Agent();
        }
    }

    private void Load_Data_Month()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select MonthCode,MonthName from Acc_Mst_Month ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMonth.DataSource = DT;

        dr = DT.NewRow();
        dr["MonthCode"] = "-Select-";
        dr["MonthName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlMonth.DataValueField = "MonthCode";
        ddlMonth.DataTextField = "MonthName";
        ddlMonth.DataBind();
    }

    private void Load_Data_Account_Type()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select AccTypeCode,AccTypeName from Acc_Mst_AccountType  where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status='Add' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAccType.DataSource = DT;

        dr = DT.NewRow();
        dr["AccTypeCode"] = "-Select-";
        dr["AccTypeName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlAccType.DataValueField = "AccTypeCode";
        ddlAccType.DataTextField = "AccTypeName";
        ddlAccType.DataBind();
    }

    private void Load_Data_Bank()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select BankCode,BankName from MstBank where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status='Add' And Active='1'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlBankName.DataSource = DT;

        dr = DT.NewRow();
        dr["BankCode"] = "-Select-";
        dr["BankName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlBankName.DataValueField = "BankCode";
        ddlBankName.DataTextField = "BankName";
        ddlBankName.DataBind();
    }

    private void Load_Data_Supplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger  where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status='Add' And LedgerGrpName='Supplier'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlLedgerName.DataSource = DT;

        dr = DT.NewRow();
        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlLedgerName.DataValueField = "LedgerCode";
        ddlLedgerName.DataTextField = "LedgerName";
        ddlLedgerName.DataBind();
    }

    private void Load_Data_Agent()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger  where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status='Add' And LedgerGrpName='Supplier'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlAgentName.DataSource = DT;

        dr = DT.NewRow();
        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlAgentName.DataValueField = "LedgerCode";
        ddlAgentName.DataTextField = "LedgerName";
        ddlAgentName.DataBind();
    }

    protected void ddlLedgerGrp_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where Ccode='" + SessionCcode +"' and LCode='"+ SessionLcode +"' And Status='Add' ";
        SSQL = SSQL + " And LedgerGrpName='"+ ddlLedgerGrp.SelectedItem.Text +"'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlLedgerName.DataSource = DT;
        DataRow Dr = DT.NewRow();

        Dr["LedgerCode"] = "-Select-";
        Dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(Dr, 0);

        ddlLedgerName.DataTextField = "LedgerName";
        ddlLedgerName.DataValueField = "LedgerCode";
        ddlLedgerName.DataBind();

    }

    public void Load_TwoDates()
    {
        if (ddlMonth.SelectedItem.Text != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = SessionFinYearVal.ToString();

            Years = Temp_Years.Split('_');

            if (ddlMonth.SelectedItem.Text == "January" || ddlMonth.SelectedItem.Text == "March" || ddlMonth.SelectedItem.Text == "May" || ddlMonth.SelectedItem.Text == "July" || ddlMonth.SelectedItem.Text == "August" || ddlMonth.SelectedItem.Text == "October" || ddlMonth.SelectedItem.Text == "December")
            {
                Month_Total_days = 31;
            }
            else if (ddlMonth.SelectedItem.Text == "April" || ddlMonth.SelectedItem.Text == "June" || ddlMonth.SelectedItem.Text == "September" || ddlMonth.SelectedItem.Text == "November")
            {
                Month_Total_days = 30;
            }

            else if (ddlMonth.SelectedItem.Text == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonth.SelectedItem.Text)
            {
                case "January":
                    Month_Last = "01";
                    break;
                case "February":
                    Month_Last = "02";
                    break;
                case "March":
                    Month_Last = "03";
                    break;
                case "April":
                    Month_Last = "04";
                    break;
                case "May":
                    Month_Last = "05";
                    break;
                case "June":
                    Month_Last = "06";
                    break;
                case "July":
                    Month_Last = "07";
                    break;
                case "August":
                    Month_Last = "08";
                    break;
                case "September":
                    Month_Last = "09";
                    break;
                case "October":
                    Month_Last = "10";
                    break;
                case "November":
                    Month_Last = "11";
                    break;
                case "December":
                    Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonth.SelectedItem.Text == "January") || (ddlMonth.SelectedItem.Text == "February") || (ddlMonth.SelectedItem.Text == "March"))
            {
                Year_Last = Years[1];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFromDate.Text = FromDate.ToString();
            txtToDate.Text = ToDate.ToString();

        }
        else
        {
            txtToDate.Text = "";
            txtFromDate.Text = "";
        }
    }

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }

    protected void btnTransAll_Click(object sender, EventArgs e)
    {
        string RptName = "Transaction Report";

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blanck", "");
    }

    protected void btnAgentList_Click(object sender, EventArgs e)
    {
        
        if (ListRptName.SelectedItem.Text == "Cash Negative Report")
        {
            ResponseHelper.Redirect("/Reports/Acc_Cash_Negative_Report.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text, "_blanck", "");
        }
        else if(ListRptName.SelectedItem.Text == "Payments List")
        {
            ResponseHelper.Redirect("/Reports/Acc_Payments_List.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text+"&Filtype="+ ddlGL.SelectedValue +"&FillAmt="+txtAmt.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Receipts List")
        {
            ResponseHelper.Redirect("/Reports/Acc_Receipts_List.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&Filtype=" + ddlGL.SelectedValue + "&FillAmt=" + txtAmt.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Sundry Debtors Transaction List")
        {
            ResponseHelper.Redirect("/Reports/Acc_Sundry_Debtors_Transaction.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Long Pending Debtors List")
        {
            ResponseHelper.Redirect("/Reports/Acc_Long_Pending_Debitors.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Debtors Ageing List")
        {
            ResponseHelper.Redirect("/Reports/Acc_Debitors_Agenting_List.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Sundry Creditors Transaction List")
        {
            ResponseHelper.Redirect("/Reports/Acc_Sundry_Creditors_Transaction.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "MSME Creditors List ")
        {                                          
            ResponseHelper.Redirect("/Reports/Acc_MSME_Creditors.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Fixed Assets Addition")
        {
            ResponseHelper.Redirect("/Reports/Acc_Fixed_Assets_Addition.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Book Turnover Vs GST Portal Turnover (3B) Matching ")
        {
            //ResponseHelper.Redirect("/Reports/Acc_MSME_Creditors.aspx?, "_blanck", "");
            ResponseHelper.Redirect("/Reports/Acc_Book_Turnover_GST_Portal_Turnover.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "GST Input Cross Check With 2A of GST Portal")
        {
            ResponseHelper.Redirect("/Reports/Acc_GST_Input_2A_GST_Portal.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Bank Reconciliation Statement")
        {
            ResponseHelper.Redirect("/Reports/Acc_Bank_Reconciliation.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "Book TDS Vs 26 AS TDS Matching")
        {
            ResponseHelper.Redirect("/Reports/Acc_BookTDS_26TDS_Matching.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "TDS Payables Expenses Wise")
        {
            ResponseHelper.Redirect("/Reports/Acc_TDS_Payables_Expenses.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text, "_blanck", "");
        }
        else if (ListRptName.SelectedItem.Text == "GST Input and Output Reconciliation")
        {
            ResponseHelper.Redirect("/Reports/GST_Input_Output_Reconciliation.aspx?LedgerGroup=" + ddlLedgerGrp.SelectedItem.Text + "&LedgerName=" + ddlLedgerName.SelectedItem.Text + "&AccType=" + ddlAccType.SelectedItem.Text + "&AgentName=" + ddlAgentName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text, "_blanck", "");
        }

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlLedgerGrp.SelectedItem.Text = "-Select-";    ddlLedgerGrp.SelectedValue = "0";
        ddlLedgerName.SelectedItem.Text = "-Select-";   ddlLedgerName.SelectedValue = "-Select-";
        ddlAccType.SelectedItem.Text = "-Select-";      ddlAccType.SelectedValue = "-Select-";
        ddlBankName.SelectedItem.Text = "-Select-";     ddlBankName.SelectedValue = "-Select-";
        ddlAgentName.SelectedItem.Text = "-Select-";    ddlAgentName.SelectedValue = "-Select-";
        ddlMonth.SelectedValue = "-Select-";            ddlMonth.SelectedItem.Text = "-Select-";
        txtFromDate.Text = "";                          txtToDate.Text = "";
        ddlFinPeriod.SelectedValue = "0";               ddlFinPeriod.SelectedItem.Text = "-Select-";
    }

    protected void ddlRptName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select * From Report_User_Rights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
            SSQL = SSQL + " UserName ='" + SessionUserID + "' and ReportType='" + ddlRptName.SelectedItem.Text + "' ";
            
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ListRptName.Items.Clear();
            if (DT.Rows.Count != 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    ListRptName.Items.Add(DT.Rows[i]["ReportName"].ToString());
                }

            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ListRptName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ListRptName.SelectedItem.Text == "Cash Negative Report")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "Payments List")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = true;
            txtAmt.Enabled = true;
            txtAmt.Text = "0";
        }
        else if (ListRptName.SelectedItem.Text == "Receipts List")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = true;
            txtAmt.Enabled = true;
            txtAmt.Text = "0";
        }
        else if (ListRptName.SelectedItem.Text == "Sundry Debtors Transaction List")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.SelectedItem.Text = "Customer";
            ddlLedgerGrp.SelectedValue = "2";
            ddlLedgerGrp_SelectedIndexChanged(sender, e);

            ddlLedgerGrp.Enabled = false;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "Long Pending Debtors List")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.SelectedItem.Text = "Customer";
            ddlLedgerGrp.SelectedValue = "2";
            ddlLedgerGrp_SelectedIndexChanged(sender, e);

            ddlLedgerGrp.Enabled = false;

            
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "Debtors Ageing List")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.SelectedItem.Text = "Customer";
            ddlLedgerGrp.SelectedValue = "2";
            ddlLedgerGrp_SelectedIndexChanged(sender, e);

            ddlLedgerGrp.Enabled = false;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "Sundry Creditors Transaction List")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.SelectedItem.Text = "Supplier";
            ddlLedgerGrp.SelectedValue = "1";
            ddlLedgerGrp_SelectedIndexChanged(sender, e);

            ddlLedgerGrp.Enabled = false;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "MSME Creditors List ")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.SelectedItem.Text = "Supplier";
            ddlLedgerGrp.SelectedValue = "1";
            ddlLedgerGrp_SelectedIndexChanged(sender, e);

            ddlLedgerGrp.Enabled = false;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "Fixed Assets Addition")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.SelectedItem.Text = "Supplier";
            ddlLedgerGrp.SelectedValue = "1";
            ddlLedgerGrp_SelectedIndexChanged(sender, e);

            ddlLedgerGrp.Enabled = false;

            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "Book Turnover Vs GST Portal Turnover (3B) Matching ")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "GST Input Cross Check With 2A of GST Portal")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "Bank Reconciliation Statement")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "Book TDS Vs 26 AS TDS Matching")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "TDS Payables Expenses Wise")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }
        else if (ListRptName.SelectedItem.Text == "GST Input and Output Reconciliation")
        {
            btnExcel.Enabled = true;
            btnReport.Enabled = false;
            btnClear.Enabled = true;

            ddlLedgerGrp.Enabled = true;
            ddlLedgerName.Enabled = true;
            ddlAccType.Enabled = true;
            ddlBankName.Enabled = false;
            ddlAgentName.Enabled = true;
            ddlMonth.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
            ddlFinPeriod.Enabled = true;

            ddlGL.Enabled = false;
            txtAmt.Enabled = false;
        }

    }

    protected void ddlGL_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}