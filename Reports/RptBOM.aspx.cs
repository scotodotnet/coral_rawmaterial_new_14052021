﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_RptBOM : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionCompanyName;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";

    System.Web.UI.WebControls.DataGrid grid = new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();

            if (!IsPostBack)
            {
                Load_Department();
                Load_Genrator();
                Load_Product();
                Load_Supplier();
                Load_BOM();
            }
        }
    }

    private void Load_BOM()
    {
        SSQL = "";
        SSQL = "Select (Mat_No+' ~> '+Raw_Mat_Name) as Text,Mat_No as Value from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";

        ddlItems.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
        ddlItems.DataTextField = "Text";
        ddlItems.DataValueField = "Value";
        ddlItems.DataBind();
        ddlItems.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Product()
    {
        SSQL = "";
        SSQL = "Select  (ProductNo+' ~> '+ProductName) as Text,ProductNo as Value from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
        ddlProduct.DataTextField = "Text";
        ddlProduct.DataValueField = "Value";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    private void Load_Supplier()
    {
        SSQL = "";
        SSQL = "Select (LedgerName) as Text,LedgerCode as Value from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " Lcode ='" + SessionLcode + "' and Status ='Add' and LedgerGrpName='Supplier'";
        ddlSupplier.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlSupplier.DataTextField = "Text";
        ddlSupplier.DataValueField = "Value";
        ddlSupplier.DataBind();
        ddlSupplier.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Genrator()
    {
        SSQL = "";
        SSQL = "Select (GenModelNo+' ~> '+GenModelName) as Text,GenModelNo as Value from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
       
        ddlGenerator.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlGenerator.DataTextField = "Text";
        ddlGenerator.DataValueField = "Value";
        ddlGenerator.DataBind();
        ddlGenerator.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        
    }

    private void Load_Department()
    {
        SSQL = "";
        SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlDepartment.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDepartment.DataTextField = "deptname";
        ddlDepartment.DataValueField = "deptCode";
        ddlDepartment.DataBind();
        ddlDepartment.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlDepartment.ClearSelection();
        ddlGenerator.ClearSelection();
        ddlProduct.ClearSelection();
        ddlSupplier.ClearSelection();
        txtDrawingNo.Text = "";
        txtSapNo.Text = "";
        txtValidFrom.Text = "";
        txtValidTo.Text = "";
        
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/master/BOMMasterMain.aspx");
    }

    protected void btnBOMItemsReport_Click(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select Raw_mat_name as [Name],Sap_No as [SAP Number],Mat_No as [BOM Number],Drawing_No as [Drawing Number],Revision as Revision,ImgPath_Get as Picture";
        SSQL = SSQL + ",Amt_of_stations as [Amount of Station],Amt_pro_stations as [Ammount Pro Station],Possible_Capacity as [Possible Capacity],Production_Steps as [Production Step]";
        SSQL = SSQL + ",GenModelNo as [Generator Model],Production_Type as [Production Type],Amount as [Cost per last known offer],Amount as [Costs summary],ValidTo_Str as [Lifetime]";
        SSQL = SSQL + " from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        if (ddlGenerator.SelectedValue != "-Select-")
        {
            SSQL = SSQL + " and GenModelNo='" + ddlGenerator.SelectedValue + "'";
        }
        if (ddlProduct.SelectedValue != "-Select-")
        {
            SSQL = SSQL + " and ProductionPartNo='" + ddlProduct.SelectedValue + "'";
        }
        if (ddlSupplier.SelectedValue != "-Select-")
        {
            SSQL = SSQL + " and Supp_Code='" + ddlSupplier.SelectedValue + "'";
        }
        if (ddlDepartment.SelectedValue != "-Select-")
        {
            SSQL = SSQL + " and DeptCode='" + ddlDepartment.SelectedValue + "'";
        }
        if (ddlItems.SelectedValue != "-Select-")
        {
            SSQL = SSQL + " and Mat_No='" + ddlItems.SelectedValue + "'";
        }

        SSQL = SSQL + " group by GenModelNo,ProductionPartNo,Mat_No,Raw_mat_name,Sap_No,Drawing_No,Revision,ImgPath_Get,Amt_of_stations,Amt_pro_stations,Possible_Capacity,Production_Steps,";
        SSQL = SSQL + " Production_Type,Amount,ValidTo_Str order by Mat_No asc";

        DataTable dt_report = new DataTable();
        dt_report = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_report.Rows.Count > 0)
        {
            grid.DataSource = dt_report;
            grid.DataBind();
            string attachment = "attachment;filename=BOM_MATERIALS.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr>");
            Response.Write("<td align=center colspan='" + dt_report.Columns.Count + "'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td align=center colspan='" + dt_report.Columns.Count + "'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw);
            Response.End();
            Response.Clear();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
    }
}