﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class Reports_Report_Supplier_Quotation : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionBlanketPOOrderNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SSQL;
    DateTime FromDate;
    DateTime ToDate;
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    string ReportName = "SUPPLIER QUOTATION REPORT";
    string SupQtnNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Page.Title = "CORAL ERP :: Report Supplier Quotation";

        if (!IsPostBack)
        {
            Load_Data_Empty_SupQtnNo();
            Load_Data_Empty_ItemCode();
            Load_Data_Empty_Supp1();
        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        if (txtSPONo.SelectedItem.Text != "-Select-")
        {
            SupQtnNo = txtSPONo.SelectedItem.Text;
        }
        else
        {
            SupQtnNo = "";
        }

        ResponseHelper.Redirect("Supplier_Quotation_Crystalpage.aspx?QNumber=" + SupQtnNo + "&ReportName=" + ReportName.ToString() + "&SupplierName=" + txtSupplierName.Text + "&ItemName=" + txtItemName.Text + "&FDate=" + txtFromDate.Text + "&TDate=" + txtToDate.Text, "_blank", "");
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtItemCodeHide.Value = "";
        txtItemName.Text = "";
        //txtSPODateHide.Value = "";
        txtSPONo.SelectedValue = "-Select-";
        txtItemNameSelect.SelectedIndex = 0;
        txtSupplierNameSelect.SelectedIndex = 0;
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtSuppCodehide.Value = "";
        txtSupplierName.Text = "";
    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select Mat_No as ItemCode,Raw_Mat_Name as ItemName from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();

    }


    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCode.Text = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;
            txtItemCodeHide.Value = txtItemNameSelect.SelectedValue;
        }
        else
        {
            txtItemCode.Text = ""; txtItemName.Text = ""; txtItemCodeHide.Value = "";
        }
    }

    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select (LedgerName) as SuppName  ,LedgerCode as SuppCode from Acc_Mst_Ledger where LedgerGrpName='Supplier' ";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status='Add' ";

        DT = objdata.RptEmployeeMultipleDetails(query);

        txtSupplierNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtSupplierNameSelect.DataTextField = "SuppName";
        txtSupplierNameSelect.DataValueField = "SuppCode";
        txtSupplierNameSelect.DataBind();

    }

    protected void txtSupplierNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtSupplierNameSelect.SelectedValue != "-Select-")
        {
            txtSupplierName.Text = txtSupplierNameSelect.SelectedItem.Text;
            txtSuppCodehide.Value = txtSupplierNameSelect.SelectedValue;
        }
        else
        {
            txtSupplierName.Text = ""; txtSuppCodehide.Value = "";
        }
    }


    protected void btnSupQtnNo_Click(object sender, EventArgs e)
    {
        //modalPop_SupQtnNo.Show();
    }

    private void Load_Data_Empty_SupQtnNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();


        txtSPONo.Items.Clear();
        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtSPONo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["QuotNo"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSPONo.DataTextField = "QuotNo";

        txtSPONo.DataBind();
    }
}