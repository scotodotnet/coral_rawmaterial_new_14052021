﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Rpt_GoodsReceipt.aspx.cs" Inherits="Rpt_GoodsReceipts" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">

        <asp:UpdatePanel ID="upExcel" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Report Goods Received</span></h3>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <label for="exampleInputName">Supplier Type</label>
                                            <asp:DropDownList ID="ddlPurType" runat="server" class="form-control select2">
                                                <asp:ListItem Value="1">Enercon</asp:ListItem>
                                                <asp:ListItem Value="2">Escrow</asp:ListItem>
                                                <asp:ListItem Value="3">Coral</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="exampleInputName">Material Type</label>
                                            <asp:DropDownList ID="ddlMatType" runat="server" class="form-control select2" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                <asp:ListItem Value="1">RawMaterial</asp:ListItem>
                                                <asp:ListItem Value="2">Tools</asp:ListItem>
                                                <asp:ListItem Value="3">Asset</asp:ListItem>
                                                <asp:ListItem Value="4">General Item</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="exampleInputName">Supplier Name</label>
                                            <asp:DropDownList ID="ddlSupplier" runat="server" class="form-control select2"></asp:DropDownList>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="exampleInputName">Goods Receipts No</label>
                                            <asp:DropDownList ID="ddlGRNNo" runat="server" class="form-control select2"></asp:DropDownList>
                                        </div>


                                    </div>

                                    <div class="row" runat="server" style="padding-bottom: 15px; padding-top: 15px">

                                        <div class="col-md-3">
                                            <label for="exampleInputName">SAP NO</label>
                                            <asp:DropDownList ID="ddlSapNo" runat="server" class="form-control select2"></asp:DropDownList>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="exampleInputName">Item Name</label>
                                            <asp:DropDownList ID="ddlItem" runat="server" class="form-control select2"></asp:DropDownList>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="exampleInputName">From Date</label>
                                            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtFromDate" ValidChars="0123456789./">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="exampleInputName">To Date</label>
                                            <asp:TextBox ID="txtToDate" MaxLength="20" class="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtToDate" ValidChars="0123456789./">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnReceiptList" class="btn btn-primary" runat="server" Text="Received List" OnClick="btnRecList_Click" />
                                        <asp:Button ID="btnStdReceiptList" class="btn btn-primary" runat="server" Text="Received List (Enercon Standard)" OnClick="btnStdReceiptList_Click" />
                                        <asp:Button ID="btnSamplereport" class="btn btn-primary" runat="server" Text="Sample Report" OnClick="btnSamplereport_Click" />
                                        <asp:Button ID="btnGRNInv" Visible="false" CssClass="btn btn-primary" runat="server" Text="Invoice Format(GRN)" OnClick="btnGRNInv_Click" />
                                        <asp:Button ID="btnItemWise" Visible="false" CssClass="btn btn-primary" runat="server" Text="Item Wise(Excess)" OnClick="btnItemWise_Click" />
                                        <asp:Button ID="btnPurGPinList" CssClass="btn btn-primary" runat="server" Text="Purchase GatePass IN List" OnClick="btnPurGPinList_Click" />
                                        <asp:Button ID="btnGPOutList" CssClass="btn btn-primary" runat="server" Text="GatePass Out List" OnClick="btnGPOutList_Click"  />
                                        <asp:Button ID="btnDeliveryChallan" CssClass="btn btn-primary" runat="server" Text="Delivery Challan" OnClick="btnDeliveryChallan_Click" />

                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="Panel1" runat="server">
                                <ContentTemplate>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnReceiptList" />
                                    <asp:PostBackTrigger ControlID="btnStdReceiptList" />
                                    <asp:PostBackTrigger ControlID="btnItemWise" />
                                    <asp:PostBackTrigger ControlID="btnPurGPinList" />
                                    <asp:PostBackTrigger ControlID="btnGPOutList" />
                                    <asp:PostBackTrigger ControlID="btnDeliveryChallan" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: 'ture' });
                    $('.select2').select2();
                }
            });
        };
    </script>
</asp:Content>

