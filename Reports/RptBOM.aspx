﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptBOM.aspx.cs" Inherits="Reports_RptBOM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upRptBOM" runat="server">
            <ContentTemplate>
                  <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-cubes text-primary"></i> BOM Report
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Genrator Model</label>
                                                <asp:DropDownList ID="ddlGenerator" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Product Item</label>
                                                <asp:DropDownList ID="ddlProduct" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Supplier</label>
                                                <asp:DropDownList ID="ddlSupplier" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Department</label>
                                                <asp:DropDownList ID="ddlDepartment" class="form-control select2" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Item Name</label>
                                                <asp:DropDownList ID="ddlItems" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="visibility:hidden">
                                            <div class="form-group">
                                                <label class="control-label">Valid From</label>
                                                <asp:TextBox ID="txtValidFrom" autocomplete="off" class="form-control datepicker" MaxLength="10" runat="server"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtValidFrom" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2" style="visibility:hidden">
                                            <div class="form-group">
                                                <label class="control-label">Valid To</label>
                                                <asp:TextBox ID="txtValidTo" autocomplete="off" class="form-control datepicker" MaxLength="10" runat="server"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtValidTo" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="visibility:hidden">
                                            <div class="form-group">
                                                <label class="control-label">SAP Number</label>
                                                <asp:TextBox ID="txtSapNo" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" style="visibility:hidden">
                                            <div class="form-group">
                                                <label class="control-label">Drawing Number</label>
                                                <asp:TextBox ID="txtDrawingNo" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                   
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group" align="center">
                                        <asp:Button ID="btnBOMItemsReport" class="btn btn-success" OnClick="btnBOMItemsReport_Click" runat="server" Text="BOM Items" />
                                        <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                                    </div>
                                </div>
                                <!-- /.box-footer-->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col-md-12-->
                        <div class="col-md-3" hidden>
                            <div class="callout callout-info" style="margin-bottom: 0!important;">
                                <h4><i class="fa fa-info"></i>Info:</h4>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnBOMItemsReport"  />
                </Triggers>
            </asp:UpdatePanel>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
      
        <!-- /.content -->
    </div>
</asp:Content>

