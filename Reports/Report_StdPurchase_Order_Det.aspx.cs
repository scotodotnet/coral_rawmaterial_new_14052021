﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Reports_Report_StdPurchase_Order_Det : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string DeptName = ""; string MatType = ""; string StdPurOrdNo = ""; string SuppType = ""; string SapNo = "";
    string SupQtnNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "CORAL ERP :: Purchase Order Details";

        if (!IsPostBack)
        {
            Load_Data_Empty_Dept();
            Load_Data_Empty_SupQtnNo();
            Load_Data_Empty_StdPurOrdNo();
            Load_Data_Empty_ItemCode();
            Load_Data_Empty_Supp1();
            Load_Data_Empty_SAPNo();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDepartmentName.SelectedValue = "-Select-";
        txtSupplierNameSelect.SelectedIndex = 0;
        txtItemNameSelect.SelectedIndex = 0;
        // txtDeptCodeHide.Value = "";
        txtSupplierName.Text = "";
        txtSuppCodehide.Value = "";
        txtStdPurOrdNo.SelectedValue = "-Select-";
        //txtStdPurOrdDateHide.Value = "";
        txtSPONo.SelectedValue = "-Select-";
        //txtSPODateHide.Value = "";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
        {
            StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
        }
        else
        {
            StdPurOrdNo = "";
        }

        if (ddlMatType.SelectedItem.Text != "-Select-")
        {
            MatType = ddlMatType.SelectedItem.Text;
        }
        else
        {
            MatType = "";
        }

        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }

        if (ddlPurType.SelectedItem.Text != "-Select-")
        {
            SuppType = ddlPurType.SelectedItem.Text;
        }
        else
        {
            SuppType = "";
        }

        RptName = "Standard Purchase Order Details Report";
        ResponseHelper.Redirect("ReportDisplay.aspx?SuppType=" + SuppType + "&MatType=" + MatType + "&DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnInvcFormat_Click(object sender, EventArgs e)
    {
        Boolean Errflg = false;


        RptName = "Standard Purchase Order Details Invoice Format";

        if (txtStdPurOrdNo.SelectedItem.Text == "-Select-" && ddlPurType.SelectedItem.Text == "-Select-")
        {
            Errflg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Purchase Order No and Supplier Type..');", true);
        }


        if (!Errflg)
        {
            if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
            {
                StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
            }
            else
            {
                StdPurOrdNo = "";
            }
            //if (txtSPONo.SelectedItem.Text != "-Select-")
            //{
            //    SupQtnNo = txtSPONo.SelectedItem.Text;
            //}
            //else
            //{
            //    SupQtnNo = "";
            //}
            if (txtDepartmentName.SelectedItem.Text != "-Select-")
            {
                DeptName = txtDepartmentName.SelectedItem.Text;
            }
            else
            {
                DeptName = "";
            }


            if (ddlPurType.SelectedItem.Text != "-Select-")
            {
                SuppType = ddlPurType.SelectedItem.Text;
            }
            else
            {
                SuppType = "";
            }

            ResponseHelper.Redirect("ReportDisplay.aspx?SuppType=" + SuppType + "&DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
        }
    }


    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();


    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    private void Load_Data_Empty_ItemCode()
    {

        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        if (ddlMatType.SelectedItem.Text == "RawMaterial")
        {
            SSQL = "Select Mat_No as ItemCode,Raw_Mat_Name as ItemName from BOMMaster Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status !='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "Tools")
        {
            SSQL = "Select ToolCode as ItemCode,ToolName as ItemName from MstTools Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status !='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "Asset")
        {
            SSQL = "Select ItemCode,ItemDesc[ItemName] from CORAL_ERP_Asset..MstAssetItem Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status !='Delete'";
        }
        else if (ddlMatType.SelectedItem.Text == "General Items")
        {
            SSQL = "Select ItemCode,ItemName from MstGeneralItem Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status !='Delete'";
        }


        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();

    }

    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCode.Text = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;
            txtItemCodeHide.Value = txtItemNameSelect.SelectedValue;
        }
        else
        {
            txtItemCode.Text = ""; txtItemName.Text = ""; txtItemCodeHide.Value = "";
        }
    }


    protected void btnSupQtnNo_Click(object sender, EventArgs e)
    {
        //modalPop_SupQtnNo.Show();
    }
    private void Load_Data_Empty_SupQtnNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtSPONo.Items.Clear();
        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        query = query + "And FinYearCode ='" + SessionFinYearCode + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtSPONo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["QuotNo"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSPONo.DataTextField = "QuotNo";

        txtSPONo.DataBind();

    }

    protected void GridViewClick_SupQtnNo(object sender, CommandEventArgs e)
    {
        txtSPONo.Text = Convert.ToString(e.CommandArgument);
        //txtSPODateHide.Value = Convert.ToString(e.CommandName);


    }

    private void Load_Data_Empty_SAPNo()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = "Select Mat_No[ItemCode],Sap_No[SapNo] from BomMaster Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete' and Sap_No!='' Order by Cast(Sap_No as int) ";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode[ItemCode],RefCode[SapNo] from MstTools Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ItemCode[ItemCode],RefCode[SapNo] from CORAL_ERP_Asset..MstAssetItem ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = "Select ItemCode[ItemCode],RefCode[SapNo] from MstGeneralItem Where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlSAPNo.DataSource = DT;

            DataRow dr = DT.NewRow();

            dr["SapNo"] = "-Select-";
            dr["ItemCode"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);
            ddlSAPNo.DataTextField = "SapNo";
            ddlSAPNo.DataValueField = "ItemCode";
            ddlSAPNo.DataBind();
        }
        catch (Exception ex)
        {

        }

    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        //query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        query = "Select (LedgerName) as SuppName,LedgerCode as SuppCode from Acc_Mst_Ledger where LedgerGrpName='Supplier' ";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status='Add' ";

        DT = objdata.RptEmployeeMultipleDetails(query);

        txtSupplierNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtSupplierNameSelect.DataTextField = "SuppName";
        txtSupplierNameSelect.DataValueField = "SuppCode";
        txtSupplierNameSelect.DataBind();

    }

    protected void txtSupplierNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtSupplierNameSelect.SelectedValue != "-Select-")
        {
            txtSupplierName.Text = txtSupplierNameSelect.SelectedItem.Text;
            txtSuppCodehide.Value = txtSupplierNameSelect.SelectedValue;
        }
        else
        {
            txtSupplierName.Text = ""; txtSuppCodehide.Value = "";
        }
    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);

    }


    protected void btnStdPurOrdNo_Click(object sender, EventArgs e)
    {
        //modalPop_StdPurOrdNo.Show();
    }
    private void Load_Data_Empty_StdPurOrdNo()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtStdPurOrdNo.Items.Clear();

        if (ddlPurType.SelectedItem.Text == "Coral")
        {
            SSQL = " Select Std_PO_No from Trans_Coral_PurOrd_Main Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and Active!='Delete' ";

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = SSQL + " And MatType='1' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = SSQL + " And MatType='2' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = SSQL + " And MatType='3' ";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = SSQL + " And MatType='4' ";
            }

            SSQL = SSQL + " Order by Std_PO_No Desc";
        }

        else if (ddlPurType.SelectedItem.Text == "Enercon")
        {
            SSQL = " Select Std_PO_No from Trans_Enercon_PurOrd_Main Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and Active!='Delete' Order by Std_PO_No Desc ";
        }
        else if (ddlPurType.SelectedItem.Text == "Escrow")
        {
            SSQL = " Select Std_PO_No from Trans_Escrow_PurOrd_Main Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and Active!='Delete' ";

            if (ddlMatType.SelectedItem.Text == "RawMaterial")
            {
                SSQL = SSQL + " And MatType='1' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = SSQL + " And MatType='2' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = SSQL + " And MatType='3' ";
            }
            else if (ddlMatType.SelectedItem.Text == "General Items")
            {
                SSQL = SSQL + " And MatType='4' ";
            }

            SSQL = SSQL + " Order by Std_PO_No Desc ";
        }
        else
        {
            SSQL = "Select * from ( ";

            SSQL = SSQL + " Select Std_PO_No from Trans_Coral_PurOrd_Main Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and Active!='Delete' ";

            SSQL = SSQL + "Union All";

            SSQL = SSQL + " Select Std_PO_No from Trans_Enercon_PurOrd_Main Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and Active!='Delete' ";

            SSQL = SSQL + "Union All";

            SSQL = SSQL + " Select Std_PO_No from Trans_Escrow_PurOrd_Main Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and Active!='Delete' ";

            SSQL = SSQL + " ) A Order By A.Std_PO_No Desc";
        }

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtStdPurOrdNo.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();

        dr["Std_PO_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtStdPurOrdNo.DataTextField = "Std_PO_No";

        txtStdPurOrdNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Std_PO_No,Std_PO_Date from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And PO_Status='1'"; 
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_StdPurOrdNo.DataSource = DT;
        //Repeater_StdPurOrdNo.DataBind();
        //Repeater_StdPurOrdNo.Visible = true;

    }

    protected void GridViewClick_StdPurOrdNo(object sender, CommandEventArgs e)
    {
        txtStdPurOrdNo.Text = Convert.ToString(e.CommandArgument);
        //txtStdPurOrdDateHide.Value = Convert.ToString(e.CommandName);

    }

    protected void btnPendingPO_With_Receipt_Click(object sender, EventArgs e)
    {
        Boolean ErrFlg = false;

        RptName = "Pending Standard Purchase Order With Receipt Details";

        if (ddlPurType.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Supplier Type..');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Supplier Type..');", true);
        }

        if (!ErrFlg)
        {
            if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
            {
                StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
            }
            else
            {
                StdPurOrdNo = "";
            }

            if (ddlPurType.SelectedItem.Text != "-Select-")
            {
                SuppType = ddlPurType.SelectedItem.Text;
            }
            else
            {
                SuppType = "";
            }

            if (ddlMatType.SelectedItem.Text != "-Select-")
            {
                MatType = ddlMatType.SelectedItem.Text;
            }
            else
            {
                MatType = "";
            }

            ResponseHelper.Redirect("ReportDisplay.aspx?SuppType=" + SuppType + "&MatType=" + MatType + "&SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
        }
    }

    protected void ddlPurType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_StdPurOrdNo();
    }

    protected void btnPurOrdList_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataGrid GvExcel = new DataGrid();
        string daycolspan = "";
        string tblMainName = "";
        string tblSubName = "";
        bool errflg = false;

        if (ddlPurType.SelectedItem.Text == "Coral")
        {
            tblMainName = "Trans_Coral_PurOrd_Main";
            tblSubName = "Trans_Coral_PurOrd_Sub";
        }
        else if (ddlPurType.SelectedItem.Text == "Enercon")
        {
            tblMainName = "Trans_Enercon_PurOrd_Main";
            tblSubName = "Trans_Enercon_PurOrd_Sub";
        }
        else if (ddlPurType.SelectedItem.Text == "Escrow")
        {
            tblMainName = "Trans_Escrow_PurOrd_Main";
            tblSubName = "Trans_Escrow_PurOrd_Sub";
        }
        else
        {
            errflg = true;
        }


        if (!errflg)
        {
            SSQL = "Select PM.Std_PO_No[Order No],PM.Std_PO_Date[Order Date],PM.RefNo[Coral Order No],PM.DeptName[Department],PM.Supp_Name[Supplier],";
            SSQL = SSQL + " PM.PaymentTerms,PM.DeliveryAt,PS.SAPNo,PS.ItemName,PS.UOMCode[UOM],PS.OrderQty,PS.Rate,PS.ItemTotal,";
            SSQL = SSQL + " PS.CalWeek,PM.CurrencyType,PM.TaxType,PS.CGSTPer,PS.CGSTAmount,PS.SGSTPer,PS.SGSTAmount,PS.IGSTPer,PS.IGSTAmount,";
            SSQL = SSQL + " PS.LineTotal From " + tblMainName + " PM ";
            SSQL = SSQL + " Inner Join " + tblSubName + " PS on PS.Std_PO_No=PM.Std_PO_No and PS.Ccode=PM.Ccode and PS.LCode=PM.LCode ";
            SSQL = SSQL + " And PS.Finyearcode=PM.FinYearCode";
            SSQL = SSQL + " Where PM.Ccode = '" + SessionCcode + "' And PM.Lcode = '" + SessionLcode + "' ";
            SSQL = SSQL + " And PM.FinYearCode = '" + SessionFinYearCode + "' And PM.Active!='Delete' And PS.Active!='Delete' ";

            //--And PM.PO_Status = '1'

            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                SSQL = SSQL + " And Convert (DateTime,PM.Std_PO_Date, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                SSQL = SSQL + " And Convert (DateTime,PM.Std_PO_Date, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
            }

            if (txtDepartmentName.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PM.DeptName='" + txtDepartmentName.SelectedItem.Text + "' ";
            }

            if (txtSupplierNameSelect.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PM.Supp_Name='" + txtSupplierNameSelect.SelectedItem.Text + "' ";
            }

            if (ddlSAPNo.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PS.SAPNo='" + ddlSAPNo.SelectedItem.Text + "' ";
            }

            if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PM.Std_PO_No='" + txtStdPurOrdNo.SelectedItem.Text + "' ";
            }

            if (ddlTaxType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PM.TaxType='" + ddlTaxType.SelectedItem.Text + "' ";
            }

            if (ddlMatType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PM.MatType='" + ddlMatType.SelectedValue + "' ";
            }

            SSQL = SSQL + " Order by PM.Std_PO_No ";
            //SSQL = SSQL + " Order by CONVERT(datetime,PM.Std_PO_Date,105) ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                string attachment = "attachment;filename=PurchaseOrderListNew_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 23) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 23) + "'>");
                Response.Write("<a style=\"font-weight:bold\">Purchase Order List</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");


                if (txtFromDate.Text == "" && txtToDate.Text == "")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 23) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> As On Date -" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 23) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }

                if (ddlPurType.SelectedItem.Text == "Coral")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 23) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : CORAL </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Enercon")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 23) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : ENERCON </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Escrow")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 23) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : ESCROW </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                Response.Write("</table>");

                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
            }
        }
        else
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select Supplier Type');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Supplier Type..');", true);
        }
    }
    protected void btnOrdList_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataGrid GvExcel = new DataGrid();
        string daycolspan = "";
        string tblName = "";
        Boolean ErrFlg = false;

        if (ddlPurType.SelectedItem.Text == "Coral")
        {
            tblName = "Trans_Coral_PurOrd_Main";
        }
        else if (ddlPurType.SelectedItem.Text == "Enercon")
        {
            tblName = "Trans_Enercon_PurOrd_Main";
        }
        else if (ddlPurType.SelectedItem.Text == "Escrow")
        {
            tblName = "Trans_Escrow_PurOrd_Main";
        }
        else
        {
            ErrFlg = true;
        }

        if (!ErrFlg)
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No,Pur_Request_Date,Supp_Qtn_No,";
            SSQL = SSQL + " Supp_Qtn_Date,DeliveryMode,DeliveryDate,DeliveryAt,PaymentMode,DeptName,PaymentTerms,";
            SSQL = SSQL + " Description,Note,Others,TotalAmt,Discount,TaxPer,TaxAmount,OtherCharge,NetAmount ";
            SSQL = SSQL + " From " + tblName + " Where Ccode = '" + SessionCcode + "' And Lcode = '" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode = '" + SessionFinYearCode + "' ";// And PO_Status = '1' ";

            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                SSQL = SSQL + " And Convert (DateTime,Std_PO_Date, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                SSQL = SSQL + " And Convert (DateTime,Std_PO_Date, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
            }

            if (txtDepartmentName.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And DeptName='" + txtDepartmentName.SelectedItem.Text + "' ";
            }

            if (txtSupplierNameSelect.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And Supp_Name='" + txtSupplierNameSelect.SelectedItem.Text + "' ";
            }

            if (ddlMatType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And MatType='" + ddlMatType.SelectedValue + "' ";
            }


            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                string attachment = "attachment;filename=PurchaseOrderList_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");
                Response.Write("<a style=\"font-weight:bold\">Purchase Order List</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");

                Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                Response.Write("&nbsp;&nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                Response.Write("  ");
                Response.Write("</td>");
                Response.Write("</tr>");


                if (ddlPurType.SelectedItem.Text == "Coral")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : CORAL </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Enercon")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : ENERCON </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Escrow")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : ESCROW </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                Response.Write("</table>");

                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
            }
        }
        else
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Supplier Type');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Any One Supplier Type..');", true);
        }
    }

    protected void btnPurOrdPDF_Click(object sender, EventArgs e)
    {
        if (ddlPurType.SelectedItem.Text != "-Select-")
        {
            string tblMainName = "";
            string tblSubName = "";

            if (ddlPurType.SelectedItem.Text == "Coral")
            {
                tblMainName = "Trans_Coral_PurOrd_Main";
                tblSubName = "Trans_Coral_PurOrd_Sub";
            }
            else if (ddlPurType.SelectedItem.Text == "Enercon")
            {
                tblMainName = "Trans_Enercon_PurOrd_Main";
                tblSubName = "Trans_Enercon_PurOrd_Sub";
            }
            else if (ddlPurType.SelectedItem.Text == "Escrow")
            {
                tblMainName = "Trans_Escrow_PurOrd_Main";
                tblSubName = "Trans_Escrow_PurOrd_Sub";
            }

            if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
            {
                StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
            }
            else
            {
                StdPurOrdNo = "";
            }

            if (ddlMatType.SelectedItem.Text != "-Select-")
            {
                MatType = ddlMatType.SelectedValue;
            }
            else
            {
                MatType = "";
            }

            if (txtDepartmentName.SelectedItem.Text != "-Select-")
            {
                DeptName = txtDepartmentName.SelectedItem.Text;
            }
            else
            {
                DeptName = "";
            }

            if (ddlPurType.SelectedItem.Text != "-Select-")
            {
                SuppType = ddlPurType.SelectedItem.Text;
            }
            else
            {
                SuppType = "";
            }

            if (ddlSAPNo.SelectedItem.Text != "-Select-")
            {
                SapNo = ddlSAPNo.SelectedItem.Text;
            }
            else
            {
                SapNo = "";
            }

            RptName = "Purchase Order Details Report";
            ResponseHelper.Redirect("ReportDisplay.aspx?SapNo=" + SapNo + "&tblMainName=" + tblMainName + "&tblSubName=" + tblSubName + "&SuppType=" + SuppType + "&DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
        }
        else
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Supplier Type');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
        }
    }

    protected void btnAmendList_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataGrid GvExcel = new DataGrid();
        string daycolspan = "";
        string tblNameMain = "";
        string tblNameSub = "";
        Boolean ErrFlg = false;

        //if (ddlPurType.SelectedItem.Text == "Coral")
        //{
        //    tblNameMain = "Trans_Coral_PurOrd_Main";
        //    tblNameSub = "";
        //}
        //else if (ddlPurType.SelectedItem.Text == "Enercon")
        //{
        //    tblNameMain = "Trans_Enercon_PurOrd_Main";
        //    tblNameSub = "";
        //}
        //else if (ddlPurType.SelectedItem.Text == "Escrow")
        //{
        tblNameMain = "Trans_Escrow_PurOrd_Amend_Main";
        tblNameSub = "Trans_Escrow_PurOrd_Amend_Sub";
        //}
        //else
        //{
        //    ErrFlg = true;
        //}

        if (!ErrFlg)
        {
            SSQL = "Select PAM.AmendPONo,PAM.AmendPoDate,PAM.PurOrdNo,PAM.PurOrdDate,PAM.Supp_Name,PAM.Pur_Request_No,";
            SSQL = SSQL + " PAM.Pur_Request_Date,PAM.Supp_Qtn_No, PAM.Supp_Qtn_Date,PAM.DeliveryMode,PAM.DeliveryDate,";
            SSQL = SSQL + " PAM.DeliveryAt,PAM.DeptName,PAM.PaymentTerms, PAM.Description,PAM.Note,PAM.MatType, ";
            SSQL = SSQL + " PAM.TotalQuantity,PAM.TotalAmt,PAM.NetAmount ";
            SSQL = SSQL + " From Trans_Escrow_PurOrd_Amend_Main PAM ";
            SSQL = SSQL + " Where PAM.Ccode = '" + SessionCcode + "' And PAM.Lcode = '" + SessionLcode + "'  ";
            SSQL = SSQL + " And PAM.FinYearCode = '" + SessionFinYearCode + "' ";
            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                SSQL = SSQL + " And Convert (DateTime,PAM.AmendPoDate, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                SSQL = SSQL + " And Convert (DateTime,PAM.AmendPoDate, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
            }

            if (txtDepartmentName.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PAM.DeptName='" + txtDepartmentName.SelectedItem.Text + "' ";
            }

            if (txtSupplierNameSelect.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PAM.Supp_Name='" + txtSupplierNameSelect.SelectedItem.Text + "' ";
            }

            if (ddlMatType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And PAM.MatType='" + ddlMatType.SelectedValue + "' ";
            }


            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                string attachment = "attachment;filename=PurchaseOrderAmendmentList_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 20) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 20) + "'>");
                Response.Write("<a style=\"font-weight:bold\">Purchase Order Amendment List</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");

                if (txtFromDate.Text == "" && txtToDate.Text == "")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 20) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> As On Date -" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 20) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }


                if (ddlPurType.SelectedItem.Text == "Coral")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 20) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : CORAL </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Enercon")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : ENERCON </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Escrow")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 22) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : ESCROW </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                Response.Write("</table>");

                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
            }
        }
        else
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Supplier Type');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Any One Supplier Type..');", true);
        }
    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_SAPNo();

        Load_Data_Empty_StdPurOrdNo();

        Load_Data_Empty_ItemCode();
    }

    protected void btnPendingPO_With_Receipt_Ecel_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataGrid GvExcel = new DataGrid();
            string daycolspan = "";
            string tblNameMain = "";
            string tblNameSub = "";
            Boolean ErrFlg = false;

            if (ddlPurType.SelectedItem.Text == "Escrow")
            {
                SSQL = "Select P.Std_PO_No,P.Std_PO_Date,P.Supp_Name,P.SAPNo,P.ItemName,P.UOMCode,p.OrderQty,P.ReceivedQty,P.Balance_Qty from (";
                SSQL = SSQL + " Select '1'W,SPS.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPS.SAPNo,SPS.itemName,SPS.UOMCode,SPS.OrderQty,'0' as ReceivedQty,'0' as Balance_Qty from Trans_Escrow_PurOrd_Main SPM";
                SSQL = SSQL + " inner join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode And SPM.FinYearCode=SPS.FinYearCode And SPM.Std_PO_No=SPS.Std_PO_No";
                SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And (SPS.Std_PO_No not in";
                SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " Lcode ='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ) or ";
                SSQL = SSQL + " SPS.RefNo not in ";
                SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " Lcode ='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ))";

                SSQL = SSQL + " UNION ALL";

                SSQL = SSQL + " Select '1'W,SPS.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPS.SAPNo,SPS.itemName,SPS.UOMCode,SPS.OrderQty,'0' as ReceivedQty,'0' as Balance_Qty from Trans_Escrow_PurOrd_Main SPM";
                SSQL = SSQL + " inner join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode And SPM.FinYearCode=SPS.FinYearCode And SPM.Std_PO_No=SPS.Std_PO_No";
                SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.ItemName not in";
                SSQL = SSQL + " (Select ItemName from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And (PurOrdNo=SPS.Std_PO_No or PurOrdNo=SPS.RefNo)) ";
                SSQL = SSQL + " And SPS.Std_PO_No in";
                SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " Lcode ='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And (PurOrdNo=SPS.Std_PO_No or PurOrdNo=SPS.RefNo))";

                SSQL = SSQL + " UNION ALL";

                SSQL = SSQL + " Select '1'W,SPS.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPS.SAPNo,SPS.itemName,SPS.UOMCode,SPS.OrderQty,";
                SSQL = SSQL + " PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty";
                SSQL = SSQL + " from Trans_Escrow_PurOrd_Main SPM ";
                SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode And ";
                SSQL = SSQL + " SPM.FinYearCode=SPS.FinYearCode And SPM.Std_PO_No=SPS.Std_PO_No";
                SSQL = SSQL + " Inner join Trans_GoodsReceipt_Sub PRS on PRS.Ccode=SPS.Ccode And PRS.Lcode=PRS.Lcode And ";
                SSQL = SSQL + " SPS.FinYearCode=PRS.FinYearCode And (PRS.PurOrdNo=SPS.Std_PO_No or PRS.PurOrdNo=SPS.RefNo) And PRS.ItemName=SPS.ItemName";
                SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " group by SPS.Std_PO_No,SPM.Std_PO_Date,SPS.SAPNo,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty";
                SSQL = SSQL + " having (sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0";
                SSQL = SSQL + " ) as P";
                SSQL = SSQL + " Where W='1'";

                if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Std_PO_No='" + StdPurOrdNo + "'";
                }
                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Std_PO_Date, 103) >= CONVERT(DATETIME,'" + txtFromDate.Text + "',103) And ";
                    SSQL = SSQL + " CONVERT(DATETIME,Std_PO_Date, 103) <= CONVERT(DATETIME,'" + txtToDate.Text + "',103)";
                }
                if (txtItemNameSelect.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And ItemName='" + txtItemNameSelect.SelectedItem.Text + "'";
                }
                if (txtSupplierNameSelect.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Supp_Name='" + txtSupplierNameSelect.SelectedItem.Text + "'";
                }

                SSQL = SSQL + " order by Std_PO_No,ItemName";
            }
            else if (ddlPurType.SelectedItem.Text == "Coral")
            {
                SSQL = "Select * from (";
                SSQL = SSQL + " Select '1' W,SPS.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPS.itemName,SPS.UOMCode,SPS.OrderQty,'0' as ReceivedQty,'0' as Balance_Qty from Trans_Coral_PurOrd_Main SPM";
                SSQL = SSQL + " inner join Trans_Coral_PurOrd_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode And SPM.FinYearCode=SPS.FinYearCode And SPM.Std_PO_No=SPS.Std_PO_No";
                SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.Std_PO_No not in";
                SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "')";

                SSQL = SSQL + " UNION ALL";

                SSQL = SSQL + " Select  '1' W,SPS.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPS.itemName,SPS.UOMCode,SPS.OrderQty,'0' as ReceivedQty,'0' as Balance_Qty from Trans_Coral_PurOrd_Main SPM";
                SSQL = SSQL + " inner join Trans_Coral_PurOrd_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode And SPM.FinYearCode=SPS.FinYearCode And SPM.Std_PO_No=SPS.Std_PO_No";
                SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.ItemName not in";
                SSQL = SSQL + " (Select ItemName from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PurOrdNo=SPS.Std_PO_No)";
                SSQL = SSQL + " And SPS.Std_PO_No in";
                SSQL = SSQL + " (Select PurOrdNo as Pur_Order_No from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PurOrdNo=SPS.Std_PO_No)";

                SSQL = SSQL + " UNION ALL";

                SSQL = SSQL + " Select '1' W,SPS.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPS.itemName,SPS.UOMCode,SPS.OrderQty,PRS.ReceiveQty as ReceivedQty,(sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) as Balance_Qty";
                SSQL = SSQL + " from Trans_Coral_PurOrd_Main SPM inner join Trans_Coral_PurOrd_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode And SPM.FinYearCode=SPS.FinYearCode And SPM.Std_PO_No=SPS.Std_PO_No";
                SSQL = SSQL + " inner join Trans_GoodsReceipt_Sub PRS on PRS.Ccode=SPS.Ccode And PRS.Lcode=PRS.Lcode And SPS.FinYearCode=PRS.FinYearCode And PRS.PurOrdNo=SPS.Std_PO_No And PRS.ItemName=SPS.ItemName";
                SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " group by SPS.Std_PO_No,SPM.Std_PO_Date,SPS.itemName,SPS.UOMCode,SPS.OrderQty,SPM.Supp_Name,PRS.ReceiveQty";
                SSQL = SSQL + " having (sum(SPS.OrderQty) - sum(PRS.ReceiveQty)) > 0";
                SSQL = SSQL + " ) as P";
                SSQL = SSQL + " Where W='1'";

                if (StdPurOrdNo != "")
                {
                    SSQL = SSQL + " And Std_PO_No='" + StdPurOrdNo + "'";
                }
                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Std_PO_Date, 103) >= CONVERT(DATETIME,'" + txtFromDate.Text + "',103) And ";
                    SSQL = SSQL + " CONVERT(DATETIME,Std_PO_Date, 103) <= CONVERT(DATETIME,'" + txtToDate.Text + "',103)";
                }
                if (txtItemNameSelect.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And ItemName='" + txtItemNameSelect.SelectedItem.Text + "'";
                }
                if (txtSupplierNameSelect.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Supp_Name='" + txtSupplierNameSelect.SelectedItem.Text + "'";
                }

                SSQL = SSQL + " order by Std_PO_No,ItemName";
            }
            //else
            //{
            //    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Supplier Type');", true);
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Any One Supplier Type..');", true);
            //}

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                string attachment = "attachment;filename=PurOrd_Pending_GRN_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");
                Response.Write("<a style=\"font-weight:bold\">PENDING PURCHASE ORDER WITH GRN DETAILS</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");

                if (txtFromDate.Text == "" && txtToDate.Text == "")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> As On Date -" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }


                if (ddlPurType.SelectedItem.Text == "Coral")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : CORAL </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Enercon")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : ENERCON </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlPurType.SelectedItem.Text == "Escrow")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> SupplierType : ESCROW </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                Response.Write("</table>");

                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records..');", true);
            }
        }
        catch (Exception ex)
        {

        }
    }
}



