﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Rpt_New_Reports.aspx.cs" Inherits="Rpt_New_Reports" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
     <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.select2').select2();
                }
            });
        };
    </script>

    <div class="content-wrapper">
        <asp:UpdatePanel ID="upChallMain" runat="server">
            <ContentTemplate>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Auditor Report</span></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding" >             
                            <div class="col-md-12">
                                <div class="col-md-5" >
                                    <div class="panel panel-white">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading clearfix">
                                                <h3 class="panel-title">Report Type</h3>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div>
                                                <div class="form-group">
                                                    <label for="exampleInputName">REPORT NAME</label>
                                                    <asp:DropDownList ID="ddlRptName" runat="server" class="form-control select2"
                                                     AutoPostBack="true" OnSelectedIndexChanged="ddlRptName_SelectedIndexChanged" >
                                                        <asp:ListItem Value="-Select-" Text="- select -"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="ALL"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="DEBITORS"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="CREDITORS"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="AUDITOR"></asp:ListItem>
                                                    </asp:DropDownList>   
                                                </div>
                                                <div>
                                                    <asp:ListBox ID="ListRptName" runat="server" Width="440px" Height="409px"  class="form-control"
                                                        Font-Bold="True" Font-Names="Times New Roman" Font-Size="150%" AutoPostBack="true" 
                                                        OnSelectedIndexChanged="ListRptName_SelectedIndexChanged">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:ListBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7" >
                                    <div class="panel panel-white">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading clearfix">
                                                <h3 class="panel-title">List</h3>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-6" runat="server" >
                                                    <label for="exampleInputName">Accounts Type</label>
                                                    <asp:DropDownList ID="ddlLedgerGrp" runat="server" class="form-control select2" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlLedgerGrp_SelectedIndexChanged">
                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="1">Enercon</asp:ListItem>
                                                        <asp:ListItem Value="2">Escrow</asp:ListItem>
                                                        <asp:ListItem Value="3">Coral</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-sm-6" runat="server" >
                                                    <label for="exampleInputName">Material Type</label>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" class="form-control select2" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlLedgerGrp_SelectedIndexChanged">
                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="1">RawMaterial</asp:ListItem>
                                                        <asp:ListItem Value="2">Tools</asp:ListItem>
                                                        <asp:ListItem Value="3">Asset</asp:ListItem>
                                                        <asp:ListItem Value="4">General Items</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>

                                            <div class="row" style="padding-top:15px">
                                                <div class="col-sm-6">
                                                    <label for="exampleInputName">Ledger Name</label>
                                                    <asp:DropDownList ID="ddlLedgerName" runat="server" class="form-control select2" ></asp:DropDownList>
                                                </div>

                                                <div class="col-sm-6" runat="server" visible="false">
                                                    <label for="exampleInputName">Account Type</label>
                                                    <asp:DropDownList ID="ddlAccType" runat="server" class="form-control select2" ></asp:DropDownList>
                                                </div>

                                                <div class="col-sm-6" runat="server">
                                                    <label for="exampleInputName">Bank Name</label>
                                                    <asp:DropDownList ID="ddlBankName" runat="server" class="form-control select2" ></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:15px">

                                                <div class="col-sm-6" runat="server">
                                                    <label for="exampleInputName">Agent Name</label>
                                                    <asp:DropDownList ID="ddlAgentName" runat="server" class="form-control select2" ></asp:DropDownList>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="exampleInputName">Month</label>
                                                    <asp:DropDownList ID="ddlMonth" runat="server" class="form-control select2" AutoPostBack="true" 
                                                        OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row" style="padding-top:15px">
                                                <div class="col-sm-6">
                                                    <label for="exampleInputName">From Date</label>
                                                        <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtFromDate" ValidChars="0123456789./">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="exampleInputName">To Date</label>
                                                        <asp:TextBox ID="txtToDate" MaxLength="20" class="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtToDate" ValidChars="0123456789./">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:15px">
                                                <div class="col-sm-6">
                                                    <label for="exampleInputName">Filter</label>
                                                    <asp:DropDownList ID="ddlGL" runat="server" class="form-control select2" AutoPostBack="true" 
                                                        OnSelectedIndexChanged="ddlGL_SelectedIndexChanged">
                                                        <asp:ListItem Value="1">Grater than</asp:ListItem>
                                                        <asp:ListItem Value="2">Less than</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="exampleInputName">Amount</label>
                                                    <asp:Textbox ID="txtAmt" runat="server" class="form-control" AutoPostBack="true"> 
                                                    </asp:Textbox>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:15px">
                                                <div class="col-sm-6">
                                                    <label for="exampleInputName">Financial Period</label>
                                                    <asp:DropDownList ID="ddlFinPeriod" runat="server" class="form-control select2">
                                                        <asp:ListItem Text="0">-Select-</asp:ListItem>
                                                        <asp:ListItem Text="1">First Quarter (Q1)</asp:ListItem>
                                                        <asp:ListItem Text="2">Second Quarter (Q2)</asp:ListItem>
                                                        <asp:ListItem Text="3">Third Quarter (Q3)</asp:ListItem>
                                                        <asp:ListItem Text="4">Four Quarter (Q4)</asp:ListItem>
                                                        <asp:ListItem Text="5">First Half</asp:ListItem>
                                                        <asp:ListItem Text="2">Second Half</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                            <div class="box-footer" style="padding-top:25px">
                                                <div class="form-group text-center">
                                                    <asp:Button ID="btnExcel" class="btn btn-primary" runat="server" Text="Excel" OnClick="btnAgentList_Click"/>
                                                    <asp:Button ID="btnReport" class="btn btn-primary" runat="server" Text="Report" OnClick="btnTransAll_Click" />
                                                    <asp:Button ID="btnClear" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="upTrigPanl" runat="server">
                                    <ContentTemplate></ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnExcel" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </ContentTemplate>
</asp:UpdatePanel>
</div>
</asp:Content>

