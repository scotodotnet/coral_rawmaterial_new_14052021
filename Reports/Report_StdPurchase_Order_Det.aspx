﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Report_StdPurchase_Order_Det.aspx.cs" Inherits="Reports_Report_StdPurchase_Order_Det" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <section class="content-header">
                    <h1><i class=" text-primary"></i>Purchase Order Report</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Type</label>
                                                <asp:DropDownList ID="ddlPurType" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlPurType_SelectedIndexChanged">
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">Enercon</asp:ListItem>
                                                    <asp:ListItem Value="2">Escrow</asp:ListItem>
                                                    <asp:ListItem Value="3">Coral</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Material Type</label>
                                                <asp:DropDownList ID="ddlMatType" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">RawMaterial</asp:ListItem>
                                                    <asp:ListItem Value="2">Tools</asp:ListItem>
                                                    <asp:ListItem Value="3">Asset</asp:ListItem>
                                                    <asp:ListItem Value="4">General Items</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Department</label>
                                                <asp:DropDownList ID="txtDepartmentName" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier</label>
                                                <asp:DropDownList ID="txtSupplierNameSelect" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtSupplierNameSelect_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                <asp:HiddenField ID="txtSuppCodehide" runat="server" />
                                            </div>
                                        </div>




                                    </div>
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Std.Pur_Order.No</label>
                                                <asp:DropDownList ID="txtStdPurOrdNo" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Sup.Qut.No</label>
                                                <asp:DropDownList ID="txtSPONo" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Material Name</label>
                                                <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtItemNameSelect_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtItemCode" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                <asp:HiddenField ID="txtItemCodeHide" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">SAP No</label>
                                                <asp:DropDownList ID="ddlSAPNo" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Tax Type</label>
                                                <asp:DropDownList ID="ddlTaxType" runat="server" class="form-control select2">
                                                    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">GST</asp:ListItem>
                                                    <asp:ListItem Value="2">IGST</asp:ListItem>
                                                    <asp:ListItem Value="3">NONE</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>



                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">From Date</label>
                                                <asp:TextBox ID="txtFromDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">To Date</label>
                                                <asp:TextBox ID="txtToDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnPurOrdList" class="btn btn-primary" runat="server" Text="Purchase Order List(New)" OnClick="btnPurOrdList_Click" />
                                        <asp:Button ID="btnPurOrdPDF" class="btn btn-primary" runat="server" Text="Purchase Order(PDF)" OnClick="btnPurOrdPDF_Click" />
                                        <asp:Button ID="btnOrdList" class="btn btn-primary" runat="server" Text="Purchase Order List" OnClick="btnOrdList_Click" />
                                        <asp:Button ID="btnAmendList" class="btn btn-primary" runat="server" Text="Purchase Amendment List" OnClick="btnAmendList_Click" />
                                        <asp:Button ID="btnReport" class="btn btn-primary" runat="server" Text="Details Report" OnClick="btnReport_Click" />
                                        <asp:Button ID="btnInvcFormat" class="btn btn-primary" runat="server" Text="Invoice Format" OnClick="btnInvcFormat_Click" Visible="false" />
                                        <asp:Button ID="btnPendingPO_With_Receipt_Ecel" class="btn btn-primary" runat="server" Text="PendingPO With Receipt (Excel)" OnClick="btnPendingPO_With_Receipt_Ecel_Click"/>
                                        <asp:Button ID="btnPendingPO_With_Receipt" class="btn btn-primary" runat="server" Text="PendingPO With Receipt" OnClick="btnPendingPO_With_Receipt_Click" />
                                        <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                    </div>
                                </div>
                                <!-- /.box-footer-->

                                <asp:UpdatePanel ID="upExcel" runat="server">
                                    <ContentTemplate>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnPurOrdList" />
                                        <asp:PostBackTrigger ControlID="btnOrdList" />
                                        <asp:PostBackTrigger ControlID="btnAmendList" />
                                        <asp:PostBackTrigger ControlID="btnPendingPO_With_Receipt_Ecel" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <!-- /.box -->
                        </div>

                    </div>

                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: 'true' });
                    $('.select2').select2();
                }
            });
        };
    </script>

</asp:Content>

