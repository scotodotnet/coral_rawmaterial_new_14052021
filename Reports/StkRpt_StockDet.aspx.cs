﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class StkRpt_StockDet : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string RptName = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Stock Details";

            Load_Data_Supplier();
            Load_Data_Item();
            Load_Data_Warehouse();
            Load_Data_Department();
            Load_Data_Assembly();
        }
    }


    private void Load_Data_Assembly()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select TransCode,AssemblyStageName from MstAssetAssemblyStage where Ccode='" + SessionCcode + "' ";
        SSQL = SSQL + " and Status !='Delete' and AssemblyStageName!='Final Assembly' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlPreAssembly.DataSource = DT;

        dr = DT.NewRow();

        dr["TransCode"] = "-Select-";
        dr["AssemblyStageName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlPreAssembly.DataValueField = "TransCode";
        ddlPreAssembly.DataTextField = "AssemblyStageName";
        ddlPreAssembly.DataBind();
    }

    private void Load_Data_Warehouse()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status='Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlWarehouse.DataSource = DT;

        dr = DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlWarehouse.DataValueField = "WarehouseCode";
        ddlWarehouse.DataTextField = "WarehouseName";
        ddlWarehouse.DataBind();
    }

    private void Load_Data_Department()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select DeptCode,DeptName from MstDepartment  where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status='Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDept.DataSource = DT;

        dr = DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlDept.DataValueField = "DeptCode";
        ddlDept.DataTextField = "DeptName";
        ddlDept.DataBind();
    }

    private void Load_Data_Supplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger  where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status='Add' And LedgerGrpName='Supplier'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlSupplier.DataSource = DT;

        dr = DT.NewRow();
        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlSupplier.DataValueField = "LedgerCode";
        ddlSupplier.DataTextField = "LedgerName";
        ddlSupplier.DataBind();
    }

    private void Load_Data_Item()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "select Mat_No,Raw_Mat_Name from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status <> 'Delete'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlItem.DataSource = DT;

        dr = DT.NewRow();
        dr["Mat_No"] = "-Select-";
        dr["Raw_Mat_Name"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItem.DataValueField = "Mat_No";
        ddlItem.DataTextField = "Raw_Mat_Name";
        ddlItem.DataBind();
    }

    protected void btnRecList_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataGrid GvExcel = new DataGrid();
        string daycolspan = "";

        SSQL = "select GRNo[Receipt No],GRDate[Receipt Date],PurOrdNo[Purchase Order No],PurOrdDate[Purchase Order Date],SuppName[Supplier Name],";
        SSQL = SSQL + " PayTerms[Payment Terms],PayMode[Payment Mode],VehicleNo[Vecicle No],Notes[Notes],Remarks[Remarks],TotalQty[Received Qty],";
        SSQL = SSQL + " RoundOff[Round Off],TotalAmount[NetAmount] from Trans_GoodsReceipt_Main where CCode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";


        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            SSQL = SSQL + " And Convert (DateTime,GRNo, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
            SSQL = SSQL + " And Convert (DateTime,GRNo, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
        }


        if (ddlSupplier.SelectedItem.Text != "-Select-")
        {
            SSQL = SSQL + " And SuppName='" + ddlSupplier.SelectedItem.Text + "'";
        }



        if (ddlItem.SelectedItem.Text != "-Select-")
        {
            //SSQL = SSQL + " And SuppName='" + ddlCust.SelectedItem.Text + "'";
        }

        SSQL = SSQL + " And Status =''";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        string attachment = "attachment;filename=GoodsReceived.xls";
        Response.Clear();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        GvExcel.DataSource = DT;
        GvExcel.DataBind();

        GvExcel.HeaderStyle.Font.Bold = true;

        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        GvExcel.RenderControl(htextw);

        Response.Write("<table>");

        Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
        Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 11) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
        //Response.Write("--");
        //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 11) + "'>");
        Response.Write("<a style=\"font-weight:bold\">Goods Received Report</a>");
        Response.Write("  ");

        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");

        Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 11) + "'>");

        Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
        Response.Write("&nbsp;&nbsp;&nbsp;");
        Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
        Response.Write("  ");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();
    }

    protected void btnStockDet_Click(object sender, EventArgs e)
    {
        RptName = "Stock Details Reports";
        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?AccType=" + ddlSupplierType.SelectedValue + "&MatType=" + ddlMatType.SelectedValue + "&SuppName=" + ddlSupplier.SelectedItem.Text + "&ItemName=" + ddlItem.SelectedItem.Text + "&DeptName=" + ddlDept.SelectedItem.Text + "&Warehouse=" + ddlWarehouse.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blanck", "");

    }
    protected void btnStkConsRet_Click(object sender, EventArgs e)
    {
        RptName = "Stock Consolidate Report";

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?SuppName=" + ddlSupplier.SelectedItem.Text + "&ItemName=" + ddlItem.SelectedItem.Text + "&DeptName=" + ddlDept.SelectedItem.Text + "&Warehouse=" + ddlWarehouse.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blanck", "");
    }

    protected void btnDeptWise_Click(object sender, EventArgs e)
    {
        RptName = "Department Wise Stock";
        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?SuppName=" + ddlSupplier.SelectedItem.Text + "&ItemName=" + ddlItem.SelectedItem.Text + "&DeptName=" + ddlDept.SelectedItem.Text + "&Warehouse=" + ddlWarehouse.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blanck", "");
    }

    protected void btnItemRackWise_Click(object sender, EventArgs e)
    {
        RptName = "Item Rack Wise Stock";
        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?ItemCode=" + ddlItem.SelectedValue + "&WarehouseCode=" + ddlWarehouse.SelectedValue + "&RptName=" + RptName, "_blanck", "");
    }

    protected void btnStkDet_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataGrid GvExcel = new DataGrid();
        string daycolspan = "";
        bool ErrFlag = false;

        if (ddlMatType.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Any One Material Type');", true);
        }

        if (!ErrFlag)
        {
            if (ddlMatType.SelectedItem.Text == "Raw Material")
            {
                SSQL = "Select STK.SapNo[Sap No],Stk.ItemName[Item Description],Stk.Deptname[Department Name],Stk.WarehouseName[Location Name],Stk.UOM, ";
                SSQL = SSQL + " Sum(Stk.Add_Qty)-sum(Stk.Minus_Qty) [Stock Qty],CurrencyType, ";
                SSQL = SSQL + " Cast((sum(Stk.Add_Value)-sum(Stk.Minus_Value))/(sum(Stk.Add_Qty)-sum(Stk.Minus_Qty)) as decimal(18,2)) [Rate],";
                SSQL = SSQL + " Sum(Stk.Add_Value)-sum(Stk.Minus_Value) [Stock Value] ";
                SSQL = SSQL + " From Trans_Stock_Ledger_All Stk where ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' ";
                //And FinYearVal='" + SessionFinYearVal + "' ";
                SSQL = SSQL + " and STK.Mat_Type='1' and STK.Add_Qty!='0' ";

                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {
                    SSQL = SSQL + " And Convert (DateTime,Stk.Trans_Date, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                    SSQL = SSQL + " And Convert (DateTime,Stk.Trans_Date, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
                }

                if (ddlSupplierType.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.Supp_Type='" + ddlSupplierType.SelectedValue + "' ";
                }

                if (ddlItem.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.ItemName='" + ddlItem.SelectedItem.Text + "'";
                }

                if (ddlDept.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.Deptname='" + ddlDept.SelectedItem.Text + "'";
                }

                if (ddlWarehouse.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.WarehouseName='" + ddlWarehouse.SelectedItem.Text + "'";
                }

                if (ddlSupplier.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Supp_Name='" + ddlSupplier.SelectedItem.Text + "'";
                }

                if (ddlStkType.SelectedItem.Text != "-Select-")
                {

                    if (ddlStkType.SelectedItem.Text == "Un Restricted Stock")
                    {
                        SSQL = SSQL + " And ReceiveType='Direct' ";
                    }
                    else 
                    {
                        SSQL = SSQL + " And ReceiveType='" + ddlStkType.SelectedItem.Text + "' ";
                    }

                }

                SSQL = SSQL + " Group By STK.SapNo,Stk.ItemCode,Stk.ItemName,Stk.DeptCode,Stk.Deptname,Stk.WarehouseCode,Stk.WarehouseName,";
                SSQL = SSQL + " Stk.CurrencyType,Stk.UOM  ";
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select TL.RefCode[Reference Code],";

                //if()
                //SSQL = SSQL + "'Pre-Assembly' ToolsType";
                //SSQL = SSQL + "'Final Assembly' ToolsType";
                SSQL = SSQL + "Stk.ItemName[Item Description],STK.SapNo[Sap No],";
                SSQL = SSQL + " Stk.Deptname[Department Name],Stk.WarehouseName[Location Name],Stk.UOM, ";
                SSQL = SSQL + " Sum(Stk.Add_Qty)-sum(Stk.Minus_Qty) [Stock Qty],CurrencyType, ";
                SSQL = SSQL + " Cast((sum(Stk.Add_Value)-sum(Stk.Minus_Value))/(sum(Stk.Add_Qty)-sum(Stk.Minus_Qty)) as decimal(18,2)) [Rate],";
                SSQL = SSQL + " Sum(Stk.Add_Value)-sum(Stk.Minus_Value) [Stock Value] ";
                SSQL = SSQL + " From Trans_Stock_Ledger_All Stk ";
                SSQL = SSQL + " inner Join MstTools TL on TL.ToolCode= Stk.ItemCode and TL.Status!='Delete' ";
                SSQL = SSQL + " where Stk.ccode='" + SessionCcode + "' And Stk.LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Stk.FinYearCode='" + SessionFinYearCode + "' ";
                //And Stk.FinYearVal='" + SessionFinYearVal + "' ";
                SSQL = SSQL + " and STK.Mat_Type='2' and STK.Add_Qty!='0' and Cast(Stk.Add_Value as decimal(18, 2)) > cast('1.00' as decimal(18, 2))";

                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {
                    SSQL = SSQL + " And Convert (DateTime,Stk.Trans_Date, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                    SSQL = SSQL + " And Convert (DateTime,Stk.Trans_Date, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
                }

                if (ddlSupplierType.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.Supp_Type='" + ddlSupplierType.SelectedValue + "' ";
                }

                if (ddlItem.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.ItemName='" + ddlItem.SelectedItem.Text + "'";
                }

                if (ddlDept.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.Deptname='" + ddlDept.SelectedItem.Text + "'";
                }

                if (ddlWarehouse.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.WarehouseName='" + ddlWarehouse.SelectedItem.Text + "'";
                }

                if (ddlSupplier.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Supp_Name='" + ddlSupplier.SelectedItem.Text + "'";
                }

                if (ddlProductstage.SelectedItem.Text == "Final Assembly")
                {
                    SSQL = SSQL + " And TL.AssemblyStageName='Final Assembly' ";
                }
                else if (ddlProductstage.SelectedItem.Text != "Final Assembly")
                {
                    SSQL = SSQL + " And TL.AssemblyStageName !='Final Assembly' ";
                }

                if (ddlProductstage.SelectedItem.Text != "Final Assembly")
                {
                    if (ddlPreAssembly.SelectedItem.Text != "-Select-")
                    {
                        SSQL = SSQL + " And TL.AssemblyStageName ='" + ddlPreAssembly.SelectedItem.Text + "' ";
                    }
                }

                SSQL = SSQL + " Group By STK.SapNo,Stk.ItemCode,Stk.ItemName,Stk.DeptCode,Stk.Deptname,Stk.WarehouseCode,";
                SSQL = SSQL + " Stk.WarehouseName,Stk.CurrencyType,Stk.UOM,TL.RefCode  ";


            }
            else if (ddlMatType.SelectedItem.Text == "Assets")
            {
                SSQL = "Select Stk.ItemName[Item Description],STK.SapNo[Sap No],Stk.Deptname[Department Name],Stk.WarehouseName[Location Name],Stk.UOM, ";
                SSQL = SSQL + " Sum(Stk.Add_Qty)-sum(Stk.Minus_Qty) [Stock Qty],CurrencyType, ";
                SSQL = SSQL + " Cast((sum(Stk.Add_Value)-sum(Stk.Minus_Value))/(sum(Stk.Add_Qty)-sum(Stk.Minus_Qty)) as decimal(18,2)) [Rate],";
                SSQL = SSQL + " Sum(Stk.Add_Value)-sum(Stk.Minus_Value) [Stock Value] ";
                SSQL = SSQL + " From Trans_Stock_Ledger_All Stk where ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' ";
                //And FinYearVal='" + SessionFinYearVal + "' ";
                SSQL = SSQL + " and STK.Mat_Type='3' and STK.Add_Qty!='0' ";

                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {
                    SSQL = SSQL + " And Convert (DateTime,Stk.Trans_Date, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                    SSQL = SSQL + " And Convert (DateTime,Stk.Trans_Date, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
                }

                if (ddlMatType.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.Mat_Type='" + ddlMatType.SelectedValue + "' ";
                }

                if (ddlItem.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.ItemName='" + ddlItem.SelectedItem.Text + "'";
                }

                if (ddlDept.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.Deptname='" + ddlDept.SelectedItem.Text + "'";
                }

                if (ddlWarehouse.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.WarehouseName='" + ddlWarehouse.SelectedItem.Text + "'";
                }

                if (ddlSupplier.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Supp_Name='" + ddlSupplier.SelectedItem.Text + "'";
                }

                SSQL = SSQL + " Group By STK.SapNo,Stk.ItemCode,Stk.ItemName,Stk.DeptCode,Stk.Deptname,Stk.WarehouseCode,Stk.WarehouseName,";
                SSQL = SSQL + " Stk.CurrencyType,Stk.UOM  ";
            }

            else if (ddlMatType.SelectedItem.Text == "General Item")
            {

                SSQL = "Select GI.RefCode[Reference Code],Stk.ItemName[Item Description],STK.SapNo[Sap No],";
                SSQL = SSQL + " Stk.Deptname[Department Name],Stk.WarehouseName[Location Name],Stk.UOM, ";
                SSQL = SSQL + " Sum(Stk.Add_Qty)-sum(Stk.Minus_Qty) [Stock Qty],CurrencyType, ";
                SSQL = SSQL + " Cast((sum(Stk.Add_Value)-sum(Stk.Minus_Value))/(sum(Stk.Add_Qty)-sum(Stk.Minus_Qty)) as decimal(18,2)) [Rate],";
                SSQL = SSQL + " Sum(Stk.Add_Value)-sum(Stk.Minus_Value) [Stock Value] ";
                SSQL = SSQL + " From Trans_Stock_Ledger_All Stk ";
                SSQL = SSQL + " Inner Join MstGeneralItem GI on GI.ItemCode=STK.ItemCode and Status!='Delete' ";
                SSQL = SSQL + " where STK.ccode='" + SessionCcode + "' And STK.LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " STK.FinYearCode='" + SessionFinYearCode + "'";
                //And STK.FinYearVal='" + SessionFinYearVal + "' ";
                SSQL = SSQL + " and STK.Mat_Type='4' and STK.Add_Qty!='0' ";

                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {
                    SSQL = SSQL + " And Convert (DateTime,Stk.Trans_Date, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
                    SSQL = SSQL + " And Convert (DateTime,Stk.Trans_Date, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
                }

                if (ddlSupplierType.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.Supp_Type='" + ddlSupplierType.SelectedValue + "' ";
                }

                if (ddlItem.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.ItemName='" + ddlItem.SelectedItem.Text + "'";
                }

                if (ddlDept.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.Deptname='" + ddlDept.SelectedItem.Text + "'";
                }

                if (ddlWarehouse.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Stk.WarehouseName='" + ddlWarehouse.SelectedItem.Text + "'";
                }

                if (ddlSupplier.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " And Supp_Name='" + ddlSupplier.SelectedItem.Text + "'";
                }

                SSQL = SSQL + " Group By STK.SapNo,Stk.ItemCode,Stk.ItemName,Stk.DeptCode,Stk.Deptname,Stk.WarehouseCode,Stk.WarehouseName,";
                SSQL = SSQL + " Stk.CurrencyType,Stk.UOM,GI.RefCode  ";

            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);



            if (DT.Rows.Count > 0)
            {

                string attachment = "attachment;filename=StockDetails_" + Convert.ToDateTime(DateTime.Now).ToString("mmss") + ".xls";
                Response.Clear();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                GvExcel.DataSource = DT;
                GvExcel.DataBind();

                GvExcel.HeaderStyle.Font.Bold = true;

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GvExcel.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");
                Response.Write("<a style=\"font-weight:bold\"> CORAL MANUFACTURING WORKS INDIA PRIVATE LIMITED </a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");
                Response.Write("<a style=\"font-weight:bold\">Goods Received Report</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");

                DateTime dateTime = DateTime.UtcNow.Date;

                if (txtFromDate.Text == "" || txtToDate.Text == "")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");
                    Response.Write("<a style=\"font-weight:bold\"> As On Date  -" + dateTime.ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }

                if (ddlSupplierType.SelectedItem.Text == "Enercon")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=ENERCONE</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlSupplierType.SelectedItem.Text == "Escrow")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=ESCROW</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
                else if (ddlSupplierType.SelectedItem.Text == "Coral")
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                    Response.Write("<a style=\"font-weight:bold\">Supplier Type=CORAL</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                }


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 9) + "'>");

                Response.Write("<a style=\"font-weight:bold\">Stock Type=" + ddlStkType.SelectedItem.Text + "</a>");

                Response.Write("</td>");
                Response.Write("</tr>");


                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No More Records...');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
            }
        }
    }
}