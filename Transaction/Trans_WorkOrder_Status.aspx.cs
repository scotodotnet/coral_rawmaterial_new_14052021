﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_Trans_MRPProcess_Approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: MRP Process Aprroval";
            purchase_Request_Status_Add();
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }
    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {

        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Enercon PO Approval");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Request..');", true);
        }
        //User Rights Check End


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Status = '2'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process Details Already Cancelled.. Cannot Approved it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            SSQL = "Select * from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Status = '1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process Details Already get Approved..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Status = '1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table

                SSQL = "Update Trans_MRPProcess_Main set Status='1' where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Work_Order_No ='" + e.CommandArgument.ToString() + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process Details Approved Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process not is Avail give input correctly..');", true);
            }
        }


    }
    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("work in progress List");
        txtRequestStatus.Items.Add("Complete List");
    }
    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        //string[] Department_Spilit = txtDepartment.Text.Split('-');
        //string Department_Code = "";
        //string Department_Name = "";
        string SSQL = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        DT.Columns.Add("Std_PO_No");
        DT.Columns.Add("Std_PO_Date");
        DT.Columns.Add("Supp_Name");
        DT.Columns.Add("Pur_Request_No");



        if (txtRequestStatus.Text == "Pending List")
        {
            SSQL = "Select Work_Order_No,Work_Order_Date,WorkOrder_OrderNo,GenModelName,PartType from Trans_GenerateWorkOrder_Main";
            SSQL = SSQL + " Where Ccode = '" + SessionCcode + "' and LCode = '" + SessionLcode + "' and Status!= 'Delete' and ";
            SSQL = SSQL + " (WO_Status = '0' or WO_Status is null)";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            Repeater2.DataSource = DT;
            Repeater2.DataBind();
            Repeater1.Visible = false;

        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            SSQL = "Select Work_Order_No,Work_Order_Date,WorkOrder_OrderNo,GenModelName,PartType from Trans_GenerateWorkOrder_Main";
            SSQL = SSQL + " Where Ccode = '" + SessionCcode + "' and LCode = '" + SessionLcode + "' and Status!= 'Delete' and ";
            SSQL = SSQL + " WO_Status = '1' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            Repeater1.DataSource = DT;
            Repeater1.DataBind();
            Repeater2.Visible = false;
            Repeater1.Visible = true;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            SSQL = "Select Work_Order_No,Work_Order_Date,WorkOrder_OrderNo,GenModelName,PartType from Trans_GenerateWorkOrder_Main";
            SSQL = SSQL + " Where Ccode = '" + SessionCcode + "' and LCode = '" + SessionLcode + "' and Status!= 'Delete' and ";
            SSQL = SSQL + " WO_Status = '2' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            Repeater1.DataSource = DT;
            Repeater1.DataBind();
            Repeater2.Visible = false;
            Repeater1.Visible = true;
        }
    }
    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Enercon PO Approval");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Request..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandArgument.ToString() + "' And ";
            SSQL = SSQL + " Status = '1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process Details Already Approved.. Cannot Cancelled it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Status = '2'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process Details Already get Cancelled..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            SSQL = "Select * from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Status = '2'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table

                SSQL = "Update Trans_MRPProcess_Main set Status='2' where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Work_Order_No ='" + e.CommandArgument.ToString() + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process Details Cancelled Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process not is Avail give input correctly..');", true);
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Work_Order_No,Work_Order_Date,WorkOrder_OrderNo,GenModelName,PartType from Trans_GenerateWorkOrder_Main";
        SSQL = SSQL + " Where Ccode = '" + SessionCcode + "' and LCode = '" + SessionLcode + "' and Status!= 'Delete' and ";
        SSQL = SSQL + " (WO_Status = '0' or WO_Status is null)";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}