﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Trans_Escrow_PurOrd_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Escrow Purchase Order";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Order");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add New Purchase Order...');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Adding New Std Purchase Order..');", true);
        }
        else
        {
            Session.Remove("Pur_RequestNo_Approval");
            Session.Remove("Transaction_No");
            Session.Remove("Std_PO_No");
            Response.Redirect("Trans_Escrow_PurOrd_Sub.aspx");

        }

        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Transaction_No");
        //Session.Remove("Std_PO_No");
        //Response.Redirect("standard_po.aspx");
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            bool ErrFlag = false;

            //User Rights Check Start

            bool Rights_Check = false;

            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Order");
            if (Rights_Check == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit New Purchase Order...');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Adding New Std Purchase Order..');", true);
            }
            else
            {
                DataTable dtdpurchase = new DataTable();

                SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearcode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "' and ";
                SSQL = SSQL + " PO_Status= '0'";

                dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Print Purchase Order...');", true);
                if (dtdpurchase.Rows.Count > 0)
                {
                    string Enquiry_No_Str = e.CommandName.ToString();
                    Session.Remove("Pur_RequestNo_Approval");
                    Session.Remove("Transaction_No");
                    Session.Remove("Std_PO_No");
                    Session["Std_PO_No"] = Enquiry_No_Str;
                    Response.Redirect("Trans_Escrow_PurOrd_Sub.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Approved, So Can Not be Edited ..');", true);
                    
                }
            }
        }
        catch(Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            //User Rights Check Start
            bool ErrFlag = false;
            bool Rights_Check = false;

            Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Order");

            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Purchase Order...');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Std Purchase Order..');", true);
            }
            //User Rights Check End

            //Check With Already Approved Start

            DataTable DT_Check = new DataTable();

            SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + "FinYearCode = '" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "' And PO_Status='1'";

            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Do not Delete Std Purchase Order Details Already Approved..');", true);
            }
            //Check With Already Approved End

            if (!ErrFlag)
            {

                DataTable dtdpurchase = new DataTable();
                SSQL = "select PO_Status from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + "And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";
                dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
                string status = dtdpurchase.Rows[0]["PO_Status"].ToString();

                if (status == "" || status == "0")
                {
                    DataTable DT = new DataTable();
                    SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";

                    DT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT.Rows.Count != 0)
                    {
                        //Purchase Order Delete Main Table
                        SSQL = "Delete from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);

                        //Purchase Order Main Sub Table
                        SSQL = "Delete from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);


                        //Purchase Request Delete Main Table
                        SSQL = "Delete from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);

                        //Purchase Request Delete Main Sub Table
                        SSQL = "Delete from Trans_Escrow_PurRqu_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Details Deleted Successfully');", true);
                        Load_Data_Enquiry_Grid();
                    }
                }
                else if (status == "2")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Already Rejected..');", true);
                }
                else if (status == "3")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order put in pending..');", true);
                }
                else if (status == "1")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Cant Edit..');", true);
                }
                else if (status == "6")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Rejected by FM..');", true);
                }
            }
        }
        catch(Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select Std_PO_No,Std_PO_Date,Supp_Name,RefNo,Canceledby,CancelReason,Case When PO_Status='0' then 'Pending' When PO_Status='1' Then '1st Level Success' ";
            SSQL = SSQL + " When PO_Status='2' Then '2nd Level Success' When PO_Status='3' Then '3rd Level Success' when Po_Status='-1' then 'Reject' End Status ";
            SSQL = SSQL + " From Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and Active!='Delete' ";
            SSQL = SSQL + " And Lcode ='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' Order by Std_PO_No Desc ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            Repeater2.DataSource = DT;
            Repeater2.DataBind();
        }
        catch(Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        try
        {

            lblErrorMsg.Text = "";
            string RptName = "";
            string StdPurOrdNo = "";
            string SupQtnNo = "";
            string DeptName = "";
            bool ErrFlag = false;

            bool Rights_Check = false;

            Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Order");

            if (Rights_Check == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Print Purchase Order...');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Std Purchase Order..');", true);
            }
            else
            {
                RptName = "Escrow Purchase Order Details Invoice Format";
                StdPurOrdNo = e.CommandName.ToString();
                ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");
            }
        }
        catch(Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }


    protected void GridPrintClickWithSign(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string RptName = "";
            string StdPurOrdNo = "";
            string SupQtnNo = "";
            string DeptName = "";
            bool ErrFlag = false;

            bool Rights_Check = false;

            Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Order");

            if (Rights_Check == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Print Purchase Order...');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Std Purchase Order..');", true);
            }
            else
            {
                RptName = "Escrow Purchase Order Details Invoice Format With Sign";
                StdPurOrdNo = e.CommandName.ToString();
                ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblStatus = Item.FindControl("lblStatus") as Label;

            if (lblStatus.Text == "Pending")
            {
                lblStatus.BackColor = Color.Blue;
                lblStatus.ForeColor = Color.White;
            }
            else if (lblStatus.Text == "Reject")
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.ForeColor = Color.White;
            }
            else
            {
                lblStatus.BackColor = Color.Green;
                lblStatus.ForeColor = Color.White;
            }
        }
    }
}