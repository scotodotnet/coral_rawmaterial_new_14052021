﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Web.Services;

public partial class Trans_PO_GPIN_Sub : System.Web.UI.Page
{
   public static BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    public static string SessionCcode;
    public static string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGRNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal RetQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
      
        if (!IsPostBack)
        {

            Page.Title = "CORAL ERP :: Goods Return";
            Initial_Data_Referesh();

            Load_BOM_ProductItem();

            //List<GatePassINGrid> GatePassINGridList = new List<GatePassINGrid>();
            //GatePassINGridList.Add(new GatePassINGrid());
           // Repeater2.DataSource = GatePassINGridList;
            //Repeater2.DataBind();

            if (Session["GRetNo"] == null)
            {
                SessionGRNo = "";
            }
            else
            {
                SessionGRNo = Session["GRetNo"].ToString();
            }
        }
        Load_OLD_data();
    }

    private void Load_BOM_ProductItem()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
         
        SSQL = "Select ProductNo,ProductName from ProductModel where Ccode='CORAL'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlTest.DataSource = DT;

        DataRow  dr = DT.NewRow();

        dr["ProductNo"] = "-Select-";
        dr["ProductName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlTest.DataValueField = "ProductNo";
        ddlTest.DataTextField = "ProductName";
        ddlTest.DataBind();
    }  
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
      
    }


    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ModelCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelName", typeof(string)));
        dt.Columns.Add(new DataColumn("PrctPartCode", typeof(string)));
        dt.Columns.Add(new DataColumn("PrctPartName", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("CalendarWeek", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelQty", typeof(string)));
        dt.Columns.Add(new DataColumn("SumOfQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRemarks", typeof(string)));
        dt.Columns.Add(new DataColumn("Item_Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRate_Other", typeof(string)));

        dt.Columns.Add(new DataColumn("CGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("VATP", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
    }

    protected void txtModQty_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txtQty_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_PO_GPIN_Main.aspx");
    }

    protected void ddlTest_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DTGRID = new DataTable();

        SSQL = "Select GenModelNo[ModelCode],GenModelName[ModelName],ProductionPartNo[PrctPartCode],ProductionPartName[PrctPartName],Sap_no[SAPNo],";
        SSQL = SSQL + " Mat_No[ItemCode],Raw_Mat_Name[ItemName],UOM_Full[UOMCode],RequiredQty [ReuiredQty],''ReuiredDate,''CalendarWeek,";
        SSQL = SSQL + "  '5'ModelQty,'0'SumOfQty,Remarks[ItemRemarks],Amount [Item_Rate], AmtOther[ItemRate_Other],CGSTP,SGSTP,IGSTP,VATP from BOMMaster ";
        SSQL = SSQL + " Where ProductionPartName='" + ddlTest.SelectedItem.Text + "'";

        DTGRID = objdata.RptEmployeeMultipleDetails(SSQL);

        ViewState["ItemTable"] = DTGRID;
        Repeater1.DataSource = DTGRID;
        Repeater1.DataBind();
    }
    [WebMethod]
    public static List<GatePassINGrid> TestItemName(string PartName, string Filter)
    {
        string msg = "Success";
        string SSQL = "";

        string[] Test = Filter.Split(',');

        string ModelQty = Test[0].ToString();
        string ReqDate = Test[1].ToString();
        string CalWeek = Test[2].ToString();
        string Remarks = Test[3].ToString();

        SSQL = "Select GenModelNo[ModelCode],GenModelName[ModelName],ProductionPartNo[PrctPartCode],ProductionPartName[PrctPartName], ";
        SSQL = SSQL + " Sap_no[SAPNo],Mat_No[ItemCode],Raw_Mat_Name[ItemName],UOM_Full[UOMCode],";
        SSQL = SSQL + "'" + ReqDate + "'ReuiredDate,'" + CalWeek + "'CalendarWeek,";
        SSQL = SSQL + " Cast(RequiredQty as Decimal(18,2)) [ReuiredQty], Cast('" + ModelQty + "' as decimal(18,2))ModelQty,";
        SSQL = SSQL + " Cast(Cast(RequiredQty as Decimal(18,2))*cast('" + ModelQty + "' as decimal(18,2)) as Decimal(18,2))SumOfQty,";
        SSQL = SSQL + " '" + Remarks + "'[ItemRemarks],Cast(Amount as Decimal(18,2)) [Item_Rate], ";
        SSQL = SSQL + " Cast(AmtOther as decimal(18,2))[ItemRate_Other],";
        SSQL = SSQL + " CGSTP,SGSTP,IGSTP,VATP from BOMMaster Where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
        SSQL = SSQL + " ProductionPartName='" + PartName + "'";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {

            List<GatePassINGrid> GatePassINGridList = new List<GatePassINGrid>();
            foreach (DataRow dr in dt.Rows)
            {
                GatePassINGridList.Add(new GatePassINGrid
                {
                    ModelCode = dr["ModelCode"].ToString(),
                    ModelName = dr["ModelName"].ToString(),
                    PrctPartCode = dr["PrctPartCode"].ToString(),
                    PrctPartName = dr["PrctPartName"].ToString(),
                    SAPNo = dr["SAPNo"].ToString(),
                    ItemCode = dr["ItemCode"].ToString(),
                    ItemName = dr["ItemName"].ToString(),
                    UOMCode = dr["UOMCode"].ToString(),
                    ReuiredDate = dr["ReuiredDate"].ToString(),
                    CalendarWeek = dr["CalendarWeek"].ToString(),
                    ReuiredQty = dr["ReuiredQty"].ToString(),
                    ModelQty = dr["ModelQty"].ToString(),
                    SumOfQty = dr["SumOfQty"].ToString(),
                    ItemRemarks = dr["ItemRemarks"].ToString(),
                    Item_Rate = dr["Item_Rate"].ToString(),
                    ItemRate_Other = dr["ItemRate_Other"].ToString(),
                    CGSTP = dr["CGSTP"].ToString(),
                    SGSTP = dr["SGSTP"].ToString(),
                    IGSTP = dr["IGSTP"].ToString(),
                    VATP = dr["VATP"].ToString(),
                });
            }
            return GatePassINGridList.ToList();
        }
        else
        {
            List<GatePassINGrid> GatePassINGridList = new List<GatePassINGrid>();
            return GatePassINGridList;
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string customerJSON = Request.Form["CustomerJSON"];
        dt = JsonConvert.DeserializeObject<DataTable>(customerJSON);

    }
}

public class GatePassINGrid
{
    public string ModelCode { get; set; }
    public string ModelName { get; set; }
    public string PrctPartCode { get; set; }
    public string PrctPartName { get; set; }
    public string SAPNo { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }     
    public string UOMCode { get; set; }
    public string ReuiredDate { get; set; }
    public string CalendarWeek { get; set; }
    public string ReuiredQty { get; set; }
    public string ModelQty { get; set; }
    public string SumOfQty { get; set; }
    public string ItemRemarks { get; set; }       
    public string Item_Rate { get; set; }
    public string ItemRate_Other { get; set; }
    public string CGSTP { get; set; }
    public string SGSTP { get; set; }
    public string IGSTP { get; set; }
    public string VATP { get; set; }   
}