﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Coral_PurReq_Main.aspx.cs" Inherits="Transaction_Croal_PurRqu_main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />
                        </div>
                        <!-- /.col -->
                    </div>
                    <!--/.row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Coral Purchase Request List</span></h3>
                                    <asp:Label runat="server" ID="lblErrorMsg" style="color:red;font-size:x-large" ></asp:Label>
                                    <div class="box-tools pull-right">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">

                                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false" OnItemDataBound="Repeater2_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-hover table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Sl. No</th>
                                                                <th>Request No</th>
                                                                <th>Request Date</th>
                                                                <th>Department Name</th>
                                                                <th>Material Type</th>
                                                                <th>Status</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("Pur_Request_No")%></td>
                                                        <td><%# Eval("Pur_Request_Date")%></td>
                                                        <td><%# Eval("DeptName")%></td>
                                                        <td><%# Eval("Material_Type")%></td>
                                                        <td>
                                                            <asp:Label ID="lblStatus" Width="100%" runat="server" class="form-control" Text='<%# Eval("Status") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("Pur_Request_No")%>'>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnPrint" class="btn btn-primary btn-sm fa fa-print" runat="server"
                                                                Text="" OnCommand="GridPrintClick" CommandArgument="Edit" CommandName='<%# Eval("Pur_Request_No")+","+ Eval("MaterialType")%>'>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" Visible="false" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("Pur_Request_No")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Purchase Request details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                            <input type="hidden" id="EditEnqNo" value="" />
                                            <asp:HiddenField ID="EditEnqAsp" Value="" runat="server" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

