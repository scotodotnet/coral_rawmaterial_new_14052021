﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Drawing;

public partial class Trans_Coral_PO_Amend_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionAmendNo;
    string SessionAmendNot;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    static Decimal OrderQty;
    string NameTypeRequest;
    string SessionPurRequestNoApproval;
    static string SAPNo;
    static string CGSTP;
    static string SGSTP;
    static string IGSTP;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Escrow Amendment";
            Initial_Data_Referesh();

            Load_Data_PurOrd_No();
            Load_Data_GST();

            Load_Data_SAPNo();
            Load_Data_ItemName();

            txtTransDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy");

            if (Session["Amend_Po_No"] == null)
            {

                string Auto_Transaction_No = "";
                TransactionNoGenerate TransNO = new TransactionNoGenerate();


                Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow PurOrd Amendment", SessionFinYearVal, "1");

                lblAmendNo.Text = Auto_Transaction_No;
            }
            else
            {
                SessionAmendNo = Session["Amend_Po_No"].ToString();
                lblAmendNo.Text = SessionAmendNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DTMain = new DataTable();
            DataTable DTSub = new DataTable();

            SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and AmendPoNo='" + SessionAmendNo + "' and ";
            SSQL = SSQL + " Active!='Delete'";

            DTMain = objdata.RptEmployeeMultipleDetails(SSQL);


            if (DTMain.Rows.Count > 0)
            {
                lblAmendNo.Text = DTMain.Rows[0]["AmendPoNo"].ToString();
                txtTransDate.Text = DTMain.Rows[0]["AmendPoDate"].ToString();
                lblCoralOrdNo.Text = DTMain.Rows[0]["RefNo"].ToString();
                ddlPurOrdNo.SelectedValue = DTMain.Rows[0]["PurOrdNo"].ToString();
                txtDate.Text = DTMain.Rows[0]["PurOrdDate"].ToString();
                lblQuatNo.Text = DTMain.Rows[0]["Supp_Qtn_No"].ToString();
                lblQuatDate.Text = DTMain.Rows[0]["Supp_Qtn_Date"].ToString();
                lblSuppName.Text = DTMain.Rows[0]["Supp_Name"].ToString();
                hfSuppCode.Value = DTMain.Rows[0]["Supp_Code"].ToString();
                lblDeptName.Text = DTMain.Rows[0]["DeptName"].ToString();
                hfDeptCode.Value = DTMain.Rows[0]["DeptCode"].ToString();
                txtDeliveryMode.Text = DTMain.Rows[0]["DeliveryMode"].ToString();
                hfDeliModeCode.Value = DTMain.Rows[0]["DeliveryModeCode"].ToString();
                txtPaymentMode.SelectedItem.Text = DTMain.Rows[0]["PaymentMode"].ToString();
                txtDeliveryDate.Text = DTMain.Rows[0]["DeliveryDate"].ToString();
                txtDeliveryAt.Text = DTMain.Rows[0]["DeliveryAt"].ToString();
                txtPaymentTerms.Text = DTMain.Rows[0]["PaymentTerms"].ToString();
                txtDescription.Text = DTMain.Rows[0]["Description"].ToString();
                txtSplDesc.Text = DTMain.Rows[0]["SplNote"].ToString();
                txtNote.Text = DTMain.Rows[0]["Note"].ToString();
                txtOthers.Text = DTMain.Rows[0]["Others"].ToString();
                ddlCurrencyType.SelectedValue = DTMain.Rows[0]["CurrencyType"].ToString();
                ddlGSTPre.SelectedValue = DTMain.Rows[0]["TaxType"].ToString();
                hfGstPre.Value = DTMain.Rows[0]["GSTPer"].ToString();
                //ddlGSTPre.SelectedItem.Text = DTMain.Rows[0]["GSTName"].ToString();

                txtPurRquNo.Text = DTMain.Rows[0]["Pur_Request_No"].ToString();
                txtRequestDate.Text = DTMain.Rows[0]["Pur_Request_Date"].ToString();

                txtAmendRmk.Text = DTMain.Rows[0]["AmendRemarks"].ToString();

                txtTotQty.Text = DTMain.Rows[0]["TotalQuantity"].ToString();
                txtTotAmt.Text = DTMain.Rows[0]["TotalAmt"].ToString();
                lblTotCGST.Text = DTMain.Rows[0]["TotCGST"].ToString();
                lblTotSGST.Text = DTMain.Rows[0]["TotSGST"].ToString();
                lblTotIGST.Text = DTMain.Rows[0]["TotIGST"].ToString();
                lblTotLineAmt.Text = DTMain.Rows[0]["TotalAmt"].ToString();
                txtAddOrLess.Text = DTMain.Rows[0]["AddOrLess"].ToString();
                txtNetAmt.Text = DTMain.Rows[0]["NetAmount"].ToString();

                SSQL = " Select SAPNo,ItemCode,ItemName,UOMCode,BFReuiredQty [ReuiredQty],BFModelQty[ModelQty],";
                SSQL = SSQL + " BFOrderQty [OrderQty],BFRate [Rate],BFCalWeek[CalWeek],AFReuiredQty[AmendRQty],";
                SSQL = SSQL + " AFModelQty[AmendMQty],FFOrderQty[AmendOrdQty],AFRate[AmendRate],AFCalWeek[AmendCalWeek],";
                SSQL = SSQL + " ReuiredDate,Remarks,ItemTotal,Discount_Per,Discount,GstPer,CGSTPer,CGSTAmount,SGSTPer,";
                SSQL = SSQL + " SGSTAmount,IGSTPer,IGSTAmount,OtherCharge,LineTotal from Trans_Escrow_PurOrd_Amend_Sub ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' and LCode ='" + SessionLcode + "' and ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' and Active!='Delete' And ";
                SSQL = SSQL + " AmendPoNo='" + SessionAmendNo + "' ";

                DTSub = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater1.DataSource = DTSub;
                Repeater1.DataBind();
                ViewState["ItemTable"] = DTSub;

                btnSave.Text = "Update";

            }

        }
        catch (Exception ex)
        { }
    }

    private void Load_Data_SAPNo()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Sap_No[SAPNo],Mat_No[ItemCode] from BOMMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Status!='Delete'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlAmendSAPNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["SAPNo"] = "-Select-";
        dr["ItemCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlAmendSAPNo.DataTextField = "SAPNo";
        ddlAmendSAPNo.DataValueField = "ItemCode";
        ddlAmendSAPNo.DataBind();
    }

    private void Load_Data_ItemName()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Raw_Mat_Name[ItemName],Mat_No[ItemCode] from BOMMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Status!='Delete'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlAmendItemName.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemName"] = "-Select-";
        dr["ItemCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlAmendItemName.DataTextField = "ItemName";
        ddlAmendItemName.DataValueField = "ItemCode";
        ddlAmendItemName.DataBind();
    }

    private void Load_Data_GST()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select GST_Type from GSTMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Status!='Delete'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlGSTPre.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["GST_Type"] = "-Select-";
        dr["GST_Type"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlGSTPre.DataTextField = "GST_Type";
        ddlGSTPre.DataValueField = "GST_Type";
        ddlGSTPre.DataBind();
    }

    private void Load_Data_PurOrd_No()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Std_PO_No from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'  ";
        SSQL = SSQL + " and FinYearCode='" + SessionFinYearCode + "' and (PO_Status='1'  or AmendStatus='1') ";
        SSQL = SSQL + " and ReceiveStatus!='1' and Active!='Delete'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlPurOrdNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["Std_PO_No"] = "-Select-";
        dr["Std_PO_No"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlPurOrdNo.DataTextField = "Std_PO_No";
        ddlPurOrdNo.DataValueField = "Std_PO_No";
        ddlPurOrdNo.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";

        //	Lcode	FinYearCode

        if (btnSave.Text == "Save")
        {
            string Auto_Transaction_No = "";
            TransactionNoGenerate TransNO = new TransactionNoGenerate();

            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow PurOrd Amendment", SessionFinYearVal, "1");

            lblAmendNo.Text = Auto_Transaction_No;

        }

        SSQL = "Delete From Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and AmendPoNo='" + lblAmendNo.Text + "'";

        objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Delete from Trans_Escrow_PurOrd_Amend_Sub where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and AmendPoNo='" + lblAmendNo.Text + "'";

        objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Insert Into Trans_Escrow_PurOrd_Amend_Main(Ccode,Lcode,FinYearCode,FinYearVal,AmendPoNo,AmendPoDate,PurOrdNo,";
        SSQL = SSQL + " PurOrdDate,Supp_Code,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,Pur_Request_No,Pur_Request_Date,DeliveryModeCode,";
        SSQL = SSQL + " DeliveryMode,DeliveryDate,DeliveryAt,PaymentMode,DeptCode,DeptName,PaymentTerms,Description,Note,Others,";
        SSQL = SSQL + " TotalQuantity,TotalAmt,NetAmount,PO_Type,AddOrLess,RefNo,GSTPer,TaxType,Active,CurrencyType,";
        SSQL = SSQL + " MatType,SplNote,AmendStatus,AmendRemarks,TotCGST,TotSGST,TotIGST,UserID,UserName,CreateOn)";
        SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
        SSQL = SSQL + " '" + lblAmendNo.Text + "','" + txtTransDate.Text + "','" + ddlPurOrdNo.SelectedItem.Text + "',";
        SSQL = SSQL + " '" + txtDate.Text + "','" + hfSuppCode.Value + "','" + lblSuppName.Text + "','" + lblQuatNo.Text + "',";
        SSQL = SSQL + " '" + lblQuatDate.Text + "','" + txtPurRquNo.Text + "','" + txtRequestDate.Text + "',";
        SSQL = SSQL + " '" + hfDeliModeCode.Value + "','" + txtDeliveryMode.Text + "','" + txtDeliveryDate.Text + "',";
        SSQL = SSQL + " '" + txtDeliveryAt.Text + "','" + txtPaymentMode.Text + "','" + hfDeptCode.Value + "',";
        SSQL = SSQL + " '" + lblDeptName.Text + "','" + txtPaymentTerms.Text + "','" + txtDescription.Text + "',";
        SSQL = SSQL + " '" + txtNote.Text + "','" + txtOthers.Text + "','" + txtTotQty.Text + "','" + txtTotAmt.Text + "',";
        SSQL = SSQL + " '" + txtNetAmt.Text + "','Purchase Amendment','" + txtAddOrLess.Text + "','" + lblCoralOrdNo.Text + "',";
        SSQL = SSQL + " '" + hfGstPre.Value + "','" + ddlGSTPre.SelectedItem.Text + "','Add','" + ddlCurrencyType.SelectedItem.Text + "',";
        SSQL = SSQL + " '" + ddlMatType.SelectedValue + "','" + txtSplDesc.Text + "',0,'" + txtAmendRmk.Text + "',";
        SSQL = SSQL + " '" + lblTotCGST.Text + "','" + lblTotSGST.Text + "','" + lblTotIGST.Text + "','" + SessionUserID + "',";
        SSQL = SSQL + " '" + SessionUserName + "',Convert(nvarchar,GETDATE(),103))";

        objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];

        string FGSTP;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            SSQL = "Insert Into Trans_Escrow_PurOrd_Amend_Sub(Ccode,Lcode,FinYearCode,FinYearVal,AmendPoNo,AmendPoDate,Std_PO_No,";
            SSQL = SSQL + " Std_PO_Date,RefNo,Pur_Request_No,Pur_Request_Date,ItemCode,ItemName,SAPNo,UOMCode,BFCalWeek,";
            SSQL = SSQL + " BFModelQty,BFRquQty,BFReuiredQty,BFOrderQty,BFRate,AFCalWeek,AFModelQty,AFRquQty,AFReuiredQty,";
            SSQL = SSQL + " FFOrderQty,AFRate,ReuiredDate,ItemTotal,GSTPer,GSTName,Discount_Per,Discount,CGSTPer,CGSTAmount,";
            SSQL = SSQL + " SGSTPer,SGSTAmount,IGSTPer,IGSTAmount,OtherCharge,LineTotal,Remarks,Active,UserID,UserName)";

            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            SSQL = SSQL + " '" + SessionFinYearVal + "','" + lblAmendNo.Text + "','" + txtTransDate.Text + "', ";
            SSQL = SSQL + " '" + ddlPurOrdNo.SelectedItem.Text + "','" + txtDate.Text + "','" + lblCoralOrdNo.Text + "',";
            SSQL = SSQL + " '" + txtPurRquNo.Text + "','" + txtRequestDate.Text + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["SAPNo"].ToString() + "','" + dt.Rows[i]["UOMCode"].ToString() + "',";

            //Before Amentment
            SSQL = SSQL + " '" + dt.Rows[i]["CalWeek"].ToString() + "','" + dt.Rows[i]["ModelQty"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["OrderQty"].ToString() + "','" + dt.Rows[i]["ReuiredQty"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["OrderQty"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "',";

            //After Amentment
            SSQL = SSQL + " '" + dt.Rows[i]["AmendCalWeek"].ToString() + "','" + dt.Rows[i]["AmendMQty"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["AmendOrdQty"].ToString() + "','" + dt.Rows[i]["AmendRQty"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["AmendOrdQty"].ToString() + "','" + dt.Rows[i]["AmendRate"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["ReuiredDate"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["ItemTotal"].ToString() + "','" + dt.Rows[i]["GstPer"].ToString() + "',";
            SSQL = SSQL + " '" + ddlGSTPre.SelectedItem.Text + "','" + dt.Rows[i]["Discount_Per"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["Discount"].ToString() + "','" + dt.Rows[i]["CGSTPer"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["CGSTAmount"].ToString() + "','" + dt.Rows[i]["SGSTPer"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["SGSTAmount"].ToString() + "','" + dt.Rows[i]["IGSTPer"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["IGSTAmount"].ToString() + "','0.00','" + dt.Rows[i]["LineTotal"].ToString() + "',";
            SSQL = SSQL + " '" + dt.Rows[i]["Remarks"].ToString() + "','Add','" + SessionUserID + "',";
            SSQL = SSQL + " '" + SessionUserName + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);
        }

        if (btnSave.Text == "Save")
        {
            TransactionNoGenerate TransNO = new TransactionNoGenerate();
            string Auto_Transaction_No = "";
            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow PurOrd Amendment", SessionFinYearVal, "1");
        }

        Response.Redirect("Trans_Escrow_PurOrd_Amend_Main.aspx");

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));


        dt.Columns.Add(new DataColumn("ModelQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("CalWeek", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));


        dt.Columns.Add(new DataColumn("AmendMQty", typeof(string)));
        dt.Columns.Add(new DataColumn("AmendRQty", typeof(string)));
        dt.Columns.Add(new DataColumn("AmendOrdQty", typeof(string)));
        dt.Columns.Add(new DataColumn("AmendCalWeek", typeof(string)));
        dt.Columns.Add(new DataColumn("AmendRate", typeof(string)));

        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        //dt.Columns.Add(new DataColumn("RateEUR", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount_Per", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("OtherCharge", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("GstPer", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Grid_Total_Calculate();
        Final_Total_Calculate();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_Escrow_PurOrd_Amend_Main.aspx");
    }
    protected void ddlPurOrdNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DTMain = new DataTable();
        DataTable DTSub = new DataTable();

        SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' and Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "' and ";
        SSQL = SSQL + " Active!='Delete'";

        DTMain = objdata.RptEmployeeMultipleDetails(SSQL);

        lblCoralOrdNo.Text = DTMain.Rows[0]["RefNo"].ToString();
        txtDate.Text = DTMain.Rows[0]["Std_PO_Date"].ToString();
        lblQuatNo.Text = DTMain.Rows[0]["Supp_Qtn_No"].ToString();
        lblQuatDate.Text = DTMain.Rows[0]["Supp_Qtn_Date"].ToString();
        lblSuppName.Text = DTMain.Rows[0]["Supp_Name"].ToString();
        hfSuppCode.Value = DTMain.Rows[0]["Supp_Code"].ToString();
        lblDeptName.Text = DTMain.Rows[0]["DeptName"].ToString();
        hfDeptCode.Value = DTMain.Rows[0]["DeptCode"].ToString();
        txtDeliveryMode.Text = DTMain.Rows[0]["DeliveryMode"].ToString();
        hfDeliModeCode.Value = DTMain.Rows[0]["DeliveryModeCode"].ToString();
        txtPaymentMode.SelectedItem.Text = DTMain.Rows[0]["PaymentMode"].ToString();
        txtDeliveryDate.Text = DTMain.Rows[0]["DeliveryDate"].ToString();
        txtDeliveryAt.Text = DTMain.Rows[0]["DeliveryAt"].ToString();
        txtPaymentTerms.Text = DTMain.Rows[0]["PaymentTerms"].ToString();
        txtDescription.Text = DTMain.Rows[0]["Description"].ToString();
        txtSplDesc.Text = DTMain.Rows[0]["SplNote"].ToString();
        txtNote.Text = DTMain.Rows[0]["Note"].ToString();
        txtOthers.Text = DTMain.Rows[0]["Others"].ToString();
        ddlCurrencyType.SelectedItem.Text = DTMain.Rows[0]["CurrencyType"].ToString();
        ddlTaxType.SelectedValue = DTMain.Rows[0]["TaxType"].ToString();
        hfGstPre.Value = DTMain.Rows[0]["GSTPer"].ToString();
        ddlGSTPre.SelectedValue = DTMain.Rows[0]["GstName"].ToString();

        txtPurRquNo.Text = DTMain.Rows[0]["Pur_Request_No"].ToString();
        txtRequestDate.Text = DTMain.Rows[0]["Pur_Request_Date"].ToString();

        txtTotQty.Text = DTMain.Rows[0]["TotalQuantity"].ToString();
        txtTotAmt.Text = DTMain.Rows[0]["TotalAmt"].ToString();
        lblTotCGST.Text = DTMain.Rows[0]["TotCGST"].ToString();
        lblTotSGST.Text = DTMain.Rows[0]["TotSGST"].ToString();
        lblTotIGST.Text = DTMain.Rows[0]["TotIGST"].ToString();
        lblTotLineAmt.Text = DTMain.Rows[0]["TotalAmt"].ToString();
        txtAddOrLess.Text = DTMain.Rows[0]["AddOrLess"].ToString();
        txtNetAmt.Text = DTMain.Rows[0]["NetAmount"].ToString();

        SSQL = " Select SAPNo,ItemCode,ItemName,UOMCode,ReuiredQty,ModelQty,OrderQty,Rate,CalWeek,ReuiredQty[AmendRQty],";
        SSQL = SSQL + " ModelQty[AmendMQty],OrderQty[AmendOrdQty],Rate[AmendRate],CalWeek[AmendCalWeek],ReuiredDate,Remarks,";
        SSQL = SSQL + " ItemTotal,Discount_Per,Discount,TaxPer,TaxAmount,GstPer,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,";
        SSQL = SSQL + " IGSTAmount,OtherCharge,LineTotal from Trans_Escrow_PurOrd_sub where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' and Active!='Delete' And ";
        SSQL = SSQL + " Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "' ";

        DTSub = objdata.RptEmployeeMultipleDetails(SSQL);

        Repeater1.DataSource = DTSub;
        Repeater1.DataBind();
        ViewState["ItemTable"] = DTSub;

        Grid_Total_Calculate();
        Final_Total_Calculate();

    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblBFAReqQty = Item.FindControl("lblRQty") as Label;
            Label lblBFAModel = Item.FindControl("lblMdlQty") as Label;
            Label lblBFAOrder = Item.FindControl("lblRquQty") as Label;
            Label lblBFARate = Item.FindControl("lblRate") as Label;
            Label lblBFACalWeek = Item.FindControl("lblCalWeek") as Label;

            TextBox txtAFAReq = Item.FindControl("txtReqQty") as TextBox;
            TextBox txtAFAModel = Item.FindControl("txtModelQty") as TextBox;
            TextBox txtAFAOrder = Item.FindControl("txtOrdQty") as TextBox;
            TextBox txtAFARate = Item.FindControl("txtGrdRate") as TextBox;
            TextBox txtAFACalWeek = Item.FindControl("txtCalWeek") as TextBox;


            lblBFAReqQty.BackColor = Color.Green;
            lblBFAReqQty.ForeColor = Color.White;
            lblBFAModel.BackColor = Color.Green;
            lblBFAModel.ForeColor = Color.White;
            lblBFAOrder.BackColor = Color.Green;
            lblBFAOrder.ForeColor = Color.White;
            lblBFARate.BackColor = Color.Green;
            lblBFARate.ForeColor = Color.White;
            lblBFACalWeek.BackColor = Color.Green;
            lblBFACalWeek.ForeColor = Color.White;


            txtAFAReq.BorderColor = Color.Red;
            txtAFAModel.BorderColor = Color.Red;
            txtAFAOrder.BorderColor = Color.Red;
            txtAFARate.BorderColor = Color.Red;
            txtAFACalWeek.BorderColor = Color.Red;
        }
    }

    protected void txtAmCalWeek_TextChanged(object sender, EventArgs e)
    {
        TextBox button = (sender as TextBox);
        RepeaterItem item = button.NamingContainer as RepeaterItem;
        int index = item.ItemIndex;
        Text_Change_Value_Cal(sender, index);

        ((TextBox)Repeater1.Items[index].FindControl("txtCalWeek")).Focus();
    }


    protected void txtGrdRate_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            ((TextBox)Repeater1.Items[index].FindControl("txtCalWeek")).Focus();

        }
        catch (Exception ex)
        {

        }
    }

    protected void txtOrdQty_TextChanged(object sender, EventArgs e)
    {

    }

    private void Text_Change_Value_Cal(object sender, int Row_No)
    {
        try
        {
            string Ord_Qty = "0";
            string Qty_Val = "0";
            string Item_Rate = "0";
            string ModelQty = "0";
            string Item_Total = "0";
            string Discount_Percent = "0";
            string Discount_Amt = "0";
            string VAT_Per = "0";
            string VAT_Amt = "0";
            string Tax_Amt = "0";
            string CGST_Per = "0";
            string CGST_Amt = "0";
            string SGST_Per = "0";
            string SGST_Amt = "0";
            string IGST_Per = "0";
            string IGST_Amt = "0";
            string BDUTax_Per = "0";
            string BDUTax_Amt = "0";
            string Other_Charges = "0";
            string Final_Amount = "0";
            string PackingAmt = "0";

            int i = Row_No;

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];

            TextBox txtGrdOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;

            if (txtGrdOrdQty.Text.ToString() != "")
            {
                TextBox txtTest = ((TextBox)(sender));
                RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));


                Label lblSapNo = Repeater1.Items[i].FindControl("lblSAPNo") as Label;
                Label lblItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                Label lblItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                Label lblUOM = Repeater1.Items[i].FindControl("lblUOM") as Label;


                Label lblBfrRQty = Repeater1.Items[i].FindControl("lblRQty") as Label;
                Label lblBfrMdlQty = Repeater1.Items[i].FindControl("lblMdlQty") as Label;
                Label lblBfrOrdQty = Repeater1.Items[i].FindControl("lblRquQty") as Label;
                Label lblBfrRate = Repeater1.Items[i].FindControl("lblRate") as Label;
                Label lblBfrCalWeek = Repeater1.Items[i].FindControl("lblCalWeek") as Label;

                TextBox txtAfrRQty = Repeater1.Items[i].FindControl("txtReqQty") as TextBox;
                TextBox txtAfrMdlQty = Repeater1.Items[i].FindControl("txtModelQty") as TextBox;
                TextBox txtAfrOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;
                TextBox txtAfrRate = Repeater1.Items[i].FindControl("txtGrdRate") as TextBox;
                TextBox txtAfrCalWeek = Repeater1.Items[i].FindControl("txtCalWeek") as TextBox;

                TextBox txtRquDate = Repeater1.Items[i].FindControl("txtRDate") as TextBox;
                Label lblRemarks = Repeater1.Items[i].FindControl("lblRemarks") as Label;
                Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                Label lblDiscPer = Repeater1.Items[i].FindControl("lblDiscPer") as Label;
                Label lblDisc = Repeater1.Items[i].FindControl("lblDisc") as Label;
                Label lblTaxPer = Repeater1.Items[i].FindControl("lblTaxPer") as Label;
                Label lblTaxAmt = Repeater1.Items[i].FindControl("lblTaxAmt") as Label;
                Label lblCGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                Label lblCGSTAmt = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                Label lblSGSTAmt = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                Label lblIGSTAmt = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblGSTPer = Repeater1.Items[i].FindControl("lblGSTPer") as Label;
                Label lblOtherAmt = Repeater1.Items[i].FindControl("lblOtherAmt") as Label;
                Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;


                //txtAfrOrdQty.Text = Math.Round(Convert.ToDecimal(txtAfrMdlQty.Text.ToString()) * Convert.ToDecimal(lblBfrRQty.Text.ToString()), 2, MidpointRounding.AwayFromZero).ToString();

                //if (txtAfrOrdQty.Text != "") { Qty_Val = txtAfrOrdQty.Text.ToString(); }
                //if (txtAfrMdlQty.Text != "") { ModelQty = txtAfrMdlQty.Text.ToString(); }
                //if (txtAfrRate.Text != "") { Item_Rate = txtAfrRate.Text.ToString(); }


                Ord_Qty = (Convert.ToDecimal(txtAfrRQty.Text) * Convert.ToDecimal(txtAfrMdlQty.Text)).ToString();
                Ord_Qty = (Math.Round(Convert.ToDecimal(Ord_Qty), 2, MidpointRounding.AwayFromZero)).ToString();

                txtAfrOrdQty.Text = Ord_Qty.ToString();

                Item_Total = (Convert.ToDecimal(txtAfrOrdQty.Text.ToString()) * Convert.ToDecimal(txtAfrRate.Text.ToString())).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();

                if (Convert.ToDecimal(Item_Total) != 0)
                {
                    if (Convert.ToDecimal(lblDiscPer.Text.ToString()) != 0) { Discount_Percent = lblDiscPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                    if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                    //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                    //Discount Amt Calculate
                    if (Convert.ToDecimal(Discount_Percent) != 0)
                    {
                        Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                        Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                        Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        Discount_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(CGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                        CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                        CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        CGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(VAT_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                        VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                        VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        VAT_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(SGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                        SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                        SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        SGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(IGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                        IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                        IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        IGST_Amt = "0.00";
                    }

                    //Other Charges
                    //if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                    //Final Amt
                    Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                    Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

                    lblItemTot.Text = Item_Total;
                    lblDisc.Text = Discount_Amt;
                    //lblTaxAmt.Text = Tax_Amt;
                    lblCGSTAmt.Text = CGST_Amt;
                    lblSGSTAmt.Text = SGST_Amt;
                    lblIGSTAmt.Text = IGST_Amt;
                    //txtVAT_AMT.Text = VAT_Amt;
                    lblLineTot.Text = Final_Amount;
                    txtTotAmt.Text = Final_Amount;
                    txtNetAmt.Text = Final_Amount;
                }

                dt.Rows[i]["SAPNo"] = lblSapNo.Text;
                dt.Rows[i]["ItemCode"] = lblItemCode.Text;
                dt.Rows[i]["ItemName"] = lblItemName.Text;
                dt.Rows[i]["UOMCode"] = lblUOM.Text;

                dt.Rows[i]["ReuiredQty"] = Math.Round(Convert.ToDecimal(lblBfrRQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["ModelQty"] = Math.Round(Convert.ToDecimal(lblBfrMdlQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["OrderQty"] = Math.Round(Convert.ToDecimal(lblBfrOrdQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["Rate"] = Math.Round(Convert.ToDecimal(lblBfrRate.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["CalWeek"] = lblBfrCalWeek.Text;

                dt.Rows[i]["AmendRQty"] = Math.Round(Convert.ToDecimal(txtAfrRQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["AmendMQty"] = Math.Round(Convert.ToDecimal(txtAfrMdlQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["AmendOrdQty"] = Math.Round(Convert.ToDecimal(txtAfrOrdQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["AmendRate"] = Math.Round(Convert.ToDecimal(txtAfrRate.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["AmendCalWeek"] = txtAfrCalWeek.Text;

                dt.Rows[i]["ItemTotal"] = Math.Round(Convert.ToDecimal(lblItemTot.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["Discount_Per"] = Math.Round(Convert.ToDecimal(lblDiscPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["Discount"] = Math.Round(Convert.ToDecimal(lblDisc.Text), 2, MidpointRounding.AwayFromZero);
                //dt.Rows[i]["TaxPer"] = Math.Round(Convert.ToDecimal(lblTaxPer.Text), 2, MidpointRounding.AwayFromZero);
                //dt.Rows[i]["TaxAmount"] = Math.Round(Convert.ToDecimal(lblTaxAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["GstPer"] = Math.Round(Convert.ToDecimal(lblGSTPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["CGSTPer"] = Math.Round(Convert.ToDecimal(lblCGSTPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["CGSTAmount"] = Math.Round(Convert.ToDecimal(lblCGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["SGSTPer"] = Math.Round(Convert.ToDecimal(lblSGSTPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["SGSTAmount"] = Math.Round(Convert.ToDecimal(lblSGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["IGSTPer"] = Math.Round(Convert.ToDecimal(lblIGSTPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["IGSTAmount"] = Math.Round(Convert.ToDecimal(lblIGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["OtherCharge"] = Math.Round(Convert.ToDecimal(lblOtherAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["LineTotal"] = Math.Round(Convert.ToDecimal(lblLineTot.Text), 2, MidpointRounding.AwayFromZero);


                Grid_Total_Calculate();
            }
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtReqQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            ((TextBox)Repeater1.Items[index].FindControl("txtModelQty")).Focus();

        }
        catch (Exception ex)
        {

        }
    }

    protected void txtCalWeek_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            if (Convert.ToDecimal(Repeater1.Items.Count) == Convert.ToDecimal(index + 1))
            {
                ((TextBox)Repeater1.Items[index].FindControl("txtCalWeek")).Focus();
            }
            else
            {
                ((TextBox)Repeater1.Items[index + 1].FindControl("txtReqQty")).Focus();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtModelQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            ((TextBox)Repeater1.Items[index].FindControl("txtGrdRate")).Focus();

        }
        catch (Exception ex)
        {

        }
    }

    private void Grid_Total_Calculate()
    {
        decimal SumItemTot = 0;
        decimal SumLineTot = 0;
        decimal SumQtyTot = 0;
        decimal SumCGSTTot = 0;
        decimal SumSGSTTot = 0;
        decimal SumIGSTTot = 0;

        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            Label ItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
            Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;
            TextBox txtOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;
            Label CGST = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
            Label SGST = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
            Label IGST = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;


            SumItemTot = SumItemTot + Convert.ToDecimal(ItemTot.Text);
            SumLineTot = SumLineTot + Convert.ToDecimal(LineTotal.Text);
            SumQtyTot = SumQtyTot + Convert.ToDecimal(txtOrdQty.Text);
            SumCGSTTot = SumCGSTTot + Convert.ToDecimal(CGST.Text);
            SumSGSTTot = SumSGSTTot + Convert.ToDecimal(SGST.Text);
            SumIGSTTot = SumIGSTTot + Convert.ToDecimal(IGST.Text);
        }

        txtTotAmt.Text = SumItemTot.ToString();
        lblTotLineAmt.Text = SumLineTot.ToString();
        txtTotQty.Text = SumQtyTot.ToString();
        lblTotCGST.Text = SumCGSTTot.ToString();
        lblTotSGST.Text = SumSGSTTot.ToString();
        lblTotIGST.Text = SumIGSTTot.ToString();
        txtNetAmt.Text = (SumLineTot + Convert.ToDecimal(txtAddOrLess.Text)).ToString();
    }


    private void Final_Total_Calculate()
    {

        string Final_NetAmt = "0";
        string AddorLess = "0";
        string Item_Total_Amt = lblTotLineAmt.Text;

        string[] ROff = Item_Total_Amt.Split('.');


        if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0)
        {
            AddorLess = txtAddOrLess.Text.ToString();
        }

        if (Convert.ToDecimal(ROff[1].ToString()) < Convert.ToDecimal("49"))
        {
            Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) - Convert.ToDecimal(AddorLess)).ToString();
        }
        else if (Convert.ToDecimal(ROff[1].ToString()) > Convert.ToDecimal("49"))
        {
            Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) + Convert.ToDecimal(AddorLess)).ToString();
        }

        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetAmt.Text = Final_NetAmt;

    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlAmendSAPNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select * From BOMMaster where Status!='Delete' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " and Sap_No = '" + ddlAmendSAPNo.SelectedItem.Text + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlAmendItemName.SelectedValue = DT.Rows[0]["Mat_No"].ToString();
            txtAmendReqQty.Text = DT.Rows[0]["RequiredQty"].ToString();
            txtAmendRate.Text = DT.Rows[0]["Amount"].ToString();
            hfAmendUomName.Value = DT.Rows[0]["UOM_Full"].ToString();
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlAmendItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select * From BOMMaster where Status!='Delete' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " and Raw_Mat_Name = '" + ddlAmendItemName.SelectedItem.Text + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlAmendSAPNo.SelectedValue = DT.Rows[0]["Mat_No"].ToString();
            txtAmendReqQty.Text = DT.Rows[0]["RequiredQty"].ToString();
            txtAmendRate.Text = DT.Rows[0]["Amount"].ToString();
            hfAmendUomName.Value = DT.Rows[0]["UOM_Full"].ToString();
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtAmendReqQty_TextChanged(object sender, EventArgs e)
    {
        Add_Single_Amend_New_ItemName();
    }

    protected void txtAmendModelQty_TextChanged(object sender, EventArgs e)
    {
        Add_Single_Amend_New_ItemName();
    }

    protected void txtAmendRate_TextChanged(object sender, EventArgs e)
    {
        Add_Single_Amend_New_ItemName();
    }
    private void Add_Single_Amend_New_ItemName()
    {
        try
        {
            string SSQL = "";
            string CGST = "";
            string SGST = "";
            string IGST = "";

            DataTable DT = new DataTable();

            if (txtAmendReqQty.Text == "") { txtAmendReqQty.Text = "0.00"; }
            if (txtAmendModelQty.Text == "") { txtAmendModelQty.Text = "0.00"; }
            if (txtAmendRate.Text == "") { txtAmendRate.Text = "0.00"; }
            if (lblAmendOrdQty.Text == "") { lblAmendOrdQty.Text = "0.00"; }

            lblAmendOrdQty.Text = Math.Round(Convert.ToDecimal(txtAmendReqQty.Text) * Convert.ToDecimal(txtAmendModelQty.Text), 2, MidpointRounding.AwayFromZero).ToString();
            lblAmendItemTot.Text = Math.Round(Convert.ToDecimal(lblAmendOrdQty.Text) * Convert.ToDecimal(txtAmendRate.Text), 2, MidpointRounding.AwayFromZero).ToString();

            if (ddlGSTPre.SelectedItem.Text != "-Select-")
            {
                SSQL = "Select * from GSTMaster where Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " and GST_Type='" + ddlGSTPre.SelectedItem.Text + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                CGST = DT.Rows[0]["CGST_Per"].ToString();
                SGST = DT.Rows[0]["SGST_Per"].ToString();
                IGST = DT.Rows[0]["IGST_Per"].ToString();

                hfAmendCGSTP.Value = CGST;
                hfAmendSGSTP.Value = SGST;
                hfAmendIGSTP.Value = IGST;
            }

            if (Convert.ToDecimal(CGST) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = Convert.ToDecimal(lblAmendItemTot.Text).ToString();
                lblAmendCGST.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST)).ToString();
                lblAmendCGST.Text = (Convert.ToDecimal(lblAmendCGST.Text) / Convert.ToDecimal(100)).ToString();
                lblAmendCGST.Text = (Math.Round(Convert.ToDecimal(lblAmendCGST.Text), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                lblAmendCGST.Text = "0.00";
            }

            if (Convert.ToDecimal(SGST) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = Convert.ToDecimal(lblAmendItemTot.Text).ToString();
                lblAmendSGST.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST)).ToString();
                lblAmendSGST.Text = (Convert.ToDecimal(lblAmendSGST.Text) / Convert.ToDecimal(100)).ToString();
                lblAmendSGST.Text = (Math.Round(Convert.ToDecimal(lblAmendSGST.Text), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                lblAmendSGST.Text = "0.00";
            }

            if (Convert.ToDecimal(IGST) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = Convert.ToDecimal(lblAmendItemTot.Text).ToString();
                lblAmendIGST.Text = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST)).ToString();
                lblAmendIGST.Text = (Convert.ToDecimal(lblAmendIGST.Text) / Convert.ToDecimal(100)).ToString();
                lblAmendIGST.Text = (Math.Round(Convert.ToDecimal(lblAmendIGST.Text), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                lblAmendIGST.Text = "0.00";
            }

            lblAmendLineTot.Text = (Convert.ToDecimal(lblAmendItemTot.Text) + Convert.ToDecimal(lblAmendCGST.Text) + Convert.ToDecimal(lblAmendSGST.Text) + Convert.ToDecimal(lblAmendIGST.Text)).ToString();
            lblAmendLineTot.Text = (Math.Round(Convert.ToDecimal(lblAmendLineTot.Text), 2, MidpointRounding.AwayFromZero)).ToString();
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable qry_dt = new DataTable();
            bool ErrFlag = false;
            DataRow dr = null;
            string SSQL = "";


            if (ViewState["ItemTable"] != null)
            {

                dt = (DataTable)ViewState["ItemTable"];


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlAmendSAPNo.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["SAPNo"] = ddlAmendSAPNo.SelectedItem.Text;
                    dr["ItemCode"] = ddlAmendItemName.SelectedValue;
                    dr["ItemName"] = ddlAmendItemName.SelectedItem.Text;
                    dr["UOMCode"] = hfAmendUomName.Value;


                    dr["ModelQty"] = "0.00";
                    dr["ReuiredQty"] = "0.00";
                    dr["OrderQty"] = "0.00";
                    dr["Rate"] = "0.00";
                    dr["CalWeek"] = "NA";

                    dr["AmendMQty"] = txtAmendModelQty.Text;
                    dr["AmendRQty"] = txtAmendReqQty.Text;
                    dr["AmendOrdQty"] = lblAmendOrdQty.Text;
                    dr["AmendRate"] = txtAmendRate.Text;
                    dr["AmendCalWeek"] = txtAmendCalWeek.Text;

                    dr["ReuiredDate"] = "";
                    dr["Remarks"] = "";

                    dr["ItemTotal"] = lblAmendItemTot.Text;

                    dr["Discount_Per"] = "0.00";
                    dr["Discount"] = "0.00";
                    dr["GstPer"] = (Convert.ToDecimal(hfAmendCGSTP.Value) + Convert.ToDecimal(hfAmendSGSTP.Value) + Convert.ToDecimal(hfAmendIGSTP.Value)).ToString();
                    dr["CGSTPer"] = hfAmendCGSTP.Value;
                    dr["CGSTAmount"] = lblAmendCGST.Text;
                    dr["SGSTPer"] = hfAmendSGSTP.Value;
                    dr["SGSTAmount"] = lblAmendSGST.Text;
                    dr["IGSTPer"] = hfAmendIGSTP.Value;
                    dr["IGSTAmount"] = lblAmendIGST.Text;

                    dr["OtherCharge"] = "0.00";
                    dr["LineTotal"] = lblAmendLineTot.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    Grid_Total_Calculate();
                    Final_Total_Calculate();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
}



