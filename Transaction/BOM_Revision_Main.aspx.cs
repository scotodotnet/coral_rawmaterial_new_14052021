﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Master_BOM_Revision_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Material Planning Details";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session.Remove("BOM_Revision_PO_NO");
        Response.Redirect("BOM_Revision.aspx");

    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool Rights_Check = false;
        bool ErrFlag = false;

        DataTable dtdpurchase = new DataTable();
        query = "select Status from [Coral_ERP_Sales]..Sales_Material_Plan_Conform_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Cus_Pur_Order_No='" + e.CommandName.ToString() + "'";
        dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
        string status = dtdpurchase.Rows[0]["Status"].ToString();

        if (status == "" || status == "0")
        {
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("BOM_Revision_PO_NO");
            Session["BOM_Revision_PO_NO"] = Enquiry_No_Str;
            Response.Redirect("BOM_Revision.aspx");
        }
        else if (status == "2")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Material Planning Details Already Rejected..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Rejected..');", true);
        }
        else if (status == "3")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Material Planning Details put in pending..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details put in pending..');", true);
        }
        else if (status == "1")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Approved Material Planning Cant Edit..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Purchase Request Cant Edit..');", true);
        }


    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Distinct Order_No,GenModelNo,Revision_Name,Delivery_Date,Delivery_CW from BOM_Master_Revision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        //Print View Open
        string RptName = "";
        string Trans_No = "";

        RptName = "Material Planning BOM Details";
        Trans_No = e.CommandName.ToString();

        ResponseHelper.Redirect("~/Reports/ReportDisplay.aspx?Trans_No=" + Trans_No + "&RptName=" + RptName, "_blank", "");

    }
}