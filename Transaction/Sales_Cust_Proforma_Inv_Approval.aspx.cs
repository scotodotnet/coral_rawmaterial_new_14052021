﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Sales_Transaction_Sales_Cust_Proforma_Inv_Approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Proforma Invoice Approval";
            //Department_Code_Add();
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();
        }
        Load_Data_Empty_Grid();
    }

    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "4", "Sales Proforma Invoice Approval");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Request..');", true);
        }
        ////User Rights Check End


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + e.CommandArgument.ToString() + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Sales Invoice Details Already Cancelled.. Cannot Approved it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + e.CommandArgument.ToString() + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Sales Invoice Details Already get Approved..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + e.CommandArgument.ToString() + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table
                query = "Update Sales_Proforma_Invoice_Main set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Sales_Inv_No='" + e.CommandArgument.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Customer_Payment_Table_Saved(e.CommandArgument.ToString());


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Sales Invoice Details Approved Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Sales Invoice not is Avail give input correctly..');", true);
            }
        }
    }



    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "4", "Sales Proforma Invoice Approval");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Request..');", true);
        }
        ////User Rights Check End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + e.CommandArgument.ToString() + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Sales Invoice Details Already Approved.. Cannot Cancelled it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + e.CommandArgument.ToString() + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Sales Invoice Details Already get Cancelled..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + e.CommandArgument.ToString() + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table
                query = "Update Sales_Proforma_Invoice_Main set Status='2',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Sales_Inv_No='" + e.CommandArgument.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Sales Invoice Details Cancelled Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Sales Invoice not is Avail give input correctly..');", true);
            }
        }
    }



    private void Load_Data_Empty_Grid()
    {

        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();


        DT.Columns.Add("Sales_Inv_No");
        DT.Columns.Add("Sales_Inv_Date");
        DT.Columns.Add("Cus_Pur_Order_No");
        DT.Columns.Add("CustomerName");
        DT.Columns.Add("NetAmount");

        if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Sales_Inv_No,Sales_Inv_Date,Cus_Pur_Order_No,CustomerName,NetAmount from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null) Order By Sales_Inv_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_Pending.DataSource = DT;
            Repeater_Pending.DataBind();
            Repeater_Pending.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Sales_Inv_No,Sales_Inv_Date,Cus_Pur_Order_No,CustomerName,NetAmount from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' Order By Sales_Inv_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            Repeater_Approve.Visible = true;
            Repeater_Pending.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Sales_Inv_No,Sales_Inv_Date,Cus_Pur_Order_No,CustomerName,NetAmount from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' Order By Sales_Inv_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Rejected.DataSource = DT;
            Repeater_Rejected.DataBind();
            Repeater_Rejected.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Pending.Visible = false;
        }
    }

    //private void Department_Code_Add()
    //{
    //    string query = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDepartment.Items.Clear();
    //    query = "Select (DeptName + '-' + DeptCode) as DpetName_Join from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptName Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    //    txtDepartment.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDepartment.Items.Add(Main_DT.Rows[i]["DpetName_Join"].ToString());
    //    }
    //}

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }

    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        //string[] Department_Spilit = txtDepartment.Text.Split('-');
        //string Department_Code = "";
        //string Department_Name = "";
        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        DT.Columns.Add("Sales_Inv_No");
        DT.Columns.Add("Sales_Inv_Date");
        DT.Columns.Add("Cus_Pur_Order_No");
        DT.Columns.Add("CustomerName");
        DT.Columns.Add("NetAmount");



        if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Sales_Inv_No,Sales_Inv_Date,Cus_Pur_Order_No,CustomerName,NetAmount from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null) Order By Sales_Inv_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_Pending.DataSource = DT;
            Repeater_Pending.DataBind();
            Repeater_Pending.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Sales_Inv_No,Sales_Inv_Date,Cus_Pur_Order_No,CustomerName,NetAmount from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' Order By Sales_Inv_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            Repeater_Approve.Visible = true;
            Repeater_Pending.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Sales_Inv_No,Sales_Inv_Date,Cus_Pur_Order_No,CustomerName,NetAmount from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' Order By Sales_Inv_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Rejected.DataSource = DT;
            Repeater_Rejected.DataBind();
            Repeater_Rejected.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Pending.Visible = false;
        }
    }

    //protected void txtDepartment_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (txtDepartment.Text != "-Select-")
    //    {
    //        txtRequestStatus_SelectedIndexChanged(sender, e);
    //    }
    //}

    //private void Department_Code_Add_Test()
    //{
    //    string query = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDeptCode_test.Items.Clear();
    //    query = "Select DeptCode from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptCode Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    //    txtDeptCode_test.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDeptCode_test.Items.Add(Main_DT.Rows[i]["DeptCode"].ToString());
    //    }
    //}


    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {
        //Print View Open
        string RptName = "";
        string Trans_No = "";

        RptName = "Customer Proforma Invoice Format";
        Trans_No = e.CommandName.ToString();

        ResponseHelper.Redirect("~/Sales_Report/Sales_Report_Display.aspx?Trans_No=" + Trans_No + "&Genset_Model=" + "" + "&Customer_Name=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");

    }

    protected void GridPrintView_Approval(object sender, CommandEventArgs e)
    {
        //Print View Open
        string RptName = "";
        string Trans_No = "";

        RptName = "Customer Proforma Invoice Format";
        Trans_No = e.CommandName.ToString();

        ResponseHelper.Redirect("~/Sales_Report/Sales_Report_Display.aspx?Trans_No=" + Trans_No + "&Genset_Model=" + "" + "&Customer_Name=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");

    }

    protected void GridPrintView_Reject(object sender, CommandEventArgs e)
    {
        //Print View Open
        string RptName = "";
        string Trans_No = "";

        RptName = "Customer Proforma Invoice Format";
        Trans_No = e.CommandName.ToString();

        ResponseHelper.Redirect("~/Sales_Report/Sales_Report_Display.aspx?Trans_No=" + Trans_No + "&Genset_Model=" + "" + "&Customer_Name=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");

    }

    private void Customer_Payment_Table_Saved(string Transaction_No_Str)
    {
        string query = "";
        string Qty_Tot = "0";
        string Remarks_Str = "";
        string Generator_Model_Str = "";
        DataTable DT_CUS = new DataTable();

        //Get Qty Total
        query = "Select Isnull(Sum(Qty),0) as Qty from Sales_Proforma_Invoice_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + Transaction_No_Str + "'";
        DT_CUS = objdata.RptEmployeeMultipleDetails(query);
        if (DT_CUS.Rows.Count != 0)
        {
            Qty_Tot = DT_CUS.Rows[0]["Qty"].ToString();
        }

        //Get Qty Total
        query = "Select * from Sales_Proforma_Invoice_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + Transaction_No_Str + "'";
        DT_CUS = objdata.RptEmployeeMultipleDetails(query);
        if (DT_CUS.Rows.Count != 0)
        {
            Generator_Model_Str = DT_CUS.Rows[0]["GenModelName"].ToString();
        }

        query = "Select * from Sales_Customer_Transaction_Det Where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' and Trans_No='" + Transaction_No_Str + "'";
        DT_CUS = objdata.RptEmployeeMultipleDetails(query);
        if (DT_CUS.Rows.Count != 0)
        {
            query = "Delete from Sales_Customer_Transaction_Det Where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' and Trans_No='" + Transaction_No_Str + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        //Main Table Details
        query = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + Transaction_No_Str + "'";
        DT_CUS = objdata.RptEmployeeMultipleDetails(query);
        if (DT_CUS.Rows.Count != 0)
        {
            //Remarks Fill
            Remarks_Str = "Sales Invoice Generaror Model : " + Generator_Model_Str;
            //Insert Customer Payment Table
            query = "Insert Into Sales_Customer_Transaction_Det(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Type,";
            query = query + " CustomerID,CustomerName,Remarks,TotalQty,BillNetAmount,PaidAmount,UserID,UserName,Invoice_No) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Transaction_No_Str + "','" + DT_CUS.Rows[0]["Sales_Inv_Date"].ToString() + "',";
            query = query + " 'SALES INVOICE','" + DT_CUS.Rows[0]["CustomerID"].ToString() + "','" + DT_CUS.Rows[0]["CustomerName"].ToString() + "','" + Remarks_Str + "','" + Qty_Tot + "',";
            query = query + " '" + DT_CUS.Rows[0]["NetAmount"].ToString() + "','0.00','" + SessionUserID + "','" + SessionUserName + "','" + Transaction_No_Str + "')";
            objdata.RptEmployeeMultipleDetails(query);
        }
    }
}
