﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Escrow_PurOrd_Amend_Sub.aspx.cs" Inherits="Trans_Escrow_PO_Amend_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Purchase Amendment(Escrow)</h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Trans No</label>
                                                <asp:Label ID="lblAmendNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Trans Date</label>
                                                <asp:TextBox ID="txtTransDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Material Type</label>
                                                <asp:DropDownList ID="ddlMatType" runat="server" class="form-control select2"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">RawMaterial</asp:ListItem>
                                                    <asp:ListItem Value="2">Tools</asp:ListItem>
                                                    <asp:ListItem Value="3">Asset</asp:ListItem>
                                                    <asp:ListItem Value="4">General Items</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label" for="Req_No">Ref No</label>
                                                <asp:DropDownList ID="ddlPurOrdNo" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlPurOrdNo_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label" for="Req_No">Purchase Order No</label>
                                                <asp:Label ID="lblCoralOrdNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Date</label>
                                                <asp:TextBox ID="txtDate" MaxLength="20" class="form-control datepicker" runat="server"></asp:TextBox>

                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label" for="Req_No">Quotation No</label>
                                                <asp:TextBox ID="lblQuatNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label" for="Req_No">Quotation Date</label>
                                                <asp:TextBox ID="lblQuatDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Name</label>
                                                <asp:TextBox ID="lblSuppName" runat="server" class="form-control"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hfSuppCode" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Department Name</label>
                                                <asp:TextBox ID="lblDeptName" runat="server" class="form-control"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hfDeptCode" />
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Dispatch Through</label>
                                                <asp:TextBox name="txtDeliveryMode" ID="txtDeliveryMode" class="form-control" runat="server"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hfDeliModeCode" />
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Payment Mode</label>
                                                <asp:DropDownList name="txtPaymentMode" ID="txtPaymentMode" class="form-control select2" runat="server">
                                                    <asp:ListItem Value="- select -">- select -</asp:ListItem>
                                                    <asp:ListItem Value="CASH">CASH</asp:ListItem>
                                                    <asp:ListItem Value="CHEQUE">CHEQUE</asp:ListItem>
                                                    <asp:ListItem Value="NEFT">NEFT</asp:ListItem>
                                                    <asp:ListItem Value="RTGS">RTGS</asp:ListItem>
                                                    <asp:ListItem Value="DD">DD</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Request Date</label>
                                                <asp:TextBox ID="txtDeliveryDate" MaxLength="20" class="form-control datepicker" AutoComplete="off" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDeliveryDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtDeliveryDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Delivery At(Inco Terms)</label>
                                                <asp:TextBox ID="txtDeliveryAt" class="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Payment Terms</label>
                                                <asp:TextBox ID="txtPaymentTerms" TextMode="MultiLine" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Purpose (Sub)</label>
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Special Instruction (Sub)</label>
                                            <asp:TextBox ID="txtSplDesc" TextMode="MultiLine" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Note (Freight)</label>
                                            <asp:TextBox ID="txtNote" class="form-control" TextMode="MultiLine" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Delivery Schedules</label>
                                            <asp:TextBox ID="txtOthers" class="form-control" TextMode="MultiLine" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>

                                         <div class="form-group col-md-3">
                                            <label for="exampleInputName">Amendment Remark</label>
                                            <asp:TextBox ID="txtAmendRmk" class="form-control"  TextMode="MultiLine" 
                                                Style="resize: none;border-color:red" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Currency Type</label>
                                            <asp:DropDownList runat="server" ID="ddlCurrencyType" class="form-control select2" Style="width: 100%">
                                                <asp:ListItem Value="INR">INR</asp:ListItem>
                                                <asp:ListItem Value="EUR">EUR</asp:ListItem>
                                                <asp:ListItem Value="USD">USD</asp:ListItem>
                                                <asp:ListItem Value="JPY">JPY</asp:ListItem>
                                                <asp:ListItem Value="GBP">GBP</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group col-md-3" runat="server" visible="false">
                                            <label for="exampleInputName">Tax Type</label>
                                            <asp:DropDownList runat="server" ID="ddlTaxType" class="form-control select2" Style="width: 100%">
                                                <asp:ListItem Value="GST">GST</asp:ListItem>
                                                <asp:ListItem Value="IGST">IGST</asp:ListItem>
                                                <asp:ListItem Value="USD">USD</asp:ListItem>
                                                <asp:ListItem Value="NONE">NONE</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">GST(%)</label>
                                            <asp:DropDownList runat="server" ID="ddlGSTPre" class="form-control select2" Style="width: 100%"
                                                 AutoPostBack="true" OnSelectedIndexChanged="ddlGSTPre_SelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:HiddenField ID="hfGstPre" runat="server" />
                                        </div>
                                         <div class="form-group col-md-3">
                                            <label for="exampleInputName">Subject</label>
                                            <asp:TextBox ID="txtsubject" TextMode="MultiLine" class="form-control" Style="resize: none"
                                                runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Special Subject</label>
                                            <asp:TextBox ID="txtSplSubject" TextMode="MultiLine"
                                                class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Purpose</label>
                                            <asp:TextBox ID="txtPurpose" TextMode="MultiLine" class="form-control"
                                                
                                                Style="resize: none" runat="server"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="form-group col-md-4" runat="server" visible="false">
                                            <label for="exampleInputName">Pur Order Type</label>
                                            <asp:RadioButtonList ID="RdpPOType" class="form-control" runat="server"
                                                RepeatColumns="3" AutoPostBack="true">
                                                <asp:ListItem Value="1" Text="Direct" style="padding-right: 40px"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Req/Amend" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Special"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>



                                    </div>

                                    <div class="clearfix"></div>
                                    <br />

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Item Details</h3>
                                                </div>

                                                <div class="clearfix"></div>
                                                <br />
                                                <div class="clearfix" runat="server" visible="false"></div>

                                                <asp:Panel ID="pnlPurRqu" runat="server">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Request No</label>
                                                                <asp:TextBox ID="txtPurRquNo" class="form-control" runat="server"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Request Date</label>
                                                            <asp:Label ID="txtRequestDate" class="form-control datepicker" runat="server"></asp:Label>
                                                        </div>

                                                        <div class="form-group col-md-4" visible="false" runat="server" style="padding-top: 1.75%">
                                                            <asp:FileUpload ID="fuDocument" runat="server" class="form-control" BorderStyle="None" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlSingle" runat="server">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">SapNo</label>
                                                                <asp:DropDownList ID="ddlAmendSAPNo" class="form-control select2" AutoPostBack="true"
                                                                    runat="server" OnSelectedIndexChanged="ddlAmendSAPNo_SelectedIndexChanged"></asp:DropDownList>

                                                                <asp:HiddenField ID="hfAmendUomName" runat="server" />
                                                                <asp:HiddenField ID="hfAmendCGSTP" runat="server" />
                                                                <asp:HiddenField ID="hfAmendSGSTP" runat="server" />
                                                                <asp:HiddenField ID="hfAmendIGSTP" runat="server" />
                                                                


                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">ItemName</label>
                                                                <asp:DropDownList ID="ddlAmendItemName" class="form-control select2"  AutoPostBack="true"
                                                                    runat="server" OnSelectedIndexChanged="ddlAmendItemName_SelectedIndexChanged"></asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Required Qty</label>
                                                                <asp:TextBox ID="txtAmendReqQty" class="form-control" AutoPostBack="true"
                                                                    runat="server" OnTextChanged="txtAmendReqQty_TextChanged"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">No Of Set</label>
                                                                <asp:TextBox ID="txtAmendModelQty" class="form-control" AutoPostBack="true"
                                                                    runat="server" OnTextChanged="txtAmendModelQty_TextChanged"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Order Qty</label>
                                                                <asp:Label ID="lblAmendOrdQty" class="form-control" runat="server">
                                                                </asp:Label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Rate</label>
                                                                <asp:TextBox ID="txtAmendRate" class="form-control" AutoPostBack="true"
                                                                    runat="server" OnTextChanged="txtAmendRate_TextChanged"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">ItemTotal</label>
                                                                <asp:Label ID="lblAmendItemTot" class="form-control" 
                                                                    runat="server"></asp:Label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">CGST</label>
                                                                <asp:Label ID="lblAmendCGST" class="form-control" 
                                                                    runat="server"></asp:Label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">SGST</label>
                                                                <asp:Label ID="lblAmendSGST" class="form-control" 
                                                                    runat="server"></asp:Label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">IGST</label>
                                                                <asp:Label ID="lblAmendIGST" class="form-control" 
                                                                    runat="server"></asp:Label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Line Total</label>
                                                                <asp:Label ID="lblAmendLineTot" class="form-control" 
                                                                    runat="server"></asp:Label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Calendar Week</label>
                                                                <asp:TextBox ID="txtAmendCalWeek" class="form-control" 
                                                                    runat="server"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group" >
                                                                <asp:Button ID="btnAdd" class="btn btn-primary" 
                                                                    runat="server" Text="Add" OnClick="btnAdd_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>


                                                <div class="box box-primary">

                                                    <div class="box-header with-border">
                                                        <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Order Item List</span></h3>
                                                    </div>
                                                    <asp:Panel ID="pnlRpter" runat="server" Height="400px" Style="overflow-x: scroll; overflow-y: scroll">
                                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false" OnItemDataBound="Repeater1_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblAmend" class="table table-hover table-bordered table-striped">
                                                                    <thead>

                                                                        <tr>
                                                                            <th rowspan="2" colspan="1">SL.No</th>
                                                                            <th rowspan="2" colspan="1">SAP_No</th>
                                                                            <th rowspan="2" colspan="1">ItemName</th>
                                                                            <th rowspan="2" colspan="1">UOM</th>
                                                                            <th rowspan="1" colspan="5" style="text-align: center">Before Amendment</th>
                                                                            <th rowspan="1" colspan="5" style="text-align: center">After Amendment</th>
                                                                            <th rowspan="2" colspan="1">ItemTotal</th>
                                                                            <th rowspan="2" colspan="1">GST%</th>
                                                                            <th rowspan="2" colspan="1">CGST</th>
                                                                            <th rowspan="2" colspan="1">SGST</th>
                                                                            <th rowspan="2" colspan="1">IGST</th>
                                                                            <th rowspan="2" colspan="1">Total</th>
                                                                            <th rowspan="2" colspan="1">Mode</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Required_Qty</th>
                                                                            <th>Model_Qty</th>
                                                                            <th>Order_Qty</th>
                                                                            <th>Rate</th>
                                                                            <th>Calendar_Week</th>
                                                                            <th runat="server" visible="false">Requ._Date</th>
                                                                            <th>Amend_Required_Qty</th>
                                                                            <th>Amend_Model_Qty</th>
                                                                            <th>Amend_Order_Qty</th>
                                                                            <th>Amend_Rate</th>
                                                                            <th>Amend_Calendar_Week</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>

                                                                    <td>
                                                                        <asp:Label ID="lblSAPNo" runat="server" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblItemCode" runat="server" Text='<%# Eval("ItemCode")%>'>
                                                                        </asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOMCode")%>'></asp:Label>
                                                                    </td>


                                                                    <%--Amendment Brfore--%>

                                                                    <td>
                                                                        <asp:Label ID="lblRQty" class="form-control" Style="width: 100%"
                                                                            runat="server" Text='<%# Eval("ReuiredQty")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblMdlQty" class="form-control" Style="width: 100%"
                                                                            runat="server" Text='<%# Eval("ModelQty")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblRquQty" class="form-control" Style="width: 100%"
                                                                            runat="server" Text='<%# Eval("OrderQty")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblRate" class="form-control" Style="width: 100%"
                                                                            runat="server" Text='<%# Eval("Rate")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblCalWeek" class="form-control" Style="width: 100%"
                                                                            runat="server" Text='<%# Eval("CalWeek")%>'></asp:Label>
                                                                    </td>
                                                                    <%--*************************************--%>

                                                                    <%--Amendment After--%>

                                                                    <td>
                                                                        <asp:TextBox ID="txtReqQty" class="form-control" Style="width: 100%"
                                                                            runat="server" Text='<%# Eval("AmendRQty")%>'
                                                                            AutoComplete="off" AutoPostBack="true" OnTextChanged="txtReqQty_TextChanged">
                                                                        </asp:TextBox>
                                                                    </td>

                                                                    <td>
                                                                        <asp:TextBox ID="txtModelQty" class="form-control" Style="width: 100%"
                                                                            runat="server" Text='<%# Eval("AmendMQty")%>'
                                                                            AutoComplete="off" AutoPostBack="true" OnTextChanged="txtModelQty_TextChanged">
                                                                        </asp:TextBox>
                                                                    </td>

                                                                    <td>
                                                                        <asp:TextBox ID="txtOrdQty" class="form-control" Style="width: 100%" runat="server"
                                                                            Text='<%# Eval("AmendOrdQty")%>' AutoComplete="off" AutoPostBack="true"
                                                                            OnTextChanged="txtOrdQty_TextChanged">
                                                                        </asp:TextBox>
                                                                    </td>

                                                                    <td>
                                                                        <asp:TextBox ID="txtGrdRate" class="form-control" Style="width: 100%"
                                                                            runat="server" Text='<%# Eval("AmendRate")%>' AutoComplete="off"
                                                                            AutoPostBack="true" OnTextChanged="txtGrdRate_TextChanged">
                                                                        </asp:TextBox>
                                                                    </td>

                                                                    <td>
                                                                        <asp:TextBox ID="txtCalWeek" class="form-control"
                                                                            Style="width: 100%" runat="server" Text='<%# Eval("AmendCalWeek")%>'
                                                                            AutoComplete="off" AutoPostBack="true" OnTextChanged="txtAmCalWeek_TextChanged">
                                                                        </asp:TextBox>
                                                                    </td>

                                                                    <%--**************************************************--%>

                                                                    <td runat="server" visible="false">
                                                                        <asp:TextBox ID="txtRDate" BorderStyle="None" runat="server"
                                                                            Text='<%# Eval("ReuiredDate")%>'>
                                                                        </asp:TextBox>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblItemTot" runat="server" Text='<%# Eval("ItemTotal") %>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblDiscPer" runat="server" Text='<%# Eval("Discount_Per")%>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblDisc" runat="server" Text='<%# Eval("Discount") %>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblGSTPer" runat="server" Text='<%# Eval("GstPer")%>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblCGSTPer" runat="server" Text='<%# Eval("CGSTPer") %>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblCGSTAmt" runat="server" Text='<%# Eval("CGSTAmount") %>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblSGSTPer" runat="server" Text='<%# Eval("SGSTPer") %>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblSGSTAmt" runat="server" Text='<%# Eval("SGSTAmount") %>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblIGSTPer" runat="server" Text='<%# Eval("IGSTPer") %>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblIGSTAmt" runat="server" Text='<%# Eval("IGSTAmount") %>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblOtherAmt" runat="server" Text='<%# Eval("OtherCharge") %>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblLineTot" runat="server" Text='<%# Eval("LineTotal")%>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteClick" CommandName='<%# Eval("ItemCode")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </asp:Panel>

                                                </div>
                                                <!-- /.mail-box-messages -->
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-8"></div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Total Qty</label>
                                                    <asp:Label ID="txtTotQty" class="form-control" runat="server"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-2">

                                                    <label for="exampleInputName">Total Amt</label>
                                                    <asp:Label ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6"></div>
                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Total CGST</label>
                                                    <asp:Label ID="lblTotCGST" runat="server" class="form-control"></asp:Label>
                                                </div>


                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Total SGST</label>
                                                    <asp:Label ID="lblTotSGST" runat="server" class="form-control"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Total IGST</label>
                                                    <asp:Label ID="lblTotIGST" runat="server" class="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6"></div>
                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Total Line Total</label>
                                                    <asp:Label ID="lblTotLineAmt" runat="server" class="form-control"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Add or Less</label>
                                                    <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server"
                                                        Text="0" AutoPostBack="true" OnTextChanged="txtAddOrLess_TextChanged"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Net Amt</label>
                                                    <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" />
                                            <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server" Text="Back" OnClick="btnBackRequest_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <%--<script type="text/javascript" src="../assets/js/Trans_Receipt_Calc.js"></script>--%>
    <%--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>--%>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            console.log("Start");
            $("#tblAmend").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": true,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });
        });
    </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                    $("#tblAmend").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": true,
                        "ordering": false,
                        "searching": true
                        //"drawCallback":true
                    });
                }
            });
        };
    </script>
</asp:Content>

