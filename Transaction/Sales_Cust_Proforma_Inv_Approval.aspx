﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Sales_Cust_Proforma_Inv_Approval.aspx.cs" Inherits="Sales_Transaction_Sales_Cust_Proforma_Inv_Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="content-wrapper">
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Proforma Invoice Approval</span></h3>
                        </div>
                    
                        <div id="Div1" class="row" runat="server" style="padding-top:25px;padding-bottom:25px">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label class="control-label" for="Req_No">Customer Proforma Invoice Status</label>
                                    <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                        onselectedindexchanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="box-body no-padding">
                            <div class="table-responsive mailbox-messages">
                                <div class="col-md-12">
					                <div class="row">
                                        <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
			                                <HeaderTemplate>
                                                <table id="example" class="table table-hover table-striped">
                                                    <thead>
                                        <tr>
                                            <th>Proforma Inv.No</th>
                                            <th>Date</th>
                                            <th>PO. No</th>
                                            <th>Customer</th>
                                            <th>Net Amount</th>
                                            <th>Approve</th>
                                            <th>Cancel</th>
                                            <th>View</th>
                                            </tr>
                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("Sales_Inv_No")%></td>
                                                    <td><%# Eval("Sales_Inv_Date")%></td>
                                                    <td><%# Eval("Cus_Pur_Order_No")%></td>
                                                    <td><%# Eval("CustomerName")%></td>
                                                    <td><%# Eval("NetAmount")%></td>                                        
                                                    <td>
                                                        <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Sales_Inv_No")%>' CommandName='<%# Eval("Sales_Inv_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this PROFORMA INVOICE details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="LinkButton1" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Sales_Inv_No")%>' CommandName='<%# Eval("Sales_Inv_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this PROFORMA INVOICE details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-print"  
                                                            runat="server" Text="" OnCommand="GridEditPurRequestClick" 
                                                            CommandArgument='<%# Eval("Sales_Inv_No")%>' CommandName='<%# Eval("Sales_Inv_No")%>'>
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
			                        </asp:Repeater>
					            </div>
                                    <div class="row">
			            <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false">
			                <HeaderTemplate>
                                <table id="example" class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>Proforma Inv.No</th>
                                            <th>Date</th>
                                            <th>PO. No</th>
                                            <th>Customer</th>
                                            <th>Net Amount</th>
                                            <th>View</th>   
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Sales_Inv_No")%></td>
                                    <td><%# Eval("Sales_Inv_Date")%></td>
                                    <td><%# Eval("Cus_Pur_Order_No")%></td>
                                    <td><%# Eval("CustomerName")%></td>
                                    <td><%# Eval("NetAmount")%></td>
                                    <td>
                                        <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-print"  
                                            runat="server" Text="" OnCommand="GridPrintView_Approval" 
                                            CommandArgument='<%# Eval("Sales_Inv_No")%>' CommandName='<%# Eval("Sales_Inv_No")%>'>
                                        </asp:LinkButton>
                                    </td>    
                                        
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>                                
			            </asp:Repeater>
			        </div>
                                    <div class="row">
			            <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
			                <HeaderTemplate>
                                <table id="example" class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>Proforma Inv.No</th>
                                            <th>Date</th>
                                            <th>PO. No</th>
                                            <th>Customer</th>
                                            <th>Net Amount</th>
                                            <th>View</th>   
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Sales_Inv_No")%></td>
                                    <td><%# Eval("Sales_Inv_Date")%></td>
                                    <td><%# Eval("Cus_Pur_Order_No")%></td>
                                    <td><%# Eval("CustomerName")%></td>
                                    <td><%# Eval("NetAmount")%></td>      
                                    <td>
                                        <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-print"  
                                            runat="server" Text="" OnCommand="GridPrintView_Reject" 
                                            CommandArgument='<%# Eval("Sales_Inv_No")%>' CommandName='<%# Eval("Sales_Inv_No")%>'>
                                        </asp:LinkButton>
                                    </td> 
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>                                
			            </asp:Repeater>
			        </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
    
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    
</asp:Content>

