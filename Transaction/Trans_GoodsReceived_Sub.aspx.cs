﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_GoodsReceived_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGRNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal ReqQty;

    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;
    static Decimal RowCount = 0;
    static Decimal BatchQty = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Goods Received";
            Initial_Data_Referesh();
            Initial_Data_SerialNo();
            Initial_Data_BatchNo();
            Load_Data_Warehouse();

            Load_PurOrder_GatePassIN();

            //Load_PurOrder_No();

            txtGRDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy");

            if ((string)Session["GRNo"] == null)
            {
                SessionGRNo = "";

                //TransactionNoGenerate TransNO = new TransactionNoGenerate();
                //string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Goods Received", SessionFinYearVal, "1");
                //if (Auto_Transaction_No == "")
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                //}
                //else
                //{
                //    txtGRNo.Text = Auto_Transaction_No;
                //}

            }
            else
            {
                SessionGRNo = Session["GRNo"].ToString();
                txtGRNo.Text = SessionGRNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }

    private void Load_PurOrder_GatePassIN()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select GM.Pur_GIN_No from Trans_Pur_GIN_Main GM Inner Join Trans_Pur_GIN_Sub GS on GS.Pur_GIN_No=GM.Pur_GIN_No";
        SSQL = SSQL + " Where GS.Ccode='" + SessionCcode + "' and GS.LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " GS.AccType='" + ddlSupplierType.SelectedValue + "' and GS.MatType='" + ddlMatType.SelectedValue + "' and ";
        SSQL = SSQL + " GM.ApprovalStatus='1' and GM.Status !='Delete' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlGPIN.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["Pur_GIN_No"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlGPIN.DataTextField = "Pur_GIN_No";
        ddlGPIN.DataValueField = "Pur_GIN_No";
        ddlGPIN.DataBind();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        string MatType = "";
        string PackGst = "";
        string TransGst = "";
        string CourierGst = "";
        string Other1Gst = "";
        string Other2Gst = "";

        if (ddlMatType.SelectedItem.Text == "Raw Material")
        {
            MatType = "1";
        }
        else if (ddlMatType.SelectedItem.Text == "Tools")
        {
            MatType = "2";
        }
        else if (ddlMatType.SelectedItem.Text == "Assets")
        {
            MatType = "3";
        }
        else if (ddlMatType.SelectedItem.Text == "General Item")
        {
            MatType = "4";
        }


        if (chkPFGst.Checked == true)
        {
            PackGst = "1";
        }
        else
        {
            PackGst = "0";
        }

        if (chkTCGst.Checked == true)
        {
            TransGst = "1";
        }
        else
        {
            TransGst = "0";
        }

        if (chkCCGst.Checked == true)
        {
            CourierGst = "1";
        }
        else
        {
            CourierGst = "0";
        }

        if (chkOther1Gst.Checked == true)
        {
            Other1Gst = "1";
        }
        else
        {
            Other1Gst = "0";
        }

        if (chkOther2Gst.Checked == true)
        {
            Other2Gst = "1";
        }
        else
        {
            Other2Gst = "0";
        }

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Goods Received", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGRNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            //if (btnSave.Text == "Update")
            //{

            SSQL = "Delete From Trans_GoodsReceipt_Main ";
            SSQL = SSQL + " Where GRNo='" + txtGRNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete from Trans_GoodsReceipt_Sub";
            SSQL = SSQL + " Where GRNo='" + txtGRNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete from Trans_Stock_Ledger_All ";
            SSQL = SSQL + " Where Trans_No='" + txtGRNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete from Trans_Stock_SerialNo ";
            SSQL = SSQL + " Where GRNNo='" + txtGRNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete from Trans_Stock_BatchNo ";
            SSQL = SSQL + " Where GRNNo='" + txtGRNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

            objdata.RptEmployeeMultipleDetails(SSQL);

            //Insert Main Table
            SSQL = "Insert Into Trans_GoodsReceipt_Main(CCode,LCode,FinYearCode,FinYearVal,GRNo,GRDate,PurOrdNo,PurOrdDate,PartyInvNo,";
            SSQL = SSQL + " PartyInvDate,SuppType,SuppCode,SuppName,PayTerms,PayMode,VehicleNo,Notes,Remarks,TotalQty,TotAmt,RoundOff,";
            SSQL = SSQL + " TotalAmount,UserId,UserName,Status,CreateOn,CurrencyType,GSTType,TotLineAmt,TotCGSTAmt,TotSGSTAmt,TotIGSTAmt,";
            SSQL = SSQL + " TotDiscAmt,MatType,PackWGST,TotPackChr,TransWGST,TotTransChr,CourWGST,TotCourChr,GPINo,GPIDate,";
            SSQL = SSQL + " Othr1WGST,TotOther1Chr,Othr2WGST,TotOther2Chr) Values('" + SessionCcode + "','" + SessionLcode + "', ";
            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtGRNo.Text + "','" + txtGRDate.Text + "',";
            SSQL = SSQL + " '" + ddlPurOrdNo.SelectedItem.Text + "','" + txtPurOrdDate.Text + "','" + txtPartyInvNo.Text + "',";
            SSQL = SSQL + " '" + txtPartyInvDate.Text + "','" + ddlSupplierType.SelectedValue + "','" + hdSuppCode.Value + "',";
            SSQL = SSQL + " '" + txtSuppName.Text + "','" + txtPayTerms.Text + "','" + txtPayMode.Text + "','" + txtVehiNo.Text + "',";
            SSQL = SSQL + " '" + txtNotes.Text + "','" + txtRemarks.Text + "','" + txtTotQty.Text + "','" + txtTotAmt.Text + "',";
            SSQL = SSQL + " '" + txtRoundOff.Text + "','" + txtNetAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "',";
            SSQL = SSQL + " '0',Getdate(),'" + txtCurrency.Text + "','" + txtGSTType.Text + "','" + lblTotLineAmt.Text + "',";
            SSQL = SSQL + " '" + lblTotCGST.Text + "','" + lblTotSGST.Text + "','" + lblTotIGST.Text + "','" + lblTotDisc.Text + "',";
            SSQL = SSQL + " '" + MatType + "','" + PackGst + "','" + txtPackFrwdCharge.Text + "','" + TransGst + "',";
            SSQL = SSQL + " '" + txtTransCharge.Text + "', '" + CourierGst + "','" + txtCourierCharge.Text + "',";
            SSQL = SSQL + " '" + ddlGPIN.SelectedItem.Text + "','" + txtGINDate.Text + "','" + Other1Gst + "','" + txtOther1Amt.Text + "',";
            SSQL = SSQL + " '" + Other2Gst + "','" + txtOther2Amt.Text + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Label SapNo = Repeater1.Items[i].FindControl("lblSAPNo") as Label;
                Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                Label DeptCode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;
                Label UOM = Repeater1.Items[i].FindControl("lblUOM") as Label;
                Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                Label Warehouse = Repeater1.Items[i].FindControl("lblWarehouse") as Label;
                Label RackName = Repeater1.Items[i].FindControl("lblRackName") as Label;
                Label OrdQty = Repeater1.Items[i].FindControl("lblOrdQty") as Label;
                TextBox ReceiveQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;
                Label BalQty = Repeater1.Items[i].FindControl("lblBalQty") as Label;
                Label ExcessQty = Repeater1.Items[i].FindControl("lblExcessQty") as Label;

                Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;
                Label ItemTotal = Repeater1.Items[i].FindControl("lblItemTot") as Label;

                TextBox txtDescP = Repeater1.Items[i].FindControl("txtGrdDiscp") as TextBox;
                Label lblDiscAmt = Repeater1.Items[i].FindControl("lblDiscAmt") as Label;

                Label CGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                Label CGSTAmount = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label SGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                Label SGSTAmount = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label IGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                Label IGSTAmount = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;


                if (ReceiveQty.Text.ToString() != "0.00" && ReceiveQty.Text.ToString() != "0.0" && ReceiveQty.Text.ToString() != "0")
                {

                    SSQL = "Insert Into Trans_GoodsReceipt_Sub(CCode,LCode,FinYearCode,FinYearVal,GRNo,GRDate,PurOrdNo,";
                    SSQL = SSQL + " PurOrdDate,SapNo,ItemCode,ItemName,DeptCode,DeptName,UOM,WarehouseCode,WarehouseName,";
                    SSQL = SSQL + " RackName,PurOrdQty,ReceiveQty,BalQty,Rate,ItemTotal,Disc,DiscAmt,CGSTPer,CGSTAmt,";
                    SSQL = SSQL + " SGSTPer,SGSTAmt,IGSTPer,IGSTAmt,LineTotal,UserId,UserName,Status,CreateOn,ExcessQty) ";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                    SSQL = SSQL + " '" + SessionFinYearVal + "','" + txtGRNo.Text + "','" + txtGRDate.Text + "',";
                    SSQL = SSQL + " '" + ddlPurOrdNo.SelectedItem.Text + "','" + txtPurOrdDate.Text + "',";
                    SSQL = SSQL + " '" + SapNo.Text + "', '" + ItemCode.Text + "','" + ItemName.Text + "', ";
                    SSQL = SSQL + " '" + DeptCode.Text + "','" + DeptName.Text + "','" + UOM.Text + "',";
                    SSQL = SSQL + " '" + WarehouseCode.Text + "','" + Warehouse.Text + "', '" + RackName.Text + "',";
                    SSQL = SSQL + " '" + OrdQty.Text + "','" + ReceiveQty.Text.ToString() + "','" + BalQty.Text + "',";
                    SSQL = SSQL + " '" + Rate.Text + "','" + ItemTotal.Text + "','" + txtDescP.Text + "',";
                    SSQL = SSQL + " '" + lblDiscAmt.Text + "',";
                    SSQL = SSQL + " '" + CGSTPer.Text.ToString() + "','" + CGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + SGSTPer.Text.ToString() + "','" + SGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + IGSTPer.Text.ToString() + "','" + IGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + LineTotal.Text.ToString() + "','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " '',Getdate(),'" + ExcessQty.Text + "') ";
                    //SSQL = SSQL + " '" + Stock_Qty + "','" + Last_Issue_Date + "','" + Last_Purchase_Date + "','" + Last_Purchase_Qty + "','" + Prev_Purchase_Rate + "','" + dt.Rows[i]["Re_Order_Stock_Qty"].ToString() + "','" + dt.Rows[i]["Re_Order_Item_Qty"].ToString() + "','" + dt.Rows[i]["Indent_Type"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    //Stock Insert
                    DateTime transDate = Convert.ToDateTime(txtGRDate.Text);

                    SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,";
                    SSQL = SSQL + " Trans_Date_Str,Trans_Type,Supp_Type,Mat_Type,Supp_Code,Supp_Name,ItemCode,ItemName,";
                    SSQL = SSQL + " DeptCode,DeptName,UOM,WarehouseCode,WarehouseName,Add_Qty,";
                    SSQL = SSQL + " Add_Value,Minus_Qty,Minus_Value,UserID,UserName,CurrencyType,SapNo) Values(";
                    SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                    SSQL = SSQL + " '" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtGRNo.Text + "','" + transDate.ToString("dd/MM/yyyy") + "','" + txtGRDate.Text + "',";
                    SSQL = SSQL + " 'Goods Received','" + ddlSupplierType.SelectedValue + "','" + ddlMatType.SelectedValue + "',";
                    SSQL = SSQL + " '" + hdSuppCode.Value + "','" + txtSuppName.Text + "','" + ItemCode.Text + "',";
                    SSQL = SSQL + " '" + ItemName.Text + "','" + DeptCode.Text + "','" + DeptName.Text + "','" + UOM.Text + "',";
                    SSQL = SSQL + " '" + WarehouseCode.Text + "','" + Warehouse.Text + "','" + ReceiveQty.Text.ToString() + "',";
                    SSQL = SSQL + " '" + ItemTotal.Text.ToString() + "','0.00','0.00','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " '" + txtCurrency.Text + "','" + SapNo.Text + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            //Serial Number Updation

            DataTable dt_SerialNo = new DataTable();
            dt_SerialNo = (DataTable)ViewState["SerialNoDet"];

            for (int j = 0; j < dt_SerialNo.Rows.Count; j++)
            {
                SSQL = "Insert into Trans_Stock_SerialNo( CCode,LCode,FinYearCode,FinYearVal,GRNNo,GRNDate,PurOrderNo,PurOrderDate,PurGPINNo,";
                SSQL = SSQL + " PurGPINDate,SAPNo,ItemCode,ItemName,UOM,SerialNo,Qty,Status,Stock_Status,UserCode,UserName,CreateOn) Values (";
                SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + txtGRNo.Text + "','" + txtGRDate.Text + "','" + ddlPurOrdNo.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + txtPurOrdDate.Text + "','" + ddlGPIN.SelectedItem.Text + "','" + txtGINDate.Text + "',";
                SSQL = SSQL + " '" + dt_SerialNo.Rows[j]["SAPNo"].ToString() + "','" + dt_SerialNo.Rows[j]["ItemCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt_SerialNo.Rows[j]["ItemName"].ToString() + "','" + dt_SerialNo.Rows[j]["UOM"].ToString() + "',";
                SSQL = SSQL + " '" + dt_SerialNo.Rows[j]["SerialNo"].ToString() + "','1','ADD','0','" + SessionUserID + "',";
                SSQL = SSQL + " '" + SessionUserName + "',Convert(nvarchar,GetDate(),103))";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }


            //Batch Number Updation

            DataTable dt_BatchNo = new DataTable();
            dt_BatchNo = (DataTable)ViewState["BatchNoDet"];

            for (int j = 0; j < dt_BatchNo.Rows.Count; j++)
            {
                SSQL = "Insert into Trans_Stock_BatchNo( CCode,LCode,FinYearCode,FinYearVal,GRNNo,GRNDate,PurOrderNo,PurOrderDate,PurGPINNo,";
                SSQL = SSQL + " PurGPINDate,SAPNo,ItemCode,ItemName,UOM,BatchNo,Qty,Status,Stock_Status,UserCode,UserName,CreateOn) Values (";
                SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + txtGRNo.Text + "','" + txtGRDate.Text + "','" + ddlPurOrdNo.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + txtPurOrdDate.Text + "','" + ddlGPIN.SelectedItem.Text + "','" + txtGINDate.Text + "',";
                SSQL = SSQL + " '" + dt_BatchNo.Rows[j]["SAPNo"].ToString() + "','" + dt_BatchNo.Rows[j]["ItemCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt_BatchNo.Rows[j]["ItemName"].ToString() + "','" + dt_BatchNo.Rows[j]["UOM"].ToString() + "',";
                SSQL = SSQL + " '" + dt_BatchNo.Rows[j]["BatchNo"].ToString() + "','" + dt_BatchNo.Rows[j]["Qty"].ToString() + "',";
                SSQL = SSQL + " 'ADD','0','" + SessionUserID + "','" + SessionUserName + "',Convert(nvarchar,GetDate(),103))";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (ddlSupplierType.SelectedItem.Text == "Escrow")
            {
                SSQL = "Update Trans_Escrow_PurOrd_Main set ReceiveStatus ='1' Where Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
            }
            else if (ddlSupplierType.SelectedItem.Text == "Enercon")
            {
                SSQL = "Update Trans_Enercon_PurOrd_Main set ReceiveStatus ='1' Where Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
            }
            else
            {
                SSQL = "Update Trans_Coral_PurOrd_Main set ReceiveStatus ='1' Where Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "' and ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
            }

            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Update Trans_Pur_GIN_Main set ApprovalStatus ='1' Where Pur_GIN_No='" + ddlGPIN.SelectedItem.Text + "' and ";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";

            objdata.RptEmployeeMultipleDetails(SSQL);


            if (btnSave.Text != "Update")
            {
                TransactionNoGenerate TransNO_up = new TransactionNoGenerate();
                string Auto_Transaction_No_Up = "";
                Auto_Transaction_No_Up = TransNO_up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Goods Received", SessionFinYearVal, "4");
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Pur_Request_No"] = txtGRNo.Text;
            btnSave.Text = "Update";
            Response.Redirect("Trans_GoodsReceived_Main.aspx");
        }
    }


    private void Stock_Add()
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtGRNo.Text = ""; txtGRDate.Text = "";
        // txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        //txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Pur_Request_No");
        //Load_Data_Enquiry_Grid();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("Rack", typeof(string)));

        //dt.Columns.Add(new DataColumn("PurQty", typeof(string)));
        //dt.Columns.Add(new DataColumn("ReceivQty", typeof(string)));
        //dt.Columns.Add(new DataColumn("BalQty", typeof(string)));

        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReceiveQty", typeof(string)));
        dt.Columns.Add(new DataColumn("BalQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ExcessQty", typeof(string)));

        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("DiscPer", typeof(string)));
        dt.Columns.Add(new DataColumn("DiscAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));

        ViewState["ItemTable"] = dt;

        Repeater1.DataSource = dt;
        Repeater1.DataBind();


        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        //Load_OLD_data();
        TotalReqQty();

        Grid_Total_Value();

        OtherCharge_Calc();

    }
    public void TotalReqQty()
    {

        //ReqQty = 0;
        //DataTable dt = new DataTable();
        //dt = (DataTable)ViewState["ItemTable"];
        //for (int i = 0; i < dt.Rows.Count; i++)
        //{
        //    ReqQty = Convert.ToDecimal(ReqQty) + Convert.ToDecimal(dt.Rows[i]["ReuiredQty"]);
        //}
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //Search Purchase Enquiry
            string SSQL = "";
            DataTable Main_DT = new DataTable();
            SSQL = "Select * from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GRNo ='" + txtGRNo.Text + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Main_DT.Rows.Count != 0)
            {
                //txtGRNo.ReadOnly = true;
                txtGRNo.Text = Main_DT.Rows[0]["GRNo"].ToString();
                txtGRDate.Text = Main_DT.Rows[0]["GRDate"].ToString();
                ddlGPIN.SelectedValue = Main_DT.Rows[0]["GPINo"].ToString();
                txtGINDate.Text = Main_DT.Rows[0]["GPIDate"].ToString();


                ddlGPIN.SelectedItem.Text = Main_DT.Rows[0]["GPINo"].ToString();
                //if (ddlGPIN.SelectedItem.Text != "-Select-")
                //{


                ddlGPIN_SelectedIndexChanged(sender, e);
                //}


                ddlPurOrdNo.SelectedValue = Main_DT.Rows[0]["PurOrdNo"].ToString();
                //ddlPurOrdNo.SelectedItem.Text = Main_DT.Rows[0]["PurOrdNo"].ToString();
                txtPurOrdDate.Text = Main_DT.Rows[0]["PurOrdDate"].ToString();

                //ddlGPIN.SelectedItem.Text = Main_DT.Rows[0][""].ToString();

                ddlSupplierType.SelectedValue = Main_DT.Rows[0]["SuppType"].ToString();
                ddlMatType.SelectedValue = Main_DT.Rows[0]["MatType"].ToString();

                hdSuppCode.Value = Main_DT.Rows[0]["SuppCode"].ToString();
                txtSuppName.Text = Main_DT.Rows[0]["SuppName"].ToString();

                txtPartyInvNo.Text = Main_DT.Rows[0]["PartyInvNo"].ToString();
                txtPartyInvDate.Text = Main_DT.Rows[0]["PartyInvDate"].ToString();

                txtPayTerms.Text = Main_DT.Rows[0]["PayTerms"].ToString();
                txtPayMode.Text = Main_DT.Rows[0]["PayMode"].ToString();
                txtVehiNo.Text = Main_DT.Rows[0]["VehicleNo"].ToString();

                txtCurrency.Text = Main_DT.Rows[0]["CurrencyType"].ToString();
                txtGSTType.Text = Main_DT.Rows[0]["GSTType"].ToString();

                txtNotes.Text = Main_DT.Rows[0]["Notes"].ToString();
                txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

                //txtTotAmt.Text = Main_DT.Rows[0]["TotAmt"].ToString();
                //txtTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();

                //lblTotLineAmt.Text = Main_DT.Rows[0]["TotLineAmt"].ToString();
                //lblTotCGST.Text = Main_DT.Rows[0]["TotCGSTAmt"].ToString();
                //lblTotSGST.Text = Main_DT.Rows[0]["TotSGSTAmt"].ToString();
                //lblTotIGST.Text = Main_DT.Rows[0]["TotIGSTAmt"].ToString();

                if (Main_DT.Rows[0]["PackWGST"].ToString() == "1")
                {
                    chkPFGst.Checked = true;
                }
                else
                {
                    chkPFGst.Checked = false;
                }

                if (Main_DT.Rows[0]["TransWGST"].ToString() == "1")
                {
                    chkTCGst.Checked = true;
                }
                else
                {
                    chkTCGst.Checked = false;
                }

                if (Main_DT.Rows[0]["CourWGST"].ToString() == "1")
                {
                    chkCCGst.Checked = true;
                }
                else
                {
                    chkCCGst.Checked = false;
                }

                if (Main_DT.Rows[0]["Othr1WGST"].ToString() == "1")
                {
                    chkOther1Gst.Checked = true;
                }
                else
                {
                    chkOther1Gst.Checked = false;
                }

                if (Main_DT.Rows[0]["Othr2WGST"].ToString() == "1")
                {
                    chkOther2Gst.Checked = true;
                }
                else
                {
                    chkOther2Gst.Checked = false;
                }

                txtPackFrwdCharge.Text = Main_DT.Rows[0]["TotPackChr"].ToString();
                txtTransCharge.Text = Main_DT.Rows[0]["TotTransChr"].ToString();
                txtCourierCharge.Text = Main_DT.Rows[0]["TotCourChr"].ToString();

                txtOther1Amt.Text = Main_DT.Rows[0]["TotOther1Chr"].ToString();
                txtOther2Amt.Text = Main_DT.Rows[0]["TotOther2Chr"].ToString();

                txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
                //txtNetAmt.Text = Main_DT.Rows[0]["TotalAmount"].ToString();



                //Pur_Enq_Main_Sub Table Load
                DataTable dt = new DataTable();
                SSQL = "Select RSUB.SAPNo[SAPNo],RSUB.ItemCode[ItemCode],RSUB.ItemName[ItemName],RSUB.DeptCode[DeptCode],RSUB.DeptName[DeptName],";
                SSQL = SSQL + " RSUB.WarehouseCode[WarehouseCode],RSUB.WarehouseName[WarehouseName],RSUB.RackName[Rack],";
                SSQL = SSQL + " RSUB.PurOrdQty[OrderQty],RSUB.ReceiveQty[ReceiveQty],RSUB.BalQty[BalQty],RSUB.Rate[Rate],Disc[DiscPer],";
                SSQL = SSQL + " DiscAmt[DiscAmount],RSUB.ItemTotal[ItemTotal],RSUB.CGSTPer[CGSTPer],RSUB.CGSTAmt [CGSTAmount], ";
                SSQL = SSQL + " RSUB.SGSTPer[SGSTPer],RSUB.SGSTAmt[SGSTAmount],RSUB.IGSTPer[IGSTPer],RSUB.IGSTAmt[IGSTAmount],";
                SSQL = SSQL + " RSUB.LineTotal[LineTotal],RSUB.UOM,RSUB.ExcessQty From Trans_GoodsReceipt_Sub RSUB ";
                SSQL = SSQL + " Where RSUB.Ccode='" + SessionCcode + "' And RSUB.Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " RSUB.FinYearCode = '" + SessionFinYearCode + "' And RSUB.GRNo='" + txtGRNo.Text + "'";

                dt = objdata.RptEmployeeMultipleDetails(SSQL);


                ddlWareHouse.SelectedValue = dt.Rows[0]["WarehouseCode"].ToString();
                ddlWareHouse_SelectedIndexChanged(sender, e);
                ddlRackName.SelectedValue = dt.Rows[0]["Rack"].ToString();

                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                TotalReqQty();
                Grid_Total_Value();

                OtherCharge_Calc();

                SSQL = "Select SAPNo,ItemCode,ItemName,UOM,SerialNo from Trans_Stock_SerialNo Where Ccode='" + SessionCcode + "' And  ";
                SSQL = SSQL + " Lcode='" + SessionLcode + "' And   FinYearCode = '" + SessionFinYearCode + "' And GRNNo='" + txtGRNo.Text + "'";

                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    pnlSerialNoDet.Visible = true;
                    ViewState["SerialNoDet"] = dt;
                    rptSerialNoDet.DataSource = dt;
                    rptSerialNoDet.DataBind();
                }


                SSQL = "Select SAPNo,ItemCode,ItemName,UOM,BatchNo,Qty from Trans_Stock_BatchNo Where Ccode='" + SessionCcode + "' And  ";
                SSQL = SSQL + " Lcode='" + SessionLcode + "' And   FinYearCode = '" + SessionFinYearCode + "' And GRNNo='" + txtGRNo.Text + "'";

                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    pnlBatchNo.Visible = true;
                    ViewState["BatchNoDet"] = dt;
                    rptBatchNoDet.DataSource = dt;
                    rptBatchNoDet.DataBind();
                }

                btnSave.Text = "Update";

            }
            else
            {
                Clear_All_Field();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_GoodsReceived_Main.aspx");
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlPurOrdNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //string SSQL = "";
            //DataTable DT = new DataTable();
            //Boolean ErrFlag = false;
            //string TableType = "";
            //if (ddlWareHouse.SelectedItem.Text == "-Select-")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Warehouse Name');", true);
            //    //Load_PurOrder_GatePassIN();
            //    //Load_PurOrder_No();
            //    ErrFlag = true;
            //}


            //if (!ErrFlag)
            //{
            //    if (ddlSupplierType.SelectedItem.Text == "Enercon")
            //    {
            //        SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
            //        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And (PO_Status='2' or PO_Status='3' or PO_Status='4') and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
            //    }
            //    else if (ddlSupplierType.SelectedItem.Text == "Escrow")
            //    {
            //        // Check With Amendment
            //        SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
            //        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And PO_Status='1' and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
            //        DT = objdata.RptEmployeeMultipleDetails(SSQL);
            //        if (DT.Rows.Count == 0)
            //        {
            //            TableType = "EscrowPO";
            //            SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
            //            SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And (PO_Status='2' or PO_Status='3' or PO_Status='4') and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
            //        }
            //        else
            //        {
            //            TableType = "EscrowAmend";
            //            SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
            //            SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And PO_Status='1' and PurOrdNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
            //        }
            //    }
            //    else if (ddlSupplierType.SelectedItem.Text == "Coral")
            //    {
            //        SSQL = "Select * from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
            //        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And (PO_Status='2' or PO_Status='3' or PO_Status='4') and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
            //    }

            //    DT = objdata.RptEmployeeMultipleDetails(SSQL);

            //    if (TableType == "EscrowPO")
            //    {
            //        txtPurOrdDate.Text = DT.Rows[0]["Std_PO_Date"].ToString();
            //        txtSuppName.Text = DT.Rows[0]["Supp_Name"].ToString();
            //        hdSuppCode.Value = DT.Rows[0]["Supp_Code"].ToString();
            //        txtCurrency.Text = DT.Rows[0]["CurrencyType"].ToString();
            //        txtGSTType.Text = DT.Rows[0]["GstName"].ToString();
            //        txtPayTerms.Text = DT.Rows[0]["PaymentTerms"].ToString();
            //        txtPayMode.Text = DT.Rows[0]["PaymentMode"].ToString();

            //        //txtTotQty.Text = DT.Rows[0]["TotalQuantity"].ToString();

            //        //lblTotDisc.Text = DT.Rows[0]["Discount"].ToString();
            //        //lblTotCGST.Text = DT.Rows[0]["TotCGST"].ToString();
            //        //lblTotSGST.Text = DT.Rows[0]["TotSGST"].ToString();
            //        //lblTotIGST.Text = DT.Rows[0]["TotIGST"].ToString();

            //        //txtRoundOff.Text = DT.Rows[0]["AddOrLess"].ToString();
            //        //lblTotLineAmt.Text = DT.Rows[0]["TotalAmt"].ToString();

            //        //txtNetAmt.Text = DT.Rows[0]["NetAmount"].ToString();

            //        if (DT.Rows[0]["PackWGST"].ToString() == "1")
            //        {
            //            chkPFGst.Checked = true;
            //        }
            //        else
            //        {
            //            chkPFGst.Checked = false;
            //        }

            //        if (DT.Rows[0]["TransWGST"].ToString() == "1")
            //        {
            //            chkTCGst.Checked = true;
            //        }
            //        else
            //        {
            //            chkTCGst.Checked = false;
            //        }

            //        if (DT.Rows[0]["CourWGST"].ToString() == "1")
            //        {
            //            chkCCGst.Checked = true;
            //        }
            //        else
            //        {
            //            chkCCGst.Checked = false;
            //        }
            //    }
            //    else
            //    {
            //        txtPurOrdDate.Text = DT.Rows[0]["PurOrdDate"].ToString();
            //        txtSuppName.Text = DT.Rows[0]["Supp_Name"].ToString();
            //        hdSuppCode.Value = DT.Rows[0]["Supp_Code"].ToString();
            //        txtCurrency.Text = DT.Rows[0]["CurrencyType"].ToString();
            //        txtGSTType.Text = DT.Rows[0]["TaxType"].ToString();
            //        txtPayTerms.Text = DT.Rows[0]["PaymentTerms"].ToString();
            //        txtPayMode.Text = DT.Rows[0]["PaymentMode"].ToString();

            //        txtPackFrwdCharge.Text = "0.00";
            //        txtTransCharge.Text = "0.00";
            //        txtCourierCharge.Text = "0.00";
            //    }


            //    if (ddlSupplierType.SelectedItem.Text == "Enercon")
            //    {
            //        //SSQL = "Select POS.SAPNo,POS.ItemCode,POS.ItemName,BOM.DeptCode,BOM.DeptName,'" + ddlWareHouse.SelectedValue + "'[WarehouseCode],";
            //        //SSQL = SSQL + " '" + ddlWareHouse.SelectedItem.Text + "'[WarehouseName],'" + ddlRackName.SelectedItem.Text + "'[Rack],";
            //        //SSQL = SSQL + " POS.OrderQty,POS.OrderQty[ReceiveQty],POS.Rate,POS.ItemTotal,POS.CGSTPer,POS.CGSTAmount,";
            //        //SSQL = SSQL + " POS.SGSTPer,POS.SGSTAmount,POS.IGSTPer,POS.IGSTAmount,POS.LineTotal From Trans_Enercon_PurOrd_Sub POS";
            //        //SSQL = SSQL + " Inner join BOMMaster BOM on BOM.Raw_Mat_Name= POS.ItemName and BOM.Mat_No= POS.ItemCode and BOM.Ccode= POS.Ccode";
            //        //SSQL = SSQL + " where POS.Ccode='" + SessionCcode + "' and POS.Lcode='" + SessionLcode + "' And ";
            //        //SSQL = SSQL + " POS.FinYearcode='" + SessionFinYearCode + "' and POS.Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "'";

            //        SSQL = "Select POS.SAPNo,POS.ItemCode,POS.ItemName,POM.DeptCode,POM.DeptName,'" + ddlWareHouse.SelectedValue + "'[WarehouseCode],";
            //        SSQL = SSQL + " '" + ddlWareHouse.SelectedItem.Text + "'[WarehouseName],'" + ddlRackName.SelectedItem.Text + "'[Rack],";
            //        SSQL = SSQL + " POS.OrderQty,POS.OrderQty[ReceiveQty],POS.Rate,POS.ItemTotal,POS.Discount_Per[DiscPer],POS.Discount[DiscAmount],";
            //        SSQL = SSQL + " POS.CGSTPer,POS.CGSTAmount,POS.SGSTPer,POS.SGSTAmount,POS.IGSTPer,POS.IGSTAmount,POS.LineTotal,POM.TaxType, ";
            //        SSQL = SSQL + " POM.CurrencyType,'0'[BalQty] From Trans_Enercon_PurOrd_Sub POS";
            //        SSQL = SSQL + " Inner join Trans_Enercon_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and POM.Ccode= POS.Ccode ";
            //        SSQL = SSQL + " where POS.Ccode='" + SessionCcode + "' and POS.Lcode='" + SessionLcode + "' And ";
            //        SSQL = SSQL + " POS.FinYearcode='" + SessionFinYearCode + "' and POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' And ";
            //        SSQL = SSQL + " POS.Active!= 'Delete' and POM.Active!= 'Delete'";

            //    }
            //    else if (ddlSupplierType.SelectedItem.Text == "Escrow")
            //    {
            //        // Check With Amendment Sub
            //        SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Sub where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
            //        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
            //        DT = objdata.RptEmployeeMultipleDetails(SSQL);
            //        if (DT.Rows.Count == 0)
            //        {

            //            SSQL = " Select A.SAPNo,ItemCode,A.ItemName,A.UOM,A.DeptCode,A.DeptName,";
            //            SSQL = SSQL + "'" + ddlWareHouse.SelectedValue + "'WarehouseCode,'" + ddlWareHouse.SelectedItem.Text + "'WarehouseName,";
            //            SSQL = SSQL + "'" + ddlRackName.SelectedItem.Text + "' Rack,A.OrderQty,A.ReceiveQty,A.BalQty,A.ExcessQty,";
            //            SSQL = SSQL + " A.Rate,A.ItemTotal,A.DiscPer,A.DiscAmount,A.CGSTPer,A.CGSTAmount,A.SGSTPer,A.SGSTAmount,A.IGSTPer,";
            //            SSQL = SSQL + " A.IGSTAmount,A.LineTotal,A.TaxType,A.CurrencyType from ( ";

            //            SSQL = SSQL + " Select POS.SAPNo,POS.ItemCode,POS.ItemName,POS.UOMCode[UOM],POM.DeptCode,POM.DeptName,POS.OrderQty,";
            //            SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,POS.Rate,'0.00' ItemTotal,POS.Discount_Per[DiscPer],";
            //            SSQL = SSQL + " '0.00' [DiscAmount],POS.CGSTPer,'0.00' CGSTAmount,POS.SGSTPer,'0.00' SGSTAmount,POS.IGSTPer,";
            //            SSQL = SSQL + " '0.00' IGSTAmount,'0.00' LineTotal,POM.TaxType,POM.CurrencyType From Trans_Escrow_PurOrd_Sub POS ";
            //            SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
            //            SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
            //            SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
            //            SSQL = SSQL + " And POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' And ";
            //            SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.RefNo) ";
            //            //SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.Std_PO_No) ";

            //            SSQL = SSQL + " Union All ";

            //            SSQL = SSQL + " Select GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,POM.DeptCode,POM.DeptName,";
            //            SSQL = SSQL + " (POS.OrderQty-sum(GRS.ReceiveQty)) OrderQty,";
            //            //SSQL = SSQL + " (POS.OrderQty-sum(GRS.ReceiveQty)) ReceiveQty,0 BalQty,0 ExcessQty,";
            //            SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,";
            //            SSQL = SSQL + " GRS.Rate,'0.00' ItemTotal,POS.Discount_Per[DiscPer],'0.00' [DiscAmount],GRS.CGSTPer,'0.00' CGSTAmt,";
            //            SSQL = SSQL + " GRS.SGSTPer,'0.00' SGSTAmt,GRS.IGSTPer,'0.00' IGSTAmt,'0.00' LineTotal,POM.TaxType,POM.CurrencyType ";
            //            SSQL = SSQL + " From Trans_Escrow_PurOrd_Sub POS ";
            //            SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
            //            SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
            //            SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRS.ItemCode=POS.ItemCode and GRS.Ccode= POS.Ccode ";
            //            SSQL = SSQL + " and  GRS.Lcode= POS.Lcode and  GRS.FinYearCode= POS.FinYearCode and GRS.PurOrdNo=POS.RefNo ";
            //            SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
            //            SSQL = SSQL + " and POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' ";
            //            SSQL = SSQL + " Group By GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,GRS.Rate,POS.ItemTotal,POS.Discount_Per,POS.Discount,";
            //            SSQL = SSQL + " GRS.CGSTPer,GRS.CGSTAmt,GRS.SGSTPer,GRS.SGSTAmt,GRS.IGSTPer,GRS.IGSTAmt,GRS.LineTotal,";
            //            SSQL = SSQL + " POM.TaxType,POM.CurrencyType,POM.DeptCode,POM.DeptName,POS.OrderQty ";
            //            SSQL = SSQL + " having (sum(POS.OrderQty)-sum(GRS.ReceiveQty)) >0)A";
            //        }
            //        else
            //        {

            //            SSQL = " Select A.SAPNo,ItemCode,A.ItemName,A.UOM,A.DeptCode,A.DeptName,";
            //            SSQL = SSQL + "'" + ddlWareHouse.SelectedValue + "'WarehouseCode,'" + ddlWareHouse.SelectedItem.Text + "'WarehouseName,";
            //            SSQL = SSQL + "'" + ddlRackName.SelectedItem.Text + "' Rack,A.OrderQty,A.ReceiveQty,A.BalQty,A.ExcessQty,";
            //            SSQL = SSQL + " A.Rate,A.ItemTotal,A.DiscPer,A.DiscAmount,A.CGSTPer,A.CGSTAmount,A.SGSTPer,A.SGSTAmount,A.IGSTPer,";
            //            SSQL = SSQL + " A.IGSTAmount,A.LineTotal,A.TaxType,A.CurrencyType from ( ";

            //            SSQL = SSQL + " Select POS.SAPNo,POS.ItemCode,POS.ItemName,POS.UOMCode[UOM],POM.DeptCode,POM.DeptName,POS.AFRquQty[OrderQty],";
            //            SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,POS.AFRate as Rate,'0.00' ItemTotal,POS.Discount_Per[DiscPer],";
            //            SSQL = SSQL + " '0.00' [DiscAmount],POS.CGSTPer,'0.00' CGSTAmount,POS.SGSTPer,'0.00' SGSTAmount,POS.IGSTPer,";
            //            SSQL = SSQL + " '0.00' IGSTAmount,'0.00' LineTotal,POM.TaxType,POM.CurrencyType From Trans_Escrow_PurOrd_Amend_sub POS ";
            //            SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Amend_Main POM on POM.PurOrdNo= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
            //            SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
            //            SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
            //            SSQL = SSQL + " And POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' And ";
            //            SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.RefNo) ";
            //            //SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.Std_PO_No) ";

            //            SSQL = SSQL + " Union All ";

            //            SSQL = SSQL + " Select GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,POM.DeptCode,POM.DeptName,";
            //            SSQL = SSQL + " (POS.AFRquQty-sum(GRS.ReceiveQty)) OrderQty,";
            //            //SSQL = SSQL + " (POS.OrderQty-sum(GRS.ReceiveQty)) ReceiveQty,0 BalQty,0 ExcessQty,";
            //            SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,";
            //            SSQL = SSQL + " GRS.Rate,'0.00' ItemTotal,POS.Discount_Per[DiscPer],'0.00' [DiscAmount],GRS.CGSTPer,'0.00' CGSTAmt,";
            //            SSQL = SSQL + " GRS.SGSTPer,'0.00' SGSTAmt,GRS.IGSTPer,'0.00' IGSTAmt,'0.00' LineTotal,POM.TaxType,POM.CurrencyType ";
            //            SSQL = SSQL + " From Trans_Escrow_PurOrd_Amend_Sub POS ";
            //            SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Amend_Main POM on POM.PurOrdNo= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
            //            SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
            //            SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRS.ItemCode=POS.ItemCode and GRS.Ccode= POS.Ccode ";
            //            SSQL = SSQL + " and  GRS.Lcode= POS.Lcode and  GRS.FinYearCode= POS.FinYearCode and GRS.PurOrdNo=POS.RefNo ";
            //            SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
            //            SSQL = SSQL + " and POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' ";
            //            SSQL = SSQL + " Group By GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,GRS.Rate,POS.ItemTotal,POS.Discount_Per,POS.Discount,";
            //            SSQL = SSQL + " GRS.CGSTPer,GRS.CGSTAmt,GRS.SGSTPer,GRS.SGSTAmt,GRS.IGSTPer,GRS.IGSTAmt,GRS.LineTotal,";
            //            SSQL = SSQL + " POM.TaxType,POM.CurrencyType,POM.DeptCode,POM.DeptName,POS.AFRquQty ";
            //            SSQL = SSQL + " having (sum(POS.AFRquQty)-sum(GRS.ReceiveQty)) >0)A";
            //        }

            //    }
            //    else
            //    {
            //        SSQL = " Select A.SAPNo,ItemCode,A.ItemName,A.DeptCode,A.DeptName,A.UOM,";
            //        SSQL = SSQL + "'" + ddlWareHouse.SelectedValue + "'WarehouseCode,'" + ddlWareHouse.SelectedItem.Text + "'WarehouseName,";
            //        SSQL = SSQL + "'" + ddlRackName.SelectedItem.Text + "' Rack,A.OrderQty,A.ReceiveQty,A.BalQty,A.ExcessQty,";
            //        SSQL = SSQL + " A.Rate,A.ItemTotal,A.DiscPer,A.DiscAmount,A.CGSTPer,A.CGSTAmount,A.SGSTPer,A.SGSTAmount,A.IGSTPer,";
            //        SSQL = SSQL + " A.IGSTAmount,A.LineTotal,A.TaxType,A.CurrencyType from ( Select POS.SAPNo, ";
            //        SSQL = SSQL + " POS.ItemCode,POS.ItemName,POS.UOMCode[UOM],POM.DeptCode,POM.DeptName,POS.OrderQty,";
            //        SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,";
            //        //SSQL = SSQL + " POS.OrderQty ReceiveQty,0 BalQty,";
            //        SSQL = SSQL + " '0.00' ExcessQty,POS.Rate,'0.00' ItemTotal,POS.Discount_Per [DiscPer],'0.00' [DiscAmount],";
            //        SSQL = SSQL + " POS.CGSTPer,'0.00' CGSTAmount,POS.SGSTPer,'0.00' SGSTAmount,POS.IGSTPer,";
            //        SSQL = SSQL + " '0.00' IGSTAmount,'0.00' LineTotal,POM.TaxType,POM.CurrencyType from Trans_Coral_PurOrd_Sub POS ";
            //        SSQL = SSQL + " Inner join Trans_Coral_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
            //        SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
            //        SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
            //        SSQL = SSQL + " And POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' And ";
            //        SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.RefNo) ";

            //        SSQL = SSQL + " Union All ";

            //        SSQL = SSQL + " Select GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,POM.DeptCode,POM.DeptName,";
            //        SSQL = SSQL + " (POS.OrderQty-sum(GRS.ReceiveQty)) OrderQty,";
            //        SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,";
            //        //SSQL = SSQL + "(POS.OrderQty-sum(GRS.ReceiveQty)) ReceiveQty,0 BalQty,0 ExcessQty,";
            //        SSQL = SSQL + " GRS.Rate,'0.00' ItemTotal,POS.Discount_Per [DiscPer],'0.00' [DiscAmount],GRS.CGSTPer,'0.00' CGSTAmt,";
            //        SSQL = SSQL + " GRS.SGSTPer,'0.00' SGSTAmt,GRS.IGSTPer,'0.00' IGSTAmt,'0.00' LineTotal,POM.TaxType,POM.CurrencyType ";
            //        SSQL = SSQL + " From Trans_Coral_PurOrd_Sub POS ";
            //        SSQL = SSQL + " Inner join Trans_Coral_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
            //        SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
            //        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRS.ItemCode=POS.ItemCode and GRS.Ccode= POS.Ccode ";
            //        SSQL = SSQL + " and  GRS.Lcode= POS.Lcode and  GRS.FinYearCode= POS.FinYearCode and GRS.PurOrdNo=POS.RefNo ";
            //        SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
            //        SSQL = SSQL + " and POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' ";
            //        SSQL = SSQL + " Group By GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,GRS.Rate,POS.ItemTotal,POS.Discount_Per,POS.Discount,";
            //        SSQL = SSQL + " GRS.CGSTPer,GRS.CGSTAmt,GRS.SGSTPer,GRS.SGSTAmt,GRS.IGSTPer,GRS.IGSTAmt,GRS.LineTotal,";
            //        SSQL = SSQL + " POM.TaxType,POM.CurrencyType,POM.DeptCode,POM.DeptName,POS.OrderQty ";
            //        SSQL = SSQL + " having (sum(POS.OrderQty)-sum(GRS.ReceiveQty)) >0)A";
            //    }

            //    DT = objdata.RptEmployeeMultipleDetails(SSQL);

            //    if (DT.Rows.Count > 0)
            //    {
            //        ViewState["ItemTable"] = DT;
            //        Repeater1.DataSource = DT;
            //        Repeater1.DataBind();

            //        //Text_Change_Value_Cal_Test();
            //        Grid_Total_Value();
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('All items Are Received in this Purchase Order');", true);
            //    }
            //}
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            Boolean ErrFlag = false;
            string TableType = "";
            if (ddlWareHouse.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Warehouse Name');", true);
                //Load_PurOrder_GatePassIN();
                //Load_PurOrder_No();
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                if (ddlSupplierType.SelectedItem.Text == "Enercon")
                {
                    SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And (PO_Status='2' or PO_Status='3' or PO_Status='4') and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
                }
                else if (ddlSupplierType.SelectedItem.Text == "Escrow")
                {
                    // Check With Amendment
                    SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And PO_Status='1' and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
                    DT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT.Rows.Count == 0)
                    {
                        TableType = "EscrowPO";
                        SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And (PO_Status='2' or PO_Status='3' or PO_Status='4') and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
                    }
                    else
                    {
                        TableType = "EscrowAmend";
                        SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And PO_Status='1' and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
                    }
                }
                else if (ddlSupplierType.SelectedItem.Text == "Coral")
                {
                    TableType = "CoralPO";
                    SSQL = "Select * from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And (PO_Status='2' or PO_Status='3' or PO_Status='4') and RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
                }

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (TableType == "EscrowPO" || TableType == "CoralPO")
                {
                    txtPurOrdDate.Text = DT.Rows[0]["Std_PO_Date"].ToString();
                    txtSuppName.Text = DT.Rows[0]["Supp_Name"].ToString();
                    hdSuppCode.Value = DT.Rows[0]["Supp_Code"].ToString();
                    txtCurrency.Text = DT.Rows[0]["CurrencyType"].ToString();
                    txtGSTType.Text = DT.Rows[0]["GstName"].ToString();
                    txtPayTerms.Text = DT.Rows[0]["PaymentTerms"].ToString();
                    txtPayMode.Text = DT.Rows[0]["PaymentMode"].ToString();

                    //txtTotQty.Text = DT.Rows[0]["TotalQuantity"].ToString();

                    //lblTotDisc.Text = DT.Rows[0]["Discount"].ToString();
                    //lblTotCGST.Text = DT.Rows[0]["TotCGST"].ToString();
                    //lblTotSGST.Text = DT.Rows[0]["TotSGST"].ToString();
                    //lblTotIGST.Text = DT.Rows[0]["TotIGST"].ToString();

                    //txtRoundOff.Text = DT.Rows[0]["AddOrLess"].ToString();
                    //lblTotLineAmt.Text = DT.Rows[0]["TotalAmt"].ToString();

                    //txtNetAmt.Text = DT.Rows[0]["NetAmount"].ToString();

                    if (DT.Rows[0]["PackWGST"].ToString() == "1")
                    {
                        chkPFGst.Checked = true;
                    }
                    else
                    {
                        chkPFGst.Checked = false;
                    }

                    if (DT.Rows[0]["TransWGST"].ToString() == "1")
                    {
                        chkTCGst.Checked = true;
                    }
                    else
                    {
                        chkTCGst.Checked = false;
                    }

                    if (DT.Rows[0]["CourWGST"].ToString() == "1")
                    {
                        chkCCGst.Checked = true;
                    }
                    else
                    {
                        chkCCGst.Checked = false;
                    }
                }
                else
                {
                    txtPurOrdDate.Text = DT.Rows[0]["PurOrdDate"].ToString();
                    txtSuppName.Text = DT.Rows[0]["Supp_Name"].ToString();
                    hdSuppCode.Value = DT.Rows[0]["Supp_Code"].ToString();
                    txtCurrency.Text = DT.Rows[0]["CurrencyType"].ToString();
                    txtGSTType.Text = DT.Rows[0]["TaxType"].ToString();
                    txtPayTerms.Text = DT.Rows[0]["PaymentTerms"].ToString();
                    txtPayMode.Text = DT.Rows[0]["PaymentMode"].ToString();

                    txtPackFrwdCharge.Text = "0.00";
                    txtTransCharge.Text = "0.00";
                    txtCourierCharge.Text = "0.00";
                }


                if (ddlSupplierType.SelectedItem.Text == "Enercon")
                {
                    //SSQL = "Select POS.SAPNo,POS.ItemCode,POS.ItemName,BOM.DeptCode,BOM.DeptName,'" + ddlWareHouse.SelectedValue + "'[WarehouseCode],";
                    //SSQL = SSQL + " '" + ddlWareHouse.SelectedItem.Text + "'[WarehouseName],'" + ddlRackName.SelectedItem.Text + "'[Rack],";
                    //SSQL = SSQL + " POS.OrderQty,POS.OrderQty[ReceiveQty],POS.Rate,POS.ItemTotal,POS.CGSTPer,POS.CGSTAmount,";
                    //SSQL = SSQL + " POS.SGSTPer,POS.SGSTAmount,POS.IGSTPer,POS.IGSTAmount,POS.LineTotal From Trans_Enercon_PurOrd_Sub POS";
                    //SSQL = SSQL + " Inner join BOMMaster BOM on BOM.Raw_Mat_Name= POS.ItemName and BOM.Mat_No= POS.ItemCode and BOM.Ccode= POS.Ccode";
                    //SSQL = SSQL + " where POS.Ccode='" + SessionCcode + "' and POS.Lcode='" + SessionLcode + "' And ";
                    //SSQL = SSQL + " POS.FinYearcode='" + SessionFinYearCode + "' and POS.Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "'";

                    SSQL = "Select POS.SAPNo,POS.ItemCode,POS.ItemName,POM.DeptCode,POM.DeptName,'" + ddlWareHouse.SelectedValue + "'[WarehouseCode],";
                    SSQL = SSQL + " '" + ddlWareHouse.SelectedItem.Text + "'[WarehouseName],'" + ddlRackName.SelectedItem.Text + "'[Rack],";
                    SSQL = SSQL + " POS.OrderQty,POS.OrderQty[ReceiveQty],POS.Rate,POS.ItemTotal,POS.Discount_Per[DiscPer],POS.Discount[DiscAmount],";
                    SSQL = SSQL + " POS.CGSTPer,POS.CGSTAmount,POS.SGSTPer,POS.SGSTAmount,POS.IGSTPer,POS.IGSTAmount,POS.LineTotal,POM.TaxType, ";
                    SSQL = SSQL + " POM.CurrencyType,'0'[BalQty] From Trans_Enercon_PurOrd_Sub POS";
                    SSQL = SSQL + " Inner join Trans_Enercon_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and POM.Ccode= POS.Ccode ";
                    SSQL = SSQL + " where POS.Ccode='" + SessionCcode + "' and POS.Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " POS.FinYearcode='" + SessionFinYearCode + "' and POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' And ";
                    SSQL = SSQL + " POS.Active!= 'Delete' and POM.Active!= 'Delete'";

                }
                else if (ddlSupplierType.SelectedItem.Text == "Escrow")
                {
                    // Check With Amendment Sub
                    SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Sub where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And RefNo='" + ddlPurOrdNo.SelectedItem.Text + "'";
                    DT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT.Rows.Count == 0)
                    {

                        SSQL = " Select A.SAPNo,ItemCode,A.ItemName,A.UOM,A.DeptCode,A.DeptName,";
                        SSQL = SSQL + "'" + ddlWareHouse.SelectedValue + "'WarehouseCode,'" + ddlWareHouse.SelectedItem.Text + "'WarehouseName,";
                        SSQL = SSQL + "'" + ddlRackName.SelectedItem.Text + "' Rack,A.OrderQty,A.ReceiveQty,A.BalQty,A.ExcessQty,";
                        SSQL = SSQL + " A.Rate,A.ItemTotal,A.DiscPer,A.DiscAmount,A.CGSTPer,A.CGSTAmount,A.SGSTPer,A.SGSTAmount,A.IGSTPer,";
                        SSQL = SSQL + " A.IGSTAmount,A.LineTotal,A.TaxType,A.CurrencyType from ( ";

                        SSQL = SSQL + " Select POS.SAPNo,POS.ItemCode,POS.ItemName,POS.UOMCode[UOM],POM.DeptCode,POM.DeptName,POS.OrderQty,";
                        SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,POS.Rate,'0.00' ItemTotal,POS.Discount_Per[DiscPer],";
                        SSQL = SSQL + " '0.00' [DiscAmount],POS.CGSTPer,'0.00' CGSTAmount,POS.SGSTPer,'0.00' SGSTAmount,POS.IGSTPer,";
                        SSQL = SSQL + " '0.00' IGSTAmount,'0.00' LineTotal,POM.TaxType,POM.CurrencyType From Trans_Escrow_PurOrd_Sub POS ";
                        SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
                        SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
                        SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
                        SSQL = SSQL + " And POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' And ";
                        SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.RefNo) ";
                        //SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.Std_PO_No) ";

                        SSQL = SSQL + " Union All ";

                        SSQL = SSQL + " Select GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,POM.DeptCode,POM.DeptName,";
                        SSQL = SSQL + " (POS.OrderQty-sum(GRS.ReceiveQty)) OrderQty,";
                        //SSQL = SSQL + " (POS.OrderQty-sum(GRS.ReceiveQty)) ReceiveQty,0 BalQty,0 ExcessQty,";
                        SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,";
                        SSQL = SSQL + " GRS.Rate,'0.00' ItemTotal,POS.Discount_Per[DiscPer],'0.00' [DiscAmount],GRS.CGSTPer,'0.00' CGSTAmt,";
                        SSQL = SSQL + " GRS.SGSTPer,'0.00' SGSTAmt,GRS.IGSTPer,'0.00' IGSTAmt,'0.00' LineTotal,POM.TaxType,POM.CurrencyType ";
                        SSQL = SSQL + " From Trans_Escrow_PurOrd_Sub POS ";
                        SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
                        SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
                        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRS.ItemCode=POS.ItemCode and GRS.Ccode= POS.Ccode ";
                        SSQL = SSQL + " and  GRS.Lcode= POS.Lcode and  GRS.FinYearCode= POS.FinYearCode and GRS.PurOrdNo=POS.RefNo ";
                        SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
                        SSQL = SSQL + " and POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' ";
                        SSQL = SSQL + " Group By GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,GRS.Rate,POS.ItemTotal,POS.Discount_Per,POS.Discount,";
                        SSQL = SSQL + " GRS.CGSTPer,GRS.CGSTAmt,GRS.SGSTPer,GRS.SGSTAmt,GRS.IGSTPer,GRS.IGSTAmt,GRS.LineTotal,";
                        SSQL = SSQL + " POM.TaxType,POM.CurrencyType,POM.DeptCode,POM.DeptName,POS.OrderQty ";
                        SSQL = SSQL + " having (sum(POS.OrderQty)-sum(GRS.ReceiveQty)) >0)A";
                    }
                    else
                    {

                        SSQL = " Select A.SAPNo,ItemCode,A.ItemName,A.UOM,A.DeptCode,A.DeptName,";
                        SSQL = SSQL + "'" + ddlWareHouse.SelectedValue + "'WarehouseCode,'" + ddlWareHouse.SelectedItem.Text + "'WarehouseName,";
                        SSQL = SSQL + "'" + ddlRackName.SelectedItem.Text + "' Rack,A.OrderQty,A.ReceiveQty,A.BalQty,A.ExcessQty,";
                        SSQL = SSQL + " A.Rate,A.ItemTotal,A.DiscPer,A.DiscAmount,A.CGSTPer,A.CGSTAmount,A.SGSTPer,A.SGSTAmount,A.IGSTPer,";
                        SSQL = SSQL + " A.IGSTAmount,A.LineTotal,A.TaxType,A.CurrencyType from ( ";

                        SSQL = SSQL + " Select POS.SAPNo,POS.ItemCode,POS.ItemName,POS.UOMCode[UOM],POM.DeptCode,POM.DeptName,POS.AFRquQty[OrderQty],";
                        SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,POS.AFRate as Rate,'0.00' ItemTotal,POS.Discount_Per[DiscPer],";
                        SSQL = SSQL + " '0.00' [DiscAmount],POS.CGSTPer,'0.00' CGSTAmount,POS.SGSTPer,'0.00' SGSTAmount,POS.IGSTPer,";
                        SSQL = SSQL + " '0.00' IGSTAmount,'0.00' LineTotal,POM.TaxType,POM.CurrencyType From Trans_Escrow_PurOrd_Amend_sub POS ";
                        SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Amend_Main POM on POM.PurOrdNo= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
                        SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
                        SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
                        SSQL = SSQL + " And POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' And ";
                        SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where (PurOrdNo=POS.RefNo or PurOrdNo=POS.Std_PO_No)) ";
                        //SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.Std_PO_No) ";

                        SSQL = SSQL + " Union All ";

                        SSQL = SSQL + " Select GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,POM.DeptCode,POM.DeptName,";
                        SSQL = SSQL + " (POS.AFRquQty-sum(GRS.ReceiveQty)) OrderQty,";
                        //SSQL = SSQL + " (POS.OrderQty-sum(GRS.ReceiveQty)) ReceiveQty,0 BalQty,0 ExcessQty,";
                        SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,";
                        SSQL = SSQL + " GRS.Rate,'0.00' ItemTotal,POS.Discount_Per[DiscPer],'0.00' [DiscAmount],GRS.CGSTPer,'0.00' CGSTAmt,";
                        SSQL = SSQL + " GRS.SGSTPer,'0.00' SGSTAmt,GRS.IGSTPer,'0.00' IGSTAmt,'0.00' LineTotal,POM.TaxType,POM.CurrencyType ";
                        SSQL = SSQL + " From Trans_Escrow_PurOrd_Amend_Sub POS ";
                        SSQL = SSQL + " Inner join Trans_Escrow_PurOrd_Amend_Main POM on POM.PurOrdNo= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
                        SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
                        SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRS.ItemCode=POS.ItemCode and GRS.Ccode= POS.Ccode ";
                        SSQL = SSQL + " and  GRS.Lcode= POS.Lcode and  GRS.FinYearCode= POS.FinYearCode and GRS.PurOrdNo=POS.RefNo ";
                        SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
                        SSQL = SSQL + " and POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' ";
                        SSQL = SSQL + " Group By GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,GRS.Rate,POS.ItemTotal,POS.Discount_Per,POS.Discount,";
                        SSQL = SSQL + " GRS.CGSTPer,GRS.CGSTAmt,GRS.SGSTPer,GRS.SGSTAmt,GRS.IGSTPer,GRS.IGSTAmt,GRS.LineTotal,";
                        SSQL = SSQL + " POM.TaxType,POM.CurrencyType,POM.DeptCode,POM.DeptName,POS.AFRquQty ";
                        SSQL = SSQL + " having (sum(POS.AFRquQty)-sum(GRS.ReceiveQty)) >0)A";
                    }

                }
                else
                {
                    SSQL = " Select A.SAPNo,ItemCode,A.ItemName,A.DeptCode,A.DeptName,A.UOM,";
                    SSQL = SSQL + "'" + ddlWareHouse.SelectedValue + "'WarehouseCode,'" + ddlWareHouse.SelectedItem.Text + "'WarehouseName,";
                    SSQL = SSQL + "'" + ddlRackName.SelectedItem.Text + "' Rack,A.OrderQty,A.ReceiveQty,A.BalQty,A.ExcessQty,";
                    SSQL = SSQL + " A.Rate,A.ItemTotal,A.DiscPer,A.DiscAmount,A.CGSTPer,A.CGSTAmount,A.SGSTPer,A.SGSTAmount,A.IGSTPer,";
                    SSQL = SSQL + " A.IGSTAmount,A.LineTotal,A.TaxType,A.CurrencyType from ( Select POS.SAPNo, ";
                    SSQL = SSQL + " POS.ItemCode,POS.ItemName,POS.UOMCode[UOM],POM.DeptCode,POM.DeptName,POS.OrderQty,";
                    SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,";
                    //SSQL = SSQL + " POS.OrderQty ReceiveQty,0 BalQty,";
                    SSQL = SSQL + " '0.00' ExcessQty,POS.Rate,'0.00' ItemTotal,POS.Discount_Per [DiscPer],'0.00' [DiscAmount],";
                    SSQL = SSQL + " POS.CGSTPer,'0.00' CGSTAmount,POS.SGSTPer,'0.00' SGSTAmount,POS.IGSTPer,";
                    SSQL = SSQL + " '0.00' IGSTAmount,'0.00' LineTotal,POM.TaxType,POM.CurrencyType from Trans_Coral_PurOrd_Sub POS ";
                    SSQL = SSQL + " Inner join Trans_Coral_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
                    SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
                    SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " And POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' And ";
                    SSQL = SSQL + " POS.ItemCode not in (Select ItemCode from Trans_GoodsReceipt_Sub where PurOrdNo=POS.RefNo) ";

                    SSQL = SSQL + " Union All ";

                    SSQL = SSQL + " Select GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,POM.DeptCode,POM.DeptName,";
                    SSQL = SSQL + " (POS.OrderQty-sum(GRS.ReceiveQty)) OrderQty,";
                    SSQL = SSQL + " '0.00' ReceiveQty,'0.00' BalQty,'0.00' ExcessQty,";
                    //SSQL = SSQL + "(POS.OrderQty-sum(GRS.ReceiveQty)) ReceiveQty,0 BalQty,0 ExcessQty,";
                    SSQL = SSQL + " GRS.Rate,'0.00' ItemTotal,POS.Discount_Per [DiscPer],'0.00' [DiscAmount],GRS.CGSTPer,'0.00' CGSTAmt,";
                    SSQL = SSQL + " GRS.SGSTPer,'0.00' SGSTAmt,GRS.IGSTPer,'0.00' IGSTAmt,'0.00' LineTotal,POM.TaxType,POM.CurrencyType ";
                    SSQL = SSQL + " From Trans_Coral_PurOrd_Sub POS ";
                    SSQL = SSQL + " Inner join Trans_Coral_PurOrd_Main POM on POM.Std_PO_No= POS.Std_PO_No and  POM.Ccode= POS.Ccode ";
                    SSQL = SSQL + " and  POM.Lcode= POs.Lcode and  POM.FinYearCode= POS.FinYearCode ";
                    SSQL = SSQL + " Inner Join Trans_GoodsReceipt_Sub GRS on GRS.ItemCode=POS.ItemCode and GRS.Ccode= POS.Ccode ";
                    SSQL = SSQL + " and  GRS.Lcode= POS.Lcode and  GRS.FinYearCode= POS.FinYearCode and GRS.PurOrdNo=POS.RefNo ";
                    SSQL = SSQL + " Where POS.RefNo='" + ddlPurOrdNo.SelectedItem.Text + "' and POM.Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " and POM.Lcode='" + SessionLcode + "' and POM.FinYearCode='" + SessionFinYearCode + "' ";
                    SSQL = SSQL + " Group By GRS.SAPNo,GRS.ItemCode,GRS.ItemName,GRS.UOM,GRS.Rate,POS.ItemTotal,POS.Discount_Per,POS.Discount,";
                    SSQL = SSQL + " GRS.CGSTPer,GRS.CGSTAmt,GRS.SGSTPer,GRS.SGSTAmt,GRS.IGSTPer,GRS.IGSTAmt,GRS.LineTotal,";
                    SSQL = SSQL + " POM.TaxType,POM.CurrencyType,POM.DeptCode,POM.DeptName,POS.OrderQty ";
                    SSQL = SSQL + " having (sum(POS.OrderQty)-sum(GRS.ReceiveQty)) >0)A";
                }

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    ViewState["ItemTable"] = DT;
                    Repeater1.DataSource = DT;
                    Repeater1.DataBind();

                    //Text_Change_Value_Cal_Test();
                    Grid_Total_Value();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('All items Are Received in this Purchase Order');", true);
                }
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('All items Are Received in this Purchase Order');", true);
        }
    }

    protected void txtReceive_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            Grid_Total_Value();

            ((TextBox)Repeater1.Items[index].FindControl("txtGrdDiscp")).Focus();

        }
        catch (Exception ex)
        {

        }
    }

    protected void txtGrdDiscp_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            Grid_Total_Value();
            OtherCharge_Calc();

            if (Convert.ToDecimal(Repeater1.Items.Count) == index + 1)
            {
                ((TextBox)Repeater1.Items[index].FindControl("txtGrdDiscp")).Focus();
            }
            else
            {
                ((TextBox)Repeater1.Items[index + 1].FindControl("txtReceivQty")).Focus();
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void Text_Change_Value_Cal_Test()
    {
        try
        {
            bool ErrFlag = false;

            string Qty_Val = "0";
            string Item_Rate = "0";
            string Item_Total = "0";
            string Discount_Percent = "0";
            string Discount_Amt = "0";
            string VAT_Per = "0";
            string VAT_Amt = "0";
            string Tax_Amt = "0";
            string CGST_Per = "0";
            string CGST_Amt = "0";
            string SGST_Per = "0";
            string SGST_Amt = "0";
            string IGST_Per = "0";
            string IGST_Amt = "0";
            string BDUTax_Per = "0";
            string BDUTax_Amt = "0";
            string Other_Charges = "0";
            string Final_Amount = "0";
            string PackingAmt = "0";

            string OrdQty = "0";
            string BalQty = "0";

            //string Item_Discount_Amt = "0.00";


            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; dt.Rows.Count > 0; i++)
            {

                TextBox txtGrdOrdQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;

                if (txtGrdOrdQty.Text.ToString() != "" || txtGrdOrdQty.Text.ToString() != "0" || txtGrdOrdQty.Text.ToString() != "0.0" || txtGrdOrdQty.Text.ToString() != "0.00")
                {
                    //TextBox txtTest = ((TextBox)(sender));
                    //RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

                    Label lblSAPNo = Repeater1.Items[i].FindControl("lblSAPNo") as Label;
                    Label lblItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label lblItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;

                    Label lblGrdDeptCode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label lblGrdDeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;
                    Label lblGrdWHCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label lblGrdWHName = Repeater1.Items[i].FindControl("lblWarehouse") as Label;
                    Label lblRackName = Repeater1.Items[i].FindControl("lblRackName") as Label;

                    Label lblBOMQty = Repeater1.Items[i].FindControl("lblOrdQty") as Label;
                    TextBox txtGrdReceQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;
                    Label lblBalQty = Repeater1.Items[i].FindControl("lblBalQty") as Label;
                    Label lblRate = Repeater1.Items[i].FindControl("lblRate") as Label;
                    Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                    TextBox txtDiscPer = Repeater1.Items[i].FindControl("txtGrdDiscp") as TextBox;
                    Label lblDisc = Repeater1.Items[i].FindControl("lblDiscAmt") as Label;
                    Label lblCGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                    Label lblCGSTAmt = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                    Label lblSGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                    Label lblSGSTAmt = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                    Label lblIGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                    Label lblIGSTAmt = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                    Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                    //txtGrdRateEUR.ReadOnly = true;

                    if (txtGrdReceQty.Text != "") { Qty_Val = txtGrdReceQty.Text.ToString(); }
                    if (lblBOMQty.Text != "") { OrdQty = lblBOMQty.Text.ToString(); }


                    if (lblRate.Text != "") { Item_Rate = lblRate.Text.ToString(); }

                    Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();


                    Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();

                    //if(txtTransCharge.Text!="") { Trans = txtGrdReceQty.Text.ToString(); }

                    //if (txtDiscount.Text != "") { Discount_Percent = txtDiscount.Text.ToString(); }

                    BalQty = (Convert.ToDecimal(OrdQty) - Convert.ToDecimal(Qty_Val)).ToString();


                    if (Convert.ToDecimal(BalQty) < 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('More Then Order Qty');", true);
                    }
                    if (!ErrFlag)
                    {
                        if (Convert.ToDecimal(Item_Total) > 0)
                        {

                            if (Convert.ToDecimal(Item_Total) != 0)
                            {
                                if (Convert.ToDecimal(txtDiscPer.Text.ToString()) != 0) { Discount_Percent = txtDiscPer.Text.ToString(); }
                                //if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                                if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                                if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                                if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                                //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                                //Discount Amt Calculate
                                if (Convert.ToDecimal(Discount_Percent) != 0)
                                {
                                    Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                                    Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                                    Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                                }
                                else
                                {
                                    Discount_Amt = "0.00";
                                }

                                if (Convert.ToDecimal(CGST_Per) != 0)
                                {
                                    string Item_Discount_Amt = "0.00";
                                    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                    CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                                    CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                                    CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                                }
                                else
                                {
                                    CGST_Amt = "0.00";
                                }

                                if (Convert.ToDecimal(VAT_Per) != 0)
                                {
                                    string Item_Discount_Amt = "0.00";
                                    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                    VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                                    VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                                    VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                                }
                                else
                                {
                                    VAT_Amt = "0.00";
                                }

                                if (Convert.ToDecimal(SGST_Per) != 0)
                                {
                                    string Item_Discount_Amt = "0.00";
                                    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                    SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                                    SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                                    SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                                }
                                else
                                {
                                    SGST_Amt = "0.00";
                                }

                                if (Convert.ToDecimal(IGST_Per) != 0)
                                {
                                    string Item_Discount_Amt = "0.00";
                                    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                    IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                                    IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                                    IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                                }
                                else
                                {
                                    IGST_Amt = "0.00";
                                }

                                //Other Charges
                                //if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                                //Final Amt
                                Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                                Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                                Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                                Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

                                lblItemTot.Text = Item_Total;

                                lblDisc.Text = Discount_Amt;
                                //lblTaxAmt.Text = Tax_Amt;
                                lblCGSTAmt.Text = CGST_Amt;
                                lblSGSTAmt.Text = SGST_Amt;
                                lblIGSTAmt.Text = IGST_Amt;
                                //txtVAT_AMT.Text = VAT_Amt;
                                lblLineTot.Text = Final_Amount;
                                txtTotAmt.Text = Final_Amount;
                                txtNetAmt.Text = Final_Amount;

                                lblBalQty.Text = BalQty;
                            }

                            dt.Rows[i]["SAPNo"] = lblSAPNo.Text;
                            dt.Rows[i]["ItemCode"] = lblItemCode.Text;
                            dt.Rows[i]["ItemName"] = lblItemName.Text;
                            dt.Rows[i]["DeptCode"] = lblGrdDeptCode.Text;
                            dt.Rows[i]["DeptName"] = lblGrdDeptName.Text;
                            dt.Rows[i]["WarehouseCode"] = lblGrdWHCode.Text;
                            dt.Rows[i]["WarehouseName"] = lblGrdWHName.Text;
                            dt.Rows[i]["Rack"] = lblRackName.Text;


                            dt.Rows[i]["OrderQty"] = lblBOMQty.Text;
                            dt.Rows[i]["ReceiveQty"] = txtGrdReceQty.Text;
                            dt.Rows[i]["BalQty"] = lblBalQty.Text;
                            dt.Rows[i]["Rate"] = Math.Round(Convert.ToDecimal(lblRate.Text), 2, MidpointRounding.AwayFromZero);
                            dt.Rows[i]["ItemTotal"] = Math.Round(Convert.ToDecimal(lblItemTot.Text), 2, MidpointRounding.AwayFromZero);
                            dt.Rows[i]["DiscPer"] = txtDiscPer.Text;
                            dt.Rows[i]["DiscAmount"] = Math.Round(Convert.ToDecimal(lblDisc.Text), 2, MidpointRounding.AwayFromZero);
                            dt.Rows[i]["CGSTPer"] = lblCGSTPer.Text;
                            dt.Rows[i]["CGSTAmount"] = Math.Round(Convert.ToDecimal(lblCGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                            dt.Rows[i]["SGSTPer"] = lblSGSTPer.Text;
                            dt.Rows[i]["SGSTAmount"] = Math.Round(Convert.ToDecimal(lblSGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                            dt.Rows[i]["IGSTPer"] = lblIGSTPer.Text;
                            dt.Rows[i]["IGSTAmount"] = Math.Round(Convert.ToDecimal(lblIGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                            dt.Rows[i]["LineTotal"] = Math.Round(Convert.ToDecimal(lblLineTot.Text), 2, MidpointRounding.AwayFromZero);

                            Grid_Total_Value();
                            //Final_Total_Calculate();

                            ViewState["ItemTable"] = dt;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();

                        }
                        else
                        {


                            if (Convert.ToDecimal(txtDiscPer.Text.ToString()) != 0) { Discount_Percent = txtDiscPer.Text.ToString(); }
                            //if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                            if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                            if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                            if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                            //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                            //Discount Amt Calculate
                            if (Convert.ToDecimal(Discount_Percent) != 0)
                            {
                                Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                                Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                                Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                            }
                            else
                            {
                                Discount_Amt = "0.00";
                            }

                            if (Convert.ToDecimal(CGST_Per) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                            }
                            else
                            {
                                CGST_Amt = "0.00";
                            }

                            if (Convert.ToDecimal(VAT_Per) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                                VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                                VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                            }
                            else
                            {
                                VAT_Amt = "0.00";
                            }

                            if (Convert.ToDecimal(SGST_Per) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                            }
                            else
                            {
                                SGST_Amt = "0.00";
                            }

                            if (Convert.ToDecimal(IGST_Per) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                            }
                            else
                            {
                                IGST_Amt = "0.00";
                            }

                            //Other Charges
                            //if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                            //Final Amt
                            Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                            //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

                            lblItemTot.Text = Item_Total;

                            lblDisc.Text = Discount_Amt;
                            //lblTaxAmt.Text = Tax_Amt;
                            lblCGSTAmt.Text = CGST_Amt;
                            lblSGSTAmt.Text = SGST_Amt;
                            lblIGSTAmt.Text = IGST_Amt;
                            //txtVAT_AMT.Text = VAT_Amt;
                            lblLineTot.Text = Final_Amount;
                            txtTotAmt.Text = Final_Amount;
                            txtNetAmt.Text = Final_Amount;

                            lblBalQty.Text = BalQty;
                        }
                    }




                    dt.Rows[i]["SAPNo"] = lblSAPNo.Text;
                    dt.Rows[i]["ItemCode"] = lblItemCode.Text;
                    dt.Rows[i]["ItemName"] = lblItemName.Text;
                    dt.Rows[i]["DeptCode"] = lblGrdDeptCode.Text;
                    dt.Rows[i]["DeptName"] = lblGrdDeptName.Text;
                    dt.Rows[i]["WarehouseCode"] = lblGrdWHCode.Text;
                    dt.Rows[i]["WarehouseName"] = lblGrdWHName.Text;
                    dt.Rows[i]["Rack"] = lblRackName.Text;



                    dt.Rows[i]["OrderQty"] = lblBOMQty.Text;
                    dt.Rows[i]["ReceiveQty"] = txtGrdReceQty.Text;
                    dt.Rows[i]["BalQty"] = lblBalQty.Text;
                    dt.Rows[i]["Rate"] = Math.Round(Convert.ToDecimal(lblRate.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["ItemTotal"] = Math.Round(Convert.ToDecimal(lblItemTot.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["DiscPer"] = txtDiscPer.Text;
                    dt.Rows[i]["DiscAmount"] = Math.Round(Convert.ToDecimal(lblDisc.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["CGSTPer"] = lblCGSTPer.Text;
                    dt.Rows[i]["CGSTAmount"] = Math.Round(Convert.ToDecimal(lblCGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["SGSTPer"] = lblSGSTPer.Text;
                    dt.Rows[i]["SGSTAmount"] = Math.Round(Convert.ToDecimal(lblSGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["IGSTPer"] = lblIGSTPer.Text;
                    dt.Rows[i]["IGSTAmount"] = Math.Round(Convert.ToDecimal(lblIGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["LineTotal"] = Math.Round(Convert.ToDecimal(lblLineTot.Text), 2, MidpointRounding.AwayFromZero);

                    Grid_Total_Value();
                    //Final_Total_Calculate();

                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }


    private void Text_Change_Value_Cal(object sender, int Row_No)
    {
        try
        {
            bool ErrFlag = false;

            string Qty_Val = "0";
            string Item_Rate = "0";
            string Item_Total = "0";
            string Discount_Percent = "0";
            string Discount_Amt = "0";
            string VAT_Per = "0";
            string VAT_Amt = "0";
            string Tax_Amt = "0";
            string CGST_Per = "0";
            string CGST_Amt = "0";
            string SGST_Per = "0";
            string SGST_Amt = "0";
            string IGST_Per = "0";
            string IGST_Amt = "0";
            string BDUTax_Per = "0";
            string BDUTax_Amt = "0";
            string Other_Charges = "0";
            string Final_Amount = "0";
            string PackingAmt = "0";

            string OrdQty = "0";
            string BalQty = "0";
            string ExcessQty = "0";

            //string Item_Discount_Amt = "0.00";

            int i = Row_No;


            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];

            TextBox txtGrdOrdQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;

            if (txtGrdOrdQty.Text.ToString() != "" || txtGrdOrdQty.Text.ToString() != "0" || txtGrdOrdQty.Text.ToString() != "0.0" || txtGrdOrdQty.Text.ToString() != "0.00")
            {
                TextBox txtTest = ((TextBox)(sender));
                RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

                Label lblSAPNo = Repeater1.Items[i].FindControl("lblSAPNo") as Label;
                Label lblItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                Label lblItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                Label lblGrdUOM = Repeater1.Items[i].FindControl("lblUOM") as Label;
                Label lblGrdDeptCode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                Label lblGrdDeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;
                Label lblGrdWHCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                Label lblGrdWHName = Repeater1.Items[i].FindControl("lblWarehouse") as Label;
                Label lblRackName = Repeater1.Items[i].FindControl("lblRackName") as Label;

                Label lblBOMQty = Repeater1.Items[i].FindControl("lblOrdQty") as Label;
                TextBox txtGrdReceQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;
                Label lblBalQty = Repeater1.Items[i].FindControl("lblBalQty") as Label;
                Label lblExcessQty = Repeater1.Items[i].FindControl("lblExcessQty") as Label;
                Label lblRate = Repeater1.Items[i].FindControl("lblRate") as Label;
                Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                TextBox txtDiscPer = Repeater1.Items[i].FindControl("txtGrdDiscp") as TextBox;
                Label lblDisc = Repeater1.Items[i].FindControl("lblDiscAmt") as Label;
                Label lblCGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                Label lblCGSTAmt = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                Label lblSGSTAmt = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                Label lblIGSTAmt = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                //txtGrdRateEUR.ReadOnly = true;

                if (txtGrdReceQty.Text != "") { Qty_Val = txtGrdReceQty.Text.ToString(); }
                if (lblBOMQty.Text != "") { OrdQty = lblBOMQty.Text.ToString(); }


                if (lblRate.Text != "") { Item_Rate = lblRate.Text.ToString(); }

                Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();

                //if (txtDiscount.Text != "") { Discount_Percent = txtDiscount.Text.ToString(); }

                BalQty = (Convert.ToDecimal(OrdQty) - Convert.ToDecimal(Qty_Val)).ToString();


                if (Convert.ToDecimal(BalQty) < 0)
                {
                    //ErrFlag = true;

                    ExcessQty = (Convert.ToDecimal(Qty_Val) - Convert.ToDecimal(OrdQty)).ToString();
                    BalQty = "0.00";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('More Then Order Qty');", true);
                }

                if (!ErrFlag)
                {
                    if (Convert.ToDecimal(Item_Total) > 0)
                    {

                        if (Convert.ToDecimal(Item_Total) != 0)
                        {
                            if (Convert.ToDecimal(txtDiscPer.Text.ToString()) != 0) { Discount_Percent = txtDiscPer.Text.ToString(); }
                            //if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                            if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                            if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                            if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                            //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                            //Discount Amt Calculate
                            if (Convert.ToDecimal(Discount_Percent) != 0)
                            {
                                Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                                Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                                Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                            }
                            else
                            {
                                Discount_Amt = "0.00";
                            }

                            if (Convert.ToDecimal(CGST_Per) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                            }
                            else
                            {
                                CGST_Amt = "0.00";
                            }

                            if (Convert.ToDecimal(VAT_Per) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                                VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                                VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                            }
                            else
                            {
                                VAT_Amt = "0.00";
                            }

                            if (Convert.ToDecimal(SGST_Per) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                            }
                            else
                            {
                                SGST_Amt = "0.00";
                            }

                            if (Convert.ToDecimal(IGST_Per) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                            }
                            else
                            {
                                IGST_Amt = "0.00";
                            }

                            //Other Charges
                            //if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                            //Final Amt
                            Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                            //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

                            lblItemTot.Text = Item_Total;

                            lblDisc.Text = Discount_Amt;
                            //lblTaxAmt.Text = Tax_Amt;
                            lblCGSTAmt.Text = CGST_Amt;
                            lblSGSTAmt.Text = SGST_Amt;
                            lblIGSTAmt.Text = IGST_Amt;
                            //txtVAT_AMT.Text = VAT_Amt;
                            lblLineTot.Text = Final_Amount;
                            txtTotAmt.Text = Final_Amount;
                            txtNetAmt.Text = Final_Amount;

                            lblBalQty.Text = BalQty;
                            lblExcessQty.Text = ExcessQty;
                        }

                        dt.Rows[i]["SAPNo"] = lblSAPNo.Text;
                        dt.Rows[i]["ItemCode"] = lblItemCode.Text;
                        dt.Rows[i]["ItemName"] = lblItemName.Text;
                        dt.Rows[i]["UOM"] = lblGrdUOM.Text;
                        dt.Rows[i]["DeptCode"] = lblGrdDeptCode.Text;
                        dt.Rows[i]["DeptName"] = lblGrdDeptName.Text;
                        dt.Rows[i]["WarehouseCode"] = lblGrdWHCode.Text;
                        dt.Rows[i]["WarehouseName"] = lblGrdWHName.Text;
                        dt.Rows[i]["Rack"] = lblRackName.Text;



                        dt.Rows[i]["OrderQty"] = lblBOMQty.Text;
                        dt.Rows[i]["ReceiveQty"] = Convert.ToDecimal(txtGrdReceQty.Text).ToString();
                        dt.Rows[i]["BalQty"] = Math.Round(Convert.ToDecimal(lblBalQty.Text), 2, MidpointRounding.AwayFromZero);
                        dt.Rows[i]["ExcessQty"] = Math.Round(Convert.ToDecimal(lblExcessQty.Text), 2, MidpointRounding.AwayFromZero);
                        dt.Rows[i]["Rate"] = Math.Round(Convert.ToDecimal(lblRate.Text), 2, MidpointRounding.AwayFromZero);
                        dt.Rows[i]["ItemTotal"] = Math.Round(Convert.ToDecimal(lblItemTot.Text), 2, MidpointRounding.AwayFromZero);
                        dt.Rows[i]["DiscPer"] = txtDiscPer.Text;
                        dt.Rows[i]["DiscAmount"] = Math.Round(Convert.ToDecimal(lblDisc.Text), 2, MidpointRounding.AwayFromZero);
                        dt.Rows[i]["CGSTPer"] = lblCGSTPer.Text;
                        dt.Rows[i]["CGSTAmount"] = Math.Round(Convert.ToDecimal(lblCGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                        dt.Rows[i]["SGSTPer"] = lblSGSTPer.Text;
                        dt.Rows[i]["SGSTAmount"] = Math.Round(Convert.ToDecimal(lblSGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                        dt.Rows[i]["IGSTPer"] = lblIGSTPer.Text;
                        dt.Rows[i]["IGSTAmount"] = Math.Round(Convert.ToDecimal(lblIGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                        dt.Rows[i]["LineTotal"] = Math.Round(Convert.ToDecimal(lblLineTot.Text), 2, MidpointRounding.AwayFromZero);

                        Grid_Total_Value();
                        //Final_Total_Calculate();

                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();

                    }
                    else
                    {


                        if (Convert.ToDecimal(txtDiscPer.Text.ToString()) != 0) { Discount_Percent = txtDiscPer.Text.ToString(); }
                        //if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                        if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                        if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                        if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                        //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                        //Discount Amt Calculate
                        if (Convert.ToDecimal(Discount_Percent) != 0)
                        {
                            Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                            Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                            Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                        }
                        else
                        {
                            Discount_Amt = "0.00";
                        }

                        if (Convert.ToDecimal(CGST_Per) != 0)
                        {
                            string Item_Discount_Amt = "0.00";
                            Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                            CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                            CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                            CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                        }
                        else
                        {
                            CGST_Amt = "0.00";
                        }

                        if (Convert.ToDecimal(VAT_Per) != 0)
                        {
                            string Item_Discount_Amt = "0.00";
                            Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                            VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                            VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                            VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                        }
                        else
                        {
                            VAT_Amt = "0.00";
                        }

                        if (Convert.ToDecimal(SGST_Per) != 0)
                        {
                            string Item_Discount_Amt = "0.00";
                            Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                            SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                            SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                            SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                        }
                        else
                        {
                            SGST_Amt = "0.00";
                        }

                        if (Convert.ToDecimal(IGST_Per) != 0)
                        {
                            string Item_Discount_Amt = "0.00";
                            Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                            IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                            IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                            IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                        }
                        else
                        {
                            IGST_Amt = "0.00";
                        }

                        //Other Charges
                        //if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                        //Final Amt
                        Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                        Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                        Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                        Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

                        lblItemTot.Text = Item_Total;

                        lblDisc.Text = Discount_Amt;
                        //lblTaxAmt.Text = Tax_Amt;
                        lblCGSTAmt.Text = CGST_Amt;
                        lblSGSTAmt.Text = SGST_Amt;
                        lblIGSTAmt.Text = IGST_Amt;
                        //txtVAT_AMT.Text = VAT_Amt;
                        lblLineTot.Text = Final_Amount;
                        txtTotAmt.Text = Final_Amount;
                        txtNetAmt.Text = Final_Amount;

                        lblBalQty.Text = BalQty;
                        lblExcessQty.Text = ExcessQty;
                    }
                    dt.Rows[i]["SAPNo"] = lblSAPNo.Text;
                    dt.Rows[i]["ItemCode"] = lblItemCode.Text;
                    dt.Rows[i]["ItemName"] = lblItemName.Text;
                    dt.Rows[i]["UOM"] = lblGrdUOM.Text;
                    dt.Rows[i]["DeptCode"] = lblGrdDeptCode.Text;
                    dt.Rows[i]["DeptName"] = lblGrdDeptName.Text;
                    dt.Rows[i]["WarehouseCode"] = lblGrdWHCode.Text;
                    dt.Rows[i]["WarehouseName"] = lblGrdWHName.Text;
                    dt.Rows[i]["Rack"] = lblRackName.Text;



                    dt.Rows[i]["OrderQty"] = lblBOMQty.Text;
                    dt.Rows[i]["ReceiveQty"] = txtGrdReceQty.Text;
                    dt.Rows[i]["BalQty"] = Math.Round(Convert.ToDecimal(lblBalQty.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["ExcessQty"] = Math.Round(Convert.ToDecimal(lblExcessQty.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["Rate"] = Math.Round(Convert.ToDecimal(lblRate.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["ItemTotal"] = Math.Round(Convert.ToDecimal(lblItemTot.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["DiscPer"] = txtDiscPer.Text;
                    dt.Rows[i]["DiscAmount"] = Math.Round(Convert.ToDecimal(lblDisc.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["CGSTPer"] = lblCGSTPer.Text;
                    dt.Rows[i]["CGSTAmount"] = Math.Round(Convert.ToDecimal(lblCGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["SGSTPer"] = lblSGSTPer.Text;
                    dt.Rows[i]["SGSTAmount"] = Math.Round(Convert.ToDecimal(lblSGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["IGSTPer"] = lblIGSTPer.Text;
                    dt.Rows[i]["IGSTAmount"] = Math.Round(Convert.ToDecimal(lblIGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["LineTotal"] = Math.Round(Convert.ToDecimal(lblLineTot.Text), 2, MidpointRounding.AwayFromZero);

                    //Final_Total_Calculate();

                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    string SSQL = "";
                    DataTable DT_Serial = new DataTable();
                    

                    SSQL = "Select * from BOMMaster Where Mat_No='" + lblItemCode.Text + "' and SerialNoWise='1' and Status!='Delete' ";

                    DT_Serial = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_Serial.Rows.Count > 0)
                    {
                        hfSerialSAPNo.Value = lblSAPNo.Text;
                        hfSerialItemCode.Value = lblItemCode.Text;
                        hfSerialItemName.Value = lblItemName.Text;
                        hfSerialUOM.Value = lblGrdUOM.Text;
                        hfSerialReceivedQty.Value = txtGrdOrdQty.Text;

                       
                        DataTable DTSerial = new DataTable();

                        RowCount = 0;

                        DTSerial = (DataTable)ViewState["SerialNoDet"];

                        rptSerialNoDet.DataSource = DTSerial;
                        rptSerialNoDet.DataBind();

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Enter The Serial Number');", true);

                        pnlSerialNoDet.Visible = true;

                    }
                    else
                    {
                        pnlSerialNoDet.Visible = false;
                    }


                    DataTable DT_Batch = new DataTable();

                    SSQL = "Select * from BOMMaster Where Mat_No='" + lblItemCode.Text + "' and BatchNoApplicable='1' and Status!='Delete' ";

                    DT_Batch = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_Batch.Rows.Count > 0)
                    {
                        hfBatchSAPNo.Value = lblSAPNo.Text;
                        hfBatchItemCode.Value = lblItemCode.Text;
                        hfBatchItemName.Value = lblItemName.Text;
                        hfBatchUOM.Value = lblGrdUOM.Text;
                        hfBatchQty.Value = txtGrdOrdQty.Text;


                        DataTable DTBatch = new DataTable();

                        BatchQty = 0;

                        DTBatch = (DataTable)ViewState["BatchNoDet"];

                        rptBatchNoDet.DataSource = DTBatch;
                        rptBatchNoDet.DataBind();

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Enter The Batch Number');", true);

                        pnlBatchNo.Visible = true;

                    }
                    else
                    {
                        pnlBatchNo.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        TextBox txtReceiveQyt = e.Item.FindControl("txtReceivQty") as TextBox;
    }

    protected void txtRoundOff_TextChanged(object sender, EventArgs e)
    {
        string Final_NetAmt = "0";
        string AddorLess = "0";
        string Item_Total_Amt = lblTotLineAmt.Text;

        string[] AOL = Item_Total_Amt.Split('.');

        if (Convert.ToDecimal(txtRoundOff.Text.ToString()) != 0) { AddorLess = txtRoundOff.Text.ToString(); }

        if (Convert.ToDecimal(AOL[1].ToString()) >= Convert.ToDecimal(50))
        {
            Final_NetAmt = (Convert.ToDecimal(AddorLess) + Convert.ToDecimal(Item_Total_Amt)).ToString();
        }
        else if (Convert.ToDecimal(AOL[1].ToString()) < Convert.ToDecimal(49))
        {
            Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) - Convert.ToDecimal(AddorLess)).ToString();
        }

        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetAmt.Text = Final_NetAmt;
    }


    private void Load_Data_Warehouse()
    {
        string SSQL = "";
        DataTable dt_Warehouse = new DataTable();

        SSQL = "Select  WarehouseName,WarehouseCode from MstWarehouse where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Status !='Delete'";

        dt_Warehouse = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlWareHouse.DataSource = dt_Warehouse;
        ddlWareHouse.DataTextField = "WarehouseName";
        ddlWareHouse.DataValueField = "WarehouseCode";
        ddlWareHouse.DataBind();
        ddlWareHouse.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void ddlWareHouse_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!ddlWareHouse.SelectedValue.Contains("-Select-"))
        {
            string SSQL = "";

            SSQL = "Select RackSerious from MstWareHouseRackMain where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
            SSQL = SSQL + " and Status!='Delete'";

            DataTable dt_RackSerious = new DataTable();
            dt_RackSerious = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlRackName.DataSource = dt_RackSerious;
            ddlRackName.DataTextField = "RackSerious";
            ddlRackName.DataValueField = "RackSerious";
            ddlRackName.DataBind();
            ddlRackName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }

    protected void ddlSupplierType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_PurOrder_No();
        Load_PurOrder_GatePassIN();
    }

    public void Grid_Total_Value()
    {
        decimal SumLineTot = 0;
        decimal SumReceQtyTot = 0;
        decimal SumItemTot = 0;
        decimal SumCGSTTot = 0;
        decimal SumSGSTTot = 0;
        decimal SumIGSTTot = 0;
        decimal SumDiscTot = 0;

        string PackAmt = "0.00";
        string TransAmt = "0.00";
        string CourierAmt = "0.00";
        string OtherAmt1 = "0.00";
        string OtherAmt2 = "0.00";

        DataTable DT = new DataTable();

        DT = (DataTable)ViewState["ItemTable"];

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            TextBox txtGrdRecQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;
            Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
            Label lblDiscTot = Repeater1.Items[i].FindControl("lblDiscAmt") as Label;
            Label lblCGSTTot = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
            Label lblSGSTTot = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
            Label lblIGSTTot = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
            Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;

            SumReceQtyTot = SumReceQtyTot + Convert.ToDecimal(txtGrdRecQty.Text);
            SumItemTot = SumItemTot + Convert.ToDecimal(lblItemTot.Text);
            SumDiscTot = SumDiscTot + Convert.ToDecimal(lblDiscTot.Text);
            SumCGSTTot = SumCGSTTot + Convert.ToDecimal(lblCGSTTot.Text);
            SumSGSTTot = SumSGSTTot + Convert.ToDecimal(lblSGSTTot.Text);
            SumIGSTTot = SumIGSTTot + Convert.ToDecimal(lblIGSTTot.Text);
            SumLineTot = SumLineTot + Convert.ToDecimal(lblLineTot.Text);
        }


        if (chkPFGst.Checked == true || chkTCGst.Checked == true || chkCCGst.Checked == true || chkOther1Gst.Checked == true || chkOther2Gst.Checked == true)
        {
            if (txtPackFrwdCharge.Text != "") { PackAmt = txtPackFrwdCharge.Text.ToString(); }
            if (txtTransCharge.Text != "") { TransAmt = txtTransCharge.Text.ToString(); }
            if (txtCourierCharge.Text != "") { CourierAmt = txtCourierCharge.Text.ToString(); }
            if (txtOther1Amt.Text != "") { OtherAmt1 = txtOther1Amt.Text.ToString(); }
            if (txtOther2Amt.Text != "") { OtherAmt2 = txtOther2Amt.Text.ToString(); }

            txtTotQty.Text = SumReceQtyTot.ToString();
            //SumItemTot.ToString();
            txtTotAmt.Text = (Convert.ToDecimal(SumItemTot) + Convert.ToDecimal(PackAmt) + Convert.ToDecimal(CourierAmt) + Convert.ToDecimal(OtherAmt1) + Convert.ToDecimal(OtherAmt2) + Convert.ToDecimal(TransAmt)).ToString();
            lblTotDisc.Text = SumDiscTot.ToString();

            string ItemAmt = (Convert.ToDecimal(txtTotAmt.Text) - Convert.ToDecimal(SumDiscTot)).ToString();
            string SSQL = "";
            string CGST_Amt = "";
            string SGST_Amt = "";
            string IGST_Amt = "";

            SSQL = "Select * from GstMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " and GST_Type='" + txtGSTType.Text + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            //Tax Calculate
            CGST_Amt = (Convert.ToDecimal(ItemAmt) * Convert.ToDecimal(DT.Rows[0]["CGST_Per"].ToString())).ToString();
            CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
            CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            SGST_Amt = (Convert.ToDecimal(ItemAmt) * Convert.ToDecimal(DT.Rows[0]["SGST_Per"].ToString())).ToString();
            SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
            SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            IGST_Amt = (Convert.ToDecimal(ItemAmt) * Convert.ToDecimal(DT.Rows[0]["IGST_Per"].ToString())).ToString();
            IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
            IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();


            lblTotCGST.Text = CGST_Amt.ToString();
            lblTotSGST.Text = SGST_Amt.ToString();
            lblTotIGST.Text = IGST_Amt.ToString();
            lblTotLineAmt.Text = (Convert.ToDecimal(ItemAmt) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            txtNetAmt.Text = (Convert.ToDecimal(ItemAmt) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
        }
        else
        {

            if (txtPackFrwdCharge.Text != "") { PackAmt = txtPackFrwdCharge.Text.ToString(); }
            if (txtTransCharge.Text != "") { TransAmt = txtTransCharge.Text.ToString(); }
            if (txtCourierCharge.Text != "") { CourierAmt = txtCourierCharge.Text.ToString(); }
            if (txtOther1Amt.Text != "") { OtherAmt1 = txtOther1Amt.Text.ToString(); }
            if (txtOther2Amt.Text != "") { OtherAmt2 = txtOther2Amt.Text.ToString(); }

            txtTotQty.Text = SumReceQtyTot.ToString();
            txtTotAmt.Text = SumItemTot.ToString();
            lblTotDisc.Text = SumDiscTot.ToString();
            lblTotCGST.Text = SumCGSTTot.ToString();
            lblTotSGST.Text = SumSGSTTot.ToString();
            lblTotIGST.Text = SumIGSTTot.ToString();
            lblTotLineAmt.Text = (Convert.ToDecimal(SumLineTot) + Convert.ToDecimal(PackAmt) + Convert.ToDecimal(CourierAmt) + Convert.ToDecimal(OtherAmt1) + Convert.ToDecimal(OtherAmt2) + Convert.ToDecimal(TransAmt)).ToString();
            txtNetAmt.Text = (Convert.ToDecimal(SumLineTot) + Convert.ToDecimal(PackAmt) + Convert.ToDecimal(CourierAmt) + Convert.ToDecimal(OtherAmt1) + Convert.ToDecimal(OtherAmt2) + Convert.ToDecimal(TransAmt)).ToString();
        }


    }

    private void OtherCharge_Calc()
    {
        //Boolean Errflg = false;

        //string SubTot;
        //string PackFrwdChr = "0.00";
        //string TransChr = "0.00";
        //string CouriChr = "0.00";
        //string SSQL;
        //DataTable DT = new DataTable();
        //string CGST_Amt;
        //string SGST_Amt;
        //string IGST_Amt;

        //if (txtPackFrwdCharge.Text != "") { PackFrwdChr = txtPackFrwdCharge.Text.ToString(); }
        //if (txtTransCharge.Text != "") { TransChr = txtTransCharge.Text.ToString(); }
        //if (txtCourierCharge.Text != "") { CouriChr = txtCourierCharge.Text.ToString(); }

        //if (chkPFGst.Checked == true || chkTCGst.Checked == true || chkCCGst.Checked == true)
        //{
        //    if (txtGSTType.Text == "-Select-")
        //    {
        //        Errflg = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select GST Type');", true);
        //    }

        //    if (!Errflg)
        //    {

        //        SubTot = (Convert.ToDecimal(txtTotAmt.Text) - Convert.ToDecimal(lblTotDisc.Text)).ToString();

        //        SubTot = (Convert.ToDecimal(SubTot) + Convert.ToDecimal(PackFrwdChr) + Convert.ToDecimal(TransChr) + Convert.ToDecimal(CouriChr)).ToString();

        //        SubTot = (Math.Round(Convert.ToDecimal(SubTot), 2, MidpointRounding.AwayFromZero)).ToString();

        //        SSQL = "Select * from GstMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        //        SSQL = SSQL + " and GST_Type='" + txtGSTType.Text + "'";

        //        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //        //Tax Calculate
        //        CGST_Amt = (Convert.ToDecimal(SubTot) * Convert.ToDecimal(DT.Rows[0]["CGST_Per"].ToString())).ToString();
        //        CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
        //        CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        //        SGST_Amt = (Convert.ToDecimal(SubTot) * Convert.ToDecimal(DT.Rows[0]["SGST_Per"].ToString())).ToString();
        //        SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
        //        SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        //        IGST_Amt = (Convert.ToDecimal(SubTot) * Convert.ToDecimal(DT.Rows[0]["IGST_Per"].ToString())).ToString();
        //        IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
        //        IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        //        lblTotCGST.Text = CGST_Amt;
        //        lblTotSGST.Text = SGST_Amt;
        //        lblTotIGST.Text = IGST_Amt;

        //        txtTotAmt.Text = (Math.Round(Convert.ToDecimal(SubTot) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        //        lblTotLineAmt.Text = txtTotAmt.Text;

        //        txtNetAmt.Text = txtTotAmt.Text;
        //    }
        //}
        //else
        //{
        //    SubTot = (Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(lblTotCGST.Text) + Convert.ToDecimal(lblTotSGST.Text) + Convert.ToDecimal(lblTotIGST.Text) + Convert.ToDecimal(PackFrwdChr) + Convert.ToDecimal(TransChr) + Convert.ToDecimal(CouriChr)).ToString();

        //    SubTot = (Math.Round(Convert.ToDecimal(SubTot), 2, MidpointRounding.AwayFromZero)).ToString();

        //    lblTotLineAmt.Text = SubTot;

        //    txtNetAmt.Text = SubTot;
        //}
    }

    protected void txtPackFrwdCharge_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkPFGst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkTCGst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void txtTransCharge_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkCCGst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void txtCourierCharge_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_PurOrder_No();
        Load_PurOrder_GatePassIN();
    }

    protected void ddlGPIN_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Sub = new DataTable();
        DataTable DT_Main = new DataTable();



        SSQL = "Select * from Trans_Pur_GIN_Main where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Pur_GIN_No='" + ddlGPIN.SelectedItem.Text + "' ";

        DT_Main = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Trans_Pur_GIN_Sub where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Pur_GIN_No='" + ddlGPIN.SelectedItem.Text + "' ";

        DT_Sub = objdata.RptEmployeeMultipleDetails(SSQL);


        txtGINDate.Text = DT_Main.Rows[0]["Pur_GIN_Date"].ToString();
        txtVehiNo.Text = DT_Main.Rows[0]["VehicleNo"].ToString();
        txtPartyInvNo.Text = DT_Main.Rows[0]["SuppInvNo"].ToString();
        txtPartyInvDate.Text = DT_Main.Rows[0]["SuppInvDate"].ToString();

        ddlPurOrdNo.DataSource = DT_Sub;

        DataRow dr = DT_Sub.NewRow();

        dr["PurOrdRefNo"] = "-Select-";

        DT_Sub.Rows.InsertAt(dr, 0);
        ddlPurOrdNo.DataTextField = "PurOrdRefNo";
        ddlPurOrdNo.DataValueField = "PurOrdRefNo";
        ddlPurOrdNo.DataBind();
    }

    private void Initial_Data_SerialNo()
    {

        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM", typeof(string)));
        dt.Columns.Add(new DataColumn("SerialNo", typeof(string)));

        ViewState["SerialNoDet"] = dt;

        rptSerialNoDet.DataSource = dt;
        rptSerialNoDet.DataBind();

    }

    private void Initial_Data_BatchNo()
    {

        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM", typeof(string)));
        dt.Columns.Add(new DataColumn("BatchNo", typeof(string)));
        dt.Columns.Add(new DataColumn("Qty", typeof(string)));

        ViewState["BatchNoDet"] = dt;

        rptBatchNoDet.DataSource = dt;
        rptBatchNoDet.DataBind();

    }

    protected void btnAddSerialNo_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable qry_dt = new DataTable();
            bool ErrFlag = false;
            DataRow dr = null;
            string SSQL = "";
             
            dt = (DataTable)ViewState["SerialNoDet"];

            if (txtSerialNo.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Serial Number...');", true);
            }

            SSQL = "Select * from Trans_Stock_SerialNo where PurOrderNo='" + ddlPurOrdNo.SelectedItem.Text + "' And ";
            SSQL = SSQL + " ItemCode='" + hfSerialItemCode.Value + "' and SerialNo='" + txtSerialNo.Text + "' ";

            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);


            if (qry_dt.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Serial Number Already Exist on This Goods Received No :" + qry_dt.Rows[0]["GRNNo"].ToString() + "...');", true);
            }

            if (!ErrFlag)
            {
                if (ViewState["SerialNoDet"] != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["SerialNo"].ToString().ToUpper() == txtSerialNo.Text.ToString())
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Serial No Already Added..');", true);

                        }
                    }

                    


                    if (!ErrFlag)
                    {
                        if (RowCount < Convert.ToDecimal(hfSerialReceivedQty.Value))
                        {
                            dr = dt.NewRow();
                            dr["SAPNo"] = hfSerialSAPNo.Value;
                            dr["ItemCode"] = hfSerialItemCode.Value;
                            dr["ItemName"] = hfSerialItemName.Value;
                            dr["UOM"] = hfSerialUOM.Value;
                            dr["SerialNo"] = txtSerialNo.Text;

                            dt.Rows.Add(dr);
                            ViewState["SerialNoDet"] = dt;
                            rptSerialNoDet.DataSource = dt;
                            rptSerialNoDet.DataBind();

                            txtSerialNo.Text = "";

                            RowCount = RowCount + 1;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('More No Of Serial No in Received Qty..');", true);
                            ViewState["SerialNoDet"] = dt;
                            rptSerialNoDet.DataSource = dt;
                            rptSerialNoDet.DataBind();
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < Convert.ToDecimal(hfSerialReceivedQty.Value); j++)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["SerialNo"].ToString().ToUpper() == txtSerialNo.Text.ToString())
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Serial No Already Added..');", true);

                            }
                        }

                        dr = dt.NewRow();
                        dr["SAPNo"] = hfSerialSAPNo.Value;
                        dr["ItemCode"] = hfSerialItemCode.Value;
                        dr["ItemName"] = hfSerialItemName.Value;
                        dr["UOM"] = hfSerialUOM.Value;
                        dr["SerialNo"] = txtSerialNo.Text;

                        dt.Rows.Add(dr);
                        ViewState["SerialNoDet"] = dt;
                        rptSerialNoDet.DataSource = dt;
                        rptSerialNoDet.DataBind();

                        txtSerialNo.Text = "";
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void GridSerialNoDeleteClick(object sender, CommandEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            string ItemName = "";

            dt = (DataTable)ViewState["SerialNoDet"];


            ItemName = e.CommandName.ToString();

            string[] Val = ItemName.Split(',');

            string ItemCode = Val[0].ToString();
            string SerialNo = Val[1].ToString();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ItemCode"].ToString() == ItemCode && dt.Rows[i]["SerialNo"].ToString() == SerialNo.ToString())
                {
                    dt.Rows.RemoveAt(i);
                    dt.AcceptChanges();
                }
            }

            ViewState["SerialNoDet"] = dt;
            rptSerialNoDet.DataSource = dt;
            rptSerialNoDet.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtOther2Amt_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void txtOther1Amt_TextChanged(object sender, EventArgs e)
        {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkOther1Gst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkOther2Gst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void btnAddBatchNo_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable qry_dt = new DataTable();
            bool ErrFlag = false;
            DataRow dr = null;
            string SSQL = "";

            dt = (DataTable)ViewState["BatchNoDet"];

            if (txtBatchNo.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Batch Number...');", true);
            }

            SSQL = "Select * from Trans_Stock_BatchNo where PurOrderNo='" + ddlPurOrdNo.SelectedItem.Text + "' And ";
            SSQL = SSQL + " ItemCode='" + hfBatchItemCode.Value + "' and BatchNo='" + txtBatchNo.Text + "' ";

            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);


            if (qry_dt.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Batch Number Already Exist on This Goods Received No :" + qry_dt.Rows[0]["GRNNo"].ToString() + "...');", true);
            }

            if (!ErrFlag)
            {
                if (ViewState["BatchNoDet"] != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["BatchNo"].ToString().ToUpper() == txtBatchNo.Text.ToString())
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Batch No Already Added..');", true);

                        }
                    }




                    if (!ErrFlag)
                    {
                        if (BatchQty < Convert.ToDecimal(hfBatchQty.Value))
                        {
                            dr = dt.NewRow();
                            dr["SAPNo"] = hfBatchSAPNo.Value;
                            dr["ItemCode"] = hfBatchItemCode.Value;
                            dr["ItemName"] = hfBatchItemName.Value;
                            dr["UOM"] = hfBatchUOM.Value;
                            dr["BatchNo"] = txtBatchNo.Text;
                            dr["Qty"] = txtBatchQty.Text;


                            dt.Rows.Add(dr);
                            ViewState["BatchNoDet"] = dt;
                            rptBatchNoDet.DataSource = dt;
                            rptBatchNoDet.DataBind();

                            BatchQty = BatchQty + Convert.ToDecimal(txtBatchQty.Text);

                            txtBatchNo.Text = "";
                            txtBatchQty.Text = "";

                            
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('More No Of Batch Qty in Received Qty..');", true);
                            ViewState["BatchNoDet"] = dt;
                            rptBatchNoDet.DataSource = dt;
                            rptBatchNoDet.DataBind();
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < Convert.ToDecimal(hfBatchQty.Value); j++)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["BatchNo"].ToString().ToUpper() == txtBatchNo.Text.ToString())
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Batch No Already Added..');", true);

                            }
                        }

                        dr = dt.NewRow();
                        dr["SAPNo"] = hfBatchSAPNo.Value;
                        dr["ItemCode"] = hfBatchItemCode.Value;
                        dr["ItemName"] = hfBatchItemName.Value;
                        dr["UOM"] = hfBatchUOM.Value;
                        dr["BatchNo"] = txtBatchNo.Text;
                        dr["Qty"] = txtBatchQty.Text;


                        dt.Rows.Add(dr);
                        ViewState["BatchNoDet"] = dt;
                        rptBatchNoDet.DataSource = dt;
                        rptBatchNoDet.DataBind();

                        BatchQty = BatchQty + Convert.ToDecimal(txtBatchQty.Text);

                        txtBatchNo.Text = "";
                        txtBatchQty.Text = "";
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }
    }
}