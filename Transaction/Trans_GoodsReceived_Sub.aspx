﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GoodsReceived_Sub.aspx.cs" Inherits="Trans_GoodsReceived_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <asp:UpdatePanel ID="upGoodsRecev" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Create New Goods Receipt</h1>
                    <asp:Label runat="server" ID="lblErrorMsg" style="color:red;font-size:x-large" ></asp:Label>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Account Type</label>
                                                <asp:DropDownList runat="server" ID="ddlSupplierType" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlSupplierType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">Enercon</asp:ListItem>
                                                    <asp:ListItem Value="2">Escrow</asp:ListItem>
                                                    <asp:ListItem Value="3">Coral</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Material Type</label>
                                                <asp:DropDownList runat="server" ID="ddlMatType" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">Raw Material</asp:ListItem>
                                                    <asp:ListItem Value="2">Tools</asp:ListItem>
                                                    <%--<asp:ListItem Value="3">Assets</asp:ListItem>--%>
                                                    <asp:ListItem Value="4">General Item</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Goods_Receipt_No.</label>
                                                <asp:Label ID="txtGRNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Goods_Receipt_Date</label>
                                                <asp:TextBox ID="txtGRDate" runat="server" class="form-control pull-right datepicker" AutoComplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtGRDate" ValidationGroup="Validate_Field" class="form_error"
                                                    ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars"
                                                    FilterType="Custom,Numbers" TargetControlID="txtGRDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Warehouse</label>
                                                <asp:DropDownList ID="ddlWareHouse" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlWareHouse_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Rack Name</label>
                                                <asp:DropDownList ID="ddlRackName" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Gate Pass IN</label>
                                                <asp:DropDownList runat="server" ID="ddlGPIN" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlGPIN_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Gate Pass Date</label>
                                                <asp:TextBox runat="server" ID="txtGINDate" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Purchase_Order_No.</label>
                                                <asp:DropDownList runat="server" ID="ddlPurOrdNo" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlPurOrdNo_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Purchase_Order_Date</label>
                                                <asp:TextBox ID="txtPurOrdDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Name</label>
                                                <asp:TextBox ID="txtSuppName" runat="server" class="form-control"></asp:TextBox>
                                                <asp:HiddenField ID="hdSuppCode" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Party_Invoice_No.</label>
                                                <asp:TextBox ID="txtPartyInvNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Party_Invoice_Date</label>
                                                <asp:TextBox ID="txtPartyInvDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtPartyInvDate" ValidationGroup="Validate_Field" class="form_error"
                                                    ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtPartyInvDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Payment Mode</label>
                                                <asp:TextBox ID="txtPayMode" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Vehicle No</label>
                                                <asp:TextBox ID="txtVehiNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">GST Type</label>
                                                <asp:TextBox ID="txtGSTType" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Currency Type</label>
                                                <asp:TextBox ID="txtCurrency" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Notes</label>
                                                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Style="resize: none" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Payment Terms</label>
                                                <asp:TextBox ID="txtPayTerms" TextMode="MultiLine" Style="resize: none" runat="server"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Style="resize: none" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Item Details</span></h3>
                                                </div>
                                                <asp:Panel ID="pnlRpter" runat="server" Height="400px" Style="overflow-x: scroll; overflow-y: scroll">
                                                    <div class="box-body no-padding">
                                                        <div class="table-responsive mailbox-messages" runat="server" style="padding-top: 15px">
                                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false" OnItemCommand="Repeater1_ItemCommand">
                                                                <HeaderTemplate>
                                                                    <table id="tbltest" class="table table-hover table-bordered table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Sl_No</th>
                                                                                <th>Sap_No</th>
                                                                                <th>Item Code</th>
                                                                                <th>Item_Name</th>
                                                                                <th runat="server">UOM</th>
                                                                                <th runat="server" visible="false">DeptCode</th>
                                                                                <th runat="server" visible="false">DeptName</th>
                                                                                <th runat="server" visible="false">WarehouseCode</th>
                                                                                <th runat="server" visible="false">Warehouse</th>
                                                                                <th runat="server" visible="false">RackName</th>
                                                                                <th>Order_Qty</th>
                                                                                <th>Received_Qty</th>
                                                                                <th runat="server" visible="true">Balance_Qty</th>
                                                                                <th runat="server" visible="true">Excess_Qty</th>
                                                                                <th>Rate</th>
                                                                                <th>ItemTotal</th>
                                                                                <th>Discount(%)</th>
                                                                                <th>Discount_Amount</th>
                                                                                <th runat="server" visible="false">CGSTPer</th>
                                                                                <th>CGST</th>
                                                                                <th runat="server" visible="false">SGSTPer</th>
                                                                                <th>SGST</th>
                                                                                <th runat="server" visible="false">IGSTPer</th>
                                                                                <th>IGST</th>
                                                                                <th>Line_Total</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <%# Container.ItemIndex + 1 %>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblSAPNo" runat="server" Style="width: 100%; border: none" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server">
                                                                            <asp:Label ID="lblItemCode" runat="server" Style="width: 100%; border: none" Text='<%# Eval("ItemCode")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblItemName" runat="server" Style="width: 100%; border: none" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server">
                                                                            <asp:Label ID="lblUOM" runat="server" Style="width: 100%; border: none" Text='<%# Eval("UOM")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblDeptCode" runat="server" Style="width: 100%; border: none" Text='<%# Eval("DeptCode")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblDeptName" runat="server" Style="width: 100%; border: none" Text='<%# Eval("DeptName")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblWarehouseCode" runat="server" Style="width: 100%; border: none" Text='<%# Eval("WarehouseCode")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblWarehouse" runat="server" Style="width: 100%; border: none" Text='<%# Eval("WarehouseName")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblRackName" runat="server" Style="width: 100%; border: none" Text='<%# Eval("Rack")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblOrdQty" runat="server" Style="width: 100%; border: none" Text='<%# Eval("OrderQty")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtReceivQty" class="form-control" Text='<%# Eval("ReceiveQty")%>'
                                                                                Style="width: 100%; border: none" AutoPostBack="true" OnTextChanged="txtReceive_TextChanged"> 
                                                                            </asp:TextBox>

                                                                            <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                                TargetControlID="txtReceivQty" ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:Label ID="lblBalQty" runat="server" Style="width: 100%; border: none" Text='<%# Eval("BalQty")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:Label ID="lblExcessQty" runat="server" Style="width: 100%; border: none" Text='<%# Eval("ExcessQty")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblRate" Style="width: 100%; border: none" Text='<%# Eval("Rate")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblItemTot" Style="width: 100%; border: none" Text='<%# Eval("ItemTotal")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtGrdDiscp" class="form-control" Text='<%# Eval("DiscPer")%>'
                                                                                Style="width: 100%; border: none" AutoPostBack="true" OnTextChanged="txtGrdDiscp_TextChanged"> 
                                                                            </asp:TextBox>

                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                                TargetControlID="txtGrdDiscp" ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblDiscAmt" Style="width: 100%; border: none" Text='<%# Eval("DiscAmount")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblCGSTPer" Style="width: 100%; border: none" Text='<%# Eval("CGSTPer")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblCGSTAmt" Style="width: 100%; border: none" Text='<%# Eval("CGSTAmount")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblSGSTPer" Style="width: 100%; border: none" Text='<%# Eval("SGSTPer")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblSGSTAmt" Style="width: 100%; border: none" Text='<%# Eval("SGSTAmount")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblIGSTPer" Style="width: 100%; border: none" Text='<%# Eval("IGSTPer")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblIGSTAmt" Style="width: 100%; border: none" Text='<%# Eval("IGSTAmount")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblLineTot" Text='<%# Eval("LineTotal")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                            <!-- /.table -->
                                                        </div>
                                                        <!-- /.mail-box-messages -->
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>

                                    <div class="box-body no-padding">
                                        <asp:Panel ID="pnlSerialNoDet" runat="server" class="panel panel-success" Visible="false">
                                            <div class="panel-body">

                                                <div class="row" style="padding-left: 1%; padding-bottom: 1%">
                                                    <h3 class="box-title" style="color: green;"><span>Serial Number Details</span></h3>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-2">
                                                        <label for="exampleInputName">Serial No</label>
                                                        <asp:TextBox runat="server" class="form-control" ID="txtSerialNo" autocomplete="off" />
                                                    </div>

                                                    <div class="col-md-2" runat="server" style="padding-top: 2%">
                                                        <asp:Button runat="server" class="btn btn-success" ID="btnAddSerialNo" Text="Add" OnClick="btnAddSerialNo_Click" />
                                                    </div>
                                                </div>

                                                <div class="row" runat="server" style="padding-top: 1%">
                                                    <asp:Panel ID="pnlSerialDetRpter" class="panel panel-success" runat="server"
                                                        Height="400px" Style="overflow-x: scroll; overflow-y: scroll" BorderStyle="None">
                                                        <div class="col-md-12">
                                                            <asp:Repeater ID="rptSerialNoDet" runat="server" EnableViewState="true">
                                                                <HeaderTemplate>
                                                                    <table id="tblSerialNoDet" class="table table-hover table-bordered table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S.No</th>
                                                                                <th>SapNo</th>
                                                                                <th runat="server" visible="false">ItemCode</th>
                                                                                <th>ItemName</th>
                                                                                <th>UOM</th>
                                                                                <th>Serial_No</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Container.ItemIndex + 1 %></td>

                                                                        <td>
                                                                            <asp:Label ID="lblSAPNo" Style="width: 100%; border: none"
                                                                                runat="server" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblItemCode" Style="width: 100%; border: none"
                                                                                runat="server" Text='<%# Eval("ItemCode")%>'>
                                                                            </asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblItemName" runat="server" Style="width: 100%; border: none"
                                                                                Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblUOM" runat="server" Style="width: 100%; border: none"
                                                                                Text='<%# Eval("UOM")%>'>
                                                                            </asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblSerialNo" runat="server" Style="width: 100%; border: none"
                                                                                Text='<%# Eval("SerialNo")%>'>
                                                                            </asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" CommandName='<%# Eval("ItemCode") +","+ Eval("SerialNo")%>'
                                                                                OnCommand="GridSerialNoDeleteClick" CausesValidation="true"
                                                                                OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>

                                                        <asp:HiddenField ID="hfSerialSAPNo" runat="server" />
                                                        <asp:HiddenField ID="hfSerialItemCode" runat="server" />
                                                        <asp:HiddenField ID="hfSerialItemName" runat="server" />
                                                        <asp:HiddenField ID="hfSerialUOM" runat="server" />
                                                        <asp:HiddenField ID="hfSerialReceivedQty" runat="server" />
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>


                                        <asp:Panel ID="pnlBatchNo" runat="server" class="panel panel-success" Visible="false">
                                            <div class="panel-body">

                                                <div class="row" style="padding-left: 1%; padding-bottom: 1%">
                                                    <h3 class="box-title" style="color: green;"><span>Batch Number Details</span></h3>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-2">
                                                        <label for="exampleInputName">Batch No</label>
                                                        <asp:TextBox runat="server" class="form-control" ID="txtBatchNo" autocomplete="off" />
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label for="exampleInputName">Qty(Weight)</label>
                                                        <asp:TextBox runat="server" class="form-control" ID="txtBatchQty" autocomplete="off" />
                                                    </div>

                                                    <div class="col-md-2" runat="server" style="padding-top: 2%">
                                                        <asp:Button runat="server" class="btn btn-success" ID="btnAddBatchNo" Text="Add" OnClick="btnAddBatchNo_Click" />
                                                    </div>
                                                </div>

                                                <div class="row" runat="server" style="padding-top: 1%">
                                                    <asp:Panel ID="pnlBatchDetRpter" class="panel panel-success" runat="server"
                                                        Height="400px" Style="overflow-x: scroll; overflow-y: scroll" BorderStyle="None">
                                                        <div class="col-md-12">
                                                            <asp:Repeater ID="rptBatchNoDet" runat="server" EnableViewState="true">
                                                                <HeaderTemplate>
                                                                    <table id="tblBatchNoDet" class="table table-hover table-bordered table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S.No</th>
                                                                                <th>SapNo</th>
                                                                                <th runat="server" visible="false">ItemCode</th>
                                                                                <th>ItemName</th>
                                                                                <th>UOM</th>
                                                                                <th>BatchNo_No</th>
                                                                                <th>Qty(Weight)</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Container.ItemIndex + 1 %></td>

                                                                        <td>
                                                                            <asp:Label ID="lblBatSAPNo" Style="width: 100%; border: none"
                                                                                runat="server" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label ID="lblBatItemCode" Style="width: 100%; border: none"
                                                                                runat="server" Text='<%# Eval("ItemCode")%>'>
                                                                            </asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblBatItemName" runat="server" Style="width: 100%; border: none"
                                                                                Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblBatUOM" runat="server" Style="width: 100%; border: none"
                                                                                Text='<%# Eval("UOM")%>'>
                                                                            </asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblBatchNo" runat="server" Style="width: 100%; border: none"
                                                                                Text='<%# Eval("BatchNo")%>'>
                                                                            </asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblBatchQty" runat="server" Style="width: 100%; border: none"
                                                                                Text='<%# Eval("Qty")%>'>
                                                                            </asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" CommandName='<%# Eval("ItemCode") +","+ Eval("BatchNo")%>'
                                                                                OnCommand="GridSerialNoDeleteClick" CausesValidation="true"
                                                                                OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>

                                                        <asp:HiddenField ID="hfBatchSAPNo" runat="server" />
                                                        <asp:HiddenField ID="hfBatchItemCode" runat="server" />
                                                        <asp:HiddenField ID="hfBatchItemName" runat="server" />
                                                        <asp:HiddenField ID="hfBatchUOM" runat="server" />
                                                        <asp:HiddenField ID="hfBatchQty" runat="server" />
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2" runat="server">
                                            <label for="exampleInputName">Total Qty</label>
                                            <asp:Label ID="txtTotQty" runat="server" class="form-control"></asp:Label>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <label for="exampleInputName">Total_Item_Amount</label>
                                            <asp:Label ID="txtTotAmt" runat="server" class="form-control"></asp:Label>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <label for="exampleInputName">Total Discount</label>
                                            <asp:Label ID="lblTotDisc" runat="server" class="form-control"></asp:Label>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <label for="exampleInputName">Total CGST</label>
                                            <asp:Label ID="lblTotCGST" runat="server" class="form-control"></asp:Label>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <label for="exampleInputName">Total SGST</label>
                                            <asp:Label ID="lblTotSGST" runat="server" class="form-control"></asp:Label>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <label for="exampleInputName">Total IGST</label>
                                            <asp:Label ID="lblTotIGST" runat="server" class="form-control"></asp:Label>
                                        </div>

                                    </div>

                                    <div class="row" runat="server" style="padding-top: 1%">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Pack & Forward</label>
                                                <asp:CheckBox ID="chkPFGst" BorderStyle="None" Text="GST" runat="server"
                                                    AutoPostBack="true" OnCheckedChanged="chkPFGst_CheckedChanged" />
                                                <asp:TextBox ID="txtPackFrwdCharge" class="form-control" runat="server" Text="0.00"
                                                    AutoPostBack="true" OnTextChanged="txtPackFrwdCharge_TextChanged">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Transport_Charges</label>
                                            <asp:CheckBox ID="chkTCGst" BorderStyle="None" Text="GST" runat="server"
                                                AutoPostBack="true" OnCheckedChanged="chkTCGst_CheckedChanged" />
                                            <asp:TextBox ID="txtTransCharge" class="form-control" runat="server" Text="0.00"
                                                AutoPostBack="true" OnTextChanged="txtTransCharge_TextChanged">
                                            </asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Courier_Charges</label>
                                                <asp:CheckBox ID="chkCCGst" BorderStyle="None" Text="GST" runat="server"
                                                    AutoPostBack="true" OnCheckedChanged="chkCCGst_CheckedChanged" />
                                                <asp:TextBox ID="txtCourierCharge" class="form-control" runat="server" Text="0.00"
                                                    AutoPostBack="true" OnTextChanged="txtCourierCharge_TextChanged">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Custom Duty Charges</label>
                                                <asp:CheckBox ID="chkOther1Gst" BorderStyle="None" Text="GST" runat="server"
                                                    AutoPostBack="true" OnCheckedChanged="chkOther1Gst_CheckedChanged" />
                                                <asp:TextBox ID="txtOther1Amt" class="form-control" runat="server" Text="0.00"
                                                    AutoPostBack="true" OnTextChanged="txtOther1Amt_TextChanged">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Duty & WH Charges</label>
                                                <asp:CheckBox ID="chkOther2Gst" BorderStyle="None" Text="GST" runat="server"
                                                    AutoPostBack="true" OnCheckedChanged="chkOther2Gst_CheckedChanged" />
                                                <asp:TextBox ID="txtOther2Amt" class="form-control" runat="server" Text="0.00"
                                                    AutoPostBack="true" OnTextChanged="txtOther2Amt_TextChanged">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <label for="exampleInputName">Total Line Total</label>
                                            <asp:Label ID="lblTotLineAmt" runat="server" class="form-control"></asp:Label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8"></div>
                                        <div class="col-md-2">
                                            <label for="exampleInputName">Round Off</label>
                                            <asp:TextBox ID="txtRoundOff" runat="server" class="form-control" Text="0.00" AutoPostBack="true"
                                                OnTextChanged="txtRoundOff_TextChanged"></asp:TextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label for="exampleInputName">Net Amount</label>
                                            <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field"
                                            OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />

                                    </div>
                                </div>
                                <!-- /.box-footer-->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>

        <!-- /.content -->
    </div>

    <script type="text/javascript" src="../assets/js/Trans_Receipt_Calc.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#tbltest").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": true
            });

            $("#tblSerialNoDet").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": true
            });

            $("#tblBatchNoDet").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": true
            });

        });
    </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.select2').select2();
                    $("#tbltest").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "ordering": false,
                        "searching": true
                    });
                    $("#tblSerialNoDet").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "ordering": false,
                        "searching": true
                    });

                    $("#tblBatchNoDet").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "ordering": false,
                        "searching": true
                    });
                }
            });
        };
    </script>

</asp:Content>

