﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BOM_Revision.aspx.cs" Inherits="Master_BOM_Revision" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
    </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <asp:UpdatePanel ID="upGenModelsub" runat="server">
        <ContentTemplate>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-sun-o text-primary"></i>Material Planning Details
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2" runat="server">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Order No</label>
                                    <asp:DropDownList ID="txtOrder_No" runat="server" class="form-control select2" OnSelectedIndexChanged="txtOrder_No_SelectedIndexChanged"
                                           AutoPostBack="true">
                                        </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Generator Model</label>
                                    <asp:Label ID="txtGenerator_Model" class="form-control" runat="server"></asp:Label>                                    
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Select BOM Revision</label>
                                    <asp:DropDownList ID="txtBom_Select" runat="server" class="form-control select2"  OnSelectedIndexChanged="txtBom_Select_SelectedIndexChanged"
                                           AutoPostBack="true" Enabled="false">
                                        </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Revision Type</label>
                                    <asp:RadioButtonList ID="RdbRevision_Type" RepeatColumns="2" runat="server" OnSelectedIndexChanged="RdbRevision_Type_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="New" Value="1" style="padding: 20px"></asp:ListItem>
                                        <asp:ListItem Selected="True" Text="Existing BOM" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Revision Name</label>
                                    <asp:TextBox ID="txtRevision_Name" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Delivery Date</label>
                                    <asp:Label ID="txtDelivery_Date" class="form-control" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Delivery CW</label>
                                    <asp:Label ID="txtDelivery_CW" class="form-control" runat="server"></asp:Label>                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-primary">

                                        <div class="box-header with-border">
                                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>BOM Details</span></h3>
                                            
                                        </div>

                                        <div class="box-body no-padding">
                                            <div class="table-responsive mailbox-messages">                                                
                                                <div class="row" runat="server">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Parts Name</label>
					                                        <asp:DropDownList ID="ddlPartName" runat="server" class="form-control select2" 
                                                                onselectedindexchanged="ddlPartName_SelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="ddlPartName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Sap No</label>
					                                        <asp:DropDownList ID="txtSapNo" runat="server" class="form-control select2" 
                                                                onselectedindexchanged="txtSapNo_SelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="ddlPartName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">BOM ID</label>
					                                        <asp:DropDownList ID="txtBOM_ID" runat="server" class="form-control select2" 
                                                                onselectedindexchanged="txtBOM_ID_SelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="ddlPartName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Item Name</label>
                                                            <asp:Label ID="txtItem_Name" class="form-control" runat="server"></asp:Label>	
                                                            <asp:HiddenField runat="server" ID="txtUOM_Hide" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" runat="server">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="control-label">Required Qty</label>
                                                            <asp:TextBox ID="txtRequired_Qty" class="form-control" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-2" style="padding-top:5px" >
					                                    <br />
					                                    <asp:Button ID="btnAddItem" class="btn btn-primary"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click"/>
					                                </div>
                                                </div>

                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                                        <HeaderTemplate>
                                                        <table  class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Item_ID</th>
                                                                   <th>Part Name</th>
                                                                    <th>Production Stage</th>
                                                                    <th>Sap No</th>
                                                                    <th>Item Descrition</th>
                                                                    <th>UOM</th>
                                                                    <th>Required Qty</th>
                                                                    <th>Model</th>
                                                                    </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Eval("Mat_No")%></td>
                                                           
                                                            <td><%# Eval("PartType")%></td>
                                                            <td><%# Eval("Production_Steps")%></td>
                                                            <td><%# Eval("Sap_No")%></td>
                                                            <td><%# Eval("Raw_Mat_Name")%></td>
                                                            <td><%# Eval("UOM_Full")%></td>
                                                          <td><%# Eval("RequiredQty")%></td>
                                                            <td>
                                                                  <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("Mat_No")%>'>
                                                                    </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                    Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("Mat_No")%>' 
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');"> </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>                                
			                                    </asp:Repeater>
					                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                  <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" ValidationGroup="Validate_Field" OnClick="btnSave_Click" runat="server" Text="Save" />
                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                        </div>
                  
                        
                </div>
            </div>
              <div class="box-footer">
                       
                    </div>
            <div class="col-md-3" hidden>
                <div class="callout callout-info" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-info"></i>Info:</h4>
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    <!-- /.content -->
        </div>

     <!-- iCheck -->
    <script src="assets/adminlte/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('.checkbox').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>

</asp:Content>

