﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_Trans_MRPProcess_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: MRP Process";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_Enquiry_Grid();
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Request");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add New Purchase Request...');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Request..');", true);
        }
        else
        {
            Session.Remove("WONo");
            Response.Redirect("Trans_MRPProcess_Sub.aspx");
        }

        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Pur_Request_No_Amend_Approval");
        //Session.Remove("Pur_Request_No");
       // Response.Redirect("Trans_MRPProcess_Sub.aspx");
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        string RptName = "";
        string Arguments = "";
        string PurRquNo = "";
        string MatType = "";

        bool Rights_Check = false;
        bool ErrFlag = false;


        Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Request");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit Purchase Request...');", true);
        }

        if (!ErrFlag)
        {
            RptName = "Escrow Purchase Request";
            Arguments = e.CommandName.ToString();

            string[] A = Arguments.ToString().Split(',');

            PurRquNo = A[0].ToString();
            MatType = A[1].ToString();

            ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?PurRquNo=" + PurRquNo + "&MatType=" + MatType + "&RptName=" + RptName, "_blank", "");
        }

    }

    protected void GridEditOrderFormatClick(object sender, CommandEventArgs e)
    {
        string RptName = "";
        string StdPurOrdNo = "";
        string WorkOrderNo = "";
        string SupQtnNo = "";
        string DeptName = "";
        bool ErrFlag = false;

        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Coral Purchase Order");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Print Purchase Order...');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Std Purchase Order..');", true);
        }
        else
        {
            RptName = "Work Order Format";
            WorkOrderNo = e.CommandName.ToString();
            ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&Work_Order_No=" + WorkOrderNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");
        }
    }

    protected void GridEditOrderDetailClick(object sender, CommandEventArgs e)
    {
        string RptName = "";
        string StdPurOrdNo = "";
        string WorkOrderNo = "";
        string SupQtnNo = "";
        string DeptName = "";
        bool ErrFlag = false;

        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Coral Purchase Order");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Print Purchase Order...');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Std Purchase Order..');", true);
        }
        else
        {
            RptName = "Work Order Detailed Format";
            WorkOrderNo = e.CommandName.ToString();
            ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&Work_Order_No=" + WorkOrderNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");
        }
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool Rights_Check = false;
        bool ErrFlag = false;

        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
       // bool ErrFlag = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Request");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit Purchase Request...');", true);
        }
        else
        {
            string WorkOrder_No = e.CommandName.ToString();
            // Approve Check
            SSQL = "Select * from Trans_GenerateWorkOrder_Main Where Work_Order_No='" + WorkOrder_No + "' And ApproveStatus='1' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Work Order Already Approved');", true);

            }
            else
            {


                Session["WONo"] = WorkOrder_No;

                Response.Redirect("Trans_WorkOrder_Approve.aspx");
            }
            //string status = dtdpurchase.Rows[0]["Status"].ToString();

            //if (status == "" || status == "0")
            //{
            //    string Enquiry_No_Str = e.CommandName.ToString();
            //    Session["WONo"] = Enquiry_No_Str;
            //    EditEnqAsp.Value = Enquiry_No_Str;
            //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "send();", true);
            //    Response.Redirect("Trans_MRPProcess_Sub.aspx");
            //}
            //else if (status == "2")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Already Rejected..');", true);
            //    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Rejected..');", true);
            //}
            //else if (status == "3")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details put in pending..');", true);
            //    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details put in pending..');", true);
            //}
            //else if (status == "1")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Approved Purchase Request Cant Edit..');", true);
            //    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Purchase Request Cant Edit..');", true);
            //}
        }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Request");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Purchase Request..');", true);
        }
        //User Rights Check End

        DataTable DT = new DataTable();
        query = "Select * from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //Delete Main Table
            query = "Delete from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //Delete Main Sub Table
            query = "Delete from Trans_MRPProcess_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MRP Process Details Deleted Successfully');", true);

            Load_Data_Enquiry_Grid();
        }


        //Check With Already Approved Start
        //DataTable DT_Check = new DataTable();
        //query = "Select * from Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        //query = query + " And FinYearCode='" + SessionFinYearCode + "' And Work_Order_No='" + e.CommandName.ToString() + "' And Status='1'";

        //DT_Check = objdata.RptEmployeeMultipleDetails(query);
        //if (DT_Check.Rows.Count != 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Do not Delete Purchase Request Details Already Approved..');", true);
        //}
        ////Check With Already Approved End

        //if (!ErrFlag)
        //{
        //    DataTable dtdpurchase = new DataTable();

        //    query = "select Status from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        //    query = query + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";

        //    dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
        //    string status = dtdpurchase.Rows[0]["Status"].ToString();

        //    if (status == "" || status == "0")
        //    {
        //        DataTable DT = new DataTable();
        //        query = "Select * from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        //        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
        //        DT = objdata.RptEmployeeMultipleDetails(query);
        //        if (DT.Rows.Count != 0)
        //        {
        //            //Delete Main Table
        //            query = "Delete from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        //            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
        //            objdata.RptEmployeeMultipleDetails(query);

        //            //Delete Main Sub Table
        //            query = "Delete from Trans_Escrow_PurRqu_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
        //            objdata.RptEmployeeMultipleDetails(query);

        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Deleted Successfully');", true);

        //            Load_Data_Enquiry_Grid();
        //        }
        //    }
        //    else if (status == "2")
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Already Rejected..');", true);

        //    }
        //    else if (status == "3")
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details put in pending..');", true);
        //    }
        //}
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Work_Order_No,Order_No,GenModel,Part_Name,Machine_Specification,DeliveryReqBy,PartBOMRevisionStatus";
        query = query + " From Trans_MRPProcess_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    //protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
    //    {
    //        RepeaterItem Item = e.Item;

    //        Label lblStatus = Item.FindControl("lblStatus") as Label;

    //        if (lblStatus.Text == "Successfully")
    //        {
    //            lblStatus.BackColor = Color.Green;
    //            lblStatus.ForeColor = Color.White;

    //        }
    //        else
    //        {
    //            lblStatus.BackColor = Color.Red;
    //            lblStatus.ForeColor = Color.White;
    //        }
    //    }
    //}
}