﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_Trans_WorkOrder_Approve : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionWONo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal RetQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Work Order Approve";
            //Initial_Data_Referesh();
            //Load_Data_OrderNo();
            //Load_Data_PartName();
            if (Session["WONo"] == null)
            {
                //SessionWONo = "";

                //TransactionNoGenerate TransNO = new TransactionNoGenerate();
                //string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "MRP Process", SessionFinYearVal, "1");

                //txtworkorder.Text = Auto_Transaction_No;
                ////txtTransDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            }
            else
            {
                SessionWONo = Session["WONo"].ToString();
                txtworkorder.Text = SessionWONo;
                Load_Data_OrderNo();
                Load_Data_PartName();
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }
    protected void Load_Data_OrderNo()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select WorkOrder_OrderNo from Trans_GenerateWorkOrder_Main Where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        //SSQL = SSQL + " Status='0'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlorderNo.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["WorkOrder_OrderNo"] = "-Select-";
        dr["WorkOrder_OrderNo"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlorderNo.DataTextField = "WorkOrder_OrderNo";
        ddlorderNo.DataValueField = "WorkOrder_OrderNo";
        ddlorderNo.DataBind();
    }
    protected void Load_Data_OrderNo1()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select WorkOrder_OrderNo from Trans_GenerateWorkOrder_Main Where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Status='0'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlorderNo.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["WorkOrder_OrderNo"] = "-Select-";
        dr["WorkOrder_OrderNo"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlorderNo.DataTextField = "WorkOrder_OrderNo";
        ddlorderNo.DataValueField = "WorkOrder_OrderNo";
        ddlorderNo.DataBind();
    }
    protected void Load_Data_PartName()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select distinct PartType from Trans_GenerateWorkOrder_Main Where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";


        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlPartName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["PartType"] = "-Select-";
        dr["PartType"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlPartName.DataTextField = "PartType";
        ddlPartName.DataValueField = "PartType";
        ddlPartName.DataBind();
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;
        string ReqQty = "";
        string StockQty = "";
        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["MRPProcess"];
        if (DT_Check.Rows.Count != 0)
        {
            for (int i = 0; i < DT_Check.Rows.Count; i++)
            {
                if (DT_Check.Rows[i]["PR_Qty"].ToString() == "0")
                {
                    continue;

                }
                else
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Cannot Approve Now..');", true);
                }


            }

            //Check Approval Status 
            DataTable DT_Approve = new DataTable();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();


            for (int i = 0; i < DT_Check.Rows.Count; i++)
            {
                //Reserve Qty Update
                //SSQL = "Update Trans_GenerateWorkOrder_Sub set Reserve_Qty='" + DT_Check.Rows[i]["Req_Qty"] + "' Where Sap_No='" + DT_Check.Rows[i]["Sap_No"] + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
                //objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "Select * From BOMMaster Where Sap_No='" + DT_Check.Rows[i]["Sap_No"] + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Select * From Trans_GenerateWorkOrder_Sub Where Work_Order_No='" + txtworkorder.Text + "' And Sap_No='" + DT_Check.Rows[i]["Sap_No"] + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);


                //Item Value Update
                string Qty = "";
                string Value = "";
                string ItemRate = "";
                string RequQty = "";
                string Rate = "";
                SSQL = "select sum(Add_Qty) as Qty,Sum(Add_Value) as Value from Trans_Stock_Ledger_All ";
                SSQL = SSQL + " where ReceiveType='Direct' or ReceiveType='Restricted' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "'";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt2.Rows.Count != 0)
                {
                    Qty = dt2.Rows[0]["Qty"].ToString();
                    Value = dt2.Rows[0]["Value"].ToString();
                    ItemRate = (Convert.ToDecimal(Value) / Convert.ToDecimal(Qty)).ToString();

                   
                }

                RequQty = DT_Check.Rows[i]["Req_Qty"].ToString();
                Rate = (Convert.ToDecimal(RequQty) * Convert.ToDecimal(ItemRate)).ToString();

                SSQL = "Update Trans_GenerateWorkOrder_Sub set Item_Rate='" + Rate + "' where Work_Order_No='" + txtworkorder.Text + "' And Mat_No='" + dt1.Rows[0]["Mat_No"] + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
                objdata.RptEmployeeMultipleDetails(SSQL);

                //Stock(Add Qty)
                SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                SSQL = SSQL + " Trans_Type,Supp_Type,Mat_Type,Supp_Code,Supp_Name,SapNo,ItemCode,ItemName,DeptCode,DeptName,UOM,";
                SSQL = SSQL + " WarehouseCode,WarehouseName,Add_Qty,Add_Value,Minus_Qty,Minus_Value,UserID,UserName,CurrencyType,ReceiveType)";
                SSQL = SSQL + " Values( '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                SSQL = SSQL + " '" + SessionFinYearVal + "', '" + txtworkorder.Text + "','" + dt1.Rows[0]["Work_Order_Date"].ToString() + "','" + dt1.Rows[0]["Work_Order_Date"].ToString() + "',";
                SSQL = SSQL + " 'Work Order','2','1', 'Coral','Coral',";
                SSQL = SSQL + " '" + DT_Check.Rows[i]["Sap_No"].ToString() + "','" + dt1.Rows[0]["Mat_No"].ToString() + "',";
                SSQL = SSQL + " '" + DT_Check.Rows[i]["Item_Desc"].ToString() + "','" + dt.Rows[0]["DeptCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[0]["DeptName"].ToString() + "','" + dt1.Rows[0]["UOM"].ToString() + "',";
                SSQL = SSQL + " '','',";
                SSQL = SSQL + " '" + dt1.Rows[0]["Req_Qty"].ToString() + "','" + Rate + "',";
                SSQL = SSQL + " '0.00','0.00','" + SessionUserID + "','" + SessionUserName + "','INR','Requisition Stock')";

                objdata.RptEmployeeMultipleDetails(SSQL);


                // Stock(Minus Qty)
                SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                SSQL = SSQL + " Trans_Type,Supp_Type,Mat_Type,Supp_Code,Supp_Name,SapNo,ItemCode,ItemName,DeptCode,DeptName,UOM,";
                SSQL = SSQL + " WarehouseCode,WarehouseName,Add_Qty,Add_Value,Minus_Qty,Minus_Value,UserID,UserName,CurrencyType,ReceiveType)";
                SSQL = SSQL + " Values( '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                SSQL = SSQL + " '" + SessionFinYearVal + "', '" + txtworkorder.Text + "','" + dt1.Rows[0]["Work_Order_Date"].ToString() + "','" + dt1.Rows[0]["Work_Order_Date"].ToString() + "',";
                SSQL = SSQL + " 'Work Order','2','1', 'Coral','Coral',";
                SSQL = SSQL + " '" + DT_Check.Rows[i]["Sap_No"].ToString() + "','" + dt1.Rows[0]["Mat_No"].ToString() + "',";
                SSQL = SSQL + " '" + DT_Check.Rows[i]["Item_Desc"].ToString() + "','" + dt.Rows[0]["DeptCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[0]["DeptName"].ToString() + "','" + dt1.Rows[0]["UOM"].ToString() + "',";
                SSQL = SSQL + " '','',";
                SSQL = SSQL + " '0.00','0.00','" + dt1.Rows[0]["Req_Qty"].ToString() + "',";
                SSQL = SSQL + " '0.00','" + SessionUserID + "','" + SessionUserName + "','INR','Direct')";


                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (!ErrFlag)
            {
                //Approval Status Update
                SSQL = "Update Trans_GenerateWorkOrder_Main set ApproveStatus='1',ApproveBy='" + SessionUserName + "' Where Work_Order_No='" + txtworkorder.Text + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Approved Successfully..');", true);
                txtworkorder.Text = "";
                ddlorderNo.SelectedValue = "-Select-"; txtgenModel.Text = "";
                ddlPartName.SelectedValue = "-Select-"; txtMachineSpec.Text = "";
                txtdelivery.Text = ""; txtPartBOM.Text = "";
                Initial_Data_Referesh();
            }

        }



    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //Clear_All_Field();
        Session.Remove("WONo");
        Response.Redirect("Trans_MRPProcess_Main.aspx");
    }

    private void Clear_All_Field()
    {
        //txtworkorder.Text = "";
        //ddlorderNo.SelectedValue = "-Select-"; txtgenModel.Text = "";
        //ddlPartName.SelectedValue = "-Select-"; txtMachineSpec.Text = "";
        //txtdelivery.Text = ""; txtPartBOM.Text = "";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        //// btnSave.Text = "Save";
        // Initial_Data_Referesh();
        // Session.Remove("WONo");
        //Load_Data_Enquiry_Grid();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["MRPProcess"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["Sap_No"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["MRPProcess"] = dt;
        Load_OLD_data();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["MRPProcess"];
        rptGenPartType.DataSource = dt;
        rptGenPartType.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

        string SSQL = "";
        DataTable Main_DT = new DataTable();
        DataTable dt = new DataTable();

        //SSQL = "Select * from Trans_GenerateWorkOrder_Main A inner join Trans_GenerateWorkOrder_Sub B on A.Work_Order_No=B.Work_Order_No And A.Lcode=B.Lcode where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " A.FinYearCode ='" + SessionFinYearCode + "' And A.Work_Order_No ='" + txtworkorder.Text + "'";
        //Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (Main_DT.Rows.Count != 0)
        //{
        //    txtworkorder.Text = Main_DT.Rows[0]["Work_Order_No"].ToString();
        //    ddlorderNo.SelectedValue = Main_DT.Rows[0]["WorkOrder_OrderNo"].ToString();
        //    txtgenModel.Text = Main_DT.Rows[0]["GenModelName"].ToString();

        //    ddlPartName.SelectedValue = Main_DT.Rows[0]["PartType"].ToString();
        //    txtMachineSpec.Text = Main_DT.Rows[0]["Machine_Specification"].ToString();
        //    txtdelivery.Text = Main_DT.Rows[0]["DeliveryReqBy"].ToString();
        //    txtPartBOM.Text = Main_DT.Rows[0]["PartBOMRevisionStatus"].ToString();


        //    SSQL = " Select BOM_ID as BOI_ID,Production_Stage,Sap_No,Item_Desc,Req_Qty,Stock_Qty, PR_Qty,Mat_No,UOM,Req_Date,BOM_ID";
        //    SSQL = SSQL + " from Trans_GenerateWorkOrder_sub where Ccode='" + SessionCcode + "' and ";
        //    SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' And ";
        //    SSQL = SSQL + " Work_Order_No='" + txtworkorder.Text + "' ";

        //    Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //    rptGenPartType.DataSource = Main_DT;
        //    rptGenPartType.DataBind();
        //    ViewState["MRPProcess"] = Main_DT;



        //    // btnSave.Text = "Update";
        //}
        //else
        //{
        //    Clear_All_Field();
        //}

        //SSQL = "Select * from Trans_GenerateWorkOrder_Main A inner join Trans_GenerateWorkOrder_Sub B on A.Work_Order_No=B.Work_Order_No And A.Lcode=B.Lcode where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " A.FinYearCode ='" + SessionFinYearCode + "' And A.Work_Order_No ='" + txtworkorder.Text + "'";
        //Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (Main_DT.Rows.Count != 0)
        //{
        //    txtworkorder.Text = Main_DT.Rows[0]["Work_Order_No"].ToString();
        //    ddlorderNo.SelectedValue = Main_DT.Rows[0]["WorkOrder_OrderNo"].ToString();
        //    txtgenModel.Text = Main_DT.Rows[0]["GenModelName"].ToString();

        //    ddlPartName.SelectedValue = Main_DT.Rows[0]["PartType"].ToString();
        //}
            Load_Data_DataTable1();
    }
    private void Load_Data_DataTable1()
    {
        DataTable dt_BOM = new DataTable();
        DataTable dt = new DataTable();
        string ReqDate = "";
        DataTable dtLeadDays = new DataTable();
        DataTable dtReqDate = new DataTable();
        DataTable qry_dt = new DataTable();
        DataTable qry_dt1 = new DataTable();
        DataTable qry_dt2 = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

       // string SSQL = "";
        DataTable Main_DT = new DataTable();
        //DataTable dt = new DataTable();
        Initial_Data_Referesh();

        //Load_OLD_data();

        //SSQL = SSQL + " Select * from Trans_GenerateWorkOrder_Main where Ccode='" + SessionCcode + "' and ";
        //SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' And ";
        //SSQL = SSQL + " Work_Order_No='" + txtworkorder.Text + "' and PartType='" + ddlPartName.SelectedValue + "'";

        //qry_dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Trans_GenerateWorkOrder_Main A inner join Trans_GenerateWorkOrder_Sub B on A.Work_Order_No=B.Work_Order_No And A.Lcode=B.Lcode where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " A.FinYearCode ='" + SessionFinYearCode + "' And A.Work_Order_No ='" + txtworkorder.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtworkorder.Text = Main_DT.Rows[0]["Work_Order_No"].ToString();
            ddlorderNo.SelectedValue = Main_DT.Rows[0]["WorkOrder_OrderNo"].ToString();
            txtgenModel.Text = Main_DT.Rows[0]["GenModelName"].ToString();

            ddlPartName.SelectedValue = Main_DT.Rows[0]["PartType"].ToString();
        }

        SSQL = SSQL + " Select * from Trans_GenerateWorkOrder_Main A Inner Join Trans_GenerateWorkOrder_Sub B on A.Work_Order_No=B.Work_Order_No where A.Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " A.LCode ='" + SessionLcode + "' and A.FinYearCode='" + SessionFinYearCode + "' And ";
        SSQL = SSQL + " A.Work_Order_No='" + txtworkorder.Text + "' and A.PartType='" + ddlPartName.SelectedValue + "'";

        qry_dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        string Stock = "";
        string PRQty = "";
        string ReqQty = "";
        for (int i = 0; i < qry_dt1.Rows.Count; i++)
        {

            //BOM ID
            SSQL = " select B.BOMid from Trans_GenerateWorkOrder_Sub A ";
            SSQL = SSQL + " Inner Join BOMMaster B on A.Mat_No=B.Mat_No and a.Sap_No=b.Sap_No  where A.Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " A.LCode ='" + SessionLcode + "' And ";
            SSQL = SSQL + " A.sap_no='" + qry_dt1.Rows[i]["Sap_no"] + "' and A.Mat_No='" + qry_dt1.Rows[i]["Mat_No"] + "'";

            dt_BOM = objdata.RptEmployeeMultipleDetails(SSQL);

            // Stock Qty
            SSQL = " Select isnull(Sum(Add_Qty)-Sum(Minus_qty),0) as Stock";
            SSQL = SSQL + " from Trans_Stock_Ledger_All where Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' And ";
            SSQL = SSQL + " sapno='" + qry_dt1.Rows[i]["Sap_no"] + "' and ItemCode='" + qry_dt1.Rows[i]["Mat_No"] + "' and ReceiveType='Direct'";

            qry_dt2 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (qry_dt2.Rows.Count != 0)
            {
                Stock = qry_dt2.Rows[0]["Stock"].ToString();
            }
            else
            {
                Stock = "0";
            }

            ReqQty = qry_dt1.Rows[i]["Req_Qty"].ToString();
            PRQty = (Convert.ToDecimal(ReqQty) - Convert.ToDecimal(Stock)).ToString();
            if (Convert.ToDecimal(PRQty) > 0)
            {
                PRQty = PRQty;
            }
            else
            {
                PRQty = "0";
            }
            string ProdStep = qry_dt1.Rows[i]["Production_Stage"].ToString();
            string Sapno = qry_dt1.Rows[i]["Sap_No"].ToString();
            string ItemName = qry_dt1.Rows[i]["Item_Desc"].ToString();
            string Reqty = qry_dt1.Rows[i]["Req_Qty"].ToString();
            string MatNo = qry_dt1.Rows[i]["Mat_No"].ToString();
            string UOM = qry_dt1.Rows[i]["UOM"].ToString();
            string BOM_ID = qry_dt1.Rows[0]["BOM_ID"].ToString();

            // Lead Days

            SSQL = "Select LeadDays from BOMMaster where Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " LCode ='" + SessionLcode + "' And ";
            SSQL = SSQL + " sap_no='" + qry_dt1.Rows[i]["Sap_no"] + "' and Mat_No='" + qry_dt1.Rows[i]["Mat_No"] + "'";

            dtLeadDays = objdata.RptEmployeeMultipleDetails(SSQL);
            string LeadDays = dtLeadDays.Rows[0][0].ToString();

            SSQL = " select DATEADD(day," + LeadDays + ",GETDATE()) ";
            dtReqDate = objdata.RptEmployeeMultipleDetails(SSQL);

            ReqDate = Convert.ToDateTime(dtReqDate.Rows[0][0]).ToString("dd/MM/yyyy");

            //if (Convert.ToDecimal(PRQty) > 0)
            //{
            if (ViewState["MRPProcess"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["MRPProcess"];


                dr = dt.NewRow();

                dr["Production_Stage"] = ProdStep;
                dr["SAP_No"] = Sapno;
                dr["Item_Desc"] = ItemName;
                dr["Req_Qty"] = Reqty;
                dr["Stock_Qty"] = Stock;
                dr["PR_Qty"] = PRQty;
                dr["Mat_No"] = MatNo;
                dr["UOM"] = UOM;
                dr["Req_Date"] = ReqDate;
                dr["BOM_ID"] = BOM_ID;
                dt.Rows.Add(dr);
                ViewState["MRPProcess"] = dt;
                rptGenPartType.DataSource = dt;
                rptGenPartType.DataBind();
            }
            else
            {

                dt = (DataTable)ViewState["MRPProcess"];


                dr = dt.NewRow();

                dr["Production_Stage"] = ProdStep;
                dr["SAP_No"] = Sapno;
                dr["Item_Desc"] = ItemName;
                dr["Req_Qty"] = Reqty;
                dr["Stock_Qty"] = Stock;
                dr["PR_Qty"] = PRQty;
                dr["Mat_No"] = MatNo;
                dr["UOM"] = UOM;
                dr["Req_Date"] = ReqDate;
                dr["BOM_ID"] = BOM_ID;
                dt.Rows.Add(dr);
                ViewState["MRPProcess"] = dt;
                rptGenPartType.DataSource = dt;
                rptGenPartType.DataBind();
            }

            //}

        }


    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_MRPProcess_Main.aspx");
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Production_Stage", typeof(string)));
        dt.Columns.Add(new DataColumn("Sap_No", typeof(string)));
        dt.Columns.Add(new DataColumn("Item_Desc", typeof(string)));
        dt.Columns.Add(new DataColumn("Req_Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("Stock_Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("PR_Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("Mat_No", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM", typeof(string)));
        dt.Columns.Add(new DataColumn("Req_Date", typeof(string)));
        dt.Columns.Add(new DataColumn("BOM_ID", typeof(string)));

        rptGenPartType.DataSource = dt;
        rptGenPartType.DataBind();
        ViewState["MRPProcess"] = rptGenPartType.DataSource;
    }

    protected void ddlPartName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_DataTable();
    }
    protected void ddlorderNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select GeneratorModel ";
        SSQL = SSQL + " From CORAL_ERP_Sales..Sales_Material_Plan_Conform_Main Where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LCode='" + SessionLcode + "' and Status='1' And Cus_Pur_Order_No='" + ddlorderNo.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            txtgenModel.Text = DT.Rows[0]["GeneratorModel"].ToString();
        }

        // Load_Data_DataTable();
    }

    private void Load_Data_DataTablePartName()
    {

        string ReqDate = "";
        DataTable dtLeadDays = new DataTable();
        DataTable dtReqDate = new DataTable();
        DataTable dt = new DataTable();
        DataTable dt_BOM = new DataTable();
        DataTable qry_dt = new DataTable();
        DataTable qry_dt1 = new DataTable();
        DataTable qry_dt2 = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";
        string BOM_ID = "";

        SSQL = SSQL + " Select * from [CORAL_ERP_Sales]..Sales_Material_Plan_Conform_Main_Sub where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' And ";
        SSQL = SSQL + " Cus_Pur_Order_No='" + ddlorderNo.SelectedItem.Text + "' and PartType='" + ddlPartName.SelectedItem.Text + "'";

        qry_dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        string Stock = "";
        string PRQty = "";
        string ReqQty = "";

        for (int i = 0; i < qry_dt1.Rows.Count; i++)
        {
            //BOM ID
            SSQL = " select B.BOMid from [CORAL_ERP_Sales]..Sales_Material_Plan_Conform_Main_Sub A ";
            SSQL = SSQL + " Inner Join BOMMaster B on A.Mat_No=B.Mat_No and a.Sap_No=b.Sap_No  where A.Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " A.LCode ='" + SessionLcode + "' And ";
            SSQL = SSQL + " A.sap_no='" + qry_dt1.Rows[i]["Sap_no"] + "' and A.Mat_No='" + qry_dt1.Rows[i]["Mat_No"] + "'";

            dt_BOM = objdata.RptEmployeeMultipleDetails(SSQL);

            // Stock Qty
            SSQL = " Select isnull(Sum(Add_Qty)-Sum(Minus_qty),0) as Stock";
            SSQL = SSQL + " from Trans_Stock_Ledger_All where Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' And ";
            SSQL = SSQL + " sapno='" + qry_dt1.Rows[i]["Sap_no"] + "' and ItemCode='" + qry_dt1.Rows[i]["Mat_No"] + "' and Mat_Type='1'";

            qry_dt2 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (qry_dt2.Rows.Count != 0)
            {
                Stock = qry_dt2.Rows[0]["Stock"].ToString();
            }
            else
            {
                Stock = "0";
            }
            ReqQty = qry_dt1.Rows[i]["RequiredQty"].ToString();
            PRQty = (Convert.ToDecimal(ReqQty) - Convert.ToDecimal(Stock)).ToString();
            if (Convert.ToDecimal(PRQty) > 0)
            {
                PRQty = PRQty;
            }
            else
            {
                PRQty = "0";
            }
            string ProdStep = qry_dt1.Rows[i]["Production_Steps"].ToString();
            string Sapno = qry_dt1.Rows[i]["Sap_No"].ToString();
            string ItemName = qry_dt1.Rows[i]["Raw_Mat_Name"].ToString();
            string Reqty = qry_dt1.Rows[i]["RequiredQty"].ToString();
            string MatNo = qry_dt1.Rows[i]["Mat_No"].ToString();
            string UOM = qry_dt1.Rows[i]["UOM_Full"].ToString();
            BOM_ID = dt_BOM.Rows[i]["BOMid"].ToString();
            dt = (DataTable)ViewState["MRPProcess"];

            // Lead Days

            SSQL = "Select LeadDays from BOMMaster where Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " LCode ='" + SessionLcode + "' And ";
            SSQL = SSQL + " sap_no='" + qry_dt1.Rows[i]["Sap_no"] + "' and Mat_No='" + qry_dt1.Rows[i]["Mat_No"] + "'";

            dtLeadDays = objdata.RptEmployeeMultipleDetails(SSQL);
            string LeadDays = dtLeadDays.Rows[0][0].ToString();

            SSQL = " select DATEADD(day," + LeadDays + ",GETDATE()) ";
            dtReqDate = objdata.RptEmployeeMultipleDetails(SSQL);

            ReqDate = Convert.ToDateTime(dtReqDate.Rows[0][0]).ToString("dd/MM/yyyy");
            if (Convert.ToDecimal(PRQty) > 0)
            {
                if (ViewState["MRPProcess"] != null)
                {
                    //get datatable from view state   


                    dr = dt.NewRow();

                    dr["Production_Stage"] = ProdStep;
                    dr["SAP_No"] = Sapno;
                    dr["Item_Desc"] = ItemName;
                    dr["Req_Qty"] = Reqty;
                    dr["Stock_Qty"] = Stock;
                    dr["PR_Qty"] = PRQty;
                    dr["Mat_No"] = MatNo;
                    dr["UOM"] = UOM;
                    dr["Req_Date"] = ReqDate;
                    dt.Rows.Add(dr);
                    ViewState["MRPProcess"] = dt;
                    rptGenPartType.DataSource = dt;
                    rptGenPartType.DataBind();
                }
                else
                {


                    dr = dt.NewRow();

                    dr["Production_Stage"] = ProdStep;
                    dr["SAP_No"] = Sapno;
                    dr["Item_Desc"] = ItemName;
                    dr["Req_Qty"] = Reqty;
                    dr["Stock_Qty"] = Stock;
                    dr["PR_Qty"] = PRQty;
                    dr["Mat_No"] = MatNo;
                    dr["UOM"] = UOM;
                    dr["Req_Date"] = ReqDate;
                    dt.Rows.Add(dr);
                    ViewState["MRPProcess"] = dt;
                    rptGenPartType.DataSource = dt;
                    rptGenPartType.DataBind();
                }
            }


        }


    }
    private void Load_Data_DataTable()
    {
        DataTable dt_BOM = new DataTable();
        DataTable dt = new DataTable();
        string ReqDate = "";
        DataTable dtLeadDays = new DataTable();
        DataTable dtReqDate = new DataTable();
        DataTable qry_dt = new DataTable();
        DataTable qry_dt1 = new DataTable();
        DataTable qry_dt2 = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

        Initial_Data_Referesh();

        //Load_OLD_data();

        SSQL = SSQL + " Select * from [CORAL_ERP_Sales]..Sales_Material_Plan_Conform_Main_Sub where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' And ";
        SSQL = SSQL + " Cus_Pur_Order_No='" + ddlorderNo.SelectedItem.Text + "' and PartType='" + ddlPartName.SelectedItem.Text + "'";

        qry_dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        string Stock = "";
        string PRQty = "";
        string ReqQty = "";
        for (int i = 0; i < qry_dt1.Rows.Count; i++)
        {

            //BOM ID
            SSQL = " select B.BOMid from [CORAL_ERP_Sales]..Sales_Material_Plan_Conform_Main_Sub A ";
            SSQL = SSQL + " Inner Join BOMMaster B on A.Mat_No=B.Mat_No and a.Sap_No=b.Sap_No  where A.Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " A.LCode ='" + SessionLcode + "' And ";
            SSQL = SSQL + " A.sap_no='" + qry_dt1.Rows[i]["Sap_no"] + "' and A.Mat_No='" + qry_dt1.Rows[i]["Mat_No"] + "'";

            dt_BOM = objdata.RptEmployeeMultipleDetails(SSQL);

            // Stock Qty
            SSQL = " Select isnull(Sum(Add_Qty)-Sum(Minus_qty),0) as Stock";
            SSQL = SSQL + " from Trans_Stock_Ledger_All where Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " LCode ='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' And ";
            SSQL = SSQL + " sapno='" + qry_dt1.Rows[i]["Sap_no"] + "' and ItemCode='" + qry_dt1.Rows[i]["Mat_No"] + "'";

            qry_dt2 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (qry_dt2.Rows.Count != 0)
            {
                Stock = qry_dt2.Rows[0]["Stock"].ToString();
            }
            else
            {
                Stock = "0";
            }

            ReqQty = qry_dt1.Rows[i]["RequiredQty"].ToString();
            PRQty = (Convert.ToDecimal(ReqQty) - Convert.ToDecimal(Stock)).ToString();
            if (Convert.ToDecimal(PRQty) > 0)
            {
                PRQty = PRQty;
            }
            else
            {
                PRQty = "0";
            }
            string ProdStep = qry_dt1.Rows[i]["Production_Steps"].ToString();
            string Sapno = qry_dt1.Rows[i]["Sap_No"].ToString();
            string ItemName = qry_dt1.Rows[i]["Raw_Mat_Name"].ToString();
            string Reqty = qry_dt1.Rows[i]["RequiredQty"].ToString();
            string MatNo = qry_dt1.Rows[i]["Mat_No"].ToString();
            string UOM = qry_dt1.Rows[i]["UOM_Full"].ToString();
            string BOM_ID = dt_BOM.Rows[0]["BOMid"].ToString();

            // Lead Days

            SSQL = "Select LeadDays from BOMMaster where Ccode='" + SessionCcode + "' and ";
            SSQL = SSQL + " LCode ='" + SessionLcode + "' And ";
            SSQL = SSQL + " sap_no='" + qry_dt1.Rows[i]["Sap_no"] + "' and Mat_No='" + qry_dt1.Rows[i]["Mat_No"] + "'";

            dtLeadDays = objdata.RptEmployeeMultipleDetails(SSQL);
            string LeadDays = dtLeadDays.Rows[0][0].ToString();

            SSQL = " select DATEADD(day," + LeadDays + ",GETDATE()) ";
            dtReqDate = objdata.RptEmployeeMultipleDetails(SSQL);

            ReqDate = Convert.ToDateTime(dtReqDate.Rows[0][0]).ToString("dd/MM/yyyy");

            //if (Convert.ToDecimal(PRQty) > 0)
            //{
            if (ViewState["MRPProcess"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["MRPProcess"];


                dr = dt.NewRow();

                dr["Production_Stage"] = ProdStep;
                dr["SAP_No"] = Sapno;
                dr["Item_Desc"] = ItemName;
                dr["Req_Qty"] = Reqty;
                dr["Stock_Qty"] = Stock;
                dr["PR_Qty"] = PRQty;
                dr["Mat_No"] = MatNo;
                dr["UOM"] = UOM;
                dr["Req_Date"] = ReqDate;
                dr["BOM_ID"] = BOM_ID;
                dt.Rows.Add(dr);
                ViewState["MRPProcess"] = dt;
                rptGenPartType.DataSource = dt;
                rptGenPartType.DataBind();
            }
            else
            {

                dt = (DataTable)ViewState["MRPProcess"];


                dr = dt.NewRow();

                dr["Production_Stage"] = ProdStep;
                dr["SAP_No"] = Sapno;
                dr["Item_Desc"] = ItemName;
                dr["Req_Qty"] = Reqty;
                dr["Stock_Qty"] = Stock;
                dr["PR_Qty"] = PRQty;
                dr["Mat_No"] = MatNo;
                dr["UOM"] = UOM;
                dr["Req_Date"] = ReqDate;
                dr["BOM_ID"] = BOM_ID;
                dt.Rows.Add(dr);
                ViewState["MRPProcess"] = dt;
                rptGenPartType.DataSource = dt;
                rptGenPartType.DataBind();
            }

            //}

        }


    }
    protected void btnGenerateWO_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

        GetIPAndName getIPAndName = new GetIPAndName();
        string SysIP = getIPAndName.GetIP();
        string SysName = getIPAndName.GetName();

        SSQL = SSQL + " Select * from Trans_MRPProcess_Main A inner join Trans_MRPProcess_Main_Sub B on A.Work_Order_No=B.Work_Order_No and A.LCode=B.LCode where A.Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " A.LCode ='" + SessionLcode + "' and ";
        SSQL = SSQL + " A.Work_Order_No='" + txtworkorder.Text + "'";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            //Insert Main Table

            SSQL = "Insert Into Trans_GenerateWorkOrder_Main(Ccode,Lcode,FinYearCode,FinYearVal,Work_Order_No,Work_Order_Date,GenModelName,PartType,ApproveStatus, ApproveBy,";
            SSQL = SSQL + " ApproveDate,UserId,UserName,SystemIP,System_Name,Created_On,Status) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            SSQL = SSQL + " '" + SessionFinYearVal + "','" + dt.Rows[0]["Work_Order_No"] + "',Convert(Datetime,GetDate(),103),'" + dt.Rows[0]["GenModel"] + "','" + dt.Rows[0]["Part_Name"] + "','0','',";
            SSQL = SSQL + " '','" + SessionUserID + "','" + SessionUserName + "','" + SysIP + "','" + SysName + "', Convert(Datetime, GetDate(), 103),'ADD')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string WONO = dt.Rows[i]["Work_Order_No"].ToString();
                string Pstage = dt.Rows[i]["Production_Stage"].ToString();
                string SAP = dt.Rows[i]["SAP_No"].ToString();
                string ItemDesc = dt.Rows[i]["Item_Desc"].ToString();
                string Reqty = dt.Rows[i]["Req_Qty"].ToString();
                string StockQty = dt.Rows[i]["Stock_Qty"].ToString();
                string PR_Qty = dt.Rows[i]["PR_Qty"].ToString();
                string WONO1 = dt.Rows[i]["WorkOrder_OrderNo"].ToString();
                string ReverseQty = dt.Rows[i]["Reserve_Qty"].ToString();
                string ExcessQty = dt.Rows[i]["Excess_Qty"].ToString();
                string ConsumeQty = dt.Rows[i]["Consume_Qty"].ToString();
                string MatNo = dt.Rows[i]["Mat_No"].ToString();
                string UOM = dt.Rows[i]["UOM"].ToString();
                string ReqDate = dt.Rows[i]["Req_Date"].ToString();
                string BOMID = dt.Rows[i]["BOM_ID"].ToString();
                string WONO2 = dt.Rows[i]["WOrder_No"].ToString();

                SSQL = "Insert Into Trans_GenerateWorkOrder_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Work_Order_No,Work_Order_Date,Production_Stage,SAP_No,Item_Desc,";
                SSQL = SSQL + " Req_Qty,Stock_Qty,PR_Qty,Reserve_Qty,Excess_Qty,Consume_Qty,Mat_No,UOM,Req_Date,WorkOrder_OrderNo,BOM_ID,WOrder_No) values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + WONO + "',Convert(Datetime,GetDate(),103),'" + Pstage + "',";
                SSQL = SSQL + " '" + SAP + "','" + ItemDesc + "',";
                SSQL = SSQL + " '" + Reqty + "','" + StockQty + "','" + PR_Qty + "','" + ReverseQty + "','" + ExcessQty + "','" + ConsumeQty + "','" + MatNo + "','" + UOM + "','" + ReqDate + "','" + WONO1 + "','" + BOMID + "','" + WONO2 + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Work Order Generated Successfully');", true);
        }
    }
}