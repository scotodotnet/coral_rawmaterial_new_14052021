﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using GeneralClass;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class Transaction_Escrow_PurRqu_Sub : System.Web.UI.Page
{
    public static BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    public static string SessionCcode;
    public static string SessionLcode;
    public static string SessionUserID;
    public static string SessionUserName;
    string SessionPurRequestNo;
    public static string SessionFinYearCode;
    public static string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal ReqQty;
    static Decimal TotReqQty;
    string GenModNo;
    string GenModQty = "";
    string MatType = "";

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Escrow Purchase Request";

            Load_Data_Department();

            //Rawmaterial BOM
            Initial_Data_RawMaterial_SingleItem();
            Load_Data_SapNo();
            Load_Data_ItemName();
            Load_Data_ReferenceCode();

            //Tools 
            Initial_Data_Tools_SingleItem();
            Load_Data_ToolsSapNo();
            Load_Data_ToolsName();
            Load_Data_ToolsRefCode();

            //Asset
            Initial_Data_Asset_SingleItem();
            Load_Data_AssetSapNo();
            Load_Data_AssetName();
            Load_Data_AssetRefCode();

            txtDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy");

            if (Session["Pur_Request_No"] == null)
            {
                SessionPurRequestNo = "";

                //TransactionNoGenerate TransNO = new TransactionNoGenerate();
                //string Auto_Transaction_No = "";
                //if (ddlMatType.SelectedItem.Text == "Tools")
                //{
                //    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Tools Request", SessionFinYearVal, "4");
                //}
                //else if (ddlMatType.SelectedItem.Text == "Asset")
                //{

                //}
                //else
                //{
                //    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Request", SessionFinYearVal, "1");
                //}
                //txtPurRequestNo.Text = Auto_Transaction_No;
            }
            else
            {
                SessionPurRequestNo = Session["Pur_Request_No"].ToString();
                txtPurRequestNo.Text = SessionPurRequestNo;
                btnSearch_Click(sender, e);
            }
        }

        Load_Data_SingleRM();
        Load_Data_SingleTools();
        Load_Data_SingleAsset();
    }

    private void Load_Data_Department()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            try
            {
                ddlDeptName.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["DeptCode"] = "-Select-";
                dr["DeptName"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlDeptName.DataTextField = "DeptName";
                ddlDeptName.DataValueField = "DeptCode";
                ddlDeptName.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
    }

    //**********************************************************************************************************************
    //Rawmaterial
    //**********************************************************************************************************************

    private void Load_Data_SingleRM()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["RMSingleItem"];
        rptRMSingleRM.DataSource = dt;
        rptRMSingleRM.DataBind();
    }

    private void Load_Data_SapNo()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select  Sap_No as SAPNo,Mat_No as ItemCode from BOMMaster where Ccode='" + SessionCcode + "' ";
        SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Sap_No !='' and Status!='Delete'";
        SSQL = SSQL + " Order by Raw_Mat_Name Asc";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            try
            {
                ddlSapNo.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["SAPNo"] = "-Select-";
                dr["ItemCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlSapNo.DataTextField = "SAPNo";
                ddlSapNo.DataValueField = "ItemCode";
                ddlSapNo.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
    }

    private void Load_Data_ItemName()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select  Raw_Mat_Name as ItemName,Mat_No as ItemCode from BOMMaster where Ccode='" + SessionCcode + "' ";
        SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Sap_No !='' and Status!='Delete'";
        SSQL = SSQL + " Order by Raw_Mat_Name Asc";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            try
            {
                ddlItemName.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["ItemName"] = "-Select-";
                dr["ItemCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlItemName.DataTextField = "ItemName";
                ddlItemName.DataValueField = "ItemCode";
                ddlItemName.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
    }

    private void Load_Data_ReferenceCode()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select  Raw_Mat_Name as ItemName,Mat_No as ItemCode from BOMMaster where Ccode='" + SessionCcode + "' ";
        SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Sap_No !='' and Status!='Delete'";
        SSQL = SSQL + " Order by Raw_Mat_Name Asc";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            try
            {
                ddlReferCode.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["ItemCode"] = "-Select-";
                dr["ItemCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlReferCode.DataTextField = "ItemCode";
                ddlReferCode.DataValueField = "ItemCode";
                ddlReferCode.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
    }

    protected void ddlSapNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  ";
            SSQL = SSQL + " And Sap_No='" + ddlSapNo.SelectedItem.Text + "' And Mat_No='" + ddlSapNo.SelectedValue + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlItemName.SelectedValue = DT.Rows[0]["Mat_No"].ToString();
                ddlReferCode.SelectedValue = DT.Rows[0]["Mat_No"].ToString();

                txtBOMQty.Text = DT.Rows[0]["RequiredQty"].ToString();
                txtItemRate.Text = DT.Rows[0]["Amount"].ToString();
                hfMdlCode.Value = DT.Rows[0]["GenModelNo"].ToString();
                hfMdlName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfPrdPrtCode.Value = DT.Rows[0]["ProductionPartNo"].ToString();
                hfPrdPrtName.Value = DT.Rows[0]["ProductionPartName"].ToString();
                hfUOM.Value = DT.Rows[0]["UOM_Full"].ToString();
                hfCGST.Value = DT.Rows[0]["CGSTP"].ToString();
                hfSGST.Value = DT.Rows[0]["SGSTP"].ToString();
                hfIGST.Value = DT.Rows[0]["IGSTP"].ToString();
                hfVAT.Value = DT.Rows[0]["VATP"].ToString();

                hfConvertStatus.Value = DT.Rows[0]["Conversion_Status"].ToString();

                if (DT.Rows[0]["Conversion_Status"].ToString()=="1")
                {
                    //txtBOMQty.Enabled = false;
                    txtUOM.Text = DT.Rows[0]["Conversion_UOM"].ToString();
                    txtBOMQty.Text = DT.Rows[0]["RequiredQty"].ToString();
                    lblConvQty.Text = DT.Rows[0]["Conversion_UOMQty"].ToString();
                    //txtMdlQty.Text = (Convert.ToDecimal(lblConvQty.Text) * Convert.ToDecimal(txtRquQty.Text) / Convert.ToDecimal(txtBOMQty.Text)).ToString();
                }
                else
                {
                    txtUOM.Text = DT.Rows[0]["UOM_Full"].ToString();
                    //txtMdlQty.Text = (Convert.ToDecimal(txtRquQty.Text) / Convert.ToDecimal(txtBOMQty.Text)).ToString();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  ";
            SSQL = SSQL + " And Raw_Mat_Name='" + ddlItemName.SelectedItem.Text + "' And Mat_No='" + ddlItemName.SelectedValue + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlSapNo.SelectedValue = DT.Rows[0]["Mat_No"].ToString();
                ddlReferCode.SelectedValue = DT.Rows[0]["Mat_No"].ToString();

                txtBOMQty.Text = DT.Rows[0]["RequiredQty"].ToString();
                txtItemRate.Text = DT.Rows[0]["Amount"].ToString();
                hfMdlCode.Value = DT.Rows[0]["GenModelNo"].ToString();
                hfMdlName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfPrdPrtCode.Value = DT.Rows[0]["ProductionPartNo"].ToString();
                hfPrdPrtName.Value = DT.Rows[0]["ProductionPartName"].ToString();
                hfUOM.Value = DT.Rows[0]["UOM_Full"].ToString();
                hfCGST.Value = DT.Rows[0]["CGSTP"].ToString();
                hfSGST.Value = DT.Rows[0]["SGSTP"].ToString();
                hfIGST.Value = DT.Rows[0]["IGSTP"].ToString();
                hfVAT.Value = DT.Rows[0]["VATP"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlReferCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  ";
            SSQL = SSQL + " And  Mat_No='" + ddlReferCode.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlItemName.SelectedValue = DT.Rows[0]["Mat_No"].ToString();
                ddlSapNo.SelectedValue = DT.Rows[0]["Mat_No"].ToString();

                txtBOMQty.Text = DT.Rows[0]["RequiredQty"].ToString();
                txtItemRate.Text = DT.Rows[0]["Amount"].ToString();
                hfMdlCode.Value = DT.Rows[0]["GenModelNo"].ToString();
                hfMdlName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfPrdPrtCode.Value = DT.Rows[0]["ProductionPartNo"].ToString();
                hfPrdPrtName.Value = DT.Rows[0]["ProductionPartName"].ToString();
                hfUOM.Value = DT.Rows[0]["UOM_Full"].ToString();
                hfCGST.Value = DT.Rows[0]["CGSTP"].ToString();
                hfSGST.Value = DT.Rows[0]["SGSTP"].ToString();
                hfIGST.Value = DT.Rows[0]["IGSTP"].ToString();
                hfVAT.Value = DT.Rows[0]["VATP"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtMdlQty_TextChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    Boolean Errflg = false;

        //    if (txtBOMQty.Text == "" || txtBOMQty.Text == "0" || txtBOMQty.Text == "0.00")
        //    {
        //        Errflg = true;
        //    }

        //    if (!Errflg)
        //    {
        //        txtRquQty.Text = Math.Round((Convert.ToDecimal(txtBOMQty.Text) * Convert.ToDecimal(txtMdlQty.Text)), 2).ToString();
        //    }
        //}
        //catch (Exception ex)
        //{

        //}
    }
    protected void txtOrderQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Boolean Errflg = false;

            if (txtBOMQty.Text == "" || txtBOMQty.Text == "0" || txtBOMQty.Text == "0.00")
            {
                Errflg = true;
            }

            if (!Errflg)
            {

                if (hfConvertStatus.Value == "1")
                {

                    txtMdlQty.Text = Math.Round((Convert.ToDecimal(lblConvQty.Text) * Convert.ToDecimal(txtRquQty.Text)), 2).ToString();
                    txtMdlQty.Text = Math.Round((Convert.ToDecimal(txtMdlQty.Text) / Convert.ToDecimal(txtBOMQty.Text)), 2).ToString();

                    txtMdlQty.Text = Math.Round(Convert.ToDecimal(txtMdlQty.Text), 2).ToString("#.00");
                }
                else
                {

                    txtMdlQty.Text = (Convert.ToDecimal(txtRquQty.Text) / Convert.ToDecimal(txtBOMQty.Text)).ToString();

                    //txtMdlQty.Text = (Convert.ToDecimal(txtMdlQty.Text) * Convert.ToDecimal("1")).ToString();

                    txtMdlQty.Text = Math.Round(Convert.ToDecimal(txtMdlQty.Text), 2, MidpointRounding.AwayFromZero).ToString("#.00");
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtBOMQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Boolean Errflg = false;

            if (txtMdlQty.Text == "" || txtMdlQty.Text == "0" || txtMdlQty.Text == "0.00")
            {
                Errflg = true;
            }

            if (!Errflg)
            {
                txtRquQty.Text = Math.Round((Convert.ToDecimal(txtBOMQty.Text) * Convert.ToDecimal(txtMdlQty.Text)), 2).ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void Initial_Data_RawMaterial_SingleItem()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelName", typeof(string)));
        dt.Columns.Add(new DataColumn("PrdPartCode", typeof(string)));
        dt.Columns.Add(new DataColumn("PrdPartName", typeof(string)));
        dt.Columns.Add(new DataColumn("BOMQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelQty", typeof(string)));
        dt.Columns.Add(new DataColumn("RquQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRate", typeof(string)));
        
        dt.Columns.Add(new DataColumn("CGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        dt.Columns.Add(new DataColumn("CalWeek", typeof(string)));

        rptRMSingleRM.DataSource = dt;
        rptRMSingleRM.DataBind();
        ViewState["RMSingleItem"] = rptRMSingleRM.DataSource;
    }

    protected void btnAddSingRM_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable qry_dt = new DataTable();
            bool ErrFlag = false;
            DataRow dr = null;
            string SSQL = "";

            if (txtRquQty.Text == "" || txtRquQty.Text == "0" || txtRquQty.Text == "0.00")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Reuired Qty...');", true);
            }



            //check with Item Code And Item Name 

            SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And Sap_No='" + ddlSapNo.SelectedItem.Text + "' And Mat_No='" + ddlReferCode.SelectedItem.Text + "' And  ";
            SSQL = SSQL + " Raw_Mat_Name='" + ddlItemName.SelectedItem.Text + "' ";

            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Check with BOM Material Details..');", true);

            }

            if (!ErrFlag)
            {

                if (ViewState["RMSingleItem"] != null)
                {
                    dt = (DataTable)ViewState["RMSingleItem"];

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                        {
                            if (dt.Rows[i]["CalWeek"].ToString() == txtCalWeek.Text)
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                            }
                        }
                    }
                    if (!ErrFlag)
                    {
                        dr = dt.NewRow();
                        dr["SAPNo"] = ddlSapNo.SelectedItem.Text;
                        dr["ItemCode"] = ddlReferCode.SelectedItem.Text;
                        dr["ItemName"] = ddlItemName.SelectedItem.Text;
                        //dr["UOM"] = hfUOM.Value;
                        dr["UOM"] = txtUOM.Text;
                        dr["ModelCode"] = hfMdlCode.Value;
                        dr["ModelName"] = hfMdlName.Value;
                        dr["PrdPartCode"] = hfPrdPrtCode.Value;
                        dr["PrdPartName"] = hfPrdPrtName.Value;
                        dr["BOMQty"] = txtBOMQty.Text;
                        dr["ModelQty"] = txtMdlQty.Text;
                        dr["RquQty"] = txtRquQty.Text;
                        dr["CalWeek"] = txtCalWeek.Text;
                        dr["ReuiredDate"] = txtRquDate.Text;
                        dr["ItemRate"] = Math.Round(Convert.ToDecimal(txtItemRate.Text), 4, MidpointRounding.AwayFromZero).ToString();
                        
                        dr["CGSTP"] = hfCGST.Value;
                        dr["SGSTP"] = hfSGST.Value;
                        dr["IGSTP"] = hfIGST.Value;
                        dr["Remarks"] = txtRemark.Text;

                        dt.Rows.Add(dr);
                        ViewState["RMSingleItem"] = dt;
                        rptRMSingleRM.DataSource = dt;
                        rptRMSingleRM.DataBind();
                        //TotalReqQty();
                        //TotalQty();

                        //ddlSapNo.SelectedItem.Text = "-Select-";
                        ddlSapNo.SelectedIndex = 0;
                        //ddlItemName.SelectedItem.Text = "-Select-";
                        ddlItemName.SelectedIndex = 0;
                        //ddlReferCode.SelectedItem.Text = "-Select-";
                        ddlReferCode.SelectedIndex = 0;

                        hfMdlCode.Value = "";
                        hfMdlName.Value = "";
                        hfPrdPrtCode.Value = "";
                        hfPrdPrtName.Value = "";
                        hfUOM.Value = "";
                        hfCGST.Value = "";
                        hfSGST.Value = "";
                        hfIGST.Value = "";
                        hfVAT.Value = "";

                        txtBOMQty.Text = "";
                        txtMdlQty.Text = "";
                        txtRquQty.Text = "";
                        txtRquDate.Text = "";
                        txtCalWeek.Text = "";
                        txtItemRate.Text = "";
                        txtUOM.Text = "";
                        txtRemark.Text = "";

                    }
                }
                else
                {
                    dr = dt.NewRow();
                    dr["SAPNo"] = ddlSapNo.SelectedItem.Text;
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    //dr["UOM"] = hfUOM.Value;
                    dr["UOM"] = txtUOM.Text;
                    dr["ModelCode"] = hfMdlCode.Value;
                    dr["ModelName"] = hfMdlName.Value;
                    dr["PrdPartCode"] = hfPrdPrtCode.Value;
                    dr["PrdPartName"] = hfPrdPrtName.Value;
                    dr["BOMQty"] = txtBOMQty.Text;
                    dr["ModelQty"] = txtMdlQty.Text;
                    dr["RquQty"] = txtRquQty.Text;
                    dr["CalWeek"] = txtCalWeek.Text;
                    dr["ReuiredDate"] = txtRquDate.Text;
                    dr["ItemRate"] = txtItemRate.Text;
                    dr["CGSTP"] = hfCGST.Value;
                    dr["SGSTP"] = hfSGST.Value;
                    dr["IGSTP"] = hfIGST.Value;
                    dr["Remarks"] = txtRemark.Text;

                    dt.Rows.Add(dr);
                    ViewState["RMSingleItem"] = dt;
                    rptRMSingleRM.DataSource = dt;
                    rptRMSingleRM.DataBind();
                    //TotalReqQty();
                    //TotalQty();

                    //ddlSapNo.SelectedItem.Text = "-Select-";
                    ddlSapNo.SelectedIndex = 0;
                    //ddlItemName.SelectedItem.Text = "-Select-";
                    ddlItemName.SelectedIndex = 0;
                    //ddlReferCode.SelectedItem.Text = "-Select-";
                    ddlReferCode.SelectedIndex = 0;

                    hfMdlCode.Value = "";
                    hfMdlName.Value = "";
                    hfPrdPrtCode.Value = "";
                    hfPrdPrtName.Value = "";
                    hfUOM.Value = "";
                    hfCGST.Value = "";
                    hfSGST.Value = "";
                    hfIGST.Value = "";
                    hfVAT.Value = "";

                    txtBOMQty.Text = "";
                    txtMdlQty.Text = "";
                    txtRquQty.Text = "";
                    txtRquDate.Text = "";
                    txtCalWeek.Text = "";
                    txtItemRate.Text = "";
                    txtRemark.Text = "";
                    txtUOM.Text = "";
                }
            }
            Total_RM_Qty_Grid_Calc();
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtGrdBOMQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtGrdMdlQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);
        }
        catch (Exception ex)
        {

        }
    }

    private void Text_Change_Value_Cal(object sender, int Row_No)
    {
        try
        {
            string GrdBOMQty = "";
            string GrdModelQty = "";
            string RquestQty = "";

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["RMSingleItem"];
            int i = Row_No;

            TextBox txtBOMQty = rptRMSingleRM.Items[i].FindControl("txtGrdBOMQty") as TextBox;

            if (txtBOMQty.Text.ToString() != "")
            {
                Label lblSapNo = rptRMSingleRM.Items[i].FindControl("lblSAPNo") as Label;
                Label lblItemCode = rptRMSingleRM.Items[i].FindControl("lblItemCode") as Label;
                Label lblItemName = rptRMSingleRM.Items[i].FindControl("lblItemName") as Label;
                Label lblUOM = rptRMSingleRM.Items[i].FindControl("lblUOM") as Label;
                Label lblModelCode = rptRMSingleRM.Items[i].FindControl("lblMdlCode") as Label;
                Label lblModelName = rptRMSingleRM.Items[i].FindControl("lblMdlName") as Label;
                Label lblPrdPrtCode = rptRMSingleRM.Items[i].FindControl("lblPrdPrtCode") as Label;
                Label lblPrdPrtName = rptRMSingleRM.Items[i].FindControl("lblPrdPrtName") as Label;
                TextBox txtGrdBOMQty = rptRMSingleRM.Items[i].FindControl("txtGrdBOMQty") as TextBox;
                TextBox txtGrdMdlQty = rptRMSingleRM.Items[i].FindControl("txtGrdMdlQty") as TextBox;
                Label lblGrdRQty = rptRMSingleRM.Items[i].FindControl("txtGrdRquQty") as Label;
                TextBox txtGrdCalWeek = rptRMSingleRM.Items[i].FindControl("txtGrdCalWeek") as TextBox;
                Label lblRquDate = rptRMSingleRM.Items[i].FindControl("txtRDate") as Label;
                Label lblRemark = rptRMSingleRM.Items[i].FindControl("lblRemarks") as Label;
                Label lblINRRate = rptRMSingleRM.Items[i].FindControl("lblINR") as Label;
                
                Label lblCGSTP = rptRMSingleRM.Items[i].FindControl("lblCGSTP") as Label;
                Label lblSGSTP = rptRMSingleRM.Items[i].FindControl("lblSGSTP") as Label;
                Label lblIGSTP = rptRMSingleRM.Items[i].FindControl("lblIGSTP") as Label;

                if (txtGrdBOMQty.Text != "") { GrdBOMQty = txtGrdBOMQty.Text; }
                if (txtGrdMdlQty.Text != "") { GrdModelQty = txtGrdMdlQty.Text; }

                RquestQty = Math.Round(Convert.ToDecimal(GrdBOMQty) * Convert.ToDecimal(GrdModelQty), 2, MidpointRounding.AwayFromZero).ToString();

                lblGrdRQty.Text = RquestQty;

                dt.Rows[i]["SAPNo"] = lblSapNo.Text;
                dt.Rows[i]["ItemCode"] = lblItemCode.Text;
                dt.Rows[i]["ItemName"] = lblItemName.Text;
                dt.Rows[i]["UOM"] = lblUOM.Text;
                dt.Rows[i]["ModelCode"] = lblModelCode.Text;
                dt.Rows[i]["ModelName"] = lblModelName.Text;
                dt.Rows[i]["PrdPartCode"] = lblPrdPrtCode.Text;
                dt.Rows[i]["PrdPartName"] = lblPrdPrtName.Text;
                dt.Rows[i]["BOMQty"] = txtGrdBOMQty.Text;
                dt.Rows[i]["ModelQty"] = txtGrdMdlQty.Text;
                dt.Rows[i]["RquQty"] = lblGrdRQty.Text;
                dt.Rows[i]["CalWeek"] = txtGrdCalWeek.Text;
                dt.Rows[i]["ReuiredDate"] = lblRquDate.Text;
                dt.Rows[i]["Remarks"] = lblRemark.Text;
                dt.Rows[i]["RateINR"] = lblINRRate.Text;
                //dt.Rows[i]["RateEUR"] = lblEURRate.Text;
                dt.Rows[i]["CGSTP"] = lblCGSTP.Text;
                dt.Rows[i]["SGSTP"] = lblSGSTP.Text;
                dt.Rows[i]["IGSTP"] = lblIGSTP.Text;
            }
            ViewState["RMSingleItem"] = dt;
            rptRMSingleRM.DataSource = dt;
            rptRMSingleRM.DataBind();

            Total_RM_Qty_Grid_Calc();
        }

        catch (Exception ex)
        {

        }
    }

    protected void btnExcelUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        decimal tmpindex = 0;


        lbluploadstatus.Visible = false;

        try
        {
            string _pathToSave = "/Upload/Purchase_Rquest/";

            if (FileUpload.HasFile)
            {

                if (!Directory.Exists(Server.MapPath(_pathToSave)))
                {
                    Directory.CreateDirectory(Server.MapPath(_pathToSave));
                }
                FileUpload.SaveAs(Server.MapPath(_pathToSave + FileUpload.FileName));

            }

            if (!ErrFlag)
            {
                DataRow dr;
                DataTable DT = new DataTable();
                if (Path.GetExtension(FileUpload.FileName) == ".xlsx")
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage package = new ExcelPackage(FileUpload.FileContent);

                    ExcelWorksheet workSheet = package.Workbook.Worksheets.First();

                    foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
                    {
                        DT.Columns.Add(firstRowCell.Text);
                    }
                    for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
                    {
                        var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                        var newRow = DT.NewRow();
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                        DT.Rows.Add(newRow);
                    }
                }

                string SSQL = "";

                if (DT.Rows.Count > 0)
                {
                    DataTable dtExcel = new DataTable();

                    for (int i = 0; DT.Rows.Count > i; i++)
                    {

                        tmpindex = i;

                        ErrFlag = false;
                        SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " Sap_No='" + DT.Rows[i]["SAPNo"].ToString() + "' ";

                        dtExcel = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (dtExcel.Rows.Count > 0)
                        {
                            DataTable dtItemDet = new DataTable();

                            dtItemDet = (DataTable)ViewState["RMSingleItem"];

                            for (int k = 0; dtItemDet.Rows.Count > k; k++)
                            {
                                if (dtItemDet.Rows[k]["SAPNo"].ToString().ToUpper() == dtExcel.Rows[0]["Sap_No"].ToString())
                                {
                                    if (dtItemDet.Rows[k]["CalWeek"].ToString() == DT.Rows[i]["CalWeek"].ToString())
                                    {
                                        string DupliSapNo = "";

                                        ErrFlag = true;
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);

                                        DupliSapNo = dtItemDet.Rows[k]["SAPNo"].ToString();

                                        lbluploadstatus.Visible = true;

                                        lbluploadstatus.Text = lbluploadstatus.Text + DupliSapNo + ",";

                                    }
                                }
                            }
                            if (!ErrFlag)
                            {
                                dr = dtItemDet.NewRow();
                                dr["SAPNo"] = DT.Rows[i]["SAPNo"].ToString();
                                dr["ItemCode"] = dtExcel.Rows[0]["Mat_No"].ToString();
                                dr["ItemName"] = dtExcel.Rows[0]["Raw_Mat_Name"].ToString();
                                dr["UOM"] = dtExcel.Rows[0]["UOM_Full"].ToString();
                                dr["ModelCode"] = dtExcel.Rows[0]["GenModelNo"].ToString();
                                dr["ModelName"] = dtExcel.Rows[0]["GenModelName"].ToString();
                                dr["PrdPartCode"] = dtExcel.Rows[0]["ProductionPartNo"].ToString();
                                dr["PrdPartName"] = dtExcel.Rows[0]["ProductionPartName"].ToString();
                                dr["BOMQty"] = DT.Rows[i]["RquQty"].ToString();
                                dr["ModelQty"] = DT.Rows[i]["ModelQty"].ToString();

                                string RquQty = DT.Rows[i]["RquQty"].ToString();
                                string MdlQty = DT.Rows[i]["ModelQty"].ToString();

                                dr["RquQty"] = Math.Round(Convert.ToDecimal(RquQty) * Convert.ToDecimal(MdlQty), 2, MidpointRounding.AwayFromZero).ToString();
                                dr["CalWeek"] = DT.Rows[i]["CalWeek"].ToString();
                                dr["ReuiredDate"] = DT.Rows[i]["RquDate"].ToString();
                                dr["ItemRate"] = Math.Round(Convert.ToDecimal(DT.Rows[i]["Rate"]), 4, MidpointRounding.AwayFromZero).ToString();
                                dr["CGSTP"] = dtExcel.Rows[0]["CGSTP"].ToString();
                                dr["SGSTP"] = dtExcel.Rows[0]["SGSTP"].ToString();
                                dr["IGSTP"] = dtExcel.Rows[0]["IGSTP"].ToString();
                                dr["Remarks"] = DT.Rows[i]["Remarks"].ToString();

                                dtItemDet.Rows.Add(dr);
                                ViewState["RMSingleItem"] = dtItemDet;
                                rptRMSingleRM.DataSource = dtItemDet;
                                rptRMSingleRM.DataBind();

                            }
                        }
                        else
                        {
                            string NewSapNo;
                            NewSapNo = DT.Rows[i]["SAPNo"].ToString();

                            lbluploadNewSapNo.Visible = true;

                            //lbluploadstatus.Text = "This SapNo's Are New : ";

                            lbluploadNewSapNo.Text = lbluploadNewSapNo.Text + NewSapNo + ",";
                        }

                    }


                    Total_RM_Qty_Grid_Calc();
                }
            }
        }
        catch (Exception ex)
        {
            lbluploadstatus.Visible = true;
            lbluploadstatus.Text = "Error On this Row : " + (tmpindex + 1).ToString();
            Initial_Data_RawMaterial_SingleItem();
            Load_Data_SingleRM();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
     
        try
        {
            SSQL = "Select * from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
            SSQL = SSQL + " Pur_Request_No ='" + txtPurRequestNo.Text + "' and Pur_Req_Status!='Delete'";

            DataTable dtMain = new DataTable();

            dtMain = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dtMain.Rows.Count > 0)
            {
                txtDate.Text = dtMain.Rows[0]["Pur_Request_Date"].ToString();
                ddlDeptName.SelectedValue = dtMain.Rows[0]["DeptCode"].ToString();
                hfDeptCode.Value = dtMain.Rows[0]["DeptCode"].ToString();

                txtOthers.Text = dtMain.Rows[0]["Others"].ToString();

                if (dtMain.Rows[0]["MaterialType"].ToString() == "1")
                {
                    pnlTool.Visible = false;
                    pnlAsset.Visible = false;
                    pnlRawmat.Visible = true;

                    lblRMTotQty.Text = dtMain.Rows[0]["TotSRquQty"].ToString();

                    ddlMatType.SelectedItem.Text = "Raw Material";
                    MatType = dtMain.Rows[0]["MaterialType"].ToString();

                    SSQL = "Select SAPNo,ItemCode,ItemName,UOMCode[UOM],GenModelCode [ModelCode],GenModelName[ModelName] ,ProductionPartCode[PrdPartCode],";
                    SSQL = SSQL + " ProductionPartName[PrdPartName],ReuiredQty [BOMQty],NoOfModel [ModelQty], SumRquQty[RquQty],CalendarWeek[CalWeek],";
                    SSQL = SSQL + " ReuiredDate[ReuiredDate],ItemRemarks[Remarks],Item_Rate[ItemRate],CGSTP,SGSTP,IGSTP ";
                    SSQL = SSQL + " from Trans_Escrow_PurRqu_Sub where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
                    SSQL = SSQL + " and Pur_Request_No='" + txtPurRequestNo.Text + "' and Pur_Req_Status!='Delete'";

                    DataTable dtSub = new DataTable();
                    dtSub = objdata.RptEmployeeMultipleDetails(SSQL);

                    ViewState["RMSingleItem"] = dtSub;
                    rptRMSingleRM.DataSource = dtSub;
                    rptRMSingleRM.DataBind();

                    Total_RM_Qty_Grid_Calc();

                    btnRMSingleItemSave.Text = "Update";
                }
                else if (dtMain.Rows[0]["MaterialType"].ToString() == "2")
                {

                    pnlTool.Visible = true;
                    pnlAsset.Visible = false;
                    pnlRawmat.Visible = false;

                    lblToolsTotQty.Text = dtMain.Rows[0]["TotSRquQty"].ToString();

                    ddlMatType.SelectedItem.Text = "Tools";
                    MatType = dtMain.Rows[0]["MaterialType"].ToString();

                    SSQL = "Select RefNo[RefCode],SAPNo,ToolCode,ToolName,UOM,GenModelCode [ModelCode],GenModelName[ModelName],";
                    SSQL = SSQL + " AssblyStpeCode [AssStpCode],AssblyStpeName[AssStpName],ProductPartCode[PrdCode],";
                    SSQL = SSQL + " ProductPartName[PrdName],ReuiredQty [ToolsQty],NoOfModel [ModelQty], SumRquQty[RequestQty],";
                    SSQL = SSQL + " CalendarWeek [CalenderWeek],ReuiredDate,ItemRemarks[Remarks],Item_Rate[RateINR],ItemRate_Other[RateEUR],";
                    SSQL = SSQL + " CGSTP,SGSTP,IGSTP from Trans_Escrow_PurRqu_Tools_Sub where Ccode='" + SessionCcode + "' and ";
                    SSQL = SSQL + " Lcode='" + SessionLcode + "' and Pur_Request_No='" + txtPurRequestNo.Text + "' and Pur_Req_Status!='Delete'";

                    DataTable dtSub = new DataTable();
                    dtSub = objdata.RptEmployeeMultipleDetails(SSQL);

                    ViewState["ToolSingleItem"] = dtSub;
                    rptToolSingle.DataSource = dtSub;
                    rptToolSingle.DataBind();

                    Total_TL_Qty_Grid_Calc();

                    btnToolsSingleSave.Text = "Update";
                }
                else if (dtMain.Rows[0]["MaterialType"].ToString() == "3")
                {

                    pnlTool.Visible = false;
                    pnlAsset.Visible = true;
                    pnlRawmat.Visible = false;

                    lblToolsTotQty.Text = dtMain.Rows[0]["TotSRquQty"].ToString();

                    ddlMatType.SelectedItem.Text = "Asset";
                    MatType = dtMain.Rows[0]["MaterialType"].ToString();

                    SSQL = "Select RefNo[RefCode],SAPNo,ToolCode[AssetCode],ToolName[AssetName],UOM,GenModelCode [ModelCode],GenModelName[ModelName],";
                    SSQL = SSQL + " AssblyStpeCode [AssStpCode],AssblyStpeName[AssStpName],ProductPartCode[PrdCode],";
                    SSQL = SSQL + " ProductPartName[PrdName],ReuiredQty [AssetQty],NoOfModel [ModelQty], SumRquQty[RequestQty],";
                    SSQL = SSQL + " CalendarWeek [CalenderWeek],ReuiredDate,ItemRemarks[Remarks],Item_Rate[RateINR],ItemRate_Other[RateEUR],";
                    SSQL = SSQL + " CGSTP,SGSTP,IGSTP from Trans_Escrow_PurRqu_Asset_Sub where Ccode='" + SessionCcode + "' and ";
                    SSQL = SSQL + " Lcode='" + SessionLcode + "' and Pur_Request_No='" + txtPurRequestNo.Text + "' and Pur_Req_Status!='Delete'";

                    DataTable dtSub = new DataTable();
                    dtSub = objdata.RptEmployeeMultipleDetails(SSQL);

                    ViewState["AssetSingleItem"] = dtSub;
                    rptAssetSingle.DataSource = dtSub;
                    rptAssetSingle.DataBind();

                    Total_Asset_Qty_Grid_Calc();

                    btnAssetSingleSave.Text = "Update";
                }

            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnRMSingleItemSave_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT_Check = new DataTable();
            DataTable qry_dt = new DataTable();
            string SaveMode = "Insert";
            bool ErrFlag = false;

            DT_Check = (DataTable)ViewState["RMSingleItem"];

            if (DT_Check.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
            }


            if (hfDeptCode.Value == "" || hfDeptCode.Value == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select Department Name...');", true);
            }

            if (!ErrFlag)
            {
                if (ddlMatType.SelectedItem.Text == "Raw Material")
                {
                    MatType = "1";
                }
                else if (ddlMatType.SelectedItem.Text == "Tools")
                {
                    MatType = "2";
                }
                else if (ddlMatType.SelectedItem.Text == "Asset")
                {
                    MatType = "3";
                }

                if (!ErrFlag)
                {
                    if (btnRMSingleItemSave.Text != "Update")
                    {
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = "";
                        if (MatType == "2")
                        {
                            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Tools Request", SessionFinYearVal, "4");
                        }
                        else
                        {
                            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Request", SessionFinYearVal, "1");
                        }
                        txtPurRequestNo.Text = Auto_Transaction_No;
                    }



                    SSQL = "Select * from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";

                    DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_Check.Rows.Count != 0)
                    {
                        SSQL = "";
                        SSQL = "Delete from Trans_Escrow_PurRqu_Main where Pur_Request_No= '" + txtPurRequestNo.Text + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);

                        SSQL = "";
                        SSQL = "Delete Trans_Escrow_PurRqu_Sub where Pur_Request_No='" + txtPurRequestNo.Text + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);

                    }

                    SSQL = "Insert Into Trans_Escrow_PurRqu_Main(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,DeptCode,";
                    SSQL = SSQL + " DeptName,GenModelCode,GenModelName,TotalReqQty,TotSRquQty,Indent_Type,Others,CostCenter,CostElement,";
                    SSQL = SSQL + " Requestby,Approvedby,Status,UserID,UserName,MaterialType,Purchase_type,Pur_Req_Status) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "',";
                    SSQL = SSQL + " '" + txtDate.Text + "','" + hfDeptCode.Value + "','" + ddlDeptName.SelectedItem.Text + "',";
                    SSQL = SSQL + " '" + "" + "','" + "" + "',";
                    SSQL = SSQL + " '" + ReqQty + "','" + lblRMTotQty.Text + "','Escrow','" + txtOthers.Text + "','','','" + SessionUserName + "',";
                    SSQL = SSQL + " '','0','" + SessionUserID + "','" + SessionUserName + "','" + MatType + "','1','Add')";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    DataTable dt = new DataTable();
                    dt = (DataTable)ViewState["RMSingleItem"];

                    for (int i = 0; i < rptRMSingleRM.Items.Count; i++)
                    {
                        Label lblSapNo = rptRMSingleRM.Items[i].FindControl("lblSAPNo") as Label;
                        Label lblItemCode = rptRMSingleRM.Items[i].FindControl("lblItemCode") as Label;
                        Label lblItemName = rptRMSingleRM.Items[i].FindControl("lblItemName") as Label;
                        Label lblUOM = rptRMSingleRM.Items[i].FindControl("lblUOM") as Label;
                        Label lblModelCode = rptRMSingleRM.Items[i].FindControl("lblMdlCode") as Label;
                        Label lblModelName = rptRMSingleRM.Items[i].FindControl("lblMdlName") as Label;
                        Label lblPrdPrtCode = rptRMSingleRM.Items[i].FindControl("lblPrdPrtCode") as Label;
                        Label lblPrdPrtName = rptRMSingleRM.Items[i].FindControl("lblPrdPrtName") as Label;
                        TextBox txtGrdBOMQty = rptRMSingleRM.Items[i].FindControl("txtGrdBOMQty") as TextBox;
                        TextBox txtGrdMdlQty = rptRMSingleRM.Items[i].FindControl("txtGrdMdlQty") as TextBox;
                        Label lblGrdRQty = rptRMSingleRM.Items[i].FindControl("txtGrdRquQty") as Label;
                        TextBox txtGrdCalWeek = rptRMSingleRM.Items[i].FindControl("txtGrdCalWeek") as TextBox;
                        Label lblRquDate = rptRMSingleRM.Items[i].FindControl("txtRDate") as Label;
                        Label lblRemark = rptRMSingleRM.Items[i].FindControl("lblRemarks") as Label;
                        Label lblItemRate = rptRMSingleRM.Items[i].FindControl("lblItemRate") as Label;
                        
                        Label lblCGSTP = rptRMSingleRM.Items[i].FindControl("lblCGSTP") as Label;
                        Label lblSGSTP = rptRMSingleRM.Items[i].FindControl("lblSGSTP") as Label;
                        Label lblIGSTP = rptRMSingleRM.Items[i].FindControl("lblIGSTP") as Label;


                        SSQL = "Insert Into Trans_Escrow_PurRqu_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,";
                        SSQL = SSQL + " GenModelCode,GenModelName,ProductionPartCode,ProductionPartName,ItemCode,ItemName,SAPNo,UOMCode,";
                        SSQL = SSQL + " Item_Rate,ItemRate_Other,ReuiredQty,NoOfModel,SumRquQty,ReuiredDate,Stock_Qty,ItemRemarks,Last_Issue_Date,";
                        SSQL = SSQL + " Last_Purchase_Date,Last_Purchase_Qty,Prev_Purchase_Rate,Re_Order_Stock_Qty,Re_Order_Item_Qty,";
                        SSQL = SSQL + " CGSTP,SGSTP,IGSTP,VATP,UserID,UserName,CalendarWeek,Indent_Type,Pur_Req_Status) Values('" + SessionCcode + "','" + SessionLcode + "',";
                        SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "','" + txtDate.Text + "',";
                        SSQL = SSQL + " '" + lblModelCode.Text + "','" + lblModelName.Text + "','" + lblPrdPrtCode.Text + "','" + lblPrdPrtName.Text + "',";
                        SSQL = SSQL + " '" + lblItemCode.Text + "','" + lblItemName.Text.ToString().Replace("'", "`") + "','" + lblSapNo.Text + "',";
                        SSQL = SSQL + " '" + lblUOM.Text + "','" + lblItemRate.Text + "','0.00','" + txtGrdBOMQty.Text + "',";
                        SSQL = SSQL + " '" + txtGrdMdlQty.Text + "','" + lblGrdRQty.Text + "','" + lblRquDate.Text + "','0','" + lblRemark.Text + "',";
                        SSQL = SSQL + " '','','0', '0','0','0','" + lblCGSTP.Text + "','" + lblSGSTP.Text + "','" + lblIGSTP.Text + "',";
                        SSQL = SSQL + " '0.00','" + SessionUserID + "','" + SessionUserName + "','" + txtGrdCalWeek.Text + "','Escrow','Add')";

                        objdata.RptEmployeeMultipleDetails(SSQL);

                    }

                    if (btnRMSingleItemSave.Text == "Save")
                    {
                        TransactionNoGenerate TransNO_Up = new TransactionNoGenerate();
                        string Auto_Transaction_No_Up = "";
                        if (MatType == "2")
                        {
                            Auto_Transaction_No_Up = TransNO_Up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Tools Request", SessionFinYearVal, "4");
                        }
                        else
                        {
                             Auto_Transaction_No_Up = TransNO_Up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Purchase Request", SessionFinYearVal, "1");
                        }

                    }

                    if (SaveMode == "Insert")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchas Request Saved Successfully...');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchas Request Update Successfully...');", true);
                    }

                    Clear_All_Field();
                    Response.Redirect("Trans_Escrow_PurReq_Main.aspx");
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void Clear_All_Field()
    {
        txtPurRequestNo.Text = ""; txtDate.Text = ""; ddlDeptName.SelectedValue = "-Select-";
        ddlDeptName.SelectedItem.Text = "-Select-";
        txtOthers.Text = "";
        txtBOMQty.Text = ""; txtRemark.Text = "";
        txtItemRate.Text = "0";
        btnRMSingleItemSave.Text = "Save";
        Initial_Data_RawMaterial_SingleItem();

        Session.Remove("Pur_Request_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string Auto_Transaction_No = "";
            TransactionNoGenerate TransNO = new TransactionNoGenerate();

            if (ddlMatType.SelectedItem.Text == "Raw Material")
            {
                MatType = "1";
                pnlRawmat.Visible = true;
                pnlTool.Visible = false;
                pnlAsset.Visible = false;
                //Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Request", SessionFinYearVal, "4");
                //txtPurRequestNo.Text = Auto_Transaction_No;
            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                MatType = "2";
                pnlRawmat.Visible = false;
                pnlTool.Visible = true;
                pnlAsset.Visible = false;

                //Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Tools Request", SessionFinYearVal, "4");
                //txtPurRequestNo.Text = Auto_Transaction_No;

            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                MatType = "3";
                pnlRawmat.Visible = false;
                pnlTool.Visible = false;
                pnlAsset.Visible = true;

                //Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Asset Request", SessionFinYearVal, "4");
                //txtPurRequestNo.Text = Auto_Transaction_No;
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlDeptName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "Select * from MstDepartment where DeptName='" + ddlDeptName.SelectedItem.Text + "'";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
    }

    private void Total_RM_Qty_Grid_Calc()
    {
        decimal SumRequestQty = 0;

        for (int i = 0; i < rptRMSingleRM.Items.Count; i++)
        {
            Label txtRequestQty = rptRMSingleRM.Items[i].FindControl("txtGrdRquQty") as Label;

            SumRequestQty = SumRequestQty + Convert.ToDecimal(txtRequestQty.Text);
        }
        lblRMTotQty.Text = SumRequestQty.ToString();
    }

    //**********************************************************************************************************************
    //Tools 
    //**********************************************************************************************************************

    private void Total_TL_Qty_Grid_Calc()
    {
        decimal SumRequestQty = 0;

        for (int i = 0; i < rptToolSingle.Items.Count; i++)
        {
            TextBox txtRequestQty = rptToolSingle.Items[i].FindControl("txtTolGrdPRQty") as TextBox;

            SumRequestQty = SumRequestQty + Convert.ToDecimal(txtRequestQty.Text);
        }
        lblToolsTotQty.Text = SumRequestQty.ToString();
    }

    private void Initial_Data_Tools_SingleItem()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("RefCode", typeof(string)));
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ToolCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ToolName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelName", typeof(string)));
        dt.Columns.Add(new DataColumn("AssStpCode", typeof(string)));
        dt.Columns.Add(new DataColumn("AssStpName", typeof(string)));
        dt.Columns.Add(new DataColumn("PrdCode", typeof(string)));
        dt.Columns.Add(new DataColumn("PrdName", typeof(string)));
        dt.Columns.Add(new DataColumn("ToolsQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelQty", typeof(string)));
        dt.Columns.Add(new DataColumn("RequestQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("RateINR", typeof(string)));
        dt.Columns.Add(new DataColumn("RateEUR", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        dt.Columns.Add(new DataColumn("CalenderWeek", typeof(string)));

        rptToolSingle.DataSource = dt;
        rptToolSingle.DataBind();
        ViewState["ToolSingleItem"] = rptToolSingle.DataSource;
    }

    private void Load_Data_SingleTools()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ToolSingleItem"];
        rptToolSingle.DataSource = dt;
        rptToolSingle.DataBind();
    }

    private void Load_Data_ToolsRefCode()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select ToolCode,RefCode from MstTools where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "'and Status!='Delete'";
            SSQL = SSQL + " Order by ToolName Asc";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                ddlToolRefCode.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["RefCode"] = "-Select-";
                dr["ToolCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlToolRefCode.DataTextField = "RefCode";
                ddlToolRefCode.DataValueField = "ToolCode";
                ddlToolRefCode.DataBind();
            }
        }
        catch (Exception ex)
        {

        }

    }

    private void Load_Data_ToolsName()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  ToolName,ToolCode from MstTools where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            SSQL = SSQL + " Order by ToolName Asc";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                ddlToolName.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["ToolName"] = "-Select-";
                dr["ToolCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlToolName.DataTextField = "ToolName";
                ddlToolName.DataValueField = "ToolCode";
                ddlToolName.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void Load_Data_ToolsSapNo()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  SAPNo,ToolCode from MstTools where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Status!='Delete'";
            SSQL = SSQL + " Order by ToolName Asc";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                ddlToolSapNo.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["SAPNo"] = "-Select-";
                dr["ToolCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlToolSapNo.DataTextField = "SAPNo";
                ddlToolSapNo.DataValueField = "ToolCode";
                ddlToolSapNo.DataBind();
            }
        }
        catch (Exception ex)
        {

        }

    }

    protected void btnToolAdd_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable qry_dt = new DataTable();
            bool ErrFlag = false;
            DataRow dr = null;
            string SSQL = "";

            if (txtToolRquQty.Text == "" || txtToolRquQty.Text == "0" || txtToolRquQty.Text == "0.00")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Reuired Qty...');", true);
            }


            //check with Item Code And Item Name 

            SSQL = "Select * from MstTools where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And RefCode='" + ddlToolRefCode.SelectedItem.Text + "' And ";
            SSQL = SSQL + " ToolName='" + ddlToolName.SelectedItem.Text + "' ";

            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Tools Details..');", true);
            }

            if (!ErrFlag)
            {

                if (ViewState["ToolSingleItem"] != null)
                {
                    dt = (DataTable)ViewState["ToolSingleItem"];

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["RefCode"].ToString().ToUpper() == ddlReferCode.SelectedItem.Text.ToString().ToUpper())
                        {
                            if (dt.Rows[i]["CalWeek"].ToString() == txtCalWeek.Text)
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Tools Already Added..');", true);
                            }
                        }
                    }
                    if (!ErrFlag)
                    {
                        dr = dt.NewRow();
                        dr["SAPNo"] = ddlToolSapNo.SelectedItem.Text;
                        dr["RefCode"] = ddlToolRefCode.SelectedItem.Text;
                        dr["ToolCode"] = hfToolCode.Value;
                        dr["ToolName"] = ddlToolName.SelectedItem.Text;
                        dr["UOM"] = hfToolUOM.Value;
                        dr["ModelCode"] = hfToolModelCode.Value;
                        dr["ModelName"] = hfToolModelName.Value;
                        dr["AssStpCode"] = hfToolAssemblyCode.Value;
                        dr["AssStpName"] = hfToolAssemblyName.Value;
                        dr["PrdCode"] = hfProductCode.Value;
                        dr["PrdName"] = hfProductName.Value;
                        dr["ToolsQty"] = txtToolRquQty.Text;
                        dr["ModelQty"] = txtToolMdlQty.Text;
                        dr["RequestQty"] = txtToolRquQty.Text;
                        dr["CalenderWeek"] = txtToolCalWeek.Text;
                        dr["ReuiredDate"] = txtToolRquDate.Text;
                        dr["RateINR"] = txtToolINR.Text;
                        dr["RateEUR"] = txtToolEUR.Text;
                        dr["CGSTP"] = hdToolCGSTP.Value;
                        dr["SGSTP"] = hdToolSGSTP.Value;
                        dr["IGSTP"] = hdToolIGSTP.Value;
                        dr["Remarks"] = txtToolRemarks.Text;

                        dt.Rows.Add(dr);
                        ViewState["ToolSingleItem"] = dt;
                        rptToolSingle.DataSource = dt;
                        rptToolSingle.DataBind();
                        //TotalReqQty();
                        //TotalQty();

                        //ddlToolRefCode.SelectedItem.Text = "-Select-";
                        ddlToolRefCode.SelectedIndex = 0;
                        //ddlToolName.SelectedItem.Text = "-Select-";
                        ddlToolName.SelectedIndex = 0;
                        //ddlToolSapNo.SelectedItem.Text = "-Select-";
                        ddlToolSapNo.SelectedIndex = 0;

                        hfToolUOM.Value = "";
                        hfToolModelCode.Value = "";
                        hfToolModelName.Value = "";
                        hfToolAssemblyCode.Value = "";
                        hfToolAssemblyName.Value = "";
                        hfProductCode.Value = "";
                        hfProductName.Value = "";
                        hdToolCGSTP.Value = "";
                        hdToolSGSTP.Value = "";
                        hdToolIGSTP.Value = "";
                        hfToolCode.Value = "";

                        txtToolQty.Text = "1.00 ";
                        txtToolMdlQty.Text = "1.00";
                        txtToolRquQty.Text = "";
                        txtToolCalWeek.Text = "";
                        txtToolRquDate.Text = "";
                        txtToolRemarks.Text = "";
                        txtToolINR.Text = "";
                        txtToolEUR.Text = "";

                    }
                }
                else
                {
                    dr = dt.NewRow();
                    dr["SapNo"] = ddlToolSapNo.SelectedItem.Text;
                    dr["RefCode"] = ddlToolRefCode.SelectedItem.Text;
                    dr["ToolCode"] = hfToolCode.Value;
                    dr["ToolName"] = ddlToolName.SelectedItem.Text;
                    dr["UOM"] = hfToolUOM.Value;
                    dr["ModelCode"] = hfToolModelCode.Value;
                    dr["ModelName"] = hfToolModelName.Value;
                    dr["AssStpCode"] = hfToolAssemblyCode.Value;
                    dr["AssStpName"] = hfToolAssemblyName.Value;
                    dr["PrdCode"] = hfProductCode.Value;
                    dr["PrdName"] = hfProductName.Value;
                    dr["ToolsQty"] = txtToolRquQty.Text;
                    dr["ModelQty"] = txtToolMdlQty.Text;
                    dr["RequestQty"] = txtToolRquQty.Text;
                    dr["CalenderWeek"] = txtToolCalWeek.Text;
                    dr["ReuiredDate"] = txtToolRquDate.Text;
                    dr["RateINR"] = txtToolINR.Text;
                    dr["RateEUR"] = txtToolEUR.Text;
                    dr["CGSTP"] = hdToolCGSTP.Value;
                    dr["SGSTP"] = hdToolSGSTP.Value;
                    dr["IGSTP"] = hdToolIGSTP.Value;
                    dr["Remarks"] = txtToolRemarks.Text;

                    dt.Rows.Add(dr);
                    ViewState["ToolSingleItem"] = dt;
                    rptToolSingle.DataSource = dt;
                    rptToolSingle.DataBind();
                    //TotalReqQty();
                    //TotalQty();

                    //ddlToolRefCode.SelectedItem.Text = "-Select-";
                    ddlToolRefCode.SelectedIndex = 0;
                    //ddlToolName.SelectedItem.Text = "-Select-";
                    ddlToolName.SelectedIndex = 0;
                    //ddlToolSapNo.SelectedItem.Text = "-Select-";
                    ddlToolSapNo.SelectedIndex = 0;

                    hfToolUOM.Value = "";
                    hfToolModelCode.Value = "";
                    hfToolModelName.Value = "";
                    hfToolAssemblyCode.Value = "";
                    hfToolAssemblyName.Value = "";
                    hfProductCode.Value = "";
                    hfProductName.Value = "";
                    hdToolCGSTP.Value = "";
                    hdToolSGSTP.Value = "";
                    hdToolIGSTP.Value = "";
                    hfToolCode.Value = "";

                    txtToolQty.Text = "1.00 ";
                    txtToolMdlQty.Text = "1.00";
                    txtToolRquQty.Text = "";
                    txtToolCalWeek.Text = "";
                    txtToolRquDate.Text = "";
                    txtToolRemarks.Text = "";
                    txtToolINR.Text = "";
                    txtToolEUR.Text = "";
                }
            }

            Total_TL_Qty_Grid_Calc();

        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlToolRefCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from MstTools where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " RefCode='" + ddlToolRefCode.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlToolSapNo.SelectedValue = DT.Rows[0]["ToolCode"].ToString();
                ddlToolName.SelectedValue = DT.Rows[0]["ToolCode"].ToString();

                txtBOMQty.Text = "1.00";
                txtToolINR.Text = DT.Rows[0]["Rate"].ToString();
                txtToolEUR.Text = DT.Rows[0]["RateOther"].ToString();
                hfToolModelCode.Value = DT.Rows[0]["GenModelCode"].ToString();
                hfToolModelName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfToolAssemblyCode.Value = DT.Rows[0]["AssemblyStageCode"].ToString();
                hfToolAssemblyName.Value = DT.Rows[0]["AssemblyStageName"].ToString();
                hfProductCode.Value = DT.Rows[0]["ProductionStepCode"].ToString();
                hfProductName.Value = DT.Rows[0]["ProductionStepName"].ToString();
                hfToolUOM.Value = DT.Rows[0]["UOM"].ToString();
                hdToolCGSTP.Value = DT.Rows[0]["CGSTP"].ToString();
                hdToolSGSTP.Value = DT.Rows[0]["SGSTP"].ToString();
                hdToolIGSTP.Value = DT.Rows[0]["IGSTP"].ToString();
                hfToolCode.Value = DT.Rows[0]["ToolCode"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlToolName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from MstTools where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " ToolName='" + ddlToolName.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlToolSapNo.SelectedItem.Text = DT.Rows[0]["SapNo"].ToString();
                ddlToolRefCode.SelectedItem.Text = DT.Rows[0]["RefCode"].ToString();

                txtBOMQty.Text = "1.00";
                txtToolINR.Text = DT.Rows[0]["Rate"].ToString();
                txtToolEUR.Text = DT.Rows[0]["RateOther"].ToString();
                hfToolModelCode.Value = DT.Rows[0]["GenModelCode"].ToString();
                hfToolModelName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfToolAssemblyCode.Value = DT.Rows[0]["AssemblyStageCode"].ToString();
                hfToolAssemblyName.Value = DT.Rows[0]["AssemblyStageName"].ToString();
                hfProductCode.Value = DT.Rows[0]["ProductionStepCode"].ToString();
                hfProductName.Value = DT.Rows[0]["ProductionStepName"].ToString();
                hfUOM.Value = DT.Rows[0]["UOM"].ToString();
                hfCGST.Value = DT.Rows[0]["CGSTP"].ToString();
                hfSGST.Value = DT.Rows[0]["SGSTP"].ToString();
                hfIGST.Value = DT.Rows[0]["IGSTP"].ToString();
                hfToolCode.Value = DT.Rows[0]["ToolCode"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlToolSapNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from MstTools where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " SapNo='" + ddlToolSapNo.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlToolRefCode.SelectedItem.Text = DT.Rows[0]["RefCode"].ToString();
                ddlToolName.SelectedItem.Text = DT.Rows[0]["ToolName"].ToString();

                txtBOMQty.Text = "1.00";
                txtToolINR.Text = DT.Rows[0]["Rate"].ToString();
                txtToolEUR.Text = DT.Rows[0]["RateOther"].ToString();
                hfToolModelCode.Value = DT.Rows[0]["GenModelCode"].ToString();
                hfToolModelName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfToolAssemblyCode.Value = DT.Rows[0]["AssemblyStageCode"].ToString();
                hfToolAssemblyName.Value = DT.Rows[0]["AssemblyStageName"].ToString();
                hfProductCode.Value = DT.Rows[0]["ProductionStepCode"].ToString();
                hfProductName.Value = DT.Rows[0]["ProductionStepName"].ToString();
                hfUOM.Value = DT.Rows[0]["UOM"].ToString();
                hfCGST.Value = DT.Rows[0]["CGSTP"].ToString();
                hfSGST.Value = DT.Rows[0]["SGSTP"].ToString();
                hfIGST.Value = DT.Rows[0]["IGSTP"].ToString();

                hfToolCode.Value = DT.Rows[0]["ToolCode"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_Escrow_PurReq_Main.aspx");
    }

    protected void btnToolsSingleSave_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT_Check = new DataTable();
            string SaveMode = "Insert";
            bool ErrFlag = false;

            DT_Check = (DataTable)ViewState["ToolSingleItem"];

            if (DT_Check.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
            }


            if (hfDeptCode.Value == "" || hfDeptCode.Value == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select Department Name...');", true);
            }

            if (!ErrFlag)
            {

                if (ddlMatType.SelectedItem.Text == "Raw Material")
                {
                    MatType = "1";
                }
                else if (ddlMatType.SelectedItem.Text == "Tools")
                {
                    MatType = "2";
                }
                else if (ddlMatType.SelectedItem.Text == "Asset")
                {
                    MatType = "3";
                }


                if (btnToolsSingleSave.Text != "Update")
                {
                    if (!ErrFlag)
                    {
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = "";
                        if (MatType == "2")
                        {
                            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Tools Request", SessionFinYearVal, "1");
                        }
                        else
                        {
                            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Request", SessionFinYearVal, "1");
                        }

                        if (Auto_Transaction_No == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin..');", true);
                        }
                        else
                        {
                            txtPurRequestNo.Text = Auto_Transaction_No;
                        }
                    }
                }

                if (!ErrFlag)
                {

                    if (btnToolsSingleSave.Text == "Update")
                    {
                        SSQL = "Delete From Trans_Escrow_PurRqu_Main where Pur_Request_No='" + txtPurRequestNo.Text + "' and ";
                        SSQL = SSQL + "  Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                        //SSQL = SSQL + "";

                        objdata.RptEmployeeMultipleDetails(SSQL);

                        SSQL = "Delete From Trans_Escrow_PurRqu_Tools_Sub ";
                        SSQL = SSQL + " Where Pur_Request_No='" + txtPurRequestNo.Text + "' And Ccode ='" + SessionCcode + "' And  ";
                        SSQL = SSQL + " LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And ";
                        SSQL = SSQL + " FinYearVal ='" + SessionFinYearVal + "'";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    SSQL = "Insert Into Trans_Escrow_PurRqu_Main(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,DeptCode,";
                    SSQL = SSQL + " DeptName,GenModelCode,GenModelName,TotalReqQty,TotSRquQty,Indent_Type,Others,CostCenter,CostElement,";
                    SSQL = SSQL + " Requestby,Approvedby,Status,UserID,UserName,MaterialType,Pur_Req_Status,Purchase_type) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "',";
                    SSQL = SSQL + " '" + txtDate.Text + "','" + hfDeptCode.Value + "','" + ddlDeptName.SelectedItem.Text + "','','',";
                    SSQL = SSQL + " '" + ReqQty + "','" + lblToolsTotQty.Text + "','Escrow','" + txtOthers.Text + "','','','" + SessionUserName + "',";
                    SSQL = SSQL + " '','0','" + SessionUserID + "','" + SessionUserName + "','" + MatType + "','Add','1')";

                    objdata.RptEmployeeMultipleDetails(SSQL);


                    for (int i = 0; i < rptToolSingle.Items.Count; i++)
                    {
                        //Get Some Details

                        string Stock_Qty = "0";
                        string Last_Issue_Date = "";
                        string Last_Purchase_Date = "";
                        string Last_Purchase_Qty = "0";
                        string Prev_Purchase_Rate = "0";
                        DataTable DT_Val = new DataTable();

                        Label lblToolGrdRefCode = rptToolSingle.Items[i].FindControl("lblTolGrdRefCode") as Label;
                        Label lblToolGrdSapNo = rptToolSingle.Items[i].FindControl("lblTolGrdSAPNo") as Label;
                        Label lblToolGrdToolCode = rptToolSingle.Items[i].FindControl("lblTolGrdToolsCode") as Label;
                        Label lblToolGrdToolName = rptToolSingle.Items[i].FindControl("lblTolGrdToolsName") as Label;
                        Label lblToolGrdUOM = rptToolSingle.Items[i].FindControl("lblTolGrdUOM") as Label;
                        Label lblToolGrdMdlCode = rptToolSingle.Items[i].FindControl("lblTolGrdMdlCode") as Label;
                        Label lblToolGrdMdlName = rptToolSingle.Items[i].FindControl("lblTolGrdMdlName") as Label;
                        Label lblToolGrdAssCode = rptToolSingle.Items[i].FindControl("lblTolGrdAssStpCode") as Label;
                        Label lblToolGrdAssName = rptToolSingle.Items[i].FindControl("lblTolGrdAssStpName") as Label;
                        Label lblToolGrdPrdCode = rptToolSingle.Items[i].FindControl("lblTolGrdPrdCode") as Label;
                        Label lblToolGrdPrdName = rptToolSingle.Items[i].FindControl("lblTolGrdPrdName") as Label;
                        Label lblToolGrdReqQty = rptToolSingle.Items[i].FindControl("lblTolGrdRquQty") as Label;
                        //Label lblItemRateOther = Repeater2.Items[i].FindControl("lblItemRate_Other") as Label;
                        Label lblToolGrdMdlQty = rptToolSingle.Items[i].FindControl("lblTolGrdMdlQty") as Label;
                        TextBox txtToolGrdRquQty = rptToolSingle.Items[i].FindControl("txtTolGrdPRQty") as TextBox;
                        TextBox txtToolGrdCalWeek = rptToolSingle.Items[i].FindControl("txtTolGrdCalWeek") as TextBox;
                        Label lblToolGrdRquDate = rptToolSingle.Items[i].FindControl("txtTolGrdRDate") as Label;
                        Label lblToolGrdRemarks = rptToolSingle.Items[i].FindControl("lblTolGrdRemarks") as Label;
                        Label lblToolGrdINR = rptToolSingle.Items[i].FindControl("lblTolGrdINR") as Label;
                        Label lblToolGrdEUR = rptToolSingle.Items[i].FindControl("lblTolGrdEUR") as Label;

                        Label lblToolGrdCGSTP = rptToolSingle.Items[i].FindControl("lblTolGrdCGSTP") as Label;
                        Label lblToolGrdSGSTP = rptToolSingle.Items[i].FindControl("lblTolGrdSGSTP") as Label;
                        Label lblToolGrdIGSTP = rptToolSingle.Items[i].FindControl("lblTolGrdIGSTP") as Label;

                        SSQL = "Insert Into Trans_Escrow_PurRqu_Tools_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,";
                        SSQL = SSQL + " GenModelCode,GenModelName,AssblyStpeCode,AssblyStpeName,ProductPartCode,ProductPartName,SAPNo,RefNo,ToolCode,";
                        SSQL = SSQL + " ToolName,UOM,ReuiredQty,NoOfModel,SumRquQty,Item_Rate,ItemRate_Other,ReuiredDate,CalendarWeek,ItemRemarks,";
                        SSQL = SSQL + " Indent_Type,CGSTP,SGSTP,IGSTP,VATP,Stock_Qty,Last_Issue_Date,Last_Purchase_Date,Last_Purchase_Qty,";
                        SSQL = SSQL + " Prev_Purchase_Rate,Re_Order_Stock_Qty,Re_Order_Item_Qty,Pur_Req_Status,UserName,UserID)Values(";
                        SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                        SSQL = SSQL + " '" + txtPurRequestNo.Text + "','" + txtDate.Text + "','" + lblToolGrdMdlCode.Text + "','" + lblToolGrdMdlName.Text + "',";
                        SSQL = SSQL + " '" + lblToolGrdAssCode.Text + "','" + lblToolGrdAssName.Text + "','" + lblToolGrdPrdCode.Text + "',";
                        SSQL = SSQL + " '" + lblToolGrdPrdName.Text + "','" + lblToolGrdSapNo.Text + "','" + lblToolGrdRefCode.Text + "',";
                        SSQL = SSQL + " '" + lblToolGrdToolCode.Text + "','" + lblToolGrdToolName.Text.Replace("'", "-") + "','" + lblToolGrdUOM.Text + "',";
                        SSQL = SSQL + " '" + lblToolGrdReqQty.Text + "','" + lblToolGrdMdlQty.Text + "','" + txtToolGrdRquQty.Text + "','" + lblToolGrdINR.Text + "',";
                        SSQL = SSQL + " '" + lblToolGrdEUR.Text + "','" + lblToolGrdRquDate.Text + "','" + txtToolGrdCalWeek.Text + "','" + lblToolGrdRemarks.Text + "',";
                        SSQL = SSQL + " 'Escrow','" + lblToolGrdCGSTP.Text + "','" + lblToolGrdSGSTP.Text + "','" + lblToolGrdIGSTP.Text + "','0',";
                        SSQL = SSQL + " '0','" + Last_Issue_Date + "','" + Last_Purchase_Date + "','" + Last_Purchase_Qty + "','" + Prev_Purchase_Rate + "',";
                        SSQL = SSQL + " '0.00','0.00','Add','" + SessionUserName + "','" + SessionUserID + "')";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                }

                if (btnToolsSingleSave.Text == "Save")
                {
                    TransactionNoGenerate TransNO_Up = new TransactionNoGenerate();
                    string Auto_Transaction_No_Up = "";
                    if (MatType == "2")
                    {
                        Auto_Transaction_No_Up = TransNO_Up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Tools Request", SessionFinYearVal, "4");
                    }
                    else
                    {
                        Auto_Transaction_No_Up = TransNO_Up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Purchase Request", SessionFinYearVal, "1");
                    }

                }

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchas Request Saved Successfully...');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchas Request Update Successfully...');", true);
                }

                Clear_All_Field();
                Response.Redirect("Trans_Escrow_PurReq_Main.aspx");
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnToolUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        decimal tmpindex = 0;

        lblToolsUploadStatus.Visible = false;

        try
        {
            string _pathToSave = "/Upload/Purchase_Rquest/";

            if (ToolFileUpload.HasFile)
            {

                if (!Directory.Exists(Server.MapPath(_pathToSave)))
                {
                    Directory.CreateDirectory(Server.MapPath(_pathToSave));
                }
                ToolFileUpload.SaveAs(Server.MapPath(_pathToSave + ToolFileUpload.FileName));

            }

            if (!ErrFlag)
            {
                DataRow dr;
                DataTable DT = new DataTable();
                if (Path.GetExtension(ToolFileUpload.FileName) == ".xlsx")
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage package = new ExcelPackage(ToolFileUpload.FileContent);

                    ExcelWorksheet workSheet = package.Workbook.Worksheets.First();

                    foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
                    {
                        DT.Columns.Add(firstRowCell.Text);
                    }
                    for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
                    {
                        var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                        var newRow = DT.NewRow();
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                        DT.Rows.Add(newRow);
                    }
                }

                string SSQL = "";

                if (DT.Rows.Count > 0)
                {
                    DataTable dtExcel = new DataTable();

                    for (int i = 0; DT.Rows.Count > i; i++)
                    {
                        tmpindex = i;

                        SSQL = "Select * from MstTools where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " RefCode='" + DT.Rows[i]["RefCode"].ToString() + "' ";

                        dtExcel = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (dtExcel.Rows.Count > 0)
                        {
                            DataTable dtItemDet = new DataTable();

                            dtItemDet = (DataTable)ViewState["ToolSingleItem"];

                            for (int k = 0; dtItemDet.Rows.Count > k; k++)
                            {
                                if (dtItemDet.Rows[k]["RefCode"].ToString() == dtExcel.Rows[0]["RefCode"].ToString())
                                {
                                    if (dtItemDet.Rows[k]["CalenderWeek"].ToString() == DT.Rows[i]["CalWeek"].ToString())
                                    {
                                        string DupliSapNo = "";

                                        ErrFlag = true;
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);

                                        DupliSapNo = dtItemDet.Rows[k]["RefCode"].ToString();

                                        lblToolsUploadStatus.Visible = true;

                                        lblToolsUploadStatus.Text = lblToolsUploadStatus.Text + DupliSapNo + ",";
                                    }
                                }
                            }
                            if (!ErrFlag)
                            {
                                dr = dtItemDet.NewRow();
                                dr["SapNo"] = dtExcel.Rows[0]["SapNo"].ToString();
                                dr["RefCode"] = DT.Rows[i]["RefCode"].ToString();
                                dr["ToolCode"] = dtExcel.Rows[0]["ToolCode"].ToString();
                                dr["ToolName"] = dtExcel.Rows[0]["ToolName"].ToString();
                                dr["UOM"] = dtExcel.Rows[0]["UOM"].ToString();
                                dr["ModelCode"] = dtExcel.Rows[0]["GenModelCode"].ToString();
                                dr["ModelName"] = dtExcel.Rows[0]["GenModelName"].ToString();
                                dr["AssStpCode"] = dtExcel.Rows[0]["AssemblyStageCode"].ToString();
                                dr["AssStpName"] = dtExcel.Rows[0]["AssemblyStageName"].ToString();
                                dr["PrdCode"] = dtExcel.Rows[0]["ProductionStepCode"].ToString();
                                dr["PrdName"] = dtExcel.Rows[0]["ProductionStepName"].ToString();
                                dr["ToolsQty"] = DT.Rows[i]["RquQty"].ToString();
                                dr["ModelQty"] = "1";

                                string toolQty = DT.Rows[i]["RquQty"].ToString();
                                string mdlQty = "1.00";


                                dr["RequestQty"] = Math.Round(Convert.ToDecimal(toolQty) * Convert.ToDecimal(mdlQty), 2, MidpointRounding.AwayFromZero).ToString();
                                dr["CalenderWeek"] = DT.Rows[i]["CalWeek"].ToString();
                                dr["ReuiredDate"] = DT.Rows[i]["RquDate"].ToString();

                                if (DT.Rows[i]["RateINR"].ToString() == "")
                                {
                                    dr["RateINR"] = "0.00";
                                }
                                else
                                {
                                    dr["RateINR"] = DT.Rows[i]["RateINR"].ToString();
                                }

                                if (DT.Rows[i]["RateEUR"].ToString() == "")
                                {
                                    dr["RateEUR"] = "0.00";
                                }
                                else
                                {
                                    dr["RateEUR"] = DT.Rows[i]["RateEUR"].ToString();
                                }

                                dr["CGSTP"] = dtExcel.Rows[0]["CGSTP"].ToString();
                                dr["SGSTP"] = dtExcel.Rows[0]["SGSTP"].ToString();
                                dr["IGSTP"] = dtExcel.Rows[0]["IGSTP"].ToString();
                                dr["Remarks"] = DT.Rows[i]["Remarks"].ToString();

                                dtItemDet.Rows.Add(dr);
                                ViewState["ToolSingleItem"] = dtItemDet;
                                rptToolSingle.DataSource = dtItemDet;
                                rptToolSingle.DataBind();

                            }
                        }

                    }

                    Total_TL_Qty_Grid_Calc();
                }

            }
        }
        catch (Exception ex)
        {
            lblToolsUploadStatus.Visible = true;
            lblToolsUploadStatus.Text = "Error On this Row : " + (tmpindex + 1).ToString();
            Initial_Data_Tools_SingleItem();
            Load_Data_SingleTools();
        }
    }

    protected void txtTolGrdPRQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal_Tools(sender, index);

            ((TextBox)rptToolSingle.Items[index].FindControl("txtTolGrdCalWeek")).Focus();
        }
        catch (Exception ex)
        {

        }
    }

    private void Text_Change_Value_Cal_Tools(object sender, int Row_No)
    {
        try
        {
            string GrdBOMQty = "";
            string GrdModelQty = "";
            string RquestQty = "";

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ToolSingleItem"];
            int i = Row_No;

            TextBox txtBOMQty = rptToolSingle.Items[i].FindControl("txtTolGrdPRQty") as TextBox;

            if (txtBOMQty.Text.ToString() != "")
            {
                Label lblTLSapNo = rptToolSingle.Items[i].FindControl("lblTolGrdSAPNo") as Label;
                Label lblTLRefNo = rptToolSingle.Items[i].FindControl("lblTolGrdRefCode") as Label;
                Label lblTLToolCode = rptToolSingle.Items[i].FindControl("lblTolGrdToolsCode") as Label;
                Label lblTLToolName = rptToolSingle.Items[i].FindControl("lblTolGrdToolsName") as Label;
                Label lblTLUOM = rptToolSingle.Items[i].FindControl("lblTolGrdUOM") as Label;
                Label lblTLModelCode = rptToolSingle.Items[i].FindControl("lblTolGrdMdlCode") as Label;
                Label lblTLModelName = rptToolSingle.Items[i].FindControl("lblTolGrdMdlName") as Label;
                Label lblTLAssCode = rptToolSingle.Items[i].FindControl("lblTolGrdAssStpCode") as Label;
                Label lblTLAssName = rptToolSingle.Items[i].FindControl("lblTolGrdAssStpName") as Label;
                Label lblTLPrdStpCode = rptToolSingle.Items[i].FindControl("lblTolGrdPrdCode") as Label;
                Label lblTLPrdStpName = rptToolSingle.Items[i].FindControl("lblTolGrdPrdName") as Label;
                Label lblTLGrdRequiredQty = rptToolSingle.Items[i].FindControl("lblTolGrdRquQty") as Label;
                Label lblTLMdlQty = rptToolSingle.Items[i].FindControl("lblTolGrdMdlQty") as Label;
                TextBox txtTLRequestQty = rptToolSingle.Items[i].FindControl("txtTolGrdPRQty") as TextBox;
                TextBox txtTLCalWeek = rptToolSingle.Items[i].FindControl("txtTolGrdCalWeek") as TextBox;
                Label lblTLRquDate = rptToolSingle.Items[i].FindControl("txtTolGrdRDate") as Label;
                Label lblTLRemarks = rptToolSingle.Items[i].FindControl("lblTolGrdRemarks") as Label;
                Label lblTLINR = rptToolSingle.Items[i].FindControl("lblTolGrdINR") as Label;
                Label lblTLEUR = rptToolSingle.Items[i].FindControl("lblTolGrdEUR") as Label;
                Label lblTLCGST = rptToolSingle.Items[i].FindControl("lblTolGrdCGSTP") as Label;
                Label lblTLSGST = rptToolSingle.Items[i].FindControl("lblTolGrdSGSTP") as Label;
                Label lblTLIGST = rptToolSingle.Items[i].FindControl("lblTolGrdIGSTP") as Label;

                //if (txtGrdBOMQty.Text != "") { GrdBOMQty = txtGrdBOMQty.Text; }
                //if (txtGrdMdlQty.Text != "") { GrdModelQty = txtGrdMdlQty.Text; }

                //RquestQty = Math.Round(Convert.ToDecimal(GrdBOMQty) * Convert.ToDecimal(GrdModelQty), 2, MidpointRounding.AwayFromZero).ToString();

                //lblGrdRQty.Text = RquestQty;

                dt.Rows[i]["SAPNo"] = lblTLSapNo.Text;
                dt.Rows[i]["RefCode"] = lblTLRefNo.Text;
                dt.Rows[i]["ToolCode"] = lblTLToolCode.Text;
                dt.Rows[i]["ToolName"] = lblTLToolName.Text;
                dt.Rows[i]["UOM"] = lblTLUOM.Text;
                dt.Rows[i]["ModelCode"] = lblTLModelCode.Text;
                dt.Rows[i]["ModelName"] = lblTLModelName.Text;
                dt.Rows[i]["AssStpCode"] = lblTLAssCode.Text;
                dt.Rows[i]["AssStpName"] = lblTLAssName.Text;
                dt.Rows[i]["PrdCode"] = lblTLPrdStpCode.Text;
                dt.Rows[i]["PrdName"] = lblTLPrdStpName.Text;
                dt.Rows[i]["ToolsQty"] = txtTLRequestQty.Text;
                dt.Rows[i]["ModelQty"] = txtToolMdlQty.Text;
                dt.Rows[i]["RequestQty"] = txtTLRequestQty.Text;
                dt.Rows[i]["CalenderWeek"] = txtTLCalWeek.Text;
                dt.Rows[i]["ReuiredDate"] = lblTLRquDate.Text;
                dt.Rows[i]["RateINR"] = lblTLINR.Text;
                dt.Rows[i]["RateEUR"] = lblTLEUR.Text;
                dt.Rows[i]["CGSTP"] = lblTLCGST.Text;
                dt.Rows[i]["SGSTP"] = lblTLSGST.Text;
                dt.Rows[i]["IGSTP"] = lblTLIGST.Text;
                dt.Rows[i]["Remarks"] = lblTLRemarks.Text;
            }
            ViewState["ToolSingleItem"] = dt;
            rptToolSingle.DataSource = dt;
            rptToolSingle.DataBind();


            Total_TL_Qty_Grid_Calc();
        }

        catch (Exception ex)
        {

        }
    }

    //**********************************************************************************************************************
    //Asset
    //**********************************************************************************************************************


    private void Total_Asset_Qty_Grid_Calc()
    {
        decimal SumRequestQty = 0;

        for (int i = 0; i < rptAssetSingle.Items.Count; i++)
        {
            TextBox txtRequestQty = rptAssetSingle.Items[i].FindControl("txtASTGrdPRQty") as TextBox;

            SumRequestQty = SumRequestQty + Convert.ToDecimal(txtRequestQty.Text);
        }
        lblAssetTotQty.Text = SumRequestQty.ToString();
    }

    private void Initial_Data_Asset_SingleItem()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("RefCode", typeof(string)));
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("AssetCode", typeof(string)));
        dt.Columns.Add(new DataColumn("AssetName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelName", typeof(string)));
        dt.Columns.Add(new DataColumn("AssStpCode", typeof(string)));
        dt.Columns.Add(new DataColumn("AssStpName", typeof(string)));
        dt.Columns.Add(new DataColumn("PrdCode", typeof(string)));
        dt.Columns.Add(new DataColumn("PrdName", typeof(string)));
        dt.Columns.Add(new DataColumn("AssetQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelQty", typeof(string)));
        dt.Columns.Add(new DataColumn("RequestQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("RateINR", typeof(string)));
        dt.Columns.Add(new DataColumn("RateEUR", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTP", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        dt.Columns.Add(new DataColumn("CalenderWeek", typeof(string)));

        rptAssetSingle.DataSource = dt;
        rptAssetSingle.DataBind();
        ViewState["AssetSingleItem"] = rptAssetSingle.DataSource;
    }

    private void Load_Data_SingleAsset()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["AssetSingleItem"];
        rptAssetSingle.DataSource = dt;
        rptAssetSingle.DataBind();
    }

    private void Load_Data_AssetRefCode()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select ToolCode,RefCode from MstTools where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "'and Status!='Delete'";
            SSQL = SSQL + " Order by ToolName Asc";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                ddlAssetRefcode.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["RefCode"] = "-Select-";
                dr["ToolCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlAssetRefcode.DataTextField = "RefCode";
                ddlAssetRefcode.DataValueField = "ToolCode";
                ddlAssetRefcode.DataBind();
            }
        }
        catch (Exception ex)
        {

        }

    }

    private void Load_Data_AssetName()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  ToolName,ToolCode from MstTools where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' and Status!='Delete'";
            SSQL = SSQL + " Order by ToolName Asc";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                ddlAssetName.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["ToolName"] = "-Select-";
                dr["ToolCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlAssetName.DataTextField = "ToolName";
                ddlAssetName.DataValueField = "ToolCode";
                ddlAssetName.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void Load_Data_AssetSapNo()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  SAPNo,ToolCode from MstTools where Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Status!='Delete'";
            SSQL = SSQL + " Order by ToolName Asc";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {

                ddlAssetSapNo.DataSource = DT;
                DataRow dr = DT.NewRow();

                dr["SAPNo"] = "-Select-";
                dr["ToolCode"] = "-Select-";

                DT.Rows.InsertAt(dr, 0);
                ddlAssetSapNo.DataTextField = "SAPNo";
                ddlAssetSapNo.DataValueField = "ToolCode";
                ddlAssetSapNo.DataBind();
            }
        }
        catch (Exception ex)
        {

        }

    }

    protected void btnAssetAdd_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable qry_dt = new DataTable();
            bool ErrFlag = false;
            DataRow dr = null;
            string SSQL = "";

            if (txtAssetRquestQty.Text == "" || txtAssetRquestQty.Text == "0" || txtAssetRquestQty.Text == "0.00")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Reuired Qty...');", true);
            }


            //check with Item Code And Item Name 

            SSQL = "Select * from MstTools where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And RefCode='" + ddlAssetRefcode.SelectedItem.Text + "' And ";
            SSQL = SSQL + " ToolName='" + ddlAssetName.SelectedItem.Text + "' ";

            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Tools Details..');", true);
            }

            if (!ErrFlag)
            {

                if (ViewState["AssetSingleItem"] != null)
                {
                    dt = (DataTable)ViewState["AssetSingleItem"];

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["RefCode"].ToString().ToUpper() == ddlAssetRefcode.SelectedItem.Text.ToString().ToUpper())
                        {
                            if (dt.Rows[i]["CalWeek"].ToString() == txtAssetCalWeek.Text)
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Asset Already Added..');", true);
                            }
                        }
                    }
                    if (!ErrFlag)
                    {
                        dr = dt.NewRow();
                        dr["SAPNo"] = ddlAssetSapNo.SelectedItem.Text;
                        dr["RefCode"] = ddlAssetRefcode.SelectedItem.Text;
                        dr["AssetCode"] = hfAssetCode.Value;
                        dr["AssetName"] = ddlAssetName.SelectedItem.Text;
                        dr["UOM"] = hfAssetUOM.Value;
                        dr["ModelCode"] = hfAssetModelCode.Value;
                        dr["ModelName"] = hfAssetModelName.Value;
                        dr["AssStpCode"] = hfAssetAssemblyCode.Value;
                        dr["AssStpName"] = hfAssetAssemblyName.Value;
                        dr["PrdCode"] = hfAssetProductCode.Value;
                        dr["PrdName"] = hfAssetProductName.Value;
                        dr["AssetQty"] = lblAssetReqQty.Text;
                        dr["ModelQty"] = lblAssetModelQty.Text;
                        dr["RequestQty"] = txtAssetRquestQty.Text;
                        dr["CalenderWeek"] = txtAssetCalWeek.Text;
                        dr["ReuiredDate"] = txtAssetRquDate.Text;
                        dr["RateINR"] = txtAssetINR.Text;
                        dr["RateEUR"] = txtAssetEUR.Text;
                        dr["CGSTP"] = hdAssetCGSTP.Value;
                        dr["SGSTP"] = hdAssetSGSTP.Value;
                        dr["IGSTP"] = hdAssetIGSTP.Value;
                        dr["Remarks"] = txtAssetRemark.Text;

                        dt.Rows.Add(dr);
                        ViewState["AssetSingleItem"] = dt;
                        rptAssetSingle.DataSource = dt;
                        rptAssetSingle.DataBind();
                        //TotalReqQty();
                        //TotalQty();

                        ddlAssetRefcode.SelectedItem.Text = "-Select-";
                        ddlAssetRefcode.SelectedValue = "-Select-";
                        ddlAssetName.SelectedItem.Text = "-Select-";
                        ddlAssetName.SelectedValue = "-Select-";
                        ddlAssetSapNo.SelectedItem.Text = "-Select-";
                        ddlAssetSapNo.SelectedValue = "-Select-";

                        hfAssetUOM.Value = "";
                        hfAssetModelCode.Value = "";
                        hfAssetModelName.Value = "";
                        hfAssetAssemblyCode.Value = "";
                        hfAssetAssemblyName.Value = "";
                        hfAssetProductCode.Value = "";
                        hfAssetProductName.Value = "";
                        hdAssetCGSTP.Value = "";
                        hdAssetSGSTP.Value = "";
                        hdAssetIGSTP.Value = "";
                        hfAssetCode.Value = "";

                        lblAssetReqQty.Text = "1.00 ";
                        lblAssetModelQty.Text = "1.00";
                        txtAssetRquestQty.Text = "";
                        txtAssetCalWeek.Text = "";
                        txtAssetRquDate.Text = "";
                        txtAssetRemark.Text = "";
                        txtAssetINR.Text = "";
                        txtAssetEUR.Text = "";

                    }
                }
                else
                {
                    dr = dt.NewRow();
                    dr["SAPNo"] = ddlAssetSapNo.SelectedItem.Text;
                    dr["RefCode"] = ddlAssetRefcode.SelectedItem.Text;
                    dr["AssetCode"] = hfAssetCode.Value;
                    dr["AssetName"] = ddlAssetName.SelectedItem.Text;
                    dr["UOM"] = hfAssetUOM.Value;
                    dr["ModelCode"] = hfAssetModelCode.Value;
                    dr["ModelName"] = hfAssetModelName.Value;
                    dr["AssStpCode"] = hfAssetAssemblyCode.Value;
                    dr["AssStpName"] = hfAssetAssemblyName.Value;
                    dr["PrdCode"] = hfAssetProductCode.Value;
                    dr["PrdName"] = hfAssetProductName.Value;
                    dr["AssetQty"] = lblAssetReqQty.Text;
                    dr["ModelQty"] = lblAssetModelQty.Text;
                    dr["RequestQty"] = txtAssetRquestQty.Text;
                    dr["CalenderWeek"] = txtAssetCalWeek.Text;
                    dr["ReuiredDate"] = txtAssetRquDate.Text;
                    dr["RateINR"] = txtAssetINR.Text;
                    dr["RateEUR"] = txtAssetEUR.Text;
                    dr["CGSTP"] = hdAssetCGSTP.Value;
                    dr["SGSTP"] = hdAssetSGSTP.Value;
                    dr["IGSTP"] = hdAssetIGSTP.Value;
                    dr["Remarks"] = txtAssetRemark.Text;

                    dt.Rows.Add(dr);
                    ViewState["AssetSingleItem"] = dt;
                    rptAssetSingle.DataSource = dt;
                    rptAssetSingle.DataBind();
                    //TotalReqQty();
                    //TotalQty();

                    ddlAssetRefcode.SelectedItem.Text = "-Select-";
                    ddlAssetRefcode.SelectedValue = "-Select-";
                    ddlAssetName.SelectedItem.Text = "-Select-";
                    ddlAssetName.SelectedValue = "-Select-";
                    ddlAssetSapNo.SelectedItem.Text = "-Select-";
                    ddlAssetSapNo.SelectedValue = "-Select-";

                    hfAssetUOM.Value = "";
                    hfAssetModelCode.Value = "";
                    hfAssetModelName.Value = "";
                    hfAssetAssemblyCode.Value = "";
                    hfAssetAssemblyName.Value = "";
                    hfAssetProductCode.Value = "";
                    hfAssetProductName.Value = "";
                    hdAssetCGSTP.Value = "";
                    hdAssetSGSTP.Value = "";
                    hdAssetIGSTP.Value = "";
                    hfAssetCode.Value = "";

                    lblAssetReqQty.Text = "1.00 ";
                    lblAssetModelQty.Text = "1.00";
                    txtAssetRquestQty.Text = "";
                    txtAssetCalWeek.Text = "";
                    txtAssetRquDate.Text = "";
                    txtAssetRemark.Text = "";
                    txtAssetINR.Text = "";
                    txtAssetEUR.Text = "";
                }
            }

            Total_Asset_Qty_Grid_Calc();
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlAssetRefcode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from MstTools where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " RefCode='" + ddlAssetRefcode.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlAssetSapNo.SelectedItem.Text = DT.Rows[0]["SapNo"].ToString();
                ddlAssetName.SelectedItem.Text = DT.Rows[0]["ToolName"].ToString();

                lblAssetReqQty.Text = "1.00";
                txtAssetINR.Text = DT.Rows[0]["Rate"].ToString();
                txtAssetEUR.Text = DT.Rows[0]["RateOther"].ToString();
                hfAssetModelCode.Value = DT.Rows[0]["GenModelCode"].ToString();
                hfAssetModelName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfAssetAssemblyCode.Value = DT.Rows[0]["AssemblyStageCode"].ToString();
                hfAssetAssemblyName.Value = DT.Rows[0]["AssemblyStageName"].ToString();
                hfAssetProductCode.Value = DT.Rows[0]["ProductionStepCode"].ToString();
                hfAssetProductName.Value = DT.Rows[0]["ProductionStepName"].ToString();
                hfAssetUOM.Value = DT.Rows[0]["UOM"].ToString();
                hdAssetCGSTP.Value = DT.Rows[0]["CGSTP"].ToString();
                hdAssetSGSTP.Value = DT.Rows[0]["SGSTP"].ToString();
                hdAssetIGSTP.Value = DT.Rows[0]["IGSTP"].ToString();
                hfAssetCode.Value = DT.Rows[0]["ToolCode"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlAssetName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from MstTools where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " ToolName='" + ddlAssetName.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlAssetSapNo.SelectedItem.Text = DT.Rows[0]["SapNo"].ToString();
                ddlAssetRefcode.SelectedItem.Text = DT.Rows[0]["RefCode"].ToString();

                lblAssetReqQty.Text = "1.00";
                txtAssetINR.Text = DT.Rows[0]["Rate"].ToString();
                txtAssetEUR.Text = DT.Rows[0]["RateOther"].ToString();
                hfAssetModelCode.Value = DT.Rows[0]["GenModelCode"].ToString();
                hfAssetModelName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfAssetAssemblyCode.Value = DT.Rows[0]["AssemblyStageCode"].ToString();
                hfAssetAssemblyName.Value = DT.Rows[0]["AssemblyStageName"].ToString();
                hfAssetProductCode.Value = DT.Rows[0]["ProductionStepCode"].ToString();
                hfAssetProductName.Value = DT.Rows[0]["ProductionStepName"].ToString();
                hfAssetUOM.Value = DT.Rows[0]["UOM"].ToString();
                hdAssetCGSTP.Value = DT.Rows[0]["CGSTP"].ToString();
                hdAssetSGSTP.Value = DT.Rows[0]["SGSTP"].ToString();
                hdAssetIGSTP.Value = DT.Rows[0]["IGSTP"].ToString();
                hfAssetCode.Value = DT.Rows[0]["ToolCode"].ToString();
            }
        }
        catch (Exception ex)
        {

        }

    }

    protected void ddlAssetSapNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "Select  * from MstTools where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " SapNo='" + ddlAssetSapNo.SelectedItem.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ddlAssetName.SelectedItem.Text = DT.Rows[0]["ToolName"].ToString();
                ddlAssetRefcode.SelectedItem.Text = DT.Rows[0]["RefCode"].ToString();

                lblAssetReqQty.Text = "1.00";
                txtAssetINR.Text = DT.Rows[0]["Rate"].ToString();
                txtAssetEUR.Text = DT.Rows[0]["RateOther"].ToString();
                hfAssetModelCode.Value = DT.Rows[0]["GenModelCode"].ToString();
                hfAssetModelName.Value = DT.Rows[0]["GenModelName"].ToString();
                hfAssetAssemblyCode.Value = DT.Rows[0]["AssemblyStageCode"].ToString();
                hfAssetAssemblyName.Value = DT.Rows[0]["AssemblyStageName"].ToString();
                hfAssetProductCode.Value = DT.Rows[0]["ProductionStepCode"].ToString();
                hfAssetProductName.Value = DT.Rows[0]["ProductionStepName"].ToString();
                hfAssetUOM.Value = DT.Rows[0]["UOM"].ToString();
                hdAssetCGSTP.Value = DT.Rows[0]["CGSTP"].ToString();
                hdAssetSGSTP.Value = DT.Rows[0]["SGSTP"].ToString();
                hdAssetIGSTP.Value = DT.Rows[0]["IGSTP"].ToString();
                hfAssetCode.Value = DT.Rows[0]["ToolCode"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtAssetGrdPRQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal_Asset(sender, index);

            ((TextBox)rptAssetSingle.Items[index].FindControl("txtASTGrdCalWeek")).Focus();
        }
        catch (Exception ex)
        {

        }
    }

    private void Text_Change_Value_Cal_Asset(object sender, int Row_No)
    {
        try
        {
            //string GrdBOMQty = "";
            //string GrdModelQty = "";
            //string RquestQty = "";

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["AssetSingleItem"];
            int i = Row_No;

            TextBox txtBOMQty = rptAssetSingle.Items[i].FindControl("txtASTGrdPRQty") as TextBox;

            if (txtBOMQty.Text.ToString() != "")
            {
                Label lblASTSapNo = rptAssetSingle.Items[i].FindControl("lblASTGrdSAPNo") as Label;
                Label lblASTRefNo = rptAssetSingle.Items[i].FindControl("lblASTGrdRefCode") as Label;
                Label lblASTToolCode = rptAssetSingle.Items[i].FindControl("lblASTGrdToolsCode") as Label;
                Label lblASTToolName = rptAssetSingle.Items[i].FindControl("lblASTGrdToolsName") as Label;
                Label lblASTUOM = rptAssetSingle.Items[i].FindControl("lblASTGrdUOM") as Label;
                Label lblASTModelCode = rptAssetSingle.Items[i].FindControl("lblASTGrdMdlCode") as Label;
                Label lblASTModelName = rptAssetSingle.Items[i].FindControl("lblASTGrdMdlName") as Label;
                Label lblASTAssCode = rptAssetSingle.Items[i].FindControl("lblASTGrdAssStpCode") as Label;
                Label lblASTAssName = rptAssetSingle.Items[i].FindControl("lblASTGrdAssStpName") as Label;
                Label lblASTPrdStpCode = rptAssetSingle.Items[i].FindControl("lblASTGrdPrdCode") as Label;
                Label lblASTPrdStpName = rptAssetSingle.Items[i].FindControl("lblASTGrdPrdName") as Label;
                Label lblASTGrdRequiredQty = rptAssetSingle.Items[i].FindControl("lblASTGrdRquQty") as Label;
                Label lblASTMdlQty = rptAssetSingle.Items[i].FindControl("lblASTGrdMdlQty") as Label;
                TextBox txtASTRequestQty = rptAssetSingle.Items[i].FindControl("txtASTGrdPRQty") as TextBox;
                TextBox txtASTCalWeek = rptAssetSingle.Items[i].FindControl("txtASTGrdCalWeek") as TextBox;
                Label lblASTRquDate = rptAssetSingle.Items[i].FindControl("txtASTGrdRDate") as Label;
                Label lblASTRemarks = rptAssetSingle.Items[i].FindControl("lblASTGrdRemarks") as Label;
                Label lblASTINR = rptAssetSingle.Items[i].FindControl("lblASTGrdINR") as Label;
                Label lblASTEUR = rptAssetSingle.Items[i].FindControl("lblASTGrdEUR") as Label;
                Label lblASTCGST = rptAssetSingle.Items[i].FindControl("lblASTGrdCGSTP") as Label;
                Label lblASTSGST = rptAssetSingle.Items[i].FindControl("lblASTGrdSGSTP") as Label;
                Label lblASTIGST = rptAssetSingle.Items[i].FindControl("lblASTGrdIGSTP") as Label;

                //if (txtGrdBOMQty.Text != "") { GrdBOMQty = txtGrdBOMQty.Text; }
                //if (txtGrdMdlQty.Text != "") { GrdModelQty = txtGrdMdlQty.Text; }

                //RquestQty = Math.Round(Convert.ToDecimal(GrdBOMQty) * Convert.ToDecimal(GrdModelQty), 2, MidpointRounding.AwayFromZero).ToString();

                //lblGrdRQty.Text = RquestQty;

                dt.Rows[i]["SAPNo"] = lblASTSapNo.Text;
                dt.Rows[i]["RefCode"] = lblASTRefNo.Text;
                dt.Rows[i]["AssetCode"] = lblASTToolCode.Text;
                dt.Rows[i]["AssetName"] = lblASTToolName.Text;
                dt.Rows[i]["UOM"] = lblASTUOM.Text;
                dt.Rows[i]["ModelCode"] = lblASTModelCode.Text;
                dt.Rows[i]["ModelName"] = lblASTModelName.Text;
                dt.Rows[i]["AssStpCode"] = lblASTAssCode.Text;
                dt.Rows[i]["AssStpName"] = lblASTAssName.Text;
                dt.Rows[i]["PrdCode"] = lblASTPrdStpCode.Text;
                dt.Rows[i]["PrdName"] = lblASTPrdStpName.Text;
                dt.Rows[i]["AssetQty"] = txtASTRequestQty.Text;
                dt.Rows[i]["ModelQty"] = txtToolMdlQty.Text;
                dt.Rows[i]["RequestQty"] = txtASTRequestQty.Text;
                dt.Rows[i]["CalenderWeek"] = txtASTCalWeek.Text;
                dt.Rows[i]["ReuiredDate"] = lblASTRquDate.Text;
                dt.Rows[i]["RateINR"] = lblASTINR.Text;
                dt.Rows[i]["RateEUR"] = lblASTEUR.Text;
                dt.Rows[i]["CGSTP"] = lblASTCGST.Text;
                dt.Rows[i]["SGSTP"] = lblASTSGST.Text;
                dt.Rows[i]["IGSTP"] = lblASTIGST.Text;
                dt.Rows[i]["Remarks"] = lblASTRemarks.Text;
            }
            ViewState["AssetSingleItem"] = dt;
            rptAssetSingle.DataSource = dt;
            rptAssetSingle.DataBind();


            Total_Asset_Qty_Grid_Calc();
        }

        catch (Exception ex)
        {

        }
    }

    protected void btnAssetSingleSave_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL = "";
            DataTable DT_Check = new DataTable();
            string SaveMode = "Insert";
            bool ErrFlag = false;

            DT_Check = (DataTable)ViewState["AssetSingleItem"];

            if (DT_Check.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
            }


            if (hfDeptCode.Value == "" || hfDeptCode.Value == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select Department Name...');", true);
            }


            if (!ErrFlag)
            {
                if (ddlMatType.SelectedItem.Text == "Raw Material")
                {
                    MatType = "1";
                }
                else if (ddlMatType.SelectedItem.Text == "Tools")
                {
                    MatType = "2";
                }
                else if (ddlMatType.SelectedItem.Text == "Asset")
                {
                    MatType = "3";
                }

                if (btnAssetSingleSave.Text != "Update")
                {
                    if (!ErrFlag)
                    {
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        string Auto_Transaction_No = "";
                        if (MatType == "3")
                        {
                            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Asset Request", SessionFinYearVal, "1");
                        }
                        else
                        {
                            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Request", SessionFinYearVal, "1");
                        }

                        if (Auto_Transaction_No == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin..');", true);
                        }
                        else
                        {
                            txtPurRequestNo.Text = Auto_Transaction_No;
                        }
                    }
                }

                if (!ErrFlag)
                {

                    if (btnAssetSingleSave.Text == "Update")
                    {
                        SSQL = "Delete From Trans_Escrow_PurRqu_Main where Pur_Request_No='" + txtPurRequestNo.Text + "' and ";
                        SSQL = SSQL + "  Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                        //SSQL = SSQL + "";

                        objdata.RptEmployeeMultipleDetails(SSQL);

                        SSQL = "Delete From Trans_Escrow_PurRqu_Asset_Sub ";
                        SSQL = SSQL + " Where Pur_Request_No='" + txtPurRequestNo.Text + "' And Ccode ='" + SessionCcode + "' And  ";
                        SSQL = SSQL + " LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And ";
                        SSQL = SSQL + " FinYearVal ='" + SessionFinYearVal + "'";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    SSQL = "Insert Into Trans_Escrow_PurRqu_Main(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,DeptCode,";
                    SSQL = SSQL + " DeptName,GenModelCode,GenModelName,TotalReqQty,TotSRquQty,Indent_Type,Others,CostCenter,CostElement,";
                    SSQL = SSQL + " Requestby,Approvedby,Status,UserID,UserName,MaterialType,Pur_Req_Status,Purchase_type) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "',";
                    SSQL = SSQL + " '" + txtDate.Text + "','" + hfDeptCode.Value + "','" + ddlDeptName.SelectedItem.Text + "','','',";
                    SSQL = SSQL + " '" + ReqQty + "','" + lblAssetTotQty.Text + "','Escrow','" + txtOthers.Text + "','','','" + SessionUserName + "',";
                    SSQL = SSQL + " '','0','" + SessionUserID + "','" + SessionUserName + "','" + MatType + "','Add','1')";

                    objdata.RptEmployeeMultipleDetails(SSQL);


                    for (int i = 0; i < rptAssetSingle.Items.Count; i++)
                    {
                        //Get Some Details

                        string Stock_Qty = "0";
                        string Last_Issue_Date = "";
                        string Last_Purchase_Date = "";
                        string Last_Purchase_Qty = "0";
                        string Prev_Purchase_Rate = "0";
                        DataTable DT_Val = new DataTable();

                        Label lblASTSapNo = rptAssetSingle.Items[i].FindControl("lblASTGrdSAPNo") as Label;
                        Label lblASTRefNo = rptAssetSingle.Items[i].FindControl("lblASTGrdRefCode") as Label;
                        Label lblASTToolCode = rptAssetSingle.Items[i].FindControl("lblASTGrdToolsCode") as Label;
                        Label lblASTToolName = rptAssetSingle.Items[i].FindControl("lblASTGrdToolsName") as Label;
                        Label lblASTUOM = rptAssetSingle.Items[i].FindControl("lblASTGrdUOM") as Label;
                        Label lblASTModelCode = rptAssetSingle.Items[i].FindControl("lblASTGrdMdlCode") as Label;
                        Label lblASTModelName = rptAssetSingle.Items[i].FindControl("lblASTGrdMdlName") as Label;
                        Label lblASTAssCode = rptAssetSingle.Items[i].FindControl("lblASTGrdAssStpCode") as Label;
                        Label lblASTAssName = rptAssetSingle.Items[i].FindControl("lblASTGrdAssStpName") as Label;
                        Label lblASTPrdStpCode = rptAssetSingle.Items[i].FindControl("lblASTGrdPrdCode") as Label;
                        Label lblASTPrdStpName = rptAssetSingle.Items[i].FindControl("lblASTGrdPrdName") as Label;
                        Label lblASTGrdRequiredQty = rptAssetSingle.Items[i].FindControl("lblASTGrdRquQty") as Label;
                        Label lblASTMdlQty = rptAssetSingle.Items[i].FindControl("lblASTGrdMdlQty") as Label;
                        TextBox txtASTRequestQty = rptAssetSingle.Items[i].FindControl("txtASTGrdPRQty") as TextBox;
                        TextBox txtASTCalWeek = rptAssetSingle.Items[i].FindControl("txtASTGrdCalWeek") as TextBox;
                        Label lblASTRquDate = rptAssetSingle.Items[i].FindControl("txtASTGrdRDate") as Label;
                        Label lblASTRemarks = rptAssetSingle.Items[i].FindControl("lblASTGrdRemarks") as Label;
                        Label lblASTINR = rptAssetSingle.Items[i].FindControl("lblASTGrdINR") as Label;
                        Label lblASTEUR = rptAssetSingle.Items[i].FindControl("lblASTGrdEUR") as Label;
                        Label lblASTCGSTP = rptAssetSingle.Items[i].FindControl("lblASTGrdCGSTP") as Label;
                        Label lblASTSGSTP = rptAssetSingle.Items[i].FindControl("lblASTGrdSGSTP") as Label;
                        Label lblASTIGSTP = rptAssetSingle.Items[i].FindControl("lblASTGrdIGSTP") as Label;

                        SSQL = "Insert Into Trans_Escrow_PurRqu_Asset_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,";
                        SSQL = SSQL + " GenModelCode,GenModelName,AssblyStpeCode,AssblyStpeName,ProductPartCode,ProductPartName,SAPNo,RefNo,ToolCode,";
                        SSQL = SSQL + " ToolName,UOM,ReuiredQty,NoOfModel,SumRquQty,Item_Rate,ItemRate_Other,ReuiredDate,CalendarWeek,ItemRemarks,";
                        SSQL = SSQL + " Indent_Type,CGSTP,SGSTP,IGSTP,VATP,Stock_Qty,Last_Issue_Date,Last_Purchase_Date,Last_Purchase_Qty,";
                        SSQL = SSQL + " Prev_Purchase_Rate,Re_Order_Stock_Qty,Re_Order_Item_Qty,Pur_Req_Status,UserName,UserID)Values(";
                        SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                        SSQL = SSQL + " '" + txtPurRequestNo.Text + "','" + txtDate.Text + "','" + lblASTModelCode.Text + "','" + lblASTModelName.Text + "',";
                        SSQL = SSQL + " '" + lblASTAssCode.Text + "','" + lblASTAssName.Text + "','" + lblASTPrdStpCode.Text + "',";
                        SSQL = SSQL + " '" + lblASTPrdStpName.Text + "','" + lblASTSapNo.Text + "','" + lblASTRefNo.Text + "',";
                        SSQL = SSQL + " '" + lblASTToolCode.Text + "','" + lblASTToolName.Text.Replace("'", "-") + "','" + lblASTUOM.Text + "',";
                        SSQL = SSQL + " '" + lblASTGrdRequiredQty.Text + "','" + lblASTMdlQty.Text + "','" + txtASTRequestQty.Text + "','" + lblASTINR.Text + "',";
                        SSQL = SSQL + " '" + lblASTEUR.Text + "','" + lblASTRquDate.Text + "','" + txtASTCalWeek.Text + "','" + lblASTRemarks.Text + "',";
                        SSQL = SSQL + " 'Escrow','" + lblASTCGSTP.Text + "','" + lblASTSGSTP.Text + "','" + lblASTIGSTP.Text + "','0',";
                        SSQL = SSQL + " '0','" + Last_Issue_Date + "','" + Last_Purchase_Date + "','" + Last_Purchase_Qty + "','" + Prev_Purchase_Rate + "',";
                        SSQL = SSQL + " '0.00','0.00','Add','" + SessionUserName + "','" + SessionUserID + "')";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                }

                if (btnAssetSingleSave.Text == "Save")
                {
                    TransactionNoGenerate TransNO_Up = new TransactionNoGenerate();
                    string Auto_Transaction_No_Up = "";

                    if (MatType == "3")
                    {
                        Auto_Transaction_No_Up = TransNO_Up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Asset Request", SessionFinYearVal, "4");
                    }
                    else
                    {
                        Auto_Transaction_No_Up = TransNO_Up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Purchase Request", SessionFinYearVal, "1");
                    }

                }

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchas Request Saved Successfully...');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchas Request Update Successfully...');", true);
                }

                Clear_All_Field();
                Response.Redirect("Trans_Escrow_PurReq_Main.aspx");
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnAssetUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        decimal tmpindex = 0;

        lbluploadstatus.Visible = false;

        try
        {
            string _pathToSave = "/Upload/Purchase_Rquest/";

            if (fileupAsset.HasFile)
            {

                if (!Directory.Exists(Server.MapPath(_pathToSave)))
                {
                    Directory.CreateDirectory(Server.MapPath(_pathToSave));
                }
                fileupAsset.SaveAs(Server.MapPath(_pathToSave + fileupAsset.FileName));

            }

            if (!ErrFlag)
            {
                DataRow dr;
                DataTable DT = new DataTable();
                if (Path.GetExtension(fileupAsset.FileName) == ".xlsx")
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage package = new ExcelPackage(fileupAsset.FileContent);

                    ExcelWorksheet workSheet = package.Workbook.Worksheets.First();

                    foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
                    {
                        DT.Columns.Add(firstRowCell.Text);
                    }
                    for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
                    {
                        var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                        var newRow = DT.NewRow();
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                        DT.Rows.Add(newRow);
                    }
                }

                string SSQL = "";

                if (DT.Rows.Count > 0)
                {
                    DataTable dtExcel = new DataTable();

                    for (int i = 0; DT.Rows.Count > i; i++)
                    {
                        tmpindex = i;

                        ErrFlag = false;

                        SSQL = "Select * from MstTools where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " RefCode='" + DT.Rows[i]["RefCode"].ToString() + "' ";

                        dtExcel = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (dtExcel.Rows.Count > 0)
                        {
                            DataTable dtItemDet = new DataTable();

                            dtItemDet = (DataTable)ViewState["AssetSingleItem"];

                            for (int k = 0; dtItemDet.Rows.Count > k; k++)
                            {
                                if (dtItemDet.Rows[k]["RefCode"].ToString() == dtExcel.Rows[0]["ToolCode"].ToString())
                                {
                                    if (dtItemDet.Rows[k]["CalenderWeek"].ToString() == DT.Rows[i]["CalWeek"].ToString())
                                    {
                                        string DupliSapNo = "";

                                        ErrFlag = true;
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);

                                        DupliSapNo = dtItemDet.Rows[k]["SAPNo"].ToString();

                                        lblAssetUploadstatus.Visible = true;

                                        lblAssetUploadstatus.Text = lblAssetUploadstatus.Text + DupliSapNo + ",";
                                    }
                                }
                            }
                            if (!ErrFlag)
                            {
                                dr = dtItemDet.NewRow();
                                dr["SapNo"] = dtExcel.Rows[0]["SapNo"].ToString();
                                dr["RefCode"] = DT.Rows[i]["RefCode"].ToString();
                                dr["AssetCode"] = dtExcel.Rows[0]["ToolCode"].ToString();
                                dr["AssetName"] = dtExcel.Rows[0]["ToolName"].ToString();
                                dr["UOM"] = dtExcel.Rows[0]["UOM"].ToString();
                                dr["ModelCode"] = dtExcel.Rows[0]["GenModelCode"].ToString();
                                dr["ModelName"] = dtExcel.Rows[0]["GenModelName"].ToString();
                                dr["AssStpCode"] = dtExcel.Rows[0]["AssemblyStageCode"].ToString();
                                dr["AssStpName"] = dtExcel.Rows[0]["AssemblyStageName"].ToString();
                                dr["PrdCode"] = dtExcel.Rows[0]["ProductionStepCode"].ToString();
                                dr["PrdName"] = dtExcel.Rows[0]["ProductionStepName"].ToString();
                                dr["AssetQty"] = DT.Rows[i]["RquQty"].ToString();
                                dr["ModelQty"] = "1";

                                string toolQty = DT.Rows[i]["RquQty"].ToString();
                                string mdlQty = "1.00";


                                dr["RequestQty"] = Math.Round(Convert.ToDecimal(toolQty) * Convert.ToDecimal(mdlQty), 2, MidpointRounding.AwayFromZero).ToString();
                                dr["CalenderWeek"] = DT.Rows[i]["CalWeek"].ToString();
                                dr["ReuiredDate"] = DT.Rows[i]["RquDate"].ToString();

                                if (DT.Rows[i]["RateINR"].ToString() == "")
                                {
                                    dr["RateINR"] = "0.00";
                                }
                                else
                                {
                                    dr["RateINR"] = DT.Rows[i]["RateINR"].ToString();
                                }

                                if (DT.Rows[i]["RateEUR"].ToString() == "")
                                {
                                    dr["RateEUR"] = "0.00";
                                }
                                else
                                {
                                    dr["RateEUR"] = DT.Rows[i]["RateEUR"].ToString();
                                }

                                dr["CGSTP"] = dtExcel.Rows[0]["CGSTP"].ToString();
                                dr["SGSTP"] = dtExcel.Rows[0]["SGSTP"].ToString();
                                dr["IGSTP"] = dtExcel.Rows[0]["IGSTP"].ToString();
                                dr["Remarks"] = DT.Rows[i]["Remarks"].ToString();

                                dtItemDet.Rows.Add(dr);
                                ViewState["AssetSingleItem"] = dtItemDet;
                                rptAssetSingle.DataSource = dtItemDet;
                                rptAssetSingle.DataBind();

                            }
                        }

                    }

                    Total_Asset_Qty_Grid_Calc();
                }

            }
        }
        catch (Exception ex)

        {
            lblAssetUploadstatus.Visible = true;
            lblAssetUploadstatus.Text = "Error On this Row : " + (tmpindex + 1).ToString();
            Initial_Data_Asset_SingleItem();
            Load_Data_SingleAsset();
        }
    }


    protected void btnDetAssetClick (object sender,CommandEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["AssetSingleItem"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["RefCode"].ToString() == e.CommandName.ToString())
                {
                    dt.Rows.RemoveAt(i);
                    dt.AcceptChanges();
                }
            }
            ViewState["AssetSingleItem"] = dt;
            Load_Data_SingleAsset();

            //txtTax.Text = "0";
            //txtTaxAmt.Text = "0";
            //txtDiscount.Text = "0";
            //txtOthers.Text = "0";
            //txtNetAmt.Text = "0";
            Total_Asset_Qty_Grid_Calc();
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnDetRMClick(object sender, CommandEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["RMSingleItem"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
                {
                    dt.Rows.RemoveAt(i);
                    dt.AcceptChanges();
                }
            }
            ViewState["RMSingleItem"] = dt;
            Load_Data_SingleRM();

            //txtTax.Text = "0";
            //txtTaxAmt.Text = "0";
            //txtDiscount.Text = "0";
            //txtOthers.Text = "0";
            //txtNetAmt.Text = "0";
            Total_RM_Qty_Grid_Calc();
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnDetToolsClick(object sender, CommandEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ToolSingleItem"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["RefCode"].ToString() == e.CommandName.ToString())
                {
                    dt.Rows.RemoveAt(i);
                    dt.AcceptChanges();
                }
            }
            ViewState["ToolSingleItem"] = dt;
            Load_Data_SingleTools();

            //txtTax.Text = "0";
            //txtTaxAmt.Text = "0";
            //txtDiscount.Text = "0";
            //txtOthers.Text = "0";
            //txtNetAmt.Text = "0";
            Total_TL_Qty_Grid_Calc();
        }
        catch (Exception ex)
        {

        }
    }
}

