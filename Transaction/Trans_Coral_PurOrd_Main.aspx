﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Coral_PurOrd_Main.aspx.cs" Inherits="Trans_Coral_PurOrd_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                }
            });
        };
    </script>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
<div class="content-wrapper">
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            
        <section class="content">
            <div class="row">
                <div class="col-md-2">
                    <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />
                </div>
                <!-- /.col -->
            </div>
            <!--/.row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Coral Purchase Order List</span></h3>
                          <%--  <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div>--%>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-body no-padding">
                                <div class="table-responsive mailbox-messages">

                                    <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false" OnItemDataBound="Repeater2_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="example" class="table table-hover table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Purchase No</th>
                                                        <th>Date</th>
                                                        <th>Identification No</th>
                                                        <th>Supplier</th>
                                                        <th>Material_Type</th>
                                                        <th>CancelledBy</th>
                                                        <th>CancelledReason</th>
                                                        <th>Approval_Status</th>
                                                        <th>Edit/View/Delete</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex+1 %></td>
                                                <td><%# Eval("Std_PO_No")%></td>
                                                <td><%# Eval("Std_PO_Date")%></td>
                                                <td><%# Eval("RefNo")%></td>
                                                <td><%# Eval("Supp_Name")%></td>
                                                <td><%# Eval("MaterialType") %></td>
                                                 <td><%# Eval("Canceledby")%></td>
                                                 <td><%# Eval("CancelReason")%></td>
                                                <td>
                                                     <asp:Label ID="lblStatus" style="width:100%" runat="server" class="form-control" Text='<%# Eval("Status") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("Std_PO_No")%>'>
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="btnPrint" class="btn btn-primary btn-sm fa fa-print" runat="server"
                                                        Text="" OnCommand="GridPrintClick" CommandArgument="Edit" CommandName='<%# Eval("Std_PO_No")%>'>
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="btnPrint_Sign" Visible="false" class="btn btn-primary btn-success fa fa-print" runat="server"
                                                        Text="" OnCommand="GridPrintClickWithSign" CommandArgument="Edit" CommandName='<%# Eval("Std_PO_No")%>'>
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButton1" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("Std_PO_No")%>'>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    
        </ContentTemplate>
    </asp:UpdatePanel>
  </div>  

</asp:Content>

