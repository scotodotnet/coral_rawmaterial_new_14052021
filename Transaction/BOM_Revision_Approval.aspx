﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BOM_Revision_Approval.aspx.cs" Inherits="Master_BOM_Revision_Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="content-wrapper">
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Material Planning Approval</span></h3>
                        </div>
                    
                        <div id="Div1" class="row" runat="server" style="padding-top:25px;padding-bottom:25px">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label class="control-label" for="Req_No">Material Planning Status</label>
                                    <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                        onselectedindexchanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="box-body no-padding">
                            <div class="table-responsive mailbox-messages">
                                <div class="col-md-12">
					                <div class="row">
                                        <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
			                                <HeaderTemplate>
                                                <table id="example" class="table table-hover table-striped">
                                                    <thead>
                                        <tr>
                                            <th>OrderNo</th>
                                            <th>Generator Model</th>
                                            <th>Revision Name</th>
                                            <th>Delivery Date</th>
                                            <th>Delivery CW</th>
                                            <th>Approve</th>
                                            <th>Cancel</th>
                                            <th>View</th>
                                            </tr>
                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("Order_No")%></td>
                                                    <td><%# Eval("GenModelNo")%></td>
                                                    <td><%# Eval("Revision_Name")%></td>
                                                    <td><%# Eval("Delivery_Date")%></td>
                                                    <td><%# Eval("Delivery_CW")%></td>
                                                    <td>
                                                        <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Order_No")%>' CommandName='<%# Eval("Order_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this PO details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="LinkButton1" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Order_No")%>' CommandName='<%# Eval("Order_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this PO details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-print"  
                                                            runat="server" Text="" OnCommand="GridEditPurRequestClick" 
                                                            CommandArgument='<%# Eval("Order_No")%>' CommandName='<%# Eval("Order_No")%>'>
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
			                        </asp:Repeater>
					            </div>
                                    <div class="row">
			            <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false">
			                <HeaderTemplate>
                                <table id="example" class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>OrderNo</th>
                                            <th>Generator Model</th>
                                            <th>Revision Name</th>
                                            <th>Delivery Date</th>
                                            <th>Delivery CW</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Order_No")%></td>
                                    <td><%# Eval("GenModelNo")%></td>
                                    <td><%# Eval("Revision_Name")%></td>
                                    <td><%# Eval("Delivery_Date")%></td>
                                    <td><%# Eval("Delivery_CW")%></td>   
                                    <td>
                                        <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-print"  
                                            runat="server" Text="" OnCommand="GridPrintView_Approval" 
                                            CommandArgument='<%# Eval("Order_No")%>' CommandName='<%# Eval("Order_No")%>'>
                                        </asp:LinkButton>
                                    </td>
                                        
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>                                
			            </asp:Repeater>
			        </div>
                                    <div class="row">
			            <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
			                <HeaderTemplate>
                                <table id="example" class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>OrderNo</th>
                                            <th>Generator Model</th>
                                            <th>Revision Name</th>
                                            <th>Delivery Date</th>
                                            <th>Delivery CW</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Order_No")%></td>
                                    <td><%# Eval("GenModelNo")%></td>
                                    <td><%# Eval("Revision_Name")%></td>
                                    <td><%# Eval("Delivery_Date")%></td>
                                    <td><%# Eval("Delivery_CW")%></td>   
                                    <td>
                                        <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-print"  
                                            runat="server" Text="" OnCommand="GridPrintView_Reject" 
                                            CommandArgument='<%# Eval("Order_No")%>' CommandName='<%# Eval("Order_No")%>'>
                                        </asp:LinkButton>
                                    </td>
                                        
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>                                
			            </asp:Repeater>
			        </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
    
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>

</asp:Content>

