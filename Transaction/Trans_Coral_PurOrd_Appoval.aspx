﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Coral_PurOrd_Appoval.aspx.cs" Inherits="Trans_Coral_PurOrd_Appoval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Purchase Order Approval (Coral)</span></h3>
                                <asp:Label runat="server" ID="lblErrorMsg" style="color:red;font-size:x-large" ></asp:Label>
                                </div>

                                <div class="row" runat="server" style="padding-top: 25px; padding-bottom: 25px">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="control-label" for="Req_No">Purchase Order Status</label>
                                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                                OnSelectedIndexChanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-3" id="divAppType" runat="server" visible="false">
                                            <label class="control-label" for="Req_No">Types Of Approval</label>
                                            <asp:DropDownList ID="ddlAppType" runat="server" class="form-control select2" AutoPostBack="True" OnSelectedIndexChanged="ddlAppType_SelectedIndexChanged">
                                                <asp:ListItem Value="1">Purchase Manager</asp:ListItem>
                                                <asp:ListItem Value="2">Vice President</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>


                                        <div class="col-md-3" runat="server" id="divCancelReason">
                                            <label class="control-label" for="Req_No">Cancel Reason</label>
                                            <asp:TextBox TextMode="MultiLine" ID="txtCancelReason" Style="resize:none" runat="server" 
                                                class="form-control">
                                            </asp:TextBox>
                                        </div>

                                        

                                    </div>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">

                                        <asp:Panel runat="server" ID="pnlPOPending">
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                     <td>SL. No</td>
                                                                    <th>PO No</th>
                                                                    <th>Date</th>
                                                                    <th>Supplier</th>
                                                                    <th>Material_Type</th>
                                                                    <th>Approve</th>
                                                                    <th>Reject</th>
                                                                    <th>View</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                             <td><%# Container.ItemIndex+1 %></td>
                                                            <td><%# Eval("Std_Po_No")%></td>
                                                            <td><%# Eval("Std_PO_Date")%></td>
                                                            <td><%# Eval("Supp_Name")%></td>
                                                            <td><%# Eval("Material_Type")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square" runat="server"
                                                                    Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Std_PO_No")%>' CommandName='<%# Eval("Std_PO_No")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this PO details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="LinkButton1" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Std_PO_No")%>' CommandName='<%# Eval("Std_PO_No")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this PO details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-external-link-square"
                                                                    runat="server" Text="" OnCommand="GridEditPurRequestClick"
                                                                    CommandArgument='<%# Eval("Std_PO_No")%>' CommandName='<%# Eval("Std_PO_No")%>'>
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel runat="server" ID="pnlApproval" Visible="false">
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" OnItemDataBound="Repeater_Approve_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sl._No</th>
                                                                    <th>PO_No</th>
                                                                    <th>Date</th>
                                                                    <th>Supplier</th>
                                                                    <th>Material_Type</th>
                                                                    <th>Approval_Status</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1 %></td>
                                                            <td><%# Eval("Std_PO_No")%></td>
                                                            <td><%# Eval("Std_PO_Date")%></td>
                                                            <td><%# Eval("Supp_Name")%></td>
                                                            <td><%# Eval("Material_Type")%></td>
                                                            <td>
                                                                <asp:Label ID="lblStatus" runat="server" class="form-control" Text='<%# Eval("Approval_Status") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel runat="server" ID="pnlReject" Visible="false">
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" >
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                  <th>Sl. No</th>
                                                                    <th>PO No</th>
                                                                    <th>Date</th>
                                                                    <th>Supplier</th>
                                                                    <th>Material_Type</th>
                                                                    <th>Cancel_Reason</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1 %></td>
                                                            <td><%# Eval("Std_PO_No")%></td>
                                                            <td><%# Eval("Std_PO_Date")%></td>
                                                            <td><%# Eval("Supp_Name")%></td>
                                                            <td><%# Eval("Material_Type")%></td>
                                                            <td><%# Eval("CancelReason")%></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                }
            });
        };
    </script>

</asp:Content>

