﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Sales_Transaction_Sales_Cust_Proforma_Inv_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Proforma Invoice Details";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        //bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "4", "Sales Proforma Invoice");

        //if (Rights_Check == false)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add New Purchase Request...');", true);
        //    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Request..');", true);
        //}
        //else
        //{
        //    //Session.Remove("Pur_RequestNo_Approval");
        //    //Session.Remove("Pur_Request_No_Amend_Approval");
        //    //Session.Remove("Pur_Request_No");
        //    //Response.Redirect("purchase_request.aspx");
        //    Session.Remove("Sales_Proforma_Invoice_No");
        //    Response.Redirect("Sales_Cust_Proforma_Inv_Sub.aspx");
        //}

        Session.Remove("Sales_Proforma_Invoice_No");
        Response.Redirect("Sales_Cust_Proforma_Inv_Sub.aspx");


    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool Rights_Check = false;
        bool ErrFlag = false;

        string Enquiry_No_Str = e.CommandName.ToString();
        Session.Remove("Sales_Proforma_Invoice_No");
        Session["Sales_Proforma_Invoice_No"] = Enquiry_No_Str;
        Response.Redirect("Sales_Cust_Proforma_Inv_Sub.aspx");

        //Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Purchase Request");

        //if (Rights_Check == false)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit Purchase Request...');", true);
        //}
        //else
        //{
        //    DataTable dtdpurchase = new DataTable();
        //    query = "select Status from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
        //    dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
        //    string status = dtdpurchase.Rows[0]["Status"].ToString();

        //    if (status == "" || status == "0")
        //    {
        //        string Enquiry_No_Str = e.CommandName.ToString();
        //        Session.Remove("Pur_RequestNo_Approval");
        //        Session.Remove("Pur_Request_No_Amend_Approval");
        //        Session.Remove("Pur_Request_No");
        //        Session["Pur_Request_No"] = Enquiry_No_Str;
        //        Response.Redirect("purchase_request.aspx");
        //    }
        //    else if (status == "2")
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Already Rejected..');", true);
        //        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Rejected..');", true);
        //    }
        //    else if (status == "3")
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details put in pending..');", true);
        //        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details put in pending..');", true);
        //    }
        //    else if (status == "1")
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Approved Purchase Request Cant Edit..');", true);
        //        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Purchase Request Cant Edit..');", true);
        //    }
        //}
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Sales_Inv_No,Sales_Inv_Date,CustomerName,Cus_Pur_Order_No,NetAmount from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        //Print View Open
        string RptName = "";
        string Trans_No = "";

        RptName = "Customer Proforma Invoice Format";
        Trans_No = e.CommandName.ToString();

        ResponseHelper.Redirect("~/Sales_Report/Sales_Report_Display.aspx?Trans_No=" + Trans_No + "&Genset_Model=" + "" + "&Customer_Name=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");

    }
}
