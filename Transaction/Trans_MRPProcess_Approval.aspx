﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_MRPProcess_Approval.aspx.cs" Inherits="Transaction_Trans_MRPProcess_Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                }
            });
        };
    </script>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
<div class="content-wrapper">
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            
        <section class="content">
           
            <!--/.row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>MRP Process Approval</span></h3>
                          <%--  <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div>--%>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-body no-padding">
                                <div class="table-responsive mailbox-messages">
                      <div class="row" runat="server" style="padding-top:25px;padding-bottom:25px">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label class="control-label" for="Req_No">Work Order Status</label>
                                    <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                        onselectedindexchanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                                    <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                        <HeaderTemplate>
                                            <table id="example" class="table table-hover table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>WorkOrderNo</th>
                                                        <th>OrderNo</th>
                                                        <th>GenaratorModel</th>
                                                        <th>PartName</th>
                                                        <th>Mode</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex+1 %></td>
                                               <td><%# Eval("Work_Order_No")%></td>
                                                <td><%# Eval("Order_No")%></td>
                                                <td><%# Eval("GenModel")%></td>
                                                <td><%# Eval("Part_Name")%></td>
                                              
                                                <td>
                                                 <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Work_Order_No")%>' CommandName='<%# Eval("Work_Order_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this MRP Process details?');">
                                                        </asp:LinkButton>
                                                   
                                                        <asp:LinkButton ID="LinkButton2" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                            Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Work_Order_No")%>' CommandName='<%# Eval("Work_Order_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this MRP Process details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>
                                    </asp:Repeater>

                                     <asp:Repeater ID="Repeater1" runat="server" Visible="false" EnableViewState="false">
                                        <HeaderTemplate>
                                            <table id="example" class="table table-hover table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                         <th>S.No</th>
                                                      <th>WorkOrderNo</th>
                                                        <th>OrderNo</th>
                                                        <th>GenaratorModel</th>
                                                        <th>PartName</th>
                                                       
                                                       
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex+1 %></td>
                                               <td><%# Eval("Work_Order_No")%></td>
                                                <td><%# Eval("Order_No")%></td>
                                                <td><%# Eval("GenModel")%></td>
                                                <td><%# Eval("Part_Name")%></td>
                                              
                                                
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    
        </ContentTemplate>
    </asp:UpdatePanel>
  </div>  

</asp:Content>

