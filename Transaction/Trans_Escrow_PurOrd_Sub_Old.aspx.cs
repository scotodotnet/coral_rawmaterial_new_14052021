﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using GeneralClass;


public partial class Trans_Escrow_PurOrd_Sub_Old : System.Web.UI.Page
{
    public static BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    public static string SessionCcode;
    public static string SessionLcode;
    public static string SessionUserID;
    public static string SessionUserName;
    public static string SessionStdPOOrderNo;
    public static string SessionStdPOOrderNot;
    public static string SessionFinYearCode;
    public static string SessionFinYearVal;
    static string sum;
    static Decimal OrderQty;
    string NameTypeRequest;
    public static string SessionPurRequestNoApproval;
    static string SAPNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Escrow Purchase Order";
            Initial_Data_Referesh();
            Delivery_Mode_Add();
            Load_Data_Empty_SuppRefDocNo();
            Load_Data_Empty_Dept();
            Load_Data_GST();
            Load_TaxData();
            Load_Data_Empty_Supp1();

            txtSupplierName_Select_SelectedIndexChanged(sender, e);

            txtDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy");

            if (Session["Std_PO_No"] == null)
            {
                SessionStdPOOrderNo = "";

                //RdpPOType.SelectedValue = "2";
                Load_Data_Purchase_Request_No();
                Load_Data_Empty_ItemCode();

            }
            else
            {
                SessionStdPOOrderNo = Session["Std_PO_No"].ToString();
                txtStdOrderNo.Text = SessionStdPOOrderNo;
                Load_Data_Purchase_Request_No();
                Load_Data_Empty_ItemCode();
                btnSearch_Click(sender, e);


            }
            if (Session["Transaction_No"] == null)
            {
                SessionStdPOOrderNot = "";

                //RdpPOType.SelectedValue = "1";
            }
            else
            {
                SessionStdPOOrderNot = Session["Transaction_No"].ToString();
                btnStd_Purchase_Click(sender, e);

                //RdpPOType.SelectedValue = "2";
            }

            if (Session["Pur_RequestNo_Approval"] == null)
            {
                SessionPurRequestNoApproval = "";

            }
            else
            {
                RdpPOType.SelectedValue = "3";
                SessionPurRequestNoApproval = Session["Pur_RequestNo_Approval"].ToString();
                txtStdOrderNo.Text = SessionPurRequestNoApproval;
                btnSearchView_Click(sender, e);
            }
        }
        Load_OLD_data();
    }

    private void Load_Data_GST()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtGST_Type.Items.Clear();
        SSQL = "Select *from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtGST_Type.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();
        //dr["GST_Type"] = "-Select-";
        //dr["GST_Type"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        txtGST_Type.DataTextField = "GST_Type";
        txtGST_Type.DataValueField = "GST_Type";
        txtGST_Type.DataBind();

    }

    private void Load_Data_Purchase_Request_No()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtRequestNo.Items.Clear();

        DataTable da_Customer = new DataTable();
        DataTable da_Order = new DataTable();
        string Cus_Po = "";
        string Bag_PO;
        DataTable dt = new DataTable();
        dt.Columns.Add("Pur_Request_No", typeof(string));

        if (Session["Std_PO_No"] == null)
        {
            SSQL = "Select PRM.Pur_Request_No,PRS.ItemCode,PRS.ReuiredQty From Trans_Escrow_PurRqu_Sub PRS ";
            SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Ccode=PRS.Ccode And PRM.Lcode=PRS.Lcode And ";
            SSQL = SSQL + " PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No ";
            SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " PRS.FinYearCode='" + SessionFinYearCode + "' And PRM.Ccode='" + SessionCcode + "' And  ";
            SSQL = SSQL + " PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";

            SSQL = SSQL + " And PRM.MaterialType='" + ddlMatType.SelectedItem.Text + "' Order by Pur_Request_No Asc";

            da_Customer = objdata.RptEmployeeMultipleDetails(SSQL);

            if (da_Customer.Rows.Count != 0)
            {
                for (int i = 0; i < da_Customer.Rows.Count; i++)
                {
                    string Req_No = ""; string Req_Item = ""; string Req_Qty = "0";
                    Req_No = da_Customer.Rows[i]["Pur_Request_No"].ToString();
                    Req_Item = da_Customer.Rows[i]["ItemCode"].ToString();
                    Req_Qty = da_Customer.Rows[i]["ReuiredQty"].ToString();

                    SSQL = "Select isnull(Sum(OrderQty),0) as PO_Qty from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                    SSQL = SSQL + " Pur_Request_No ='" + Req_No + "' And ItemCode='" + Req_Item + "'";

                    da_Order = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (da_Order.Rows.Count != 0)
                    {
                        Bag_PO = da_Order.Rows[0]["PO_Qty"].ToString();
                        if (Convert.ToDecimal(Bag_PO.ToString()) < Convert.ToDecimal(Req_Qty))
                        {
                            DataRow row = dt.NewRow();
                            row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                            dt.Rows.Add(row);
                        }
                    }
                    else
                    {
                        DataRow row = dt.NewRow();
                        row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                        dt.Rows.Add(row);
                    }
                }
            }
        }
        else
        {
            SSQL = "Select PRM.Pur_Request_No,PRS.ItemCode,PRS.ReuiredQty From Trans_Escrow_PurRqu_Sub PRS ";
            SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Ccode=PRS.Ccode And PRM.Lcode=PRS.Lcode And ";
            SSQL = SSQL + " PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No ";
            SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " PRS.FinYearCode='" + SessionFinYearCode + "' And PRM.Ccode='" + SessionCcode + "' ";
            SSQL = SSQL + " And PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";
            SSQL = SSQL + " And PRM.MaterialType='" + ddlMatType.SelectedItem.Text + "' Order by Pur_Request_No Asc";

            da_Customer = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_Customer.Rows.Count != 0)
            {
                for (int i = 0; i < da_Customer.Rows.Count; i++)
                {
                    string Req_No = ""; string Req_Item = ""; string Req_Qty = "0";
                    Req_No = da_Customer.Rows[i]["Pur_Request_No"].ToString();
                    Req_Item = da_Customer.Rows[i]["ItemCode"].ToString();
                    Req_Qty = da_Customer.Rows[i]["ReuiredQty"].ToString();

                    SSQL = "Select isnull(Sum(OrderQty),0) as PO_Qty From Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                    SSQL = SSQL + " Pur_Request_No ='" + Req_No + "'";
                    SSQL = SSQL + " And ItemCode='" + Req_Item + "' And Std_PO_No <> '" + txtStdOrderNo.Text + "'";

                    da_Order = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (da_Order.Rows.Count != 0)
                    {
                        Bag_PO = da_Order.Rows[0]["PO_Qty"].ToString();
                        if (Convert.ToDecimal(Bag_PO.ToString()) < Convert.ToDecimal(Req_Qty))
                        {
                            DataRow row = dt.NewRow();
                            row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                            dt.Rows.Add(row);
                        }
                    }
                    else
                    {
                        DataRow row = dt.NewRow();
                        row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                        dt.Rows.Add(row);
                    }
                }
            }
        }
        if (dt.Rows.Count != 0)
        {
            Main_DT = RemoveDuplicatesRecords(dt);
        }
        else
        {
            Main_DT = dt;
        }

        txtRequestNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Pur_Request_No"] = "-Select-";
        dr["Pur_Request_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtRequestNo.DataTextField = "Pur_Request_No";
        txtRequestNo.DataValueField = "Pur_Request_No";
        txtRequestNo.DataBind();

    }

    [WebMethod]
    private DataTable RemoveDuplicatesRecords(DataTable dt)
    {
        //Returns just 5 unique rows
        var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
        DataTable dt2 = UniqueRows.CopyToDataTable();
        return dt2;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }
        if (txtDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Date...');", true);
        }
        if (txtSupplierName_Select.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Supplier Name...');", true);
        }
        if (txtDeliveryDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Delivery Date...');", true);
        }
        //if (txtDeliveryMode.Value == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Select Delivery Mode...');", true);
        //}
        //if (txtPaymentMode.Value == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Select Payment Mode...');", true);
        //}
        if (txtDepartmentName.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Department Name...');", true);
        }

        if (ddlPurMode.SelectedItem.Text == "Purchase Request")
        {
            if (txtRequestNo.SelectedItem.Text == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Purchase Request No...');", true);
            }
        }

        //Check Supplier Name And Quotation No
        SSQL = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And QuotNo='" + txtSuppRefDocNo.Text + "' And SuppCode='" + txtSuppCodehide.Value + "'";

        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Supplier Name And Supp.Ref.Doc.No...');", true);
        //}
        //Check Department Name
        //SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        //qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Department Name...');", true);
        //}

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = "";
                if (ddlMatType.SelectedItem.Text == "Tools")
                {
                    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Asset Purchase Order", SessionFinYearVal, "1");
                }
                else
                {
                    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");
                }

                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtStdOrderNo.Text = Auto_Transaction_No;
                }
            }
        }

        //if (btnSave.Text == "Update")
        //{
        //    SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        //    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "' And PO_Status='1'";

        //    DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        //    if (DT_Check.Rows.Count != 0)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Standard Purchase Order Details get Approved Cannot Update it..');", true);
        //    }
        //}


        if (!ErrFlag)
        {
            SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                SSQL = "Delete from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Delete from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Delete from Trans_Escrow_PurOrd_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            string CurrencyType = "";

            //if(rdpCurrencyType.SelectedIndex != 1)
            //{
            //    CurrencyType = "INR";
            //}
            //else if (rdpCurrencyType.SelectedIndex == 1)
            //{
            //    CurrencyType = "EUR";
            //}

            //Response.Write(strValue);

            //" + txtPaymentMode.Value + "
            //Insert Main Table
            SSQL = "Insert Into Trans_Escrow_PurOrd_Main(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,";
            SSQL = SSQL + " Supp_Qtn_Date,Pur_Request_No,Pur_Request_Date,DeliveryMode,DeliveryDate,DeliveryAt,PaymentMode,DeptCode,DeptName,";
            SSQL = SSQL + " PaymentTerms,Description,Note,Others,TotalQuantity,TotalAmt,Discount,TaxPer,TaxAmount,OtherCharge,AddOrLess,NetAmount,";
            SSQL = SSQL + " UserID,UserName,PO_Type,PO_Status,ModeOfTransfer,ChequeNo,ChequeDate,ChequeAmt,TaxType,CurrencyType) Values('" + SessionCcode + "',";
            SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "',";
            SSQL = SSQL + " '" + txtDate.Text + "','" + txtSupplierName_Select.SelectedValue + "','" + txtSupplierName_Select.SelectedItem.Text + "',";
            SSQL = SSQL + " '" + txtSuppRefDocNo.Text + "','" + txtDocDate.Text + "','" + txtRequestNo.Text + "','" + txtRequestDate.Text + "',";
            SSQL = SSQL + " '','" + txtDeliveryDate.Text + "','" + txtDeliveryAt.Text + "',";
            SSQL = SSQL + " '','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "',";
            SSQL = SSQL + " '" + txtPaymentTerms.Text + "','" + txtDescription.Text + "','" + txtNote1.Text + "','" + txtOthers1.Text + "',";
            SSQL = SSQL + " '" + txtTotQty.Text + "','" + txtTotAmt.Text + "','0','0','0','0','" + txtAddOrLess.Text + "','" + txtNetAmt.Text + "',";
            SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','" + ddlPurMode.SelectedItem.Text + "',";

            if (RdpPOType.SelectedItem.Text == "Special")
            {
                SSQL = SSQL + " '4',";
            }
            else
            {
                SSQL = SSQL + " '0',";
            }
            SSQL = SSQL + " '" + txtModeOfTransfer.Text + "','" + txtChequeNo.Text + "','" + txtChequeDate.Text + "','" + txtChequeAmt.Text + "',";
            SSQL = SSQL + " '','" + CurrencyType + "')";
            //" + RdpTaxType.SelectedValue + "
            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];

            for (int i = 0; i < Repeater1.Items.Count; i++)
            {
                Label lblSAPNo = Repeater1.Items[i].FindControl("lblSAPNo") as Label;
                Label lblItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                Label lblItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                Label lblUOM = Repeater1.Items[i].FindControl("lblUOM") as Label;
                Label lblRQty = Repeater1.Items[i].FindControl("lblRQty") as Label;
                Label lblMdlQty = Repeater1.Items[i].FindControl("lblMdlQty") as Label;
                Label lblRquQty = Repeater1.Items[i].FindControl("lblRquQty") as Label;
                TextBox txtOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;
                TextBox txtRquDate = Repeater1.Items[i].FindControl("txtRDate") as TextBox;
                TextBox txtGrdRateINR = Repeater1.Items[i].FindControl("txtGrdRateINR") as TextBox;
                TextBox txtGrdRateEUR = Repeater1.Items[i].FindControl("txtGrdRateEUR") as TextBox;
                Label lblRemarks = Repeater1.Items[i].FindControl("lblRemarks") as Label;
                Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                Label lblDiscPer = Repeater1.Items[i].FindControl("lblDiscPer") as Label;
                Label lblDisc = Repeater1.Items[i].FindControl("lblDisc") as Label;
                Label lblTaxPer = Repeater1.Items[i].FindControl("lblTaxPer") as Label;
                Label lblTaxAmt = Repeater1.Items[i].FindControl("lblTaxAmt") as Label;
                Label lblCGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                Label lblCGSTAmt = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                Label lblSGSTAmt = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                Label lblIGSTAmt = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblGSTPer = Repeater1.Items[i].FindControl("lblGSTPer") as Label;
                Label lblOtherAmt = Repeater1.Items[i].FindControl("lblOtherAmt") as Label;
                Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                string Rate = "";

                //if (rdpCurrencyType.SelectedIndex == 0)
                //{
                //    Rate = txtGrdRateINR.Text;
                //}
                //else if(rdpCurrencyType.SelectedIndex != 0)
                //{
                //    Rate = txtGrdRateEUR.Text;
                //}

                SSQL = "Insert Into Trans_Escrow_PurOrd_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Pur_Request_No,";
                SSQL = SSQL + " Pur_Request_Date,ItemCode,ItemName,SAPNo,UOMCode,ReuiredQty,ModelQty,RquQty,OrderQty,ReuiredDate,Rate,Remarks,ItemTotal,";
                SSQL = SSQL + " Discount_Per,Discount,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount,OtherCharge,LineTotal,";
                SSQL = SSQL + " GstPer,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "', ";
                SSQL = SSQL + " '" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','" + txtRequestNo.Text + "',";
                SSQL = SSQL + " '" + txtRequestDate.Text + "','" + lblItemCode.Text + "','" + lblItemName.Text + "','" + lblSAPNo.Text + "',";
                SSQL = SSQL + " '" + lblUOM.Text + "','" + lblRQty.Text + "','" + lblMdlQty.Text + "','" + lblRquQty.Text + "',";
                SSQL = SSQL + " '" + txtOrdQty.Text + "','" + txtRquDate.Text + "','" + Rate + "','" + lblRemarks.Text + "',";
                SSQL = SSQL + " '" + lblItemTot.Text + "','" + lblDiscPer.Text + "','" + lblDisc.Text + "','" + lblCGSTPer.Text + "',";
                SSQL = SSQL + " '" + lblCGSTAmt.Text + "','" + lblSGSTPer.Text + "','" + lblSGSTAmt.Text + "','" + lblIGSTPer.Text + "',";
                SSQL = SSQL + " '" + lblIGSTAmt.Text + "','" + lblOtherAmt.Text + "','" + lblLineTot.Text + "','" + lblGSTPer.Text + "',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (RdpPOType.SelectedItem.Text == "Special")
            {
                //Insert Purchase Request Approval Table
                //SSQL = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                //SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','Std/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
                //SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                //objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (RdpPOType.SelectedItem.Text == "Direct" || RdpPOType.SelectedItem.Text == "Req/Amend")
            {
                //Insert Purchase Request Approval Table
                SSQL = "Insert Into Trans_Escrow_PurOrd_Approve(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','Std/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Details Updated Successfully');", true);
            }


            //DataTable dtd_REqNo = new DataTable();

            ////if (!ErrFlag)
            ////{
            //SSQL = "select Pur_Request_No from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "'";
            //SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //SSQL = SSQL + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";

            //dtd_REqNo = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (dtd_REqNo.Rows.Count > 0)
            //{
            //    string Pur_Req_No = dtd_REqNo.Rows[0]["Pur_Request_No"].ToString();
            //    DataTable Std_Req_Apprv = new DataTable();

            //    DataTable Pur_Req_Apprv = new DataTable();

            //    //Check Purchase Req Item and Purchase Order Item are same code start

            //    if (Pur_Req_No != "")
            //    {

            //        SSQL = "Select *from Trans_Coral_PurOrd_Sub where Pur_Request_No='" + Pur_Req_No + "'";
            //        Std_Req_Apprv = objdata.RptEmployeeMultipleDetails(SSQL);

            //        string ss = Pur_Req_No.Substring(0, 3);

            //        if (ss == "PR/")
            //        {
            //            SSQL = "Select *from Pur_Request_Main_Sub where Pur_Request_No ='" + Pur_Req_No + "'";
            //            Pur_Req_Apprv = objdata.RptEmployeeMultipleDetails(SSQL);
            //        }
            //        else
            //        {
            //            SSQL = "Select *from Pur_Request_Main_Sub where Pur_Request_No ='" + Pur_Req_No + "'";
            //            Pur_Req_Apprv = objdata.RptEmployeeMultipleDetails(SSQL);
            //        }


            //        if (Std_Req_Apprv.Rows.Count == Pur_Req_Apprv.Rows.Count)
            //        {
            //            SSQL = "Update Pur_Request_Approval set PO_Status='1' where Ccode='" + SessionCcode + "'";
            //            SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //            SSQL = SSQL + "  And Transaction_No='" + Pur_Req_No + "'";
            //            objdata.RptEmployeeMultipleDetails(SSQL);
            //        }
            //        else
            //        {
            //            SSQL = "Update Pur_Request_Approval set PO_Status='0' where Ccode='" + SessionCcode + "'";
            //            SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //            SSQL = SSQL + "  And Transaction_No='" + Pur_Req_No + "'";
            //            objdata.RptEmployeeMultipleDetails(SSQL);
            //        }
            //    }

            //}

            //Clear_All_Field();
            Session["Std_PO_No"] = txtStdOrderNo.Text;
            btnSave.Text = "Update";
            //Session["Transaction_No"] = txtStdOrderNo.Text;
            //Load_Data_Enquiry_Grid();
            Response.Redirect("Trans_Escrow_PurOrd_Main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
        txtGST_Type_SelectedIndexChanged(sender, e);
    }

    private void Clear_All_Field()
    {
        txtStdOrderNo.Text = ""; txtDate.Text = ""; txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
        txtSuppRefDocNo.SelectedItem.Text = "-Select-"; txtDocDate.Text = ""; txtDeliveryDate.Text = "";
        txtDeliveryAt.Text = ""; txtDepartmentName.SelectedItem.Text = "-Select-";
        //txtPaymentMode.Value = "";
        txtPaymentTerms.Text = ""; txtDescription.Text = ""; txtNote1.Text = ""; txtOthers1.Text = "";
        txtRequestNo.Text = ""; txtRequestDate.Text = ""; txtItemCodeHide.Value = ""; txtItemName.Text = "";
        txtRequiredQty.Text = ""; txtOrderQty.Text = ""; txtRequiredDate.Text = ""; txtRateINR.Text = "0.0"; txtRateEUR.Text = "0.0";
        txtDiscount.Text = ""; txtTax.Text = "0"; txtOtherCharge.Text = "0"; txtRemarks.Text = "";
        txtTaxAmt.Text = "0"; txtTotAmt.Text = "0.0"; txtNetAmt.Text = "0"; RdpPOType.SelectedValue = "2";
        txtModeOfTransfer.Text = "";
        txtChequeAmt.Text = "";
        txtChequeDate.Text = "";
        txtChequeNo.Text = "";

        txtAddOrLess.Text = "0";

        txtItemTotal.Text = "0.00";
        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
        txtFinal_Total_Amt.Text = "0.00";

        TotalGstPer_Hidden.Value = "0";


        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Std_PO_No");
        Session.Remove("Transaction_No");
        Session.Remove("Pur_RequestNo_Approval");
        //Load_Data_Enquiry_Grid();
        //txtDeptCodeHide.Value = "";
        if (RdpPOType.SelectedValue == "1")
        {
            txtRequestNo.Enabled = false;
            txtRequestDate.Enabled = false;
        }
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

        if (ddlPurMode.SelectedItem.Text == "Purchase Request")
        {
            NameTypeRequest = "Purchase Request";

            if (NameTypeRequest.Trim() == "Purchase Request")
            {
                //SSQL = "Select * from Trans_Coral_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
                //SSQL = SSQL + "And Pur_Request_No='" + txtRequestNo.Text + "'";
                //qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                //if (qry_dt.Rows.Count != 0)
                //{
                //    ErrFlag = true;
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Item Details Already Exists in Purchase Order..');", true);
                //    txtItemCodeHide.Value = ""; txtItemName.Text = "";
                //    txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                //    txtRemarks.Text = "";

                //}

                SSQL = "Select * from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (qry_dt.Rows.Count == 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Check with Purchase Request No..');", true);
                }

                //check with Item Code And Item Name

                SSQL = "Select * from Trans_Escrow_PurRqu_sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' ";
                SSQL = SSQL + " And ItemName='" + txtItemName.Text + "' And Pur_Request_No='" + txtRequestNo.Text + "'";

                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (qry_dt.Rows.Count == 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Check with Item Details..');", true);
                }

            }

            if (!ErrFlag)
            {
                //UOM Code get
                string UOM_Code_Str = qry_dt.Rows[0]["UOMCode"].ToString();
                //string SAPNo = qry_dt.Rows[0]["SAPNo"].ToString();

                //string Line_Total = "";
                //Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
                //Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();
                //Total_Calculate();

                // check view state is not null  
                if (ViewState["ItemTable"] != null)
                {
                    //get datatable from view state   
                    dt = (DataTable)ViewState["ItemTable"];

                    //check Item Already add or not
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                        }
                    }

                    if (!ErrFlag)
                    {
                        dr = dt.NewRow();
                        dr["SAPNo"] = SAPNo;
                        dr["ItemCode"] = txtItemCodeHide.Value;
                        dr["ItemName"] = txtItemName.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["ModelQty"] = txtModelQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["ReuiredDate"] = txtRequiredDate.Text;
                        dr["RateINR"] = txtRateINR.Text;
                        dr["RateEUR"] = txtRateEUR.Text;
                        dr["Remarks"] = txtRemarks.Text;

                        dr["ItemTotal"] = txtItemTotal.Text;

                        dr["Discount_Per"] = txtDiscount_Per.Text;
                        dr["Discount"] = txtDiscount_Amount.Text;
                        dr["TaxPer"] = txtVAT_Per.Text;
                        dr["TaxAmount"] = txtVAT_AMT.Text;
                        dr["CGSTPer"] = txtCGSTPer.Text;
                        dr["CGSTAmount"] = txtCGSTAmt.Text;
                        dr["SGSTPer"] = txtSGSTPer.Text;
                        dr["SGSTAmount"] = txtSGSTAmt.Text;
                        dr["IGSTPer"] = txtIGSTPer.Text;
                        dr["IGSTAmount"] = txtIGSTAmt.Text;
                        dr["GstPer"] = TotalGstPer_Hidden.Value;
                        dr["OtherCharge"] = txtOtherCharge.Text;
                        dr["LineTotal"] = txtFinal_Total_Amt.Text;

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();
                        Totalsum();
                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRateINR.Text = "0.0"; txtRateEUR.Text = "0.00";
                        txtRemarks.Text = "";
                        TotalGstPer_Hidden.Value = "0";
                        txtItemTotal.Text = "0.00";

                        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                        txtFinal_Total_Amt.Text = "0.00";
                        txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                        txtGST_Type_SelectedIndexChanged(sender, e);
                    }
                    else
                    {
                        if (NameTypeRequest.Trim() == "Request")
                        {
                            SSQL = "Select * from Trans_Escrow_PurRqu_sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (qry_dt.Rows.Count != 0)
                            {
                                txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                            }
                        }
                        else if (NameTypeRequest.Trim() == "Amend")
                        {
                            SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (qry_dt.Rows.Count != 0)
                            {
                                txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                            }

                        }
                    }
                }
                else
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCodeHide.Value;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["ReuiredQty"] = txtRequiredQty.Text;
                    dr["OrderQty"] = txtOrderQty.Text;
                    dr["ReuiredDate"] = txtRequiredDate.Text;
                    dr["RateINR"] = txtRateINR.Text;
                    dr["RateEUR"] = txtRateEUR.Text;
                    dr["Remarks"] = txtRemarks.Text;

                    dr["ItemTotal"] = txtItemTotal.Text;
                    dr["Discount_Per"] = txtDiscount_Per.Text;
                    dr["Discount"] = txtDiscount_Amount.Text;
                    dr["TaxPer"] = txtVAT_Per.Text;
                    dr["TaxAmount"] = txtVAT_AMT.Text;
                    dr["CGSTPer"] = txtCGSTPer.Text;
                    dr["CGSTAmount"] = txtCGSTAmt.Text;
                    dr["SGSTPer"] = txtSGSTPer.Text;
                    dr["SGSTAmount"] = txtSGSTAmt.Text;
                    dr["IGSTPer"] = txtIGSTPer.Text;
                    dr["IGSTAmount"] = txtIGSTAmt.Text;
                    dr["GstPer"] = TotalGstPer_Hidden.Value;
                    dr["OtherCharge"] = txtOtherCharge.Text;
                    dr["LineTotal"] = txtFinal_Total_Amt.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    Totalsum();

                    txtItemCodeHide.Value = ""; txtItemName.Text = "";
                    txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRateINR.Text = "0.0"; txtRateEUR.Text = "0.00";
                    txtRemarks.Text = "";
                    TotalGstPer_Hidden.Value = "0";
                    txtItemTotal.Text = "0.00";
                    txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                    txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                    txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                    txtFinal_Total_Amt.Text = "0.00";
                    txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                    txtGST_Type_SelectedIndexChanged(sender, e);
                }
            }
            else
            {
                if (NameTypeRequest.Trim() == "Request")
                {
                    SSQL = "Select * from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (qry_dt.Rows.Count != 0)
                    {
                        txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                        txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                        txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                    }
                }
                else if (NameTypeRequest.Trim() == "Amend")
                {
                    SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (qry_dt.Rows.Count != 0)
                    {
                        txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                        txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                        txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                    }

                }
            }
        }
        else
        {

            if (txtOrderQty.Text == "0.0" || txtOrderQty.Text == "0" || txtOrderQty.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Order Qty...');", true);
            }


            if (txtRateEUR.Text == "0.0" || txtRateEUR.Text == "0" || txtRateEUR.Text == "" || txtRateINR.Text == "0.0" || txtRateINR.Text == "0" || txtRateINR.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Rate...');", true);
            }

            NameTypeRequest = "Direct Purchase";

            if (NameTypeRequest.Trim() == "Direct Purchase")
            {
                if (!ErrFlag)
                {
                    //UOM Code get
                    string UOM_Code_Str = hfUOM.Value;
                    string Line_Total = "";
                    Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRateINR.Text)).ToString();
                    Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRateEUR.Text)).ToString();
                    Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();
                    Total_Calculate();

                    // check view state is not null  
                    if (ViewState["ItemTable"] != null)
                    {
                        //get datatable from view state   
                        dt = (DataTable)ViewState["ItemTable"];

                        //check Item Already add or not
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                            }
                        }
                        if (!ErrFlag)
                        {
                            dr = dt.NewRow();
                            dr["ItemCode"] = txtItemCodeHide.Value;
                            dr["ItemName"] = txtItemName.Text;
                            dr["UOMCode"] = UOM_Code_Str;
                            dr["ReuiredQty"] = txtRequiredQty.Text;
                            dr["OrderQty"] = txtOrderQty.Text;
                            dr["ReuiredDate"] = txtRequiredDate.Text;
                            dr["RateINR"] = txtRateINR.Text;
                            dr["RateEUR"] = txtRateEUR.Text;
                            dr["Remarks"] = txtRemarks.Text;

                            dr["ItemTotal"] = txtItemTotal.Text;

                            dr["Discount_Per"] = txtDiscount_Per.Text;
                            dr["Discount"] = txtDiscount_Amount.Text;
                            dr["TaxPer"] = txtVAT_Per.Text;
                            dr["TaxAmount"] = txtVAT_AMT.Text;
                            dr["CGSTPer"] = txtCGSTPer.Text;
                            dr["CGSTAmount"] = txtCGSTAmt.Text;
                            dr["SGSTPer"] = txtSGSTPer.Text;
                            dr["SGSTAmount"] = txtSGSTAmt.Text;
                            dr["IGSTPer"] = txtIGSTPer.Text;
                            dr["IGSTAmount"] = txtIGSTAmt.Text;
                            dr["GstPer"] = TotalGstPer_Hidden.Value;
                            dr["OtherCharge"] = txtOtherCharge.Text;
                            dr["LineTotal"] = txtFinal_Total_Amt.Text;

                            dt.Rows.Add(dr);
                            ViewState["ItemTable"] = dt;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();
                            Totalsum();
                            txtItemCodeHide.Value = ""; txtItemName.Text = "";
                            txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRateEUR.Text = "0.0"; txtRateINR.Text = "0.0";
                            txtRemarks.Text = "";
                            TotalGstPer_Hidden.Value = "0";
                            txtItemTotal.Text = "0.00";
                            txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                            txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                            txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                            txtFinal_Total_Amt.Text = "0.00";
                            txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                            txtGST_Type_SelectedIndexChanged(sender, e);
                        }
                        else
                        {
                            if (NameTypeRequest.Trim() == "Request")
                            {
                                SSQL = "Select * from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (qry_dt.Rows.Count != 0)
                                {
                                    txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                    txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                    txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                }
                            }
                            else if (NameTypeRequest.Trim() == "Amend")
                            {
                                SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (qry_dt.Rows.Count != 0)
                                {
                                    txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                    txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                    txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                }

                            }
                        }
                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["ItemCode"] = txtItemNameSelect.SelectedValue;
                        dr["ItemName"] = txtItemNameSelect.SelectedItem.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["ReuiredDate"] = txtRequiredDate.Text;
                        dr["RateINR"] = txtRateINR.Text;
                        dr["RateEUR"] = txtRateEUR.Text;
                        dr["Remarks"] = txtRemarks.Text;

                        dr["ItemTotal"] = txtItemTotal.Text;
                        dr["Discount_Per"] = txtDiscount_Per.Text;
                        dr["Discount"] = txtDiscount_Amount.Text;
                        dr["TaxPer"] = txtVAT_Per.Text;
                        dr["TaxAmount"] = txtVAT_AMT.Text;
                        dr["CGSTPer"] = txtCGSTPer.Text;
                        dr["CGSTAmount"] = txtCGSTAmt.Text;
                        dr["SGSTPer"] = txtSGSTPer.Text;
                        dr["SGSTAmount"] = txtSGSTAmt.Text;
                        dr["IGSTPer"] = txtIGSTPer.Text;
                        dr["IGSTAmount"] = txtIGSTAmt.Text;
                        dr["GstPer"] = TotalGstPer_Hidden.Value;
                        dr["OtherCharge"] = txtOtherCharge.Text;
                        dr["LineTotal"] = txtFinal_Total_Amt.Text;

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();

                        Totalsum();

                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = "";
                        txtRateINR.Text = "0.0"; txtRateEUR.Text = "0.0";
                        txtRemarks.Text = "";
                        TotalGstPer_Hidden.Value = "0";
                        txtItemTotal.Text = "0.00";
                        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                        txtFinal_Total_Amt.Text = "0.00";
                        txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                        txtGST_Type_SelectedIndexChanged(sender, e);
                    }
                }
                else
                {
                    if (NameTypeRequest.Trim() == "Request")
                    {
                        SSQL = "Select * from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (qry_dt.Rows.Count != 0)
                        {
                            txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                            txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                            txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                        }
                    }
                    else if (NameTypeRequest.Trim() == "Amend")
                    {
                        SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (qry_dt.Rows.Count != 0)
                        {
                            txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                            txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                            txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                        }

                    }
                }
            }
        }
    }


    public void Totalsum()
    {
        sum = "0";
        OrderQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
            txtTotAmt.Text = sum;
            OrderQty = Convert.ToDecimal(OrderQty) + Convert.ToDecimal(dt.Rows[i]["OrderQty"]);
            txtTotQty.Text = OrderQty.ToString();
        }
        Final_Total_Calculate();

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ModelQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("RateINR", typeof(string)));
        dt.Columns.Add(new DataColumn("RateEUR", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount_Per", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("OtherCharge", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("GstPer", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();
        txtTax.Text = "0";
        txtTaxAmt.Text = "0";
        txtDiscount.Text = "0";
        txtOthers1.Text = "0";
        txtNetAmt.Text = "0";
    }

    protected void btnStd_Purchase_Click(object sender, EventArgs e)
    {
        //string SSQL = "";
        //DataTable Main_DT = new DataTable();
        //SSQL = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + SessionStdPOOrderNot + "'";
        //Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (Main_DT.Rows.Count != 0)
        //{
        //    txtRequestNo.Text = Main_DT.Rows[0]["Transaction_No"].ToString();
        //    txtRequestDate.Text = Main_DT.Rows[0]["Date"].ToString();
        //}
        //elsep
        //{
        //    Clear_All_Field();
        //}
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {

            //if (Main_DT.Rows[0]["PO_Status"].ToString() == "1")
            //{
            //    lblStatus.Text = "Approved by : " + Main_DT.Rows[0]["Approvedby"].ToString();
            //}
            //else if (Main_DT.Rows[0]["PO_Status"].ToString() == "2")
            //{
            //    lblStatus.Text = "Cancelled by : " + Main_DT.Rows[0]["Approvedby"].ToString();
            //}
            //else if (Main_DT.Rows[0]["PO_Status"].ToString() == "3" || Main_DT.Rows[0]["PO_Status"].ToString() == "0" || Main_DT.Rows[0]["PO_Status"].ToString() == "5" || Main_DT.Rows[0]["PO_Status"].ToString() == "")
            //{
            //    lblStatus.Text = "Pending..";
            //}
            //else if (Main_DT.Rows[0]["PO_Status"].ToString() == "6")
            //{
            //    lblStatus.Text = "Rejected by FM..";
            //}


            txtDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSupplierName_Select.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            //txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            //txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote1.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers1.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            txtTotQty.Text = Main_DT.Rows[0]["TotalQuantity"].ToString();
            //txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
            txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
            //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
            txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtRequestNo.SelectedItem.Text = Main_DT.Rows[0]["Pur_Request_No"].ToString();
            txtRequestDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();


            if (Main_DT.Rows[0]["PO_Type"].ToString() == "Purchase Request")
            {
                ddlPurMode.SelectedItem.Text = Main_DT.Rows[0]["PO_Type"].ToString();
                pnlPurRqu.Visible = true;
                pnlDirectOrd.Visible = false;
            }

            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Direct")
            {
                ddlPurMode.SelectedItem.Text = Main_DT.Rows[0]["PO_Type"].ToString();
                pnlPurRqu.Visible = false;
                pnlDirectOrd.Visible = true;
            }

            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Req/Amend")
            {
                RdpPOType.SelectedValue = "2";
            }

            //txtModeOfTransfer.Text = Main_DT.Rows[0]["ModeOfTransfer"].ToString();
            //txtChequeNo.Text = Main_DT.Rows[0]["ChequeNo"].ToString();
            //txtChequeDate.Text = Main_DT.Rows[0]["ChequeDate"].ToString();
            //txtChequeAmt.Text = Main_DT.Rows[0]["ChequeAmt"].ToString();
            //RdpTaxType.SelectedValue = Main_DT.Rows[0]["TaxType"].ToString();

            //Trans_Coral_PurOrd_Sub Table Load
            DataTable dt = new DataTable();

            SSQL = "Select SAPNo,ItemCode,ItemName,UOMCode,ModelQty,ReuiredQty,OrderQty,ReuiredDate, ";


            if (Main_DT.Rows[0]["CurrencyType"].ToString() == "INR")
            {
                SSQL = SSQL + " Rate RateINR,0 RateEUR, ";
            }
            else
            {
                SSQL = SSQL + " 0 RateINR,Rate RateEUR, ";
            }

            SSQL = SSQL + "Remarks,ItemTotal,Discount_Per,Discount,TaxPer,TaxAmount,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,";
            SSQL = SSQL + " IGSTAmount,BDUTaxPer,BDUTaxAmount,OtherCharge,LineTotal,GstPer";
            SSQL = SSQL + " from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_Escrow_PurOrd_Main.aspx");
    }

    private void Delivery_Mode_Add()
    {
        //string SSQL = "";
        //DataTable Main_DT = new DataTable();
        //ddlDeliveryMode.Items.Clear();

        //SSQL = "Select * from MstDeliveryMode where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        //SSQL = SSQL + " Order by DeliveryMode Asc";

        //Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //ddlDeliveryMode.Items.Add("-Select-");
        //for (int i = 0; i < Main_DT.Rows.Count; i++)
        //{
        //    ddlDeliveryMode.Items.Add(Main_DT.Rows[i]["DeliveryMode"].ToString());
        //}
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Escrow PO Approval");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approve Std Purchase Order...');", true);
        }
        //User Rights Check End

        SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('First You have to Register this Std Purchase Order Details..');", true);
        }

        SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "' And PO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Std Purchase Order Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Update Trans_Escrow_PurOrd_Main set PO_Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);


            SSQL = "Update Trans_Escrow_PurOrd_Approve set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);


            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Std Purchase Order Details Approved Successfully..');", true);
        }
    }

    protected void txtTax_TextChanged(object sender, EventArgs e)
    {
        txtTaxAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) * (Convert.ToDecimal(txtTax.Text) / 100)).ToString();

        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }


    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }


    protected void btnSearchView_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {

            txtDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            //txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            //txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote1.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers1.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            //txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
            txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
            //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
            txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtRequestNo.Text = Main_DT.Rows[0]["Pur_Request_No"].ToString();
            txtRequestDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();

            if (Main_DT.Rows[0]["PO_Type"].ToString() == "Special")
            {
                RdpPOType.SelectedValue = "3";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Direct")
            {
                RdpPOType.SelectedValue = "1";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Req/Amend")
            {
                RdpPOType.SelectedValue = "2";
            }


            //Trans_Coral_PurOrd_Sub Table Load
            DataTable dt = new DataTable();

            SSQL = "Select *from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Visible = false;
        }
        else
        {
            Clear_All_Field();
        }
    }


    private void Load_Data_Empty_Supp1()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status='Add' And LedgerGrpName='Supplier' and LedgerName <> 'Enercon'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtSupplierName_Select.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        txtSupplierName_Select.DataTextField = "LedgerName";
        txtSupplierName_Select.DataValueField = "LedgerCode";
        txtSupplierName_Select.DataBind();

        txtSupplierName_Select.SelectedValue = DT.Rows[0]["LedgerCode"].ToString();
        txtSupplierName_Select.SelectedItem.Text = DT.Rows[0]["LedgerName"].ToString();
    }




    protected void txtSupplierName_Select_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        if (txtSupplierName_Select.SelectedValue != "-Select-")
        {
            SSQL = "Select * from Acc_Mst_Ledger Where LedgerName='" + txtSupplierName_Select.SelectedItem.Text + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            txtSuppCodehide.Value = txtSupplierName_Select.SelectedValue;
            txtSupplierName.Text = txtSupplierName_Select.SelectedItem.Text;
            txtPaymentTerms.Text = DT.Rows[0]["TC"].ToString();
        }
    }


    protected void btnSuppRefDocNo_Click(object sender, EventArgs e)
    {
        //modalPop_SuppRefDocNo.Show();
    }

    private void Load_Data_Empty_SuppRefDocNo()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SuppCode='" + txtSuppCodehide.Value + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtSuppRefDocNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["QuotNo"] = "-Select-";
        //dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSuppRefDocNo.DataTextField = "QuotNo";
        //txtDeptCode.DataValueField = "DeptCode";
        txtSuppRefDocNo.DataBind();

    }

    protected void GridViewClick_SuppRefDocNo(object sender, CommandEventArgs e)
    {
        txtSuppRefDocNo.Text = Convert.ToString(e.CommandArgument);
        txtDocDate.Text = Convert.ToString(e.CommandName);
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }

    private void Load_Data_Empty_Dept()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        // txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        // txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    private void Load_Data_Empty_ItemCode()
    {
        string SSQL = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        string ss = "";
        string PO_Type = "";
        string Request_No = "";

        PO_Type = ddlPurMode.SelectedValue;

        Request_No = txtRequestNo.Text;
        ss = Request_No.Substring(0, 3);


        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");
        DT.Columns.Add("ReuiredQty");
        DT.Columns.Add("ReuiredDate");

        DataTable DT1 = new DataTable();
        DT1.Columns.Add("ItemCode");
        DT1.Columns.Add("ItemName");

        if (ddlMatType.SelectedItem.Text == "Tools")
        {
            SSQL = "Select ItemCode,ItemName,ReuiredQty,ReuiredDate from Trans_Escrow_PurRqu_Sub Where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + "  and  Lcode ='" + SessionLcode + "' and Pur_Request_No='" + Request_No + "' ";
        }
        else
        {
            SSQL = "Select PRS.ItemCode,PRS.ItemName,PRS.ReuiredQty,PRS.ReuiredDate From Trans_Escrow_PurRqu_Sub PRS ";
            SSQL = SSQL + " Inner join Acc_Mst_Ledger_BOMList BOM on BOM.ItemName=PRS.ItemName and BOM.Ccode=PRS.Ccode ";
            SSQL = SSQL + " Where PRS.Ccode='" + SessionCcode + "' And ";
            SSQL = SSQL + " PRS.Lcode ='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "' And ";
            SSQL = SSQL + " PRS.Pur_Request_No='" + Request_No + "' And BOM.LedgerName='" + txtSupplierName_Select.SelectedItem.Text + "' ";
        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();
    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);

    }
    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCodeHide.Value = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;

            string SSQL = "";
            string Comp_Code = "";
            string Loc_Code = "";
            string Fin_Year_Code = "";
            string ss = "";
            string PO_Type = "";
            string Request_No = "";
            DataTable DT = new DataTable();


            PO_Type = RdpPOType.SelectedValue;
            Request_No = txtRequestNo.SelectedValue;


            if (ddlPurMode.SelectedItem.Text == "Purchase Request")
            {

                if (ddlMatType.SelectedItem.Text == "Tools")
                {
                    SSQL = "Select ReuiredQty,NoOfModel,SumRquQty,ReuiredDate,Item_Rate,ItemRate_Other,SapNo from Trans_Escrow_PurRqu_Sub ";
                    SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                    SSQL = SSQL + "  and  Lcode ='" + SessionLcode + "' and Pur_Request_No='" + Request_No + "' ";
                }
                else
                {
                    SSQL = "Select PRS.ReuiredQty,PRS.NoOfModel,PRS.SumRquQty,PRS.ReuiredDate,PRS.Item_Rate,PRS.ItemRate_Other ,PRS.SapNo ";
                    SSQL = SSQL + " from Trans_Escrow_PurRqu_sub PRS ";
                    SSQL = SSQL + " Inner join Acc_Mst_Ledger_BOMList BOM on BOM.ItemName=PRS.ItemName and BOM.Ccode=PRS.Ccode ";
                    SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " PRS.FinYearCode ='" + SessionFinYearCode + "' and BOM.LedgerName='" + txtSupplierName_Select.SelectedItem.Text + "' ";
                    SSQL = SSQL + " And PRS.Pur_Request_No='" + Request_No + "' And PRS.ItemCode='" + txtItemCodeHide.Value + "'";
                }
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count != 0)
                {
                    SAPNo = "";
                    SAPNo = DT.Rows[0]["SapNo"].ToString();
                    txtRequiredQty.Text = DT.Rows[0]["ReuiredQty"].ToString();
                    txtRequiredDate.Text = DT.Rows[0]["ReuiredDate"].ToString();
                    txtModelQty.Text = DT.Rows[0]["NoOfModel"].ToString();
                    txtOrderQty.Text = DT.Rows[0]["SumRquQty"].ToString();
                    txtRateEUR.Text = DT.Rows[0]["ItemRate_Other"].ToString();
                    txtRateINR.Text = DT.Rows[0]["Item_Rate"].ToString();
                }
            }
            else
            {
                SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status='Add' ";
                SSQL = SSQL + " And Raw_Mat_Name='" + txtItemNameSelect.SelectedItem.Text + "' ";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count != 0)
                {
                    txtRequiredQty.Text = DT.Rows[0]["RequiredQty"].ToString();
                    txtRateINR.Text = DT.Rows[0]["Amount"].ToString();
                    txtRateEUR.Text = DT.Rows[0]["AmtOther"].ToString();
                }
            }

            //Get TAX Percentage
            string CGST_Per = "0";
            string SGST_Per = "0";
            string IGST_Per = "0";
            string VAT_Per = "0";
            string UOMName = "";

            string ItemRate_INR = "0.00";
            string ItemRate_EUR = "0.00";

            //SSQL = "Select * from BOMMaster Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            //SSQL = SSQL + " Raw_Mat_Name ='" + txtItemNameSelect.SelectedItem.Text + "'";

            SSQL = "Select * from Trans_Escrow_PurRqu_sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And Pur_Request_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                CGST_Per = DT.Rows[0]["CGSTP"].ToString();
                SGST_Per = DT.Rows[0]["SGSTP"].ToString();
                IGST_Per = DT.Rows[0]["IGSTP"].ToString();
                VAT_Per = DT.Rows[0]["VATP"].ToString();
                UOMName = DT.Rows[0]["UOMCode"].ToString();

                ItemRate_INR = DT.Rows[0]["Item_Rate"].ToString();
                ItemRate_EUR = DT.Rows[0]["ItemRate_Other"].ToString();
            }

            //if (RdpTaxType.SelectedValue == "1") { IGST_Per = "0";VAT_Per = "0"; }
            //if (RdpTaxType.SelectedValue == "2") { CGST_Per = "0"; SGST_Per = "0"; VAT_Per = "0"; }
            //if (RdpTaxType.SelectedValue == "3") { CGST_Per = "0"; SGST_Per = "0"; IGST_Per = "0"; }
            //if (RdpTaxType.SelectedValue == "4") { CGST_Per = "0"; SGST_Per = "0"; IGST_Per = "0"; VAT_Per = "0"; }

            //if (rdpCurrencyType.SelectedValue == "1") { ItemRate_EUR = "0"; }
            //if (rdpCurrencyType.SelectedValue == "2") { ItemRate_INR = "0"; }

            txtCGSTPer.Text = CGST_Per;
            txtSGSTPer.Text = SGST_Per;
            txtIGSTPer.Text = IGST_Per;
            txtVAT_Per.Text = VAT_Per;
            hfUOM.Value = UOMName;

            txtRateINR.Text = ItemRate_INR;
            txtRateEUR.Text = ItemRate_EUR;

            TotalGstPer_Hidden.Value = (Convert.ToDecimal(txtCGSTPer.Text) + Convert.ToDecimal(txtSGSTPer.Text) + Convert.ToDecimal(txtIGSTPer.Text)).ToString();
            TotalGstPer_Hidden.Value = (Convert.ToDecimal(TotalGstPer_Hidden.Value) + Convert.ToDecimal(txtVAT_Per.Text)).ToString();
            Total_Calculate();
        }
    }
    protected void GridViewClick_ItemCode_PR(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        string ss = "";
        string PO_Type = "";
        string Request_No = "";
        DataTable DT = new DataTable();


        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);


        PO_Type = RdpPOType.SelectedValue;
        Request_No = txtRequestNo.Text;

        if (PO_Type == "2")
        {
            ss = Request_No.Substring(0, 3);
        }

        if (PO_Type == "2")
        {
            if (ss != "PRA")
            {
                SSQL = "Select ReuiredQty,ReuiredDate from Trans_Escrow_PurRqu_sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And Pur_Request_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                SSQL = "Select ReuiredQty,ReuiredDate from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And Amend_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

        }

        txtRequiredQty.Text = DT.Rows[0]["ReuiredQty"].ToString();
        txtRequiredDate.Text = DT.Rows[0]["ReuiredDate"].ToString();
        txtOrderQty.Text = DT.Rows[0]["ReuiredQty"].ToString();

    }
    protected void RdpPOType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdpPOType.SelectedValue == "2") { txtRequestNo.Enabled = true; }
        Load_Data_Empty_ItemCode();
    }

    protected void txtSuppRefDocNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        //txtDeptName.Items.Clear();
        if (txtSuppRefDocNo.Text != "" && txtSuppRefDocNo.Text != "-Select-")
        {
            SSQL = "Select QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And QuotNo='" + txtSuppRefDocNo.Text + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < Main_DT.Rows.Count; i++)
            {
                txtDocDate.Text = Main_DT.Rows[i]["QuotDate"].ToString();
                //txtDeptName.Text = Main_DT.Rows[i]["DeptName"].ToString();
            }
        }
    }
    protected void txtDiscount_Per_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }
    protected void txtGST_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TaxData();
    }

    private void Load_TaxData()
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();
        //SSQL = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + txtGST_Type.SelectedValue + "'";
        //DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (DT.Rows.Count != 0)
        //{
        //    txtCGSTPer.Text = DT.Rows[0]["CGST_Percent"].ToString();
        //    txtSGSTPer.Text = DT.Rows[0]["SGST_Percent"].ToString();
        //    txtIGSTPer.Text = DT.Rows[0]["IGST_Percent"].ToString();
        //}
        //else
        //{
        //    txtCGSTPer.Text = "0";
        //    txtSGSTPer.Text = "0";
        //    txtIGSTPer.Text = "0";
        //}
        //TotalGstPer_Hidden.Value = (Convert.ToDecimal(txtCGSTPer.Text) + Convert.ToDecimal(txtSGSTPer.Text) + Convert.ToDecimal(txtIGSTPer.Text)).ToString();

        //Total_Calculate();
    }
    protected void txtOtherCharge_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    private void Total_Calculate()
    {
        string Qty_Val = "0";
        string ItemRate_INR = "0";
        string ItemRate_EUR = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string VAT_Per = "0";
        string VAT_Amt = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";

        if (txtOrderQty.Text != "") { Qty_Val = txtOrderQty.Text.ToString(); }
        if (txtRateINR.Text != "0") { ItemRate_INR = txtRateINR.Text.ToString(); }
        if (txtRateEUR.Text != "0") { ItemRate_EUR = txtRateEUR.Text.ToString(); }

        if (ItemRate_INR != "0")
        {
            Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(ItemRate_INR)).ToString();
            Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();
        }

        if (ItemRate_EUR != "0")
        {
            Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(ItemRate_EUR)).ToString();
            Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();
        }

        if (Convert.ToDecimal(Item_Total) != 0)
        {
            if (Convert.ToDecimal(txtDiscount_Per.Text.ToString()) != 0) { Discount_Percent = txtDiscount_Per.Text.ToString(); }
            if (Convert.ToDecimal(txtOtherCharge.Text.ToString()) != 0) { Other_Charges = txtOtherCharge.Text.ToString(); }
            if (Convert.ToDecimal(txtCGSTPer.Text.ToString()) != 0) { CGST_Per = txtCGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtSGSTPer.Text.ToString()) != 0) { SGST_Per = txtSGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtIGSTPer.Text.ToString()) != 0) { IGST_Per = txtIGSTPer.Text.ToString(); }

            if (Convert.ToDecimal(txtVAT_Per.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

            //Discount Amt Calculate
            if (Convert.ToDecimal(Discount_Percent) != 0)
            {
                Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
            }
            else
            {
                Discount_Amt = "0.00";
            }

            if (Convert.ToDecimal(CGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                CGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(VAT_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
            }
            else
            {
                VAT_Amt = "0.00";
            }

            if (Convert.ToDecimal(SGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
            }
            else
            {
                SGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(IGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                IGST_Amt = "0.00";
            }

            //Other Charges
            if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

            //Final Amt
            Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
            //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

            txtItemTotal.Text = Item_Total;
            txtDiscount_Amount.Text = Discount_Amt;
            txtTaxAmt.Text = Tax_Amt;
            txtCGSTAmt.Text = CGST_Amt;
            txtSGSTAmt.Text = SGST_Amt;
            txtIGSTAmt.Text = IGST_Amt;
            txtVAT_AMT.Text = VAT_Amt;
            txtFinal_Total_Amt.Text = Final_Amount;
            txtNetAmt.Text = Final_Amount;
        }
    }
    private void Final_Total_Calculate()
    {

        string Final_NetAmt = "0";
        string AddorLess = "0";
        string Item_Total_Amt = txtTotAmt.Text;

        string[] ROff = Item_Total_Amt.Split('.');


        if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0)
        {
            AddorLess = txtAddOrLess.Text.ToString();
        }

        if (Convert.ToDecimal(ROff[1].ToString()) < Convert.ToDecimal("49"))
        {
            Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) - Convert.ToDecimal(AddorLess)).ToString();
        }
        else if (Convert.ToDecimal(ROff[1].ToString()) > Convert.ToDecimal("49"))
        {
            Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) + Convert.ToDecimal(AddorLess)).ToString();
        }

        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetAmt.Text = Final_NetAmt;

    }

    protected void txtAddOrLess_TextChanged(object sender, EventArgs e)
    {
        if (txtAddOrLess.Text.ToString() != "") { Final_Total_Calculate(); }
    }

    protected void txtOrderQty_TextChanged(object sender, EventArgs e)
    {
        if (txtOrderQty.Text.ToString() != "") { Total_Calculate(); }
    }

    protected void txtRateINR_TextChanged(object sender, EventArgs e)
    {
        if (txtRateINR.Text.ToString() != "") { Total_Calculate(); }
    }

    protected void txtRateEUR_TextChanged(object sender, EventArgs e)
    {
        if (txtRateEUR.Text.ToString() != "") { Total_Calculate(); }
    }

    protected void txtRequestNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtSupplierName_Select.SelectedItem.Text != "-Select-")
        {
            Initial_Data_Referesh();

            if (txtRequestNo.SelectedValue != "-Select-")
            {
                string SSQL = "";
                DataTable DT_DA = new DataTable();
                SSQL = "Select * from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Pur_Request_No ='" + txtRequestNo.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
                DT_DA = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_DA.Rows.Count != 0)
                {
                    txtRequestDate.Text = DT_DA.Rows[0]["Pur_Request_Date"].ToString();
                    txtDepartmentName.SelectedItem.Text = DT_DA.Rows[0]["DeptName"].ToString();
                    //txtDeliveryDate.Text = DT_DA.Rows[0]["DeptName"].ToString();
                }
            }
            Load_Data_Empty_ItemCode();
            All_Item_Added_DataTable(sender, e);

            Grid_Total_Calculate();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Supplier Name..');", true);

            Load_Data_Purchase_Request_No();
        }
    }

    protected void btnPO_Preview_Click(object sender, EventArgs e)
    {
        string RptName = "";
        string StdPurOrdNo = "";
        string SupQtnNo = "";
        string DeptName = "";

        RptName = "Standard Purchase Order Details Invoice Format";
        StdPurOrdNo = txtStdOrderNo.Text.ToString();

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");

    }

    private void All_Item_Added_DataTable(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        if (ddlMatType.SelectedItem.Text == "Tools")
        {
            SSQL = "Select SAPNo,ItemCode,ItemName,NoOfModel,ReuiredQty,ReuiredDate from Trans_Escrow_PurRqu_Sub Where ";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And Pur_Request_No='" + txtRequestNo.SelectedValue + "'";
        }
        else
        {
            SSQL = "Select PRS.SAPNo,PRS.ItemCode,PRS.ItemName,PRS.NoOfModel,PRS.ReuiredQty,PRS.ReuiredDate from Trans_Escrow_PurRqu_sub PRS ";
            SSQL = SSQL + " Inner join Acc_Mst_Ledger_BOMList BOM on BOM.ItemName=PRS.ItemName and BOM.Ccode=PRS.Ccode ";
            SSQL = SSQL + " Where PRS.Ccode='" + SessionCcode + "' And ";
            SSQL = SSQL + " PRS.Lcode ='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And PRS.Pur_Request_No='" + txtRequestNo.SelectedValue + "' And ";
            SSQL = SSQL + " BOM.LedgerName='" + txtSupplierName_Select.SelectedItem.Text + "'";
        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i = 0; i < DT.Rows.Count; i++)
        {
            txtItemNameSelect.SelectedValue = DT.Rows[i]["ItemCode"].ToString();
            txtItemNameSelect_SelectedIndexChanged(sender, e);
            btnAddItem_Click(sender, e);
        }
        txtItemNameSelect.SelectedIndex = 0;
    }

    protected void ddlPurMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPurMode.SelectedItem.Text == "Purchase Request")
        {
            pnlPurRqu.Visible = true;
            pnlDirectOrd.Visible = false;
        }
        else
        {
            pnlPurRqu.Visible = false;
            pnlDirectOrd.Visible = true;
            Load_Data_ItemMas();
        }
    }

    private void Load_Data_ItemMas()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "select Mat_No,Raw_Mat_Name from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtItemNameSelect.DataSource = DT;

        DataRow DR = DT.NewRow();

        DR["Mat_No"] = "-Select-";
        DR["Raw_Mat_Name"] = "-Select-";

        DT.Rows.InsertAt(DR, 0);

        txtItemNameSelect.DataTextField = "Raw_Mat_Name";
        txtItemNameSelect.DataValueField = "Mat_No";
        txtItemNameSelect.DataBind();
    }

    protected void txtGrdRateEUR_TextChanged(object sender, EventArgs e)
    {
        string Qty_Val = "0";
        string Item_Rate = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string VAT_Per = "0";
        string VAT_Amt = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";

        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            TextBox txtGrdOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;

            if (txtGrdOrdQty.Text.ToString() != "")
            {
                TextBox txtTest = ((TextBox)(sender));
                RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

                Label lblItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                Label lblItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                Label lblUOM = Repeater1.Items[i].FindControl("lblUOM") as Label;
                Label lblRQty = Repeater1.Items[i].FindControl("lblRQty") as Label;
                TextBox txtOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;
                TextBox txtRquDate = Repeater1.Items[i].FindControl("txtRDate") as TextBox;
                TextBox txtGrdRateINR = Repeater1.Items[i].FindControl("txtGrdRateINR") as TextBox;
                TextBox txtGrdRateEUR = Repeater1.Items[i].FindControl("txtGrdRateEUR") as TextBox;
                Label lblRemarks = Repeater1.Items[i].FindControl("lblRemarks") as Label;
                Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                Label lblDiscPer = Repeater1.Items[i].FindControl("lblDiscPer") as Label;
                Label lblDisc = Repeater1.Items[i].FindControl("lblDisc") as Label;
                Label lblTaxPer = Repeater1.Items[i].FindControl("lblTaxPer") as Label;
                Label lblTaxAmt = Repeater1.Items[i].FindControl("lblTaxAmt") as Label;
                Label lblCGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                Label lblCGSTAmt = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                Label lblSGSTAmt = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                Label lblIGSTAmt = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblGSTPer = Repeater1.Items[i].FindControl("lblGSTPer") as Label;
                Label lblOtherAmt = Repeater1.Items[i].FindControl("lblOtherAmt") as Label;
                Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                //txtGrdRateINR.ReadOnly = true;

                if (txtOrdQty.Text != "") { Qty_Val = txtOrdQty.Text.ToString(); }
                if (txtGrdRateEUR.Text != "") { Item_Rate = txtGrdRateEUR.Text.ToString(); }


                Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();

                if (Convert.ToDecimal(Item_Total) != 0)
                {
                    if (Convert.ToDecimal(lblDiscPer.Text.ToString()) != 0) { Discount_Percent = lblDiscPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                    if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                    //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                    //Discount Amt Calculate
                    if (Convert.ToDecimal(Discount_Percent) != 0)
                    {
                        Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                        Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                        Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        Discount_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(CGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                        CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                        CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        CGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(VAT_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                        VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                        VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        VAT_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(SGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                        SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                        SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        SGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(IGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                        IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                        IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        IGST_Amt = "0.00";
                    }

                    //Other Charges
                    if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                    //Final Amt
                    Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                    Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

                    lblItemTot.Text = Item_Total;
                    lblDisc.Text = Discount_Amt;
                    lblTaxAmt.Text = Tax_Amt;
                    lblCGSTAmt.Text = CGST_Amt;
                    lblSGSTAmt.Text = SGST_Amt;
                    lblIGSTAmt.Text = IGST_Amt;
                    //txtVAT_AMT.Text = VAT_Amt;
                    lblLineTot.Text = Final_Amount;
                    txtTotAmt.Text = Final_Amount;
                    txtNetAmt.Text = Final_Amount;
                }
                Grid_Total_Calculate();
                Final_Total_Calculate();
            }
        }
    }

    protected void txtGrdRateINR_TextChanged(object sender, EventArgs e)
    {
        string Qty_Val = "0";
        string Item_Rate = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string VAT_Per = "0";
        string VAT_Amt = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";

        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            TextBox txtGrdOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;

            if (txtGrdOrdQty.Text.ToString() != "")
            {
                TextBox txtTest = ((TextBox)(sender));
                RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

                Label lblItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                Label lblItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                Label lblUOM = Repeater1.Items[i].FindControl("lblUOM") as Label;
                Label lblRQty = Repeater1.Items[i].FindControl("lblRQty") as Label;
                TextBox txtOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;
                TextBox txtRquDate = Repeater1.Items[i].FindControl("txtRDate") as TextBox;
                TextBox txtGrdRateINR = Repeater1.Items[i].FindControl("txtGrdRateINR") as TextBox;
                TextBox txtGrdRateEUR = Repeater1.Items[i].FindControl("txtGrdRateEUR") as TextBox;
                Label lblRemarks = Repeater1.Items[i].FindControl("lblRemarks") as Label;
                Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                Label lblDiscPer = Repeater1.Items[i].FindControl("lblDiscPer") as Label;
                Label lblDisc = Repeater1.Items[i].FindControl("lblDisc") as Label;
                Label lblTaxPer = Repeater1.Items[i].FindControl("lblTaxPer") as Label;
                Label lblTaxAmt = Repeater1.Items[i].FindControl("lblTaxAmt") as Label;
                Label lblCGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                Label lblCGSTAmt = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                Label lblSGSTAmt = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                Label lblIGSTAmt = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblGSTPer = Repeater1.Items[i].FindControl("lblGSTPer") as Label;
                Label lblOtherAmt = Repeater1.Items[i].FindControl("lblOtherAmt") as Label;
                Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                //txtGrdRateEUR.ReadOnly = true;

                if (txtOrdQty.Text != "") { Qty_Val = txtOrdQty.Text.ToString(); }
                if (txtGrdRateINR.Text != "") { Item_Rate = txtGrdRateINR.Text.ToString(); }


                Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();

                if (Convert.ToDecimal(Item_Total) != 0)
                {
                    if (Convert.ToDecimal(lblDiscPer.Text.ToString()) != 0) { Discount_Percent = lblDiscPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                    if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                    //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                    //Discount Amt Calculate
                    if (Convert.ToDecimal(Discount_Percent) != 0)
                    {
                        Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                        Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                        Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        Discount_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(CGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                        CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                        CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        CGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(VAT_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                        VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                        VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        VAT_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(SGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                        SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                        SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        SGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(IGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                        IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                        IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        IGST_Amt = "0.00";
                    }

                    //Other Charges
                    if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                    //Final Amt
                    Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                    Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

                    lblItemTot.Text = Item_Total;
                    lblDisc.Text = Discount_Amt;
                    lblTaxAmt.Text = Tax_Amt;
                    lblCGSTAmt.Text = CGST_Amt;
                    lblSGSTAmt.Text = SGST_Amt;
                    lblIGSTAmt.Text = IGST_Amt;
                    //txtVAT_AMT.Text = VAT_Amt;
                    lblLineTot.Text = Final_Amount;
                    txtTotAmt.Text = Final_Amount;
                    txtNetAmt.Text = Final_Amount;
                }
                Grid_Total_Calculate();
                Final_Total_Calculate();
            }
        }
    }
    protected void txtOrdQty_TextChanged(object sender, EventArgs e)
    {
        string Qty_Val = "0";
        string Item_Rate = "0";
        string ItemRateEUR = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string VAT_Per = "0";
        string VAT_Amt = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";

        foreach (RepeaterItem item in Repeater1.Items)
        {
            TextBox txtGrdOrdQty = (TextBox)item.FindControl("txtOrdQty") as TextBox;

            if (txtGrdOrdQty.Text.ToString() != "")
            {
                TextBox txtTest = ((TextBox)(sender));
                RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

                Label lblItemCode = (Label)item.FindControl("lblItemCode") as Label;
                Label lblItemName = (Label)item.FindControl("lblItemName") as Label;
                Label lblUOM = (Label)item.FindControl("lblUOM") as Label;
                Label lblRQty = (Label)item.FindControl("lblRQty") as Label;
                TextBox txtOrdQty = (TextBox)item.FindControl("txtOrdQty") as TextBox;
                TextBox txtRquDate = (TextBox)item.FindControl("txtRDate") as TextBox;
                TextBox txtGrdRateINR = (TextBox)item.FindControl("txtGrdRateINR") as TextBox;
                TextBox txtGrdRateEUR = (TextBox)item.FindControl("txtGrdRateEUR") as TextBox;
                Label lblRemarks = (Label)item.FindControl("lblRemarks") as Label;
                Label lblItemTot = (Label)item.FindControl("lblItemTot") as Label;
                Label lblDiscPer = (Label)item.FindControl("lblDiscPer") as Label;
                Label lblDisc = (Label)item.FindControl("lblDisc") as Label;
                Label lblTaxPer = (Label)item.FindControl("lblTaxPer") as Label;
                Label lblTaxAmt = (Label)item.FindControl("lblTaxAmt") as Label;
                Label lblCGSTPer = (Label)item.FindControl("lblCGSTPer") as Label;
                Label lblCGSTAmt = (Label)item.FindControl("lblCGSTAmt") as Label;
                Label lblSGSTPer = (Label)item.FindControl("lblSGSTPer") as Label;
                Label lblSGSTAmt = (Label)item.FindControl("lblSGSTAmt") as Label;
                Label lblIGSTPer = (Label)item.FindControl("lblIGSTPer") as Label;
                Label lblIGSTAmt = (Label)item.FindControl("lblIGSTAmt") as Label;
                Label lblGSTPer = (Label)item.FindControl("lblGSTPer") as Label;
                Label lblOtherAmt = (Label)item.FindControl("lblOtherAmt") as Label;
                Label lblLineTot = (Label)item.FindControl("lblLineTot") as Label;


                if (txtOrdQty.Text != "") { Qty_Val = txtOrdQty.Text.ToString(); }


                if (txtGrdRateINR.Text != "") { Item_Rate = txtGrdRateINR.Text.ToString(); }
                if (txtGrdRateEUR.Text != "") { Item_Rate = txtGrdRateEUR.Text.ToString(); }


                Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();

                if (Convert.ToDecimal(Item_Total) != 0)
                {
                    if (Convert.ToDecimal(lblDiscPer.Text.ToString()) != 0) { Discount_Percent = lblDiscPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                    if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                    //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                    //Discount Amt Calculate
                    if (Convert.ToDecimal(Discount_Percent) != 0)
                    {
                        Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                        Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                        Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        Discount_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(CGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                        CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                        CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        CGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(VAT_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                        VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                        VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        VAT_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(SGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                        SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                        SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        SGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(IGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                        IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                        IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        IGST_Amt = "0.00";
                    }

                    //Other Charges
                    if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                    //Final Amt
                    Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                    Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

                    lblItemTot.Text = Item_Total;
                    lblDisc.Text = Discount_Amt;
                    lblTaxAmt.Text = Tax_Amt;
                    lblCGSTAmt.Text = CGST_Amt;
                    lblSGSTAmt.Text = SGST_Amt;
                    lblIGSTAmt.Text = IGST_Amt;
                    //txtVAT_AMT.Text = VAT_Amt;
                    lblLineTot.Text = Final_Amount;
                    txtTotAmt.Text = Final_Amount;
                    txtNetAmt.Text = Final_Amount;
                }
                Grid_Total_Calculate();
                Final_Total_Calculate();
            }
        }
    }

    private void Grid_Total_Calculate()
    {
        decimal SumLineTot = 0;
        decimal SumQtyTot = 0;

        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;
            TextBox txtOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;

            SumLineTot = SumLineTot + Convert.ToDecimal(LineTotal.Text);
            SumQtyTot = SumQtyTot + Convert.ToDecimal(txtOrdQty.Text);
        }

        txtTotAmt.Text = SumLineTot.ToString();
        txtTotQty.Text = SumQtyTot.ToString();
        txtNetAmt.Text = (SumLineTot + Convert.ToDecimal(txtAddOrLess.Text)).ToString();
    }
    private void Grid_Final_Total_Calculate()
    {

        string Final_NetAmt = "0";
        string AddorLess = "0";
        string Item_Total_Amt = txtTotAmt.Text;

        if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0) { AddorLess = txtAddOrLess.Text.ToString(); }
        Final_NetAmt = (Convert.ToDecimal(AddorLess) + Convert.ToDecimal(Item_Total_Amt)).ToString();
        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetAmt.Text = Final_NetAmt;

    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Purchase_Request_No();
    }

    protected void rdpCurrencyType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (rdpCurrencyType.SelectedItem.Text!="INR")
        //{
        //    RdpTaxType.SelectedIndex = 3;
        //}
        //else
        //{
        //    RdpTaxType.SelectedIndex = 0;
        //}
    }


    [WebMethod]
    public static List<LoadSupplier> GetSupplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select LedgerCode[SuppCode],LedgerName[SuppName] from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' ";
        SSQL = SSQL + " And Status='Add' And LedgerGrpName='Supplier' and LedgerName <> 'Enercon'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            try
            {
                List<LoadSupplier> GetSuppList = new List<LoadSupplier>();

                foreach (DataRow dr in DT.Rows)
                {
                    GetSuppList.Add(new LoadSupplier
                    {
                        SuppCode = dr["SuppCode"].ToString(),
                        SuppName = dr["SuppName"].ToString()
                    });
                }
                return GetSuppList.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            List<LoadSupplier> GetSuppList = new List<LoadSupplier>();
            return GetSuppList.ToList();
        }
    }

    [WebMethod]

    public static List<LoadDepartment> GetDepartment()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Main_DT.Rows.Count > 0)
        {
            try
            {
                List<LoadDepartment> GetDeptList = new List<LoadDepartment>();


                foreach (DataRow dr in Main_DT.Rows)
                {
                    GetDeptList.Add(new LoadDepartment
                    {
                        DeptCode = dr["DeptCode"].ToString(),
                        DeptName = dr["DeptName"].ToString()
                    });
                }

                return GetDeptList.ToList();
            }

            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            List<LoadDepartment> GetDeptList = new List<LoadDepartment>();
            return GetDeptList.ToList();
        }
    }

    [WebMethod]
    public static List<LoadDeliveryMode> GetDeliveryMode()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select DeliveryCode,DeliveryMode from MstDeliveryMode where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Main_DT.Rows.Count > 0)
        {
            try
            {
                List<LoadDeliveryMode> GetDelModeList = new List<LoadDeliveryMode>();

                foreach (DataRow dr in Main_DT.Rows)
                {
                    GetDelModeList.Add(new LoadDeliveryMode
                    {
                        DeliveryCode = dr["DeliveryCode"].ToString(),
                        DeliveryMode = dr["DeliveryMode"].ToString()
                    });
                }

                return GetDelModeList.ToList();
            }

            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            List<LoadDeliveryMode> GetDelModeList = new List<LoadDeliveryMode>();
            return GetDelModeList.ToList();
        }
    }

    [WebMethod]

    public static List<EditPurOrdSelRqu> GetEditPurOrd(string PurOrdNo)
    {

        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select * from Trans_Escrow_PurOrd_Main where  Std_PO_No='" + PurOrdNo + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And PO_Status!='Delete' ";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Main_DT.Rows.Count > 0)
        {
            try
            {
                List<EditPurOrdSelRqu> GetDelModeList = new List<EditPurOrdSelRqu>();

                foreach (DataRow dr in Main_DT.Rows)
                {
                    GetDelModeList.Add(new EditPurOrdSelRqu
                    {
                        PurRquNo = dr["Pur_Request_No"].ToString()
                    });
                }

                return GetDelModeList.ToList();
            }

            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            List<EditPurOrdSelRqu> GetDelModeList = new List<EditPurOrdSelRqu>();
            return GetDelModeList.ToList();
        }

    }

    [WebMethod]

    public static List<Load_PurRquNo> GetPurRquNos(string MatType)
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        DataTable da_Customer = new DataTable();
        DataTable da_Order = new DataTable();
        string Cus_Po = "";
        string Bag_PO;
        DataTable dt = new DataTable();
        dt.Columns.Add("Pur_Request_No", typeof(string));

        string MatTypeCode = "";

        //if (Session["Std_PO_No"] == null)
        //{

        if (MatType== "RawMaterial")
        {
            MatTypeCode = "1";
        }
        else
        {
            MatTypeCode = "2";
        }

        SSQL = "Select PRM.Pur_Request_No,PRS.ItemCode,PRS.ReuiredQty From Trans_Escrow_PurRqu_Sub PRS ";
        SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Ccode=PRS.Ccode And PRM.Lcode=PRS.Lcode And ";
        SSQL = SSQL + " PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No ";
        SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " PRS.FinYearCode='" + SessionFinYearCode + "' And PRM.Ccode='" + SessionCcode + "' And  ";
        SSQL = SSQL + " PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";

        SSQL = SSQL + " And PRM.MaterialType='" + MatTypeCode + "' and PRM.Pur_Req_Status!='Delete' Order by Pur_Request_No Asc";

        da_Customer = objdata.RptEmployeeMultipleDetails(SSQL);

        if (da_Customer.Rows.Count != 0)
        {
            for (int i = 0; i < da_Customer.Rows.Count; i++)
            {
                string Req_No = ""; string Req_Item = ""; string Req_Qty = "0";
                Req_No = da_Customer.Rows[i]["Pur_Request_No"].ToString();
                Req_Item = da_Customer.Rows[i]["ItemCode"].ToString();
                Req_Qty = da_Customer.Rows[i]["ReuiredQty"].ToString();

                SSQL = "Select isnull(Sum(OrderQty),0) as PO_Qty from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                SSQL = SSQL + " Pur_Request_No ='" + Req_No + "' And ItemCode='" + Req_Item + "'";

                da_Order = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_Order.Rows.Count != 0)
                {
                    Bag_PO = da_Order.Rows[0]["PO_Qty"].ToString();
                    if (Convert.ToDecimal(Bag_PO.ToString()) < Convert.ToDecimal(Req_Qty))
                    {
                        DataRow row = dt.NewRow();
                        row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                        dt.Rows.Add(row);
                    }
                }
                else
                {
                    DataRow row = dt.NewRow();
                    row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                    dt.Rows.Add(row);
                }
            }
        }
        //}
        //else
        //{
        //    SSQL = "Select PRM.Pur_Request_No,PRS.ItemCode,PRS.ReuiredQty From Trans_Escrow_PurRqu_Sub PRS ";
        //    SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Ccode=PRS.Ccode And PRM.Lcode=PRS.Lcode And ";
        //    SSQL = SSQL + " PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No ";
        //    SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
        //    SSQL = SSQL + " PRS.FinYearCode='" + SessionFinYearCode + "' And PRM.Ccode='" + SessionCcode + "' ";
        //    SSQL = SSQL + " And PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";
        //    SSQL = SSQL + " And PRM.MaterialType='" + ddlMatType.SelectedItem.Text + "' Order by Pur_Request_No Asc";

        //    da_Customer = objdata.RptEmployeeMultipleDetails(SSQL);
        //    if (da_Customer.Rows.Count != 0)
        //    {
        //        for (int i = 0; i < da_Customer.Rows.Count; i++)
        //        {
        //            string Req_No = ""; string Req_Item = ""; string Req_Qty = "0";
        //            Req_No = da_Customer.Rows[i]["Pur_Request_No"].ToString();
        //            Req_Item = da_Customer.Rows[i]["ItemCode"].ToString();
        //            Req_Qty = da_Customer.Rows[i]["ReuiredQty"].ToString();

        //            SSQL = "Select isnull(Sum(OrderQty),0) as PO_Qty From Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' ";
        //            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
        //            SSQL = SSQL + " Pur_Request_No ='" + Req_No + "'";
        //            SSQL = SSQL + " And ItemCode='" + Req_Item + "' And Std_PO_No <> '" + txtStdOrderNo.Text + "'";

        //            da_Order = objdata.RptEmployeeMultipleDetails(SSQL);
        //            if (da_Order.Rows.Count != 0)
        //            {
        //                Bag_PO = da_Order.Rows[0]["PO_Qty"].ToString();
        //                if (Convert.ToDecimal(Bag_PO.ToString()) < Convert.ToDecimal(Req_Qty))
        //                {
        //                    DataRow row = dt.NewRow();
        //                    row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
        //                    dt.Rows.Add(row);
        //                }
        //            }
        //            else
        //            {
        //                DataRow row = dt.NewRow();
        //                row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
        //                dt.Rows.Add(row);
        //            }
        //        }
        //    }
        //}

        DataTable dt2 = new DataTable();

        if (dt.Rows.Count != 0)
        {
            //Main_DT = RemoveDuplicatesRecords(dt);

            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            dt2 = UniqueRows.CopyToDataTable();

        }
        else
        {
            dt2 = dt;
        }


        if (dt2.Rows.Count > 0)
        {
            try
            {
                List<Load_PurRquNo> GetPurRquNo = new List<Load_PurRquNo>();

                foreach (DataRow dr in dt2.Rows)
                {
                    GetPurRquNo.Add(new Load_PurRquNo
                    {
                        PurRquNo = dr["Pur_Request_No"].ToString()
                    });
                }

                return GetPurRquNo.ToList();
            }

            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            List<Load_PurRquNo> GetPurRquNo = new List<Load_PurRquNo>();
            return GetPurRquNo.ToList();
        }

    }

    [WebMethod]
    public static List<Supplier> Suppliers(string SuppName)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        if (SuppName != "-Select-")
        {
            SSQL = "Select * from Acc_Mst_Ledger Where LedgerName='" + SuppName + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                try
                {
                    List<Supplier> GetSuppNme = new List<Supplier>();

                    foreach (DataRow dr in DT.Rows)
                    {
                        GetSuppNme.Add(new Supplier
                        {
                            SuppCode = dr["LedgerCode"].ToString(),
                            SuppName = dr["LedgerName"].ToString(),
                            TC = dr["TC"].ToString()
                        });
                    }

                    return GetSuppNme.ToList();
                }

                catch (Exception ex)
                {
                    return null;
                }
            }
            else
            {
                List<Supplier> GetSuppNme = new List<Supplier>();
                return GetSuppNme.ToList();
            }
        }
        else
        {
            List<Supplier> GetSuppNme = new List<Supplier>();
            return GetSuppNme.ToList();
        }
    }

    [WebMethod]

    public static string EditSelPurRquNo(string PurOrdNo, string PurRquNo)
    {
        string DelDate = "";


        if (PurRquNo != "-Select-")
        {
            string SSQL = "";

            DataTable DT_DA = new DataTable();

            SSQL = "Select * from Trans_Escrow_PurOrd_Main where  Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " Pur_Request_No ='" + PurRquNo + "' And FinYearCode='" + SessionFinYearCode + "'";

            DT_DA = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT_DA.Rows.Count != 0)
            {
                DelDate = DT_DA.Rows[0]["Pur_Request_Date"].ToString();
            }

            else
            {
                DelDate = "";
            }
        }
        else
        {
            //DelDate = "Select Purchase Request No";
        }

        return DelDate;
    }

    [WebMethod]

    public static string SelPurRquNo(string PurRquNo)
    {
        string DelDate = "";
        //string SuppName,

        //if (SuppName != "-Select-")
        //{   
        if (PurRquNo != "-Select-")
        {
            string SSQL = "";

            DataTable DT_DA = new DataTable();

            SSQL = "Select * from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " Pur_Request_No ='" + PurRquNo + "' And FinYearCode='" + SessionFinYearCode + "'";

            DT_DA = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT_DA.Rows.Count != 0)
            {
                DelDate = DT_DA.Rows[0]["Pur_Request_Date"].ToString();
            }

            else
            {
                DelDate = "";
            }
        }
        else
        {
            DelDate = "Select Purchase Request No";
        }
        //}
        //else
        //{
        //    DelDate = "Select Supplier Name";
        //}

        return DelDate;
    }


    [WebMethod]

    public static List<PurRquItemDet> getItemDetails(string MatType, string PurRquNo, string CurrencyTyp, string GstType)
    {

        //string SuppName,

        string SSQL;
        DataTable DT = new DataTable();

        if (MatType == "Tools")
        {
            SSQL = "Select SAPNo,ItemCode,ItemName,NoOfModel,ReuiredQty,SumRquQty[OrderQty],ReuiredDate,Item_Rate[Rate_INR],";
            SSQL = SSQL + " ItemRate_Other[Rate_EUR] from Trans_Escrow_PurRqu_Sub Where ";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And Pur_Request_No='" + PurRquNo + "' and  Pur_Req_Status!='Delete' and Status='1'";
        }
        else
        {
            //SSQL = "Select Distinct PRS.SAPNo,PRS.ItemCode,PRS.ItemName,PRS.NoOfModel,PRS.ReuiredQty,SumRquQty[OrderQty],PRS.ReuiredDate, ";
            //SSQL = SSQL + " Item_Rate[Rate_INR],ItemRate_Other[Rate_EUR],PRS.CGSTP,PRS.SGSTP,PRS.IGSTP From Trans_Escrow_PurRqu_sub PRS ";
            //SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Pur_Request_No=PRS.Pur_Request_No and PRM.Ccode=PRS.Ccode ";
            //SSQL = SSQL + " and PRM.Pur_Req_Status=PRS.Pur_Req_Status and PRM.Status='1' ";
            //SSQL = SSQL + " Inner join Acc_Mst_Ledger_BOMList BOM on BOM.ItemCode=PRS.ItemCode and BOM.Ccode=PRS.Ccode ";
            //SSQL = SSQL + " Where PRS.Ccode='" + SessionCcode + "' And PRM.Pur_Req_Status!='Delete' And ";
            //SSQL = SSQL + " PRS.Lcode ='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "' ";
            //SSQL = SSQL + " And PRS.Pur_Request_No='" + PurRquNo + "' And BOM.LedgerName='" + SuppName + "' ";

            SSQL = "Select Distinct PRS.SAPNo,PRS.ItemCode,PRS.ItemName,PRS.NoOfModel,PRS.ReuiredQty,SumRquQty[OrderQty],PRS.ReuiredDate, ";
            SSQL = SSQL + " Item_Rate[Rate_INR],ItemRate_Other[Rate_EUR],PRS.CGSTP,PRS.SGSTP,PRS.IGSTP,PRS.CalendarWeek [CalWeek] From Trans_Escrow_PurRqu_sub PRS   ";
            SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Pur_Request_No=PRS.Pur_Request_No and PRM.Ccode=PRS.Ccode  and ";
            SSQL = SSQL + " PRM.Pur_Req_Status=PRS.Pur_Req_Status and PRM.Status='1'   ";
            SSQL = SSQL + " Where PRS.Ccode='" + SessionCcode + "' And PRM.Pur_Req_Status!='Delete' And ";
            SSQL = SSQL + " PRS.Lcode ='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And PRS.Pur_Request_No='" + PurRquNo + "' ";

        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            try
            {
                List<PurRquItemDet> GetItemDet = new List<PurRquItemDet>();
                string RateINR = "0";
                string RateERU = "0";
                string ItamRate = "0";
                string CGSTP = "0";
                string SGSTP = "0";
                string IGSTP = "0";

                string CGSTAmount = "0";
                string SGSTAmount = "0";
                string IGSTAmount = "0";

                string PackingAmt = "0";
                string Discount_Amt = "0";

                foreach (DataRow dr in DT.Rows)
                {
                    if (CurrencyTyp == "INR")
                    {
                        RateINR = dr["Rate_INR"].ToString();
                        ItamRate = (Convert.ToDecimal(RateINR) * Convert.ToDecimal(dr["OrderQty"].ToString())).ToString();
                        ItamRate = (Math.Round(Convert.ToDecimal(ItamRate), 2, MidpointRounding.AwayFromZero)).ToString();

                        if (GstType == "GST")
                        {
                            CGSTP = dr["CGSTP"].ToString();
                            SGSTP = dr["SGSTP"].ToString();

                            if (Convert.ToDecimal(CGSTP) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(ItamRate) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                CGSTAmount = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGSTP)).ToString();
                                CGSTAmount = (Convert.ToDecimal(CGSTAmount) / Convert.ToDecimal(100)).ToString();
                                CGSTAmount = (Math.Round(Convert.ToDecimal(CGSTAmount), 2, MidpointRounding.AwayFromZero)).ToString();

                            }
                            else
                            {
                                CGSTAmount = "0.00";
                            }

                            if (Convert.ToDecimal(SGSTP) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(ItamRate) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                SGSTAmount = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGSTP)).ToString();
                                SGSTAmount = (Convert.ToDecimal(SGSTAmount) / Convert.ToDecimal(100)).ToString();
                                SGSTAmount = (Math.Round(Convert.ToDecimal(SGSTAmount), 2, MidpointRounding.AwayFromZero)).ToString();

                            }
                            else
                            {
                                SGSTAmount = "0.00";
                            }
                        }
                        else if (GstType == "IGST")
                        {
                            IGSTP = dr["IGSTP"].ToString();

                            if (Convert.ToDecimal(IGSTP) != 0)
                            {
                                string Item_Discount_Amt = "0.00";
                                Item_Discount_Amt = ((Convert.ToDecimal(ItamRate) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                IGSTAmount = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGSTP)).ToString();
                                IGSTAmount = (Convert.ToDecimal(IGSTAmount) / Convert.ToDecimal(100)).ToString();
                                IGSTAmount = (Math.Round(Convert.ToDecimal(IGSTAmount), 2, MidpointRounding.AwayFromZero)).ToString();

                            }
                            else
                            {
                                IGSTAmount = "0.00";
                            }
                        }
                    }
                    else
                    {
                        RateERU = dr["Rate_EUR"].ToString();
                        ItamRate = (Convert.ToDecimal(RateERU) * Convert.ToDecimal(dr["OrderQty"].ToString())).ToString();
                    }

                    GetItemDet.Add(new PurRquItemDet
                    {
                        SAPNo = dr["SAPNo"].ToString(),
                        ItemName = dr["ItemName"].ToString(),
                        ItemCode = dr["ItemCode"].ToString(),
                        ModelQty = dr["NoOfModel"].ToString(),
                        RequiredQty = dr["ReuiredQty"].ToString(),
                        OrderQty = dr["OrderQty"].ToString(),
                        ReuiredDate = dr["ReuiredDate"].ToString(),
                        Rate_INR = RateINR,
                        Rate_Other = RateERU,
                        ItemAmt = ItamRate,
                        CGSTP = dr["CGSTP"].ToString(),
                        SGSTP = dr["SGSTP"].ToString(),
                        IGSTP = dr["IGSTP"].ToString(),
                        CGSTAmt = CGSTAmount,
                        SGSTAmt = SGSTAmount,
                        IGSTAmt = IGSTAmount,
                        IncTaxAmt = (Math.Round(Convert.ToDecimal(ItamRate) + Convert.ToDecimal(CGSTAmount) + Convert.ToDecimal(SGSTAmount) + Convert.ToDecimal(IGSTAmount), 2, MidpointRounding.AwayFromZero)).ToString(),
                        CalWeek = dr["CalWeek"].ToString()
                    });
                }

                return GetItemDet.ToList();
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        else
        {
            List<PurRquItemDet> GetItemDet = new List<PurRquItemDet>();
            return GetItemDet.ToList();
        }
    }

    [WebMethod]

    public static List<AmtCalc> GSTCalc(string OrdQty, string ItemRate, string RquNo, string Data, string GST, string CURType)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataTable DtItemDet = new DataTable();

        string CGSTP = "0";
        string SGSTP = "0";
        string IGSTP = "0";
        string CGSTAmt = "0";
        string SGSTAmt = "0";
        string IGSTAmt = "0";

        string RateINR = "0";
        string ItemWOGST = "0";
        string PackingAmt = "0";
        string Discount_Amt = "0";

        string ItemName = "";

        DtItemDet = JsonConvert.DeserializeObject<DataTable>(Data);

        for (int i = 0; DtItemDet.Rows.Count > 0; i++)
        {
            SSQL = "Select * from Trans_Escrow_PurRqu_Sub where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
            SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' and  Pur_Request_No='" + RquNo + "' and SAPNo='" + SAPNo + "' ";
            SSQL = SSQL + " Pur_Req_Status!='Delete' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                try
                {
                    List<AmtCalc> GSTCalc = new List<AmtCalc>();
                    foreach (DataRow dr in DT.Rows)
                    {
                        if (CURType == "INR")
                        {
                            RateINR = ItemRate;
                            ItemWOGST = (Convert.ToDecimal(RateINR) * Convert.ToDecimal(OrdQty)).ToString();
                            ItemWOGST = (Math.Round(Convert.ToDecimal(ItemWOGST), 2, MidpointRounding.AwayFromZero)).ToString();

                            if (GST == "GST")
                            {
                                CGSTP = DT.Rows[0]["CGSTP"].ToString();
                                SGSTP = DT.Rows[0]["SGSTP"].ToString();

                                if (Convert.ToDecimal(CGSTP) != 0)
                                {
                                    string Item_Discount_Amt = "0.00";
                                    Item_Discount_Amt = ((Convert.ToDecimal(ItemWOGST) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                    CGSTAmt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGSTP)).ToString();
                                    CGSTAmt = (Convert.ToDecimal(CGSTAmt) / Convert.ToDecimal(100)).ToString();
                                    CGSTAmt = (Math.Round(Convert.ToDecimal(CGSTAmt), 2, MidpointRounding.AwayFromZero)).ToString();

                                }
                                else
                                {
                                    CGSTAmt = "0.00";
                                }

                                if (Convert.ToDecimal(SGSTP) != 0)
                                {
                                    string Item_Discount_Amt = "0.00";
                                    Item_Discount_Amt = ((Convert.ToDecimal(ItemWOGST) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                    SGSTAmt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGSTP)).ToString();
                                    SGSTAmt = (Convert.ToDecimal(SGSTAmt) / Convert.ToDecimal(100)).ToString();
                                    SGSTAmt = (Math.Round(Convert.ToDecimal(SGSTAmt), 2, MidpointRounding.AwayFromZero)).ToString();

                                }
                                else
                                {
                                    SGSTAmt = "0.00";
                                }
                            }
                            else if (GST == "IGST")
                            {
                                IGSTP = DT.Rows[0]["IGSTP"].ToString();

                                if (Convert.ToDecimal(SGSTP) != 0)
                                {
                                    string Item_Discount_Amt = "0.00";
                                    Item_Discount_Amt = ((Convert.ToDecimal(ItemWOGST) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                                    IGSTAmt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGSTP)).ToString();
                                    IGSTAmt = (Convert.ToDecimal(IGSTAmt) / Convert.ToDecimal(100)).ToString();
                                    IGSTAmt = (Math.Round(Convert.ToDecimal(IGSTAmt), 2, MidpointRounding.AwayFromZero)).ToString();

                                }
                                else
                                {
                                    IGSTAmt = "0.00";
                                }
                            }
                        }

                        GSTCalc.Add(new AmtCalc
                        {
                            SAPNo = SAPNo,
                            ItemName = dr["ItemName"].ToString(),
                            //ItemCode = dr["ItemCode"].ToString(),
                            ModelQty = dr["NoOfModel"].ToString(),
                            RequiredQty = dr["ReuiredQty"].ToString(),
                            OrdQty = OrdQty,
                            RequireDate = dr["ReuiredDate"].ToString(),
                            RateINR = ItemRate,
                            RateERU = dr["ItemRate_Other"].ToString(),
                            ItemAmt = ItemWOGST,
                            CGST = CGSTAmt,
                            SGST = SGSTAmt,
                            IGST = IGSTAmt,
                            AmtWithGST = (Math.Round(Convert.ToDecimal((Convert.ToDecimal(CGSTAmt) + Convert.ToDecimal(SGSTAmt) + Convert.ToDecimal(IGSTAmt)).ToString()), 2, MidpointRounding.AwayFromZero)).ToString()
                        });
                    }
                    return GSTCalc.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            else
            {
                List<AmtCalc> GSTCalc = new List<AmtCalc>();
                return GSTCalc.ToList();
            }
        }
        return null;
    }

    [WebMethod]
    public static string GetLastNo(string MatType)
    {
        string msg = "";
        string Auto_Transaction_No = "";
        TransactionNoGenerate TransNO = new TransactionNoGenerate();

        if (MatType == "2")
        {
            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Asset Purchase Order", SessionFinYearVal, "4");
        }
        else if (MatType == "1")
        {
            Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");
        }

        if (Auto_Transaction_No == "")
        {
            msg = "false";
        }
        else
        {
            msg = Auto_Transaction_No.ToString();
        }
        return msg;
    }


    [WebMethod]
    public static List<LoadPurOrdEdit> Search(string PurOrdNo)
    {
        string SSQL = "";
        bool ErrFlag = false;

        SSQL = "";
        SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " and Std_PO_No='" + PurOrdNo + "' and Active!='Delete'  ";

        //SSQL = SSQL + " and Pur_Req_Status!='Delete'";

        DataTable dt_edit = new DataTable();
        dt_edit = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_edit.Rows.Count > 0)
        {
            try
            {
                List<LoadPurOrdEdit> LoadPurOrdEditList = new List<LoadPurOrdEdit>();
                foreach (DataRow dr in dt_edit.Rows)
                {
                    LoadPurOrdEditList.Add(new LoadPurOrdEdit
                    {
                        MatType = dr["MatType"].ToString(),
                        CoralPurOrdNo = dr["RefNo"].ToString(),
                        PurDate = dr["Std_PO_Date"].ToString(),
                        PurRquNo = dr["Pur_Request_No"].ToString(),
                        PurRquDt = dr["Pur_Request_Date"].ToString(),
                        QuatNo = dr["Supp_Qtn_No"].ToString(),
                        QuatDate = dr["Supp_Qtn_Date"].ToString(),
                        SuppName = dr["Supp_Name"].ToString(),
                        SuppCode = dr["Supp_Code"].ToString(),
                        DeptName = dr["DeptName"].ToString(),
                        DeptCode = dr["DeptCode"].ToString(),
                        DeliModeCode = dr["DeliveryModeCode"].ToString(),
                        DeliMode = dr["DeliveryMode"].ToString(),
                        PayMode = dr["PaymentMode"].ToString(),
                        PurRquDate = dr["DeliveryDate"].ToString(),
                        DeliAt = dr["DeliveryAt"].ToString(),
                        Payterms = dr["PaymentTerms"].ToString(),
                        SubIns = dr["Description"].ToString(),
                        SplNotes = dr["SplNote"].ToString(),
                        FreightChr = dr["Note"].ToString(),
                        ETA = dr["Others"].ToString(),
                        CurType = dr["CurrencyType"].ToString(),
                        TaxType = dr["TaxType"].ToString(),
                        PurType = dr["Po_Type"].ToString(),
                        TotQty = dr["TotalQuantity"].ToString(),
                        TotItmAmt = dr["TotalAmt"].ToString(),
                        TotCGSTAmt = dr["TotCGST"].ToString(),
                        TotSGSTAmt = dr["TotSGST"].ToString(),
                        TotIGSTAmt = dr["TotIGST"].ToString(),
                        TotNetAmt = "0",
                        AOL = dr["AddOrLess"].ToString(),
                        FinalAmt = dr["NetAmount"].ToString()
                    });
                }
                return LoadPurOrdEditList.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            List<LoadPurOrdEdit> LoadPurOrdEditList = new List<LoadPurOrdEdit>();
            return LoadPurOrdEditList.ToList();
        }
    }

    [WebMethod]
    public static List<LoadPurOrdEditItemDet> SearchPurOrdSubItem(string PurOrdNo)
    {
        string SSQL = "";
        bool ErrFlag = false;
        DataTable dt_edit = new DataTable();
        DataTable DTMain = new DataTable();


        SSQL = "Select * from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Std_PO_No='" + PurOrdNo + "' and Active!='Delete' ";
        
        //and Pur_Req_Status!='Delete'";

        dt_edit = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL= "Select * from  Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Std_PO_No='" + PurOrdNo + "' and Active!='Delete'  ";

        DTMain = objdata.RptEmployeeMultipleDetails(SSQL);

        string RateEUR = "0.00";
        string RateINR = "0.00";

        if (dt_edit.Rows.Count > 0)
        {
            try
            {
                List<LoadPurOrdEditItemDet> LoadPurOrdEditSubList = new List<LoadPurOrdEditItemDet>();
                foreach (DataRow dr in dt_edit.Rows)
                {

                    if (DTMain.Rows[0]["CurrencyType"].ToString() == "INR")
                    {
                        RateINR = dr["Rate"].ToString();
                    }
                    else
                    {
                        RateEUR = dr["Rate"].ToString();
                    }

                    LoadPurOrdEditSubList.Add(new LoadPurOrdEditItemDet
                    {
                        SAPNo = dr["SAPNo"].ToString(),
                        ItemCode = dr["ItemCode"].ToString(),
                        ItemName = dr["ItemName"].ToString(),
                        UOM = dr["UOMCode"].ToString(),
                        RequiredQty = dr["ReuiredQty"].ToString(),
                        ModelQty = dr["ModelQty"].ToString(),
                        OrderQty = dr["OrderQty"].ToString(),
                        ReuiredDate = dr["ReuiredDate"].ToString(),
                        Rate_INR = RateINR,
                        Rate_Other = RateEUR,
                        ItemAmt = dr["ItemTotal"].ToString(),
                        CGSTP = dr["CGSTPer"].ToString(),
                        SGSTP = dr["SGSTPer"].ToString(),
                        IGSTP = dr["IGSTPer"].ToString(),
                        CGSTAmt = dr["CGSTAmount"].ToString(),
                        SGSTAmt = dr["SGSTAmount"].ToString(),
                        IGSTAmt = dr["IGSTAmount"].ToString(),
                        IncTaxAmt = dr["LineTotal"].ToString(),
                        CalWeek = dr["CalWeek"].ToString()
                    });
                }
                return LoadPurOrdEditSubList.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            List<LoadPurOrdEditItemDet> LoadPurOrdEditSubList = new List<LoadPurOrdEditItemDet>();
            return LoadPurOrdEditSubList.ToList();
        }
    }

    [WebMethod]
    public static string SaveItems(string SaveType,string MatType,string PurOrdDate, string PurOrdNo, string SuppName, string SuppCode, string PurRquDate, string PurRquNo,string DeliModeCode, string DeliMode, string DeliDate, string DeliAt, string PayMode, string DeptName, string DeptCode, string Payterms, string Desc,string SplNotes, string Note, string Others, string TotQty, string TotItemAmt,string TotCGST, string TotSGST, string TotIGST, string TotDisc, string TaxP, string TaxAmt, string OtherAmt, string NetAmt, string AOL, string CoralPurOrdNo, string GSTP, string GSTName, string CurType,string QuatNo,string QuatDate, string data)
    {
        DataTable dt = new DataTable();
        string result = "true";
        string SSQL = "";
        bool ErrFlag = false;
        string purordno = "";

        dt = JsonConvert.DeserializeObject<DataTable>(data);
        if (!ErrFlag)
        {
            if (SaveType == "1")
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = "";
                if (MatType == "2")
                {
                    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Asset Purchase Order", SessionFinYearVal, "4");
                }
                else
                {
                    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");
                }

                purordno = Auto_Transaction_No;
            }
            else
            {
                purordno = PurOrdNo;
            }


            if (dt.Rows.Count <= 0)
            {
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //getDeptDetails(out deptCode, out DeptName, ref deptVal);

                //SSQL = "";
                //SSQL = "Update Trans_Escrow_PurOrd_Main set Active='Delete' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                //SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' and Std_PO_No = '" + purordno + "'";
                //objdata.RptEmployeeMultipleDetails(SSQL);

                //SSQL = "";
                //SSQL = "Update Trans_Escrow_PurOrd_Sub set Active='Delete'  where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                //SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' and Std_PO_No = '" + purordno + "'";

                //objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "";
                SSQL = "Delete from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' and Std_PO_No = '" + purordno + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Delete from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' and Std_PO_No = '" + purordno + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "Insert Into Trans_Escrow_PurOrd_Main(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,";
                SSQL = SSQL + " Supp_Qtn_Date,Pur_Request_No,Pur_Request_Date,DeliveryModeCode,DeliveryMode,DeliveryDate,DeliveryAt,PaymentMode,DeptCode,";
                SSQL = SSQL + " DeptName,PaymentTerms,Description,Note,Others,TotalQuantity,TotalAmt,Discount,TaxPer,TaxAmount,OtherCharge,";
                SSQL = SSQL + " NetAmount,Approvedby,UserID,UserName,PO_Status,PO_Type,AddOrLess,RefNo,GSTPer,TaxType,Active,CurrencyType,";
                SSQL = SSQL + " TotCGST,TotSGST,TotIGST,MatType,SplNote,ReceiveStatus,AmendStatus)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + purordno + "','" + PurOrdDate + "','" + SuppCode + "','" + SuppName + "','" + QuatNo + "','" + QuatDate + "',";
                SSQL = SSQL + " '" + PurRquNo + "','" + PurRquDate + "',";
                SSQL = SSQL + " '" + DeliModeCode + "','" + DeliMode + "','" + DeliDate + "','" + DeliAt.Replace("'", "\"") + "',";
                SSQL = SSQL + " '" + PayMode + "','" + DeptCode + "','" + DeptName + "','" + Payterms.Replace("'", "\"") + "',";
                SSQL = SSQL + " '" + Desc.Replace("'", "\"") + "','" + Note.Replace("'", "\"") + "','" + Others.Replace("'", "\"") + "',";
                SSQL = SSQL + " '" + TotQty + "','" + TotItemAmt + "','" + TotDisc + "','" + TaxP + "',";
                SSQL = SSQL + " '" + TaxAmt + "','" + OtherAmt + "','" + NetAmt + "','','" + SessionUserID + "','" + SessionUserName + "','0',";
                SSQL = SSQL + " 'Purchase Request','"+ AOL +"','"+ CoralPurOrdNo + "','"+ GSTP + "','"+ GSTName + "','Add','"+ CurType + "',";
                SSQL = SSQL + " '" + TotCGST + "','" + TotSGST + "','" + TotIGST + "','" + MatType + "','" + SplNotes.Replace("'", "\"") + "','0','0')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string UOM;
                        string ItemCode;
                        string NoOfModel;
                        string ReuiredQty;
                        string SumRquQty;
                        string ReuiredDate;
                        string ItemRemarks;
                        string CalendarWeek;
                        string CGSTP;
                        string SGSTP;
                        string IGSTP;
                        string FSapNo = dt.Rows[i]["SAPNo"].ToString();
                        string FPurRquNo = PurRquNo;
                        string FGSTP;


                        GetItemDet(ref FPurRquNo, ref FSapNo, out UOM, out ItemCode,  out SumRquQty, out ReuiredDate, out ItemRemarks, out CalendarWeek, out CGSTP, out SGSTP, out IGSTP);
                      
                        SSQL = "Insert Into Trans_Escrow_PurOrd_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Pur_Request_No,";
                        SSQL = SSQL + " Pur_Request_Date,ItemCode,ItemName,SAPNo,UOMCode,ModelQty,RquQty,ReuiredQty,ReuiredDate,OrderQty,Rate,ItemTotal,";
                        SSQL = SSQL + " Discount_Per,Discount,GSTPer,GSTName,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount,LineTotal,TaxPer,";
                        SSQL = SSQL + " TaxAmount,BDUTaxPer,BDUTaxAmount,OtherCharge,RefNo,Remarks,Active,UserID,UserName,CalWeek) values (";
                        SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                        SSQL = SSQL + " '" + purordno + "','" + PurOrdDate + "','" + PurRquNo + "','" + PurRquDate + "','" + ItemCode + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["ItemName"].ToString().Replace("'", "\"") + "','" + FSapNo + "','" + UOM + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["ModelQty"].ToString() + "','" + dt.Rows[i]["OrderQty"].ToString() + "',";
                        SSQL = SSQL + " '" + dt.Rows[i]["RequiredQty"].ToString() + "','" + ReuiredDate + "','" + dt.Rows[i]["OrderQty"].ToString() + "',";

                        if(CurType=="INR")
                        {
                            SSQL = SSQL + "'" + dt.Rows[i]["Rate_INR"].ToString() + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'" + dt.Rows[i]["Rate_Other"].ToString() + "',";
                        }

                        if (GSTName == "GST")
                        {
                            FGSTP = (Convert.ToDecimal(CGSTP) + Convert.ToDecimal(SGSTP)).ToString();
                        }
                        else if (GSTName == "IGST")
                        {
                            FGSTP = IGSTP;
                        }
                        else
                        {
                            FGSTP = "0.00";
                        }

                        SSQL = SSQL + " '" + dt.Rows[i]["ItemAmt"].ToString() + "','0','0.00','" + FGSTP + "','" + GSTName + "',";

                        if (GSTName == "GST")
                        {
                            SSQL = SSQL + " '" + CGSTP + "',";
                            SSQL = SSQL + " '" + dt.Rows[i]["CGSTAmt"].ToString() + "',";
                            SSQL = SSQL + " '" + SGSTP + "',";
                            SSQL = SSQL + " '" + dt.Rows[i]["SGSTAmt"].ToString() + "',";
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                        }
                        else if(GSTName=="IGST")
                        {
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '" + IGSTP + "',";
                            SSQL = SSQL + " '" + dt.Rows[i]["IGSTAmt"].ToString() + "',";   
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                            SSQL = SSQL + " '0.00',";
                        }

                        SSQL = SSQL + " '" + dt.Rows[i]["IncTaxAmt"].ToString() + "','0.00','0.00','0.00','0.00','0.00','" + CoralPurOrdNo + "',";
                        SSQL = SSQL + " '" + ItemRemarks.Replace("'", "\"") + "','Add','" + SessionUserID + "','" + SessionUserName + "',";
                        SSQL = SSQL + " '" + CalendarWeek.Replace("'", "\"") + "')";


                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                }
            }
            if (SaveType == "1")
            {
                TransactionNoGenerate TransNO_up = new TransactionNoGenerate();
                string Auto_Transaction_No_Up = "";

                if (MatType == "2")
                {
                    Auto_Transaction_No_Up = TransNO_up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Asset Purchase Order", SessionFinYearVal, "4");
                }
                else
                {
                    Auto_Transaction_No_Up = TransNO_up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");
                }
            }
        }

        return result;
    }
    private static void GetItemDet(ref string PurRquNo, ref string SAPNo, out string UOM, out string ItemCode, out string SumRquQty, out string ReuiredDate, out string ItemRemarks, out string CalendarWeek, out string CGSTP, out string SGSTP, out string IGSTP)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select ItemCode,UOMCode,NoOfModel,ReuiredQty,SumRquQty,ReuiredDate,CalendarWeek,ItemRemarks,CGSTP,SGSTP,IGSTP ";
        SSQL = SSQL + " From Trans_Escrow_PurRqu_Sub where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' and Pur_Request_No ='" + PurRquNo + "' and SAPNo='" + SAPNo + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            UOM = DT.Rows[0]["UOMCode"].ToString();
            ItemCode = DT.Rows[0]["ItemCode"].ToString();
            //NoOfModel = DT.Rows[0]["NoOfModel"].ToString();
            //ReuiredQty = DT.Rows[0]["ReuiredQty"].ToString();
            SumRquQty = DT.Rows[0]["SumRquQty"].ToString();
            ReuiredDate = DT.Rows[0]["ReuiredDate"].ToString();
            ItemRemarks = DT.Rows[0]["ItemRemarks"].ToString();
            CalendarWeek = DT.Rows[0]["CalendarWeek"].ToString();
            CGSTP = DT.Rows[0]["CGSTP"].ToString();
            SGSTP = DT.Rows[0]["SGSTP"].ToString();
            IGSTP = DT.Rows[0]["IGSTP"].ToString();
        }
        else
        {
            UOM = "";
            ItemCode = "";
            //NoOfModel = "";
            //ReuiredQty = "";
            SumRquQty = "";
            ReuiredDate = "";
            ItemRemarks = "";
            CalendarWeek = "";
            CGSTP = "";
            SGSTP = "";
            IGSTP = "";
        }
    }
}