﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Escrow_PurReq_Sub.aspx.cs" Inherits="Transaction_Escrow_PurRqu_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-wrapper">
        <asp:UpdatePanel ID="upPurReq" runat="server">
            <ContentTemplate>

                <section class="content-header">
                    <h1><i class=" text-primary"></i>Create New Escrow Purchase Request </h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                     
                                    <div class="row">
                                        <input type="hidden" value="1" id="hiddenSaveType" />
                                        <div class="col-md-3">
                                            <label for="exampleInputName">Material Type</label>

                                            <asp:DropDownList ID="ddlMatType" runat="server" class="form-control select2"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                <asp:ListItem Value="1">Raw Material</asp:ListItem>
                                                <asp:ListItem Value="2">Tools</asp:ListItem>
                                                <asp:ListItem Value="3">Asset</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label" for="Req_No">Request No.</label>
                                            <asp:Label ID="txtPurRequestNo" runat="server" class="form-control"></asp:Label>
                                            <span class="form-error"></span>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="exampleInputName">Date</label>
                                            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control datepicker" runat="server"
                                                AutoComplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="exampleInputName">Department</label>
                                            <asp:DropDownList ID="ddlDeptName" runat="server" class="form-control select2"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlDeptName_SelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:HiddenField runat="server" ID="hfDeptCode" />

                                            <%--<asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="ddlDeptName"
                                                ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server"
                                                EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>

                                    <div class="row" runat="server" style="padding-bottom: 2%">
                                        <div class="col-md-3">
                                            <label for="exampleInputName">Remarks</label>
                                            <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control"
                                                Style="resize: none" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="box box-primary">

                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Purchase Request List</span></h3>
                                                </div>

                                                <div class="box-body no-padding">

                                                    <asp:Panel ID="pnlRawmat" runat="server" class="panel panel-success">
                                                        <div class="panel-body">

                                                             <div class="row" style="padding-left:1%;padding-bottom:1%">
                                                                <h3 class="box-title" style="color:green;"><span>Raw Material</span></h3>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <label for="exampleInputName">Purchase Request Type</label>
                                                                    <asp:DropDownList ID="ddlPurRquType" class="form-control select2" runat="server">
                                                                        <asp:ListItem Value="1">Single Item</asp:ListItem>
                                                                        <asp:ListItem Value="2">Model Based</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <asp:Panel ID="pnlSingRM" runat="server" class="panel panel-success"
                                                                Style="padding-top: 1%; border: none">

                                                                <div class="row">

                                                                    <asp:HiddenField ID="hfMdlCode" runat="server" />
                                                                    <asp:HiddenField ID="hfMdlName" runat="server" />
                                                                    <asp:HiddenField ID="hfPrdPrtCode" runat="server" />
                                                                    <asp:HiddenField ID="hfPrdPrtName" runat="server" />
                                                                    <asp:HiddenField ID="hfUOM" runat="server" />
                                                                    <asp:HiddenField ID="hfCGST" runat="server" />
                                                                    <asp:HiddenField ID="hfSGST" runat="server" />
                                                                    <asp:HiddenField ID="hfIGST" runat="server" />
                                                                    <asp:HiddenField ID="hfVAT" runat="server" />
                                                                    <asp:HiddenField ID="hfConvertStatus" runat="server" />

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">SAP No</label>
                                                                        <asp:DropDownList ID="ddlSapNo" runat="server" class="form-control select2"
                                                                            Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlSapNo_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        </p>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Material Name</label>
                                                                        <asp:DropDownList ID="ddlItemName" runat="server" class="form-control select2"
                                                                            AutoPostBack="true" Style="width: 100%" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">BOM Code(CORAL)</label>
                                                                        <asp:DropDownList ID="ddlReferCode" runat="server" class="form-control select2"
                                                                            Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlReferCode_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">UOM</label>
                                                                        <asp:Label runat="server" ID="txtUOM" class="form-control" autocomplete="off" AutoPostBack="true" OnTextChanged="txtBOMQty_TextChanged" />
                                                                        
                                                                      
                                                                    
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">BOM Qty/Gen</label>
                                                                        <asp:TextBox runat="server" ID="txtBOMQty" class="form-control" autocomplete="off" AutoPostBack="true" OnTextChanged="txtBOMQty_TextChanged" />
                                                                        
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtBOMQty" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    
                                                                    </div>

                                                                    <div class="col-md-2" runat="server" visible="false">
                                                                        <label for="exampleInputName">BOM Conversion Qty</label>
                                                                        <asp:Label runat="server" ID="lblConvQty" class="form-control"  />
                                                                    </div>

                                                                    
                                                                     <div class="col-md-2">
                                                                        <label for="exampleInputName">Order Qty</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtRquQty" AutoPostBack="true" OnTextChanged="txtOrderQty_TextChanged">0</asp:TextBox>
                                                                    
                                                                    
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                   <div class="col-md-2">
                                                                        <label for="exampleInputName">Generator Coverage</label>
                                                                        <asp:Label runat="server" ID="txtMdlQty" class="form-control" autocomplete="off" AutoPostBack="false" OnTextChanged="txtMdlQty_TextChanged" />
                                                                    
                                                                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtMdlQty" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>--%>

                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Calender Week</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtCalWeek" autocomplete="off" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Required Date</label>
                                                                        <asp:TextBox runat="server" class="form-control datepicker" ID="txtRquDate"></asp:TextBox>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Remark</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtRemark" autocomplete="off" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Rate</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtItemRate" autocomplete="off" />
                                                                        
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtItemRate" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    
                                                                    </div>
                                                                    

                                                                    <div class="col-md-2" runat="server" style="padding-top: 2%">
                                                                        <asp:Button runat="server" class="btn btn-success" ID="btnAddSingRM" Text="Add" OnClick="btnAddSingRM_Click" />
                                                                    </div>

                                                                </div>

                                                                <div class="row" style="padding-top: 1.5%">

                                                                    <div class="col-md-4">
                                                                        <asp:FileUpload runat="server" ID="FileUpload" class="form-control" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <asp:Button runat="server" ID="btnRMSingleExcelUpload" class="btn btn-success"
                                                                            Text="Upload" OnClick="btnExcelUpload_Click" />
                                                                    </div>

                                                                    

                                                                    <div id="ExcelTable"></div>
                                                                </div>

                                                                <div class="row" runat="server" style="padding-top:1%">
                                                                    <div class="col-md-12">
                                                                        <%--<label for="exampleInputName">Status</label>--%>
                                                                        <asp:Label runat="server" for="exampleInputName" Visible="false" 
                                                                            style="font-size:medium;font-weight:bold;color:red" id="lbluploadstatus" 
                                                                            Text="Deplicate SapNo's Are : " ></asp:Label>

                                                                        <asp:Label runat="server" for="exampleInputName" Visible="false" 
                                                                            style="font-size:medium;font-weight:bold;color:red" id="lbluploadNewSapNo" 
                                                                            Text="New SapNo's Are : " ></asp:Label>

                                                                    </div>
                                                                </div>

                                                                <div class="row" runat="server" style="padding-top: 1%">
                                                                    <asp:Panel ID="pnlRMSingleRpter" class="panel panel-success" runat="server"
                                                                        Height="400px" Style="overflow-x: scroll; overflow-y: scroll" BorderStyle="None">
                                                                        <div class="col-md-12">
                                                                            <asp:Repeater ID="rptRMSingleRM" runat="server" EnableViewState="false">
                                                                                <HeaderTemplate>
                                                                                    <table id="tblRMSingle" class="table table-hover table-bordered table-striped">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th>S.No</th>
                                                                                                <th>Sap_No</th>
                                                                                                <th runat="server" visible="false">ItemCode</th>
                                                                                                <th>Item_Name</th>
                                                                                                <th>UOM</th>
                                                                                                <th runat="server" visible="false">ModelCode</th>
                                                                                                <th runat="server" visible="false">ModelName</th>
                                                                                                <th runat="server" visible="false">PrdPartCode</th>
                                                                                                <th runat="server" visible="false">PrdPartName</th>
                                                                                                <th>BOM_Qty</th>
                                                                                                <th>Generator Coverage</th>
                                                                                                <th>Order_Qty</th>
                                                                                                <th>Calender_Week</th>
                                                                                                <th>Requ.Date</th>
                                                                                                <th>Remarks</th>
                                                                                                <th>Item_Rate</th>
                                                                                                <th runat="server" visible="false">CGST</th>
                                                                                                <th runat="server" visible="false">SGST</th>
                                                                                                <th runat="server" visible="false">IGST</th>
                                                                                                <th>Mode</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td><%# Container.ItemIndex + 1 %></td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblSAPNo" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblItemCode" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("ItemCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblItemName" runat="server" Style="width: 100%; border: none"
                                                                                                Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblMdlCode" runat="server" Text='<%# Eval("ModelCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblMdlName" runat="server" Text='<%# Eval("ModelName")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblPrdPrtCode" runat="server" Text='<%# Eval("PrdPartCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblPrdPrtName" runat="server" Text='<%# Eval("PrdPartName")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="true">
                                                                                            <asp:TextBox ID="txtGrdBOMQty" class="form-control" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("BOMQty")%>' AutoPostBack="true"
                                                                                                OnTextChanged="txtGrdBOMQty_TextChanged">
                                                                                            </asp:TextBox>

                                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" 
                                                                                                FilterType="Custom,Numbers" TargetControlID="txtGrdBOMQty" ValidChars="0123456789./">
                                                                                            </cc1:FilteredTextBoxExtender>

                                                                                        </td>

                                                                                        <td runat="server" visible="true">
                                                                                            <asp:TextBox class="form-control" ID="txtGrdMdlQty" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("ModelQty")%>' AutoPostBack="true"
                                                                                                OnTextChanged="txtGrdMdlQty_TextChanged">
                                                                                            </asp:TextBox>

                                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" 
                                                                                                FilterType="Custom,Numbers" TargetControlID="txtGrdMdlQty" ValidChars="0123456789./">
                                                                                            </cc1:FilteredTextBoxExtender>

                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="txtGrdRquQty" Style="width: 100%" runat="server" Text='<%# Eval("RquQty")%>'
                                                                                                AutoComplete="off">
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:TextBox ID="txtGrdCalWeek" class="form-control" Style="width: 100%; border: none"
                                                                                                AutoComplete="off" runat="server" Text='<%# Eval("CalWeek")%>'>
                                                                                            </asp:TextBox>
                                                                                        </td>

                                                                                        <td runat="server" visible="true">
                                                                                            <asp:Label ID="txtRDate" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("ReuiredDate")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="true">
                                                                                            <asp:Label ID="lblRemarks" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblItemRate" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("ItemRate")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblCGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("CGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblSGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("SGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblIGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("IGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                                Text="" CommandName='<%# Eval("ItemCode")%>' OnCommand="btnDetRMClick"
                                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                            </asp:LinkButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate></table></FooterTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>

                                                                <div class="box-footer">
                                                                    <div class="form-group">
                                                                        <asp:Button ID="btnRMSingleItemSave" class="btn btn-primary" runat="server" Text="Save"
                                                                            ValidationGroup="Validate_Field" OnClick="btnRMSingleItemSave_Click" />
                                                                    </div>
                                                                </div>

                                                            </asp:Panel>

                                                            <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Total Qty</label>
                                                                        <asp:Label ID="lblRMTotQty" runat="server" class="form-control" Style="width: 100%" >
                                                                        </asp:Label>
                                                                    </div>
                                                                </div>

                                                            <asp:Panel ID="pnlMdlRM" runat="server" class="panel panel-success"
                                                                Style="padding-top: 1%; border: none">
                                                            </asp:Panel>

                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlTool" runat="server" class="panel panel-success" Visible="false">
                                                        <div class="panel-body">
                                                            <div class="row" style="padding-left:1%;padding-bottom:1%">
                                                                <h3 class="box-title" style="color:red;"><span>Tools</span></h3>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <label for="exampleInputName">Purchase Request Type</label>
                                                                    <asp:DropDownList ID="DropDownList1" class="form-control select2" runat="server">
                                                                        <asp:ListItem Value="1">Single Item</asp:ListItem>
                                                                        <asp:ListItem Value="2">Model Based</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <asp:Panel ID="pnlToolSingle" runat="server" class="panel panel-success"
                                                                Style="padding-top: 1%; border: none">

                                                                <div class="row">

                                                                    <asp:HiddenField ID="hfToolUOM" runat="server" />
                                                                    <asp:HiddenField ID="hfToolModelCode" runat="server" />
                                                                    <asp:HiddenField ID="hfToolModelName" runat="server" />
                                                                    <asp:HiddenField ID="hfToolAssemblyCode" runat="server" />
                                                                    <asp:HiddenField ID="hfToolAssemblyName" runat="server" />
                                                                    <asp:HiddenField ID="hfProductCode" runat="server" />
                                                                    <asp:HiddenField ID="hfProductName" runat="server" />
                                                                    <asp:HiddenField ID="hdToolCGSTP" runat="server" />
                                                                    <asp:HiddenField ID="hdToolSGSTP" runat="server" />
                                                                    <asp:HiddenField ID="hdToolIGSTP" runat="server" />
                                                                    <asp:HiddenField ID="hfToolCode" runat="server" />

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Reference Code(CORAL)</label>
                                                                        <asp:DropDownList ID="ddlToolRefCode" runat="server" class="form-control select2"
                                                                            Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlToolRefCode_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Tools Name</label>
                                                                        <asp:DropDownList ID="ddlToolName" runat="server" class="form-control select2"
                                                                            AutoPostBack="true" Style="width: 100%" OnSelectedIndexChanged="ddlToolName_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">SAP No</label>
                                                                        <asp:DropDownList ID="ddlToolSapNo" runat="server" class="form-control select2"
                                                                            Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlToolSapNo_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        </p>
                                                                    </div>

                                                                    <div class="col-md-2" runat="server" visible="false">
                                                                        <label for="exampleInputName">Required Qty</label>
                                                                        <asp:Label runat="server" class="form-control" Text="1.00" ID="txtToolQty"></asp:Label>
                                                                    </div>

                                                                    <div class="col-md-2" runat="server" visible="false">
                                                                        <label for="exampleInputName">Model Qty</label>
                                                                        <asp:Label runat="server" ID="txtToolMdlQty" Text="1.00" class="form-control">
                                                                        </asp:Label>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Request Qty</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtToolRquQty" autocomplete="off">
                                                                        </asp:TextBox>

                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtToolRquQty" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Calender Week</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtToolCalWeek" autocomplete="off" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Required Date</label>
                                                                        <asp:TextBox runat="server" class="form-control datepicker" ID="txtToolRquDate"></asp:TextBox>
                                                                    </div>

                                                                </div>

                                                                <div class="row">



                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Remark</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtToolRemarks" autocomplete="off" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">RateINR</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtToolINR" autocomplete="off" />
                                                                        
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtToolINR" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">RateEUR</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtToolEUR" autocomplete="off" />
                                                                        
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtToolEUR" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="col-md-2" runat="server" style="padding-top: 2%">
                                                                        <asp:Button runat="server" class="btn btn-success" ID="btnToolAdd" Text="Add" OnClick="btnToolAdd_Click" />
                                                                    </div>

                                                                </div>

                                                                <div class="row" style="padding-top: 1.5%">

                                                                    <div class="col-md-4">
                                                                        <asp:FileUpload runat="server" ID="ToolFileUpload" class="form-control" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <asp:Button runat="server" ID="btnToolUpload" class="btn btn-success"
                                                                            Text="Upload" OnClick="btnToolUpload_Click" />
                                                                    </div>
                                                                </div>

                                                                <div class="row" runat="server" style="padding-top:1%">
                                                                    <div class="col-md-12">
                                                                        <%--<label for="exampleInputName">Status</label>--%>
                                                                        <asp:Label runat="server" for="exampleInputName" Visible="false" 
                                                                            style="font-size:medium;font-weight:bold;color:red" id="lblToolsUploadStatus" 
                                                                            Text="Deplicate SapNo's Are :" ></asp:Label>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" runat="server" style="padding-top: 1%">
                                                                    <asp:Panel ID="pnlGrdToolSingle" class="panel panel-success" runat="server"
                                                                        Height="400px" Style="overflow-x: scroll; overflow-y: scroll" BorderStyle="None">
                                                                        <div class="col-md-12">
                                                                             <asp:Repeater ID="rptToolSingle" runat="server" EnableViewState="false">
                                                                                <HeaderTemplate>
                                                                                    <table id="tblToolsSingle" class="table table-hover table-bordered table-striped">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th>S.No</th>
                                                                                                <th>RefCode</th>
                                                                                                <th>SapNo</th>
                                                                                                
                                                                                                <th runat="server" visible="false">ToolCode</th>
                                                                                                <th>ToolName</th>
                                                                                                <th>UOM</th>
                                                                                                <th runat="server" visible="false">ModelCode</th>
                                                                                                <th runat="server" visible="false">ModelName</th>
                                                                                                <th runat="server" visible="false">AssStpCode</th>
                                                                                                <th runat="server" visible="false">AssStpName</th>
                                                                                                <th runat="server" visible="false">PrdCode</th>
                                                                                                <th runat="server" visible="false">PrdName</th>
                                                                                                <th runat="server" visible="false">BOMQty</th>
                                                                                                <th runat="server" visible="false">ModelQty</th>
                                                                                                <th>RequestQty</th>
                                                                                                <th>CalenderWeek</th>
                                                                                                <th>Requ.Date</th>
                                                                                                <th>Remarks</th>
                                                                                                <th>Rate(INR)</th>
                                                                                                <th>Rate(EUR)</th>
                                                                                                <th runat="server" visible="false">CGST</th>
                                                                                                <th runat="server" visible="false">SGST</th>
                                                                                                <th runat="server" visible="false">IGST</th>
                                                                                                <th>Mode</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td><%# Container.ItemIndex + 1 %></td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblTolGrdRefCode" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("RefCode")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblTolGrdSAPNo" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdToolsCode" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("ToolCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblTolGrdToolsName" runat="server" Style="width: 100%; border: none"
                                                                                                Text='<%# Eval("ToolName")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblTolGrdUOM" runat="server" Text='<%# Eval("UOM")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdMdlCode" runat="server" Text='<%# Eval("ModelCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdMdlName" runat="server" Text='<%# Eval("ModelName")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdAssStpCode" runat="server" Text='<%# Eval("AssStpCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdAssStpName" runat="server" Text='<%# Eval("AssStpName")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdPrdCode" runat="server" Text='<%# Eval("PrdCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdPrdName" runat="server" Text='<%# Eval("PrdName")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdRquQty" runat="server" Text='<%# Eval("ToolsQty")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdMdlQty" runat="server" Text='<%# Eval("ModelQty")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:TextBox ID="txtTolGrdPRQty" class="form-control" Style="width: 100%" AutoPostBack="true"
                                                                                                AutoComplete="off" runat="server" Text='<%# Eval("RequestQty")%>' OnTextChanged="txtTolGrdPRQty_TextChanged">
                                                                                            </asp:TextBox>

                                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" 
                                                                                                FilterType="Custom,Numbers" TargetControlID="txtTolGrdPRQty" ValidChars="0123456789./">
                                                                                            </cc1:FilteredTextBoxExtender>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:TextBox ID="txtTolGrdCalWeek" class="form-control" Style="width: 100%" 
                                                                                                AutoComplete="off" runat="server" Text='<%# Eval("CalenderWeek")%>'>
                                                                                            </asp:TextBox>
                                                                                        </td>

                                                                                        <td runat="server" visible="true">
                                                                                            <asp:Label ID="txtTolGrdRDate" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("ReuiredDate")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="true">
                                                                                            <asp:Label ID="lblTolGrdRemarks" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblTolGrdINR" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("RateINR")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblTolGrdEUR" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("RateEUR")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdCGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("CGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdSGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("SGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblTolGrdIGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("IGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                                Text="" CommandName='<%# Eval("RefCode")%>'  OnCommand="btnDetToolsClick"
                                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                            </asp:LinkButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate></table></FooterTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Total Qty</label>
                                                                        <asp:Label ID="lblToolsTotQty" runat="server" class="form-control" Style="width: 100%" >
                                                                        </asp:Label>
                                                                    </div>
                                                                </div>

                                                                <div class="box-footer">
                                                                    <div class="form-group">
                                                                        <asp:Button ID="btnToolsSingleSave" class="btn btn-primary" runat="server" Text="Save"
                                                                            ValidationGroup="Validate_Field" OnClick="btnToolsSingleSave_Click" />
                                                                    </div>
                                                                </div>

                                                            </asp:Panel>

                                                            <asp:Panel ID="Panel4" runat="server" class="panel panel-success"
                                                                Style="padding-top: 1%; border: none">
                                                            </asp:Panel>

                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlAsset" runat="server" class="panel panel-success" Visible="false">
                                                        <div class="panel-body">
                                                            <div class="row" style="padding-left:1%;padding-bottom:1%">
                                                                <h3 class="box-title" style="color:blue;"><span>Asset</span></h3>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <label for="exampleInputName">Purchase Request Type</label>
                                                                    <asp:DropDownList ID="DropDownList2" class="form-control select2" runat="server">
                                                                        <asp:ListItem Value="1">Single Item</asp:ListItem>
                                                                        <asp:ListItem Value="2">Model Based</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <asp:Panel ID="pnlAssetSingle" runat="server" class="panel panel-success"
                                                                Style="padding-top: 1%; border: none">

                                                                <div class="row">

                                                                    <asp:HiddenField ID="hfAssetUOM" runat="server" />
                                                                    <asp:HiddenField ID="hfAssetModelCode" runat="server" />
                                                                    <asp:HiddenField ID="hfAssetModelName" runat="server" />
                                                                    <asp:HiddenField ID="hfAssetAssemblyCode" runat="server" />
                                                                    <asp:HiddenField ID="hfAssetAssemblyName" runat="server" />
                                                                    <asp:HiddenField ID="hfAssetProductCode" runat="server" />
                                                                    <asp:HiddenField ID="hfAssetProductName" runat="server" />
                                                                    <asp:HiddenField ID="hdAssetCGSTP" runat="server" />
                                                                    <asp:HiddenField ID="hdAssetSGSTP" runat="server" />
                                                                    <asp:HiddenField ID="hdAssetIGSTP" runat="server" />
                                                                    <asp:HiddenField ID="hfAssetCode" runat="server" />

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Reference Code(CORAL)</label>
                                                                        <asp:DropDownList ID="ddlAssetRefcode" runat="server" class="form-control select2"
                                                                            Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlAssetRefcode_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Asset Name</label>
                                                                        <asp:DropDownList ID="ddlAssetName" runat="server" class="form-control select2"
                                                                            AutoPostBack="true" Style="width: 100%" OnSelectedIndexChanged="ddlAssetName_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">SAP No</label>
                                                                        <asp:DropDownList ID="ddlAssetSapNo" runat="server" class="form-control select2"
                                                                            Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlAssetSapNo_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        </p>
                                                                    </div>

                                                                    <div class="col-md-2" runat="server" visible="false">
                                                                        <label for="exampleInputName">Required Qty</label>
                                                                        <asp:Label runat="server" class="form-control" Text="1.00" ID="lblAssetReqQty"></asp:Label>
                                                                    </div>

                                                                    <div class="col-md-2" runat="server" visible="false">
                                                                        <label for="exampleInputName">Model Qty</label>
                                                                        <asp:Label runat="server" ID="lblAssetModelQty" Text="1.00" class="form-control">
                                                                        </asp:Label>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Request Qty</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtAssetRquestQty" autocomplete="off">
                                                                        </asp:TextBox>

                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtAssetRquestQty" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Calender Week</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtAssetCalWeek" autocomplete="off" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Required Date</label>
                                                                        <asp:TextBox runat="server" class="form-control datepicker" ID="txtAssetRquDate"></asp:TextBox>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                     
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Remark</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtAssetRemark" autocomplete="off" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">RateINR</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtAssetINR" autocomplete="off" />
                                                                        
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtAssetINR" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">RateEUR</label>
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtAssetEUR" autocomplete="off" />
                                                                        
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" 
                                                                            FilterType="Custom,Numbers" TargetControlID="txtAssetEUR" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="col-md-2" runat="server" style="padding-top: 2%">
                                                                        <asp:Button runat="server" class="btn btn-success" ID="btnAssetAdd" Text="Add" OnClick="btnAssetAdd_Click" />
                                                                    </div>

                                                                </div>

                                                                <div class="row" style="padding-top: 1.5%">

                                                                    <div class="col-md-4">
                                                                        <asp:FileUpload runat="server" ID="fileupAsset" class="form-control" />
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <asp:Button runat="server" ID="btnAssetUpload" class="btn btn-success"
                                                                            Text="Upload" OnClick="btnAssetUpload_Click" />
                                                                    </div>

                                                                </div>

                                                                <div class="row" runat="server" style="padding-top:1%">
                                                                    <div class="col-md-12">
                                                                        <%--<label for="exampleInputName">Status</label>--%>
                                                                        <asp:Label runat="server" for="exampleInputName" Visible="false" 
                                                                            style="font-size:medium;font-weight:bold;color:red" id="lblAssetUploadstatus" 
                                                                            Text="Deplicate SapNo's Are :" ></asp:Label>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" runat="server" style="padding-top: 1%">
                                                                    <asp:Panel ID="pnlAssetSingleGrid" class="panel panel-success" runat="server"
                                                                        Height="400px" Style="overflow-x: scroll; overflow-y: scroll" BorderStyle="None">
                                                                        <div class="col-md-12">
                                                                             <asp:Repeater ID="rptAssetSingle" runat="server" EnableViewState="false">
                                                                                <HeaderTemplate>
                                                                                    <table id="tblAssetSingle" class="table table-hover table-bordered table-striped">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th>S.No</th>
                                                                                                <th>RefCode</th>
                                                                                                <th>SapNo</th>
                                                                                                <th runat="server" visible="false">AssetCode</th>
                                                                                                <th>AssetName</th>
                                                                                                <th>UOM</th>
                                                                                                <th runat="server" visible="false">ModelCode</th>
                                                                                                <th runat="server" visible="false">ModelName</th>
                                                                                                <th runat="server" visible="false">AssStpCode</th>
                                                                                                <th runat="server" visible="false">AssStpName</th>
                                                                                                <th runat="server" visible="false">PrdCode</th>
                                                                                                <th runat="server" visible="false">PrdName</th>
                                                                                                <th runat="server" visible="false">BOMQty</th>
                                                                                                <th runat="server" visible="false">ModelQty</th>
                                                                                                <th>RequestQty</th>
                                                                                                <th>CalenderWeek</th>
                                                                                                <th>Requ.Date</th>
                                                                                                <th>Remarks</th>
                                                                                                <th>Rate(INR)</th>
                                                                                                <th>Rate(EUR)</th>
                                                                                                <th runat="server" visible="false">CGST</th>
                                                                                                <th runat="server" visible="false">SGST</th>
                                                                                                <th runat="server" visible="false">IGST</th>
                                                                                                <th>Mode</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td><%# Container.ItemIndex + 1 %></td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblASTGrdRefCode" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("RefCode")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblASTGrdSAPNo" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdToolsCode" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("AssetCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblASTGrdToolsName" runat="server" Style="width: 100%; border: none"
                                                                                                Text='<%# Eval("AssetName")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblASTGrdUOM" runat="server" Text='<%# Eval("UOM")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdMdlCode" runat="server" Text='<%# Eval("ModelCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdMdlName" runat="server" Text='<%# Eval("ModelName")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdAssStpCode" runat="server" Text='<%# Eval("AssStpCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdAssStpName" runat="server" Text='<%# Eval("AssStpName")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdPrdCode" runat="server" Text='<%# Eval("PrdCode")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdPrdName" runat="server" Text='<%# Eval("PrdName")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdRquQty" runat="server" Text='<%# Eval("AssetQty")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdMdlQty" runat="server" Text='<%# Eval("ModelQty")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:TextBox ID="txtASTGrdPRQty" class="form-control" Style="width: 100%" AutoPostBack="true"
                                                                                                AutoComplete="off" runat="server" Text='<%# Eval("RequestQty")%>' OnTextChanged="txtAssetGrdPRQty_TextChanged">
                                                                                            </asp:TextBox>

                                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" 
                                                                                                FilterType="Custom,Numbers" TargetControlID="txtASTGrdPRQty" ValidChars="0123456789./">
                                                                                            </cc1:FilteredTextBoxExtender>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:TextBox ID="txtASTGrdCalWeek" class="form-control" Style="width: 100%" 
                                                                                                AutoComplete="off" runat="server" Text='<%# Eval("CalenderWeek")%>'>
                                                                                            </asp:TextBox>
                                                                                        </td>

                                                                                        <td runat="server" visible="true">
                                                                                            <asp:Label ID="txtASTGrdRDate" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("ReuiredDate")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="true">
                                                                                            <asp:Label ID="lblASTGrdRemarks" Style="width: 100%; border: none"
                                                                                                runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblASTGrdINR" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("RateINR")%>'>
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:Label ID="lblASTGrdEUR" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("RateEUR")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdCGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("CGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdSGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("SGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td runat="server" visible="false">
                                                                                            <asp:Label ID="lblASTGrdIGSTP" Style="width: 100%"
                                                                                                runat="server" Text='<%# Eval("IGSTP")%>'
                                                                                                AutoComplete="off"> 
                                                                                            </asp:Label>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                                Text="" CommandName='<%# Eval("RefCode")%>'  OnCommand="btnDetAssetClick"
                                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                            </asp:LinkButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate></table></FooterTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                        <label for="exampleInputName">Total Qty</label>
                                                                        <asp:Label ID="lblAssetTotQty" runat="server" class="form-control" Style="width: 100%" >
                                                                        </asp:Label>
                                                                    </div>
                                                                </div>

                                                                <div class="box-footer">
                                                                    <div class="form-group">
                                                                        <asp:Button ID="btnAssetSingleSave" class="btn btn-primary" runat="server" Text="Save"
                                                                            ValidationGroup="Validate_Field" OnClick="btnAssetSingleSave_Click" />
                                                                    </div>
                                                                </div>

                                                            </asp:Panel>

                                                            <asp:Panel ID="Panel5" runat="server" class="panel panel-success"
                                                                Style="padding-top: 1%; border: none">
                                                            </asp:Panel>

                                                        </div>
                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <%--<asp:Button ID="btnSave" Visible="false" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />--%>
                                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" />
                                            <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server"  Text="Back" OnClick="btnBackEnquiry_Click" />
                                            <%--<asp:Button ID="btnBackReqApprv" class="btn btn-default" runat="server" Text="Approve" Visible="false" OnClick="btnBackReqApprv_Click" />--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnToolUpload" />
                <asp:PostBackTrigger ControlID="btnRMSingleExcelUpload" />
                <asp:PostBackTrigger ControlID="btnAssetUpload" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <%--<script src="/assets/js/CoreExcel.js"></script>
    <script src="/assets/js/CoreExcel_xls.js"></script>--%>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            console.log("Start");
            $("#tblRMSingle").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });

            $("#tblToolsSingle").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });

            $("#tblAssetSingle").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });

        });
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                    $("#tblRMSingle").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "ordering": false,
                        "searching": true
                    });

                    $("#tblToolsSingle").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "ordering": false,
                        "searching": true
                    });

                    $("#tblAssetSingle").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "ordering": false,
                        "searching": true
                        //"drawCallback":true
                    });

                }
            });
        };
    </script>

    <style type="text/css">
        .table-loading-overlay {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 10;
            background: #000000;
            opacity: 0.5;
        }

        .table-loading-inner {
            width: 100%;
            height: 100%;
        }
    </style>

</asp:Content>

