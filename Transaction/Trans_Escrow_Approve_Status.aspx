﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Escrow_Approve_Status.aspx.cs" Inherits="Transaction_Trans_Escrow_Approve_Status" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Purchase Order Approval Status (Escrow)</span></h3>
                                </div>
                                
                                <div class="row" runat="server" style="padding-top: 25px; padding-bottom: 25px">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="control-label" for="Req_No">Purchase Order Status</label>
                                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                                OnSelectedIndexChanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">

                                        
                                        <asp:Panel runat="server" ID="pnlApproval" Visible="true" >
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sl. No</th>
                                                                    <th>PO No</th>
                                                                    <th>Date</th>
                                                                    <th>Supplier</th>
                                                                    <th>Model Name</th>
                                                                    <th>Material_Type</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1 %></td>
                                                            <td><%# Eval("RefNo")%></td>
                                                            <td><%# Eval("Std_PO_Date")%></td>
                                                            <td><%# Eval("Supp_Name")%></td>
                                                            <td><%# Eval("Pur_Request_No")%></td>
                                                            <td><%# Eval("Material_Type")%></td>
                                                            <td><%# Eval("Status")%></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </asp:Panel>

                                        

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

