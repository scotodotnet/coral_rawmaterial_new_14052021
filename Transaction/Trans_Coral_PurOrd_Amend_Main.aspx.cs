﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Trans_Coral_PO_Amend_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Escrow Pruchase Order Amend";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Enercon Purchase Order");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add New Purchase Order...');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Adding New Std Purchase Order..');", true);
        }
        else
        {
            Session.Remove("Pur_RequestNo_Approval");
            Session.Remove("Transaction_No");
            Session.Remove("Amend_Po_No");
            Response.Redirect("Trans_Escrow_PurOrd_Amend_Sub.aspx");
        }

        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Transaction_No");
        //Session.Remove("Std_PO_No");
        //Response.Redirect("standard_po.aspx");
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
         
        //User Rights Check Start

        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Enercon Purchase Order");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit New Purchase Order...');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Adding New Std Purchase Order..');", true);
        }
        else
        {
            DataTable dtdpurchase = new DataTable();

            SSQL = "Select * from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And FinYearcode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And PurOrdNo='" + e.CommandName.ToString() + "'";

            dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Print Purchase Order...');", true);
            if (dtdpurchase.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Goods Received, So Can Not be Edited ..');", true);
            }
            else
            {
                string Enquiry_No_Str = e.CommandName.ToString();
                Session.Remove("Pur_RequestNo_Approval");
                Session.Remove("Transaction_No");
                Session.Remove("Amend_Po_No");
                Session["Amend_Po_No"] = Enquiry_No_Str;
                Response.Redirect("Trans_Escrow_PurOrd_Amend_Sub.aspx");
            }

            //SSQL = "select PO_Status from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            //SSQL = SSQL + "And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";

            //dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
            //string status = dtdpurchase.Rows[0]["PO_Status"].ToString();

            //if (status == "" || status == "0")
            //{
            //    string Enquiry_No_Str = e.CommandName.ToString();
            //    Session.Remove("Pur_RequestNo_Approval");
            //    Session.Remove("Transaction_No");
            //    Session.Remove("Std_PO_No");
            //    Session["Std_PO_No"] = Enquiry_No_Str;
            //    Response.Redirect("standard_po.aspx");
            //}
            //else if (status == "2")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Already Rejected..');", true);
            //}
            //else if (status == "3")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order put in pending..');", true);
            //}
            //else if (status == "1")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Approved Standard Purchase Order Cant Edit..');", true);
            //}
            //else if (status == "6")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Rejected by FM..');", true);
            //}
        }
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Enercon Purchase Order");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Purchase Order...');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Std Purchase Order..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start

        DataTable DT_Check = new DataTable();

        SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + "FinYearCode = '" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "' And PO_Status='1'";

        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Do not Delete Std Purchase Order Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {

            DataTable dtdpurchase = new DataTable();
            SSQL = "select PO_Status from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + "And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";
            dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
            string status = dtdpurchase.Rows[0]["PO_Status"].ToString();

            if (status == "" || status == "0")
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    //Delete Main Table
                    SSQL = "Delete from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    //Delete Main Sub Table
                    SSQL = "Delete from Trans_Enercon_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Details Deleted Successfully');", true);
                    Load_Data_Enquiry_Grid();
                }
            }
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order put in pending..');", true);
            }
            else if (status == "1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Cant Edit..');", true);
            }
            else if (status == "6")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Rejected by FM..');", true);
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select AmendPONo,AmendPoDate,Supp_Name,Case When AmendStatus='1' Then 'Successfully' Else 'Pending' End Status ";
        SSQL = SSQL + " From Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' ";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        string RptName = "";
        string StdPurOrdNo = "";
        string SupQtnNo = "";
        string DeptName = "";
        bool ErrFlag = false;

        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow Purchase Order");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Print Purchase Order...');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Std Purchase Order..');", true);
        }
        else
        {
            RptName = "Escrow Purchase Order Amend Details Invoice Format";
            StdPurOrdNo = e.CommandName.ToString();
            ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");
        }
    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblStatus = Item.FindControl("lblStatus") as Label;

            if (lblStatus.Text == "Successfully")
            {
                lblStatus.BackColor = Color.Green;
                lblStatus.ForeColor = Color.White;

            }
            else
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.ForeColor = Color.White;
            }
        }
    }
}