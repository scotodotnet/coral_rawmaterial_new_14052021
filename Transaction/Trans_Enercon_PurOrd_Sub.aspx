﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Enercon_PurOrd_Sub.aspx.cs" Inherits="Trans_Enercon_PurOrd_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Create New Purchase Order (Enercon)</h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Material Type</label>
                                                <asp:DropDownList ID="ddlMatType" runat="server" class="form-control select2"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">RawMaterial</asp:ListItem>
                                                    <asp:ListItem Value="2">Tools</asp:ListItem>
                                                    <asp:ListItem Value="3">Asset</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfMatTypeCode" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label" for="Req_No">Order No</label>
                                                <asp:Label ID="txtStdOrderNo" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Coral Order No</label>
                                                <asp:TextBox ID="txtRefNo"  class="form-control" runat="server"
                                                    AutoComplete="off"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Date</label>
                                                <asp:TextBox ID="txtDate" MaxLength="20" class="form-control datepicker" runat="server" AutoComplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supp.Ref.Doc.No</label>
                                                <asp:DropDownList ID="txtSuppRefDocNo" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtSuppRefDocNo_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>



                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Quotation No</label>
                                                <asp:TextBox ID="txtSuppQutaNo" class="form-control" runat="server"
                                                    AutoComplete="off"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Quotation Date</label>
                                                <asp:TextBox ID="txtSuppQutaDate" MaxLength="20" class="form-control"
                                                    runat="server" AutoComplete="off"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-4" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Doc Date</label>
                                                <asp:TextBox ID="txtDocDate" MaxLength="20" class="form-control datepicker" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Name</label>
                                                <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                <asp:HiddenField ID="txtSuppCodehide" runat="server" />
                                                <asp:DropDownList ID="txtSupplierName_Select" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtSupplierName_Select_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtSupplierName_Select" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Department Name</label>
                                                <asp:DropDownList ID="txtDepartmentName" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtDepartmentName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfDeptCode" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Dispatch Through</label>
                                                <asp:DropDownList name="txtDeliveryMode" ID="txtDeliveryMode" class="form-control select2" 
                                                    AutoPostBack="true" runat="server" OnSelectedIndexChanged="txtDeliveryMode_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfDeliMode" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Payment Mode</label>
                                                <asp:DropDownList id="txtPaymentMode" class="form-control select2" runat="server">
                                                    <asp:ListItem value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem value="CASH">CASH</asp:ListItem>
                                                    <asp:ListItem value="CHEQUE">CHEQUE</asp:ListItem>
                                                    <asp:ListItem value="NEFT">NEFT</asp:ListItem>
                                                    <asp:ListItem value="RTGS">RTGS</asp:ListItem>
                                                    <asp:ListItem value="DD">DD</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Request Date</label>
                                                <asp:TextBox ID="txtDeliveryDate" class="form-control datepicker" AutoComplete="off" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDeliveryDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtDeliveryDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Delivery At</label>
                                                <asp:TextBox ID="txtDeliveryAt" class="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Payment Terms</label>
                                                <asp:TextBox ID="txtPaymentTerms" TextMode="MultiLine" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Purpose (Sub)</label>
                                            <asp:TextBox ID="txtSplIns" TextMode="MultiLine" class="form-control" Style="resize: none" 
                                                Text="PURCHASE ORDER FOR ENERCON GENERATOR PRODUCTION_E138-EP.E2" 
                                                runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Special Instruction (Sub)</label>
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" Text="Further to your offer 2012061-Coral-Offer IF through mail dated 28th Jan-2021, We are hereby releasing the Purchase order for Batch Type Spray Paintbooth for E138-EP3.E2"  
                                                class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Note (Freight)</label>
                                            <asp:TextBox ID="txtNote" TextMode="MultiLine" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Delivery Schedules</label>
                                            <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Currency Type</label>
                                            <asp:DropDownList ID="ddlCurrencyType" class="form-control select2" runat="server"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlCurrencyType_SelectedIndexChanged">
                                                <asp:ListItem Value="1">INR</asp:ListItem>
                                                <asp:ListItem Value="2">EUR</asp:ListItem>
                                                <asp:ListItem Value="3">USD</asp:ListItem>
                                                <asp:ListItem Value="4">JPY</asp:ListItem>
                                                <asp:ListItem Value="5">GBP</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Select Tax Type</label>
                                            <asp:DropDownList ID="ddlTaxType" class="form-control select2" runat="server" 
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlTaxType_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfGSTDescr" runat="server" />
                                        </div>

                                    </div>

                                    <div class="row" runat="server" visible="false">

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Mode Of Transfer</label>
                                            <asp:TextBox ID="txtModeOfTransfer" class="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Cheque No</label>
                                            <asp:TextBox ID="txtChequeNo" class="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Cheque Date</label>
                                            <asp:TextBox ID="txtChequeDate" class="form-control datepicker" runat="server"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtChequeDate" ValidChars="0123456789./">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Cheque Amount</label>
                                            <asp:TextBox ID="txtChequeAmt" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="form-group col-md-4" runat="server" visible="false">
                                            <label for="exampleInputName">Pur Order Type</label>
                                            <asp:RadioButtonList ID="RdpPOType" class="form-control" runat="server"
                                                RepeatColumns="3" OnSelectedIndexChanged="RdpPOType_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="1" Text="Direct" style="padding-right: 40px"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Req/Amend" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Special"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <br />

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">

                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Item Details</h3>
                                                </div>
                                                <div class="box-body no-padding">

                                                    <asp:Panel ID="pnlRawMaterialPO" runat="server" class="panel panel-success">
                                                        <div class="panel-body">

                                                            <div class="row" runat="server" visible="false">
                                                                <div class="col-md-2">
                                                                    <label for="exampleInputName">Material Type</label>
                                                                    <asp:DropDownList ID="ddlPurMode" runat="server" AutoPostBack="true" class="form-control select2"
                                                                        OnSelectedIndexChanged="ddlPurMode_SelectedIndexChanged">
                                                                        <asp:ListItem Value="1">Purchase Request</asp:ListItem>
                                                                        <asp:ListItem Value="2">Direct</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <asp:Panel ID="pnlPurRqu" runat="server">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName">Request No</label>
                                                                            <asp:DropDownList ID="txtRequestNo" runat="server" class="form-control select2"
                                                                                OnSelectedIndexChanged="txtRequestNo_SelectedIndexChanged" AutoPostBack="true">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">Request Date</label>
                                                                        <asp:TextBox ID="txtRequestDate" class="form-control datepicker" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>

                                                            <asp:Panel ID="pnlDirectOrd" runat="server" Visible="false">

                                                                <div class="row">

                                                                    <div class="form-group col-md-3">
                                                                        <label for="exampleInputName">Item Name</label>
                                                                        <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="txtItemNameSelect_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                                        <asp:HiddenField ID="txtItemCodeHide" runat="server" />
                                                                        <asp:HiddenField ID="hfUOM" runat="server" />
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">Qty</label>
                                                                        <asp:TextBox ID="txtOrderQty" class="form-control" runat="server" AutoPostBack="true"
                                                                            OnTextChanged="txtOrderQty_TextChanged"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ControlToValidate="txtOrderQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                        <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                            TargetControlID="txtOrderQty" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="form-group col-md-1" runat="server" visible="false">
                                                                        <label for="exampleInputName">Req. Qty</label>
                                                                        <asp:TextBox ID="txtRequiredQty" class="form-control" runat="server"></asp:TextBox>
                                                                    </div>

                                                                    <div class="form-group col-md-3">
                                                                        <label for="exampleInputName">Req. Date</label>
                                                                        <asp:TextBox ID="txtRequiredDate" class="form-control datepicker" AutoComplete="off"
                                                                            runat="server"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                            TargetControlID="txtRequiredDate" ValidChars="0123456789./">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="form-group col-md-1" runat="server" visible="false">
                                                                        <label for="exampleInputName">Model Qty</label>
                                                                        <asp:TextBox ID="txtModelQty" class="form-control" runat="server"></asp:TextBox>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">Rate_INR</label>
                                                                        <asp:TextBox ID="txtRateINR" class="form-control" runat="server" Text="0.0" AutoPostBack="true"
                                                                            OnTextChanged="txtRateINR_TextChanged"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ControlToValidate="txtRateINR" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                            TargetControlID="txtRateINR" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">Rate_EUR</label>
                                                                        <asp:TextBox ID="txtRateEUR" class="form-control" runat="server" Text="0.0" AutoPostBack="true"
                                                                            OnTextChanged="txtRateEUR_TextChanged"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ControlToValidate="txtRateEUR" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                            TargetControlID="txtRateEUR" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">Item Total</label>
                                                                        <asp:TextBox ID="txtItemTotal" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="form-group col-md-3" runat="server" visible="false">
                                                                        <label for="exampleInputName">Remarks</label>
                                                                        <asp:TextBox ID="txtRemarks" class="form-control" runat="server"></asp:TextBox>
                                                                    </div>

                                                                    <div class="form-group col-md-3" runat="server" visible="false">
                                                                        <label for="exampleInputName">Calendar Week</label>
                                                                        <asp:TextBox ID="txtCalWeek" class="form-control" runat="server"></asp:TextBox>
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label for="exampleInputName">Disc. Per</label>
                                                                        <asp:TextBox ID="txtDiscount_Per" class="form-control" runat="server"
                                                                            Text="0.0" AutoPostBack="true" OnTextChanged="txtDiscount_Per_TextChanged"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                            TargetControlID="txtDiscount_Per" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">Discount Amount</label>
                                                                        <asp:Label ID="txtDiscount_Amount" runat="server" class="form-control" Text="0.0"></asp:Label>
                                                                    </div>

                                                                    <div class="form-group col-md-2" runat="server" visible="false">
                                                                        <label for="exampleInputName">GST Type</label>
                                                                        <asp:DropDownList ID="txtGST_Type" runat="server" class="form-control">
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label for="exampleInputName">CGST %</label>
                                                                        <asp:Label ID="txtCGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">CGST Amt</label>
                                                                        <asp:Label ID="txtCGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label for="exampleInputName">SGST %</label>
                                                                        <asp:Label ID="txtSGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">SGST Amt</label>
                                                                        <asp:Label ID="txtSGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label for="exampleInputName">IGST %</label>
                                                                        <asp:Label ID="txtIGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">IGST Amt</label>
                                                                        <asp:Label ID="txtIGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="form-group col-md-1">
                                                                        <label for="exampleInputName">VAT %</label>
                                                                        <asp:Label ID="txtVAT_Per" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                                    </div>

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">VAT Amt</label>
                                                                        <asp:Label ID="txtVAT_AMT" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                                    </div>
                                                                    <asp:HiddenField ID="TotalGstPer_Hidden" runat="server" />

                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">Other Amt</label>
                                                                        <asp:TextBox ID="txtOtherCharge" class="form-control" runat="server" Text="0"
                                                                            AutoPostBack="true" OnTextChanged="txtOtherCharge_TextChanged"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                            TargetControlID="txtOtherCharge" ValidChars="0123456789.-">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>
                                                                    <div class="form-group col-md-2">
                                                                        <label for="exampleInputName">Total Amt</label>
                                                                        <asp:Label ID="txtFinal_Total_Amt" runat="server" class="form-control" Text="0.0"></asp:Label>
                                                                    </div>
                                                                    <div class="form-group col-md-1" runat="server" style="padding-top: 1.9%">
                                                                        <asp:Button ID="btnAddItem" class="btn btn-primary" runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click" />
                                                                    </div>
                                                                </div>

                                                            </asp:Panel>

                                                            <div class="box box-primary">

                                                                <div class="box-header with-border">
                                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Order Item List</span></h3>
                                                                </div>

                                                                <asp:Panel ID="pnlRpter" runat="server" Height="400px" Style="overflow-x: scroll; overflow-y: scroll">
                                                                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                                        <HeaderTemplate>
                                                                            <table id="tblPurOrd" class="table table-hover table-bordered table-striped">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>S.No</th>
                                                                                        <th>SapNo</th>
                                                                                        <th runat="server" visible="false">ItemCode</th>
                                                                                        <th>ItemName</th>
                                                                                        <th>UOM</th>
                                                                                        <th runat="server" visible="false">BOMQty</th>
                                                                                        <th runat="server" visible="false">ModelQty</th>
                                                                                        <th>RequestQty</th>
                                                                                        <th>OrderQty</th>
                                                                                        <th runat="server" visible="false">Requ.Date</th>
                                                                                        <th>Rate(INR)</th>
                                                                                        <th>Rate(EUR)</th>
                                                                                        <th runat="server" visible="false">Remarks</th>
                                                                                        <th>ItemTotal</th>
                                                                                        <th runat="server" visible="true">Discount_Per</th>
                                                                                        <th runat="server" visible="true">Discount</th>
                                                                                        <th runat="server" visible="false">TaxPer</th>
                                                                                        <th runat="server" visible="false">TaxAmount</th>
                                                                                        <th runat="server" visible="false">GST%</th>
                                                                                        <th runat="server" visible="true">CGSTPer</th>
                                                                                        <th>CGST</th>
                                                                                        <th runat="server" visible="true">SGSTPer</th>
                                                                                        <th>SGST</th>
                                                                                        <th runat="server" visible="true">IGSTPer</th>
                                                                                        <th>IGST</th>
                                                                                        <th runat="server" visible="false">OtherCharge</th>
                                                                                        <th>Total</th>
                                                                                        <th>CalenderWeek</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Container.ItemIndex + 1 %></td>

                                                                                <td>
                                                                                    <asp:Label ID="lblSAPNo" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:Label ID="lblItemCode" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("ItemCode")%>'>
                                                                                    </asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblItemName" runat="server"
                                                                                        Style="width: 100%; border: none"
                                                                                        Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblUOM" runat="server"
                                                                                        Text='<%# Eval("UOMCode")%>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:Label ID="lblBOMQty" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("ReuiredQty")%>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:Label ID="lblMdlQty" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("ModelQty")%>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblRquQty" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("RquQty")%>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:TextBox ID="txtOrdQty" class="form-control" Style="width: 100%"
                                                                                        runat="server" Text='<%# Eval("OrderQty")%>'
                                                                                        AutoComplete="off" AutoPostBack="true" OnTextChanged="txtOrdQty_TextChanged">
                                                                                    </asp:TextBox>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:TextBox ID="txtRDate" Style="width: 100%"
                                                                                        runat="server" Text='<%# Eval("ReuiredDate")%>'></asp:TextBox>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:TextBox ID="txtGrdRateINR" class="form-control" Style="width: 100%"
                                                                                        runat="server" Text='<%# Eval("RateINR")%>'
                                                                                        AutoComplete="off" AutoPostBack="true" OnTextChanged="txtGrdRateINR_TextChanged">
                                                                                    </asp:TextBox>

                                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                                        TargetControlID="txtGrdRateINR" ValidChars="0123456789.">
                                                                                    </cc1:FilteredTextBoxExtender>

                                                                                </td>

                                                                                <td>
                                                                                    <asp:TextBox ID="txtGrdRateEUR" class="form-control" Style="width: 100%"
                                                                                        runat="server" Text='<%# Eval("RateEUR")%>'
                                                                                        AutoComplete="off" AutoPostBack="true" OnTextChanged="txtGrdRateEUR_TextChanged">
                                                                                    </asp:TextBox>

                                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                                        TargetControlID="txtGrdRateEUR" ValidChars="0123456789.">
                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:Label ID="lblRemarks" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblItemTot" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("ItemTotal") %>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="true">
                                                                                    <asp:TextBox ID="txtGrdDiscPre" class="form-control" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("Discount_Per")%>' AutoPostBack="true" 
                                                                                        OnTextChanged="txtGrdDisc_TextChanged">
                                                                                    </asp:TextBox>

                                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                                        TargetControlID="txtGrdDiscPre" ValidChars="0123456789.">
                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                </td>

                                                                                <td runat="server" visible="true">
                                                                                    <asp:Label ID="lblDisc" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("Discount") %>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:Label ID="lblTaxPer" class="form-control" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("TaxPer") %>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:Label ID="lblTaxAmt" class="form-control" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("TaxAmount") %>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:Label ID="lblGSTPer" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("GstPer")%>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="true">
                                                                                    <asp:Label ID="lblCGSTPer" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("CGSTPer") %>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblCGSTAmt" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("CGSTAmount") %>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="true">
                                                                                    <asp:Label ID="lblSGSTPer" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("SGSTPer") %>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblSGSTAmt" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("SGSTAmount") %>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="true">
                                                                                    <asp:Label ID="lblIGSTPer" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("IGSTPer") %>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblIGSTAmt" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("IGSTAmount") %>'></asp:Label>
                                                                                </td>

                                                                                <td runat="server" visible="false">
                                                                                    <asp:Label ID="lblOtherAmt" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("OtherCharge") %>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblLineTot" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("LineTotal")%>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="lblCalWeek" Style="width: 100%; border: none"
                                                                                        runat="server" Text='<%# Eval("CalendarWeek")%>'></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="GridDeleteClick" CommandName='<%# Eval("ItemCode")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                </asp:Panel>
                                                            </div>

                                                            <div class="row">
                                                                <%--<div class="form-group col-md-2"></div>--%>
                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">Total Qty</label>
                                                                    <asp:Label ID="txtTotQty" class="form-control" runat="server"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">Item Total</label>
                                                                    <asp:Label ID="lblTotItem" class="form-control" runat="server"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">Discount</label>
                                                                    <asp:Label ID="lblTotDisc" class="form-control" runat="server">
                                                                        </asp:Label>

                                                                </div>

                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">CGST</label>
                                                                    <asp:Label ID="lblTotCGST" class="form-control" runat="server"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">SGST</label>
                                                                    <asp:Label ID="lblTotSGST" class="form-control" runat="server"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">IGST</label>
                                                                    <asp:Label ID="lblTotIGST" class="form-control" runat="server"></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-6"></div>

                                                                <div class="form-group col-md-2">

                                                                    <label for="exampleInputName">Total Amt</label>
                                                                    <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>

                                                                <div class="form-group col-md-1" runat="server" visible="false">
                                                                    <label for="exampleInputName">Tax Per</label>
                                                                    <asp:TextBox ID="txtTax" class="form-control" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtTax" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                                <div class="form-group col-md-2" runat="server" visible="false">
                                                                    <label for="exampleInputName">Tax Amount</label>
                                                                    <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0"></asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-1" runat="server" visible="false">
                                                                    <label for="exampleInputName">Discount</label>
                                                                    <asp:TextBox ID="txtDiscount" class="form-control" runat="server"
                                                                        OnTextChanged="txtDiscount_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtDiscount" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>

                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">Add or Less</label>
                                                                    <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server"
                                                                        Text="0" AutoPostBack="true" OnTextChanged="txtAddOrLess_TextChanged"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">Net Amt</label>
                                                                    <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnPO_Preview" class="btn btn-primary" runat="server" Visible="false" Text="PO Preview" OnClick="btnPO_Preview_Click" />
                                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                            <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server" Text="Back" OnClick="btnBackRequest_Click" />
                                            <asp:Button ID="btnApprove" class="btn btn-default" runat="server"
                                                Text="Approve" Visible="false" OnClick="btnApprove_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            console.log("Start");
            $("#tblPurOrd").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": true,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });
        });
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                    $("#tblPurOrd").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": true,
                        "ordering": false,
                        "searching": true
                        //"drawCallback":true
                    });
                }
            });
        };
    </script>
</asp:Content>

