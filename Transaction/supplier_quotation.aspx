﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="supplier_quotation.aspx.cs" Inherits="Transaction_supplier_quotation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy'});
                    $('.select2').select2();
                }
            });
        };
    </script>
<div class="content-wrapper">
    <asp:UpdatePanel ID="upSupQtSub" runat="server">
        <ContentTemplate>
             <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class=" text-primary"></i> Create New Quotation</h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">                            
                            <%--<div class="form-group" hidden>
                                <label class="control-label" for="Status">Status</label>
                                <input class="form-control" type="text" id="Status" name="Status" value="" />
                                <span class="text-danger field-validation-valid" data-valmsg-for="Status" data-valmsg-replace="true"></span>
                            </div>--%>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="Req_No">Quotation No</label>
                                    <asp:Label ID="txtQuatationNo" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Qtn.Date</label>
                                    <asp:TextBox ID="txtQtnDate" MaxLength="20" class="form-control datepicker" AutoComplete="off" runat="server"></asp:TextBox>
					                <asp:RequiredFieldValidator ControlToValidate="txtQtnDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Name</label>
                                    <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                    <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					                <asp:DropDownList ID="txtSupplierName_Select" runat="server" class="form-control select2" AutoPostBack="true" 
                                        onselectedindexchanged="txtSupplierName_Select_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtSupplierName_Select" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier.Qtn.No</label>					          
					                <asp:TextBox ID="txtSuppQtnNo" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtSuppQtnNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier.Qtn.Date</label>
                                    <asp:TextBox ID="txtSuppQutDate" class="form-control datepicker" runat="server" AutoComplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtSuppQutDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Ref.Doc.No</label>
					                <asp:TextBox ID="txtRefDocNo" class="form-control" runat="server"></asp:TextBox>                                    
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Ref.Date</label>
                                    <asp:TextBox ID="txtRefDocDate" class="form-control datepicker" runat="server" AutoComplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtRefDocDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Payment Mode</label>
					                <select name="txtPaymentMode" id="txtPaymentMode" class="form-control select2" runat="server">
	                                    <option value="- select -">- select -</option>
	                                    <option value="CASH">CASH</option>
	                                    <option value="CHEQUE">CHEQUE</option>
	                                    <option value="NEFT">NEFT</option>
	                                    <option value="RTGS">RTGS</option>
	                                    <option value="DD">DD</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                       
                        <div class="row">
                           
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Payment Terms</label>
					                <asp:TextBox ID="txtPaymentTerms" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                       
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Delivery Details</label>
					            <asp:TextBox ID="txtDelivery" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>	
					        </div>                            
					    </div>

                        

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Quotation Item List</span></h3>
                                        <div class="box-tools pull-right">
                                            <div class="has-feedback">
                                                <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <!-- /.box-tools -->
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                    
                                        <div class="row">
                            <div class="form-group col-md-3">
					            <label for="exampleInputName">Purchase Request No</label>
                                <asp:DropDownList ID="txtPurchase_Request_No" runat="server" class="form-control select2" AutoPostBack="true" 
                                        onselectedindexchanged="txtPurchase_Request_No_SelectedIndexChanged">
                                </asp:DropDownList>
					        </div>
                            <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
                                <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true" 
                                        onselectedindexchanged="txtItemNameSelect_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtItemCode" class="form-control" runat="server" Visible="false"></asp:TextBox>  
					        </div>
                            <div class="form-group col-md-3">
					            <label for="exampleInputName">Rate</label>
                                <asp:TextBox ID="txtRate" class="form-control" runat="server" MaxLength="10"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtRate" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
                            <div class="form-group col-md-2">
					            <br />
					            <asp:Button ID="Button1" class="btn btn-success"  runat="server" Text="Add" OnClick="btnAddItem_Click"/>
					        </div>
                        </div>

					                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>S. No</th>
                                                <th>PR No</th>
                                                <th>Item Code</th>
                                                <th>Item Name</th>
                                                <th>UOM Code</th>
                                                <th>Rate</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("Pur_Request_No")%></td>
                                        <td><%# Eval("ItemCode")%></td>
                                        <td><%# Eval("ItemName")%></td>
                                        <td><%# Eval("UOMCode")%></td>
                                        <td><%# Eval("Rate")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
			                </asp:Repeater>
					            




                                    <!-- /.table -->
                                </div>
                                <!-- /.mail-box-messages -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.col -->
                </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary"  runat="server" Text="Save" OnClick="btnSave_Click"/>
                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server" Text="Back" OnClick="btnBackEnquiry_Click"/>     
                            <asp:Button ID="btnApprove" class="btn btn-default" runat="server" 
                                Text="Approve" Visible="false" onclick="btnApprove_Click"/>
                        </div>
                    </div>
                                <!-- /.box-footer-->
                </div>
                            <!-- /.box -->
            </div>
            
        </div>
                   
        </section>
        <!-- /.content -->
        </ContentTemplate>
    </asp:UpdatePanel>
       
    </div>
</asp:Content>

