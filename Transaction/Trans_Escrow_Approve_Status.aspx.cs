﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_Trans_Escrow_Approve_Status : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionUserType;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        SessionUserType = Session["RoleCode"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Escrow Purchase Order Approval Status";
            //Department_Code_Add();
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();
        }
        Load_Data_Empty_Grid();
    }

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }

    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_Grid();
    }

    private void Load_Data_Empty_Grid()
    {

        string SSQL = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();


        DT.Columns.Add("RefNo");
        DT.Columns.Add("Std_PO_No");
        DT.Columns.Add("Std_PO_Date");
        DT.Columns.Add("Supp_Name");
        DT.Columns.Add("Pur_Request_No");
        DT.Columns.Add("Material_Type");
        DT.Columns.Add("Status");

        if (txtRequestStatus.Text == "Pending List")
        {
            SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,Part_No as Pur_Request_No,";
            SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
            SSQL = SSQL + " When MatType='2' then 'Tools'";
            SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type,'' as Status,PO_Status ";
            SSQL = SSQL + " From Trans_Escrow_PurOrd_Main  ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And PO_Status <> '-1' And PO_Status <> '4' And Active!='Delete' Order By Std_PO_No desc";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                string PO_Status = "";
                if (DT.Rows[i]["PO_Status"].ToString()=="0")
                {
                    PO_Status = "Level 1 Pending";
                }
                if (DT.Rows[i]["PO_Status"].ToString() == "1")
                {
                    PO_Status = "Level 2 Pending";
                }
                if (DT.Rows[i]["PO_Status"].ToString() == "2")
                {
                    PO_Status = "Level 3 Pending";
                }
                if (DT.Rows[i]["PO_Status"].ToString() == "3")
                {
                    PO_Status = "Level 4 Pending";
                }

                DT.Rows[i]["Status"] = PO_Status;
            }

            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,Part_No as Pur_Request_No,";
            SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
            SSQL = SSQL + " When MatType='2' then 'Tools'";
            SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type,'Approved' as Status,PO_Status ";
            SSQL = SSQL + " From Trans_Escrow_PurOrd_Main  ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And PO_Status = '4' And Active!='Delete' Order By Std_PO_No desc";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,Part_No as Pur_Request_No,";
            SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
            SSQL = SSQL + " When MatType='2' then 'Tools'";
            SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type,'' as Status,PO_Status ";
            SSQL = SSQL + " From Trans_Escrow_PurOrd_Main  ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And PO_Status = '-1' And Active!='Delete' Order By Std_PO_No desc";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);


            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
        }
    }
}