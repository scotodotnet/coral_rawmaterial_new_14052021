﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Escrow_PurOrd_Sub_Old.aspx.cs" Inherits="Trans_Escrow_PurOrd_Sub_Old" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upform1" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Create New Purchase Order (Escrow)</h1>
                </section>

                <section class="content">
                    <div class="row PruOrd">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">

                                    <div class="row">
                                        <input type="hidden" value="1" id="hiddenSaveType" />
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Material Type</label>

                                                <select id="ddlMatTyp" class="form-control select2" style="width: 100%">
                                                    <option value="1">RawMaterial</option>
                                                    <option value="2">Tools</option>
                                                </select>

                                                <asp:DropDownList ID="ddlMatType" Visible="false" runat="server" class="form-control select2"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlMatType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">RawMaterial</asp:ListItem>
                                                    <asp:ListItem Value="2">Tools</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label" for="Req_No">Order No</label>
                                                <label class="form-control" id="lblPurOrdNo"><span class="lblPurOrdNo"></span></label>
                                                <asp:Label ID="txtStdOrderNo" runat="server" class="form-control" Visible="false"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Coral Order No</label>
                                                <input class="form-control req" id="txtCoralPurOrdNo" autocomplete="off" />
                                                <span class="form-error"></span>
                                                <asp:DropDownList ID="txtSuppRefDocNo" runat="server" class="form-control select2" Visible="false" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtSuppRefDocNo_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Date</label>
                                                <input class="form-control datepicker req" id="txtOrdDate" placeholder="dd/MM/yyyy" maxlength="12"
                                                    autocomplete="off" />
                                                <span class="form-error"></span>


                                                <asp:TextBox ID="txtDate" MaxLength="20" Visible="false" class="form-control datepicker" runat="server" AutoComplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                         <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Quotation No</label>
                                                <input class="form-control req" id="txtQutNo" autocomplete="off" />
                                                <span class="form-error"></span>
                                            </div>
                                        </div>

                                         <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Quotation Date</label>
                                                <input class="form-control req" id="txtQutDate" autocomplete="off" />
                                                <span class="form-error"></span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Supplier Name</label>

                                                <select class="form-control select2 selectreq" id="ddlSuppName" style="width: 100%">
                                                    <%--<option value="-Select-">-Select-</option>--%>
                                                </select>
                                                <%--<label class="form-control" id="lblSuppCode" style="visibility:hidden" ></label>--%>
                                                <span class="form-error"></span>

                                                <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                <asp:HiddenField ID="txtSuppCodehide" runat="server" />
                                                <asp:DropDownList ID="txtSupplierName_Select" runat="server" Visible="false" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtSupplierName_Select_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtSupplierName_Select" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Doc Date</label>
                                                <asp:TextBox ID="txtDocDate" MaxLength="20" class="form-control datepicker" runat="server">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Department Name</label>
                                                <select ID="ddlDeptName" class="form-control select2 selectreq" style="width: 100%"></select>
                                                <p class="form-error"></p>

                                                <asp:DropDownList ID="txtDepartmentName" runat="server" Visible="false" class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Dispatch Through</label>
                                                <select ID="ddlDeliveryMode" class="form-control select2 selectreq" style="width: 100%"></select>
                                                <p class="form-error"></p>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Payment Mode</label>
                                                <select name="txtPaymentMode" id="txtPaymentMode" class="form-control select2 selectreq" style="width: 100%">
                                                    <option value="- select -">- select -</option>
                                                    <option value="CASH">CASH</option>
                                                    <option value="CHEQUE">CHEQUE</option>
                                                    <option value="NEFT">NEFT</option>
                                                    <option value="RTGS">RTGS</option>
                                                    <option value="DD">DD</option>
                                                </select>
                                                <p class="form-error"></p>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Request Date</label>

                                                <input id="txtDeliDate" class="form-control datepicker req" autocomplete="off" />
                                                <span class="form-error"></span>

                                                <asp:TextBox ID="txtDeliveryDate" MaxLength="20" Visible="false" class="form-control datepicker" AutoComplete="off" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDeliveryDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtDeliveryDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        
                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Delivery At</label>
                                                <input id="txtDeliAt" class="form-control req" />
                                                <span class="form-error"></span>
                                                <asp:TextBox ID="txtDeliveryAt" Visible="false" class="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                         
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Payment Terms</label>
                                                <textarea id="txtPayTerms" class="form-control req" rows="2" style="resize: none"></textarea>
                                                <span class="form-error"></span>
                                                <asp:TextBox ID="txtPaymentTerms" TextMode="MultiLine" Visible="false" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Purpose (Sub)</label>
                                            <textarea id="txtDesc" class="form-control req" rows="2" style="resize: none">PURCHASE ORDER FOR ENERCON GENERATOR PRODUCTION_E138-EP.E2</textarea>
                                            <span class="form-error"></span>
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" Visible="false" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Special Instruction (Sub)</label>
                                            <textarea id="txtSplDesc" class="form-control req" rows="2" style="resize: none">Further to your offer 2012061-Coral-Offer IF through mail dated 28th Jan-2021, We are hereby releasing the Purchase order for Batch Type Spray Paintbooth for E138-EP3.E2
                                            </textarea>
                                            <span class="form-error"></span>
                                            <asp:TextBox ID="TextBox2" TextMode="MultiLine" Visible="false" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Note (Freight)</label>
                                            <textarea id="txtNote" class="form-control req" rows="2" style="resize: none"></textarea>
                                            <span class="form-error"></span>
                                            <asp:TextBox ID="txtNote1" class="form-control" Visible="false" Style="resize: none" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Delivery Schedules</label>
                                            <textarea id="txtOthers" class="form-control req" rows="2" style="resize: none"></textarea>
                                            <span class="form-error"></span>
                                            <asp:TextBox ID="txtOthers1" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Currency Type</label><br />

                                            <select id="ddlCurrencyType" class="form-control select2" style="width: 100%">
                                                <option value="INR">INR</option>
                                                <option value="EUR">EUR</option>
                                                <option value="USD">USD</option>
                                                <option value="JPY">JPY</option>
                                                <option value="GBP">GBP</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Select Tax Type</label>
                                            <select id="ddlTaxType" class="form-control select2" style="width: 100%">
                                                <option value="GST">GST</option>
                                                <option value="IGST">IGST</option>
                                                <option value="USD">USD</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="row" runat="server" visible="false">

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Mode Of Transfer</label>
                                            <asp:TextBox ID="txtModeOfTransfer" class="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Cheque No</label>
                                            <asp:TextBox ID="txtChequeNo" class="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Cheque Date</label>
                                            <asp:TextBox ID="txtChequeDate" class="form-control datepicker" runat="server"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtChequeDate" ValidChars="0123456789./">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Cheque Amount</label>
                                            <asp:TextBox ID="txtChequeAmt" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="form-group col-md-4" runat="server" visible="false">
                                            <label for="exampleInputName">Pur Order Type</label>
                                            <asp:RadioButtonList ID="RdpPOType" class="form-control" runat="server"
                                                RepeatColumns="3" OnSelectedIndexChanged="RdpPOType_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="1" Text="Direct" style="padding-right: 40px"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Req/Amend" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Special"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <br />

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="box box-primary">

                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Item Details</h3>
                                                </div>

                                                <div class="clearfix"></div>
                                                <br />

                                                <div class="box-body no-padding">
                                                    <div class="row" runat="server" visible="false">
                                                        <div class="col-md-2">
                                                            <label for="exampleInputName">Mode Of Purchase</label>
                                                            <asp:DropDownList ID="ddlPurMode" runat="server" AutoPostBack="true" class="form-control select2"
                                                                OnSelectedIndexChanged="ddlPurMode_SelectedIndexChanged">
                                                                <asp:ListItem Value="1">Purchase Request</asp:ListItem>
                                                                <asp:ListItem Value="2">Direct</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix" runat="server" visible="false"></div>

                                                <asp:Panel ID="pnlPurRqu" runat="server">
                                                    <div class="row">

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Request No</label>
                                                                <select id="ddlRequestNo" class="form-control select2" style="width: 100%">
                                                                </select>

                                                                <label id="lblPurRquNo" style="visibility:hidden" class="form-control"></label>

                                                                <asp:DropDownList ID="txtRequestNo" runat="server" Visible="false" class="form-control select2"
                                                                    OnSelectedIndexChanged="txtRequestNo_SelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Request Date</label>
                                                            <label id="txtReqDate" class="form-control"></label>
                                                            <asp:TextBox ID="txtRequestDate" Visible="false" class="form-control datepicker" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlDirectOrd" runat="server" Visible="false">

                                                    <div class="row">

                                                        <div class="form-group col-md-3">
                                                            <label for="exampleInputName">Item Name</label>
                                                            <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true"
                                                                OnSelectedIndexChanged="txtItemNameSelect_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                            <asp:HiddenField ID="txtItemCodeHide" runat="server" />
                                                            <asp:HiddenField ID="hfUOM" runat="server" />
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Qty</label>
                                                            <asp:TextBox ID="txtOrderQty" class="form-control" runat="server" AutoPostBack="true"
                                                                OnTextChanged="txtOrderQty_TextChanged"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtOrderQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtOrderQty" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                        <div class="form-group col-md-1" runat="server" visible="false">
                                                            <label for="exampleInputName">Req. Qty</label>
                                                            <asp:TextBox ID="txtRequiredQty" class="form-control" runat="server"></asp:TextBox>
                                                        </div>

                                                        <div class="form-group col-md-3">
                                                            <label for="exampleInputName">Req. Date</label>
                                                            <asp:TextBox ID="txtRequiredDate" class="form-control datepicker" AutoComplete="off"
                                                                runat="server"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtRequiredDate" ValidChars="0123456789./">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                        <div class="form-group col-md-1" runat="server" visible="false">
                                                            <label for="exampleInputName">Model Qty</label>
                                                            <asp:TextBox ID="txtModelQty" class="form-control" runat="server"></asp:TextBox>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Rate_INR</label>
                                                            <asp:TextBox ID="txtRateINR" class="form-control" runat="server" Text="0.0" AutoPostBack="true"
                                                                OnTextChanged="txtRateINR_TextChanged"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtRateINR" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtRateINR" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Rate_EUR</label>
                                                            <asp:TextBox ID="txtRateEUR" class="form-control" runat="server" Text="0.0" AutoPostBack="true"
                                                                OnTextChanged="txtRateEUR_TextChanged"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtRateEUR" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtRateEUR" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Item Total</label>
                                                            <asp:TextBox ID="txtItemTotal" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                        </div>


                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-3" runat="server" visible="false">
                                                            <label for="exampleInputName">Remarks</label>
                                                            <asp:TextBox ID="txtRemarks" class="form-control" runat="server"></asp:TextBox>
                                                        </div>

                                                        <div class="form-group col-md-1">
                                                            <label for="exampleInputName">Disc. Per</label>
                                                            <asp:TextBox ID="txtDiscount_Per" class="form-control" runat="server"
                                                                Text="0.0" AutoPostBack="true" OnTextChanged="txtDiscount_Per_TextChanged"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDiscount_Per" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Discount Amount</label>
                                                            <asp:Label ID="txtDiscount_Amount" runat="server" class="form-control" Text="0.0"></asp:Label>
                                                        </div>

                                                        <div class="form-group col-md-2" runat="server" visible="false">
                                                            <label for="exampleInputName">GST Type</label>
                                                            <asp:DropDownList ID="txtGST_Type" runat="server" class="form-control"
                                                                AutoPostBack="true" OnSelectedIndexChanged="txtGST_Type_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="form-group col-md-1">
                                                            <label for="exampleInputName">CGST %</label>
                                                            <asp:Label ID="txtCGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">CGST Amt</label>
                                                            <asp:Label ID="txtCGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                        </div>

                                                        <div class="form-group col-md-1">
                                                            <label for="exampleInputName">SGST %</label>
                                                            <asp:Label ID="txtSGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">SGST Amt</label>
                                                            <asp:Label ID="txtSGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                        </div>

                                                        <div class="form-group col-md-1">
                                                            <label for="exampleInputName">IGST %</label>
                                                            <asp:Label ID="txtIGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">IGST Amt</label>
                                                            <asp:Label ID="txtIGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                        </div>
                                                    </div>

                                                    <div class="row clearfix"></div>
                                                    <br />

                                                    <div class="row">
                                                        <div class="form-group col-md-1">
                                                            <label for="exampleInputName">VAT %</label>
                                                            <asp:Label ID="txtVAT_Per" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                        </div>

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">VAT Amt</label>
                                                            <asp:Label ID="txtVAT_AMT" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                                        </div>
                                                        <asp:HiddenField ID="TotalGstPer_Hidden" runat="server" />

                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Other Amt</label>
                                                            <asp:TextBox ID="txtOtherCharge" class="form-control" runat="server" Text="0"
                                                                AutoPostBack="true" OnTextChanged="txtOtherCharge_TextChanged"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtOtherCharge" ValidChars="0123456789.-">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label for="exampleInputName">Total Amt</label>
                                                            <asp:Label ID="txtFinal_Total_Amt" runat="server" class="form-control" Text="0.0"></asp:Label>
                                                        </div>
                                                        <div class="form-group col-md-1" runat="server" style="padding-top: 1.9%">
                                                            <asp:Button ID="btnAddItem" class="btn btn-primary" runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click" />
                                                        </div>
                                                    </div>

                                                </asp:Panel>


                                                <div class="box box-primary">

                                                    <div class="box-header with-border">
                                                        <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Order Item List</span></h3>
                                                    </div>

                                                    <div class="box-footer">
                                                        <div class="form-group">
                                                            <div class="table-responsive mailbox-messages">
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="divTable">
                                                                            <table id="TblItemDet" class="display table table-bordered table-responsive">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>S.No</th>
                                                                                        <th>SAP No</th>
                                                                                        <th>Item Code</th>
                                                                                        <th>Item Name</th>
                                                                                        <th>Model Qty</th>
                                                                                        <th>Required Qty</th>
                                                                                        <th>Order Qty</th>
                                                                                        <th>Required Date</th>
                                                                                        <th>Rate(INR)</th>
                                                                                        <th>Rate(EUR)</th>
                                                                                        <th>ItemTotal</th>
                                                                                        <th>CGSTP</th>
                                                                                        <th>CGST</th>
                                                                                        <th>SGSTP</th>
                                                                                        <th>SGST</th>
                                                                                        <th>IGSTP</th>
                                                                                        <th>IGST</th>
                                                                                        <th>NetAmount</th>
                                                                                        <th>calendar week</th>
                                                                                        <%--<th>Mode</th>--%>
                                                                                    </tr>
                                                                                </thead>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false" Visible="false">
                                                        <HeaderTemplate>
                                                            <table class="table table-hover table-striped RowTest">
                                                                <thead>
                                                                    <tr>
                                                                        <th>S.No</th>
                                                                        <th>SAP No</th>
                                                                        <th>Item Name</th>
                                                                        <th>Request Qty</th>
                                                                        <th>Order Qty</th>
                                                                        <th runat="server" visible="false">Requ. Date</th>
                                                                        <th>Rate(INR)</th>
                                                                        <th>Rate(EUR)</th>
                                                                        <th>ItemTotal</th>
                                                                        <th>GST%</th>
                                                                        <th>CGST</th>
                                                                        <th>SGST</th>
                                                                        <th>IGST</th>
                                                                        <th>Total</th>
                                                                        <th>Mode</th>
                                                                    </tr>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Container.ItemIndex + 1 %></td>

                                                                <td>
                                                                    <asp:Label ID="lblSAPNo" runat="server" Text='<%# Eval("SAPNo")%>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblItemCode" runat="server" Text='<%# Eval("ItemCode")%>'>
                                                                    </asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOMCode")%>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblRQty" runat="server" Text='<%# Eval("ReuiredQty")%>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblMdlQty" runat="server" Text='<%# Eval("ModelQty")%>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lblRquQty" runat="server" Text='<%# Eval("OrderQty")%>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:TextBox ID="txtOrdQty" BorderStyle="None" runat="server" Text='<%# Eval("OrderQty")%>'
                                                                        Width="50%" AutoComplete="off" AutoPostBack="true" OnTextChanged="txtOrdQty_TextChanged">
                                                                    </asp:TextBox>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:TextBox ID="txtRDate" BorderStyle="None" runat="server" Text='<%# Eval("ReuiredDate")%>'></asp:TextBox>
                                                                </td>

                                                                <td>
                                                                    <asp:TextBox ID="txtGrdRateINR" BorderStyle="None" runat="server" Text='<%# Eval("RateINR")%>'
                                                                        Width="75%" AutoComplete="off" AutoPostBack="true" OnTextChanged="txtGrdRateINR_TextChanged">
                                                                    </asp:TextBox>
                                                                </td>

                                                                <td>
                                                                    <asp:TextBox ID="txtGrdRateEUR" BorderStyle="None" runat="server" Text='<%# Eval("RateEUR")%>'
                                                                        Width="75%" AutoComplete="off" AutoPostBack="true" OnTextChanged="txtGrdRateEUR_TextChanged">
                                                                    </asp:TextBox>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lblItemTot" runat="server" Text='<%# Eval("ItemTotal") %>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblDiscPer" runat="server" Text='<%# Eval("Discount_Per")%>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblDisc" runat="server" Text='<%# Eval("Discount") %>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblTaxPer" runat="server" Text='<%# Eval("TaxPer") %>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblTaxAmt" runat="server" Text='<%# Eval("TaxAmount") %>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lblGSTPer" runat="server" Text='<%# Eval("GstPer")%>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblCGSTPer" runat="server" Text='<%# Eval("CGSTPer") %>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lblCGSTAmt" runat="server" Text='<%# Eval("CGSTAmount") %>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblSGSTPer" runat="server" Text='<%# Eval("SGSTPer") %>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lblSGSTAmt" runat="server" Text='<%# Eval("SGSTAmount") %>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblIGSTPer" runat="server" Text='<%# Eval("IGSTPer") %>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lblIGSTAmt" runat="server" Text='<%# Eval("IGSTAmount") %>'></asp:Label>
                                                                </td>

                                                                <td runat="server" visible="false">
                                                                    <asp:Label ID="lblOtherAmt" runat="server" Text='<%# Eval("OtherCharge") %>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lblLineTot" runat="server" Text='<%# Eval("LineTotal")%>'></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                        Text="" OnCommand="GridDeleteClick" CommandName='<%# Eval("ItemCode")%>'
                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate></table></FooterTemplate>
                                                    </asp:Repeater>

                                                </div>
                                                <!-- /.mail-box-messages -->
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-2"></div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Total Qty</label>
                                                    <label id="lblTotQty" class="form-control"></label>
                                                    <asp:Label ID="txtTotQty" Visible="false" class="form-control" runat="server"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Item Total Amount</label>
                                                    <label id="lblItemAmtTol" class="form-control"></label>
                                                    <asp:TextBox ID="txtTotAmt" class="form-control" Visible="false" runat="server" Text="0.0"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">CGST Amount</label>
                                                    <label class="form-control" id="lblTotCGSTAmt"></label>
                                                    <asp:TextBox ID="txtTax" class="form-control" Visible="false" runat="server"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtTax" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">SGST Amount</label>
                                                    <label class="form-control" id="lblTotSGSTAmt"></label>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">IGST Amount</label>
                                                    <label class="form-control" id="lblTotIGSTAmt"></label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6"></div>
                                                <div class="form-group col-md-2" runat="server" visible="false">
                                                    <label for="exampleInputName">Tax Amount</label>
                                                    <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-1" runat="server" visible="false">
                                                    <label for="exampleInputName">Discount</label>
                                                    <asp:TextBox ID="txtDiscount" class="form-control" runat="server"
                                                        OnTextChanged="txtDiscount_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtDiscount" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Total Amount</label>
                                                    <label class="form-control" id="lblTotAmt"></label>
                                                    <asp:TextBox ID="TextBox1" class="form-control" runat="server" Visible="false"
                                                        OnTextChanged="txtDiscount_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtDiscount" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Add or Less</label>
                                                    <input type="text" id="txtRoundOff" class="form-control" value="0.00" />
                                                    <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server" Visible="false"
                                                        Text="0" AutoPostBack="true" OnTextChanged="txtAddOrLess_TextChanged"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                        TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <label for="exampleInputName">Net Amt</label>
                                                    <label class="form-control" id="lblFinNetAmt"></label>
                                                    <asp:Label ID="txtNetAmt" runat="server" class="form-control" Visible="false"></asp:Label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">

                                            <input type="button" id="btnSavePurOrd" class="btn btn-primary" value="Save" />

                                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" OnClick="btnSave_Click" Visible="false" />
                                            <asp:Button ID="btnPO_Preview" class="btn btn-primary" runat="server" Visible="false" Text="PO Preview" OnClick="btnPO_Preview_Click" />
                                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                            <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server" Text="Back" OnClick="btnBackRequest_Click" />
                                            <asp:Button ID="btnApprove" class="btn btn-default" runat="server"
                                                Text="Approve" Visible="false" OnClick="btnApprove_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/Scripts/Transaction/Trans_Escrow_PurOrd_Sub.js"></script>

</asp:Content>

