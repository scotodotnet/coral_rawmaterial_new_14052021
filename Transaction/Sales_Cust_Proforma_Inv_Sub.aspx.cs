﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Sales_Transaction_Sales_Cust_Proforma_Inv_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionPurRequestNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal ReqQty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Customer Proforma Invoice Details";
            Initial_Data_Referesh();
            Current_Date_Display();
            Load_Data_Empty_Customer_Name();
            //Load_Data_Empty_PO_No();
            Load_Data_Empty_GST_Type();
            //Load_Data_Empty_Generator_Model();
            Load_Data_Empty_Transport();

            if (Session["Sales_Proforma_Invoice_No"] == null)
            {
                SessionPurRequestNo = "";

                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Sales Proforma Customer Invoice", SessionFinYearVal, "4");
                txtInvoice_NO.Text = Auto_Transaction_No;
            }
            else
            {
                SessionPurRequestNo = Session["Sales_Proforma_Invoice_No"].ToString();
                txtInvoice_NO.Text = SessionPurRequestNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }

    private void Current_Date_Display()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select CONVERT(Varchar(20),GETDATE(),103)";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtDate.Text = DT.Rows[0][0].ToString();
            txtParty_PO_Date.Text = DT.Rows[0][0].ToString();
        }
    }

    private void Load_Data_Empty_GST_Type()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtGST_Type.Items.Clear();
        SSQL = "Select GST_Type from GstMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtGST_Type.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["GST_Type"] = "-Select-";
        dr["GST_Type"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGST_Type.DataTextField = "GST_Type";
        txtGST_Type.DataValueField = "GST_Type";
        txtGST_Type.DataBind();

    }

    private void Load_Data_Empty_Customer_Name()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtCustomer_Name.Items.Clear();
        SSQL = "Select CustCode,CustName from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtCustomer_Name.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CustCode"] = "-Select-";
        dr["CustName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCustomer_Name.DataTextField = "CustName";
        txtCustomer_Name.DataValueField = "CustCode";
        txtCustomer_Name.DataBind();

    }

    protected void txtCustomer_Name_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_PO_No();
    }

    private void Load_Data_Empty_PO_No()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtPO_No.Items.Clear();
        SSQL = "Select Cus_Pur_Order_No from Sales_Customer_Purchase_Order_Main where Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And Cus_Pur_Order_No Not IN (Select Cus_Pur_Order_No from Sales_Proforma_Invoice_Main";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No <> '" + txtInvoice_NO.Text + "')";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtPO_No.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Cus_Pur_Order_No"] = "-Select-";
        dr["Cus_Pur_Order_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtPO_No.DataTextField = "Cus_Pur_Order_No";
        txtPO_No.DataValueField = "Cus_Pur_Order_No";
        txtPO_No.DataBind();

    }

    protected void txtPO_No_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Get Rate
        string query = "";
        DataTable DT_Q = new DataTable();
        query = "Select Cus_Pur_Order_Date,PaymentMode,Currency_Type from Sales_Customer_Purchase_Order_Main where Cus_Pur_Order_No='" + txtPO_No.SelectedValue + "'";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            txtPO_Date.Text = DT_Q.Rows[0]["Cus_Pur_Order_Date"].ToString();
            txtPayment_Mode.SelectedValue = DT_Q.Rows[0]["PaymentMode"].ToString();
            txtCurrency_Type.SelectedValue = DT_Q.Rows[0]["Currency_Type"].ToString();
        }
        else
        {
            txtPO_Date.Text = "";
            txtPayment_Mode.SelectedIndex = 0;
            txtCurrency_Type.SelectedIndex = 0;
        }
        Load_Data_Empty_Generator_Model();
        txtCurrency_Type_SelectedIndexChanged(sender, e);
    }

    private void Load_Data_Empty_Generator_Model()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();


        SSQL = "Select GenModelNo,GenModelName from Sales_Customer_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Cus_Pur_Order_No='" + txtPO_No.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtGenerator_Model.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["GenModelNo"] = "-Select-";
        dr["GenModelName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGenerator_Model.DataTextField = "GenModelName";
        txtGenerator_Model.DataValueField = "GenModelNo";
        txtGenerator_Model.DataBind();

    }

    protected void txtGenerator_Model_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Get Rate
        string query = "";
        DataTable DT_Q = new DataTable();
        query = "Select Rate,Qty from Sales_Customer_Purchase_Order_Main_Sub where GenModelNo='" + txtGenerator_Model.SelectedValue + "'";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Cus_Pur_Order_No='" + txtPO_No.SelectedValue + "'";
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            txtRate.Text = DT_Q.Rows[0]["Rate"].ToString();
            txtQty.Text = DT_Q.Rows[0]["Qty"].ToString();
            txtPO_Rate.Value = DT_Q.Rows[0]["Rate"].ToString();
        }
        else
        {
            txtRate.Text = "0";
            txtQty.Text = "1";
            txtPO_Rate.Value = "0";
        }
    }

    private void Load_Data_Empty_Transport()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtTransPort.Items.Clear();
        SSQL = "Select Cast(Transport_ID as varchar(20)) as Transport_ID,Transport_Name from TransportMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And Status='Add'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtTransPort.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Transport_ID"] = "-Select-";
        dr["Transport_Name"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtTransPort.DataTextField = "Transport_Name";
        txtTransPort.DataValueField = "Transport_ID";
        txtTransPort.DataBind();

    }

    protected void txtTransPort_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Get Rate
        string query = "";
        DataTable DT_Q = new DataTable();
        query = "Select TransPort_No from TransportMaster where Transport_Name='" + txtTransPort.SelectedItem.Text + "'";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            txtVehicleNo.Text = DT_Q.Rows[0]["TransPort_No"].ToString();
        }
        else
        {
            txtVehicleNo.Text = "";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Generator Details..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (txtDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Date..');", true);
        }
        if (txtCustomer_Name.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Customer Name..');", true);
        }
        if (txtParty_PO_No.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Party PO No..');", true);
        }
        if (txtParty_PO_Date.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Party PO Date..');", true);
        }
        if (txtPayment_Mode.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Payment Mode..');", true);
        }


        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Sales Proforma Customer Invoice", SessionFinYearVal, "4");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtInvoice_NO.Text = Auto_Transaction_No;
                }
            }
        }

        if (btnSave.Text == "Update")
        {
            SSQL = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + txtInvoice_NO.Text + "' And Status='1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sales Invoice Details get Approved Cannot Update it..');", true);
            }
        }



        if (!ErrFlag)
        {
            SSQL = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + txtInvoice_NO.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                SSQL = "Delete from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + txtInvoice_NO.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "Delete from Sales_Proforma_Invoice_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + txtInvoice_NO.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            //Response.Write(strValue);
            //Insert Main Table
            SSQL = "Insert Into Sales_Proforma_Invoice_Main(Ccode,Lcode,FinYearCode,FinYearVal,Sales_Inv_No,Sales_Inv_Date,Cus_Pur_Order_No,Cus_Pur_Order_Date,CustomerID,CustomerName,PaymentMode,";
            SSQL = SSQL + " TransPortCode,TransPortName,VehicleNo,DeliveryFrom,DeliveryAt,Note,Remarks,TotalAmt,DiscountPer,DiscountAmt,After_Discount_Total,Freight_Charges,Taxable_Amount,";
            SSQL = SSQL + " GST_Type,CGST_Per,CGST_Amt,SGST_Per,SGST_Amt,IGST_Per,IGST_Amt,GST_Total_Amt,GST_And_Gross_Amt,TCS_Tax_Type,TCS_Tax_Percent,TCS_Tax_Amt,Round_Off_val,NetAmount,";
            SSQL = SSQL + " ServerDate,Approvedby,Status,UserID,UserName,Currency_Type) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtInvoice_NO.Text + "',";
            SSQL = SSQL + " '" + txtDate.Text + "','" + txtParty_PO_No.Text + "','" + txtParty_PO_Date.Text + "','" + txtCustomer_Name.SelectedValue + "','" + txtCustomer_Name.SelectedItem.Text + "','" + txtPayment_Mode.SelectedValue + "',";
            SSQL = SSQL + " '" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "',";
            SSQL = SSQL + " '" + "" + "','" + txtSubTotal.Text + "','" + txtDiscount_Per.Text + "','" + txtDiscount_Amount.Text + "','" + txtAfter_Discount_Tot.Text + "','" + txtFreight_Charges.Text + "','" + txtTaxable_Amt.Text + "',";
            SSQL = SSQL + " '" + txtGST_Type.SelectedValue + "','" + txtCGST_Per.Text + "','" + txtCGST_Amt.Text + "','" + txtSGST_Per.Text + "','" + txtSGST_Amt.Text + "','" + txtIGST_Per.Text + "','" + txtIGST_Amt.Text + "','" + txtGST_Total.Text + "',";
            SSQL = SSQL + " '" + txtGST_And_Gross_Amt.Text + "','-Select-','0','0','" + txtRound_Off.Text + "','" + txtFinal_Net_Amt.Text + "',Convert(Datetime,GetDate(),103),'','0','" + SessionUserID + "','" + SessionUserName + "','" + txtCurrency_Type.SelectedValue + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Insert Into Sales_Proforma_Invoice_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Sales_Inv_No,Sales_Inv_Date,Cus_Pur_Order_No,Cus_Pur_Order_Date,GenModelNo,GenModelName,Sap_No,HSN_Code,Item_Desc,UOM,Qty,Rate,LineTotal,UserID,UserName) Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtInvoice_NO.Text + "','" + txtDate.Text + "','" + txtParty_PO_No.Text + "','" + txtParty_PO_Date.Text + "','" + dt.Rows[i]["GenModelNo"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["GenModelName"].ToString() + "','" + dt.Rows[i]["Sap_No"].ToString() + "','" + dt.Rows[i]["HSN_Code"].ToString() + "','" + dt.Rows[i]["Item_Desc"].ToString() + "','" + dt.Rows[i]["UOM"].ToString() + "','" + dt.Rows[i]["Qty"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (btnSave.Text == "Save")
            {
                TransactionNoGenerate TransNO_Up = new TransactionNoGenerate();
                string Auto_Transaction_No_Up = "";
                Auto_Transaction_No_Up = TransNO_Up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Sales Proforma Customer Invoice", SessionFinYearVal, "4");
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Proforma Invoice Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Proforma Invoice Details Updated Successfully');", true);
            }

            Clear_All_Field();

            Response.Redirect("Sales_Cust_Proforma_Inv_Main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtInvoice_NO.Text = ""; txtDate.Text = ""; txtCustomer_Name.SelectedIndex = 0;
        Load_Data_Empty_PO_No(); txtPO_No.SelectedIndex = 0; txtPO_Date.Text = "";
        txtPayment_Mode.SelectedIndex = 0; txtTransPort.SelectedIndex = 0; txtVehicleNo.Text = "";
        txtDelivery_From.Text = ""; txtDelivery_At.Text = ""; txtNote.Text = ""; txtRemarks.Text = "";

        
        txtSap_No.Text = ""; txtItem_Description.Text = ""; txtQty.Text = "0"; txtRate.Text = "0";

        txtGST_Type.SelectedIndex = 0;
        txtSubTotal.Text = "0"; txtDiscount_Per.Text = "0"; txtDiscount_Amount.Text = "0";
        txtAfter_Discount_Tot.Text = "0"; txtFreight_Charges.Text = "0";
        txtTaxable_Amt.Text = "0"; txtCGST_Per.Text = "0"; txtCGST_Amt.Text = "0";
        txtSGST_Per.Text = "0"; txtSGST_Amt.Text = "0"; txtIGST_Per.Text = "0";
        txtIGST_Amt.Text = "0"; txtGST_Total.Text = "0"; txtRound_Off.Text = "0";
        txtFinal_Net_Amt.Text = "0";
        txtGST_And_Gross_Amt.Text = "0";
        txtCurrency_Type.SelectedIndex = 0;


        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Current_Date_Display();
        Session.Remove("Sales_Proforma_Invoice_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

        if (txtQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Qty...');", true);
        }
        if (txtRate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Rate...');", true);
        }
        ////check with Item Code And Item Name 
        //SSQL = "Select * from GeneratorModels where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GenModelNo='" + txtGenerator_Model.SelectedValue + "'";
        //qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Generator Model Details..');", true);
        //}

        ////check with Item Code And Item Name 
        //SSQL = "Select * from Sales_Customer_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GenModelNo='" + txtGenerator_Model.SelectedValue + "'";
        //SSQL = SSQL + " And Cus_Pur_Order_No='" + txtPO_No.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
        //qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Generator Model Details..');", true);
        //}

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    if (dt.Rows[i]["GenModelNo"].ToString().ToUpper() == txtGenerator_Model.SelectedValue.ToString().ToUpper())
                //    {
                //        ErrFlag = true;
                //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Generator Already Added..');", true);
                //    }
                //}
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["GenModelNo"] = "";
                    dr["GenModelName"] = "";
                    dr["Sap_No"] = txtSap_No.Text;
                    dr["HSN_Code"] = txtHSN_Code.Text;
                    dr["Item_Desc"] = txtItem_Description.Text;
                    dr["UOM"] = txtUOM.Text;
                    dr["Qty"] = txtQty.Text;
                    dr["Rate"] = txtRate.Text;

                    string LineTotal_Str = "0";
                    LineTotal_Str = (Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtRate.Text)).ToString();
                    LineTotal_Str = (Math.Round(Convert.ToDecimal(LineTotal_Str), 2, MidpointRounding.AwayFromZero)).ToString();
                    dr["LineTotal"] = LineTotal_Str;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    TotalReqQty();
                    Final_Total_Amt_Calculate();

                    txtSap_No.Text = ""; txtItem_Description.Text = ""; txtQty.Text = "0"; txtRate.Text = "0";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["GenModelNo"] = "";
                dr["GenModelName"] = "";
                dr["Sap_No"] = txtSap_No.Text;
                dr["HSN_Code"] = txtHSN_Code.Text;
                dr["Item_Desc"] = txtItem_Description.Text;
                dr["UOM"] = txtUOM.Text;
                dr["Qty"] = txtQty.Text;
                dr["Rate"] = txtRate.Text;

                string LineTotal_Str = "0";
                LineTotal_Str = (Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtRate.Text)).ToString();
                LineTotal_Str = (Math.Round(Convert.ToDecimal(LineTotal_Str), 2, MidpointRounding.AwayFromZero)).ToString();
                dr["LineTotal"] = LineTotal_Str;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                TotalReqQty();
                Final_Total_Amt_Calculate();

                txtSap_No.Text = ""; txtItem_Description.Text = ""; txtQty.Text = "0"; txtRate.Text = "0";
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("GenModelNo", typeof(string)));
        dt.Columns.Add(new DataColumn("GenModelName", typeof(string)));
        dt.Columns.Add(new DataColumn("Sap_No", typeof(string)));
        dt.Columns.Add(new DataColumn("HSN_Code", typeof(string)));
        dt.Columns.Add(new DataColumn("Item_Desc", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM", typeof(string)));
        dt.Columns.Add(new DataColumn("Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));        
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }


    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if ((Convert.ToDecimal(i) + Convert.ToDecimal(1)) == Convert.ToDecimal(e.CommandName.ToString()))
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        TotalReqQty();
        Final_Total_Amt_Calculate();

    }
    public void TotalReqQty()
    {

        ReqQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ReqQty = Convert.ToDecimal(ReqQty) + Convert.ToDecimal(dt.Rows[i]["LineTotal"]);
        }
        txtSubTotal.Text = ReqQty.ToString();
        Final_Total_Amt_Calculate();
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + txtInvoice_NO.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Cus_Pur_Order_Date"].ToString();
            txtCustomer_Name.SelectedValue = Main_DT.Rows[0]["CustomerID"].ToString();
            txtParty_PO_No.Text = Main_DT.Rows[0]["Cus_Pur_Order_No"].ToString();
            txtParty_PO_Date.Text = Main_DT.Rows[0]["Cus_Pur_Order_Date"].ToString();
            //Load_Data_Empty_PO_No();
            //txtPO_No.SelectedValue = Main_DT.Rows[0]["Cus_Pur_Order_No"].ToString();
            //txtPO_Date.Text = Main_DT.Rows[0]["Cus_Pur_Order_Date"].ToString();
            txtPayment_Mode.SelectedValue = Main_DT.Rows[0]["PaymentMode"].ToString();
            //txtTransPort.SelectedValue = Main_DT.Rows[0]["TransPortCode"].ToString();
            //txtVehicleNo.Text = Main_DT.Rows[0]["VehicleNo"].ToString();
            //txtDelivery_From.Text = Main_DT.Rows[0]["DeliveryFrom"].ToString();
            //txtDelivery_At.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            //txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            //txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            txtSubTotal.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            txtDiscount_Per.Text = Main_DT.Rows[0]["DiscountPer"].ToString();
            txtDiscount_Amount.Text = Main_DT.Rows[0]["DiscountAmt"].ToString();
            txtAfter_Discount_Tot.Text = Main_DT.Rows[0]["After_Discount_Total"].ToString();
            txtFreight_Charges.Text = Main_DT.Rows[0]["Freight_Charges"].ToString();
            txtTaxable_Amt.Text = Main_DT.Rows[0]["Taxable_Amount"].ToString();
            txtGST_Type.SelectedValue = Main_DT.Rows[0]["GST_Type"].ToString();
            txtCGST_Per.Text = Main_DT.Rows[0]["CGST_Per"].ToString();
            txtCGST_Amt.Text = Main_DT.Rows[0]["CGST_Amt"].ToString();
            txtSGST_Per.Text = Main_DT.Rows[0]["SGST_Per"].ToString();
            txtSGST_Amt.Text = Main_DT.Rows[0]["SGST_Amt"].ToString();
            txtIGST_Per.Text = Main_DT.Rows[0]["IGST_Per"].ToString();
            txtIGST_Amt.Text = Main_DT.Rows[0]["IGST_Amt"].ToString();
            txtGST_Total.Text = Main_DT.Rows[0]["GST_Total_Amt"].ToString();
            txtGST_And_Gross_Amt.Text = Main_DT.Rows[0]["GST_And_Gross_Amt"].ToString();
            txtRound_Off.Text = Main_DT.Rows[0]["Round_Off_val"].ToString();
            txtFinal_Net_Amt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtCurrency_Type.SelectedValue = Main_DT.Rows[0]["Currency_Type"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();

            SSQL = "Select GenModelNo,GenModelName,Sap_No,HSN_Code,Item_Desc,UOM,Qty,Rate,LineTotal from Sales_Proforma_Invoice_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Sales_Inv_No='" + txtInvoice_NO.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            TotalReqQty();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("Sales_Cust_Proforma_Inv_Main.aspx");
    }
    protected void txtDiscount_Per_TextChanged(object sender, EventArgs e)
    {
        Final_Total_Amt_Calculate();
    }
    protected void txtFreight_Charges_TextChanged(object sender, EventArgs e)
    {
        Final_Total_Amt_Calculate();
    }
    protected void txtGST_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Get Rate
        string query = "";
        DataTable DT_Q = new DataTable();
        if (txtCurrency_Type.SelectedValue == "INR")
        {
            query = "Select * from GstMaster where GST_Type='" + txtGST_Type.SelectedValue + "'";
            query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            DT_Q = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Q.Rows.Count != 0)
            {
                txtCGST_Per.Text = DT_Q.Rows[0]["CGST_Per"].ToString();
                txtSGST_Per.Text = DT_Q.Rows[0]["SGST_Per"].ToString();
                txtIGST_Per.Text = DT_Q.Rows[0]["IGST_Per"].ToString();
            }
            else
            {
                txtCGST_Per.Text = "0";
                txtSGST_Per.Text = "0";
                txtIGST_Per.Text = "0";
            }
        }
        else
        {
            txtCGST_Per.Text = "0";
            txtSGST_Per.Text = "0";
            txtIGST_Per.Text = "0";
        }
        Final_Total_Amt_Calculate();
    }

    protected void txtCurrency_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Get Rate
        if (txtCurrency_Type.SelectedValue != "INR")
        {
            txtGST_Type.SelectedIndex = 0;
        }
        txtGST_Type_SelectedIndexChanged(sender, e);
    }

    private void Final_Total_Amt_Calculate()
    {
        string Sub_Total_Amt = "0";
        string Discount_Per = "0";
        string Discout_Amt = "0";
        string Taxable_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string GST_Total_Amt = "0";
        string Round_Off_Val = "0";
        string Final_Net_Amt = "0";
        string Freight_Charges = "0";
        string After_Discount_Sub_Total = "0";
        string GST_And_Gross_Amt = "0";

        if (txtSubTotal.Text != "") { Sub_Total_Amt = txtSubTotal.Text; }
        if (txtDiscount_Per.Text != "") { Discount_Per = txtDiscount_Per.Text; }
        if (txtCGST_Per.Text != "") { CGST_Per = txtCGST_Per.Text; }
        if (txtSGST_Per.Text != "") { SGST_Per = txtSGST_Per.Text; }
        if (txtIGST_Per.Text != "") { IGST_Per = txtIGST_Per.Text; }
        if (txtFreight_Charges.Text != "") { Freight_Charges = txtFreight_Charges.Text; }

        Discout_Amt = ((Convert.ToDecimal(Discount_Per) * Convert.ToDecimal(Sub_Total_Amt)) / Convert.ToDecimal(100)).ToString();
        Discout_Amt = (Math.Round(Convert.ToDecimal(Discout_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        After_Discount_Sub_Total = (Convert.ToDecimal(Sub_Total_Amt) - Convert.ToDecimal(Discout_Amt)).ToString();
        After_Discount_Sub_Total = (Math.Round(Convert.ToDecimal(After_Discount_Sub_Total), 2, MidpointRounding.AwayFromZero)).ToString();

        Taxable_Amt = (Convert.ToDecimal(After_Discount_Sub_Total) + Convert.ToDecimal(Freight_Charges)).ToString();
        Taxable_Amt = (Math.Round(Convert.ToDecimal(Taxable_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        CGST_Amt = ((Convert.ToDecimal(Taxable_Amt) * Convert.ToDecimal(CGST_Per)) / Convert.ToDecimal(100)).ToString();
        CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        SGST_Amt = ((Convert.ToDecimal(Taxable_Amt) * Convert.ToDecimal(SGST_Per)) / Convert.ToDecimal(100)).ToString();
        SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        IGST_Amt = ((Convert.ToDecimal(Taxable_Amt) * Convert.ToDecimal(IGST_Per)) / Convert.ToDecimal(100)).ToString();
        IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        GST_Total_Amt = (Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
        GST_Total_Amt = (Math.Round(Convert.ToDecimal(GST_Total_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        Final_Net_Amt = (Convert.ToDecimal(Taxable_Amt) + Convert.ToDecimal(GST_Total_Amt)).ToString();
        Final_Net_Amt = (Math.Round(Convert.ToDecimal(Final_Net_Amt), 0, MidpointRounding.AwayFromZero)).ToString();

        GST_And_Gross_Amt = (Convert.ToDecimal(Taxable_Amt) + Convert.ToDecimal(GST_Total_Amt)).ToString();
        GST_And_Gross_Amt = (Math.Round(Convert.ToDecimal(GST_And_Gross_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        Round_Off_Val = (Convert.ToDecimal(Final_Net_Amt) - (Convert.ToDecimal(Taxable_Amt) + Convert.ToDecimal(GST_Total_Amt))).ToString();
        Round_Off_Val = (Math.Round(Convert.ToDecimal(Round_Off_Val), 2, MidpointRounding.AwayFromZero)).ToString();

        txtFreight_Charges.Text = Freight_Charges;
        txtDiscount_Per.Text = Discount_Per;
        txtDiscount_Amount.Text = Discout_Amt;
        txtAfter_Discount_Tot.Text = After_Discount_Sub_Total;
        txtTaxable_Amt.Text = Taxable_Amt;
        txtCGST_Amt.Text = CGST_Amt;
        txtSGST_Amt.Text = SGST_Amt;
        txtIGST_Amt.Text = IGST_Amt;
        txtGST_Total.Text = GST_Total_Amt;
        txtRound_Off.Text = Round_Off_Val;
        txtFinal_Net_Amt.Text = Final_Net_Amt;

        txtGST_And_Gross_Amt.Text = GST_And_Gross_Amt;
    }
}
