﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_BOM_Revision : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionDesigCode = "";
    static Decimal Amnt;
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    bool Rights_Check = false;
    string SessionPurRequestNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYearCode = Session["FinYearCode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();

            if (!IsPostBack)
            {
                Page.Title = "CORAL ERP :: Material Planning Details";

                Initial_Data_Referesh();
                Load_Data_Empty_Order_No();
                Load_Data_Empty_BOM_Revision_Name();
                Load_Data_Empty_Parts_Type();
                Load_Data_Empty_Sap_No();
                Load_Data_Empty_BOM_ID();
                RdbRevision_Type_SelectedIndexChanged(sender, e);

                if (Session["BOM_Revision_PO_NO"] == null)
                {
                    SessionPurRequestNo = "";

                }
                else
                {
                    SessionPurRequestNo = Session["BOM_Revision_PO_NO"].ToString();
                    Load_Data_Empty_Order_No();
                    txtOrder_No.SelectedValue = SessionPurRequestNo;
                    btnSearch_Click(sender, e);
                }

            }
            Load_OLD_data();
            //ddlModel.SelectedItem.Text = "-Select-";
            //txtPartsName.Text = "";


        }
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("PartType", typeof(string)));
        dt.Columns.Add(new DataColumn("Production_Steps", typeof(string)));
        dt.Columns.Add(new DataColumn("Sap_No", typeof(string)));        
        dt.Columns.Add(new DataColumn("Raw_Mat_Name", typeof(string)));
        dt.Columns.Add(new DataColumn("UOM_Full", typeof(string)));
        dt.Columns.Add(new DataColumn("RequiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Mat_No", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_Data_Empty_Order_No()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtOrder_No.Items.Clear();
        SSQL = "Select Cus_Pur_Order_No from [CORAL_ERP_Sales]..Sales_Customer_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And PO_Status='1'";
        SSQL = SSQL + " And Cus_Pur_Order_No Not IN (Select Distinct Order_No from BOM_Master_Revision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Order_No <> '" + SessionPurRequestNo + "')";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtOrder_No.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Cus_Pur_Order_No"] = "-Select-";
        dr["Cus_Pur_Order_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtOrder_No.DataTextField = "Cus_Pur_Order_No";
        txtOrder_No.DataValueField = "Cus_Pur_Order_No";
        txtOrder_No.DataBind();

    }

    private void Load_Data_Empty_Parts_Type()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        ddlPartName.Items.Clear();
        SSQL = "Select id,partsname from [Coral_ERP_Sales]..PartsType where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status!='Delete'";
        SSQL = SSQL + " And GenModelName='" + txtGenerator_Model.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlPartName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["partsname"] = "-Select-";
        dr["partsname"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlPartName.DataTextField = "partsname";
        ddlPartName.DataValueField = "partsname";
        ddlPartName.DataBind();

    }

    
    private void Load_Data_Empty_Sap_No()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtSapNo.Items.Clear();
        SSQL = "Select Distinct Sap_No from BoMMaster Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.Text + "' And Status='Add' And PartType='" + ddlPartName.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtSapNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Sap_No"] = "-Select-";
        dr["Sap_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSapNo.DataTextField = "Sap_No";
        txtSapNo.DataValueField = "Sap_No";
        txtSapNo.DataBind();

    }

    protected void ddlPartName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_Sap_No();
    }
    protected void txtSapNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_BOM_ID();
    }
    protected void txtBOM_ID_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select * from BoMMaster Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.Text + "' And Status='Add' And PartType='" + ddlPartName.SelectedValue + "'";
        SSQL = SSQL + " And Sap_No='" + txtSapNo.SelectedValue + "' And Mat_No='" + txtBOM_ID.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtItem_Name.Text = Main_DT.Rows[0]["Raw_Mat_Name"].ToString();
            txtUOM_Hide.Value = Main_DT.Rows[0]["UOM_Full"].ToString();
            txtRequired_Qty.Text = Main_DT.Rows[0]["RequiredQty"].ToString();
        }
    }

    private void Load_Data_Empty_BOM_ID()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtBOM_ID.Items.Clear();
        SSQL = "Select Distinct Mat_No from BoMMaster Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.Text + "' And Status='Add' And PartType='" + ddlPartName.SelectedValue + "'";
        SSQL = SSQL + " And Sap_No='" + txtSapNo.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtBOM_ID.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Mat_No"] = "-Select-";
        dr["Mat_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtBOM_ID.DataTextField = "Mat_No";
        txtBOM_ID.DataValueField = "Mat_No";
        txtBOM_ID.DataBind();

    }

    private void Load_Data_Empty_BOM_Revision_Name()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtBom_Select.Items.Clear();
        SSQL = "Select Distinct Revision_Name from BOM_Master_Revision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And Revision_Name <> 'BOM Master'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtBom_Select.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Revision_Name"] = "BOM Master";
        dr["Revision_Name"] = "BOM Master";
        Main_DT.Rows.InsertAt(dr, 0);
        txtBom_Select.DataTextField = "Revision_Name";
        txtBom_Select.DataValueField = "Revision_Name";
        txtBom_Select.DataBind();

    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

        if (ddlPartName.SelectedIndex == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Part Name...');", true);
        }
        if (txtSapNo.SelectedIndex == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Sap No...');", true);
        }
        if (txtBOM_ID.SelectedIndex == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Bom ID...');", true);
        }


        if (!ErrFlag)
        {
            //Get Item Information
            DataTable DT_ITM = new DataTable();            
            string Production_Steps = "";
            SSQL = "Select * from BoMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.Text + "' And Status='Add' And PartType='" + ddlPartName.SelectedValue + "'";
            SSQL = SSQL + " And Sap_No='" + txtSapNo.SelectedValue + "' And Mat_No='" + txtBOM_ID.SelectedValue + "'";
            DT_ITM = objdata.RptEmployeeMultipleDetails(SSQL);
            {                
                Production_Steps = DT_ITM.Rows[0]["Production_Steps"].ToString();
            }
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Mat_No"].ToString().ToUpper() == txtBOM_ID.SelectedValue.ToString().ToUpper())
                    {
                        dt.Rows[i]["RequiredQty"] = txtRequired_Qty.Text;
                        ErrFlag = true;
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already BOM Added...');", true);

                    }

                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();

                    dr["PartType"] = ddlPartName.SelectedItem.Text;
                    dr["Production_Steps"] = Production_Steps;
                    dr["Sap_No"] = txtSapNo.SelectedValue;
                    dr["Raw_Mat_Name"] = txtItem_Name.Text;
                    dr["RequiredQty"] = txtRequired_Qty.Text;
                    dr["Mat_No"] = txtBOM_ID.SelectedValue;
                    dr["UOM_Full"] = txtUOM_Hide.Value;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    ddlPartName.SelectedIndex = 0; txtSapNo.SelectedIndex = 0; txtBOM_ID.SelectedIndex = 0; txtItem_Name.Text = ""; txtUOM_Hide.Value = "";

                }
                else
                {
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    ddlPartName.SelectedIndex = 0; txtSapNo.SelectedIndex = 0; txtBOM_ID.SelectedIndex = 0; txtItem_Name.Text = ""; txtUOM_Hide.Value = "";
                    txtRequired_Qty.Text = "0";
                }
            }
            else
            {

                dr = dt.NewRow();
                dr["PartType"] = ddlPartName.SelectedItem.Text;
                dr["Production_Steps"] = Production_Steps;
                dr["Sap_No"] = txtSapNo.SelectedValue;
                dr["Raw_Mat_Name"] = txtItem_Name.Text;
                dr["RequiredQty"] = txtRequired_Qty.Text;
                dr["Mat_No"] = txtBOM_ID.SelectedValue;
                dr["UOM_Full"] = txtUOM_Hide.Value;


                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                ddlPartName.SelectedIndex = 0; txtSapNo.SelectedIndex = 0; txtBOM_ID.SelectedIndex = 0; txtItem_Name.Text = ""; txtUOM_Hide.Value = "";
                txtRequired_Qty.Text = "0";
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ChkStatus = "";
        string query = "";
        DataTable dt = new DataTable();



        dt = (DataTable)ViewState["ItemTable"];
        if (dt.Rows.Count == 0)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one BOM Details..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (txtOrder_No.SelectedIndex == 0)
        {            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Order No..');", true);
            ErrFlg = true;
        }

        if (RdbRevision_Type.SelectedValue == "1")
        {
            if (txtRevision_Name.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Revision Name..');", true);
                ErrFlg = true;
            }
        }

        DataTable DT = new DataTable();

        if (!ErrFlg)
        {
            string Save_Mode = "Insert";
            SSQL = "Select * from BOM_Master_Revision Where Order_No ='" + txtOrder_No.SelectedValue + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count != 0)
            {
                SSQL = "Delete from BOM_Master_Revision Where Order_No ='" + txtOrder_No.SelectedValue + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                Save_Mode = "Update";
            }

            SSQL = "Delete from [Coral_ERP_Sales]..Sales_Material_Plan_Conform_Main Where Cus_Pur_Order_No ='" + txtOrder_No.SelectedValue + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete from [Coral_ERP_Sales]..Sales_Material_Plan_Conform_Main_Sub Where Cus_Pur_Order_No ='" + txtOrder_No.SelectedValue + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);


            //Insert Sales_Material_Plan_Conform_Main
            SSQL = "";
            SSQL = "Insert Into [Coral_ERP_Sales]..Sales_Material_Plan_Conform_Main(Ccode,Lcode,FinYearCode,FinYearVal,Cus_Pur_Order_No,GeneratorModel,Delivery_Date,Delivery_CW,Status,Approvedby,UserID,UserName) Values('" + SessionCcode + "',";
            SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtOrder_No.SelectedValue + "','" + txtGenerator_Model.Text + "','" + txtDelivery_Date.Text + "','" + txtDelivery_CW.Text + "',";
            SSQL = SSQL + " '0','','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);


            //Insert Bom Revision Table
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "";
                SSQL = "Insert into BOM_Master_Revision(Ccode,Lcode,Order_No,GenModelNo,Revision_Type,BOM_Selection,Revision_Name,PartType,Production_Steps,Sap_No,Raw_Mat_Name,UOM_Full,RequiredQty,Mat_No,Delivery_Date,Delivery_CW,Status,Approvedby)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "' ,'" + txtOrder_No.SelectedValue + "','" + txtGenerator_Model.Text + "','" + RdbRevision_Type.SelectedValue + "','" + txtBom_Select.SelectedValue + "',";
                SSQL = SSQL + " '" + txtRevision_Name.Text + "','" + dt.Rows[i]["PartType"].ToString() + "','" + dt.Rows[i]["Production_Steps"].ToString() + "','" + dt.Rows[i]["Sap_No"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["Raw_Mat_Name"].ToString() + "','" + dt.Rows[i]["UOM_Full"].ToString() + "','" + dt.Rows[i]["RequiredQty"].ToString() + "','" + dt.Rows[i]["Mat_No"].ToString() + "','" + txtDelivery_Date.Text + "','" + txtDelivery_CW.Text + "','0','')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Insert Into [Coral_ERP_Sales]..Sales_Material_Plan_Conform_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Cus_Pur_Order_No,GeneratorModel,Delivery_Date,Delivery_CW,PartType,Production_Steps,Sap_No,Raw_Mat_Name,RequiredQty,Mat_No,UOM_Full,UserID,UserName) Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtOrder_No.SelectedValue + "','" + txtGenerator_Model.Text + "','" + txtDelivery_Date.Text + "','" + txtDelivery_CW.Text + "','" + dt.Rows[i]["PartType"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["Production_Steps"].ToString() + "','" + dt.Rows[i]["Sap_No"].ToString() + "','" + dt.Rows[i]["Raw_Mat_Name"].ToString() + "','" + dt.Rows[i]["RequiredQty"].ToString() + "','" + dt.Rows[i]["Mat_No"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["UOM_Full"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            Response.Redirect("BOM_Revision_Main.aspx");



        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtOrder_No.Enabled = true;
        txtOrder_No.SelectedIndex = 0; txtGenerator_Model.Text = "";
        RdbRevision_Type.SelectedValue = "1";
        txtBom_Select.SelectedIndex = 0;
        txtRevision_Name.Text = "";
        txtDelivery_Date.Text = "";
        txtDelivery_CW.Text = "";

        Initial_Data_Referesh();
        Load_Data_Empty_Order_No();
        Load_Data_Empty_BOM_Revision_Name();
        Load_Data_Empty_Parts_Type();
        Load_Data_Empty_Sap_No();
        Load_Data_Empty_BOM_ID();

        txtItem_Name.Text = ""; txtRequired_Qty.Text = "0";

        btnSave.Text = "Save";
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        DT = (DataTable)ViewState["ItemTable"];
        ddlPartName.SelectedIndex = 0;
        Load_Data_Empty_Sap_No();
        txtSapNo.SelectedIndex = 0;
        Load_Data_Empty_BOM_ID();
        txtBOM_ID.SelectedIndex = 0;
        txtItem_Name.Text = "";
        txtUOM_Hide.Value = "";
        txtRequired_Qty.Text = "0";

        //check Item Already add or not
        for (int i = 0; i < DT.Rows.Count; i++)
        {
            if (DT.Rows[i]["Mat_No"].ToString().ToUpper() == e.CommandName.ToString().ToUpper())
            {
                ddlPartName.SelectedValue = DT.Rows[0]["PartType"].ToString();
                Load_Data_Empty_Sap_No();
                txtSapNo.SelectedValue = DT.Rows[0]["Sap_No"].ToString();
                Load_Data_Empty_BOM_ID();
                txtBOM_ID.SelectedValue = DT.Rows[0]["Mat_No"].ToString();
                txtItem_Name.Text = DT.Rows[0]["Raw_Mat_Name"].ToString();
                txtUOM_Hide.Value = DT.Rows[0]["UOM_Full"].ToString();
                txtRequired_Qty.Text = DT.Rows[0]["RequiredQty"].ToString();
            }

        }

    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["Mat_No"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        

    }

    protected void txtBom_Select_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdbRevision_Type.SelectedValue=="2")
        {
            txtRevision_Name.Text = txtBom_Select.SelectedValue;
        }
        Load_BOM_Item_Details();
    }

    protected void txtOrder_No_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [CORAL_ERP_Sales]..Sales_Customer_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And Cus_Pur_Order_No='" + txtOrder_No.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count !=0)
        {
            txtGenerator_Model.Text = DT.Rows[0]["GeneratorModel"].ToString();
            txtDelivery_Date.Text = DT.Rows[0]["Delivery_Date"].ToString();
            txtDelivery_CW.Text = DT.Rows[0]["Delivery_CW"].ToString();
        }
        Load_Data_Empty_Parts_Type();
        Load_BOM_Item_Details();

    }

    private void Load_BOM_Item_Details()
    {
        string query = "";
        DataTable DT = new DataTable();

        if (RdbRevision_Type.SelectedValue == "2")
        {
            if (txtBom_Select.SelectedIndex == 0)
            {
                query = "Select PartType,ProductionPartName[Production_Steps],Sap_No,Raw_Mat_Name,UOM_Full,RequiredQty,Mat_No from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                query = query + " And GenModelName = '" + txtGenerator_Model.Text + "' And Status='Add' And PartType <> '' And PartType Is Not Null";
            }
            else
            {
                query = "Select PartType,Production_Steps,Sap_No,Raw_Mat_Name,UOM_Full,RequiredQty,Mat_No from BOM_Master_Revision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                query = query + " And Revision_Name='" + txtBom_Select.SelectedValue + "' And GenModelNo='" + txtGenerator_Model.Text + "'";
            }
        }
        else
        {
            if (txtBom_Select.SelectedIndex == 0)
            {
                query = "Select PartType,ProductionPartName[Production_Steps],Sap_No,Raw_Mat_Name,UOM_Full,RequiredQty,Mat_No from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                query = query + " And GenModelName = '" + txtGenerator_Model.Text + "' And Status='Add' And PartType <> '' And PartType Is Not Null";
            }
            else
            {
                query = "Select PartType,Production_Steps,Sap_No,Raw_Mat_Name,UOM_Full,RequiredQty,Mat_No from BOM_Master_Revision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                query = query + " And Revision_Name='" + txtBom_Select.SelectedValue + "' And GenModelNo='" + txtGenerator_Model.Text + "'";
            }
            //query = "Select PartType,ProductionPartName[Production_Steps],Sap_No,Raw_Mat_Name,UOM_Full,RequiredQty,Mat_No from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            //query = query + " And GenModelName = '" + txtGenerator_Model.Text + "' And Status='Add' And PartType <> '' And PartType Is Not Null";
        }
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
    }


    protected void RdbRevision_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdbRevision_Type.SelectedValue == "1")
        {
            txtBom_Select.SelectedIndex = 0;
            txtBom_Select.Enabled = true;
            txtRevision_Name.Text = "";
        }
        else
        {
            txtBom_Select.Enabled = true;
            txtRevision_Name.Text = txtBom_Select.SelectedValue;
        }

        Load_BOM_Item_Details();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select * from BOM_Master_Revision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Order_No='" + txtOrder_No.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtGenerator_Model.Text = Main_DT.Rows[0]["GenModelNo"].ToString();
            txtDelivery_Date.Text = Main_DT.Rows[0]["Delivery_Date"].ToString();
            txtDelivery_CW.Text = Main_DT.Rows[0]["Delivery_CW"].ToString();
            RdbRevision_Type.SelectedValue= Main_DT.Rows[0]["Revision_Type"].ToString();
            txtBom_Select.SelectedValue = Main_DT.Rows[0]["BOM_Selection"].ToString();
            txtRevision_Name.Text = Main_DT.Rows[0]["Revision_Name"].ToString();
            Load_Data_Empty_Parts_Type();


            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();

            SSQL = "Select PartType,Production_Steps,Sap_No,Raw_Mat_Name,RequiredQty,Mat_No,UOM_Full from BOM_Master_Revision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Order_No='" + txtOrder_No.SelectedValue + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
            txtOrder_No.Enabled = false;
        }
        else
        {
            btnClear_Click(sender,e);
            txtOrder_No.Enabled = true;
        }

    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("BOM_Revision_Main.aspx");
    }
}