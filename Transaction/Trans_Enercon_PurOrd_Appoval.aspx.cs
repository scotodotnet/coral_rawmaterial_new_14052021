﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_Enercon_PurOrd_Appoval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Enercon Purchase Order Approval";
            //Department_Code_Add();
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();
        }
        Load_Data_Empty_Grid();
    }

    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Enercon PO Approval");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You do not have Rights to Approved Purchase Request..');", true);
        }
        //User Rights Check End


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And PO_Status = '2'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Already Cancelled.. Cannot Approved it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And PO_Status = '1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Already get Approved..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And PO_Status = '1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table

                SSQL = "Update Trans_Enercon_PurOrd_Main set PO_Status='1' where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Std_PO_No ='" + e.CommandArgument.ToString() + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Approved Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order not is Avail give input correctly..');", true);
            }
        }
    }



    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Enercon PO Approval");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You do not have Rights to Approved Purchase Request..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' And ";
            SSQL = SSQL + " PO_Status = '1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Already Approved.. Cannot Cancelled it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And PO_Status = '2'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Already get Cancelled..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            SSQL = "Select * from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And PO_Status = '2'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table

                SSQL = "Update Trans_Enercon_PurOrd_Main set PO_Status='2' where Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Std_PO_No ='" + e.CommandArgument.ToString() + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Cancelled Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order not is Avail give input correctly..');", true);
            }
        }
    }



    private void Load_Data_Empty_Grid()
    {

        string SSQL = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();


        DT.Columns.Add("Std_PO_No");
        DT.Columns.Add("Std_PO_Date");
        DT.Columns.Add("Supp_Name");
        DT.Columns.Add("Pur_Request_No");

        if (txtRequestStatus.Text == "Pending List")
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No From Trans_Enercon_PurOrd_Main  ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) And Active!='Delete' Order By Std_PO_No desc";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            Repeater_Pending.DataSource = DT;
            Repeater_Pending.DataBind();
            Repeater_Pending.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No,";
            SSQL = SSQL + " Case when ReceiveStatus='1' then 'Goods Received' else 'Goods Not Received' End Status ";
            SSQL = SSQL + " from Trans_Enercon_PurOrd_Main ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And PO_Status = '1' And Active!='Delete' Order By Std_PO_No desc";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);


            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            Repeater_Approve.Visible = true;
            Repeater_Pending.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Trans_Enercon_PurOrd_Main ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And PO_Status = '2' And Active!='Delete' Order By Std_PO_No desc";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);


            Repeater_Rejected.DataSource = DT;
            Repeater_Rejected.DataBind();
            Repeater_Rejected.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Pending.Visible = false;
        }
    }

    //private void Department_Code_Add()
    //{
    //    string SSQL = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDepartment.Items.Clear();
    //    SSQL = "Select (DeptName + '-' + DeptCode) as DpetName_Join from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptName Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //    txtDepartment.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDepartment.Items.Add(Main_DT.Rows[i]["DpetName_Join"].ToString());
    //    }
    //}

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }

    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        //string[] Department_Spilit = txtDepartment.Text.Split('-');
        //string Department_Code = "";
        //string Department_Name = "";
        string SSQL = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        DT.Columns.Add("Std_PO_No");
        DT.Columns.Add("Std_PO_Date");
        DT.Columns.Add("Supp_Name");
        DT.Columns.Add("Pur_Request_No");



        if (txtRequestStatus.Text == "Pending List")
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Trans_Enercon_PurOrd_Main ";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) And Active!='Delete' Order By Std_PO_No Asc";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            Repeater_Pending.DataSource = DT;
            Repeater_Pending.DataBind();
            Repeater_Pending.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No, ";
            SSQL = SSQL + " Case when ReceiveStatus='1' then 'Goods Received' else 'Goods Not Received' End Status ";
            SSQL = SSQL + " From Trans_Enercon_PurOrd_Main ";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And PO_Status = '1' And Active!='Delete' Order By Std_PO_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);


            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            Repeater_Approve.Visible = true;
            Repeater_Pending.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Status = '2' Order By Std_PO_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);


            Repeater_Rejected.DataSource = DT;
            Repeater_Rejected.DataBind();
            Repeater_Rejected.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Pending.Visible = false;
        }
    }

    //protected void txtDepartment_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (txtDepartment.Text != "-Select-")
    //    {
    //        txtRequestStatus_SelectedIndexChanged(sender, e);
    //    }
    //}

    //private void Department_Code_Add_Test()
    //{
    //    string SSQL = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDeptCode_test.Items.Clear();
    //    SSQL = "Select DeptCode from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptCode Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //    txtDeptCode_test.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDeptCode_test.Items.Add(Main_DT.Rows[i]["DeptCode"].ToString());
    //    }
    //}


    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {
        string Enquiry_No_Str = e.CommandName.ToString();
        Session.Remove("Pur_RequestNo_Approval");
        Session.Remove("Transaction_No");
        Session.Remove("Std_PO_No");
        Session["Std_PO_No"] = Enquiry_No_Str;
        Response.Redirect("standard_po.aspx");
    }

    protected void Repeater_Approve_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblStatus = Item.FindControl("lblStatus") as Label;

            if (lblStatus.Text == "Goods Received")
            {
                lblStatus.BackColor = Color.Green;
                lblStatus.ForeColor = Color.White;

            }
            else
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.ForeColor = Color.White;
            }
        }
    }
}