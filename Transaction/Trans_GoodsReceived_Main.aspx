﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GoodsReceived_Main.aspx.cs" Inherits="Trans_GoodsReceived_Main" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">

        <asp:UpdatePanel ID="upForm" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />
                        </div>
                        <!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Goods Received List</span></h3>

                                </div>

                                <div class="box-body">
                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
                                            <div class="col-md-12">

                                                <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false" OnItemDataBound="Repeater2_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>GRN_No</th>
                                                                    <th>GRN_Date</th>
                                                                    <th>Order_No</th>
                                                                    <th>Party_Invoice_No</th>
                                                                    <th>Party_Invoice_Date</th>
                                                                    <th>Party_Name</th>
                                                                    <th>Party_Type</th>
                                                                    <th>Material</th>
                                                                    <th>Approval/Payment_Status</th>
                                                                    <th>Edit/Print/Delete</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Eval("GRNo")%></td>
                                                            <td><%# Eval("GRDate")%></td>
                                                            <td><%# Eval("PurOrdNo")%></td>
                                                            <td><%# Eval("PartyInvNo")%></td>
                                                            <td><%# Eval("PartyInvDate")%></td>
                                                            <td><%# Eval("SuppName")%></td>
                                                            <td><%# Eval("SupplierType")%></td>
                                                            <td><%# Eval("MaterialType")%></td>
                                                            <td>
                                                                <asp:Label ID="lblStatus" runat="server" class="form-control" Text='<%# Eval("Status") %>'></asp:Label>
                                                            </td>

                                                            <td>
                                                                <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("GRNo")%>'>
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="btnPrint" class="btn btn-primary btn-sm fa fa-print" runat="server"
                                                                    Text="" OnCommand="GridPrintClick" CommandArgument="Print" CommandName='<%# Eval("GRNo")+","+Eval("MatType")%>'>
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("GRNo")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Goods Received details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                }
            });
        };
    </script>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
</asp:Content>

