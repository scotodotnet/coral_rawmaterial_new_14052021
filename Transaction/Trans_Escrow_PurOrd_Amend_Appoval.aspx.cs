﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Escrow_Amend_approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Escrow Amendmend Approval";
            divCanReason.Visible = false;
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();
        }
        Load_Data_Empty_Grid();
    }


    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            //User Rights Check Start
            bool ErrFlag = false;
            bool Rights_Check = false;

            Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow PO Amend Approval");

            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approval...');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approved Purchase Request..');", true);
            }
            //User Rights Check End


            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And AmendPoNo='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And PO_Status = '2'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Amend Details Already Cancelled.. Cannot Approved it..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Cancelled.. Cannot Approved it..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }
            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And AmendPoNo='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And PO_Status = '1'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Amend Details Already get Approved..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already get Approved..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }

            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And AmendPoNo='" + e.CommandArgument.ToString() + "' And PO_Status = '1'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count == 0)
                {
                    //Delete Main Table
                    SSQL = "Update Trans_Escrow_PurOrd_Amend_Main set PO_Status='1',Approvedby='" + SessionUserName + "',ApprovedDate=Convert(Datetime,GetDate(),103) ";
                    SSQL = SSQL + " Where Ccode ='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                    SSQL = SSQL + " Lcode ='" + SessionLcode + "' And AmendPoNo='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Amend Details Approved Successfully..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Approved Successfully');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Amend not is Avail give input correctly..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request not is Avail give input correctly..');", true);
                }
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }



    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            //User Rights Check Start
            bool ErrFlag = false;
            bool Rights_Check = false;


            divCanReason.Visible = true;

            Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow PO Amend Approval");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Order Amend..');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approved Purchase Request..');", true);
            }
            //User Rights Check End

            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And AmendPoNo='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And PO_Status = '1'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Amend Details Already Approved.. Cannot Cancelled it..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Approved.. Cannot Cancelled it..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }
            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And AmendPoNo='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And PO_Status = '2'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Amend Details Already get Cancelled..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already get Cancelled..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }


            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And AmendPoNo='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And PO_Status = '2'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (txtCanReason.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Reason For Cancellation..');", true);
                    txtCanReason.Focus();
                }

                if (!ErrFlag)
                {
                    if (DT.Rows.Count == 0)
                    {
                        //Delete Main Table
                        SSQL = "Update Trans_Escrow_PurOrd_Amend_Main set PO_Status='-1',Canceledby='" + SessionUserName + "',CanceledDate=Convert(Datetime,GetDate(),103), ";
                        SSQL = SSQL + " CanceledReason='" + txtCanReason.Text + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                        SSQL = SSQL + " Lcode ='" + SessionLcode + "' And AmendPoNo='" + e.CommandArgument.ToString() + "'";

                        objdata.RptEmployeeMultipleDetails(SSQL);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Amend Details Cancelled Successfully..');", true);
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Cancelled Successfully');", true);
                        txtRequestStatus_SelectedIndexChanged(sender, e);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Amend not is Avail give input correctly..');", true);
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request not is Avail give input correctly..');", true);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }



    private void Load_Data_Empty_Grid()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            DataTable DT2 = new DataTable();


            DT.Columns.Add("AmendPoNo");
            DT.Columns.Add("Pur_Request_Date");
            DT.Columns.Add("TotalReqQty");
            DT.Columns.Add("Approvedby");

            if (txtRequestStatus.Text == "Pending List")
            {
                SSQL = "Select AmendPoNo,AmendPoDate,PurOrdNo,Supp_Name[SuppName], ";
                SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                SSQL = SSQL + " when MatType='2' then 'Tools' ";
                SSQL = SSQL + " when MatType='3' then 'Asset' End [MaterialName],MatType ";
                SSQL = SSQL + " from Trans_Escrow_PurOrd_Amend_Main where ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) Order By AmendPoNo Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Pending.DataSource = DT;
                Repeater_Pending.DataBind();
                Repeater_Pending.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Approved List")
            {
                SSQL = "Select AmendPoNo,AmendPoDate,PurOrdNo,Supp_Name[SuppName],";
                SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                SSQL = SSQL + " when MatType='2' then 'Tools' ";
                SSQL = SSQL + " when MatType='3' then 'Asset' End [MaterialName],MatType ";
                SSQL = SSQL + " from Trans_Escrow_PurOrd_Amend_Main where ";
                SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And PO_Status = '1' Order By AmendPoNo Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Approve.DataSource = DT;
                Repeater_Approve.DataBind();
                Repeater_Approve.Visible = true;
                Repeater_Pending.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Rejected List")
            {
                SSQL = "Select AmendPoNo,AmendPoDate,PurOrdNo,Supp_Name, ";

                SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                SSQL = SSQL + " when MatType='2' then 'Tools' ";
                SSQL = SSQL + " when MatType='3' then 'Asset' End [MaterialName],MatType,CanceledReason ";

                SSQL = SSQL + " from Trans_Escrow_PurOrd_Amend_Main where ";
                SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And PO_Status = '-1' Order By AmendPoNo Asc";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Rejected.DataSource = DT;
                Repeater_Rejected.DataBind();
                Repeater_Rejected.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Pending.Visible = false;
            }
        }
        catch(Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    //private void Department_Code_Add()
    //{
    //    string SSQL = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDepartment.Items.Clear();
    //    SSQL = "Select (DeptName + '-' + DeptCode) as DpetName_Join from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptName Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //    txtDepartment.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDepartment.Items.Add(Main_DT.Rows[i]["DpetName_Join"].ToString());
    //    }
    //}

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }

    protected void  txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data_Empty_Grid();

        ////string[] Department_Spilit = txtDepartment.Text.Split('-');
        ////string Department_Code = "";
        ////string Department_Name = "";
        //string SSQL = "";
        //DataTable DT = new DataTable();
        //DataTable DT1 = new DataTable();
        //DataTable DT2 = new DataTable();

        //DT.Columns.Add("AmendPoNo");
        //DT.Columns.Add("Pur_Request_Date");
        //DT.Columns.Add("TotalReqQty");
        //DT.Columns.Add("Approvedby");



        //if (txtRequestStatus.Text == "Pending List")
        //{
        //    SSQL = "Select AmendPoNo,Pur_Request_Date,TotalReqQty,Approvedby from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null) Order By AmendPoNo Asc";
        //    DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //    Repeater_Pending.DataSource = DT;
        //    Repeater_Pending.DataBind();
        //    Repeater_Pending.Visible = true;
        //    Repeater_Approve.Visible = false;
        //    Repeater_Rejected.Visible = false;
        //}
        //else if (txtRequestStatus.Text == "Approved List")
        //{
        //    SSQL = "Select AmendPoNo,Pur_Request_Date,TotalReqQty,Approvedby from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' Order By AmendPoNo Asc";
        //    DT = objdata.RptEmployeeMultipleDetails(SSQL);


        //    Repeater_Approve.DataSource = DT;
        //    Repeater_Approve.DataBind();
        //    Repeater_Approve.Visible = true;
        //    Repeater_Pending.Visible = false;
        //    Repeater_Rejected.Visible = false;
        //}
        //else if (txtRequestStatus.Text == "Rejected List")
        //{
        //    SSQL = "Select AmendPoNo,Pur_Request_Date,TotalReqQty,Approvedby from Trans_Escrow_PurOrd_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' Order By AmendPoNo Asc";
        //    DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //    Repeater_Rejected.DataSource = DT;
        //    Repeater_Rejected.DataBind();
        //    Repeater_Rejected.Visible = true;
        //    Repeater_Approve.Visible = false;
        //    Repeater_Pending.Visible = false;
        //}
    }

    //protected void txtDepartment_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (txtDepartment.Text != "-Select-")
    //    {
    //        txtRequestStatus_SelectedIndexChanged(sender, e);
    //    }
    //}

    //private void Department_Code_Add_Test()
    //{
    //    string SSQL = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDeptCode_test.Items.Clear();
    //    SSQL = "Select DeptCode from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptCode Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //    txtDeptCode_test.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDeptCode_test.Items.Add(Main_DT.Rows[i]["DeptCode"].ToString());
    //    }
    //}


    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {

        string RptName = "";
        string StdPurOrdNo = "";
        string SupQtnNo = "";
        string DeptName = "";

        Boolean Rights_Check = false;
        Boolean ErrFlag = false;

        Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Escrow PO Amend Approval");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Order Amend..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approved Purchase Request..');", true);
        }

        if (!ErrFlag)
        {
            RptName = "Escrow Purchase Order Amend Details Invoice Format";
            StdPurOrdNo = e.CommandName.ToString();

            ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");

            //Session.Remove("Pur_RequestNo_Approval");
            //Session["Pur_RequestNo_Approval"] = PurRequest_No_Str;
            //Response.Redirect("purchase_request.aspx");
        }
    }
}