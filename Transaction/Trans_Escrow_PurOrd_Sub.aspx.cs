﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using GeneralClass;

public partial class Trans_Escrow_PurOrd_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionStdPOOrderNo;
    string SessionStdPOOrderNot;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    static Decimal OrderQty;
    string NameTypeRequest;
    string SessionPurRequestNoApproval;
    static string SAPNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            if (Session["UserId"] == null)
            {
                Response.Redirect("../Default.aspx");
                Response.Write("Your session expired");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionFinYearCode = Session["FinYearCode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();

            if (!IsPostBack)
            {
                Page.Title = "CORAL ERP :: Escrow Purchase Order";
                Initial_Data_Referesh();
                Delivery_Mode_Add();
                Load_Data_Empty_SuppRefDocNo();
                Load_Data_Empty_Dept();
                Load_Data_GST();

                Load_Data_Empty_Supp1();

                txtSupplierName_Select_SelectedIndexChanged(sender, e);

                txtDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy");

                if (Session["Std_PO_No"] == null)
                {
                    string Auto_Transaction_No = "";
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();

                    SessionStdPOOrderNo = "";

                    //RdpPOType.SelectedValue = "2";
                    Load_Data_Purchase_Request_No();
                    Load_Data_Empty_ItemCode();

                    if (ddlMatType.SelectedItem.Text == "Tools")
                    {
                        Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Tools Purchase Order", SessionFinYearVal, "1");
                    }

                    else if (ddlMatType.SelectedItem.Text == "Asset")
                    {
                        Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Asset Purchase Order", SessionFinYearVal, "1");
                    }

                    else
                    {
                        Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");

                        //Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");
                    }

                    //txtStdOrderNo.Text = Auto_Transaction_No;

                    //txtRefNo.Text = txtStdOrderNo.Text;
                }
                else
                {
                    SessionStdPOOrderNo = Session["Std_PO_No"].ToString();
                    txtStdOrderNo.Text = SessionStdPOOrderNo;
                    Load_Data_Purchase_Request_No();
                    Load_Data_Empty_ItemCode();
                    btnSearch_Click(sender, e);


                }
                if (Session["Transaction_No"] == null)
                {
                    SessionStdPOOrderNot = "";

                    //RdpPOType.SelectedValue = "1";
                }
                else
                {
                    SessionStdPOOrderNot = Session["Transaction_No"].ToString();
                    btnStd_Purchase_Click(sender, e);

                    //RdpPOType.SelectedValue = "2";
                }

                if (Session["Pur_RequestNo_Approval"] == null)
                {
                    SessionPurRequestNoApproval = "";

                }
                else
                {
                    RdpPOType.SelectedValue = "3";
                    SessionPurRequestNoApproval = Session["Pur_RequestNo_Approval"].ToString();
                    txtStdOrderNo.Text = SessionPurRequestNoApproval;
                    btnSearchView_Click(sender, e);
                }
            }
            Load_OLD_data();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }

    private void Load_Data_GST()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable Main_DT = new DataTable();

            ddlTaxType.Items.Clear();
            SSQL = "Select * from GstMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlTaxType.DataSource = Main_DT;

            DataRow dr = Main_DT.NewRow();

            dr["GST_Type"] = "-Select-";

            Main_DT.Rows.InsertAt(dr, 0);

            ddlTaxType.DataTextField = "GST_Type";
            ddlTaxType.DataValueField = "GST_Type";
            ddlTaxType.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Load_Data_Purchase_Request_No()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable Main_DT = new DataTable();

            DataTable da_Customer = new DataTable();
            DataTable da_Order = new DataTable();
            string Cus_Po = "";
            string Bag_PO;
            DataTable dt = new DataTable();
            dt.Columns.Add("Pur_Request_No", typeof(string));

            string MatTypeCode = "";

            //if (Session["Std_PO_No"] == null)
            //{

            if (ddlMatType.SelectedItem.Text == "Raw Material")
            {
                MatTypeCode = "1";

                SSQL = "Select PRM.Pur_Request_No,PRS.ItemCode,PRS.ReuiredQty From Trans_Escrow_PurRqu_Sub PRS ";
                SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Ccode=PRS.Ccode And PRM.Lcode=PRS.Lcode And ";
                SSQL = SSQL + " PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No ";
                SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " PRS.FinYearCode='" + SessionFinYearCode + "' And PRM.Ccode='" + SessionCcode + "' And  ";
                SSQL = SSQL + " PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";
                SSQL = SSQL + " And PRM.MaterialType='" + MatTypeCode + "' and PRM.Pur_Req_Status!='Delete' Order by Pur_Request_No Asc";
                //SSQL = SSQL + " Union All ";
                //SSQL = SSQL + " Select * from Trans_Escrow_PurOrd_Main ";
                //SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
                //SSQL = SSQL + " PRS.FinYearCode='" + SessionFinYearCode + "' And PRM.Ccode='" + SessionCcode + "' And  ";
                //SSQL = SSQL + " PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";



            }
            else if (ddlMatType.SelectedItem.Text == "Tools")
            {
                MatTypeCode = "2";

                SSQL = "Select PRM.Pur_Request_No,PRS.ToolCode[ItemCode],PRS.ReuiredQty From Trans_Escrow_PurRqu_Tools_Sub PRS ";
                SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Ccode=PRS.Ccode And PRM.Lcode=PRS.Lcode And ";
                SSQL = SSQL + " PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No ";
                SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " PRS.FinYearCode='" + SessionFinYearCode + "' And PRM.Ccode='" + SessionCcode + "' And  ";
                SSQL = SSQL + " PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";

                SSQL = SSQL + " And PRM.MaterialType='" + MatTypeCode + "' and PRM.Pur_Req_Status!='Delete' Order by Pur_Request_No Asc";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                MatTypeCode = "3";

                SSQL = "Select PRM.Pur_Request_No,PRS.ToolCode[ItemCode],PRS.ReuiredQty From Trans_Escrow_PurRqu_Asset_Sub PRS ";
                SSQL = SSQL + " Inner join Trans_Escrow_PurRqu_Main PRM on PRM.Ccode=PRS.Ccode And PRM.Lcode=PRS.Lcode And ";
                SSQL = SSQL + " PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No ";
                SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " PRS.FinYearCode='" + SessionFinYearCode + "' And PRM.Ccode='" + SessionCcode + "' And  ";
                SSQL = SSQL + " PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";

                SSQL = SSQL + " And PRM.MaterialType='" + MatTypeCode + "' and PRM.Pur_Req_Status!='Delete' Order by Pur_Request_No Asc";
            }

            da_Customer = objdata.RptEmployeeMultipleDetails(SSQL);

            if (da_Customer.Rows.Count != 0)
            {
                for (int i = 0; i < da_Customer.Rows.Count; i++)
                {
                    string Req_No = ""; string Req_Item = ""; string Req_Qty = "0";
                    Req_No = da_Customer.Rows[i]["Pur_Request_No"].ToString();
                    Req_Item = da_Customer.Rows[i]["ItemCode"].ToString();
                    Req_Qty = da_Customer.Rows[i]["ReuiredQty"].ToString();

                    SSQL = "Select isnull(Sum(OrderQty),0) as PO_Qty from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                    SSQL = SSQL + " Pur_Request_No ='" + Req_No + "' And ItemCode='" + Req_Item + "' and Std_Po_No!='" + txtStdOrderNo.Text + "'";

                    da_Order = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (da_Order.Rows.Count != 0)
                    {
                        Bag_PO = da_Order.Rows[0]["PO_Qty"].ToString();
                        if (Convert.ToDecimal(Bag_PO.ToString()) < Convert.ToDecimal(Req_Qty))
                        {
                            DataRow row = dt.NewRow();
                            row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                            dt.Rows.Add(row);
                        }
                    }
                    else
                    {
                        DataRow row = dt.NewRow();
                        row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                        dt.Rows.Add(row);
                    }
                }
            }

            if (dt.Rows.Count != 0)
            {
                //Main_DT = RemoveDuplicatesRecords(dt);

                var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
                Main_DT = UniqueRows.CopyToDataTable();

            }
            else
            {
                Main_DT = dt;
            }


            txtRequestNo.DataSource = Main_DT;
            DataRow dr = Main_DT.NewRow();
            dr["Pur_Request_No"] = "-Select-";
            dr["Pur_Request_No"] = "-Select-";
            Main_DT.Rows.InsertAt(dr, 0);
            txtRequestNo.DataTextField = "Pur_Request_No";
            txtRequestNo.DataValueField = "Pur_Request_No";
            txtRequestNo.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private DataTable RemoveDuplicatesRecords(DataTable dt)
    {
        try
        {
            //Returns just 5 unique rows
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT_Check = new DataTable();
            DataTable qry_dt = new DataTable();
            string SaveMode = "Insert";
            bool ErrFlag = false;

            string PackGst = "";
            string TransGst = "";
            string CourierGst = "";
            string Other1Gst = "";
            string Other2Gst = "";

            GetIPAndName getIPAndName = new GetIPAndName();
            string SysIP = getIPAndName.GetIP();
            string SysName = getIPAndName.GetName();

            //check with Item Details Add with Grid
            DT_Check = (DataTable)ViewState["ItemTable"];
            if (DT_Check.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
            }
            if (txtDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Date...');", true);
            }
            if (txtSupplierName_Select.SelectedValue == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Supplier Name...');", true);
            }

            //if (txtRefNo.Text == "")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Reference No (Coral)...');", true);
            //}

            if (txtSuppQutaNo.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Quotation No...');", true);
            }

            if (txtSuppQutaDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Quotation Date...');", true);
            }

            if (txtDeliveryDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Request Date...');", true);
            }

            if (txtDeliveryAt.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter The Value Delivery At...');", true);
            }

            if (txtNote.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter The Value Note(Freight)...');", true);
            }

            if (txtOthers.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter The Value Delivery Schedules...');", true);
            }

            if (hfDeliMode.Value == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Dispatch Through...');", true);
            }

            if (txtPaymentMode.SelectedItem.Text == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Payment Mode...');", true);
            }

            if (hfDeptCode.Value == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Department Name...');", true);
            }

            if (ddlTaxType.SelectedItem.Text == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select GST Type...');", true);
            }

            if (ddlPurMode.SelectedItem.Text == "Purchase Request")
            {
                if (txtRequestNo.SelectedItem.Text == "-Select-")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Purchase Request No...');", true);
                }
            }

            //Check Supplier Name And Quotation No
            SSQL = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And QuotNo='" + txtSuppRefDocNo.Text + "' And SuppCode='" + txtSuppCodehide.Value + "'";

            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

          

            //Auto generate Transaction Function Call
            if (btnSave.Text != "Update")
            {
                if (!ErrFlag)
                {
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = "";
                    if (ddlMatType.SelectedItem.Text == "Tools")
                    {
                        Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Tools Purchase Order", SessionFinYearVal, "1");
                    }
                    else if (ddlMatType.SelectedItem.Text == "Asset")
                    {
                        Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Asset Purchase Order", SessionFinYearVal, "1");
                    }
                    else
                    {
                        Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");
                    }

                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                        txtStdOrderNo.Text = Auto_Transaction_No;
                        txtRefNo.Text = Auto_Transaction_No;
                    }
                }
            }

         

            if (!ErrFlag)
            {
                if (btnSave.Text != "Save")
                {
                    SaveMode = "Update";
                }

                SSQL = "Delete from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Delete from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Delete from Trans_Escrow_PurOrd_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);
               
                string MatNo = "";

                if (ddlMatType.SelectedItem.Text == "Raw Material")
                {
                    MatNo = "1";
                }
                else if (ddlMatType.SelectedItem.Text == "Tools")
                {
                    MatNo = "2";
                }
                else if (ddlMatType.SelectedItem.Text == "Asset")
                {
                    MatNo = "3";
                }

                if (chkPFGst.Checked == true)
                {
                    PackGst = "1";
                }
                else
                {
                    PackGst = "0";
                }

                if (chkTCGst.Checked == true)
                {
                    TransGst = "1";
                }
                else
                {
                    TransGst = "0";
                }

                if (chkCCGst.Checked == true)
                {
                    CourierGst = "1";
                }
                else
                {
                    CourierGst = "0";
                }

                if (chkOther1Gst.Checked == true)
                {
                    Other1Gst = "1";
                }
                else
                {
                    Other1Gst = "0";
                }

                if (chkOther2Gst.Checked == true)
                {
                    Other2Gst = "1";
                }
                else
                {
                    Other2Gst = "0";
                }

                //Response.Write(strValue);
                //Insert Main Table
                SSQL = "Insert Into Trans_Escrow_PurOrd_Main(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Supp_Code,Supp_Name,";
                SSQL = SSQL + " Supp_Qtn_No,Supp_Qtn_Date,Pur_Request_No,Pur_Request_Date,DeliveryModeCode,DeliveryMode,DeliveryDate,DeliveryAt,";
                SSQL = SSQL + " PaymentMode,DeptCode,DeptName,PaymentTerms,Description,Note,Others,TotalQuantity,TotalAmt,Discount,TaxPer,";
                SSQL = SSQL + " TaxAmount,OtherCharge,NetAmount,Approvedby,UserID,UserName,PO_Status,PO_Type,AddOrLess,RefNo,GSTPer,TaxType,";
                SSQL = SSQL + " Active,CurrencyType,TotCGST,TotSGST,TotIGST,MatType,SplNote,ReceiveStatus,AmendStatus,GSTName,Purpose,SplSubject,";
                SSQL = SSQL + " IpName,SysName,CreateOn,PackWGST,TotPackChr,TransWGST,TotTransChr,CourWGST,TotCourChr,";
                SSQL = SSQL + " Othr1WGST,TotOther1Chr,Othr2WGST,TotOther2Chr) ";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + txtStdOrderNo.Text + "','" + txtDate.Text + "','" + txtSupplierName_Select.SelectedValue + "',";
                SSQL = SSQL + " '" + txtSupplierName_Select.SelectedItem.Text + "','" + txtSuppQutaNo.Text + "','" + txtSuppQutaDate.Text + "',";
                SSQL = SSQL + " '" + txtRequestNo.SelectedItem.Text + "','" + txtRequestDate.Text + "','" + hfDeliMode.Value + "',";
                SSQL = SSQL + " '" + txtDeliveryMode.SelectedItem.Text + "','" + txtDeliveryDate.Text + "','" + txtDeliveryAt.Text + "',";
                SSQL = SSQL + " '" + txtPaymentMode.SelectedItem.Text + "','" + hfDeptCode.Value + "', '" + txtDepartmentName.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + txtPaymentTerms.Text + "','" + txtDescription.Text + "','" + txtNote.Text + "','" + txtOthers.Text + "',";
                SSQL = SSQL + " '" + txtTotQty.Text + "','" + txtTotAmt.Text + "','" + lblTotDisc.Text + "','0','0','0','" + txtNetAmt.Text + "','',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','0','Purchase Request','" + txtAddOrLess.Text + "',";
                SSQL = SSQL + " '" + txtRefNo.Text + "','0','" + hfGSTDescr.Value + "','Add','" + ddlCurrencyType.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + lblTotCGST.Text + "','" + lblTotSGST.Text + "','" + lblTotIGST.Text + "','" + MatNo + "',";
                SSQL = SSQL + " '" + txtSplIns.Text + "','0','0','" + ddlTaxType.SelectedItem.Text + "','" + txtPurpose.Text + "',";
                SSQL = SSQL + " '" + txtSplSubject.Text + "','" + SysIP + "','" + SysName + "',Convert(Datetime,GetDate(),103),";
                SSQL = SSQL + " '" + PackGst + "','" + txtPackFrwdCharge.Text + "','" + TransGst + "','" + txtTransCharge.Text + "',";
                SSQL = SSQL + " '" + CourierGst + "','" + txtCourierCharge.Text + "','" + Other1Gst + "','" + txtOther1Amt.Text + "',";
                SSQL = SSQL + " '" + Other2Gst + "','" + txtOther2Amt.Text + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ItemTable"];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SSQL = "Insert Into Trans_Escrow_PurOrd_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Pur_Request_No,";
                    SSQL = SSQL + " Pur_Request_Date,ItemCode,ItemName,SAPNo,UOMCode,ReuiredQty,ModelQty,RquQty,ReuiredDate,OrderQty,Rate,ItemTotal,";
                    SSQL = SSQL + " Discount_Per,Discount,GSTPer,GSTName,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount,LineTotal,TaxPer,";
                    SSQL = SSQL + " TaxAmount,BDUTaxPer,BDUTaxAmount,OtherCharge,RefNo,Remarks,Active,UserID,UserName,CalWeek) values (";
                    SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "', ";
                    SSQL = SSQL + " '" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','" + txtRequestNo.SelectedItem.Text + "',";
                    SSQL = SSQL + " '" + txtRequestDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["SAPNo"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["ReuiredQty"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ModelQty"].ToString() + "','" + dt.Rows[i]["RquQty"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ReuiredDate"].ToString() + "','" + dt.Rows[i]["OrderQty"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ItemRate"].ToString() + "','" + dt.Rows[i]["ItemTotal"].ToString() + "','" + dt.Rows[i]["Discount_Per"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["Discount"].ToString() + "','" + dt.Rows[i]["GstPer"].ToString() + "',";
                    SSQL = SSQL + " '" + hfGSTDescr.Value + "','" + dt.Rows[i]["CGSTPer"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["CGSTAmount"].ToString() + "','" + dt.Rows[i]["SGSTPer"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["SGSTAmount"].ToString() + "','" + dt.Rows[i]["IGSTPer"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["IGSTAmount"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "',";
                    SSQL = SSQL + " '0.00','0.00','0.00','0.00','0.00','" + txtRefNo.Text + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["Remarks"].ToString() + "','Add','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["CalendarWeek"].ToString() + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                if (RdpPOType.SelectedItem.Text == "Special")
                {
                    //Insert Purchase Request Approval Table
                    //SSQL = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                    //SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','Std/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
                    //SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                    //objdata.RptEmployeeMultipleDetails(SSQL);
                }
                if (RdpPOType.SelectedItem.Text == "Direct" || RdpPOType.SelectedItem.Text == "Req/Amend")
                {
                    //Insert Purchase Request Approval Table
                    SSQL = "Insert Into Trans_Escrow_PurOrd_Approve(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                    SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','Std/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
                    SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }



                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Standard Purchase Order Details Updated Successfully');", true);
                }


                if (btnSave.Text != "Update")
                {
                    TransactionNoGenerate TransNO_up = new TransactionNoGenerate();
                    string Auto_Transaction_No_Up = "";

                    if (ddlMatType.SelectedItem.Text == "Tools")
                    {
                        Auto_Transaction_No_Up = TransNO_up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Tools Purchase Order", SessionFinYearVal, "4");
                    }
                    else if (ddlMatType.SelectedItem.Text == "Asset")
                    {
                        Auto_Transaction_No_Up = TransNO_up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Asset Purchase Order", SessionFinYearVal, "4");
                    }
                    else
                    {
                        Auto_Transaction_No_Up = TransNO_up.Auto_Generate_No_Numbering_Setup_Update(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");
                    }
                }
                //Clear_All_Field();
                Session["Std_PO_No"] = txtStdOrderNo.Text;
                btnSave.Text = "Update";
                //Session["Transaction_No"] = txtStdOrderNo.Text;
                //Load_Data_Enquiry_Grid();
                Response.Redirect("Trans_Escrow_PurOrd_Main.aspx");
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();

            if (lblErrorMsg.Text == "Error: Error converting data type varchar to numeric.")
            {
                if (txtPackFrwdCharge.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Enter Packing and Forwarding Charge...');", true);
                }
                else if (txtTransCharge.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Enter Transport Charge...');", true);
                }
                else if (txtCourierCharge.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Enter Courier Charge...');", true);
                }
                else if (txtOther1Amt.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Enter Custom Duty Charges...');", true);
                }
                else if (txtOther2Amt.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Enter Duty and Warehouse Charges...');", true);
                }
            }
            else if (lblErrorMsg.Text == "Error: String or binary data would be truncated.\r\nThe statement has been terminated.")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Error in Maximum of Charactor in Text Field...');", true);
            }

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
        //txtGST_Type_SelectedIndexChanged(sender, e);
    }

    private void Clear_All_Field()
    {
        try
        {
            lblErrorMsg.Text = "";
            txtStdOrderNo.Text = ""; txtDate.Text = ""; txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
            txtSuppRefDocNo.SelectedItem.Text = "-Select-"; txtDocDate.Text = ""; txtDeliveryMode.SelectedItem.Text = "-Select-";
            txtDeliveryDate.Text = "";
            txtDeliveryAt.Text = ""; txtPaymentMode.SelectedItem.Text = "-Select-"; txtDepartmentName.SelectedItem.Text = "-Select-";
            txtPaymentTerms.Text = ""; txtDescription.Text = ""; txtNote.Text = ""; txtOthers.Text = "";
            txtRequestNo.Text = ""; txtRequestDate.Text = ""; txtItemCodeHide.Value = ""; txtItemName.Text = "";
            txtRequiredQty.Text = ""; txtOrderQty.Text = ""; txtRequiredDate.Text = ""; txtItemRate.Text = "0.0";
            txtDiscount.Text = ""; txtTax.Text = "0"; txtOtherCharge.Text = "0"; txtRemarks.Text = "";
            txtTaxAmt.Text = "0"; txtTotAmt.Text = "0.0"; txtNetAmt.Text = "0"; RdpPOType.SelectedValue = "2";
            txtModeOfTransfer.Text = "";
            txtChequeAmt.Text = "";
            txtChequeDate.Text = "";
            txtChequeNo.Text = "";

            txtAddOrLess.Text = "0";

            txtItemTotal.Text = "0.00";
            txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
            txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
            txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
            txtFinal_Total_Amt.Text = "0.00";

            TotalGstPer_Hidden.Value = "0";


            btnSave.Text = "Save";
            Initial_Data_Referesh();
            Session.Remove("Std_PO_No");
            Session.Remove("Transaction_No");
            Session.Remove("Pur_RequestNo_Approval");
            //Load_Data_Enquiry_Grid();
            //txtDeptCodeHide.Value = "";
            if (RdpPOType.SelectedValue == "1")
            {
                txtRequestNo.Enabled = false;
                txtRequestDate.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            DataTable dt = new DataTable();
            DataTable qry_dt = new DataTable();
            bool ErrFlag = false;
            DataRow dr = null;
            string SSQL = "";

            if (ddlPurMode.SelectedItem.Text == "Purchase Request")
            {
                NameTypeRequest = "Purchase Request";

                if (NameTypeRequest.Trim() == "Purchase Request")
                {
                    //SSQL = "Select * from Trans_Coral_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
                    //SSQL = SSQL + "And Pur_Request_No='" + txtRequestNo.Text + "'";
                    //qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    //if (qry_dt.Rows.Count != 0)
                    //{
                    //    ErrFlag = true;
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Item Details Already Exists in Purchase Order..');", true);
                    //    txtItemCodeHide.Value = ""; txtItemName.Text = "";
                    //    txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                    //    txtRemarks.Text = "";

                    //}

                    if (ddlMatType.SelectedItem.Text == "Tools")
                    {
                        SSQL = "Select * from Trans_Escrow_PurRqu_Tools_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And FinYearCode ='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
                    }
                    else if (ddlMatType.SelectedItem.Text == "Raw Material")
                    {
                        SSQL = "Select * from Trans_Escrow_PurRqu_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' ";
                        SSQL = SSQL + " And CalendarWeek='" + txtCalWeek.Text + "'";
                    }
                    else if (ddlMatType.SelectedItem.Text == "Asset")
                    {
                        SSQL = "Select * from Trans_Escrow_PurRqu_Asset_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
                    }


                    qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (qry_dt.Rows.Count == 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Check with Purchase Request No..');", true);
                    }

                    //check with Item Code And Item Name


                    if (ddlMatType.SelectedItem.Text == "Tools")
                    {
                        SSQL = "Select *,UOM[UOMCode],ToolCode[ItemCode] from Trans_Escrow_PurRqu_Tools_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And ToolCode='" + txtItemCodeHide.Value + "' ";
                        SSQL = SSQL + " And ToolName='" + txtItemName.Text + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
                    }
                    else if (ddlMatType.SelectedItem.Text == "Asset")
                    {
                        SSQL = "Select *,UOM[UOMCode],ToolCode[ItemCode] from Trans_Escrow_PurRqu_Asset_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And ToolCode='" + txtItemCodeHide.Value + "' ";
                        SSQL = SSQL + " And ToolName='" + txtItemName.Text + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
                    }
                    else if (ddlMatType.SelectedItem.Text == "Raw Material")
                    {
                        SSQL = "Select * from Trans_Escrow_PurRqu_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' ";
                        SSQL = SSQL + " And ItemName='" + txtItemName.Text + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
                        SSQL = SSQL + " And CalendarWeek='" + txtCalWeek.Text + "'";
                    }

                    qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (qry_dt.Rows.Count == 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Check with Item Details..');", true);
                    }

                }

                if (!ErrFlag)
                {
                    string UOM_Code_Str = qry_dt.Rows[0]["UOMCode"].ToString();

                    if (ViewState["ItemTable"] != null)
                    {
                        //get datatable from view state   
                        dt = (DataTable)ViewState["ItemTable"];

                        //check Item Already add or not
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            if (dt.Rows[i]["CalendarWeek"].ToString() == txtCalWeek.Text)
                            {
                                if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                                {
                                    ErrFlag = true;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                                }
                            }
                        }

                        if (!ErrFlag)
                        {
                            dr = dt.NewRow();
                            dr["SAPNo"] = SAPNo;
                            dr["ItemCode"] = txtItemCodeHide.Value;
                            dr["ItemName"] = txtItemName.Text;
                            dr["UOMCode"] = UOM_Code_Str;
                            dr["ReuiredQty"] = txtRequiredQty.Text;
                            dr["ModelQty"] = txtModelQty.Text;
                            dr["RquQty"] = txtOrderQty.Text;
                            dr["OrderQty"] = txtOrderQty.Text;
                            dr["ReuiredDate"] = txtRequiredDate.Text;
                            dr["ItemRate"] = (Math.Round(Convert.ToDecimal(txtItemRate.Text), 4, MidpointRounding.AwayFromZero)).ToString();
                            dr["Remarks"] = txtRemarks.Text;
                            dr["ItemTotal"] = (Math.Round(Convert.ToDecimal(txtItemTotal.Text), 4, MidpointRounding.AwayFromZero)).ToString();
                            //dr["ItemTotal"] = txtItemTotal.Text;

                            dr["Discount_Per"] = txtDiscount_Per.Text;
                            dr["Discount"] = txtDiscount_Amount.Text;
                            dr["TaxPer"] = txtVAT_Per.Text;
                            dr["TaxAmount"] = txtVAT_AMT.Text;
                            dr["CGSTPer"] = txtCGSTPer.Text;
                            dr["CGSTAmount"] = txtCGSTAmt.Text;
                            dr["SGSTPer"] = txtSGSTPer.Text;
                            dr["SGSTAmount"] = txtSGSTAmt.Text;
                            dr["IGSTPer"] = txtIGSTPer.Text;
                            dr["IGSTAmount"] = txtIGSTAmt.Text;
                            dr["GstPer"] = TotalGstPer_Hidden.Value;
                            dr["OtherCharge"] = txtOtherCharge.Text;
                            dr["LineTotal"] = txtFinal_Total_Amt.Text;

                            dr["CalendarWeek"] = txtCalWeek.Text;

                            dt.Rows.Add(dr);
                            ViewState["ItemTable"] = dt;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();
                            Totalsum();
                            txtItemCodeHide.Value = ""; txtItemName.Text = "";
                            txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtItemRate.Text = "0.0";
                            txtRemarks.Text = "";
                            TotalGstPer_Hidden.Value = "0";
                            txtItemTotal.Text = "0.00";

                            txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                            txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                            txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00";
                            txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                            txtFinal_Total_Amt.Text = "0.00";
                            txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                            //txtGST_Type_SelectedIndexChanged(sender, e);
                        }
                        else
                        {
                            if (NameTypeRequest.Trim() == "Request")
                            {
                                SSQL = "Select * from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (qry_dt.Rows.Count != 0)
                                {
                                    txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                    txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                    txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                }
                            }
                            else if (NameTypeRequest.Trim() == "Amend")
                            {
                                SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (qry_dt.Rows.Count != 0)
                                {
                                    txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                    txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                    txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                }

                            }
                        }
                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["SAPNo"] = SAPNo;
                        dr["ItemCode"] = txtItemCodeHide.Value;
                        dr["ItemName"] = txtItemName.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["ModelQty"] = txtModelQty.Text;
                        dr["RquQty"] = txtOrderQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["ReuiredDate"] = txtRequiredDate.Text;
                        dr["ItemRate"] = (Math.Round(Convert.ToDecimal(txtItemRate.Text), 4, MidpointRounding.AwayFromZero)).ToString();
                        dr["Remarks"] = txtRemarks.Text;

                        dr["ItemTotal"] = (Math.Round(Convert.ToDecimal(txtItemTotal.Text), 4, MidpointRounding.AwayFromZero)).ToString();


                        dr["Discount_Per"] = txtDiscount_Per.Text;
                        dr["Discount"] = txtDiscount_Amount.Text;
                        dr["TaxPer"] = txtVAT_Per.Text;
                        dr["TaxAmount"] = txtVAT_AMT.Text;
                        dr["CGSTPer"] = txtCGSTPer.Text;
                        dr["CGSTAmount"] = txtCGSTAmt.Text;
                        dr["SGSTPer"] = txtSGSTPer.Text;
                        dr["SGSTAmount"] = txtSGSTAmt.Text;
                        dr["IGSTPer"] = txtIGSTPer.Text;
                        dr["IGSTAmount"] = txtIGSTAmt.Text;
                        dr["GstPer"] = TotalGstPer_Hidden.Value;
                        dr["OtherCharge"] = txtOtherCharge.Text;
                        dr["LineTotal"] = txtFinal_Total_Amt.Text;

                        dr["CalendarWeek"] = txtCalWeek.Text;

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();

                        Totalsum();

                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtItemRate.Text = "0.0";
                        txtRemarks.Text = "";
                        TotalGstPer_Hidden.Value = "0";
                        txtItemTotal.Text = "0.00";
                        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00";
                        txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                        txtFinal_Total_Amt.Text = "0.00";
                        txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                        //txtGST_Type_SelectedIndexChanged(sender, e);
                    }
                }
                else
                {
                    if (NameTypeRequest.Trim() == "Request")
                    {
                        SSQL = "Select * from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (qry_dt.Rows.Count != 0)
                        {
                            txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                            txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                            txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                        }
                    }
                    else if (NameTypeRequest.Trim() == "Amend")
                    {
                        SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (qry_dt.Rows.Count != 0)
                        {
                            txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                            txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                            txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                        }

                    }
                }
            }
            else
            {

                if (txtOrderQty.Text == "0.0" || txtOrderQty.Text == "0" || txtOrderQty.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Order Qty...');", true);
                }


                if (txtItemRate.Text == "0.0" || txtItemRate.Text == "0" || txtItemRate.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Rate...');", true);
                }

                NameTypeRequest = "Direct Purchase";

                if (NameTypeRequest.Trim() == "Direct Purchase")
                {
                    if (!ErrFlag)
                    {
                        //UOM Code get
                        string UOM_Code_Str = hfUOM.Value;
                        string Line_Total = "";
                        Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtItemRate.Text)).ToString();
                        //Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRateEUR.Text)).ToString();
                        Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();
                        Total_Calculate();

                        // check view state is not null  
                        if (ViewState["ItemTable"] != null)
                        {
                            //get datatable from view state   
                            dt = (DataTable)ViewState["ItemTable"];

                            //check Item Already add or not
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                                {
                                    ErrFlag = true;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                                }
                            }
                            if (!ErrFlag)
                            {
                                dr = dt.NewRow();
                                dr["ItemCode"] = txtItemCodeHide.Value;
                                dr["ItemName"] = txtItemName.Text;
                                dr["UOMCode"] = UOM_Code_Str;
                                dr["ReuiredQty"] = txtRequiredQty.Text;
                                dr["OrderQty"] = txtOrderQty.Text;
                                dr["ReuiredDate"] = txtRequiredDate.Text;
                                dr["ItemRate"] = (Math.Round(Convert.ToDecimal(txtItemRate.Text), 4, MidpointRounding.AwayFromZero)).ToString();
                                dr["Remarks"] = txtRemarks.Text;

                                //dr["ItemTotal"] = txtItemTotal.Text;
                                dr["ItemTotal"] = (Math.Round(Convert.ToDecimal(txtItemTotal.Text), 4, MidpointRounding.AwayFromZero)).ToString();

                                dr["Discount_Per"] = txtDiscount_Per.Text;
                                dr["Discount"] = txtDiscount_Amount.Text;
                                dr["TaxPer"] = txtVAT_Per.Text;
                                dr["TaxAmount"] = txtVAT_AMT.Text;
                                dr["CGSTPer"] = txtCGSTPer.Text;
                                dr["CGSTAmount"] = txtCGSTAmt.Text;
                                dr["SGSTPer"] = txtSGSTPer.Text;
                                dr["SGSTAmount"] = txtSGSTAmt.Text;
                                dr["IGSTPer"] = txtIGSTPer.Text;
                                dr["IGSTAmount"] = txtIGSTAmt.Text;
                                dr["GstPer"] = TotalGstPer_Hidden.Value;
                                dr["OtherCharge"] = txtOtherCharge.Text;
                                dr["LineTotal"] = txtFinal_Total_Amt.Text;

                                dr["CalendarWeek"] = txtCalWeek.Text;

                                dt.Rows.Add(dr);
                                ViewState["ItemTable"] = dt;
                                Repeater1.DataSource = dt;
                                Repeater1.DataBind();
                                Totalsum();
                                txtItemCodeHide.Value = ""; txtItemName.Text = "";
                                txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = "";
                                txtItemRate.Text = "0.0";
                                txtRemarks.Text = "";
                                TotalGstPer_Hidden.Value = "0";
                                txtItemTotal.Text = "0.00";
                                txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                                txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                                txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                                txtFinal_Total_Amt.Text = "0.00";
                                txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                                //txtGST_Type_SelectedIndexChanged(sender, e);
                            }
                            else
                            {
                                if (NameTypeRequest.Trim() == "Request")
                                {
                                    SSQL = "Select * from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                    qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (qry_dt.Rows.Count != 0)
                                    {
                                        txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                        txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                        txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                    }
                                }
                                else if (NameTypeRequest.Trim() == "Amend")
                                {
                                    SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                    qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (qry_dt.Rows.Count != 0)
                                    {
                                        txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                        txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                        txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                    }

                                }
                            }
                        }
                        else
                        {
                            dr = dt.NewRow();
                            dr["ItemCode"] = txtItemNameSelect.SelectedValue;
                            dr["ItemName"] = txtItemNameSelect.SelectedItem.Text;
                            dr["UOMCode"] = UOM_Code_Str;
                            dr["ReuiredQty"] = txtRequiredQty.Text;
                            dr["OrderQty"] = txtOrderQty.Text;
                            dr["ReuiredDate"] = txtRequiredDate.Text;
                            dr["ItemRate"] = (Math.Round(Convert.ToDecimal(txtItemRate.Text), 4, MidpointRounding.AwayFromZero)).ToString();
                            //dr["ItemRate"] = txtItemRate.Text;
                            dr["Remarks"] = txtRemarks.Text;
                            dr["ItemTotal"] = (Math.Round(Convert.ToDecimal(txtItemTotal.Text), 4, MidpointRounding.AwayFromZero)).ToString();
                            //dr["ItemTotal"] = txtItemTotal.Text;
                            dr["Discount_Per"] = txtDiscount_Per.Text;
                            dr["Discount"] = txtDiscount_Amount.Text;
                            dr["TaxPer"] = txtVAT_Per.Text;
                            dr["TaxAmount"] = txtVAT_AMT.Text;
                            dr["CGSTPer"] = txtCGSTPer.Text;
                            dr["CGSTAmount"] = txtCGSTAmt.Text;
                            dr["SGSTPer"] = txtSGSTPer.Text;
                            dr["SGSTAmount"] = txtSGSTAmt.Text;
                            dr["IGSTPer"] = txtIGSTPer.Text;
                            dr["IGSTAmount"] = txtIGSTAmt.Text;
                            dr["GstPer"] = TotalGstPer_Hidden.Value;
                            dr["OtherCharge"] = txtOtherCharge.Text;
                            dr["LineTotal"] = txtFinal_Total_Amt.Text;

                            dr["CalendarWeek"] = txtCalWeek.Text;

                            dt.Rows.Add(dr);
                            ViewState["ItemTable"] = dt;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();

                            Totalsum();

                            txtItemCodeHide.Value = ""; txtItemName.Text = "";
                            txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = "";
                            txtItemRate.Text = "0.0";
                            txtRemarks.Text = "";
                            TotalGstPer_Hidden.Value = "0";
                            txtItemTotal.Text = "0.00";
                            txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                            txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                            txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0";
                            txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                            txtFinal_Total_Amt.Text = "0.00";
                            txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                            //txtGST_Type_SelectedIndexChanged(sender, e);
                        }
                    }
                    else
                    {
                        if (NameTypeRequest.Trim() == "Request")
                        {
                            SSQL = "Select * from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (qry_dt.Rows.Count != 0)
                            {
                                txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                            }
                        }
                        else if (NameTypeRequest.Trim() == "Amend")
                        {
                            SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (qry_dt.Rows.Count != 0)
                            {
                                txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }


    public void Totalsum()
    {
        try
        {
            lblErrorMsg.Text = "";
            sum = "0";
            OrderQty = 0;
            string item_Tot = "0";
            string CGST_Tot = "0";
            string SGST_Tot = "0";
            string IGST_Tot = "0";


            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
                txtTotAmt.Text = sum;
                OrderQty = Convert.ToDecimal(OrderQty) + Convert.ToDecimal(dt.Rows[i]["OrderQty"]);

                item_Tot = (Convert.ToDecimal(item_Tot) + Convert.ToDecimal(dt.Rows[i]["ItemTotal"])).ToString();

                CGST_Tot = (Convert.ToDecimal(CGST_Tot) + Convert.ToDecimal(dt.Rows[i]["CGSTAmount"].ToString())).ToString();
                SGST_Tot = (Convert.ToDecimal(SGST_Tot) + Convert.ToDecimal(dt.Rows[i]["SGSTAmount"].ToString())).ToString();
                IGST_Tot = (Convert.ToDecimal(IGST_Tot) + Convert.ToDecimal(dt.Rows[i]["IGSTAmount"].ToString())).ToString();

                txtTotQty.Text = OrderQty.ToString();
            }

            lblTotItem.Text = item_Tot;

            lblTotCGST.Text = CGST_Tot;
            lblTotSGST.Text = SGST_Tot;
            lblTotIGST.Text = IGST_Tot;

            Final_Total_Calculate();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }

    private void Initial_Data_Referesh()
    {
        try
        {
            lblErrorMsg.Text = "";
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("SAPNo", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
            dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
            dt.Columns.Add(new DataColumn("RquQty", typeof(string)));
            dt.Columns.Add(new DataColumn("ModelQty", typeof(string)));
            dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
            dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemRate", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

            dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
            dt.Columns.Add(new DataColumn("Discount_Per", typeof(string)));
            dt.Columns.Add(new DataColumn("Discount", typeof(string)));
            dt.Columns.Add(new DataColumn("TaxPer", typeof(string)));
            dt.Columns.Add(new DataColumn("TaxAmount", typeof(string)));
            dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
            dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
            dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
            dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
            dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
            dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
            dt.Columns.Add(new DataColumn("OtherCharge", typeof(string)));
            dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
            dt.Columns.Add(new DataColumn("GstPer", typeof(string)));
            dt.Columns.Add(new DataColumn("CalendarWeek", typeof(string)));

            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            ViewState["ItemTable"] = Repeater1.DataSource;
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
                {
                    dt.Rows.RemoveAt(i);
                    dt.AcceptChanges();
                }
            }
            ViewState["ItemTable"] = dt;
            Load_OLD_data();

            txtTax.Text = "0";
            txtTaxAmt.Text = "0";
            txtDiscount.Text = "0";
            txtOthers.Text = "0";
            txtNetAmt.Text = "0";
            Totalsum();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnStd_Purchase_Click(object sender, EventArgs e)
    {
        //string SSQL = "";
        //DataTable Main_DT = new DataTable();
        //SSQL = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + SessionStdPOOrderNot + "'";
        //Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (Main_DT.Rows.Count != 0)
        //{
        //    txtRequestNo.Text = Main_DT.Rows[0]["Transaction_No"].ToString();
        //    txtRequestDate.Text = Main_DT.Rows[0]["Date"].ToString();
        //}
        //elsep
        //{
        //    Clear_All_Field();
        //}
    }

    private void Load_OLD_data()
    {
        try
        {
            lblErrorMsg.Text = "";
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            //Search Purchase Enquiry
            string SSQL = "";
            DataTable Main_DT = new DataTable();

            SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Main_DT.Rows.Count != 0)
            {
                ddlMatType.SelectedValue = Main_DT.Rows[0]["MatType"].ToString();

                txtDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
                txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
                txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
                txtSupplierName_Select.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
                txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
                txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
                txtRefNo.Text = Main_DT.Rows[0]["RefNo"].ToString();
                txtSuppQutaNo.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
                txtSuppQutaDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
                txtDeliveryMode.SelectedValue = Main_DT.Rows[0]["DeliveryMode"].ToString();
                hfDeliMode.Value = Main_DT.Rows[0]["DeliveryModeCode"].ToString();

                //txtDeliveryMode.SelectedItem.Text = Main_DT.Rows[0]["DeliveryMode"].ToString();
                txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();


                txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
                //txtPaymentMode.SelectedItem.Text = Main_DT.Rows[0]["PaymentMode"].ToString();
                txtPaymentMode.SelectedValue = Main_DT.Rows[0]["PaymentMode"].ToString();
                //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();

                txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
                txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
                hfDeptCode.Value = Main_DT.Rows[0]["DeptCode"].ToString();

                txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
                txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
                txtSplIns.Text = Main_DT.Rows[0]["SplNote"].ToString();

                txtSplSubject.Text = Main_DT.Rows[0]["SplSubject"].ToString();

                txtPurpose.Text = Main_DT.Rows[0]["Purpose"].ToString();
                txtNote.Text = Main_DT.Rows[0]["Note"].ToString();

                txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
                txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
                txtTotQty.Text = Main_DT.Rows[0]["TotalQuantity"].ToString();

                //txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
                txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
                //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
                txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();


                txtRequestNo.SelectedValue = Main_DT.Rows[0]["Pur_Request_No"].ToString();
                //txtRequestNo.SelectedItem.Text = Main_DT.Rows[0]["Pur_Request_No"].ToString();
                txtRequestDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();


                if (Main_DT.Rows[0]["PO_Type"].ToString() == "Purchase Request")
                {
                    ddlPurMode.SelectedItem.Text = Main_DT.Rows[0]["PO_Type"].ToString();
                    pnlPurRqu.Visible = true;
                    pnlDirectOrd.Visible = false;
                }

                else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Direct")
                {
                    ddlPurMode.SelectedItem.Text = Main_DT.Rows[0]["PO_Type"].ToString();
                    pnlPurRqu.Visible = false;
                    pnlDirectOrd.Visible = true;
                }

                else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Req/Amend")
                {
                    RdpPOType.SelectedValue = "2";
                }

                lblTotCGST.Text = Main_DT.Rows[0]["TotCGST"].ToString();
                lblTotSGST.Text = Main_DT.Rows[0]["TotSGST"].ToString();
                lblTotIGST.Text = Main_DT.Rows[0]["TotIGST"].ToString();

                txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
                txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();


                //txtModeOfTransfer.Text = Main_DT.Rows[0]["ModeOfTransfer"].ToString();
                //txtChequeNo.Text = Main_DT.Rows[0]["ChequeNo"].ToString();
                //txtChequeDate.Text = Main_DT.Rows[0]["ChequeDate"].ToString();
                //txtChequeAmt.Text = Main_DT.Rows[0]["ChequeAmt"].ToString();
                hfGSTDescr.Value = Main_DT.Rows[0]["TaxType"].ToString();
                //ddlTaxType.SelectedItem.Text = Main_DT.Rows[0]["GSTName"].ToString();
                ddlTaxType.SelectedValue = Main_DT.Rows[0]["GSTName"].ToString();

                if (Main_DT.Rows[0]["CurrencyType"].ToString() == "INR")
                {
                    ddlCurrencyType.SelectedValue = "1";
                }
                else if (Main_DT.Rows[0]["CurrencyType"].ToString() == "EUR")
                {
                    ddlCurrencyType.SelectedValue = "2";
                }
                else if (Main_DT.Rows[0]["CurrencyType"].ToString() == "USD")
                {
                    ddlCurrencyType.SelectedValue = "3";
                }
                else if (Main_DT.Rows[0]["CurrencyType"].ToString() == "JPY")
                {
                    ddlCurrencyType.SelectedValue = "4";
                }
                else if (Main_DT.Rows[0]["CurrencyType"].ToString() == "GBP")
                {
                    ddlCurrencyType.SelectedValue = "5";
                }


                if (Main_DT.Rows[0]["PackWGST"].ToString() == "1")
                {
                    chkPFGst.Checked = true;
                }
                else
                {
                    chkPFGst.Checked = false;
                }

                if (Main_DT.Rows[0]["TransWGST"].ToString() == "1")
                {
                    chkTCGst.Checked = true;
                }
                else
                {
                    chkTCGst.Checked = false;
                }

                if (Main_DT.Rows[0]["CourWGST"].ToString() == "1")
                {
                    chkCCGst.Checked = true;
                }
                else
                {
                    chkCCGst.Checked = false;
                }

                if (Main_DT.Rows[0]["Othr1WGST"].ToString() == "1")
                {
                    chkOther1Gst.Checked = true;
                }
                else
                {
                    chkOther1Gst.Checked = false;
                }

                if (Main_DT.Rows[0]["Othr2WGST"].ToString() == "1")
                {
                    chkOther2Gst.Checked = true;
                }
                else
                {
                    chkOther2Gst.Checked = false;
                }

                txtPackFrwdCharge.Text = Main_DT.Rows[0]["TotPackChr"].ToString();
                txtTransCharge.Text = Main_DT.Rows[0]["TotTransChr"].ToString();
                txtCourierCharge.Text = Main_DT.Rows[0]["TotCourChr"].ToString();

                txtOther1Amt.Text = Main_DT.Rows[0]["TotOther1Chr"].ToString();
                txtOther2Amt.Text = Main_DT.Rows[0]["TotOther2Chr"].ToString();


                //Trans_Coral_PurOrd_Sub Table Load
                DataTable dt = new DataTable();

                SSQL = "Select SAPNo,ItemCode,ItemName,UOMCode,RquQty,ModelQty,ReuiredQty,OrderQty,ReuiredDate,Rate [ItemRate], ";
                SSQL = SSQL + "Remarks,ItemTotal,Discount_Per,Discount,TaxPer,TaxAmount,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,";
                SSQL = SSQL + " IGSTAmount,BDUTaxPer,BDUTaxAmount,OtherCharge,LineTotal,GstPer,CalWeek[CalendarWeek] ";
                SSQL = SSQL + " from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
                SSQL = SSQL + " Order by cast(SAPNo as int),ModelQty ";

                dt = objdata.RptEmployeeMultipleDetails(SSQL);



                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    lblTotItem.Text = (Convert.ToDecimal(lblTotItem.Text) + Convert.ToDecimal(Main_DT.Rows[i]["TotalAmt"].ToString())).ToString();
                //}

                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                Grid_Total_Calculate();

                Grid_Total_Value();
                OtherCharge_Calc();

                btnSave.Text = "Update";
            }
            else
            {
                Clear_All_Field();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            Response.Redirect("Trans_Escrow_PurOrd_Main.aspx");
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Delivery_Mode_Add()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable Main_DT = new DataTable();
            txtDeliveryMode.Items.Clear();

            SSQL = "Select * from MstDeliveryMode where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " Order by DeliveryMode Asc";

            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            txtDeliveryMode.Items.Add("-Select-");
            for (int i = 0; i < Main_DT.Rows.Count; i++)
            {
                txtDeliveryMode.Items.Add(Main_DT.Rows[i]["DeliveryMode"].ToString());
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT_Check = new DataTable();
            bool ErrFlag = false;

            //User Rights Check Start
            bool Rights_Check = false;
            Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Escrow PO Approval");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approve Std Purchase Order...');", true);
            }
            //User Rights Check End

            SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('First You have to Register this Std Purchase Order Details..');", true);
            }

            SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "' And PO_Status='1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Std Purchase Order Details Already Approved..');", true);
            }

            if (!ErrFlag)
            {
                SSQL = "Update Trans_Escrow_PurOrd_Main set PO_Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "'";
                SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "Update Trans_Escrow_PurOrd_Approve set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Std Purchase Order Details Approved Successfully..');", true);
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void txtTax_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            txtTaxAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) * (Convert.ToDecimal(txtTax.Text) / 100)).ToString();

            txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
        }
        catch (Exception ex)
        {

        }
    }


    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        try
        {
            txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }


    protected void btnSearchView_Click(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            //Search Purchase Enquiry
            string SSQL = "";
            DataTable Main_DT = new DataTable();

            SSQL = "Select * from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Main_DT.Rows.Count != 0)
            {


                txtDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
                txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
                txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
                txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
                txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
                txtDeliveryMode.SelectedItem.Text = Main_DT.Rows[0]["DeliveryMode"].ToString();
                txtDeliveryMode.SelectedValue = Main_DT.Rows[0]["DeliveryModeCode"].ToString();
                txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
                txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
                txtPaymentMode.SelectedValue = Main_DT.Rows[0]["PaymentMode"].ToString();
                //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
                txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
                txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
                txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
                txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
                txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
                txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
                txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
                //txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
                txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
                //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
                txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
                txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
                txtRequestNo.Text = Main_DT.Rows[0]["Pur_Request_No"].ToString();
                txtRequestDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();

                if (Main_DT.Rows[0]["PO_Type"].ToString() == "Special")
                {
                    RdpPOType.SelectedValue = "3";
                }
                else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Direct")
                {
                    RdpPOType.SelectedValue = "1";
                }
                else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Req/Amend")
                {
                    RdpPOType.SelectedValue = "2";
                }


                //Trans_Coral_PurOrd_Sub Table Load
                DataTable dt = new DataTable();

                SSQL = "Select *from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";

                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                btnSave.Visible = false;
            }
            else
            {
                Clear_All_Field();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }


    private void Load_Data_Empty_Supp1()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();

            DT.Columns.Add("SuppCode");
            DT.Columns.Add("SuppName");

            SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And ";
            SSQL = SSQL + " Status='Add' And LedgerGrpName='Supplier'"; //and LedgerName='Enercon'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            txtSupplierName_Select.DataSource = DT;



            DataRow DR = DT.NewRow();

            DR["LedgerCode"] = "-Select-";
            DR["LedgerName"] = "-Select-";

            DT.Rows.InsertAt(DR, 0);


            //DataRow dr = DT.NewRow();

            //dr["LedgerCode"] = "-Select-";
            //dr["LedgerName"] = "-Select-";

            //DT.Rows.InsertAt(dr, 0);

            txtSupplierName_Select.DataTextField = "LedgerName";
            txtSupplierName_Select.DataValueField = "LedgerCode";
            txtSupplierName_Select.DataBind();


            //txtSupplierName_Select.SelectedValue = DT.Rows[0]["LedgerCode"].ToString();
            //txtSupplierName_Select.SelectedItem.Text = DT.Rows[0]["LedgerName"].ToString();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }




    protected void txtSupplierName_Select_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();

            if (txtSupplierName_Select.SelectedValue != "-Select-")
            {
                SSQL = "Select * from Acc_Mst_Ledger Where LedgerName='" + txtSupplierName_Select.SelectedItem.Text + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                txtSuppCodehide.Value = txtSupplierName_Select.SelectedValue;
                txtSupplierName.Text = txtSupplierName_Select.SelectedItem.Text;
                txtPaymentTerms.Text = DT.Rows[0]["TC"].ToString();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }


    protected void btnSuppRefDocNo_Click(object sender, EventArgs e)
    {
        //modalPop_SuppRefDocNo.Show();
    }

    private void Load_Data_Empty_SuppRefDocNo()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable Main_DT = new DataTable();

            SSQL = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And SuppCode='" + txtSuppCodehide.Value + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            txtSuppRefDocNo.DataSource = Main_DT;
            DataRow dr = Main_DT.NewRow();
            dr["QuotNo"] = "-Select-";
            //dr["DeptName"] = "-Select-";
            Main_DT.Rows.InsertAt(dr, 0);
            txtSuppRefDocNo.DataTextField = "QuotNo";
            //txtDeptCode.DataValueField = "DeptCode";
            txtSuppRefDocNo.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }

    protected void GridViewClick_SuppRefDocNo(object sender, CommandEventArgs e)
    {
        txtSuppRefDocNo.Text = Convert.ToString(e.CommandArgument);
        txtDocDate.Text = Convert.ToString(e.CommandName);
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }

    private void Load_Data_Empty_Dept()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable Main_DT = new DataTable();

            txtDepartmentName.Items.Clear();
            SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            txtDepartmentName.DataSource = Main_DT;
            DataRow dr = Main_DT.NewRow();
            dr["DeptCode"] = "-Select-";
            dr["DeptName"] = "-Select-";
            Main_DT.Rows.InsertAt(dr, 0);
            txtDepartmentName.DataTextField = "DeptName";
            txtDepartmentName.DataValueField = "DeptCode";
            txtDepartmentName.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        // txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        // txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    private void Load_Data_Empty_ItemCode()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            string Comp_Code = "";
            string Loc_Code = "";
            string Fin_Year_Code = "";
            string ss = "";
            string PO_Type = "";
            string Request_No = "";

            PO_Type = ddlPurMode.SelectedValue;

            Request_No = txtRequestNo.Text;
            ss = Request_No.Substring(0, 3);


            Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
            Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
            Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
            DataTable DT = new DataTable();
            DT.Columns.Add("ItemCode");
            DT.Columns.Add("ItemName");
            DT.Columns.Add("ReuiredQty");
            DT.Columns.Add("ReuiredDate");
            DT.Columns.Add("CalendarWeek");

            DataTable DT1 = new DataTable();
            DT1.Columns.Add("ItemCode");
            DT1.Columns.Add("ItemName");


            if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select ToolCode [ItemCode],ToolName [ItemName],ReuiredQty,ReuiredDate from Trans_Escrow_PurRqu_Tools_Sub ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "'  and  Lcode ='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' and Pur_Request_No='" + Request_No + "' ";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select ToolCode[ItemCode],ToolName [ItemName],ReuiredQty,ReuiredDate from Trans_Escrow_PurRqu_Asset_Sub ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "'  and  Lcode ='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' and Pur_Request_No='" + Request_No + "' ";
            }
            else
            {
                SSQL = "Select PRS.ItemCode,PRS.ItemName,PRS.ReuiredQty,PRS.ReuiredDate,PRS.CalendarWeek From Trans_Escrow_PurRqu_Sub PRS ";
                SSQL = SSQL + " Where PRS.Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " PRS.Lcode ='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "' And ";
                SSQL = SSQL + " PRS.Pur_Request_No='" + Request_No + "' order by cast(PRS.SAPNo as int),PRS.NoOfModel ";
            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            txtItemNameSelect.DataSource = DT;
            DataRow dr = DT.NewRow();
            dr["ItemCode"] = "-Select-";
            dr["ItemName"] = "-Select-";
            DT.Rows.InsertAt(dr, 0);
            txtItemNameSelect.DataTextField = "ItemName";
            txtItemNameSelect.DataValueField = "ItemCode";
            txtItemNameSelect.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        try
        {
            txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
            txtItemName.Text = Convert.ToString(e.CommandName);
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }
    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            if (txtItemNameSelect.SelectedValue != "-Select-")
            {
                txtItemCodeHide.Value = txtItemNameSelect.SelectedValue;
                txtItemName.Text = txtItemNameSelect.SelectedItem.Text;

                string SSQL = "";
                string Comp_Code = "";
                string Loc_Code = "";
                string Fin_Year_Code = "";
                string ss = "";
                string PO_Type = "";
                string Request_No = "";
                DataTable DT = new DataTable();


                PO_Type = RdpPOType.SelectedValue;
                Request_No = txtRequestNo.SelectedValue;


                if (ddlPurMode.SelectedItem.Text == "Purchase Request")
                {

                    if (ddlMatType.SelectedItem.Text == "Tools")
                    {
                        SSQL = "Select ReuiredQty,NoOfModel,SumRquQty,ReuiredDate,Item_Rate,ItemRate_Other,SapNo,CalendarWeek from Trans_Escrow_PurRqu_Tools_Sub ";
                        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And ToolCode='" + txtItemCodeHide.Value + "'";
                        SSQL = SSQL + "  and  Lcode ='" + SessionLcode + "' and Pur_Request_No='" + Request_No + "' ";
                    }
                    else if (ddlMatType.SelectedItem.Text == "Asset")
                    {
                        SSQL = "Select ReuiredQty,NoOfModel,SumRquQty,ReuiredDate,Item_Rate,ItemRate_Other,SapNo,CalendarWeek from Trans_Escrow_PurRqu_Asset_Sub ";
                        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And ToolCode='" + txtItemCodeHide.Value + "'";
                        SSQL = SSQL + "  and  Lcode ='" + SessionLcode + "' and Pur_Request_No='" + Request_No + "' ";
                    }
                    else
                    {
                        SSQL = "Select PRS.ReuiredQty,PRS.NoOfModel,PRS.SumRquQty,PRS.ReuiredDate,PRS.Item_Rate,PRS.ItemRate_Other ,PRS.SapNo, ";
                        SSQL = SSQL + " CalendarWeek From Trans_Escrow_PurRqu_Sub PRS ";
                        SSQL = SSQL + " where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " PRS.FinYearCode ='" + SessionFinYearCode + "' And PRS.CalendarWeek='" + txtCalWeek.Text + "' ";
                        SSQL = SSQL + " And PRS.Pur_Request_No='" + Request_No + "' And PRS.ItemCode='" + txtItemCodeHide.Value + "'";
                    }
                    DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT.Rows.Count != 0)
                    {
                        SAPNo = "";
                        SAPNo = DT.Rows[0]["SapNo"].ToString();
                        txtRequiredQty.Text = DT.Rows[0]["ReuiredQty"].ToString();
                        txtRequiredDate.Text = DT.Rows[0]["ReuiredDate"].ToString();
                        txtModelQty.Text = DT.Rows[0]["NoOfModel"].ToString();
                        txtOrderQty.Text = DT.Rows[0]["SumRquQty"].ToString();

                        //if (Convert.ToDecimal(DT.Rows[0]["Item_Rate"].ToString()) > 0)
                        //{
                        txtItemRate.Text = Math.Round(Convert.ToDecimal(DT.Rows[0]["Item_Rate"].ToString()), 4, MidpointRounding.AwayFromZero).ToString();
                        //}
                        //else
                        //{
                        //    txtItemRate.Text = DT.Rows[0]["ItemRate_Other"].ToString();
                        //}


                        txtCalWeek.Text = DT.Rows[0]["CalendarWeek"].ToString();
                    }
                }
                else
                {
                    SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status='Add' ";
                    SSQL = SSQL + " And Raw_Mat_Name='" + txtItemNameSelect.SelectedItem.Text + "' ";

                    DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT.Rows.Count != 0)
                    {
                        txtRequiredQty.Text = DT.Rows[0]["RequiredQty"].ToString();


                        if (Convert.ToDecimal(DT.Rows[0]["Amount"].ToString()) > 0)
                        {
                            txtItemRate.Text = DT.Rows[0]["Amount"].ToString();
                        }
                        else
                        {
                            txtItemRate.Text = DT.Rows[0]["AmtOther"].ToString();
                        }


                    }
                }

                //Get TAX Percentage
                string CGST_Per = "0";
                string SGST_Per = "0";
                string IGST_Per = "0";
                string VAT_Per = "0";
                string UOMName = "";

                string ItemRate = "0.00";


                if (ddlMatType.SelectedItem.Text == "Tools")
                {
                    SSQL = "Select *,UOM[UOMCode] from Trans_Escrow_PurRqu_Tools_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + Request_No + "' ";
                    SSQL = SSQL + " And ToolCode='" + txtItemCodeHide.Value + "'";
                }
                else if (ddlMatType.SelectedItem.Text == "Asset")
                {
                    SSQL = "Select *,UOM[UOMCode] from Trans_Escrow_PurRqu_Asset_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + Request_No + "' ";
                    SSQL = SSQL + " And ToolCode='" + txtItemCodeHide.Value + "'";
                }
                else
                {
                    SSQL = "Select * from Trans_Escrow_PurRqu_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And Pur_Request_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "' and CalendarWeek='" + txtCalWeek.Text + "'";
                }
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {

                    if (ddlTaxType.SelectedItem.Text == "-Select-")
                    {
                        CGST_Per = DT.Rows[0]["CGSTP"].ToString();
                        SGST_Per = DT.Rows[0]["SGSTP"].ToString();
                        IGST_Per = DT.Rows[0]["IGSTP"].ToString();
                        VAT_Per = DT.Rows[0]["VATP"].ToString();
                        UOMName = DT.Rows[0]["UOMCode"].ToString();

                        if (Convert.ToDecimal(DT.Rows[0]["Item_Rate"].ToString()) > 0)
                        {
                            ItemRate = DT.Rows[0]["Item_Rate"].ToString();
                        }
                        else
                        {
                            ItemRate = DT.Rows[0]["ItemRate_Other"].ToString();
                        }



                    }
                    else
                    {
                        DataTable dtTax = new DataTable();

                        SSQL = "Select * from GstMaster where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
                        SSQL = SSQL + " GST_Type='" + ddlTaxType.SelectedValue + "' ";

                        dtTax = objdata.RptEmployeeMultipleDetails(SSQL);

                        hfGSTDescr.Value = dtTax.Rows[0]["Description"].ToString();

                        CGST_Per = dtTax.Rows[0]["CGST_Per"].ToString();
                        SGST_Per = dtTax.Rows[0]["SGST_Per"].ToString();
                        IGST_Per = dtTax.Rows[0]["IGST_Per"].ToString();
                        VAT_Per = "0.00";

                        UOMName = DT.Rows[0]["UOMCode"].ToString();


                        if (Convert.ToDecimal(DT.Rows[0]["Item_Rate"].ToString()) > 0)
                        {
                            ItemRate = DT.Rows[0]["Item_Rate"].ToString();
                        }
                        else
                        {
                            ItemRate = DT.Rows[0]["ItemRate_Other"].ToString();
                        }

                        //ItemRate_INR = DT.Rows[0]["Item_Rate"].ToString();
                        //ItemRate_EUR = DT.Rows[0]["ItemRate_Other"].ToString();

                    }

                    //if (ddlTaxType.SelectedValue == "1") { IGST_Per = "0";VAT_Per = "0"; }
                    //if (ddlTaxType.SelectedValue == "2") { CGST_Per = "0"; SGST_Per = "0"; VAT_Per = "0"; }
                    //if (ddlTaxType.SelectedValue == "3") { CGST_Per = "0"; SGST_Per = "0"; IGST_Per = "0"; }
                    //if (ddlTaxType.SelectedValue == "4") { CGST_Per = "0"; SGST_Per = "0"; IGST_Per = "0"; VAT_Per = "0"; }

                    //if (ddlCurrencyType.SelectedValue == "1") { ItemRate_EUR = "0"; }
                    //if (ddlCurrencyType.SelectedValue != "1") { ItemRate_INR = "0"; }

                    txtCGSTPer.Text = CGST_Per;
                    txtSGSTPer.Text = SGST_Per;
                    txtIGSTPer.Text = IGST_Per;
                    txtVAT_Per.Text = VAT_Per;
                    hfUOM.Value = UOMName;

                    txtItemRate.Text = ItemRate;


                    TotalGstPer_Hidden.Value = (Convert.ToDecimal(txtCGSTPer.Text) + Convert.ToDecimal(txtSGSTPer.Text) + Convert.ToDecimal(txtIGSTPer.Text)).ToString();
                    TotalGstPer_Hidden.Value = (Convert.ToDecimal(TotalGstPer_Hidden.Value) + Convert.ToDecimal(txtVAT_Per.Text)).ToString();
                    Total_Calculate();
                }
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    protected void GridViewClick_ItemCode_PR(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            string Comp_Code = "";
            string Loc_Code = "";
            string Fin_Year_Code = "";
            string ss = "";
            string PO_Type = "";
            string Request_No = "";
            DataTable DT = new DataTable();


            txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
            txtItemName.Text = Convert.ToString(e.CommandName);


            PO_Type = RdpPOType.SelectedValue;
            Request_No = txtRequestNo.Text;

            if (PO_Type == "2")
            {
                ss = Request_No.Substring(0, 3);
            }

            if (PO_Type == "2")
            {
                if (ss != "PRA")
                {
                    SSQL = "Select ReuiredQty,ReuiredDate from Trans_Escrow_PurOrd_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And Pur_Request_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                    DT = objdata.RptEmployeeMultipleDetails(SSQL);
                }
                else
                {
                    SSQL = "Select ReuiredQty,ReuiredDate from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And Amend_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                    DT = objdata.RptEmployeeMultipleDetails(SSQL);
                }

            }

            txtRequiredQty.Text = DT.Rows[0]["ReuiredQty"].ToString();
            txtRequiredDate.Text = DT.Rows[0]["ReuiredDate"].ToString();
            txtOrderQty.Text = DT.Rows[0]["ReuiredQty"].ToString();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }
    protected void RdpPOType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (RdpPOType.SelectedValue == "2") { txtRequestNo.Enabled = true; }
            Load_Data_Empty_ItemCode();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void txtSuppRefDocNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable Main_DT = new DataTable();
            //txtDeptName.Items.Clear();
            if (txtSuppRefDocNo.Text != "" && txtSuppRefDocNo.Text != "-Select-")
            {
                SSQL = "Select QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And QuotNo='" + txtSuppRefDocNo.Text + "'";
                Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < Main_DT.Rows.Count; i++)
                {
                    txtDocDate.Text = Main_DT.Rows[i]["QuotDate"].ToString();
                    //txtDeptName.Text = Main_DT.Rows[i]["DeptName"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    protected void txtDiscount_Per_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    protected void txtOtherCharge_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    private void Total_Calculate()
    {
        try
        {
            lblErrorMsg.Text = "";
            string Qty_Val = "0";
            string ItemRate = "0";

            string Item_Total = "0";
            string Discount_Percent = "0";
            string Discount_Amt = "0";
            string VAT_Per = "0";
            string VAT_Amt = "0";
            string Tax_Amt = "0";
            string CGST_Per = "0";
            string CGST_Amt = "0";
            string SGST_Per = "0";
            string SGST_Amt = "0";
            string IGST_Per = "0";
            string IGST_Amt = "0";
            string BDUTax_Per = "0";
            string BDUTax_Amt = "0";
            string Other_Charges = "0";
            string Final_Amount = "0";
            string PackingAmt = "0";

            if (txtOrderQty.Text != "") { Qty_Val = txtOrderQty.Text.ToString(); }
            if (txtItemRate.Text != "0") { ItemRate = txtItemRate.Text.ToString(); }


            if (ItemRate != "0")
            {
                Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(ItemRate)).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 4, MidpointRounding.AwayFromZero)).ToString();
            }

            if (Convert.ToDecimal(Item_Total) != 0)
            {
                if (Convert.ToDecimal(txtDiscount_Per.Text.ToString()) != 0) { Discount_Percent = txtDiscount_Per.Text.ToString(); }
                if (Convert.ToDecimal(txtOtherCharge.Text.ToString()) != 0) { Other_Charges = txtOtherCharge.Text.ToString(); }
                if (Convert.ToDecimal(txtCGSTPer.Text.ToString()) != 0) { CGST_Per = txtCGSTPer.Text.ToString(); }
                if (Convert.ToDecimal(txtSGSTPer.Text.ToString()) != 0) { SGST_Per = txtSGSTPer.Text.ToString(); }
                if (Convert.ToDecimal(txtIGSTPer.Text.ToString()) != 0) { IGST_Per = txtIGSTPer.Text.ToString(); }

                if (Convert.ToDecimal(txtVAT_Per.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                //Discount Amt Calculate
                if (Convert.ToDecimal(Discount_Percent) != 0)
                {
                    Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                    Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                    Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                }
                else
                {
                    Discount_Amt = "0.00";
                }

                if (Convert.ToDecimal(CGST_Per) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                    CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                    CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                }
                else
                {
                    CGST_Amt = "0.00";
                }

                if (Convert.ToDecimal(VAT_Per) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                    VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                    VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                }
                else
                {
                    VAT_Amt = "0.00";
                }

                if (Convert.ToDecimal(SGST_Per) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                    SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                    SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                }
                else
                {
                    SGST_Amt = "0.00";
                }

                if (Convert.ToDecimal(IGST_Per) != 0)
                {
                    string Item_Discount_Amt = "0.00";
                    Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                    IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                    IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                }
                else
                {
                    IGST_Amt = "0.00";
                }

                //Other Charges
                if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                //Final Amt
                Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 4, MidpointRounding.AwayFromZero)).ToString();

                txtItemTotal.Text = Item_Total;
                txtDiscount_Amount.Text = Discount_Amt;
                txtTaxAmt.Text = Tax_Amt;
                txtCGSTAmt.Text = CGST_Amt;
                txtSGSTAmt.Text = SGST_Amt;
                txtIGSTAmt.Text = IGST_Amt;
                txtVAT_AMT.Text = VAT_Amt;
                txtFinal_Total_Amt.Text = Final_Amount;
                txtNetAmt.Text = Final_Amount;
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    private void Final_Total_Calculate()
    {
        try
        {
            lblErrorMsg.Text = "";
            string Final_NetAmt = "0";
            string AddorLess = "0";
            string Item_Total_Amt = txtTotAmt.Text;

            string[] ROff = Item_Total_Amt.Split('.');


            if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0)
            {
                AddorLess = txtAddOrLess.Text.ToString();
            }

            if (Convert.ToDecimal(ROff[1].ToString()) < Convert.ToDecimal("49"))
            {
                Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) - Convert.ToDecimal(AddorLess)).ToString();
            }
            else if (Convert.ToDecimal(ROff[1].ToString()) > Convert.ToDecimal("49"))
            {
                Final_NetAmt = (Convert.ToDecimal(Item_Total_Amt) + Convert.ToDecimal(AddorLess)).ToString();
            }
            txtNetAmt.Text = Final_NetAmt;
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void txtAddOrLess_TextChanged(object sender, EventArgs e)
    {
        if (txtAddOrLess.Text.ToString() != "") { Final_Total_Calculate(); }
    }

    protected void txtOrderQty_TextChanged(object sender, EventArgs e)
    {
        if (txtOrderQty.Text.ToString() != "") { Total_Calculate(); }
    }

    protected void txtRateINR_TextChanged(object sender, EventArgs e)
    {
        if (txtItemRate.Text.ToString() != "") { Total_Calculate(); }
    }

    protected void txtRequestNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            Initial_Data_Referesh();

            if (ddlTaxType.SelectedItem.Text != "-Select-")
            {

                if (txtRequestNo.SelectedValue != "-Select-")
                {
                    string SSQL = "";
                    DataTable DT_DA = new DataTable();
                    SSQL = "Select * from Trans_Escrow_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " Pur_Request_No ='" + txtRequestNo.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
                    DT_DA = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT_DA.Rows.Count != 0)
                    {
                        txtRequestDate.Text = DT_DA.Rows[0]["Pur_Request_Date"].ToString();
                        //txtDepartmentName.SelectedValue = DT_DA.Rows[0]["DeptCode"].ToString();
                        //hfDeptCode.Value = DT_DA.Rows[0]["DeptCode"].ToString();
                        //txtDeliveryDate.Text = DT_DA.Rows[0]["DeptName"].ToString();
                    }
                }
                Load_Data_Empty_ItemCode();
                All_Item_Added_DataTable(sender, e);

                Grid_Total_Calculate();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Any One Tax Type');", true);
                txtRequestNo.SelectedIndex = 0;
            }
        }

        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnPO_Preview_Click(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string RptName = "";
            string StdPurOrdNo = "";
            string SupQtnNo = "";
            string DeptName = "";

            RptName = "Standard Purchase Order Details Invoice Format";
            StdPurOrdNo = txtStdOrderNo.Text.ToString();

            ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void All_Item_Added_DataTable(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();

            if (ddlMatType.SelectedItem.Text == "Tools")
            {
                SSQL = "Select SAPNo,ToolCode[ItemCode],ToolName[ItemName],NoOfModel,ReuiredQty,ReuiredDate,CalendarWeek ";
                SSQL = SSQL + " from Trans_Escrow_PurRqu_Tools_Sub Where ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And Pur_Request_No='" + txtRequestNo.SelectedValue + "'";
            }
            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                SSQL = "Select SAPNo,ToolCode[ItemCode],ToolName[ItemName],NoOfModel,ReuiredQty,ReuiredDate,CalendarWeek ";
                SSQL = SSQL + " from Trans_Escrow_PurRqu_Asset_Sub Where ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And Pur_Request_No='" + txtRequestNo.SelectedValue + "'";
            }
            else
            {
                SSQL = "Select PRS.SAPNo,PRS.ItemCode,PRS.ItemName,PRS.NoOfModel,PRS.ReuiredQty,PRS.ReuiredDate,PRS.CalendarWeek ";
                SSQL = SSQL + " From Trans_Escrow_PurRqu_Sub PRS ";

                SSQL = SSQL + " Where PRS.Ccode='" + SessionCcode + "' And ";
                SSQL = SSQL + " PRS.Lcode ='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And PRS.Pur_Request_No='" + txtRequestNo.SelectedValue + "' ";

            }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data_Empty_ItemCode();

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                txtCalWeek.Text = DT.Rows[i]["CalendarWeek"].ToString();
                txtItemNameSelect.SelectedValue = DT.Rows[i]["ItemCode"].ToString();
                txtItemNameSelect_SelectedIndexChanged(sender, e);

                btnAddItem_Click(sender, e);
            }
            txtItemNameSelect.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void ddlPurMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            if (ddlPurMode.SelectedItem.Text == "Purchase Request")
            {
                pnlPurRqu.Visible = true;
                pnlDirectOrd.Visible = false;
            }
            else
            {
                pnlPurRqu.Visible = false;
                pnlDirectOrd.Visible = true;
                Load_Data_ItemMas();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Load_Data_ItemMas()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();

            SSQL = "select Mat_No,Raw_Mat_Name from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            txtItemNameSelect.DataSource = DT;

            DataRow DR = DT.NewRow();

            DR["Mat_No"] = "-Select-";
            DR["Raw_Mat_Name"] = "-Select-";

            DT.Rows.InsertAt(DR, 0);

            txtItemNameSelect.DataTextField = "Raw_Mat_Name";
            txtItemNameSelect.DataValueField = "Mat_No";
            txtItemNameSelect.DataBind();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void txtGrdDisc_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            if (Convert.ToDecimal(Repeater1.Items.Count) == Convert.ToDecimal(index + 1))
            {
                ((TextBox)Repeater1.Items[index].FindControl("txtOrdQty")).Focus();
            }
            else
            {
                ((TextBox)Repeater1.Items[index + 1].FindControl("txtOrdQty")).Focus();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void txtGrdRateEUR_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);

            ((TextBox)Repeater1.Items[index].FindControl("txtGrdDiscPre")).Focus();

        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Text_Change_Value_Cal(object sender, int Row_No)
    {
        try
        {
            string Qty_Val = "0";
            string Item_Rate = "0";
            string Item_Total = "0";
            string Discount_Percent = "0";
            string Discount_Amt = "0";
            string VAT_Per = "0";
            string VAT_Amt = "0";
            string Tax_Amt = "0";
            string CGST_Per = "0";
            string CGST_Amt = "0";
            string SGST_Per = "0";
            string SGST_Amt = "0";
            string IGST_Per = "0";
            string IGST_Amt = "0";
            string BDUTax_Per = "0";
            string BDUTax_Amt = "0";
            string Other_Charges = "0";
            string Final_Amount = "0";
            string PackingAmt = "0";

            //string Item_Discount_Amt = "0.00";

            int i = Row_No;


            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];

            TextBox txtGrdOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;

            if (txtGrdOrdQty.Text.ToString() != "")
            {
                TextBox txtTest = ((TextBox)(sender));
                RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

                Label lblSAPNo = Repeater1.Items[i].FindControl("lblSAPNo") as Label;
                Label lblItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                Label lblItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                Label lblUOM = Repeater1.Items[i].FindControl("lblUOM") as Label;
                Label lblBOMQty = Repeater1.Items[i].FindControl("lblBOMQty") as Label;
                Label lblMdlQty = Repeater1.Items[i].FindControl("lblMdlQty") as Label;
                Label lblReqQty = Repeater1.Items[i].FindControl("lblRquQty") as Label;
                TextBox txtOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;
                TextBox txtRquDate = Repeater1.Items[i].FindControl("txtRDate") as TextBox;
                TextBox txtGrdRateINR = Repeater1.Items[i].FindControl("txtGrdRateINR") as TextBox;
                //TextBox txtGrdRateEUR = Repeater1.Items[i].FindControl("txtGrdRateEUR") as TextBox;
                Label lblRemarks = Repeater1.Items[i].FindControl("lblRemarks") as Label;
                Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                TextBox lblDiscPer = Repeater1.Items[i].FindControl("txtGrdDiscPre") as TextBox;
                Label lblDisc = Repeater1.Items[i].FindControl("lblDisc") as Label;
                Label lblTaxPer = Repeater1.Items[i].FindControl("lblTaxPer") as Label;
                Label lblTaxAmt = Repeater1.Items[i].FindControl("lblTaxAmt") as Label;
                Label lblGSTPer = Repeater1.Items[i].FindControl("lblGSTPer") as Label;
                Label lblCGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                Label lblCGSTAmt = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                Label lblSGSTAmt = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                Label lblIGSTAmt = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblOtherAmt = Repeater1.Items[i].FindControl("lblOtherAmt") as Label;
                Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;
                Label lblCalWeek = Repeater1.Items[i].FindControl("lblCalWeek") as Label;

                //txtGrdRateEUR.ReadOnly = true;

                if (txtOrdQty.Text != "") { Qty_Val = txtOrdQty.Text.ToString(); }

                //if (ddlCurrencyType.SelectedItem.Text == "INR")
                //{
                if (txtGrdRateINR.Text != "") { Item_Rate = txtGrdRateINR.Text.ToString(); }
                //}

                Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 4, MidpointRounding.AwayFromZero)).ToString();

                //if (txtDiscount.Text != "") { Discount_Percent = txtDiscount.Text.ToString(); }


                if (Convert.ToDecimal(Item_Total) != 0)
                {
                    if (Convert.ToDecimal(lblDiscPer.Text.ToString()) != 0) { Discount_Percent = lblDiscPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblOtherAmt.Text.ToString()) != 0) { Other_Charges = lblOtherAmt.Text.ToString(); }
                    if (Convert.ToDecimal(lblCGSTPer.Text.ToString()) != 0) { CGST_Per = lblCGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblSGSTPer.Text.ToString()) != 0) { SGST_Per = lblSGSTPer.Text.ToString(); }
                    if (Convert.ToDecimal(lblIGSTPer.Text.ToString()) != 0) { IGST_Per = lblIGSTPer.Text.ToString(); }

                    //if (Convert.ToDecimal(lbl.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

                    //Discount Amt Calculate
                    if (Convert.ToDecimal(Discount_Percent) != 0)
                    {
                        Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                        Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                        Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        Discount_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(CGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                        CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                        CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        CGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(VAT_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                        VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                        VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        VAT_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(SGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                        SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                        SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else
                    {
                        SGST_Amt = "0.00";
                    }

                    if (Convert.ToDecimal(IGST_Per) != 0)
                    {
                        string Item_Discount_Amt = "0.00";
                        Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                        IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                        IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                        IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    }
                    else
                    {
                        IGST_Amt = "0.00";
                    }

                    //Other Charges
                    if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

                    //Final Amt
                    Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
                    //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                    Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
                    Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 4, MidpointRounding.AwayFromZero)).ToString();

                    lblItemTot.Text = Item_Total;

                    lblDisc.Text = Discount_Amt;
                    lblTaxAmt.Text = Tax_Amt;
                    lblCGSTAmt.Text = CGST_Amt;
                    lblSGSTAmt.Text = SGST_Amt;
                    lblIGSTAmt.Text = IGST_Amt;
                    //txtVAT_AMT.Text = VAT_Amt;
                    lblLineTot.Text = Final_Amount;
                    txtTotAmt.Text = Final_Amount;
                    txtNetAmt.Text = Final_Amount;
                }
                dt.Rows[i]["SAPNo"] = lblSAPNo.Text;
                dt.Rows[i]["ItemCode"] = lblItemCode.Text;
                dt.Rows[i]["ItemName"] = lblItemName.Text;
                dt.Rows[i]["UOMCode"] = lblUOM.Text;
                dt.Rows[i]["ReuiredQty"] = Math.Round(Convert.ToDecimal(lblBOMQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["ModelQty"] = Math.Round(Convert.ToDecimal(lblMdlQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["RquQty"] = Math.Round(Convert.ToDecimal(lblReqQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["OrderQty"] = Math.Round(Convert.ToDecimal(txtOrdQty.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["ReuiredDate"] = txtRquDate.Text;
                dt.Rows[i]["ItemRate"] = Math.Round(Convert.ToDecimal(txtGrdRateINR.Text), 4, MidpointRounding.AwayFromZero);

                dt.Rows[i]["Remarks"] = lblRemarks.Text;
                dt.Rows[i]["ItemTotal"] = Math.Round(Convert.ToDecimal(lblItemTot.Text), 4, MidpointRounding.AwayFromZero);
                dt.Rows[i]["Discount_Per"] = Math.Round(Convert.ToDecimal(lblDiscPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["Discount"] = Math.Round(Convert.ToDecimal(lblDisc.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["TaxPer"] = Math.Round(Convert.ToDecimal(lblTaxPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["TaxAmount"] = Math.Round(Convert.ToDecimal(lblTaxAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["GstPer"] = Math.Round(Convert.ToDecimal(lblGSTPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["CGSTPer"] = Math.Round(Convert.ToDecimal(lblCGSTPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["CGSTAmount"] = Math.Round(Convert.ToDecimal(lblCGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["SGSTPer"] = Math.Round(Convert.ToDecimal(lblSGSTPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["SGSTAmount"] = Math.Round(Convert.ToDecimal(lblSGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["IGSTPer"] = Math.Round(Convert.ToDecimal(lblIGSTPer.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["IGSTAmount"] = Math.Round(Convert.ToDecimal(lblIGSTAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["OtherCharge"] = Math.Round(Convert.ToDecimal(lblOtherAmt.Text), 2, MidpointRounding.AwayFromZero);
                dt.Rows[i]["LineTotal"] = Math.Round(Convert.ToDecimal(lblLineTot.Text), 4, MidpointRounding.AwayFromZero);
                dt.Rows[i]["CalendarWeek"] = lblCalWeek.Text;
                Grid_Total_Calculate();
                Final_Total_Calculate();
            }

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtGrdRateINR_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;
            Text_Change_Value_Cal(sender, index);
            ((TextBox)Repeater1.Items[index].FindControl("txtGrdRateEUR")).Focus();
            //(TextBox)Repeater1.Items[i].FindControl("lblDiscPer").Focus();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }
    protected void txtOrdQty_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            TextBox button = (sender as TextBox);
            RepeaterItem item = button.NamingContainer as RepeaterItem;
            int index = item.ItemIndex;

            Text_Change_Value_Cal(sender, index);
            ((TextBox)Repeater1.Items[index].FindControl("txtGrdRateINR")).Focus();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Grid_Total_Calculate()
    {
        try
        {
            lblErrorMsg.Text = "";
            decimal SumLineTot = 0;
            decimal SumQtyTot = 0;
            decimal SumItemTot = 0;
            decimal SumCGSTTot = 0;
            decimal SumSGSTTot = 0;
            decimal SumIGSTTot = 0;
            decimal SumDiscTot = 0;

            for (int i = 0; i < Repeater1.Items.Count; i++)
            {
                Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;
                TextBox txtOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;
                Label lblCGSTTot = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTTot = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTTot = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblITMTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                Label lblDisc = Repeater1.Items[i].FindControl("lblDisc") as Label;

                SumItemTot = SumItemTot + Convert.ToDecimal(lblITMTot.Text);
                SumCGSTTot = SumCGSTTot + Convert.ToDecimal(lblCGSTTot.Text);
                SumSGSTTot = SumSGSTTot + Convert.ToDecimal(lblSGSTTot.Text);
                SumIGSTTot = SumIGSTTot + Convert.ToDecimal(lblIGSTTot.Text);
                SumLineTot = SumLineTot + Convert.ToDecimal(LineTotal.Text);
                SumQtyTot = SumQtyTot + Convert.ToDecimal(txtOrdQty.Text);
                SumDiscTot = SumDiscTot + Convert.ToDecimal(lblDisc.Text);
            }

            lblTotItem.Text = SumItemTot.ToString();
            lblTotDisc.Text = SumDiscTot.ToString();

            lblTotCGST.Text = SumCGSTTot.ToString();
            lblTotSGST.Text = SumSGSTTot.ToString();
            lblTotIGST.Text = SumIGSTTot.ToString();

            txtTotAmt.Text = SumLineTot.ToString();
            txtTotQty.Text = SumQtyTot.ToString();
            txtNetAmt.Text = (SumLineTot + Convert.ToDecimal(txtAddOrLess.Text)).ToString();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }
    private void Grid_Final_Total_Calculate()
    {
        try
        {
            string Final_NetAmt = "0";
            string AddorLess = "0";
            string Item_Total_Amt = txtTotAmt.Text;

            if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0) { AddorLess = txtAddOrLess.Text.ToString(); }
            Final_NetAmt = (Convert.ToDecimal(AddorLess) + Convert.ToDecimal(Item_Total_Amt)).ToString();
            //txtFinal_Total_Amt.Text = Final_NetAmt;
            txtNetAmt.Text = Final_NetAmt;
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void ddlMatType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string Auto_Transaction_No = "";
            TransactionNoGenerate TransNO = new TransactionNoGenerate();

            if (ddlMatType.SelectedItem.Text == "Tools")
            {
                Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Tools Purchase Order", SessionFinYearVal, "1");
            }

            else if (ddlMatType.SelectedItem.Text == "Asset")
            {
                Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Asset Purchase Order", SessionFinYearVal, "1");
            }

            else
            {
                Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Escrow Purchase Order", SessionFinYearVal, "1");
            }

            //txtStdOrderNo.Text = Auto_Transaction_No;


            Load_Data_Purchase_Request_No();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void ddlCurrencyType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();

            if (ddlCurrencyType.SelectedItem.Text != "INR")
            {
                SSQL = "Select * from GstMaster where GST_Type='NONE'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                ddlTaxType.SelectedValue = DT.Rows[0]["GST_Type"].ToString();
                hfGSTDescr.Value = DT.Rows[0]["Description"].ToString();
            }
            else
            {
                ddlTaxType.SelectedValue = "-Select-";

            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void txtDeliveryMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "Select * from MstDeliveryMode where DeliveryMode='" + txtDeliveryMode.SelectedItem.Text + "'";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfDeliMode.Value = DT.Rows[0]["DeliveryCode"].ToString();
    }

    protected void txtDepartmentName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "Select * from MstDepartment where DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
    }

    private void Reset_INR_To_Euro_Value()
    {
        try
        {
            lblErrorMsg.Text = "";
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["RateEUR"] = dt.Rows[i]["RateINR"];

                dt.Rows[i]["GstPer"] = "0";
                dt.Rows[i]["CGSTPer"] = "0";
                dt.Rows[i]["CGSTAmount"] = "0.00";
                dt.Rows[i]["SGSTPer"] = "0";
                dt.Rows[i]["SGSTAmount"] = "0.00";
                dt.Rows[i]["IGSTPer"] = "0";
                dt.Rows[i]["IGSTAmount"] = "0.00";

                string Order_Qty_Str = dt.Rows[i]["OrderQty"].ToString();
                string Rate_Val_Str = dt.Rows[i]["RateEUR"].ToString();
                string Item_Total = "0";

                Item_Total = (Convert.ToDecimal(Order_Qty_Str) * Convert.ToDecimal(Rate_Val_Str)).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();

                dt.Rows[i]["LineTotal"] = Item_Total;
                dt.Rows[i]["ItemTotal"] = Item_Total;
                dt.Rows[i]["ItemRate"] = "0.00";
            }

            Totalsum();
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            Grid_Total_Calculate();
        }

        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Reset_Euro_To_INR_Value()
    {
        try
        {
            lblErrorMsg.Text = "";
            DataTable dt = new DataTable();
            DataTable DT_Com = new DataTable();
            string query = "";
            dt = (DataTable)ViewState["ItemTable"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["RateINR"] = dt.Rows[i]["RateEUR"];

                    //Get Tax Percentage()
                    string CGST_Per = "0";
                    string SGST_Per = "0";
                    string IGST_Per = "0";
                    string Order_Qty_Str = dt.Rows[i]["OrderQty"].ToString();
                    string Rate_Val_Str = dt.Rows[i]["RateINR"].ToString();
                    string Item_Total = "0";
                    string CGST_Amt = "0";
                    string SGST_Amt = "0";
                    string IGST_Amt = "0";
                    string line_Total_Str = "0";

                    Item_Total = (Convert.ToDecimal(Order_Qty_Str) * Convert.ToDecimal(Rate_Val_Str)).ToString();
                    Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();

                    query = "Select * from BOMMaster where Mat_No='" + dt.Rows[i]["ItemCode"].ToString() + "' And ";
                    query = query + " Ccode = '" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    DT_Com = objdata.RptEmployeeMultipleDetails(query);
                    if (DT_Com.Rows.Count != 0)
                    {
                        CGST_Per = DT_Com.Rows[0]["CGSTP"].ToString();
                        SGST_Per = DT_Com.Rows[0]["SGSTP"].ToString();
                        IGST_Per = DT_Com.Rows[0]["IGSTP"].ToString();
                    }

                    if (hfGSTDescr.Value == "GST")
                    {
                        IGST_Per = "0";
                    }
                    if (hfGSTDescr.Value == "IGST")
                    {
                        CGST_Per = "0";
                        SGST_Per = "0";
                    }
                    if (hfGSTDescr.Value == "VAT" || hfGSTDescr.Value == "NONE")
                    {
                        CGST_Per = "0";
                        SGST_Per = "0";
                        IGST_Per = "0";
                    }

                    //Tax Calculate
                    CGST_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(CGST_Per)).ToString();
                    CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                    CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    SGST_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(SGST_Per)).ToString();
                    SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                    SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    IGST_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(IGST_Per)).ToString();
                    IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                    IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                    line_Total_Str = (Convert.ToDecimal(Item_Total) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                    line_Total_Str = (Math.Round(Convert.ToDecimal(line_Total_Str), 2, MidpointRounding.AwayFromZero)).ToString();



                    dt.Rows[i]["CGSTPer"] = CGST_Per;
                    dt.Rows[i]["CGSTAmount"] = CGST_Amt;
                    dt.Rows[i]["SGSTPer"] = SGST_Per;
                    dt.Rows[i]["SGSTAmount"] = SGST_Amt;
                    dt.Rows[i]["IGSTPer"] = IGST_Per;
                    dt.Rows[i]["IGSTAmount"] = IGST_Amt;
                    dt.Rows[i]["LineTotal"] = line_Total_Str;
                    dt.Rows[i]["ItemTotal"] = Item_Total;
                    dt.Rows[i]["ItemRate"] = "0";

                }

                Totalsum();
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                Grid_Total_Calculate();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    private void Reset_Tax_Change_Mode_Value()
    {
        try
        {
            lblErrorMsg.Text = "";
            DataTable dt = new DataTable();
            DataTable DT_Com = new DataTable();
            string SSQL = "";

            dt = (DataTable)ViewState["ItemTable"];

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                //Get Tax Percentage()
                string CGST_Per = "0";
                string SGST_Per = "0";
                string IGST_Per = "0";
                string Order_Qty_Str = dt.Rows[i]["OrderQty"].ToString();
                string Rate_Val_Str = "0";
                string GSTPer = "0.00";

                string Item_Total = "0";
                string CGST_Amt = "0";
                string SGST_Amt = "0";
                string IGST_Amt = "0";
                string line_Total_Str = "0";


                Rate_Val_Str = dt.Rows[i]["ItemRate"].ToString();

                SSQL = "Select * from GstMaster where GST_Type='" + ddlTaxType.SelectedItem.Text + "' And Ccode='" + SessionCcode + "' ";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "'";

                DT_Com = objdata.RptEmployeeMultipleDetails(SSQL);

                hfGSTDescr.Value = DT_Com.Rows[0]["Description"].ToString();



                Item_Total = (Convert.ToDecimal(Order_Qty_Str) * Convert.ToDecimal(Rate_Val_Str)).ToString();
                Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();



                if (DT_Com.Rows.Count != 0)
                {
                    CGST_Per = DT_Com.Rows[0]["CGST_Per"].ToString();
                    SGST_Per = DT_Com.Rows[0]["SGST_Per"].ToString();
                    IGST_Per = DT_Com.Rows[0]["IGST_Per"].ToString();
                }

                if (DT_Com.Rows[0]["Description"].ToString() == "GST")
                {
                    IGST_Per = "0";

                    GSTPer = (Convert.ToDecimal(CGST_Per) + Convert.ToDecimal(SGST_Per)).ToString();
                }
                if (DT_Com.Rows[0]["Description"].ToString() == "IGST")
                {
                    CGST_Per = "0";
                    SGST_Per = "0";

                    GSTPer = IGST_Per;
                }
                if (ddlTaxType.SelectedItem.Text == "VAT" || ddlTaxType.SelectedItem.Text == "NONE")
                {
                    CGST_Per = "0";
                    SGST_Per = "0";
                    IGST_Per = "0";

                    GSTPer = "0.00";
                }

                //Tax Calculate
                CGST_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(CGST_Per)).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                SGST_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(SGST_Per)).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                IGST_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(IGST_Per)).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                line_Total_Str = (Convert.ToDecimal(Item_Total) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                line_Total_Str = (Math.Round(Convert.ToDecimal(line_Total_Str), 2, MidpointRounding.AwayFromZero)).ToString();


                dt.Rows[i]["GstPer"] = GSTPer;
                dt.Rows[i]["CGSTPer"] = CGST_Per;
                dt.Rows[i]["CGSTAmount"] = CGST_Amt;
                dt.Rows[i]["SGSTPer"] = SGST_Per;
                dt.Rows[i]["SGSTAmount"] = SGST_Amt;
                dt.Rows[i]["IGSTPer"] = IGST_Per;
                dt.Rows[i]["IGSTAmount"] = IGST_Amt;
                dt.Rows[i]["LineTotal"] = line_Total_Str;
                dt.Rows[i]["ItemTotal"] = Item_Total;
            }

            Totalsum();
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            //Grid_Total_Calculate();
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    protected void ddlTaxType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Reset_Tax_Change_Mode_Value();
    }


    protected void chkPFGst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkTCGst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkCCGst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkOther1Gst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void chkOther2Gst_CheckedChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void txtPackFrwdCharge_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void txtTransCharge_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void txtCourierCharge_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void txtOther1Amt_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    protected void txtOther2Amt_TextChanged(object sender, EventArgs e)
    {
        Grid_Total_Value();
        OtherCharge_Calc();
    }

    public void Grid_Total_Value()
    {
        try
        {
            lblErrorMsg.Text = "";
               decimal SumLineTot = 0;
            decimal SumReceQtyTot = 0;
            decimal SumItemTot = 0;
            decimal SumCGSTTot = 0;
            decimal SumSGSTTot = 0;
            decimal SumIGSTTot = 0;
            decimal SumDiscTot = 0;

            string PackAmt = "0.00";
            string TransAmt = "0.00";
            string CourierAmt = "0.00";
            string OtherAmt1 = "0.00";
            string OtherAmt2 = "0.00";

            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ItemTable"];

            Repeater1.DataSource = DT;
            Repeater1.DataBind();

            for (int i = 0; i < Repeater1.Items.Count; i++)
            {
                TextBox txtGrdOrdQty = Repeater1.Items[i].FindControl("txtOrdQty") as TextBox;
                Label lblItemTot = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                Label lblDiscTot = Repeater1.Items[i].FindControl("lblDisc") as Label;
                Label lblCGSTTot = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                Label lblSGSTTot = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                Label lblIGSTTot = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                Label lblLineTot = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                SumReceQtyTot = SumReceQtyTot + Convert.ToDecimal(txtGrdOrdQty.Text);
                SumItemTot = SumItemTot + Convert.ToDecimal(lblItemTot.Text);
                SumDiscTot = SumDiscTot + Convert.ToDecimal(lblDiscTot.Text);
                SumCGSTTot = SumCGSTTot + Convert.ToDecimal(lblCGSTTot.Text);
                SumSGSTTot = SumSGSTTot + Convert.ToDecimal(lblSGSTTot.Text);
                SumIGSTTot = SumIGSTTot + Convert.ToDecimal(lblIGSTTot.Text);
                SumLineTot = SumLineTot + Convert.ToDecimal(lblLineTot.Text);
            }

            if (chkPFGst.Checked == true || chkTCGst.Checked == true || chkCCGst.Checked == true || chkOther1Gst.Checked == true || chkOther2Gst.Checked == true)
            {
                if (txtPackFrwdCharge.Text != "") { PackAmt = txtPackFrwdCharge.Text.ToString(); }
                if (txtTransCharge.Text != "") { TransAmt = txtTransCharge.Text.ToString(); }
                if (txtCourierCharge.Text != "") { CourierAmt = txtCourierCharge.Text.ToString(); }
                if (txtOther1Amt.Text != "") { OtherAmt1 = txtOther1Amt.Text.ToString(); }
                if (txtOther2Amt.Text != "") { OtherAmt2 = txtOther2Amt.Text.ToString(); }

                txtTotQty.Text = SumReceQtyTot.ToString();
                //SumItemTot.ToString();
                lblTotItem.Text = (Convert.ToDecimal(SumItemTot) + Convert.ToDecimal(PackAmt) + Convert.ToDecimal(CourierAmt) + Convert.ToDecimal(OtherAmt1) + Convert.ToDecimal(OtherAmt2) + Convert.ToDecimal(TransAmt)).ToString();
                lblTotDisc.Text = SumDiscTot.ToString();

                string ItemAmt = (Convert.ToDecimal(lblTotItem.Text) - Convert.ToDecimal(SumDiscTot)).ToString();
                string SSQL = "";
                string CGST_Amt = "";
                string SGST_Amt = "";
                string IGST_Amt = "";

                SSQL = "Select * from GstMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " and GST_Type='" + ddlTaxType.SelectedItem.Text + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                //Tax Calculate
                CGST_Amt = (Convert.ToDecimal(ItemAmt) * Convert.ToDecimal(DT.Rows[0]["CGST_Per"].ToString())).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                SGST_Amt = (Convert.ToDecimal(ItemAmt) * Convert.ToDecimal(DT.Rows[0]["SGST_Per"].ToString())).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                IGST_Amt = (Convert.ToDecimal(ItemAmt) * Convert.ToDecimal(DT.Rows[0]["IGST_Per"].ToString())).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();


                lblTotCGST.Text = CGST_Amt.ToString();
                lblTotSGST.Text = SGST_Amt.ToString();
                lblTotIGST.Text = IGST_Amt.ToString();


                txtTotAmt.Text = (Convert.ToDecimal(ItemAmt) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
                txtNetAmt.Text = (Convert.ToDecimal(ItemAmt) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            }
            else
            {

                if (txtPackFrwdCharge.Text != "") { PackAmt = txtPackFrwdCharge.Text.ToString(); }
                if (txtTransCharge.Text != "") { TransAmt = txtTransCharge.Text.ToString(); }
                if (txtCourierCharge.Text != "") { CourierAmt = txtCourierCharge.Text.ToString(); }
                if (txtOther1Amt.Text != "") { OtherAmt1 = txtOther1Amt.Text.ToString(); }
                if (txtOther2Amt.Text != "") { OtherAmt2 = txtOther2Amt.Text.ToString(); }

                txtTotQty.Text = SumReceQtyTot.ToString();
                lblTotItem.Text = SumItemTot.ToString();
                lblTotDisc.Text = SumDiscTot.ToString();
                lblTotCGST.Text = SumCGSTTot.ToString();
                lblTotSGST.Text = SumSGSTTot.ToString();
                lblTotIGST.Text = SumIGSTTot.ToString();

                txtTotAmt.Text = (Convert.ToDecimal(SumLineTot) + Convert.ToDecimal(PackAmt) + Convert.ToDecimal(CourierAmt) + Convert.ToDecimal(OtherAmt1) + Convert.ToDecimal(OtherAmt2) + Convert.ToDecimal(TransAmt)).ToString();
                txtNetAmt.Text = (Convert.ToDecimal(SumLineTot) + Convert.ToDecimal(PackAmt) + Convert.ToDecimal(CourierAmt) + Convert.ToDecimal(OtherAmt1) + Convert.ToDecimal(OtherAmt2) + Convert.ToDecimal(TransAmt)).ToString();
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }

    }

    private void OtherCharge_Calc()
    {
        //Boolean Errflg = false;

        //string SubTot;
        //string PackFrwdChr = "0.00";
        //string TransChr = "0.00";
        //string CouriChr = "0.00";
        //string SSQL;
        //DataTable DT = new DataTable();
        //string CGST_Amt;
        //string SGST_Amt;
        //string IGST_Amt;

        //if (txtPackFrwdCharge.Text != "") { PackFrwdChr = txtPackFrwdCharge.Text.ToString(); }
        //if (txtTransCharge.Text != "") { TransChr = txtTransCharge.Text.ToString(); }
        //if (txtCourierCharge.Text != "") { CouriChr = txtCourierCharge.Text.ToString(); }

        //if (chkPFGst.Checked == true || chkTCGst.Checked == true || chkCCGst.Checked == true)
        //{
        //    if (txtGSTType.Text == "-Select-")
        //    {
        //        Errflg = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select GST Type');", true);
        //    }

        //    if (!Errflg)
        //    {

        //        SubTot = (Convert.ToDecimal(txtTotAmt.Text) - Convert.ToDecimal(lblTotDisc.Text)).ToString();

        //        SubTot = (Convert.ToDecimal(SubTot) + Convert.ToDecimal(PackFrwdChr) + Convert.ToDecimal(TransChr) + Convert.ToDecimal(CouriChr)).ToString();

        //        SubTot = (Math.Round(Convert.ToDecimal(SubTot), 2, MidpointRounding.AwayFromZero)).ToString();

        //        SSQL = "Select * from GstMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        //        SSQL = SSQL + " and GST_Type='" + txtGSTType.Text + "'";

        //        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //        //Tax Calculate
        //        CGST_Amt = (Convert.ToDecimal(SubTot) * Convert.ToDecimal(DT.Rows[0]["CGST_Per"].ToString())).ToString();
        //        CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
        //        CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        //        SGST_Amt = (Convert.ToDecimal(SubTot) * Convert.ToDecimal(DT.Rows[0]["SGST_Per"].ToString())).ToString();
        //        SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
        //        SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        //        IGST_Amt = (Convert.ToDecimal(SubTot) * Convert.ToDecimal(DT.Rows[0]["IGST_Per"].ToString())).ToString();
        //        IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
        //        IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        //        lblTotCGST.Text = CGST_Amt;
        //        lblTotSGST.Text = SGST_Amt;
        //        lblTotIGST.Text = IGST_Amt;

        //        txtTotAmt.Text = (Math.Round(Convert.ToDecimal(SubTot) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

        //        lblTotLineAmt.Text = txtTotAmt.Text;

        //        txtNetAmt.Text = txtTotAmt.Text;
        //    }
        //}
        //else
        //{
        //    SubTot = (Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(lblTotCGST.Text) + Convert.ToDecimal(lblTotSGST.Text) + Convert.ToDecimal(lblTotIGST.Text) + Convert.ToDecimal(PackFrwdChr) + Convert.ToDecimal(TransChr) + Convert.ToDecimal(CouriChr)).ToString();

        //    SubTot = (Math.Round(Convert.ToDecimal(SubTot), 2, MidpointRounding.AwayFromZero)).ToString();

        //    lblTotLineAmt.Text = SubTot;

        //    txtNetAmt.Text = SubTot;
        //}
    }
}