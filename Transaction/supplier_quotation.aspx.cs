﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_supplier_quotation : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionQuotationNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");

        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Supplier Quotation";
            Initial_Data_Referesh();
            Load_Data_Empty_Supp1();
            Load_Data_Empty_Purchase_Request_No();
            Load_Data_Empty_ItemCode();

            if (Session["Quotation_No"] == null)
            {
                SessionQuotationNo = "";
            }
            else
            {
                SessionQuotationNo = Session["Quotation_No"].ToString();
                txtQuatationNo.Text = SessionQuotationNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        if (txtQtnDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Date..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select the Date..');", true);
        }

        if (txtSupplierName_Select.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to Select the Supplier Name..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select the Supplier Name..');", true);
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Supplier Quotation", SessionFinYearVal,"1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin.');", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtQuatationNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            SSQL = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtQuatationNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                SSQL = "Delete from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtQuatationNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "Delete from Supp_Qut_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtQuatationNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            //txtSuppCode1_test.Value;

            //Response.Write(strValue);
            //Insert Main Table
            SSQL = "Insert Into Supp_Qut_Main(Ccode,Lcode,FinYearCode,FinYearVal,QuotNo,QuotDate,SuppCode,SuppName,SuppQutNo,SuppQutDate,RefDocNo,RefDocDate,PaymentTerms,PaymentMode,DeliveryDet,UserID,UserName) Values('" + SessionCcode + "',";
            SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtQuatationNo.Text + "','" + txtQtnDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "','" + txtSuppQtnNo.Text + "','" + txtSuppQutDate.Text + "','" + txtRefDocNo.Text + "',";
            SSQL = SSQL + " '" + txtRefDocDate.Text + "','" + txtPaymentTerms.Text + "','" + txtPaymentMode.Value + "','" + txtDelivery.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Insert Into Supp_Qut_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,QuotNo,QuotDate,ItemCode,ItemName,UOMCode,Rate,UserID,UserName,Pur_Request_No) Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtQuatationNo.Text + "','" + txtQtnDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "','" + dt.Rows[i]["Pur_Request_No"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Quotation Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Quotation Details Updated Successfully');", true);
            }
            Clear_All_Field();

            Response.Redirect("supplier_quotation_main.aspx");

            //Session["Quotation_No"] = txtQuatationNo.Text;
            //btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtQuatationNo.Text = ""; txtQtnDate.Text = ""; txtSuppQtnNo.Text = ""; txtSuppQutDate.Text = "";
        txtRefDocNo.Text = ""; txtRefDocDate.Text = ""; txtPaymentTerms.Text = ""; txtPaymentMode.Value = "- select -";
        txtDelivery.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        txtRate.Text = "";
        txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
        txtItemNameSelect.SelectedIndex = 0; txtPurchase_Request_No.SelectedIndex = 0;

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Quotation_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

        //check with Item Code And Item Name 
        SSQL = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCode.Text + "' And ItemName='" + txtItemName.Text + "' And Pur_Request_No='" + txtPurchase_Request_No.SelectedValue + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = qry_dt.Rows[0]["UOMCode"].ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCode.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["Pur_Request_No"] = txtPurchase_Request_No.SelectedValue;
                    dr["ItemCode"] = txtItemCode.Text;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["Rate"] = txtRate.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtItemCode.Text = ""; txtItemName.Text = ""; txtRate.Text = "";
                    txtItemNameSelect.SelectedIndex = 0;txtPurchase_Request_No.SelectedIndex = 0;
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["Pur_Request_No"] = txtPurchase_Request_No.SelectedValue;
                dr["ItemCode"] = txtItemCode.Text;
                dr["ItemName"] = txtItemName.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["Rate"] = txtRate.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCode.Text = ""; txtItemName.Text = ""; txtRate.Text = "";
            }


        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Pur_Request_No", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtQuatationNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtQtnDate.Text = Main_DT.Rows[0]["QuotDate"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["SuppCode"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["SuppName"].ToString();
            txtSuppQtnNo.Text = Main_DT.Rows[0]["SuppQutNo"].ToString();
            txtSuppQutDate.Text = Main_DT.Rows[0]["SuppQutDate"].ToString();
            txtRefDocNo.Text = Main_DT.Rows[0]["RefDocNo"].ToString();
            txtRefDocDate.Text = Main_DT.Rows[0]["RefDocDate"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            txtDelivery.Text = Main_DT.Rows[0]["DeliveryDet"].ToString();

            //Supp_Qut_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select ItemCode,ItemName,UOMCode,Rate,Pur_Request_No from Supp_Qut_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtQuatationNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }


    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        txtQuatationNo.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            //Delete Main Table
            SSQL = "Delete from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            //Delete Main Sub Table
            SSQL = "Delete from Supp_Qut_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Quotation Details Deleted Successfully');", true);

            Clear_All_Field();

        }
    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Response.Redirect("supplier_quotation_main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Supplier Quotation Approve");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Supplier Quotation..');", true);
        }
        //User Rights Check End

        SSQL = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtQuatationNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Supplier Quotation Details..');", true);
        }

        SSQL = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtQuatationNo.Text + "' And Supp_Qtn_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Quotation Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Update Supp_Qut_Main set Supp_Qtn_Status='1' where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And QuotNo='" + txtQuatationNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Quotation Details Approved Successfully..');", true);
        }
    }

    private void Load_Data_Empty_Supp1()
    {

        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status='Add' And LedgerGrpName='Supplier' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtSupplierName_Select.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        txtSupplierName_Select.DataTextField = "LedgerName";
        txtSupplierName_Select.DataValueField = "LedgerCode";
        txtSupplierName_Select.DataBind();
    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);
    }
    
    private void Load_Data_Empty_ItemCode()
    {

        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        SSQL = "Select ItemCode,ItemName as ItemName from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurchase_Request_No.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCode.Text = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCode.Text = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;
        }

        //Get Item Rate
        string SSQL = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And Pur_Request_No='" + txtPurchase_Request_No.SelectedValue + "' And ItemName='" + txtItemName.Text + "'";
        DataTable DT_R = new DataTable();
        DT_R = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_R.Rows.Count != 0)
        {
            txtRate.Text = DT_R.Rows[0]["Item_Rate"].ToString();
        }
        else
        {
            txtRate.Text = "0";
        }
    }

    protected void txtSupplierName_Select_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtSupplierName_Select.SelectedValue != "-Select-")
        {
            txtSuppCodehide.Value = txtSupplierName_Select.SelectedValue;
            txtSupplierName.Text = txtSupplierName_Select.SelectedItem.Text;
        }
    }

    private void Load_Data_Empty_Purchase_Request_No()
    {

        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("Pur_Request_No");

        SSQL = "Select Pur_Request_No from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status='1'";
        SSQL = SSQL + " Order by Pur_Request_No";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtPurchase_Request_No.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["Pur_Request_No"] = "-Select-";
        dr["Pur_Request_No"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtPurchase_Request_No.DataTextField = "Pur_Request_No";
        txtPurchase_Request_No.DataValueField = "Pur_Request_No";
        txtPurchase_Request_No.DataBind();

    }

    protected void txtPurchase_Request_No_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_Data_Empty_ItemCode();

        Load_All_Item_Purchase_Request_Wise();
    }

    private void Load_All_Item_Purchase_Request_Wise()
    {
        string SSQL = "";
        DataTable DT = new DataTable();        
        SSQL = "Select Pur_Request_No,ItemCode,ItemName,UOMCode,Item_Rate as Rate from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurchase_Request_No.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ViewState["ItemTable"] = DT;
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
}