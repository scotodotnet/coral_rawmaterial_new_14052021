﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_Escrow_PurOrd_Amend_Appoval.aspx.cs" Inherits="Escrow_Amend_approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel runat="server" ID="upMain">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Escrow Amendment List</span></h3>
                                    <asp:Label runat="server" ID="lblErrorMsg" style="color:red;font-size:x-large" ></asp:Label>
                                </div>

                                <div class="row" runat="server" style="padding-top: 25px; padding-bottom: 25px">
                                    <div class="col-md-12">
                                        <div class="row" style="padding-left: 15px">
                                            <div class="col-md-3">
                                                <label class="control-label" for="Req_No">Request Status</label>
                                                <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                                    OnSelectedIndexChanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        
                                            <div class="col-md-3" runat="server" id="divCanReason">
                                                <label class="control-label" for="Req_No">Cancel Reason</label>
                                                <asp:TextBox ID="txtCanReason" runat="server" TextMode="MultiLine" style="resize:none" class="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">
                                            <div class="box-body no-padding">
                                                <div class="table-responsive mailbox-messages">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Trans No</th>
                                                                                <th>Date</th>
                                                                                <th>Purchase_Order_Na</th>
                                                                                <th>Supplier_Name</th>
                                                                                <th>MaterialName</th>
                                                                                <th>Approve</th>
                                                                                <th>Cancel</th>
                                                                                <th>View</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Eval("AmendPoNo")%></td>
                                                                        <td><%# Eval("AmendPoDate")%></td>
                                                                        <td><%# Eval("PurOrdNo")%></td>
                                                                        <td><%# Eval("SuppName")%></td>
                                                                        <td><%# Eval("MaterialName")%></td>
                                                                        <td runat="server" visible="false"><%# Eval("MatType")%></td>
                                                                        

                                                                        <td>
                                                                            <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square" runat="server"
                                                                                Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("AmendPoNo")%>' CommandName='<%# Eval("AmendPoNo")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Purchase Request details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="LinkButton1" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("AmendPoNo")%>' CommandName='<%# Eval("AmendPoNo")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Purchase Request details?');">
                                                                            </asp:LinkButton>
                                                                        </td>

                                                                        <td>
                                                                            <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-external-link-square" runat="server"
                                                                                Text="" OnCommand="GridEditPurRequestClick" CommandArgument='<%# Eval("AmendPoNo")%>'
                                                                                CommandName='<%# Eval("AmendPoNo") %>'>
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>

                                                        <div class="row">
                                                            <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false">
                                                                <HeaderTemplate>
                                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Trans No</th>
                                                                                <th>Date</th>
                                                                                <th>Tot Qty</th>
                                                                                <th>Material Type</th>
                                                                                <th>View</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Eval("AmendPoNo")%></td>
                                                                        <td><%# Eval("AmendPoDate")%></td>
                                                                        <td><%# Eval("PurOrdNo")%></td>
                                                                        <td><%# Eval("MaterialName")%></td>
                                                                        <td runat="server" visible="false"><%# Eval("MatType")%></td>

                                                                        <td>
                                                                            <asp:LinkButton ID="btnApprovalReq_View" class="btn btn-primary btn-sm fa fa-external-link-square" runat="server"
                                                                                Text="" OnCommand="GridEditPurRequestClick" CommandArgument='<%# Eval("AmendPoNo")%>'
                                                                                CommandName='<%# Eval("AmendPoNo")+","+ Eval("MatType")%>'>
                                                                            </asp:LinkButton>
                                                                        </td>

                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                        <div class="row">
                                                            <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
                                                                <HeaderTemplate>
                                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Trans No</th>
                                                                                <th>Date</th>
                                                                                <th>Tot Qty</th>
                                                                                <th>Material Type</th>
                                                                                <th>Reason</th>
                                                                                <th>View</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Eval("AmendPoNo")%></td>
                                                                        <td><%# Eval("AmendPoDate")%></td>
                                                                        <td><%# Eval("PurOrdNo")%></td>
                                                                        <td><%# Eval("MaterialName")%></td>
                                                                        <td runat="server" visible="false"><%# Eval("MatType")%></td>
                                                                        <td><%# Eval("CanceledReason")%></td>

                                                                        <td>
                                                                            <asp:LinkButton ID="btnApprovalReq_View" class="btn btn-primary btn-sm fa fa-external-link-square" runat="server"
                                                                                Text="" OnCommand="GridEditPurRequestClick" CommandArgument='<%# Eval("AmendPoNo")%>'
                                                                                CommandName='<%# Eval("AmendPoNo")+","+ Eval("MatType")%>'>
                                                                            </asp:LinkButton>
                                                                        </td>

                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>

                                                    </div>

                                                    <!-- /.table -->
                                                </div>
                                                <!-- /.mail-box-messages -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

