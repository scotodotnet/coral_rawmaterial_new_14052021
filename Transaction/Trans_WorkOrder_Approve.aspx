﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_WorkOrder_Approve.aspx.cs" Inherits="Transaction_Trans_WorkOrder_Approve" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>MRP Process</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Work Order No</label>
                                                <asp:Label ID="txtworkorder" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Order No</label>
                                                                <asp:DropDownList ID="ddlorderNo" runat="server" class="form-control select2"
                                                                    AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlorderNo_SelectedIndexChanged" Enabled="false">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Generator Model</label>
                                                <asp:Label ID="txtgenModel" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Part Name</label>
                                                                <asp:DropDownList ID="ddlPartName" runat="server" class="form-control select2"
                                                                  AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlPartName_SelectedIndexChanged" Enabled="false" >
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                       

                                        
                                    </div>
                                    <div class="row">

                                        <asp:HiddenField ID="hfMdlCode" runat="server" />

                                        <div class="col-md-4" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Machine Specification</label>
                                                <asp:label ID="txtMachineSpec" runat="server" class="form-control" Style="resize: none"
                                                    TextMode="MultiLine"></asp:label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Delivery Req By</label>
                                                <asp:Label ID="txtdelivery" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Part BOM Revision Status</label>
                                                <asp:Label ID="txtPartBOM" runat="server" class="form-control"></asp:Label>
                                            </div>

                                        </div>
                                      
                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName"></label>
                                                        <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Generate Work Order" OnClick="btnGenerateWO_Click" />

                       
                                            </div>
                                        </div>
                                        </div>
                                   
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Item Details</span></h3>
                                                    <div class="box-tools pull-right">
                                                        <div class="has-feedback">
                                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="clearfix"></div>

                                               

                                                <asp:Panel ID="pnlRpter" runat="server" Height="300px" Style="overflow-x: scroll; overflow-y: scroll">
                                                    <div class="box-body no-padding">
                                                        <div class="table-responsive mailbox-messages">
                                                            <asp:Repeater ID="rptGenPartType" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="tbltest" class="table table-hover table-striped table-bordered">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S_No</th>
                                                                                <th>Production Stage</th>
                                                                                <th>SAP No</th>
                                                                                <th>Item Desc</th>
                                                                                <th>Req Date</th>
                                                                                <th>Req Qty</th>
                                                                                <th>Stock Qty</th>
                                                                                <th>PR Qty</th>
                                                                               
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                                        <%--<td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="Label1" Text='<%# Eval("BOI_ID")%>'></asp:Label>
                                                                        </td>--%>

                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblMat_No" Text='<%# Eval("Mat_No")%>'></asp:Label>
                                                                        </td>
                                                                         <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblUOM" Text='<%# Eval("UOM")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server" visible="false">
                                                                            <asp:Label runat="server" ID="lblBOM" Text='<%# Eval("BOM_ID")%>'></asp:Label>
                                                                        </td>
                                                                         <td runat="server" visible="true">
                                                                            <asp:Label runat="server" ID="lblProdSteps" Text='<%# Eval("Production_Stage")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblSAPNo" Text='<%# Eval("SAP_No")%>'></asp:Label>
                                                                        </td>

                                                                        <td runat="server">
                                                                            <asp:Label runat="server" ID="lblRawMat" Text='<%# Eval("Item_Desc")%>'></asp:Label>
                                                                        </td>
                                                                         <td runat="server" visible="true">
                                                                            <asp:Label runat="server" ID="lblReqdate" Text='<%# Eval("Req_Date")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server">
                                                                            <asp:Label runat="server" ID="lblReqQty" Text='<%# Eval("Req_Qty")%>'></asp:Label>
                                                                        </td>
                                                                        <td runat="server">
                                                                            <asp:Label runat="server" ID="lblStockQty" Text='<%# Eval("Stock_Qty")%>'></asp:Label>
                                                                        </td>
                                                                       <td runat="server">
                                                                            <asp:Label runat="server" ID="lblPRQty" Text='<%# Eval("PR_Qty")%>'></asp:Label>
                                                                        </td>

                                                                        <td>
                                                                          <%--  <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("Sap_No")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                            </asp:LinkButton>--%>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>

                                                            <!-- /.table -->
                                                        </div>
                                                        <!-- /.mail-box-messages -->
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <div class="form-group">
                                    <asp:Button ID="btnApprove" class="btn btn-primary" runat="server" Text="Approve" ValidationGroup="Validate_Field"
                                        OnClick="btnApprove_Click" />
                                    <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Back" OnClick="btnCancel_Click" />
                                    <%--<asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/loadingoverlay.min.js"></script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                    $('.select2').select2();
                    $("#tbltest").DataTable({
                        'processing': true,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": true,
                        "ordering": false,
                        "searching": true
                        //"drawCallback":true
                    });
                }
            });
        };
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            console.log("Start");
            $("#tbltest").DataTable({
                'processing': true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": true,
                "ordering": false,
                "searching": true
                //"drawCallback":true
            });
        });
    </script>

</asp:Content>
