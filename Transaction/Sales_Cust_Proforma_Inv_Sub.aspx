﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Sales_Cust_Proforma_Inv_Sub.aspx.cs" Inherits="Sales_Transaction_Sales_Cust_Proforma_Inv_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy'});
                    $('.select2').select2();
                }
            });
        };
    </script>
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upPurReq" runat="server">
            <ContentTemplate>
                <section class="content-header">
            <h1><i class=" text-primary"></i>Proforma Invoice Details</h1>
        </section>
    
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">         
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="Req_No">Invoice No</label>
                                        <asp:Label ID="txtInvoice_NO" runat="server" class="form-control"></asp:Label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Date</label>
                                        <asp:TextBox ID="txtDate" MaxLength="20" class="form-control datepicker" runat="server" AutoComplete="off" ></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Customer Name</label>
                                        <asp:DropDownList ID="txtCustomer_Name" runat="server" class="form-control select2" 
                                            AutoPostBack="true" onselectedindexchanged="txtCustomer_Name_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtCustomer_Name" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Party PO No</label>
                                        <asp:TextBox ID="txtParty_PO_No" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtParty_PO_No" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>                                        
                                    </div>
                                </div>
                                <div class="col-md-3" runat="server" visible="false">
                                    <div class="form-group">
                                        <label for="exampleInputName">PO No</label>
                                        <asp:DropDownList ID="txtPO_No" runat="server" class="form-control select2" 
                                            AutoPostBack="true" onselectedindexchanged="txtPO_No_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtPO_No" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Date</label>
                                        <asp:TextBox ID="txtParty_PO_Date" MaxLength="20" class="form-control datepicker" runat="server" AutoComplete="off" ></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtParty_PO_Date" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group col-md-2" runat="server" visible="false">
                                    <label for="exampleInputName">PO Date</label>
                                    <asp:Label ID="txtPO_Date" class="form-control" runat="server"></asp:Label>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Payment Mode</label>
					                    <asp:DropDownList ID="txtPayment_Mode" runat="server" class="form-control select2">
					                        <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
					                        <asp:ListItem Value="Cash" Text="Cash"></asp:ListItem>
					                        <asp:ListItem Value="Cheque" Text="Cheque"></asp:ListItem>
					                        <asp:ListItem Value="DD" Text="DD"></asp:ListItem>
					                        <asp:ListItem Value="NEFT" Text="NEFT"></asp:ListItem>
					                        <asp:ListItem Value="RTGS" Text="RTGS"></asp:ListItem>
					                        <asp:ListItem Value="IMPS" Text="IMPS"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3" runat="server" visible="false">
                                    <div class="form-group">
                                        <label for="exampleInputName">Transport</label>					                    
					                    <asp:DropDownList ID="txtTransPort" runat="server" class="form-control select2" 
					                        AutoPostBack="true" onselectedindexchanged="txtTransPort_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtTransPort" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-md-2" runat="server" visible="false">
                                    <div class="form-group">
                                        <label for="exampleInputName">Vehicle No</label>
					                    <asp:TextBox ID="txtVehicleNo" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3" runat="server" visible="false">
                                    <div class="form-group">
                                        <label for="exampleInputName">Delivery From</label>
					                    <asp:TextBox ID="txtDelivery_From" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row" runat="server" visible="false">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Delivery At</label>
					                    <asp:TextBox ID="txtDelivery_At" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputName">Note</label>
					                    <asp:TextBox ID="txtNote" class="form-control" style="resize:none" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="exampleInputName">Remarks</label>
					                    <asp:TextBox ID="txtRemarks" class="form-control" style="resize:none" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-primary">

                                        <div class="box-header with-border">
                                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Generator Details</span></h3>
                                            <div class="box-tools pull-right">
                                                <div class="has-feedback">
                                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <!-- /.box-tools -->
                                        </div>

                                        <div class="box-body no-padding">
                                            <div class="table-responsive mailbox-messages">                                                
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Sap No</label>
					                                        <asp:TextBox ID="txtSap_No" class="form-control" style="resize:none" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">HSN Code</label>
					                                        <asp:TextBox ID="txtHSN_Code" class="form-control" style="resize:none" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Item Description</label>
					                                        <asp:TextBox ID="txtItem_Description" class="form-control" style="resize:none" runat="server"></asp:TextBox>
					                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtItem_Description" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">UOM</label>
					                                        <asp:TextBox ID="txtUOM" class="form-control" style="resize:none" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Generator Model</label>
					                                        <asp:DropDownList ID="txtGenerator_Model" runat="server" class="form-control select2" AutoPostBack="true" 
                                                                onselectedindexchanged="txtGenerator_Model_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtGenerator_Model" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Qty</label>
					                                        <asp:TextBox ID="txtQty" class="form-control" runat="server" Text="0"></asp:TextBox>
					                                        <asp:RequiredFieldValidator ControlToValidate="txtQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator> 
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtQty" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Unit Price</label>
					                                        <asp:TextBox ID="txtRate" class="form-control" runat="server" Text="0"></asp:TextBox>
					                                        <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required."> </asp:RequiredFieldValidator> 
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtRate" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                            <asp:HiddenField runat="server" ID="txtPO_Rate" Value="0" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-2" style="padding-top:5px" >
					                                    <br />
					                                    <asp:Button ID="btnAddItem" class="btn btn-primary"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click"/>
					                                </div>
                                                </div>

					                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                                        <HeaderTemplate>
                                                        <table  class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>S. No</th>
                                                                    <th>Sap No</th>
                                                                    <th>HSN Code</th>
                                                                    <th>Item Description</th>
                                                                    <th>UOM</th>
                                                                    <th>Qty</th>
                                                                    <th>Unit Price</th>
                                                                    <th>Total Amount</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# Eval("Sap_No")%></td>
                                                            <td><%# Eval("HSN_Code")%></td>
                                                            <td><%# Eval("Item_Desc")%></td>
                                                            <td><%# Eval("UOM")%></td>
                                                            <td><%# Eval("Qty")%></td>
                                                            <td><%# Eval("Rate")%></td>
                                                            <td><%# Eval("LineTotal")%></td>                                                            
                                                            <td>
                                                                <%--<asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                                    Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Container.ItemIndex + 1 %>'>
                                                                </asp:LinkButton>--%>
                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                    Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Container.ItemIndex + 1 %>' 
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');"> </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>                                
			                                    </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-8"></div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">Currency Type</label>
                                        <asp:DropDownList ID="txtCurrency_Type" runat="server" class="form-control select2" 
                                            onselectedindexchanged="txtCurrency_Type_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="EUR" Value="EUR" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="INR" Value="INR"></asp:ListItem>
                                            <asp:ListItem Text="USD" Value="USD"></asp:ListItem>
                                            <asp:ListItem Text="JPY" Value="JPY"></asp:ListItem>
                                            <asp:ListItem Text="GBP" Value="GBP"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">Sub Total</label>
					                    <asp:Label ID="txtSubTotal" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">GST Type</label>
                                        <asp:DropDownList ID="txtGST_Type" runat="server" class="form-control select2" 
                                            onselectedindexchanged="txtGST_Type_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">Discount %</label>
					                    <asp:TextBox ID="txtDiscount_Per" class="form-control" runat="server" 
                                            ontextchanged="txtDiscount_Per_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtDiscount_Per" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">Discount Amt</label>
					                    <asp:Label ID="txtDiscount_Amount" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">After Dis.Sub Total</label>
					                    <asp:Label ID="txtAfter_Discount_Tot" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">Freight Charges</label>
					                    <asp:TextBox ID="txtFreight_Charges" class="form-control" runat="server" 
                                            ontextchanged="txtFreight_Charges_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtFreight_Charges" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">Taxable Amt</label>
					                    <asp:Label ID="txtTaxable_Amt" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">   
                                <div class="col-md-1"></div>                             
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="exampleInputName">CGST %</label>
					                    <asp:Label ID="txtCGST_Per" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">CGST Amt</label>
					                    <asp:Label ID="txtCGST_Amt" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="exampleInputName">SGST %</label>
					                    <asp:Label ID="txtSGST_Per" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">SGST Amt</label>
					                    <asp:Label ID="txtSGST_Amt" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="exampleInputName">IGST %</label>
					                    <asp:Label ID="txtIGST_Per" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">IGST Amt</label>
					                    <asp:Label ID="txtIGST_Amt" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">GST Total</label>
					                    <asp:Label ID="txtGST_Total" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
					                    <asp:Label ID="txtGST_And_Gross_Amt" class="form-control" runat="server" BackColor="LightGray" Text="0" Visible="false"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8"></div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">Round Off</label>
					                    <asp:Label ID="txtRound_Off" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputName">Net Amount</label>
					                    <asp:Label ID="txtFinal_Net_Amt" class="form-control" runat="server" BackColor="LightGray" Text="0"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="box-footer">
                            <div class="form-group">
                                <asp:Button ID="btnSave" class="btn btn-primary"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
    
</asp:Content>

