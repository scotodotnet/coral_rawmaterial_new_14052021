﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_Coral_purchase_request_approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Coral Purchase Request Approval";
            divCancelReason.Visible = false;
            purchase_Request_Status_Add();
        }
        Load_Data_Empty_Grid();
    }


    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            //User Rights Check Start
            bool ErrFlag = false;
            bool Rights_Check = false;

            Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Coral Request Approval");

            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approval...');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approved Purchase Request..');", true);
            }
            //User Rights Check End


            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Coral_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And Status = '2'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Already Cancelled.. Cannot Approved it..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Cancelled.. Cannot Approved it..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }
            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Coral_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And Status = '1'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Already get Approved..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already get Approved..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }

            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Coral_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "' And Status = '1'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

               
                    if (DT.Rows.Count == 0)
                    {
                        //Delete Main Table
                        SSQL = "Update Trans_Coral_PurRqu_Main set Status='1',Approvedby='" + SessionUserName + "',ApprovedDate=Convert(Datetime,GetDate(),103) ";
                        SSQL = SSQL + "where Ccode ='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                        SSQL = SSQL + " Lcode ='" + SessionLcode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Approved Successfully..');", true);
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Approved Successfully');", true);
                        txtRequestStatus_SelectedIndexChanged(sender, e);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request not is Avail give input correctly..');", true);
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request not is Avail give input correctly..');", true);
                    }
                
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "done", "alert('" + ex.Message.Replace("'", "") + "');", true);
        }
    }



    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            //User Rights Check Start
            bool ErrFlag = false;
            bool Rights_Check = false;

            divCancelReason.Visible = true;

            Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Coral Request Approval");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Request..');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approved Purchase Request..');", true);
            }
            //User Rights Check End

            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Coral_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And Status = '1'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Already Approved.. Cannot Cancelled it..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Approved.. Cannot Cancelled it..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }
            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Coral_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And Status = '2'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Already get Cancelled..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already get Cancelled..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }

            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Coral_PurRqu_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And Status = '2'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (txtCancelReason.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Reason For Cancellation');", true);
                    txtCancelReason.Focus();
                    ErrFlag = true;
                }

                if (!ErrFlag)
                {

                    if (DT.Rows.Count == 0)
                    {
                        //Delete Main Table
                        SSQL = "Update Trans_Coral_PurRqu_Main set Status='-1',Canceledby='" + SessionUserName + "',CanceledDate=Convert(Datetime,GetDate(),103), ";
                        SSQL = SSQL + " CanceledReason='" + txtCancelReason.Text + "' Where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                        SSQL = SSQL + " Lcode ='" + SessionLcode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "'";

                        objdata.RptEmployeeMultipleDetails(SSQL);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Cancelled Successfully..');", true);
                        txtRequestStatus_SelectedIndexChanged(sender, e);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request not is Avail give input correctly..');", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "done", "alert('" + ex.Message.Replace("'", "") + "');", true);
        }
    }



    private void Load_Data_Empty_Grid()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            DataTable DT2 = new DataTable();


            DT.Columns.Add("Pur_Request_No");
            DT.Columns.Add("Pur_Request_Date");
            DT.Columns.Add("TotalReqQty");
            DT.Columns.Add("Approvedby");

            if (txtRequestStatus.Text == "Pending List")
            {
                SSQL = "Select Pur_Request_No,Pur_Request_Date,TotalReqQty,Approvedby, ";
                SSQL = SSQL + " Case When MaterialType='1' then 'RawMaterial' ";
                SSQL = SSQL + " when MaterialType='2' then 'Tools' ";
                SSQL = SSQL + " when MaterialType='3' then 'Asset' ";
                SSQL = SSQL + " when MaterialType='4' then 'General Item' ";
                SSQL = SSQL + " End [MaterialName],MaterialType ";
                SSQL = SSQL + " from Trans_Coral_PurRqu_Main where ";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And (Status = '0' or Status is null) Order By Pur_Request_No Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Pending.DataSource = DT;
                Repeater_Pending.DataBind();
                Repeater_Pending.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Approved List")
            {
                SSQL = "Select Pur_Request_No,Pur_Request_Date,TotalReqQty,Approvedby,";
                SSQL = SSQL + " Case When MaterialType='1' then 'RawMaterial' ";
                SSQL = SSQL + " when MaterialType='2' then 'Tools' ";
                SSQL = SSQL + " when MaterialType='3' then 'Asset' ";
                SSQL = SSQL + "  when MaterialType='4' then 'General Item' End [MaterialName],MaterialType ";
                SSQL = SSQL + " from Trans_Coral_PurRqu_Main where ";
                SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And Status = '1' Order By Pur_Request_No Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Approve.DataSource = DT;
                Repeater_Approve.DataBind();
                Repeater_Approve.Visible = true;
                Repeater_Pending.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Rejected List")
            {
                SSQL = "Select Pur_Request_No,Pur_Request_Date,CanceledReason,Approvedby, ";

                SSQL = SSQL + " Case When MaterialType='1' then 'RawMaterial' ";
                SSQL = SSQL + " when MaterialType='2' then 'Tools' ";
                SSQL = SSQL + " when MaterialType='3' then 'Asset' ";
                SSQL = SSQL + " when MaterialType='4' then 'General Item' End [MaterialName],MaterialType ";

                SSQL = SSQL + " from Trans_Coral_PurRqu_Main where ";
                SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And Status = '-1' Order By Pur_Request_No Asc";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Rejected.DataSource = DT;
                Repeater_Rejected.DataBind();
                Repeater_Rejected.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Pending.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "done", "alert('" + ex.Message.Replace("'", "") + "');", true);
        }
    }

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }

    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_Grid();
    }

    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string RptName = "";
            string Arguments = "";
            string PurRquNo = "";
            string MatType = "";
            bool ErrFlag = false;
            bool Rights_Check = false;


            Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Coral Request Approval");

            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approval...');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approved Purchase Request..');", true);
            }

            if (!ErrFlag)
            {
                RptName = "Coral Purchase Request";
                Arguments = e.CommandName.ToString();

                string[] A = Arguments.ToString().Split(',');

                PurRquNo = A[0].ToString();
                MatType = A[1].ToString();

                ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?PurRquNo=" + PurRquNo + "&MatType=" + MatType + "&RptName=" + RptName, "_blank", "");

            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "done", "alert('" + ex.Message.Replace("'", "") + "');", true);
        }
    }
}