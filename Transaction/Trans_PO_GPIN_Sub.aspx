﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_PO_GPIN_Sub.aspx.cs" Inherits="Trans_PO_GPIN_Sub" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <asp:UpdatePanel ID="upGoodRecv" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1><i class=" text-primary"></i>Create New GatePass IN</h1>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default box -->
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <asp:DropDownList ID="ddlTest" runat="server" class="form-control select2"
                                                    OnSelectedIndexChanged="ddlTest_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row" runat="server" style="padding-top: 1.75%">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Item Details</span></h3>
                                                    <div class="box-tools pull-right">
                                                        <div class="has-feedback">
                                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-body no-padding">
                                                    <div class="table-responsive mailbox-messages">

                                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table class="table table-hover table-striped ">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S. No</th>
                                                                            <th>Sap No</th>
                                                                            <th>Item Name</th>
                                                                            <th>UOM Code</th>
                                                                            <th>Required Qty</th>
                                                                            <th>Model Qty</th>
                                                                            <th>Qty</th>
                                                                            <th runat="server" visible="false">Rate</th>
                                                                            <th>Required Date</th>
                                                                            <th>Calendar Weak</th>
                                                                            <th>Remarks</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>



                                                                    <td><%# Container.ItemIndex + 1 %></td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblModelCode" runat="server" BorderStyle="None" Text='<%# Eval("ModelCode")%>'></asp:Label></td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblModelName" runat="server" BorderStyle="None" Text='<%# Eval("ModelName")%>'></asp:Label></td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblPrdPartCode" runat="server" BorderStyle="None" Text='<%# Eval("PrctPartCode")%>'></asp:Label></td>

                                                                    <td runat="server" visible="false">>
                                                                    <asp:Label ID="lblPrdPartName" runat="server" BorderStyle="None" Text='<%# Eval("PrctPartName")%>'></asp:Label></td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblItemCode" runat="server" BorderStyle="None" Text='<%# Eval("ItemCode")%>'></asp:Label></td>

                                                                    <td>
                                                                        <asp:Label ID="lblSapNo" runat="server" BorderStyle="None" Text='<%# Eval("SAPNo")%>'></asp:Label></td>

                                                                    <td>
                                                                        <asp:Label ID="lblItemName" runat="server" BorderStyle="None" Text='<%# Eval("ItemName")%>'></asp:Label></td>

                                                                    <td>
                                                                        <asp:Label ID="lblUOM" runat="server" BorderStyle="None" Text='<%# Eval("UOMCode")%>'></asp:Label></td>

                                                                    <td>
                                                                        <asp:Label ID="txtRquiQty" BorderStyle="None" runat="server" AutoPostBack="true"
                                                                            Text='<%# Eval("ReuiredQty")%>' OnTextChanged="txtRquiQty_TextChanged"></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:TextBox ID="txtModQty" BorderStyle="None" runat="server"
                                                                            Text='<%# Eval("ModelQty")%>'></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ControlToValidate="txtModQty" ValidationGroup="Item_Validate_Field" class="form_error"
                                                                            ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                            TargetControlID="txtModQty" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </td>

                                                                    <td>
                                                                        <asp:TextBox ID="txtQty" BorderStyle="None" runat="server"
                                                                            Text='<%# Eval("SumOfQty")%>'></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ControlToValidate="txtQty" ValidationGroup="Item_Validate_Field" class="form_error"
                                                                            ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                            TargetControlID="txtQty" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblItemRate" BorderStyle="None" runat="server" Text='<%# Eval("Item_Rate") %>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblItemRate_Other" BorderStyle="None" runat="server" Text='<%# Eval("ItemRate_Other") %>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblCGSTP" BorderStyle="None" runat="server" Text='<%# Eval("CGSTP") %>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblSGSTP" BorderStyle="None" runat="server" Text='<%# Eval("SGSTP") %>'></asp:Label>
                                                                    </td>

                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblIGSTP" BorderStyle="None" runat="server" Text='<%# Eval("IGSTP") %>'></asp:Label>
                                                                    </td>
                                                                    <td runat="server" visible="false">
                                                                        <asp:Label ID="lblVATP" BorderStyle="None" runat="server" Text='<%# Eval("VATP") %>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:TextBox ID="txtRquDate" BorderStyle="None" class="datepicker" runat="server"
                                                                            Text='<%# Eval("ReuiredDate")%>'></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ControlToValidate="txtRquDate" ValidationGroup="Validate_Field"
                                                                            class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="lblCalWeak" runat="server" BorderStyle="None" Text='<%# Eval("CalendarWeek")%>'></asp:Label></td>

                                                                    <td>
                                                                        <asp:Label ID="lblRemarks" BorderStyle="None" runat="server" Text='<%# Eval("ItemRemarks") %>'></asp:Label>
                                                                    </td>

                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")+","+Eval("PrctPartCode")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                    <div class="table-responsive mailbox-messages">
                                                        <div class="divGatepassIN">
                                                            <table id="example" class="display table table-bordered table-responsive">
                                                                <thead>
                                                                    <tr>
                                                                        <th>S.No</th>
                                                                        <th>SAPNo</th>
                                                                        <th>ItemName</th>
                                                                        <th>UOMCode</th>
                                                                        <th>ReuiredQty</th>
                                                                        <th>ModelQty</th>
                                                                        <th>Qty</th>
                                                                        <th>Required Date</th>
                                                                        <th>Calendar Weak</th>
                                                                        <th>Remarks</th>
                                                                        <th>Edit</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- /.mail-box-messages -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="form-group">
                                        <input type="hidden" name="CustomerJSON" />
                                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" OnClientClick="GetTableValues()" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" />
                                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />
                                    </div>
                                </div>
                                <!-- /.box-footer-->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#divGatepassIN table .date-picker').datepicker({ format: 'dd/mm/yyyy' });
                    $('#divGatepassIN table .js-states').select2();
                    $("#example").DataTable({
                        stateSave: true,
                        "drawCallback": function (settings) {
                            $('.delete').click(function () {
                                var confm = confirm("Are you sure Want to delete this Details");
                                if (confm == true) {
                                    var id = $(this).closest("tr").find('td:eq(2)').text();
                                    alert(id);
                                }
                            });
                            $('.txtReqQtyEdit').keyup(function () {
                                var preVal = $(this).closest("tr").find('td:eq(10)').text();
                                var multipleVal = $(this).closest("tr").find('td:eq(9)').text();
                                var newVal = ($(this).val() * multipleVal);
                                $(this).closest("tr").find('td:eq(10)').text(newVal);
                            });
                        }
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">

        function GetTableValues() {
            //Create an Array to hold the Table values.
            var lists = new Array();
            //Reference the Table.
            var table = document.getElementById("example");
            //Loop through Table Rows.
            for (var i = 1; i < table.rows.length; i++) {
                //Reference the Table Row.
                var row = table.rows[i];
                //Copy values from Table Cell to JSON object.
                var list = {};
                list.SAPNo = row.cells[1].innerHTML;
                list.ItemName = row.cells[2].innerHTML;
                list.UOMCode = row.cells[3].innerHTML;
                list.RequiredQty = row.cells[4].innerHTML;
                list.ModelQty = row.cells[5].innerHTML;
                list.SumQty = row.cells[6].innerHTML;
                list.RequiredDate = row.cells[7].innerHTML;
                list.CalenderWeek = row.cells[8].innerHTML;
                list.Remarks = row.cells[9].innerHTML;
                list.ModelCode = row.cells[10].innerHTML;
                list.ModelName = row.cells[11].innerHTML;
                list.PrctPartCode = row.cells[12].innerHTML;
                list.PrctPartName = row.cells[13].innerHTML;
                list.ItemCode = row.cells[14].innerHTML;
                lists.push(list);
            }

            //Convert the JSON object to string and assign to Hidden Field.
            document.getElementsByName("CustomerJSON")[0].value = JSON.stringify(lists);
        }

        $(document).ready(function () {

            $("#example").DataTable({
                stateSave: true,
                "drawCallback": function (settings) {
                    $('.delete').click(function () {
                        var confm = confirm("Are you sure Want to delete this Details");
                        if (confm == true) {
                            var id = $(this).closest("tr").find('td:eq(2)').text();
                            alert(id);
                        }
                    });
                    $('.txtReqQtyEdit').keyup(function () {
                        var preVal = $(this).closest("tr").find('td:eq(10)').text();
                        var multipleVal = $(this).closest("tr").find('td:eq(9)').text();
                        var newVal = ($(this).val() * multipleVal);
                        $(this).closest("tr").find('td:eq(10)').text(newVal);
                    });
                }
            });

            $('[id*=ddlTest]').change(function () {
                var Dept = $(this).children("option:selected").text();
                var ModelQty = "5.00";
                var ReqDate = "22/12/2020";
                var CalWeek = "4th Week 2021";
                var Remarks = "Demo For JS";

                if (Dept != "-Select-") {
                    $.ajax({
                        type: "POST",
                        url: "Trans_PO_GPIN_Sub.aspx/TestItemName",
                        data: '{PartName:"' + Dept + '",Filter:"' + ModelQty + ',' + ReqDate + ',' + CalWeek + ',' + Remarks + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: OnSuccess,
                        failure: function (response) {
                            alert(response.d);
                        },
                        error: function (response) {
                            alert(response.d);
                        }
                    });
                }
            });
            function OnSuccess(response) {
                $("#example").DataTable().clear();
                var length = Object.keys(response.d).length;
                for (var i = 0; i < length ; i++) {
                    var row = response.d[i];

                    $('#example').dataTable().fnAddData([
                        (i + 1),
                        row.SAPNo,
                        row.ItemName,
                        row.UOMCode,
                        "<input type='text' value=" + row.ReuiredQty + " class='form-control txtReqQtyEdit' />",
                        row.ModelQty,
                        row.SumOfQty,
                        row.Item_Rate,
                        row.ItemRate_Other,
                        row.CGSTP,
                        row.SGSTP,
                        row.IGSTP,
                        row.VATP,
                        row.ReuiredDate,
                        row.ItemRemarks,
                        "<input type='hidden' id='ModelCode' value=" + row.ModelCode + "/>",
                        "<input type='hidden' id='ModelName' value=" + row.ModelName + "/>",
                        "<input type='hidden' id='PrctPartCode' value=" + row.PrctPartCode + "/>",
                        "<input type='hidden' id='PrctPartName' value=" + row.PrctPartName + "/>",
                        "<input type='hidden' id='ItemCode' value=" + row.ItemCode + "/>",
                        "<i class='btn-danger btn-sm fa fa-trash-o delete'></i>"
                    ]);
                }
            }
        });
    </script>
</asp:Content>

