﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_Coral_PurOrd_Appoval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionUserType;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        SessionUserType = Session["RoleCode"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Coral Purchase Order Approval";
            divCancelReason.Visible = false;
            //Department_Code_Add();
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();

            if (SessionUserName == "Coral")
            {
                divAppType.Visible = true;
            }

        }
        Load_Data_Empty_Grid();
    }

    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";

            string SSQL = "";
            //User Rights Check Start
            bool ErrFlag = false;
            bool Rights_Check = false;

            Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Coral PO Approval");

            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Order..');", true);
            }
            //User Rights Check End


            if (!ErrFlag)
            {
                DataTable DT = new DataTable();

                SSQL = "Select * from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And PO_Status = '-1'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Details Already Cancelled.. Cannot Approved it..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }
            if (!ErrFlag)
            {
                DataTable DT = new DataTable();

                SSQL = "Select * from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";

                if (SessionUserName == "Coral")
                {

                    if (ddlAppType.SelectedItem.Text == "Purchase Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '1') ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "Vice President")
                    {
                        SSQL = SSQL + " And (PO_Status = '2' ) ";
                    }
                }
                else
                {
                    if (SessionUserType == "Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '1') ";
                    }
                    else if (SessionUserType == "VP")
                    {
                        SSQL = SSQL + " And (PO_Status = '2' ) ";
                    }
                }


                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Details Already get Approved..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }

            if (!ErrFlag)
            {
                DataTable DT = new DataTable();



                SSQL = "Select * from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";

                if (SessionUserName == "Coral")
                {
                    if (ddlAppType.SelectedItem.Text == "Purchase Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '1') ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "Vice President")
                    {
                        SSQL = SSQL + " And (PO_Status = '2' ) ";
                    }
                }
                else
                {
                    if (SessionUserType == "Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '1') ";
                    }
                    else if (SessionUserType == "VP")
                    {
                        SSQL = SSQL + " And (PO_Status = '2' ) ";
                    }
                }


                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count == 0)
                {
                    //Delete Main Table


                    SSQL = "Update Trans_Coral_PurOrd_Main set";

                    if (SessionUserName == "Coral")
                    {
                        if (ddlAppType.SelectedItem.Text == "Purchase Manager")
                        {
                            SSQL = SSQL + "  PO_Status = '1',Approvedby='" + SessionUserName + "',ApprovedDate=Convert(Datetime,GetDate(),103) ";
                        }
                        else if (ddlAppType.SelectedItem.Text == "Vice President")
                        {
                            SSQL = SSQL + "  PO_Status = '2',Approvedby2='" + SessionUserName + "',ApprovedDate2=Convert(Datetime,GetDate(),103) ";
                        }
                    }
                    else
                    {
                        if (SessionUserType == "Manager")
                        {
                            SSQL = SSQL + "  PO_Status = '1',Approvedby='" + SessionUserName + "',ApprovedDate=Convert(Datetime,GetDate(),103) ";
                        }
                        else if (SessionUserType == "VP")
                        {
                            SSQL = SSQL + "  PO_Status = '2',Approvedby2='" + SessionUserName + "',ApprovedDate2=Convert(Datetime,GetDate(),103) ";
                        }
                    }

                    SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And ";
                    SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " Std_PO_No ='" + e.CommandArgument.ToString() + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Details Approved Successfully');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order not is Avail give input correctly..');", true);
                }
            }
        }
        catch(Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }



    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            //User Rights Check Start
            bool ErrFlag = false;
            bool Rights_Check = false;

            divCancelReason.Visible = true;

            Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Coral PO Approval");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Purchase Request..');", true);
            }
            //User Rights Check End

            if (!ErrFlag)
            {
                DataTable DT = new DataTable();

                SSQL = "Select * from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";

                if (SessionUserName == "Coral")
                {
                    if (ddlAppType.SelectedItem.Text == "Purchase Manager")
                    {
                        SSQL = SSQL + " And PO_Status = '1' ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "Vice President")
                    {
                        SSQL = SSQL + " And PO_Status = '2'  ";
                    }
                }
                else
                {
                    if (SessionUserType == "Manager")
                    {
                        SSQL = SSQL + " And PO_Status = '1' ";
                    }
                    else if (SessionUserType == "VP")
                    {
                        SSQL = SSQL + " And PO_Status = '2'  ";
                    }
                }

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Details Already Approved.. Cannot Cancelled it..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }
            if (!ErrFlag)
            {
                DataTable DT = new DataTable();

                SSQL = "Select * from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And PO_Status = '-1'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Details Already get Cancelled..');", true);
                    txtRequestStatus_SelectedIndexChanged(sender, e);
                }
            }


            if (!ErrFlag)
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' ";
                SSQL = SSQL + " And PO_Status = '-1'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (txtCancelReason.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Reason For Cancellation');", true);
                    txtCancelReason.Focus();
                    ErrFlag = true;
                }

                if (!ErrFlag)
                {
                    if (DT.Rows.Count == 0)
                    {
                        //Delete Main Table

                        SSQL = "Update Trans_Coral_PurOrd_Main set PO_Status='0',Canceledby='" + SessionUserName + "',CanceledDate=Convert(Datetime,GetDate(),103),";
                        SSQL = SSQL + " CancelReason='" + txtCancelReason.Text + "' where Ccode='" + SessionCcode + "' And ";
                        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ";
                        SSQL = SSQL + " Std_PO_No ='" + e.CommandArgument.ToString() + "'";

                        objdata.RptEmployeeMultipleDetails(SSQL);


                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order Details Cancelled Successfully');", true);
                        txtRequestStatus_SelectedIndexChanged(sender, e);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Order not is Avail give input correctly..');", true);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }



    private void Load_Data_Empty_Grid()
    {
        try
        {
            lblErrorMsg.Text = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            DataTable DT2 = new DataTable();


            DT.Columns.Add("Std_PO_No");
            DT.Columns.Add("Std_PO_Date");
            DT.Columns.Add("Supp_Name");
            DT.Columns.Add("Pur_Request_No");

            if (txtRequestStatus.Text == "Pending List")
            {

                if (SessionUserName == "Coral")
                {
                    SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,";
                    SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                    SSQL = SSQL + " When MatType='2' then 'Tools'";
                    SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type ";
                    SSQL = SSQL + " from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' and ";
                    SSQL = SSQL + " LCode ='" + SessionLcode + "' and Active!='Delete' ";

                    if (ddlAppType.SelectedItem.Text == "Purchase Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "Vice President")
                    {
                        SSQL = SSQL + " And (PO_Status = '1'  ) ";
                    }
                }

                else
                {
                    SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,";
                    SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                    SSQL = SSQL + " When MatType='2' then 'Tools'";
                    SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type ";
                    SSQL = SSQL + " From Trans_Coral_PurOrd_Main  ";
                    SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                    // SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) And Active!='Delete' Order By Std_PO_No desc";
                    if (SessionUserType == "Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) ";
                    }
                    else if (SessionUserType == "VP")
                    {
                        SSQL = SSQL + " And (PO_Status = '1'  ) ";
                    }
                }

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Pending.DataSource = DT;
                Repeater_Pending.DataBind();
                pnlPOPending.Visible = true;
                pnlApproval.Visible = false;
                pnlReject.Visible = false;
            }
            else if (txtRequestStatus.Text == "Approved List")
            {
                SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No,";
                SSQL = SSQL + " Case when ReceiveStatus='1' then 'Goods Received' else 'Goods Not Received' End Status ";
                SSQL = SSQL + " from Trans_Coral_PurOrd_Main ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                //SSQL = SSQL + " And PO_Status = '1' And Active!='Delete' Order By Std_PO_No desc";

                if (SessionUserType == "Manager")
                {
                    SSQL = SSQL + " And (PO_Status = '1' ) ";
                }
                else if (SessionUserType == "VP")
                {
                    SSQL = SSQL + " And (PO_Status = '2' ) ";
                }
                SSQL = SSQL + " And Active!='Delete' Order By Std_PO_No desc";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Approve.DataSource = DT;
                Repeater_Approve.DataBind();
                pnlApproval.Visible = true;
                pnlPOPending.Visible = false;
                pnlReject.Visible = false;
            }
            else if (txtRequestStatus.Text == "Rejected List")
            {
                SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No,CancelReason from Trans_Coral_PurOrd_Main ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                SSQL = SSQL + " And PO_Status = '-1' And Active!='Delete' Order By Std_PO_No desc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Rejected.DataSource = DT;
                Repeater_Rejected.DataBind();
                pnlReject.Visible = true;
                pnlPOPending.Visible = false;
                pnlPOPending.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    //private void Department_Code_Add()
    //{
    //    string SSQL = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDepartment.Items.Clear();
    //    SSQL = "Select (DeptName + '-' + DeptCode) as DpetName_Join from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptName Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //    txtDepartment.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDepartment.Items.Add(Main_DT.Rows[i]["DpetName_Join"].ToString());
    //    }
    //}

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }

    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblErrorMsg.Text = "";
            //string[] Department_Spilit = txtDepartment.Text.Split('-');
            //string Department_Code = "";
            //string Department_Name = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            DataTable DT2 = new DataTable();

            DT.Columns.Add("Std_PO_No");
            DT.Columns.Add("Std_PO_Date");
            DT.Columns.Add("Supp_Name");
            DT.Columns.Add("Pur_Request_No");



            if (txtRequestStatus.Text == "Pending List")
            {
                if (SessionUserName == "Coral")
                {
                    SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,";
                    SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                    SSQL = SSQL + " When MatType='2' then 'Tools' ";
                    SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type ";
                    SSQL = SSQL + " from Trans_Coral_PurOrd_Main ";
                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                    // SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) And Active!='Delete' Order By Std_PO_No Asc";
                    if (ddlAppType.SelectedItem.Text == "Purchase Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "Vice President")
                    {
                        SSQL = SSQL + " And (PO_Status = '1' ) ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "Accounts Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '2' ) ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "JMD")
                    {
                        SSQL = SSQL + " And (PO_Status = '3' ) ";
                    }

                    SSQL = SSQL + " And Active!='Delete' Order By Std_PO_No desc";
                }
                else
                {
                    SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,";
                    SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                    SSQL = SSQL + " When MatType='2' then 'Tools' ";
                    SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type ";
                    SSQL = SSQL + " from Trans_Coral_PurOrd_Main ";
                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                    // SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) And Active!='Delete' Order By Std_PO_No Asc";
                    if (SessionUserType == "Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '0' or PO_Status is null) ";
                    }
                    else if (SessionUserType == "VP")
                    {
                        SSQL = SSQL + " And (PO_Status = '1' ) ";
                    }
                    else if (SessionUserType == "Finance")
                    {
                        SSQL = SSQL + " And (PO_Status = '2' ) ";
                    }
                    else if (SessionUserType == "JMD")
                    {
                        SSQL = SSQL + " And (PO_Status = '3' ) ";
                    }

                    SSQL = SSQL + " And Active!='Delete' Order By Std_PO_No desc";
                }
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (txtRequestStatus.Text == "Approved List")
            {
                if (SessionUserName == "Coral")
                {
                    SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,";
                    SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                    SSQL = SSQL + " When MatType='2' then 'Tools'";
                    SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type,";
                    SSQL = SSQL + " Case when PO_Status='1' then 'PM Approval Done' When PO_Status='2' Then 'VP Approval Done'";
                    SSQL = SSQL + " When PO_Status='3' Then 'A/c Approval Done' When PO_Status='4' Then 'JMD Approval Done' End Approval_Status ";
                    SSQL = SSQL + " from Trans_Coral_PurOrd_Main ";
                    SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

                    if (ddlAppType.SelectedItem.Text == "Purchase Manager")
                    {
                        SSQL = SSQL + " And PO_Status = '1' ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "Vice President")
                    {
                        SSQL = SSQL + " And PO_Status = '2' ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "Accounts Manager")
                    {
                        SSQL = SSQL + " And PO_Status = '3' ";
                    }
                    else if (ddlAppType.SelectedItem.Text == "JMD")
                    {
                        SSQL = SSQL + " And PO_Status = '4' ";
                    }


                    SSQL = SSQL + " And Active!='Delete' Order By Std_PO_No desc";

                    //SSQL = SSQL + " And Active!='Delete' Order By Std_PO_No desc";
                }
                else
                {
                    SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name,";
                    SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                    SSQL = SSQL + " When MatType='2' then 'Tools'";
                    SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type,";
                    SSQL = SSQL + " Case when PO_Status='1' then 'Purchase Manager Approval Success' When PO_Status='2' Then 'VP Approval Success'";
                    SSQL = SSQL + " When PO_Status='3' Then 'Accounts Manager Approval Success' When PO_Status='4' Then 'JMD Approval Success' End Approval_Status ";
                    SSQL = SSQL + " from Trans_Coral_PurOrd_Main ";
                    SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                    //SSQL = SSQL + " And PO_Status = '1' And Active!='Delete' Order By Std_PO_No desc";
                    if (SessionUserType == "Manager")
                    {
                        SSQL = SSQL + " And (PO_Status = '1' ) ";
                    }
                    else if (SessionUserType == "VP")
                    {
                        SSQL = SSQL + " And (PO_Status = '2' ) ";
                    }
                    else if (SessionUserType == "Finance")
                    {
                        SSQL = SSQL + " And (PO_Status = '3' ) ";
                    }
                    else if (SessionUserType == "JMD")
                    {
                        SSQL = SSQL + " And (PO_Status = '4' ) ";
                    }
                    //else
                    //{
                    //    SSQL = SSQL + " And (PO_Status = '1') ";
                    //}
                    SSQL = SSQL + " And Active!='Delete' Order By Std_PO_No desc";
                }
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Approve.DataSource = DT;
                Repeater_Approve.DataBind();
                pnlApproval.Visible = true;
                pnlPOPending.Visible = false;
                pnlReject.Visible = false;
            }
            else if (txtRequestStatus.Text == "Rejected List")
            {
                SSQL = "Select RefNo,Std_PO_No,Std_PO_Date,Supp_Name, ";
                SSQL = SSQL + " Case When MatType='1' then 'RawMaterial' ";
                SSQL = SSQL + " When MatType='2' then 'Tools' ";
                SSQL = SSQL + " When MatType='3' then 'Asset' end Material_Type,CancelReason ";
                SSQL = SSQL + " from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And PO_Status = '-1' Order By Std_PO_No Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Rejected.DataSource = DT;
                Repeater_Rejected.DataBind();
                pnlReject.Visible = true;
                pnlPOPending.Visible = false;
                pnlPOPending.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblErrorMsg.Text = ex.Message.ToString();
        }
    }

    //protected void txtDepartment_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (txtDepartment.Text != "-Select-")
    //    {
    //        txtRequestStatus_SelectedIndexChanged(sender, e);
    //    }
    //}

    //private void Department_Code_Add_Test()
    //{
    //    string SSQL = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDeptCode_test.Items.Clear();
    //    SSQL = "Select DeptCode from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptCode Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //    txtDeptCode_test.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDeptCode_test.Items.Add(Main_DT.Rows[i]["DeptCode"].ToString());
    //    }
    //}


    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {
        //string Enquiry_No_Str = e.CommandName.ToString();
        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Transaction_No");
        //Session.Remove("Std_PO_No");
        //Session["Std_PO_No"] = Enquiry_No_Str;
        //Response.Redirect("standard_po.aspx");


        string RptName = "";
        string StdPurOrdNo = "";
        string SupQtnNo = "";
        string DeptName = "";
        bool ErrFlag = false;

        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.PrintRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Coral Purchase Order");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Print Purchase Order...');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Std Purchase Order..');", true);
        }
        else
        {
            RptName = "Coral Purchase Order Details Invoice Format";
            StdPurOrdNo = e.CommandName.ToString();
            ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");
        }

    }

    protected void Repeater_Approve_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblStatus = Item.FindControl("lblStatus") as Label;

            if (lblStatus.Text == "Goods Received")
            {
                lblStatus.BackColor = Color.Green;
                lblStatus.ForeColor = Color.White;

            }
            else
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.ForeColor = Color.White;
            }
        }
    }

    protected void ddlAppType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtRequestStatus_SelectedIndexChanged(sender, e);
    }
}