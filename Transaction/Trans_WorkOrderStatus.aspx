﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_WorkOrderStatus.aspx.cs" Inherits="Transaction_Trans_WorkOrderStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <%--<div class="col-md-2">
                            <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom" runat="server" Text="Add New" OnClick="btnAddNew_Click" />

                        </div>--%>
                        <!-- /.col -->
                    </div>
                    <!--/.row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span>Work Order Status</span></h3>
                                    <div class="box-tools pull-right">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">

                                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-hover table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Sl. No</th>
                                                                <th>Work Order No</th>
                                                                <th>Order No</th>
                                                                <th>GeneratorModel</th>
                                                                <th>PartName</th>
                                                                <th>RevisionStatus</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("Work_Order_No")%></td>
                                                        <td><%# Eval("Order_No")%></td>
                                                        <td><%# Eval("GenModel")%></td>
                                                        <td><%# Eval("Part_Name")%></td>
                                                        <td><%# Eval("PartBOMRevisionStatus")%></td>
                                                        
                                                      
                                                        <td>
                                                            

                                                            <asp:LinkButton ID="btnPrint" class="btn btn-primary btn-sm fa fa-print" runat="server"
                                                                Text=""  OnCommand="GridEditOrderFormatClick" CommandArgument="Edit" CommandName='<%# Eval("Work_Order_No")%>'>
                                                            </asp:LinkButton>

                                                                 

                                                        <%--    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("Work_Order_No")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this details?');">
                                                            </asp:LinkButton>--%>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                            <input type="hidden" id="EditEnqNo" value="" />
                                            <asp:HiddenField ID="EditEnqAsp" Value="" runat="server" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        <script type="text/javascript">
            function send() {
                //$('#EditEnqNo').val($());
                // $("#btnPopup").bind("click", function () {
                window.open('Trans_MRPPrcoess_Sub.aspx', 'myWindow', 'width=1400,height=800');
                //});
            }
        </script>
    </div>
</asp:Content>