﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_quotation_Item_Rate_Checking : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Quotation Item Rate Checking";
            Load_Data_Empty_ItemCode();
        }
        

        Load_Data_Enquiry_Grid();
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Load_Data_Enquiry_Grid();
    }      

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select QTM.QuotNo,QTM.QuotDate,QTM.SuppName,QTS.Rate from Supp_Qut_Main QTM";
        query = query + " inner join Supp_Qut_Main_Sub QTS on QTM.Ccode=QTS.Ccode And QTM.Lcode=QTS.Lcode And QTM.FinYearCode=QTS.FinYearCode And QTM.QuotNo=QTS.QuotNo";
        query = query + " where QTM.Ccode='" + SessionCcode + "' And QTM.Lcode='" + SessionLcode + "' And QTM.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And QTS.Ccode='" + SessionCcode + "' And QTS.Lcode='" + SessionLcode + "' And QTS.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And QTS.ItemName='" + txtItemNameSelect.SelectedItem.Text + "'";
        query = query + " order by QTS.Rate,QTM.SuppName";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    private void Load_Data_Empty_ItemCode()
    {
        string query = "";
        DataTable Main_DT = new DataTable();


        query = "Select Mat_No,Raw_Mat_Name from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);



        txtItemNameSelect.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Mat_No"] = "-Select-";
        dr["Raw_Mat_Name"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "Raw_Mat_Name";
        txtItemNameSelect.DataValueField = "Mat_No";
        txtItemNameSelect.DataBind();

    }
}