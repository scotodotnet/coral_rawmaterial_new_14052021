﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Sales_Report_Sales_Cust_Proforma_Invoice_Report : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Page.Title = "CORAL ERP :: Customer Proforma Invoice Reports";
        if (!IsPostBack)
        {
            Load_Data_Empty_Trans_No();
            Load_Data_Empty_Generator_Model();
            Load_Data_Empty_Customer_Name();
        }
    }

    private void Load_Data_Empty_Customer_Name()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtCustomer_Name.Items.Clear();
        SSQL = "Select CustCode,CustName from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtCustomer_Name.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CustCode"] = "-Select-";
        dr["CustName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCustomer_Name.DataTextField = "CustName";
        txtCustomer_Name.DataValueField = "CustCode";
        txtCustomer_Name.DataBind();

    }

    private void Load_Data_Empty_Generator_Model()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();


        SSQL = "Select Distinct Item_Desc from Sales_Proforma_Invoice_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtGenerator_Model.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Item_Desc"] = "-Select-";
        dr["Item_Desc"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGenerator_Model.DataTextField = "Item_Desc";
        txtGenerator_Model.DataValueField = "Item_Desc";
        txtGenerator_Model.DataBind();

    }

    private void Load_Data_Empty_Trans_No()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();


        SSQL = "Select Sales_Inv_No from Sales_Proforma_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' Order by Sales_Inv_No Desc";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtTrans_No.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Sales_Inv_No"] = "-Select-";
        dr["Sales_Inv_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtTrans_No.DataTextField = "Sales_Inv_No";
        txtTrans_No.DataValueField = "Sales_Inv_No";
        txtTrans_No.DataBind();

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtTrans_No.SelectedIndex = 0;
        txtGenerator_Model.SelectedIndex = 0;
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }

    protected void btnReports_Click(object sender, EventArgs e)
    {
        //RptName = "Customer Sales Invoice Details Report";
        //string Genset_Model = "";
        //string Trans_No = "";
        //string Customer_Name_Str = "";

        //if (txtGenerator_Model.SelectedItem.Text != "-Select-")
        //{
        //    Genset_Model = txtGenerator_Model.SelectedItem.Text;
        //}
        //if (txtCustomer_Name.SelectedItem.Text != "-Select-")
        //{
        //    Customer_Name_Str = txtCustomer_Name.SelectedItem.Text;
        //}
        //if (txtTrans_No.SelectedItem.Text != "-Select-")
        //{
        //    Trans_No = txtTrans_No.SelectedItem.Text;
        //}

        //ResponseHelper.Redirect("Sales_Report_Display.aspx?Trans_No=" + Trans_No + "&Genset_Model=" + Genset_Model + "&Customer_Name=" + Customer_Name_Str + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");

    }

    protected void BtnInvoice_Format_Click(object sender, EventArgs e)
    {
        RptName = "Customer Proforma Invoice Format";
        string Genset_Model = "";
        string Trans_No = "";
        string Customer_Name_Str = "";
        bool ErrFlag = false;

        if (txtTrans_No.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Invoice No...');", true);
        }
        if (!ErrFlag)
        {
            if (txtGenerator_Model.SelectedItem.Text != "-Select-")
            {
                Genset_Model = txtGenerator_Model.SelectedItem.Text;
            }
            if (txtCustomer_Name.SelectedItem.Text != "-Select-")
            {
                Customer_Name_Str = txtCustomer_Name.SelectedItem.Text;
            }
            if (txtTrans_No.SelectedItem.Text != "-Select-")
            {
                Trans_No = txtTrans_No.SelectedItem.Text;
            }

            ResponseHelper.Redirect("Sales_Report_Display.aspx?Trans_No=" + Trans_No + "&Genset_Model=" + Genset_Model + "&Customer_Name=" + Customer_Name_Str + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
        }
    }
}
