﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Sales_Report_Sales_Report_Display : System.Web.UI.Page
{
    string SSQL = "";
    BALDataAccess objdata = new BALDataAccess();
    ReportDocument RD_Report = new ReportDocument();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool Errflag;
    string RptName = "";

    string Company_Name_Str = "";
    string Company_Add1_Str = "";
    string Company_Add2_Str = "";
    string Company_City_Str = "";
    string Company_PinCode_Str = "";
    string FromDate_Str = "";
    string ToDate_Str = "";
    string Genset_Model_Str = "";
    string Transaction_No_Str = "";
    string Cust_Name_Str = "";
    string Invoice_No_Str = "";
    string PartName = "";
    string PartQR = "";
    string Warehouse = "";
    string Location = "";

    string PoNo = "";
    string GateIN = "";
    string DispNo = "";
    string WONo = "";
    string PackNo = "";
    //string DispNo = "";
    string SONo = "";
    String Customer = "";
    String GeneratorModel = "";
    string FromDate = "";
    string ToDate = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        lblUploadSuccessfully.Text = "";
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        RptName = Request.QueryString["RptName"].ToString();

        //Get Company Dertails
        DataTable DT_Comp = new DataTable();
        SSQL = "Select * from AdminRights where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "'";
        DT_Comp = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Comp.Rows.Count != 0)
        {
            Company_Name_Str = DT_Comp.Rows[0]["Cname"].ToString();
            Company_Add1_Str = DT_Comp.Rows[0]["Address1"].ToString();
            Company_Add2_Str = DT_Comp.Rows[0]["Address2"].ToString();
            Company_City_Str = DT_Comp.Rows[0]["Location"].ToString();
            Company_PinCode_Str = DT_Comp.Rows[0]["Pincode"].ToString();            
        }


        if (RptName == "Generator QC Final Approval Details Report")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Generator_Model_QC_Approval_Report();
        }

        if (RptName == "Customer PO Details Report")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_PO_Details_Report();
        }

        if (RptName == "Customer PO Pending Report")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_PO_Pending_Report();
        }

        if (RptName == "Customer PO Invoice Format")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_PO_Invoice_Format_Report();
        }
        if (RptName == "Customer PO QR Report")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_PO_QR_Report();
        }
        if (RptName == "Customer Sales Invoice Details Report")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_Sales_Invoice_Details_Report();
        }

        if (RptName == "Customer Sales Invoice Format")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_Sales_Invoice_Format_Report();
        }

        if (RptName == "Customer Proforma Invoice Format")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_Sales_Proforma_Invoice_Format_Report();
        }

        if (RptName == "Customer Payment Voucher Details Report")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Invoice_No_Str = Request.QueryString["Invoice_No"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_Payment_Voucher_Details_Report();
        }

            if (RptName == "Customer Sales Gate Pass Details Report")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Invoice_No_Str = Request.QueryString["Invoice_No"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_GatePass_Out_Details_Report();
        }

        if (RptName == "Customer Sales Gate Pass Out Format")
        {
            Transaction_No_Str = Request.QueryString["Trans_No"].ToString();
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            Invoice_No_Str = Request.QueryString["Invoice_No"].ToString();
            Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_Sales_GatePass_Out_Format_Report();
        }

        if (RptName == "Single Customer Balance Details Report")
        {
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_Single_Party_Payment_Details();
        }

        if (RptName == "All Customer Bill Wise Balance Report")
        {
            Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            FromDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_ALL_Party_Payment_Details_Bill_Wise();
        }
        if (RptName == "Customer QR Status Report")
        {
            Transaction_No_Str = Request.QueryString["Cus_Pur_Order_No"].ToString();
            //Cust_Name_Str = Request.QueryString["Customer_Name"].ToString();
            //Genset_Model_Str = Request.QueryString["Genset_Model"].ToString();
            //FromDate_Str = Request.QueryString["FromDate"].ToString();
            //ToDate_Str = Request.QueryString["ToDate"].ToString();

            Customer_PO_QR_Status_Report();
        }
        if (RptName == "Sales Stock Report")
        {
            PartName = Request.QueryString["PartName"].ToString();
            PartQR = Request.QueryString["Part_QR_Code"].ToString();
            Warehouse = Request.QueryString["WareHouse"].ToString();
            Location = Request.QueryString["Location"].ToString();
            //FromDate_Str = Request.QueryString["FromDate"].ToString();
            //ToDate_Str = Request.QueryString["ToDate"].ToString();

            Sales_Stock_Report();
        }

        else if (RptName == "Dispatch Report")
        {


            DispNo = Request.QueryString["DispNo"].ToString();
            Customer = Request.QueryString["CustomerName"].ToString();
            GeneratorModel = Request.QueryString["GeneratorModel"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Dispatch_Report();

        }

        if (RptName == "Packing Report")
        {
            SONo = Request.QueryString["SONo"].ToString();
            PackNo = Request.QueryString["PackNo"].ToString();
            Customer = Request.QueryString["CustomerName"].ToString();
            PoNo = Request.QueryString["PONo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            PackNo = Request.QueryString["PackNo"].ToString();
            Packing_Report();

        }
        if (RptName == "Sale Order Detail Report")
        {


            SONo = Request.QueryString["SONo"].ToString();
            DispNo = Request.QueryString["DispNo"].ToString();
            Customer = Request.QueryString["CustomerName"].ToString();
            PoNo = Request.QueryString["PONo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            SaleOrderDetail_Report();

        }
    }

    private void Generator_Model_QC_Approval_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "Select Trans_No,Trans_Date,Generator_ModelName,Qty,Approved_By,Approved_Date,Remarks,UserName from Sales_QC_Test_Final_Main";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And Trans_No='" + Transaction_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And Generator_ModelName='" + Genset_Model_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,Trans_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,Trans_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Generator_Model_QC_Report.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";
            
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_PO_Details_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "select A.Cus_Pur_Order_No,A.Cus_Pur_Order_Date,A.CustomerName,A.Delivery_Date,B.GenModelName,B.PartNo,B.Part_Party_PO_No,B.Qty,B.Rate,B.LineTotal,A.Approvedby from Sales_Customer_Purchase_Order_Main A";
        SSQL = SSQL + " inner join Sales_Customer_Purchase_Order_Main_Sub B on A.Cus_Pur_Order_No=B.Cus_Pur_Order_No And A.Ccode=B.Ccode And A.Lcode=B.Lcode And A.FinYearCode=B.FinYearCode";
        SSQL = SSQL + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And A.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And B.Ccode='" + SessionCcode + "' And B.Lcode='" + SessionLcode + "' And B.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And A.Cus_Pur_Order_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And B.Cus_Pur_Order_No='" + Transaction_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And B.GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And A.CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,A.Cus_Pur_Order_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,A.Cus_Pur_Order_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by A.Cus_Pur_Order_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Purchase_Order_Det_Report.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_PO_Pending_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "Select '0' as PO_Rate, W, Cus_Pur_Order_No,Cus_Pur_Order_Date,GenModelName,isnull(sum(Qty),0) as OrderQty,";
        SSQL = SSQL + " CustomerName,isnull(sum(ReceivedQty),0) as ReceivedQty,isnull(sum(Balance_Qty),0) as Balance_Qty from (";

        SSQL = SSQL + " Select '1' as W, SPS.Cus_Pur_Order_No,SPM.Cus_Pur_Order_Date,SPS.GenModelName,SPS.Qty,SPM.CustomerName,";
        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty from Sales_Customer_Purchase_Order_Main SPM";
        SSQL = SSQL + " inner join Sales_Customer_Purchase_Order_Main_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode ";
        SSQL = SSQL + " And SPM.FinYearCode=SPS.FinYearCode And SPM.Cus_Pur_Order_No=SPS.Cus_Pur_Order_No";
        SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SPS.Cus_Pur_Order_No not in";
        SSQL = SSQL + " (Select Cus_Pur_Order_No from Sales_Invoice_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "')";
        
        SSQL = SSQL + " UNION ALL";

        SSQL = SSQL + " Select '1' as W, SPS.Cus_Pur_Order_No,SPM.Cus_Pur_Order_Date,SPS.GenModelName,SPS.Qty,SPM.CustomerName,";
        SSQL = SSQL + " '0' as ReceivedQty,'0' as Balance_Qty from Sales_Customer_Purchase_Order_Main SPM";
        SSQL = SSQL + " inner join Sales_Customer_Purchase_Order_Main_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode ";
        SSQL = SSQL + " And SPM.FinYearCode=SPS.FinYearCode And SPM.Cus_Pur_Order_No=SPS.Cus_Pur_Order_No";
        SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SPS.GenModelName not in";
        SSQL = SSQL + " (Select GenModelName from Sales_Invoice_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Cus_Pur_Order_No=SPS.Cus_Pur_Order_No)";
        SSQL = SSQL + " And SPS.Cus_Pur_Order_No in";
        SSQL = SSQL + " (Select Cus_Pur_Order_No from Sales_Invoice_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Cus_Pur_Order_No=SPS.Cus_Pur_Order_No)";
        
        SSQL = SSQL + " UNION ALL";

        SSQL = SSQL + " Select '1' as W,SPS.Cus_Pur_Order_No,SPM.Cus_Pur_Order_Date,SPS.GenModelName,SPS.Qty,SPM.CustomerName,";
        SSQL = SSQL + " sum(PRS.Qty) as ReceivedQty,(sum(SPS.Qty) - sum(PRS.Qty)) as Balance_Qty";
        SSQL = SSQL + " from Sales_Customer_Purchase_Order_Main SPM inner join Sales_Customer_Purchase_Order_Main_Sub SPS on SPM.Ccode=SPS.Ccode And SPM.Lcode=SPS.Lcode And SPM.FinYearCode=SPS.FinYearCode And SPM.Cus_Pur_Order_No=SPS.Cus_Pur_Order_No";
        SSQL = SSQL + " inner join Sales_Invoice_Main_Sub PRS on PRS.Ccode=SPS.Ccode And PRS.Lcode=PRS.Lcode And SPS.FinYearCode=PRS.FinYearCode And PRS.Cus_Pur_Order_No=SPS.Cus_Pur_Order_No And PRS.GenModelName=SPS.GenModelName";
        SSQL = SSQL + " where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "' And SPM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SPS.Ccode='" + SessionCcode + "' And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " group by SPS.Cus_Pur_Order_No,SPM.Cus_Pur_Order_Date,SPS.GenModelName,SPS.Qty,SPM.CustomerName";
        SSQL = SSQL + " having (sum(SPS.Qty) - sum(PRS.Qty)) > 0";
        SSQL = SSQL + " ) as P";
        SSQL = SSQL + " Where W='1'";

        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And Cus_Pur_Order_No='" + Transaction_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,Cus_Pur_Order_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,Cus_Pur_Order_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }


        SSQL = SSQL + " group by W,Cus_Pur_Order_No,Cus_Pur_Order_Date,GenModelName,CustomerName";
        SSQL = SSQL + " having (sum(Qty) - sum(ReceivedQty)) > 0";
        SSQL = SSQL + " order by Cus_Pur_Order_No,GenModelName";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Res.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Purchase_Order_Pending_Report.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }
    
    private void Customer_PO_QR_Report()
    {
        DataTable DT_Res = new DataTable();
        DataTable DT_Table = new DataTable();
        DataTable DT_QN = new DataTable();
        DataTable DT_N = new DataTable();
        DataTable DT_Cus = new DataTable();
        DataTable DT_NP = new DataTable();
        DT_Table.Columns.Add("Cus_Pur_Order_No");
        DT_Table.Columns.Add("Cus_Pur_Order_Date");
        DT_Table.Columns.Add("CustomerName");
        DT_Table.Columns.Add("Delivery_Date");
        DT_Table.Columns.Add("PaymentMode");
        DT_Table.Columns.Add("PartyPONo");
        DT_Table.Columns.Add("PartsName");
        DT_Table.Columns.Add("Rate");
        DT_Table.Columns.Add("HSN_Code");
        DT_Table.Columns.Add("PartyPODate");
        DT_Table.Columns.Add("PartNo");
        DT_Table.Columns.Add("GeneratorModel");
        DT_Table.Columns.Add("Parts_QR_CodeNo");
        DT_Table.Columns.Add("Cust_Add1");
        DT_Table.Columns.Add("Cust_Add2");
        DT_Table.Columns.Add("Cust_City");
        DT_Table.Columns.Add("Cust_PinCode");
        DT_Table.Columns.Add("Cust_GSTNO");

        DT_Table.Columns.Add("GenModel_QR_Code", System.Type.GetType("System.Byte[]"));
        DT_Table.Columns.Add("Parts_QR_Code", System.Type.GetType("System.Byte[]"));

        DT_Table.Columns.Add("Part_Party_PO_No");
        DT_Table.Columns.Add("GenModel_QR_Code_No");

        SSQL = "select POM.Cus_Pur_Order_No,pom.Cus_Pur_Order_Date,POM.CustomerName,pom.Delivery_Date,pom.PaymentMode,pt.HSN_Code,qr.Rate, ";
        SSQL = SSQL + "  pom.PartyPONo,pom.PartyPODate,pom.GeneratorModel,QR.Part_Name,QR.Part_No,qr.GenModel_QR_File_Name,QR.Parts_QR_File_Name,QR.Parts_QR_Code[Part_QR_CodeNo],";
        SSQL = SSQL + "  QR.Part_Party_PO_No,QR.GenModel_QR_Code as GenModel_QR_Code_No from Sales_Customer_Purchase_Order_Main POM ";
        SSQL = SSQL + "  inner join Sales_Customer_Purchase_Order_Main_Sub_QR_Det QR on QR.Cus_Pur_Order_No=POM.Cus_Pur_Order_No";
        SSQL = SSQL + " inner join PartsType PT on PT.PartsName=QR.Part_Name where QR.Cus_Pur_Order_No='"+ Transaction_No_Str + "' ";
        SSQL = SSQL + " And POM.Ccode='" + SessionCcode + "' And POM.Lcode='" + SessionLcode + "' And POM.FinYearCode='" + SessionFinYearCode + "'";
        

        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And POM.Cus_Pur_Order_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And POM.Cus_Pur_Order_No='" + Transaction_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And POM.GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And POM.CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,POM.Cus_Pur_Order_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,POM.Cus_Pur_Order_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by POM.Cus_Pur_Order_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from MstCustomer where CustName='" + DT_Res.Rows[0]["CustomerName"].ToString() + "'";
        SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT_Cus = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i = 0; i < DT_Res.Rows.Count; i++)
        {



            SSQL = "Select * from QR_Generator_Last_No";
            DT_N = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select * from QR_Parts_Last_No";
            DT_NP = objdata.RptEmployeeMultipleDetails(SSQL);

            String Filepath = DT_N.Rows[0]["QR_Img_Path"].ToString();
            String Filepaths = DT_NP.Rows[0]["QR_Img_Path"].ToString();
            byte[] imageByteData_GenModelQR = new byte[0];
            byte[] imageByteData_PartQR = new byte[0];

            //QR Code
            String PurOrd = DT_Res.Rows[i]["GenModel_QR_File_Name"].ToString();

            string path_1 = Filepath + "/" + PurOrd + ".png";
            string Photo_Path = "";

            if (!System.IO.File.Exists(Photo_Path))
            {
                imageByteData_GenModelQR = System.IO.File.ReadAllBytes(path_1);

            }

            String PurOrder = DT_Res.Rows[i]["Parts_QR_File_Name"].ToString();

            string path_2 = Filepaths + "/" + PurOrder + ".png";
            if (!System.IO.File.Exists(Photo_Path))
            {
                imageByteData_PartQR = System.IO.File.ReadAllBytes(path_2);
            }



            DT_Table.NewRow();
            DT_Table.Rows.Add();

            DT_Table.Rows[i]["Cus_Pur_Order_No"] = DT_Res.Rows[i]["Cus_Pur_Order_No"].ToString();
            DT_Table.Rows[i]["Cus_Pur_Order_Date"] = DT_Res.Rows[i]["Cus_Pur_Order_Date"].ToString();
            DT_Table.Rows[i]["CustomerName"] = DT_Res.Rows[i]["CustomerName"].ToString();
            DT_Table.Rows[i]["Delivery_Date"] = DT_Res.Rows[i]["Delivery_Date"].ToString();
            DT_Table.Rows[i]["PaymentMode"] = DT_Res.Rows[i]["PaymentMode"].ToString();
            DT_Table.Rows[i]["PartyPONo"] = DT_Res.Rows[i]["PartyPONo"].ToString();
            DT_Table.Rows[i]["PartsName"] = DT_Res.Rows[i]["Part_Name"].ToString();

            DT_Table.Rows[i]["PartyPODate"] = DT_Res.Rows[i]["PartyPODate"].ToString();
            DT_Table.Rows[i]["PartNo"] = DT_Res.Rows[i]["Part_No"].ToString();

            DT_Table.Rows[i]["GeneratorModel"] = DT_Res.Rows[i]["GeneratorModel"].ToString();


            DT_Table.Rows[i]["Cust_Add1"] = DT_Cus.Rows[0]["Address1"].ToString();
            DT_Table.Rows[i]["Cust_Add2"] = DT_Cus.Rows[0]["Address2"].ToString();
            DT_Table.Rows[i]["Cust_City"] = DT_Cus.Rows[0]["City"].ToString();
            DT_Table.Rows[i]["Cust_PinCode"] = DT_Cus.Rows[0]["Pincode"].ToString();
            DT_Table.Rows[i]["Cust_GSTNO"] = DT_Cus.Rows[0]["CstNo"].ToString();


            DT_Table.Rows[i]["Parts_QR_CodeNo"] = DT_Res.Rows[i]["Part_QR_CodeNo"].ToString();
            DT_Table.Rows[i]["Rate"] = DT_Res.Rows[i]["Rate"].ToString();
            DT_Table.Rows[i]["HSN_Code"] = DT_Res.Rows[i]["HSN_Code"].ToString();
            DT_Table.Rows[i]["GenModel_QR_Code"] = imageByteData_GenModelQR;
            DT_Table.Rows[i]["Parts_QR_Code"] = imageByteData_PartQR;

            DT_Table.Rows[i]["Part_Party_PO_No"] = DT_Res.Rows[i]["Part_Party_PO_No"].ToString();
            DT_Table.Rows[i]["GenModel_QR_Code_No"] = DT_Res.Rows[i]["GenModel_QR_Code_No"].ToString();




        }

            


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Table);
          
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Purchase_Order_QR.rpt"));
           
            RD_Report.SetDataSource(ds1.Tables[0]);

         
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }

    private void Customer_PO_QR_Status_Report()
    {
        DataTable DT_Res = new DataTable();
        SSQL = " Select Cus_Pur_Order_No,Cus_Pur_Order_Date,GenModelName,GenModel_QR_Code,Part_Name[PartsName],Part_No[PartNo],Part_Description, ";
        SSQL = SSQL + " Delivery_Date,Parts_QR_Code,Total_Item_Count,Total_Item_Qty_Count,Total_Item_Recev_Count,Total_Item_Recev_Qty_Count ";
        SSQL = SSQL + " from Sales_Customer_Purchase_Order_QR_Status";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
       
        if (Transaction_No_Str != "" && Transaction_No_Str !="-Select-")
        {
            SSQL = SSQL + " And Cus_Pur_Order_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And Cus_Pur_Order_No='" + Transaction_No_Str + "'";
        }
        //if (Genset_Model_Str != "")
        //{
        //    SSQL = SSQL + " And B.GenModelName='" + Genset_Model_Str + "'";
        //}
        //if (Cust_Name_Str != "")
        //{
        //    SSQL = SSQL + " And A.CustomerName='" + Cust_Name_Str + "'";
        //}
        //if (FromDate_Str != "" && ToDate_Str != "")
        //{
        //    SSQL = SSQL + " And Convert(Datetime,A.Cus_Pur_Order_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
        //    SSQL = SSQL + " And Convert(Datetime,A.Cus_Pur_Order_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        //}
        SSQL = SSQL + " Order by Cus_Pur_Order_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
       
            //if (DT_Res.Rows.Count < 10)
            //{
            //    Int32 Row_Count = 10 - DT_Res.Rows.Count;
            //    for (int i = DT_Res.Rows.Count; i < Row_Count; i++)
            //    {
            //        DT_Res.NewRow();
            //        DT_Res.Rows.Add();



            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_No"] = DT_Res.Rows[0]["Cus_Pur_Order_No"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_Date"] = DT_Res.Rows[0]["Cus_Pur_Order_Date"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["GenModelName"] = DT_Res.Rows[0]["GenModelName"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["Delivery_Date"] = DT_Res.Rows[0]["Delivery_Date"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["GenModel_QR_Code"] = DT_Res.Rows[0]["GenModel_QR_Code"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["PartsName"] = DT_Res.Rows[0]["PartsName"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["PartNo"] = DT_Res.Rows[0]["PartNo"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["Parts_QR_Code"] = DT_Res.Rows[0]["Parts_QR_Code"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["Total_Item_Count"] = DT_Res.Rows[0]["Total_Item_Count"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["Total_Item_Qty_Count"] = DT_Res.Rows[0]["Total_Item_Qty_Count"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["Total_Item_Recev_Count"] = DT_Res.Rows[0]["Total_Item_Recev_Count"].ToString();
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["Total_Item_Recev_Qty_Count"] = DT_Res.Rows[0]["Total_Item_Recev_Qty_Count"].ToString();
                   
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["PartsName"] = "";
            //        DT_Res.Rows[DT_Res.Rows.Count - 1]["PartNo"] = "";
                   



            //    }
            //}

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
           
            //Other Currency Report
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Purchase_Order_QR_Status.rpt"));
          
            RD_Report.SetDataSource(ds1.Tables[0]);

          
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    

    private void Customer_PO_Invoice_Format_Report()
    {
        DataTable DT_Res = new DataTable();
        SSQL = "Select A.Cus_Pur_Order_No,A.Cus_Pur_Order_Date,A.CustomerName,A.Delivery_Date,A.PaymentMode,A.PartyPONo,";
        SSQL = SSQL + " A.PartyPODate,A.Payment_Terms,A.Note,A.TotalAmt,A.DiscountPer,A.DiscountAmt,A.After_Discount_Total,";
        SSQL = SSQL + " A.CGST_Per,A.CGST_Amt,A.SGST_Per,A.SGST_Amt,A.IGST_Per,A.IGST_Amt,A.GST_Total_Amt,A.Round_Off_val,C.HSN_Code,B.PartsName,B.PartNo,B.UOM,";
        SSQL = SSQL + " A.Final_Net_Amt,A.GeneratorModel,B.Qty,B.Rate,B.LineTotal,'' as Cust_Add1,'' as Cust_Add2,'' as Cust_City,'' as Cust_PinCode,'' as Cust_GSTNO,A.Currency_Type,";
        SSQL = SSQL + " A.GenModelNo as GenModelName,'' as Part_Party_PO_No,'' as Parts_QR_Code,'' as GenModel_QR_Code from Sales_Customer_Purchase_Order_Main A";
        SSQL = SSQL + " inner join Sales_Customer_Purchase_Order_Main_Sub B on A.Ccode=B.Ccode And A.Lcode=B.Lcode inner Join PartsType C on C.PartsName=B.PartsName";
        SSQL = SSQL + " And A.FinYearCode=B.FinYearCode And A.Cus_Pur_Order_No=B.Cus_Pur_Order_No";
        SSQL = SSQL + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And A.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And B.Ccode='" + SessionCcode + "' And B.Lcode='" + SessionLcode + "' And B.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And A.Cus_Pur_Order_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And B.Cus_Pur_Order_No='" + Transaction_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And B.GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And A.CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,A.Cus_Pur_Order_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,A.Cus_Pur_Order_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by A.Cus_Pur_Order_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataTable DT_N = new DataTable();
            for (int i = 0; i < DT_Res.Rows.Count; i++)
            {
                //Get QR Code Det
                SSQL = "Select * from Sales_Customer_Purchase_Order_Main_Sub_QR_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And Cus_Pur_Order_No='" + DT_Res.Rows[i]["Cus_Pur_Order_No"].ToString() + "' And Part_No='" + DT_Res.Rows[i]["PartNo"].ToString() + "'";
                DT_N = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_N.Rows.Count !=0)
                {
                    DT_Res.Rows[i]["Part_Party_PO_No"] = DT_N.Rows[0]["Part_Party_PO_No"].ToString();
                    DT_Res.Rows[i]["Parts_QR_Code"] = DT_N.Rows[0]["Parts_QR_Code"].ToString();
                    DT_Res.Rows[i]["GenModel_QR_Code"] = DT_N.Rows[0]["GenModel_QR_Code"].ToString();
                }
                //Update Customer Address
                SSQL = "Select * from MstCustomer where CustName='" + DT_Res.Rows[i]["CustomerName"].ToString() + "'";
                SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                DT_N = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_N.Rows.Count != 0)
                {
                    DT_Res.Rows[i]["Cust_Add1"] = DT_N.Rows[0]["Address1"].ToString();
                    DT_Res.Rows[i]["Cust_Add2"] = DT_N.Rows[0]["Address2"].ToString();
                    DT_Res.Rows[i]["Cust_City"] = DT_N.Rows[0]["City"].ToString();
                    DT_Res.Rows[i]["Cust_PinCode"] = DT_N.Rows[0]["Pincode"].ToString();
                    DT_Res.Rows[i]["Cust_GSTNO"] = DT_N.Rows[0]["CstNo"].ToString();
                }
            }
            if (DT_Res.Rows.Count < 10)
            {
                Int32 Row_Count = 10 - DT_Res.Rows.Count;
                for (int i = DT_Res.Rows.Count; i < Row_Count; i++)
                {
                    DT_Res.NewRow();
                    DT_Res.Rows.Add();

                   

                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_No"] = DT_Res.Rows[0]["Cus_Pur_Order_No"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_Date"] = DT_Res.Rows[0]["Cus_Pur_Order_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CustomerName"] = DT_Res.Rows[0]["CustomerName"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Delivery_Date"] = DT_Res.Rows[0]["Delivery_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PaymentMode"] = DT_Res.Rows[0]["PaymentMode"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartyPONo"] = DT_Res.Rows[0]["PartyPONo"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartsName"] = DT_Res.Rows[0]["PartsName"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["UOM"] = DT_Res.Rows[0]["UOM"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartyPODate"] = DT_Res.Rows[0]["PartyPODate"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartNo"] = DT_Res.Rows[0]["PartNo"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Payment_Terms"] = DT_Res.Rows[0]["Payment_Terms"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["GeneratorModel"] = DT_Res.Rows[0]["GeneratorModel"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["GenModelName"] = DT_Res.Rows[0]["GenModelName"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["TotalAmt"] = DT_Res.Rows[0]["TotalAmt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DiscountPer"] = DT_Res.Rows[0]["DiscountPer"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DiscountAmt"] = DT_Res.Rows[0]["DiscountAmt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["After_Discount_Total"] = DT_Res.Rows[0]["After_Discount_Total"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CGST_Per"] = DT_Res.Rows[0]["CGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CGST_Amt"] = DT_Res.Rows[0]["CGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["SGST_Per"] = DT_Res.Rows[0]["SGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["SGST_Amt"] = DT_Res.Rows[0]["SGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["IGST_Per"] = DT_Res.Rows[0]["IGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["IGST_Amt"] = DT_Res.Rows[0]["IGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["GST_Total_Amt"] = DT_Res.Rows[0]["GST_Total_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Round_Off_val"] = DT_Res.Rows[0]["Round_Off_val"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Final_Net_Amt"] = DT_Res.Rows[0]["Final_Net_Amt"];
                    
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartsName"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartNo"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["UOM"] = "";

                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_Add1"] = DT_Res.Rows[0]["Cust_Add1"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_Add2"] = DT_Res.Rows[0]["Cust_Add2"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_City"] = DT_Res.Rows[0]["Cust_City"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_PinCode"] = DT_Res.Rows[0]["Cust_PinCode"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_GSTNO"] = DT_Res.Rows[0]["Cust_GSTNO"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Currency_Type"] = DT_Res.Rows[0]["Currency_Type"].ToString();
                  


                }
            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            if (Convert.ToDecimal(DT_Res.Rows[0]["CGST_Amt"].ToString()) == 0 && Convert.ToDecimal(DT_Res.Rows[0]["SGST_Amt"].ToString()) == 0 && Convert.ToDecimal(DT_Res.Rows[0]["IGST_Amt"].ToString()) == 0)
            {
                //Other Currency Report
                RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Purchase_Order_Format_New.rpt"));
            }
            else if (Convert.ToDecimal(DT_Res.Rows[0]["CGST_Amt"].ToString()) != 0)
            {
                //INR AND CGST
                RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Purchase_Order_Format_New.rpt"));
            }
            else if (Convert.ToDecimal(DT_Res.Rows[0]["IGST_Amt"].ToString()) != 0)
            {
                //INR AND IGST
                RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Purchase_Order_Format_New.rpt"));
            }
            else
            {
                RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Purchase_Order_Format_New.rpt"));
            }
            RD_Report.SetDataSource(ds1.Tables[0]);

            //RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            //RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Company_PinCode_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + SessionLcode + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_Sales_Invoice_Details_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "select A.Sales_Inv_No,A.Sales_Inv_Date,A.Cus_Pur_Order_No,A.Cus_Pur_Order_Date,A.CustomerName,A.DeliveryFrom,A.DeliveryAt,A.Approvedby,B.GenModelName,B.Qty,B.Rate,B.LineTotal from Sales_Invoice_Main A";
        SSQL = SSQL + " inner join Sales_Invoice_Main_Sub B on A.Sales_Inv_No=B.Sales_Inv_No And A.Ccode=B.Ccode And A.Lcode=B.Lcode And A.FinYearCode=B.FinYearCode";
        SSQL = SSQL + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And A.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And B.Ccode='" + SessionCcode + "' And B.Lcode='" + SessionLcode + "' And B.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And A.Sales_Inv_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And B.Sales_Inv_No='" + Transaction_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And B.GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And A.CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,A.Sales_Inv_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,A.Sales_Inv_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by A.Sales_Inv_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Invoice_Det_Report.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_Sales_Invoice_Format_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "Select A.Sales_Inv_No,A.Sales_Inv_Date,A.Cus_Pur_Order_No,A.Cus_Pur_Order_Date,A.CustomerName,A.PaymentMode,";
        SSQL = SSQL + " A.VehicleNo,A.DeliveryFrom,A.DeliveryAt,A.Note,A.TotalAmt,A.DiscountPer,A.DiscountAmt,A.After_Discount_Total,";
        SSQL = SSQL + " A.Freight_Charges,A.Taxable_Amount,A.CGST_Per,A.CGST_Amt,A.SGST_Per,A.SGST_Amt,A.IGST_Per,A.IGST_Amt,A.GST_Total_Amt,A.Round_Off_val,";
        SSQL = SSQL + " A.NetAmount,B.GenModelName,B.Qty,B.Rate,B.LineTotal,'' as Cust_Add1,'' as Cust_Add2,'' as Cust_City,'' as Cust_PinCode,'' as Cust_GSTNO,A.Currency_Type,B.Part_Name[PartName],B.Part_QR_Code[PartNo],'14256' as [HSNCode]  from Sales_Invoice_Main A";
        SSQL = SSQL + " inner join Sales_Invoice_Main_Sub B on A.Ccode=B.Ccode And A.Lcode=B.Lcode";
        SSQL = SSQL + " And A.FinYearCode=B.FinYearCode And A.Sales_Inv_No=B.Sales_Inv_No";
        SSQL = SSQL + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And A.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And B.Ccode='" + SessionCcode + "' And B.Lcode='" + SessionLcode + "' And B.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And A.Sales_Inv_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And B.Sales_Inv_No='" + Transaction_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And B.GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And A.CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,A.Sales_Inv_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,A.Sales_Inv_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by A.Sales_Inv_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataTable DT_N = new DataTable();
            for (int i = 0; i < DT_Res.Rows.Count; i++)
            {
                //Update Customer Address
                SSQL = "Select * from MstCustomer where CustName='" + DT_Res.Rows[i]["CustomerName"].ToString() + "'";
                SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                DT_N = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_N.Rows.Count != 0)
                {
                    DT_Res.Rows[i]["Cust_Add1"] = DT_N.Rows[0]["Address1"].ToString();
                    DT_Res.Rows[i]["Cust_Add2"] = DT_N.Rows[0]["Address2"].ToString();
                    DT_Res.Rows[i]["Cust_City"] = DT_N.Rows[0]["City"].ToString();
                    DT_Res.Rows[i]["Cust_PinCode"] = DT_N.Rows[0]["Pincode"].ToString();
                    DT_Res.Rows[i]["Cust_GSTNO"] = DT_N.Rows[0]["CstNo"].ToString();
                }
            }
            if (DT_Res.Rows.Count < 10)
            {
                Int32 Row_Count = 10 - DT_Res.Rows.Count;
                for (int i = DT_Res.Rows.Count; i < Row_Count; i++)
                {
                    DT_Res.NewRow();
                    DT_Res.Rows.Add();

                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Sales_Inv_No"] = DT_Res.Rows[0]["Sales_Inv_No"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Sales_Inv_Date"] = DT_Res.Rows[0]["Sales_Inv_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_No"] = DT_Res.Rows[0]["Cus_Pur_Order_No"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_Date"] = DT_Res.Rows[0]["Cus_Pur_Order_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CustomerName"] = DT_Res.Rows[0]["CustomerName"].ToString();                    
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PaymentMode"] = DT_Res.Rows[0]["PaymentMode"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["VehicleNo"] = DT_Res.Rows[0]["VehicleNo"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DeliveryFrom"] = DT_Res.Rows[0]["DeliveryFrom"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DeliveryAt"] = DT_Res.Rows[0]["DeliveryAt"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Note"] = DT_Res.Rows[0]["Note"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["TotalAmt"] = DT_Res.Rows[0]["TotalAmt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DiscountPer"] = DT_Res.Rows[0]["DiscountPer"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DiscountAmt"] = DT_Res.Rows[0]["DiscountAmt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["After_Discount_Total"] = DT_Res.Rows[0]["After_Discount_Total"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Taxable_Amount"] = DT_Res.Rows[0]["Taxable_Amount"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CGST_Per"] = DT_Res.Rows[0]["CGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CGST_Amt"] = DT_Res.Rows[0]["CGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["SGST_Per"] = DT_Res.Rows[0]["SGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["SGST_Amt"] = DT_Res.Rows[0]["SGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["IGST_Per"] = DT_Res.Rows[0]["IGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["IGST_Amt"] = DT_Res.Rows[0]["IGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["GST_Total_Amt"] = DT_Res.Rows[0]["GST_Total_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Round_Off_val"] = DT_Res.Rows[0]["Round_Off_val"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["NetAmount"] = DT_Res.Rows[0]["NetAmount"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Currency_Type"] = DT_Res.Rows[0]["Currency_Type"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Freight_Charges"] = DT_Res.Rows[0]["Freight_Charges"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartName"] = DT_Res.Rows[0]["PartName"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartNo"] = DT_Res.Rows[0]["PartNo"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["HSNCode"] = DT_Res.Rows[0]["HSNCode"];


                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartName"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartNo"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["HSNCode"] = "";


                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_Add1"] = DT_Res.Rows[0]["Cust_Add1"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_Add2"] = DT_Res.Rows[0]["Cust_Add2"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_City"] = DT_Res.Rows[0]["Cust_City"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_PinCode"] = DT_Res.Rows[0]["Cust_PinCode"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_GSTNO"] = DT_Res.Rows[0]["Cust_GSTNO"].ToString();
                }
            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            if (Convert.ToDecimal(DT_Res.Rows[0]["CGST_Amt"].ToString()) == 0 && Convert.ToDecimal(DT_Res.Rows[0]["SGST_Amt"].ToString()) == 0 && Convert.ToDecimal(DT_Res.Rows[0]["IGST_Amt"].ToString()) == 0)
            {
                //Other Currency Report
                RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Invoice_Format_Four_None.rpt"));
            }
            else if (Convert.ToDecimal(DT_Res.Rows[0]["CGST_Amt"].ToString()) != 0)
            {
                //INR AND CGST
                RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Invoice_Format_Four_CGST.rpt"));
            }
            else if (Convert.ToDecimal(DT_Res.Rows[0]["IGST_Amt"].ToString()) != 0)
            {
                //INR AND IGST
                RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Invoice_Format_Four_IGST.rpt"));
            }
            else
            {
                RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Invoice_Format_Four.rpt"));
            }
            RD_Report.SetDataSource(ds1.Tables[0]);

            //RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            //RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Company_PinCode_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + SessionLcode + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_Payment_Voucher_Details_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "Select Voucher_No,Voucher_Date,CustomerName,PaymentMode,ChequeDDNo,ChequeDDDate,Invoice_No,Note,Invoice_Amt,Voucher_Amt,Balance_Amt,Approvedby from Sales_Payment_Voucher_Main";        
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And Voucher_No='" + Transaction_No_Str + "'";
        }
        if (Invoice_No_Str != "")
        {
            SSQL = SSQL + " And Invoice_No='" + Invoice_No_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,Voucher_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,Voucher_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by Voucher_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Payment_Voucher_Det_Report.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_GatePass_Out_Details_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "select A.GP_Out_No,A.GP_Out_Date,A.Sales_Inv_No,A.Sales_Inv_Date,A.Cus_Pur_Order_No,A.Cus_Pur_Order_Date,A.CustomerName,A.VehicleNo,A.DeliveryFrom,A.DeliveryAt,";
        SSQL = SSQL + " A.Driver_Name,B.GenModelName,B.Qty,B.Rate,B.LineTotal,A.Approvedby,A.Model_Name,A.Model_QR_Code,B.Part_Name,B.Part_QR_Code from Sales_Gate_Pass_Out_Main A";
        SSQL = SSQL + " inner join Sales_Gate_Pass_Out_Main_Sub B on A.GP_Out_No=B.GP_Out_No And A.Ccode=B.Ccode And A.Lcode=B.Lcode And A.FinYearCode=B.FinYearCode";
        SSQL = SSQL + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And A.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And B.Ccode='" + SessionCcode + "' And B.Lcode='" + SessionLcode + "' And B.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And A.GP_Out_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And B.GP_Out_No='" + Transaction_No_Str + "'";
        }
        if (Invoice_No_Str != "")
        {
            SSQL = SSQL + " And A.Sales_Inv_No='" + Invoice_No_Str + "'";
            SSQL = SSQL + " And B.Sales_Inv_No='" + Invoice_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And B.GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And A.CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,A.GP_Out_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,A.GP_Out_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by A.GP_Out_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Gate_Pass_Out_Det_Report.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Sales_Stock_Report()
    {
        DataTable DT_Res = new DataTable();

        //Stock Check


        SSQL = "Select A.GenModel_QR_Code,A.Part_QR_Code[Parts_QR_Code],A.PartName[PartsName],A.PartNo, A.GenModel_Name as GenModel_QR_File_Name,B.Grosswgt,B.Netwgt,B.Lenth,B.Width,B.Hght[Hgt],A.Warehouse[WareHouse],A.Location as [Location]";
        SSQL = SSQL + "  from TRans_Stock_Det A inner join TRans_Packing_Sub B on A.Part_QR_Code=B.Part_QR_Code";
        SSQL = SSQL + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And A.FinYearCode='" + SessionFinYearCode + "'";


        if (PartName != "")
            {
                SSQL = SSQL + " And A.PartName='" + PartName + "'";
            }
            if (PartQR != "")
            {
                SSQL = SSQL + " And A.Part_QR_Code='" + PartQR + "'";

            }
            if (Warehouse != "")
            {
                SSQL = SSQL + " And A.Warehouse='" + Warehouse + "'";
            }
            if (Location != "")
            {
                SSQL = SSQL + " And A.Location='" + Location + "'";
            }
        //if (FromDate_Str != "" && ToDate_Str != "")
        //{
        //    SSQL = SSQL + " And Convert(Datetime,A.GP_Out_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
        //    SSQL = SSQL + " And Convert(Datetime,A.GP_Out_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        //}
        SSQL = SSQL + "group by A.GenModel_QR_Code,A.Part_QR_Code,A.PartName,A.PartNo,A.GenModel_Name,B.Grosswgt,B.Netwgt,B.Lenth,B.Width,";
        SSQL = SSQL + " B.Hght,A.Warehouse,A.Location Having sum(convert(int,(A.AddQty)))-sum(convert(int,(A.MinusQty)))  > 0";
        
        //SSQL = SSQL + " Order by A.GP_Out_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
       

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
           
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Stock_Report.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();


        
    }


    private void Customer_Sales_GatePass_Out_Format_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "Select A.GP_Out_No,A.GP_Out_Date,A.Sales_Inv_No,A.Sales_Inv_Date,A.Cus_Pur_Order_No,A.Cus_Pur_Order_Date,A.CustomerName,A.VehicleNo,";
        SSQL = SSQL + " A.DeliveryFrom,A.DeliveryAt,A.Driver_Name,A.Driver_Mbl_No,A.Proof_ID_Name,A.Proof_ID_No,A.Note,A.TotalAmt,B.GenModelName,B.Qty,";
        SSQL = SSQL + " B.Rate,B.LineTotal,'' as Cust_Add1,'' as Cust_Add2,'' as Cust_City,'' as Cust_PinCode,'' as Cust_GSTNO,A.Transport_Mode,";
        SSQL = SSQL + " B.Part_Name[PartName],B.Part_QR_Code[Part_QR_Code],A.Model_QR_Code,A.Model_Name from Sales_Gate_Pass_Out_Main A";
        SSQL = SSQL + " inner join Sales_Gate_Pass_Out_Main_Sub B on A.Ccode=B.Ccode And A.Lcode=B.Lcode";
        SSQL = SSQL + " And A.FinYearCode=B.FinYearCode And A.GP_Out_No=B.GP_Out_No";
        SSQL = SSQL + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And A.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And B.Ccode='" + SessionCcode + "' And B.Lcode='" + SessionLcode + "' And B.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And A.GP_Out_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And B.GP_Out_No='" + Transaction_No_Str + "'";
        }
        if (Invoice_No_Str != "")
        {
            SSQL = SSQL + " And A.Sales_Inv_No='" + Invoice_No_Str + "'";
            SSQL = SSQL + " And B.Sales_Inv_No='" + Invoice_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And B.GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And A.CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,A.GP_Out_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,A.GP_Out_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by A.GP_Out_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataTable DT_N = new DataTable();
            for (int i = 0; i < DT_Res.Rows.Count; i++)
            {
                //Update Customer Address
                SSQL = "Select * from MstCustomer where CustName='" + DT_Res.Rows[i]["CustomerName"].ToString() + "'";
                SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                DT_N = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_N.Rows.Count != 0)
                {
                    DT_Res.Rows[i]["Cust_Add1"] = DT_N.Rows[0]["Address1"].ToString();
                    DT_Res.Rows[i]["Cust_Add2"] = DT_N.Rows[0]["Address2"].ToString();
                    DT_Res.Rows[i]["Cust_City"] = DT_N.Rows[0]["City"].ToString();
                    DT_Res.Rows[i]["Cust_PinCode"] = DT_N.Rows[0]["Pincode"].ToString();
                    DT_Res.Rows[i]["Cust_GSTNO"] = DT_N.Rows[0]["CstNo"].ToString();
                }
            }
            if (DT_Res.Rows.Count < 15)
            {
                Int32 Row_Count = 15 - DT_Res.Rows.Count;
                for (int i = DT_Res.Rows.Count; i < Row_Count; i++)
                {
                    DT_Res.NewRow();
                    DT_Res.Rows.Add();

                    DT_Res.Rows[DT_Res.Rows.Count - 1]["GP_Out_No"] = DT_Res.Rows[0]["GP_Out_No"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["GP_Out_Date"] = DT_Res.Rows[0]["GP_Out_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Sales_Inv_No"] = DT_Res.Rows[0]["Sales_Inv_No"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Sales_Inv_Date"] = DT_Res.Rows[0]["Sales_Inv_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_No"] = DT_Res.Rows[0]["Cus_Pur_Order_No"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_Date"] = DT_Res.Rows[0]["Cus_Pur_Order_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CustomerName"] = DT_Res.Rows[0]["CustomerName"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["VehicleNo"] = DT_Res.Rows[0]["VehicleNo"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DeliveryFrom"] = DT_Res.Rows[0]["DeliveryFrom"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DeliveryAt"] = DT_Res.Rows[0]["DeliveryAt"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Driver_Name"] = DT_Res.Rows[0]["Driver_Name"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Driver_Mbl_No"] = DT_Res.Rows[0]["Driver_Mbl_No"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Proof_ID_Name"] = DT_Res.Rows[0]["Proof_ID_Name"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Proof_ID_No"] = DT_Res.Rows[0]["Proof_ID_No"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Note"] = DT_Res.Rows[0]["Note"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["TotalAmt"] = DT_Res.Rows[0]["TotalAmt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Transport_Mode"] = DT_Res.Rows[0]["Transport_Mode"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Part_QR_Code"] = DT_Res.Rows[0]["Part_QR_Code"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartName"] = DT_Res.Rows[0]["PartName"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Part_QR_Code"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PartName"] ="";

                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_Add1"] = DT_Res.Rows[0]["Cust_Add1"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_Add2"] = DT_Res.Rows[0]["Cust_Add2"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_City"] = DT_Res.Rows[0]["Cust_City"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_PinCode"] = DT_Res.Rows[0]["Cust_PinCode"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_GSTNO"] = DT_Res.Rows[0]["Cust_GSTNO"].ToString();

                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Model_QR_Code"] = DT_Res.Rows[0]["Model_QR_Code"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Model_Name"] = DT_Res.Rows[0]["Model_Name"].ToString();
                }
            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_GatePass_Out_Format.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            //RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Company_PinCode_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + SessionLcode + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_Single_Party_Payment_Details()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "select Trans_No,Trans_Date,Trans_Type,CustomerName,Remarks,TotalQty,BillNetAmount,PaidAmount,Invoice_No from Sales_Customer_Transaction_Det";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,Trans_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,Trans_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by CustomerName,Convert(Datetime,Trans_Date,103) Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_CustomerPaymentDet_Single.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            RD_Report.DataDefinition.FormulaFields["Company_Name"].Text = "'" + Company_Name_Str + "'";
            
            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Fin_Year"].Text = "'" + SessionFinYearVal + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_ALL_Party_Payment_Details_Bill_Wise()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "Select CustomerName,Invoice_No,'' as Trans_Date,'' as Trans_Type,isnull(Sum(BillNetAmount),0) as BillNetAmount,";
        SSQL = SSQL + " isnull((Sum(BillNetAmount) - Sum(PaidAmount)),0) as PaidAmount from Sales_Customer_Transaction_Det";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,Trans_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,Trans_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " group by CustomerName,Invoice_No having (Sum(BillNetAmount) - Sum(PaidAmount)) > 0";
        SSQL = SSQL + " order by CustomerName,Invoice_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            //Update Invoice Date And Due Days
            //Invoice date and Due Days
            for (int i = 0; i < DT_Res.Rows.Count; i++)
            {
                SSQL = "Select Sales_Inv_Date as Inv_Date,DATEDIFF(d,Convert(Datetime,Sales_Inv_Date,103),GETDATE()) as Due_Days";
                SSQL = SSQL + " from Sales_Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And Sales_Inv_No='" + DT_Res.Rows[i]["Invoice_No"].ToString() + "'";
                DataTable DT_DA = new DataTable();
                DT_DA = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_DA.Rows.Count != 0)
                {
                    DT_Res.Rows[i]["Trans_Date"] = DT_DA.Rows[0]["Inv_Date"];
                    DT_Res.Rows[i]["Trans_Type"] = DT_DA.Rows[0]["Due_Days"];
                }
            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_CustomerPaymentDet_Bill_Wise.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            RD_Report.DataDefinition.FormulaFields["Company_Name"].Text = "'" + Company_Name_Str + "'";

            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";
            RD_Report.DataDefinition.FormulaFields["Fin_Year"].Text = "'" + SessionFinYearVal + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Customer_Sales_Proforma_Invoice_Format_Report()
    {
        DataTable DT_Res = new DataTable();

        SSQL = "Select A.Sales_Inv_No,A.Sales_Inv_Date,A.Cus_Pur_Order_No,A.Cus_Pur_Order_Date,A.CustomerName,A.PaymentMode,";
        SSQL = SSQL + " A.VehicleNo,A.DeliveryFrom,A.DeliveryAt,A.Note,A.TotalAmt,A.DiscountPer,A.DiscountAmt,A.After_Discount_Total,";
        SSQL = SSQL + " A.Freight_Charges,A.Taxable_Amount,A.CGST_Per,A.CGST_Amt,A.SGST_Per,A.SGST_Amt,A.IGST_Per,A.IGST_Amt,A.GST_Total_Amt,A.Round_Off_val,";
        SSQL = SSQL + " A.NetAmount,B.GenModelName,B.Qty,B.Rate,B.LineTotal,'' as Cust_Add1,'' as Cust_Add2,'' as Cust_City,'' as Cust_PinCode,'' as Cust_GSTNO,A.Currency_Type,";
        SSQL = SSQL + " B.Sap_No,B.HSN_Code,B.Item_Desc,B.UOM from Sales_Proforma_Invoice_Main A";
        SSQL = SSQL + " inner join Sales_Proforma_Invoice_Main_Sub B on A.Ccode=B.Ccode And A.Lcode=B.Lcode";
        SSQL = SSQL + " And A.FinYearCode=B.FinYearCode And A.Sales_Inv_No=B.Sales_Inv_No";
        SSQL = SSQL + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And A.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And B.Ccode='" + SessionCcode + "' And B.Lcode='" + SessionLcode + "' And B.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No_Str != "")
        {
            SSQL = SSQL + " And A.Sales_Inv_No='" + Transaction_No_Str + "'";
            SSQL = SSQL + " And B.Sales_Inv_No='" + Transaction_No_Str + "'";
        }
        if (Genset_Model_Str != "")
        {
            SSQL = SSQL + " And B.GenModelName='" + Genset_Model_Str + "'";
        }
        if (Cust_Name_Str != "")
        {
            SSQL = SSQL + " And A.CustomerName='" + Cust_Name_Str + "'";
        }
        if (FromDate_Str != "" && ToDate_Str != "")
        {
            SSQL = SSQL + " And Convert(Datetime,A.Sales_Inv_Date,103) >=Convert(Datetime,'" + FromDate_Str + "',103)";
            SSQL = SSQL + " And Convert(Datetime,A.Sales_Inv_Date,103) <=Convert(Datetime,'" + ToDate_Str + "',103)";
        }
        SSQL = SSQL + " Order by A.Sales_Inv_No Asc";
        DT_Res = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Res.Rows.Count != 0)
        {
            DataTable DT_N = new DataTable();
            for (int i = 0; i < DT_Res.Rows.Count; i++)
            {
                //Update Customer Address
                SSQL = "Select * from MstCustomer where CustName='" + DT_Res.Rows[i]["CustomerName"].ToString() + "'";
                SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                DT_N = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_N.Rows.Count != 0)
                {
                    DT_Res.Rows[i]["Cust_Add1"] = DT_N.Rows[0]["Address1"].ToString();
                    DT_Res.Rows[i]["Cust_Add2"] = DT_N.Rows[0]["Address2"].ToString();
                    DT_Res.Rows[i]["Cust_City"] = DT_N.Rows[0]["City"].ToString();
                    DT_Res.Rows[i]["Cust_PinCode"] = DT_N.Rows[0]["Pincode"].ToString();
                    DT_Res.Rows[i]["Cust_GSTNO"] = DT_N.Rows[0]["CstNo"].ToString();
                }
            }
            if (DT_Res.Rows.Count < 13)
            {
                Int32 Row_Count = 13 - DT_Res.Rows.Count;
                for (int i = DT_Res.Rows.Count; i < Row_Count; i++)
                {
                    DT_Res.NewRow();
                    DT_Res.Rows.Add();

                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Sales_Inv_No"] = DT_Res.Rows[0]["Sales_Inv_No"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Sales_Inv_Date"] = DT_Res.Rows[0]["Sales_Inv_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_No"] = DT_Res.Rows[0]["Cus_Pur_Order_No"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cus_Pur_Order_Date"] = DT_Res.Rows[0]["Cus_Pur_Order_Date"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CustomerName"] = DT_Res.Rows[0]["CustomerName"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["PaymentMode"] = DT_Res.Rows[0]["PaymentMode"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["VehicleNo"] = DT_Res.Rows[0]["VehicleNo"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DeliveryFrom"] = DT_Res.Rows[0]["DeliveryFrom"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DeliveryAt"] = DT_Res.Rows[0]["DeliveryAt"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Note"] = DT_Res.Rows[0]["Note"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["TotalAmt"] = DT_Res.Rows[0]["TotalAmt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DiscountPer"] = DT_Res.Rows[0]["DiscountPer"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["DiscountAmt"] = DT_Res.Rows[0]["DiscountAmt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["After_Discount_Total"] = DT_Res.Rows[0]["After_Discount_Total"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Taxable_Amount"] = DT_Res.Rows[0]["Taxable_Amount"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CGST_Per"] = DT_Res.Rows[0]["CGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["CGST_Amt"] = DT_Res.Rows[0]["CGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["SGST_Per"] = DT_Res.Rows[0]["SGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["SGST_Amt"] = DT_Res.Rows[0]["SGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["IGST_Per"] = DT_Res.Rows[0]["IGST_Per"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["IGST_Amt"] = DT_Res.Rows[0]["IGST_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["GST_Total_Amt"] = DT_Res.Rows[0]["GST_Total_Amt"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Round_Off_val"] = DT_Res.Rows[0]["Round_Off_val"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["NetAmount"] = DT_Res.Rows[0]["NetAmount"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Currency_Type"] = DT_Res.Rows[0]["Currency_Type"];
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Freight_Charges"] = DT_Res.Rows[0]["Freight_Charges"];



                    DT_Res.Rows[DT_Res.Rows.Count - 1]["GenModelName"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Sap_No"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["HSN_Code"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Item_Desc"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["UOM"] = "";

                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_Add1"] = DT_Res.Rows[0]["Cust_Add1"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_Add2"] = DT_Res.Rows[0]["Cust_Add2"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_City"] = DT_Res.Rows[0]["Cust_City"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_PinCode"] = DT_Res.Rows[0]["Cust_PinCode"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Cust_GSTNO"] = DT_Res.Rows[0]["Cust_GSTNO"].ToString();
                }
            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT_Res);
            //if (Convert.ToDecimal(DT_Res.Rows[0]["CGST_Amt"].ToString()) == 0 && Convert.ToDecimal(DT_Res.Rows[0]["SGST_Amt"].ToString()) == 0 && Convert.ToDecimal(DT_Res.Rows[0]["IGST_Amt"].ToString()) == 0)
            //{
            //    //Other Currency Report
            //    RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Proforma_Invoice_Format_Four_None.rpt"));
            //}
            //else if (Convert.ToDecimal(DT_Res.Rows[0]["CGST_Amt"].ToString()) != 0)
            //{
            //    //INR AND CGST
            //    RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Proforma_Invoice_Format_Four_CGST.rpt"));
            //}
            //else if (Convert.ToDecimal(DT_Res.Rows[0]["IGST_Amt"].ToString()) != 0)
            //{
            //    //INR AND IGST
            //    RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Proforma_Invoice_Format_Four_IGST.rpt"));
            //}
            //else
            //{
            //    RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Proforma_Invoice_Format_Four.rpt"));
            //}
            //RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Proforma_Invoice_Format_Four.rpt"));
            RD_Report.Load(Server.MapPath("~/crystal_Sales/Sales_Cust_Proforma_Invoice_Format.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Company_Name_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_Add1"].Text = "'" + Company_Add1_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_Add2"].Text = "'" + Company_Add2_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_City"].Text = "'" + Company_City_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Comp_PinCode"].Text = "'" + Company_PinCode_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["Location_Code"].Text = "'" + SessionLcode + "'";

            //RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Company_PinCode_Str + "'";
            //RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + SessionLcode + "'";

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void SaleOrderDetail_Report()
    {
        DataTable DT = new DataTable();

        SSQL = " select SOM.SONo,SOM.SODate,SOM.DispNo,SOM.CustomerName,SOM.PONo,SOM.PODate,SOM.PartyPONo, ";
        SSQL = SSQL + "  SOM.PartyPODate,SOM.RefNo,SOM.Remarks,SOM.Descrp,SOS.GeneratorModel,SOS.Qty,SOS.Rate,SOS.Amount,SOS.Part_Name,SOS.Part_QR_Code  ";
        SSQL = SSQL + "  from Trans_SaleOrder_Main SOM inner join Trans_SaleOrder_Sub SOS on SOS.SONo=SOM.SONo ";
        SSQL = SSQL + " where SOM.Ccode='" + SessionCcode + "' And SOM.Lcode='" + SessionLcode + "' And SOM.FinYearCode='" + SessionFinYearCode + "' And SOM.Status!='Delete'";


        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And DM.DispDate >='" + FromDate + "'";
            SSQL = SSQL + " And DM.DispDate <='" + ToDate + "'";
        }
        if (SONo != "-Select-" && SONo != "")
        {
            SSQL = SSQL + " And SOM.SONo='" + SONo + "'";
        }
        if (DispNo != "-Select-" && DispNo != "")
        {
            SSQL = SSQL + " And SOM.DispNo='" + DispNo + "' ";
        }
        if (Customer != "-Select-" && Customer != "")
        {
            SSQL = SSQL + " And SOM.CustomerName='" + Customer + "' ";
        }

        if (PoNo != "-Select-" && PoNo != "")
        {
            SSQL = SSQL + " And SOM.PONo='" + PoNo + "' ";
        }


        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {


            int Row_Count = DT.Rows.Count;
            for (int i = 0; i < Row_Count; i++)
            {
                //DT.NewRow();
                //DT.Rows.Add();

                DT.Rows[i]["SONo"] = DT.Rows[i]["SONo"].ToString();
                DT.Rows[i]["SODate"] = DT.Rows[i]["SODate"].ToString();
                DT.Rows[i]["DispNo"] = DT.Rows[i]["DispNo"].ToString();
                DT.Rows[i]["CustomerName"] = DT.Rows[i]["CustomerName"].ToString();
                DT.Rows[i]["PONo"] = DT.Rows[i]["PONo"].ToString();
                DT.Rows[i]["PODate"] = DT.Rows[i]["PODate"].ToString();
                DT.Rows[i]["PartyPONo"] = DT.Rows[i]["PartyPONo"].ToString();
                DT.Rows[i]["PartyPODate"] = DT.Rows[i]["PartyPODate"].ToString();
                DT.Rows[i]["RefNo"] = DT.Rows[i]["RefNo"].ToString();
                DT.Rows[i]["Remarks"] = DT.Rows[i]["Remarks"].ToString();
                DT.Rows[i]["Descrp"] = DT.Rows[i]["Descrp"].ToString();
                DT.Rows[i]["GeneratorModel"] = DT.Rows[i]["GeneratorModel"].ToString();
                DT.Rows[i]["Qty"] = DT.Rows[i]["Qty"].ToString();
                DT.Rows[i]["Rate"] = DT.Rows[i]["Rate"].ToString();
                DT.Rows[i]["Amount"] = DT.Rows[i]["Amount"].ToString();
                DT.Rows[i]["Part_Name"] = DT.Rows[i]["Part_Name"].ToString();
                DT.Rows[i]["Part_QR_Code"] = DT.Rows[i]["Part_QR_Code"].ToString();


            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT);

            RD_Report.Load(Server.MapPath("~/crystal_Sales/Coral_SaleOrder_Approval.rpt"));

            RD_Report.SetDataSource(ds1.Tables[0]);
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }

    }




    private void Packing_Report()
    {
        DataTable DT = new DataTable();

        SSQL = " select pm.Packno,pm.PackDate,pm.SalesOrdNo,pm.CustomerName,pm.PONo,pm.PODate, ";
        SSQL = SSQL + " pm.Remarks,ps.GeneratorModel,ps.Qty,ps.Remarks[Rmks],ps.Part_Name,ps.Part_QR_Code from Trans_Packing_Main PM ";
        SSQL = SSQL + " inner join Trans_Packing_Sub PS on PM.PackNo=PS.PackNo ";
        SSQL = SSQL + " where PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "' And PM.FinYearCode='" + SessionFinYearCode + "' And PM.Status!='Delete'";

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And pm.PackDate >='" + FromDate + "'";
            SSQL = SSQL + " And pm.PackDate <='" + ToDate + "'";
        }

        if (PackNo != "-Select-" && PackNo != "")
        {
            SSQL = SSQL + " And pm.Packno='" + PackNo + "' ";
        }

        if (SONo != "-Select-" && SONo != "")
        {
            SSQL = SSQL + " And pm.SalesOrdNo='" + SONo + "'";
        }

        if (Customer != "-Select-" && Customer != "")
        {
            SSQL = SSQL + " And pm.CustomerName='" + Customer + "' ";
        }

        if (PoNo != "-Select-" && PoNo != "")
        {
            SSQL = SSQL + " And pm.PONo='" + PoNo + "' ";
        }
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {


            int Row_Count = DT.Rows.Count;
            for (int i = 0; i < Row_Count; i++)
            {
                //DT.NewRow();
                //DT.Rows.Add();

                DT.Rows[i]["PackNo"] = DT.Rows[i]["PackNo"].ToString();
                DT.Rows[i]["PackDate"] = DT.Rows[i]["PackDate"].ToString();
                DT.Rows[i]["SalesOrdNo"] = DT.Rows[i]["SalesOrdNo"].ToString();
                DT.Rows[i]["CustomerName"] = DT.Rows[i]["CustomerName"].ToString();
                DT.Rows[i]["PONo"] = DT.Rows[i]["PONo"].ToString();
                DT.Rows[i]["PODate"] = DT.Rows[i]["PODate"].ToString();
                DT.Rows[i]["Remarks"] = DT.Rows[i]["Remarks"].ToString();
                //DT.Rows[i]["Description"] = DT.Rows[i]["Description"].ToString();
                DT.Rows[i]["GeneratorModel"] = DT.Rows[i]["GeneratorModel"].ToString();
                DT.Rows[i]["Qty"] = DT.Rows[i]["Qty"].ToString();
                DT.Rows[i]["Rmks"] = DT.Rows[i]["Rmks"].ToString();
                DT.Rows[i]["Part_Name"] = DT.Rows[i]["Part_Name"].ToString();
                DT.Rows[i]["Part_QR_Code"] = DT.Rows[i]["Part_QR_Code"].ToString();


            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT);

            RD_Report.Load(Server.MapPath("~/crystal_Sales/Coral_PackingList.rpt"));

            RD_Report.SetDataSource(ds1.Tables[0]);
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }

    }


    private void Dispatch_Report()
    {
        DataTable DT = new DataTable();

        SSQL = " select DM.DispNo,DM.DispDate,DM.CustomerName,DM.RefNo,DM.PreparedBy, ";
        SSQL = SSQL + " DM.Notes,DM.Descrp,DS.GeneratorModel,DS.Qty,ds.Remarks,ds.Part_Name,ds.Part_QR_Code ";
        SSQL = SSQL + " from Trans_DispatchNote_Main DM inner join Trans_DispatchNote_Sub DS on dm.DispNo=ds.DispNo ";
        SSQL = SSQL + " where DM.Ccode='" + SessionCcode + "' And DM.Lcode='" + SessionLcode + "' And DM.FinYearCode='" + SessionFinYearCode + "' And DM.Status!='Delete'";

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And DM.DispDate >='" + FromDate + "'";
            SSQL = SSQL + " And DM.DispDate <='" + ToDate + "'";
        }

        if (DispNo != "-Select-" && DispNo != "")
        {
            SSQL = SSQL + " And DM.DispNo='" + DispNo + "' ";
        }
        if (Customer != "-Select-" && Customer != "")
        {
            SSQL = SSQL + " And DM.CustomerName='" + Customer + "' ";
        }
        if (GeneratorModel != "-Select-" && GeneratorModel != "")
        {
            SSQL = SSQL + " And DM.GeneratorModel='" + GeneratorModel + "' ";
        }


        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {


            int Row_Count = DT.Rows.Count;
            for (int i = 0; i < Row_Count; i++)
            {
                //DT.NewRow();
                //DT.Rows.Add();

                DT.Rows[i]["DispNo"] = DT.Rows[i]["DispNo"].ToString();
                DT.Rows[i]["DispDate"] = DT.Rows[i]["DispDate"].ToString();
                DT.Rows[i]["CustomerName"] = DT.Rows[i]["CustomerName"].ToString();
                DT.Rows[i]["RefNo"] = DT.Rows[i]["RefNo"].ToString();
                DT.Rows[i]["Remarks"] = DT.Rows[i]["Remarks"].ToString();
                DT.Rows[i]["Descrp"] = DT.Rows[i]["Descrp"].ToString();
                DT.Rows[i]["GeneratorModel"] = DT.Rows[i]["GeneratorModel"].ToString();
                DT.Rows[i]["Qty"] = DT.Rows[i]["Qty"].ToString();
                DT.Rows[i]["Notes"] = DT.Rows[i]["Notes"].ToString();
                DT.Rows[i]["PreparedBy"] = DT.Rows[i]["PreparedBy"].ToString();
                DT.Rows[i]["Part_Name"] = DT.Rows[i]["Part_Name"].ToString();
                DT.Rows[i]["Part_QR_Code"] = DT.Rows[i]["Part_QR_Code"].ToString();


            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(DT);

            RD_Report.Load(Server.MapPath("~/crystal_Sales/Coral_DispatchNote.rpt"));

            RD_Report.SetDataSource(ds1.Tables[0]);
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }

    }
}
