﻿//Model Popup
function ShowPopup(url) {
    //   var popup;
    var modalId = 'modalDefault';
    var modalPlaceholder = $('#' + modalId + ' .modal-dialog .modal-content');
    $.get(url)
        .done(function (response) {
            modalPlaceholder.html(response);
            popup = $('#' + modalId + '').modal({
                keyboard: false,
                backdrop: 'static'
            });
        });
}
$(document).ready(function () {
    var datatabel = $('.tableUserRole').DataTable();
    $('.tableUserRole').on("click",".delete", function () {
        var currentRow = $(this).closest("tr");
        var id = currentRow.find("td:eq(0)").text().trimLeft();
        swal({
            title: "Are you sure want to Delete?",
            text: "You will not be able to restore the data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#dd4b39",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function () {
            var data = $(id).serializeJSON();
            data = JSON.stringify(data);
            $.ajax({
                type: 'GET',
                url: 'ApplicationUser/DeleteRole?Id=' + id,
                //data: data,
                contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                       // popup.modal('hide');
                        ShowMessage(data.message);
                        //datatabel.ajax.reload();
                      
                    } else {
                        ShowMessageError(data.message);
                    }
                }
            });
        });
    });
});
function SubmitAddEdit(form) {    
    var entity = 'CreateRole';
    var apiurl = '/ApplicationUser/' + entity;
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        var data = $(form).serializeJSON();
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: apiurl,
            data: data,
            contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    popup.modal('hide');
                    //ShowMessage(data.message);
                    //dataTable.ajax.reload();
                } else {
                    ShowMessageError(data.message);
                }
            }
        });

    }
    return false;
}