﻿$(document).ready(function () {
    $("#supp_ID").change(function () {
        if ($("#supp_ID").val() != "") {
            $.ajax({
                type: "GET",
                url: "/SupplierMasters/SupplierID_DuplicateCheck?Supp_ID=" + $("#supp_ID").val() + "",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response == false) {

                    } else {
                        $("#Supp_IDError").append("The Supplier ID Already Taken");
                        $("#supp_ID").focus();
                    }
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
        }
    });
});