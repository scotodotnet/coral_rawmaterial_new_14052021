﻿$(document).ready(function () {
    $("#DeptID").change(function () {
        if ($("#DeptID").val() != "") {
            $('#Dept_Code').val($(this).val());
            $('#DeptName').val($("#DeptID option:selected").text());
        }
    });
    $("#RamatID").change(function () {
        if ($("#RamatID").val() != "") {
            $('#Mat_No').val($(this).val());
            $('#Raw_Mat_Name').val($("#RamatID option:selected").text())
        }
    });

    $('#Dept_Code').keyup(function () {
        $('#Dept_Name option').map(function () {
            if ($(this).text == $('#Dept_Code').val()) return this;
        }).attr('selected', 'selected');
    });
});
function Delete(datas) {
    //alert(datas.getAttribute('data-id').trim());
    var dataPost =  datas.getAttribute('data-id').trim();
    $.ajax({
        type: "GET",
     //   url: "/Dept_BOM_Require/AjaxDelete",
        //dataType: "text",
        url: "/Dept_BOM_Require/AjaxDelete?id=" + dataPost + "",
        //data:JSON.stringify({ id:dataPost}),
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            if (response.lenght > 0) {
                location.reload(true);
                alert(response.result);
            }
        },
        error: function (req, status, error) {
            alert(error);
        }
    });
    location.reload(true);
}
//$(function () {

//    $('a[id*=btnDelete]').click(function (e) {
//        debugger;
//        e.preventDefault();
//        var id = $(this).parent()[0].id;
//        $('#confirmDialog').data('id', id).dialog('open');
//    });

//    $("#confirmDialog").dialog({
//        autoOpen: false,
//        modal: true,
//        resizable: false,
//        buttons: {
//            "Ok": function () {
//                var id = $(this).data("id");
//                window.location.href = '/Dept_BOM_Require/Delete/' + id;
//            },
//            "Cancel": function (e) {
//                $(this).dialog("close");
//            }
//        },
//    });
//});
function deleteItem(form) {
    $(form).parents('li').remove();
}
