﻿
    $(document).ready(function () {
        $("#UOMCode").change(function () {
            if ($("#UOMCode").val() != "") {
                $.ajax({
                    type: "GET",
                    url: "/UOMs/UomCodeDuplicate?code=" + $("#UOMCode").val() + "",
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        if (response == false) {

                        } else {
                            $("#UomTypeCode").append("The Material Number Already Taken");
                            $("#UOMCode").focus();
                        }
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });
            }
        });
    });
   