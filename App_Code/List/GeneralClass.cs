﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GeneralClass
/// </summary>
namespace GeneralClass
{
    public class LoadGenModelProductBasedItem
    {
        public string SApNo = "";
        public string Matname = "";
        public string UomTex = "";
        public string ReqQty = "";
    }

    public class LoadGenBasedPart
    {
        public string partNo;
        public string partName;
    }

    public class LoadGenerator
    {
        public string GenCode;
        public string GenName;
    }

    public class LoadQtyUOM
    {
        public decimal Qty;
        public string UOMCode;
        public string UOMName;
        public string RateINR;
        public string RateEUR;
    }
    public class LoadDept
    {
        public string DeptCode;
        public string DeptName;
    }

    public class LoadSAP
    {
        public string SAPNo;
        public string ItemName;
        public string ItemCode;
        public string ToolCode;
        public string UOMCode;
    }
    public class LoadPurReqEdit
    {
        public string mattype;
        public string ReqDate;
        public string DeptVal;
        public string Description;
        public string PurType;
    }
    public class LoadPurReqEditSub
    {
        public string SApNo;
        public string ToolsNo;
        public string Matname;
        public string UomTex;
        public string ReqQty;
        public string modelQty;
        public string SumQty;
        public string ReqDate;
        public string CalenderWeek;
        public string Remarks;
        public string RateINR;
        public string RateEUR;
    }

    public class LoadSupplier
    {
        public string SuppCode;
        public string SuppName;
    }

    public class LoadDepartment
    {
        public string DeptCode;
        public string DeptName;
    }

    public class LoadDeliveryMode
    {
        public string DeliveryCode;
        public string DeliveryMode;
    }

    public class LoadPurReqNo
    {
        public string partNo;
        public string partName;
    }

    public class Load_PurRquNo
    {
        public string PurRquNo;
    }

    public class Supplier
    {
        public string SuppCode;
        public string SuppName;
        public string TC;
    }

    public class SelectPurRquNo
    {
        public string DelDate;
    }

    public class EditPurOrdSelRqu
    {
        public string PurRquNo;
    }

    public class PurRquItemDet
    {
        public string SAPNo;
        public string ItemName;
        public string ItemCode;
        public string UOM;
        public string ModelQty;
        public string RequiredQty;
        public string OrderQty;
        public string ReuiredDate;
        public string Rate_INR;
        public string Rate_Other;
        public string ItemAmt;
        public string CGSTP;
        public string SGSTP;
        public string IGSTP;
        public string CGSTAmt;
        public string SGSTAmt;
        public string IGSTAmt;
        public string IncTaxAmt;
        public string CalWeek;

    }

    public class LoadPurOrdEdit
    {
        public string MatType;
        public string CoralPurOrdNo;
        public string PurRquNo;
        public string PurRquDt;
        public string PurDate;
        public string QuatNo;
        public string QuatDate;
        public string SuppName;
        public string SuppCode;
        public string DeptName;
        public string DeptCode;
        public string DeliModeCode;
        public string DeliMode;
        public string PayMode;
        public string PurRquDate;
        public string DeliAt;
        public string Payterms;
        public string SubIns;
        public string SplNotes;
        public string FreightChr;
        public string ETA;
        public string CurType;
        public string TaxType;
        public string PurType;

        public string TotQty;
        public string TotItmAmt;
        public string TotCGSTAmt;
        public string TotSGSTAmt;
        public string TotIGSTAmt;
        public string TotNetAmt;
        public string AOL;
        public string FinalAmt;
    }
    public class LoadPurOrdEditItemDet
    {
        public string SAPNo;
        public string ItemName;
        public string ItemCode;
        public string UOM;
        public string ModelQty;
        public string RequiredQty;
        public string OrderQty;
        public string ReuiredDate;
        public string Rate_INR;
        public string Rate_Other;
        public string ItemAmt;
        public string CGSTP;
        public string SGSTP;
        public string IGSTP;
        public string CGSTAmt;
        public string SGSTAmt;
        public string IGSTAmt;
        public string IncTaxAmt;
        public string CalWeek;
    }

    public class AmtCalc
    {
        public string SAPNo;
        public string ItemName;
        public string ItemCode;
        public string UOM;
        public string ModelQty;
        public string RequiredQty;
        public string RequireDate;
        public string OrdQty;
        public string RateINR;
        public string RateERU;
        public string ItemAmt;
        public string CGST;
        public string SGST;
        public string IGST;
        public string AmtWithGST;
    }

    public class RoundOff
    {
        public string FinalNetAmt;
    }

    public class LoadExcel
    {
        public string SAPNo;
        public string ItemName;
        public string UOM;
        public string ModelQty;
        public string RequiredQty;
        public string OrderQty;
        public string ReuiredDate;
        public string Rate_INR;
        public string Rate_Other;
        public string CalWeek;
        public string Remarks;
    }

    public class ItmDetSearch
    {
        public string SapNo;
    }
}