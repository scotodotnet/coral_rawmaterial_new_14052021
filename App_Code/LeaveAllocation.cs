﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for LeaveAllocation
/// </summary>
public class LeaveAllocation
{
    private string _EmpNo;
    private string _stafforLabour;
    private string _Department;
    private string _Designation;
    private string _LeaveDays;
    private string _FinYear;
    private string _LeaveAllForMoth;
	public LeaveAllocation()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string LeaveAllForMonth
    {
        get { return _LeaveAllForMoth; }
        set { _LeaveAllForMoth = value; }
    }
    public string FinYear
    {
        get { return _FinYear; }
        set { _FinYear = value; }
    }
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string StafforLabour
    {
        get { return _stafforLabour; }
        set { _stafforLabour = value; }
    }
    public string Department
    {
        get { return _Department; }
        set { _Department = value; }
    }
    public string Designation
    {
        get { return _Designation; }
        set { _Designation = value; }
    }
    public string LeaveDays
    {
        get { return _LeaveDays; }
        set { _LeaveDays = value; }
    }
}
