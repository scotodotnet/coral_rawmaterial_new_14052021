﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for EmpMainClass
/// </summary>
public class EmpMainClass
{
	public EmpMainClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _TokenNo;

    public string TokenNo
    {
        get { return _TokenNo; }
        set { _TokenNo = value; }
    }

    private string _ExistingNo;

    public string ExistingNo
    {
        get { return _ExistingNo; }
        set { _ExistingNo = value; }
    }

    private string _MachineID;

    public string MachineID
    {
        get { return _MachineID; }
        set { _MachineID = value; }
    }

   
    private string _EmpName;

    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }

    private string _Initial;

    public string Initial
    {
        get { return _Initial; }
        set { _Initial = value; }
    }

    private string _ShiftType;

    public string ShiftType
    {
        get { return _ShiftType; }
        set { _ShiftType = value; }
    }

    private string _Gender;

    public string Gender
    {
        get { return _Gender; }
        set { _Gender = value; }
    }


    private string _Age;

    public string Age
    {
        get { return _Age; }
        set { _Age = value; }
    }

    private string _DOB;

    public string DOB
    {
        get { return _DOB; }
        set { _DOB = value; }
    }

    private string _DOJ;

    public string DOJ
    {
        get { return _DOJ; }
        set { _DOJ = value; }
    }

    private string _Category;

    public string Category
    {
        get { return _Category; }
        set { _Category = value; }
    }

    private string _SubCategory;

    public string SubCategory
    {
        get { return _SubCategory; }
        set { _SubCategory = value; }
    }

    private string _MartialStatus;

    public string MartialStatus
    {
        get { return _MartialStatus; }
        set { _MartialStatus = value; }
    }

    private string _WagesType;

    public string WagesType
    {
        get { return _WagesType; }
        set { _WagesType = value; }
    }

    private string _IsActive;

    public string IsActive
    {
        get { return _IsActive; }
        set { _IsActive = value; }
    }

    private string _EmpStatus;

    public string EmpStatus
    {
        get { return _EmpStatus; }
        set { _EmpStatus = value; }
    }

    private string _EmpType;

    public string EmpType
    {
        get { return _EmpType; }
        set { _EmpType = value; }
    }

    private string _CompletedDate;

    public string CompletedDate
    {
        get { return _CompletedDate; }
        set { _CompletedDate = value; }
    }

    private string _AdminUser;

    public string AdminUser
    {
        get { return _AdminUser; }
        set { _AdminUser = value; }
    }
}
