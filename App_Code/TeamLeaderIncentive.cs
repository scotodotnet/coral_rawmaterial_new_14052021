﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for TeamLeaderIncentive
/// </summary>
public class TeamLeaderIncentive
{
    private string _TLEmpNo;
    private string _TLEmpName;
    private string _TLID;
    private string _TMEmpNo;
    private string _TMID;
    private string _TMName;
    private string _Months;
    private string _Fyear;
    private string _Days;
    private string _OT;
    private string _Incentive;
    private string _Commission;
    private string _Ccode;
    private string _Lcode;


	public TeamLeaderIncentive()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string TLEmpNo
    {
        get { return _TLEmpNo; }
        set { _TLEmpNo = value; }
    }
    public string TLEmpName
    {
        get { return _TLEmpName; }
        set { _TLEmpName = value; }
    }
    public string TLID
    {
        get { return _TLID; }
        set { _TLID = value; }
    }
    public string TMEmpNo
    {
        get { return _TMEmpNo; }
        set { _TMEmpNo = value; }
    }
    public string TMID
    {
        get { return _TMID; }
        set { _TMID = value; }
    }
    public string TMName
    {
        get { return _TMName; }
        set { _TMName = value; }
    }
    public string Months
    {
        get { return _Months; }
        set { _Months = value; }
    }
    public string FYear
    {
        get { return _Fyear; }
        set { _Fyear = value; }
    }
    public string Days
    {
        get { return _Days; }
        set { _Days = value; }
    }
    public string OT
    {
        get { return _OT; }
        set { _OT = value; }
    }
    public string Incentive
    {
        get { return _Incentive; }
        set { _Incentive = value; }
    }
    public string Commission
    {
        get { return _Commission; }
        set { _Commission = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }


}
