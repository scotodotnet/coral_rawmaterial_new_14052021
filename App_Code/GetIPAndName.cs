﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;

/// <summary>
/// Summary description for GetIPAndName
/// </summary>
public class GetIPAndName
{
    string _ipAddress = "";
    string _ipHostName = "";
    public string GetName()
    {
        _ipHostName = System.Net.Dns.GetHostName();
        return _ipHostName;
    }
    public string GetIP()
    {
        IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
        _ipAddress = Convert.ToString(ipHostInfo.AddressList.FirstOrDefault(address => address.AddressFamily == AddressFamily.InterNetwork));
        return _ipAddress;
    }
}