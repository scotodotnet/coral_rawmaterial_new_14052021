﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="change_password.aspx.cs" Inherits="change_password" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="content-wrapper">
        <asp:UpdatePanel ID="upUOMSub" runat="server">
            <ContentTemplate>
                  <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>User Password Change Details</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName"> New Password <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtNew_Password" runat="server" class="form-control"  TextMode="Password"/>
                                    <span id="FullName" class="text-danger"></span>
                                    <asp:RequiredFieldValidator ControlToValidate="txtNew_Password" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Conform Password <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtConform_Password" runat="server" class="form-control" TextMode="Password" />
                                    <span id="ShortName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtConform_Password" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>                            

                            <div class="col-md-6" style="padding-top:25px">
                                <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
      
    </div>

</asp:Content>

