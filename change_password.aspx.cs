﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class change_password : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionUOMCode = "";
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Change Password";
            

        }


    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;

        if (txtNew_Password.Text.ToString() == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the New Paswword...');", true);
        }

        if (txtConform_Password.Text.ToString() == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Conform Paswword...');", true);
        }

        if (txtNew_Password.Text.ToString() != txtConform_Password.Text.ToString())
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('New Password And Conform Password not Matched...');", true);
        }


        if (!ErrFlag)
        {
            SSQL = "Select * from UserDetails where UserID='" + SessionUserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count !=0)
            {
                SSQL = "Update UserDetails set Password='" + UTF8Encryption(txtNew_Password.Text) + "' where UserID='" + SessionUserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Update Login set Password='" + UTF8Encryption(txtNew_Password.Text) + "' where UserID='" + SessionUserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Password Changes updated...');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User ID Not Match with Our Database...');", true);
            }
        }
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }
}